var Home = {
	init : function() {
		Home.checkprivacyuser();
		Home.checkLocalStorageQuestion();
		Home.checkLocalStorageforPOST();
		Home.checkLocalStorageforTopic();
		// Home.checkLocalStorage();
		Home.populateFeaturedGroups();
		Home.initComponents();
		Home.initEvents();
		Home.populatePopularExplorePosts();
	},
	initComponents : function() {
		$("#feat_slider_container").show();
		$("#sugst_slider_container").hide();
		Home.initCarousel();
	},
	initEvents : function() {
		$("#feat_slider").click(function(){
			$("#feat_slider").addClass("active");
			$("#sugst_slider").removeClass("active");
			$("#feat_slider_container").show();
			$("#sugst_slider_container").hide();
			Home.populateFeaturedGroups();

		});

		$("#sugst_slider").click(function(){
			$("#feat_slider").removeClass("active");
			$("#sugst_slider").addClass("active");
			$("#feat_slider_container").hide();
			$("#sugst_slider_container").show();
			Home.populateSuggestedGroups();
		});
	},
	populatePopularExplorePosts : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();

		$.ajax({
			url: base_url + 'post/show_public_popular_posts/',
			method: 'POST',
			data: {
				keyword: '',
				alpha: '',
				fav: '',
				popular: '',
				privacy: '',
				latest: ''
			},
			success: function (response) {
				var length = $('.explore_popule_post_item_fromHome').length;
				for (var i=0; i<length; i++) {
					$(".explore_popule_post_item_fromHome").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				console.log("Popular POST::::::::" , response);


				$('.explore_post_slider1').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.post_no_data_item_template').clone()
						.removeClass('post_no_data_item_template')
						.addClass('explore_popule_post_item_fromHome')
						.attr('id', 'post_0aa')
						.show();
					$('.explore_post_slider1').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.post_data_item_template').clone()
							.removeClass('post_data_item_template')
							.addClass('explore_popule_post_item_fromHome')
							.attr('id', 'post_'+item.Raw_ID)
							.show();
						$('.explore_post_slider1').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#post_'+item.Raw_ID).find('.post_img a.post_link').attr('href', item.Po_URL).find('img').attr('src', item.Po_Thumb);
						$('#post_'+item.Raw_ID).find('.post_summary h4 a#board_like span').text(item.Po_Likes);
						$('#post_'+item.Raw_ID).find('.post_summary a.post_link').attr('href', item.Po_URL).text(item.Po_Title);
						$('#post_'+item.Raw_ID).find('.post_summary p.post_text').text(Home.getEllipsis(item.Po_Description, 100));
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name h5').text(item.Us_Name);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name p').text(item.Us_JobTitle);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info img').attr('src', item.Us_Thumb);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info').attr('href', item.Po_U_URL);


						if (item.Po_Privacy == "private") {
							$('#post_'+item.Raw_ID).find('.post_img div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	populateFeaturedGroups : function() {
		$.ajax({
			url: base_url + 'group/show_featured_groups/',
			method: 'POST',
			data: {
				keyword: null,
				alpha: null,
				fav: null,
				popular: null,
				privacy: null,
				latest: null
			},
			success: function (response) {
				var length = $('.featured_group_item').length;
				for (var i=0; i<length; i++) {
					$(".featured_group_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.feat_slider_container').owlCarousel({
					margin: 20,
					nav: true,
					dots: false,
					autoWidth:true,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.featured_group_no_data_item_template').clone()
							.removeClass('featured_group_no_data_item_template')
							.addClass('featured_group_item')
							.attr('id', 'featured_group_0')
							.show();
						$('.feat_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.featured_group_data_item_template').clone()
							.removeClass('featured_group_data_item_template')
							.addClass('featured_group_item')
							.attr('id', 'featured_group_'+item.Raw_ID)
							.show();
						$('.feat_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#featured_group_'+item.Raw_ID).find('.board_item figure a.group_link').attr('href', item.Gr_URL).find('img').attr('src', item.Gr_Thumb);
						$('#featured_group_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.Gr_Likes);
						$('#featured_group_'+item.Raw_ID).find('.board_item summary h3 a.group_link').attr('href', item.Gr_URL).text(Home.getEllipsis(item.Gr_Name, 35));
						if (item.Gr_Privacy == "private") {
							$('#featured_group_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	populateSuggestedGroups : function() {
		$.ajax({
			url: base_url + 'group/show_suggested_groups/',
			method: 'POST',
			data: {
				keyword: null,
				alpha: null,
				fav: null,
				popular: null,
				privacy: null,
				latest: null
			},
			success: function (response) {
				var length = $('.suggested_group_item').length;
				for (var i=0; i<length; i++) {
					$(".suggested_group_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.feat_slider_container').owlCarousel({
					margin: 20,
					nav: true,
					dots: false,
					autoWidth:true,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.suggested_group_no_data_item_template').clone()
							.removeClass('suggested_group_no_data_item_template')
							.addClass('suggested_group_item')
							.attr('id', 'suggested_group_0')
							.show();
						$('.sugst_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.suggested_group_data_item_template').clone()
							.removeClass('suggested_group_data_item_template')
							.addClass('suggested_group_item')
							.attr('id', 'suggested_group_'+item.Raw_ID)
							.show();
						$('.sugst_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#suggested_group_'+item.Raw_ID).find('.board_item figure a.group_link').attr('href', item.Gr_URL).find('img').attr('src', item.Gr_Thumb);
						$('#suggested_group_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.Gr_Likes);
						$('#suggested_group_'+item.Raw_ID).find('.board_item summary h3 a.group_link').attr('href', item.Gr_URL).text(Home.getEllipsis(item.Gr_Name, 35));
						if (item.Gr_Privacy == "private") {
							$('#suggested_group_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	getEllipsis : function(text, limit) {
		for (var i = text.length; i >= 0; i--) {
			if (text.substring(0, i).length < limit) {
				if (i < text.length) {
					text = text.substring(0, i) + "...";
				}
				return text;
			}
		}
	},
	initCarousel : function() {
		$('.ad_slider_container').owlCarousel({
			loop: true,
			margin: 25,
			autoplay: true,
			autoplayTimeout: 2500,
			autoWidth:true,
			autoplayHoverPause: true,
			nav: false,
			dots: false
		});
		$('.explore_post_slider1').owlCarousel({
			margin: 25,
			nav: true,
			dots: false,
			responsiveClass: true,
			responsive: {
				0: {
					items: 1
				},
				768: {
					items: 2
				},
				991: {
					items: 3
				}
			}
		});
		// this is a jaavascript part carousel also need to add the CDN of owlCarousel
		$('.about_slider_container').owlCarousel({
			items: 1,
			loop: true,
			margin: 10,
			autoplay: true,
			mouseDrag: false,
			nav: false,
			dots: true,
			animateOut: 'fadeOut',
			autoplayTimeout: 5000,
			autoplayHoverPause: true
	
		});

		$('.category_slider_container').owlCarousel({
			autoWidth:true,
			margin: 15,
			nav: false,
			dots: false,
			autoplayTimeout: 2000,
			autoplayHoverPause: true
	
		});

		// $('.feat_slider_container').owlCarousel({
		// 	margin: 25,
		// 	nav: true,
		// 	dots: false,
		// 	responsiveClass: true,
		// 	responsive: {
		// 	  0: {
		// 		items: 1
		// 	  },
		// 	  768: {
		// 		items: 2
		// 	  },
		// 	  991: {
		// 		items: 3
		// 	  }
		// 	}
		// });

		$('.sugst_slider_container').owlCarousel({
			margin: 25,
			nav: true,
			dots: false,
			responsiveClass: true,
			responsive: {
			  0: {
				items: 1
			  },
			  768: {
				items: 2
			  },
			  991: {
				items: 3
			  }
			}
		});

		// $('.user_popular_post_container').owlCarousel({
		// 	margin: 25,
		// 	nav: true,
		// 	dots: false,
		// 	responsiveClass: true,
		// 	responsive: {
		// 		0: {
		// 			items: 1
		// 		},
		// 		768: {
		// 			items: 2
		// 		},
		// 		991: {
		// 			items: 3
		// 		}
		// 	}
		// });
	},
	checkLocalStorageforPOST : function(){
		if (localStorage.getItem('post_id')) {
 				$role = '';
			if(localStorage.getItem('post_role')){
				if(localStorage.getItem('post_role') == 4){
					$role = 'follower';
				}else 	if(localStorage.getItem('post_role') == 3){
					$role = 'member';
				}
				else 	if(localStorage.getItem('post_role') == 2){
					$role = 'admin';
				}
				else 	if(localStorage.getItem('post_role') == 1){
					$role = 'superadmin';
				}
			}
			$.ajax({
				url: base_url + 'account/checkuseralreadymemberforPOST/',
				method:'POST',
				dataType: 'json',
				data:{post_id: localStorage.getItem('post_id'), post_referrer: localStorage.getItem('post_referrer') , post_role : $role},
				success:function(response) {

						  	if(response.status == 'success'){
								if(response.type == 'post'){
									Notify.success(response.message, {
										afterClose : function () {
											localStorage.removeItem('post_id');
											localStorage.removeItem('post_referrer');
											localStorage.removeItem('post_role');
											window.location = base_url + 'account/view/post/' + response.id;
										}
									});
								}else if(response.type == 'group'){

									Notify.success(response.message, {
										afterClose : function () {
											localStorage.removeItem('post_id');
											localStorage.removeItem('post_referrer');
											localStorage.removeItem('post_role');
											window.location = base_url + 'account/view/group/' + response.id;
										}
									});
								}

							}else {
								localStorage.removeItem('post_id');
								localStorage.removeItem('post_referrer');
								localStorage.removeItem('post_role');
								Notify.success(response.message, {

								});

							}


				}
			});
		}
	},
	checkLocalStorageforTopic : function(){
		if (localStorage.getItem('topic_id')) {
			$role = '';
			if(localStorage.getItem('topic_role')){
				if(localStorage.getItem('topic_role') == 4){
					$role = 'follower';
				}else 	if(localStorage.getItem('topic_role') == 3){
					$role = 'member';
				}
				else 	if(localStorage.getItem('topic_role') == 2){
					$role = 'admin';
				}
				else 	if(localStorage.getItem('topic_role') == 1){
					$role = 'superadmin';
				}
			}
			$.ajax({
				url: base_url + 'account/checkuseralreadymemberforTopic/',
				method:'POST',
				dataType: 'json',
				data:{topic_id: localStorage.getItem('topic_id'),
			    topic_referrer: localStorage.getItem('topic_referrer')
				 , topic_role : $role},
				success:function(response) {

					if(response.status == 'success'){
						if(response.type == 'group'){
							Notify.success(response.message, {
								afterClose : function () {
									localStorage.removeItem('topic_id');
									localStorage.removeItem('topic_referrer');
									localStorage.removeItem('topic_role');
									window.location = base_url + 'account/view/group/' + response.id;
								}
							});
						}else if(response.type == 'topic'){

							Notify.success(response.message, {
								afterClose : function () {
									localStorage.removeItem('topic_id');
									localStorage.removeItem('topic_referrer');
									localStorage.removeItem('topic_role');
									window.location = base_url + 'account/view/topic/' + response.id;
								}
							});
						}

					}else {
						localStorage.removeItem('topic_id');
						localStorage.removeItem('topic_referrer');
						localStorage.removeItem('topic_role');
						Notify.success(response.message, {

						});

					}


				}
			});
		}
	},
	checkprivacyuser  : function (){
		$.ajax({
			url: base_url + 'account/checkuserprivacy/',
			method:'POST',
			dataType: 'json',
			data:{  },
			success:function(response) {


				if(response[0]['aggrement_one'] != 'on'){
							$agrementone = '<div class="form-group px-4 ml-2 row" style="    margin-bottom: 0px;">\n' +
								'\t\t\t\t<input type="checkbox" required="" name="aggrement" id="agrrement"><p style=" width: 80%;     padding: 0px!important;\n' +
								'    \t\t\tmargin-top: 13px;\n' +
								'    \t\t\tmargin-left: 10px; color:black!important;">I have read and agree to BoardSpeak\'s\n' +
								'\n' +
								'\t\t\t\t\t<a href="javascript:void(0)" data-toggle="modal" data-target="#agreementModel">\n' +
								'\t\t\t\t\t\tTerms of Services\n' +
								'\t\t\t\t\t</a>\n' +
								'\t\t\t\t</p>\n' +
								'\t\t\t\t\t\t\t</div>';
							$('#privacydiv').append($agrementone);
				}
				if(response[0]['aggrement_two'] != 'on'){
								$agrementtwo = '<div class="form-group px-4 ml-2 row">\n' +
									'\t\t\t\t  <input type="checkbox" required="" name="aggrement1" id="agrrement1"><p style="    padding: 0px!important;\n' +
									'    \t\t\t margin-top: 13px;width: 80%;    margin-bottom: 0px;\n' +
									'    \t\t\t     margin-left: 10px; color:black!important;">I have read and agree to BoardSpeak\'s use of mu data\n' +
									'\t\t\t\t\t  for the service and everything else described in the <a class="" data-toggle="modal" data-target="#privacytModel" href="javascript:void(0)">Privacy Policy</a> and <a class="" href="javascript:void(0)">Data Processing Agreement</a></p>\n' +
									'\t\t\t\t  \t\t\t  </div>';
					$('#privacydiv').append($agrementtwo);
				}
				if(response[0]['aggrement_three'] != 'on'){
					$agrementthree = '<div class="form-group px-4 ml-2 row">\n' +
						'\t\t\t\t\t  <input type="checkbox" required="" name="aggrement2" id="agrrement2"><p style="    padding: 0px!important;\n' +
						'    \t\t width: 80%;\n' +
						'    \t\t\tmargin-left: 10px; color:black!important;">I have read and agree to BoardSpeak’ s  <a class="" href="javascript:void(0)" data-toggle="modal" data-target="#privacymodel2">Beta User Non-Disclosure Agreement</a>  </p>\n' +
						'\t\t\t\t\t  \t\t\t\t  </div>';
					$('#privacydiv').append($agrementthree);
				}

				if(response[0]['aggrement_one'] != 'on' || response[0]['aggrement_one'] != 'on' || response[0]['aggrement_three'] != 'on' ){
					$('#privacymodel').modal('show');
					$submite = 	' <div style="margin-top: 30px;  float: right;" class="form-group">\n' +
						'\t\t\t\t\t\t <button type="submit" style="margin-right: 10px;" class="btn btn-primary"  >Agree and Continue </button>\n' +
						'\t\t\t\t\t\t  \n' +
						'\t\t\t\t\t </div>';
					$('#privacydiv').append($submite);
				}

			}
		});
	},
	checkLocalStorageQuestion : function(){
		if (localStorage.getItem('group_id')) {

			$.ajax({
				url: base_url + 'account/checkuseralreadymember/',
				method:'POST',
				dataType: 'json',
				data:{ group_id: localStorage.getItem('group_id') },
				success:function(response) {


					if(response > 0){
 								 Home.checkLocalStorage();
						}else{
							$.ajax({
								url: base_url + 'account/getSaveQuestionwithEncode',
								method:'POST',
								dataType: 'json',
								data:{ group_id: localStorage.getItem('group_id') },
								success:function(response) {
									console.log(response);
									if(response[0]['group_Question'] != ''){

										$questionData = 	JSON.parse(response[0]['group_Question']);

										if($questionData != null && $questionData.length > 0 && $questionData != ''){
											$('#AnserModal').modal('show');
											$appendDiv = '';
											//
											$('#GiveAnswerDiv').empty();
											for(var x = 0 ; x < $questionData.length ; x++){
												$appendDiv += '<div>'+

													'<div class="form-group col-sm-12">'+
													'<label>'+$questionData[x]['value']+':<span class="required ">*</span></label>'+
													'<div class="col-12  ">'+
													'<input type="text"  required   class="form-control form-control-solid "   id="answerId'+$questionData[x]['id']+'" placeholder="Enter Answer">'+
													'</div>'+
													'</div>'+
													'</div>';

											}
											$('#GiveAnswerDiv').append($appendDiv);
										}else{
											Home.checkLocalStorage();
										}

									}else{
										Home.checkLocalStorage();
									}

									return;
									// Home.checkLocalStorage();
								}
							});
						}
					//
				}
			});


		}

	},
	checkLocalStorage : function() {
		if (localStorage.getItem('group_id')) {

					$role = '';
					if(localStorage.getItem('role')){
						if(localStorage.getItem('role') == 4){
							$role = 'follower';
						}else 	if(localStorage.getItem('role') == 3){
							$role = 'member';
						}
						else 	if(localStorage.getItem('role') == 2){
							$role = 'admin';
						}
						else 	if(localStorage.getItem('role') == 1){
							$role = 'superadmin';
						}
					}

			$.ajax({
				url: base_url + 'group/add_to_group_by_link',
				method:'POST',
				dataType: 'json',
				data:{group_id: localStorage.getItem('group_id'), referrer: localStorage.getItem('referrer') , role : $role},
				success:function(response) {


					// console.log("response" , response);
					// return;

					if (response.is_pending_invite) {
						Notify.confirm(response.message, function () {
							$.ajax({
								url: base_url + 'group/respond_to_group_invitation',
								method: 'POST',
								dataType: 'json',
								data: {
									id: response.group,
									action: 'accept'
								},
								success: function (response2) {
									Notify.success(response2.message, {
										afterClose : function () {
											window.location = base_url + 'account/view/group/' + response.group;
										}
									});
								}
							});
						}, function () {
							$.ajax({
								url: base_url + 'group/respond_to_group_invitation',
								method: 'POST',
								dataType: 'json',
								data: {
									id: response.group,
									action: 'decline'
								},
								success: function (response2) {
									Notify.success(response2.message, {
										afterClose : function () {
											location.reload();
										}
									});
								}
							});
						});
					}else if(response.is_pending_link_invite){
						Notify.success(response.message, {
							afterClose : function () {
								localStorage.removeItem('group_id');
								localStorage.removeItem('referrer');
								if (response.group) {
									window.location = base_url + 'account/view/group/' + response.group;
								}
							}
						});
					} else {
						Notify.success(response.message, {
							afterClose : function () {
								localStorage.removeItem('group_id');
								localStorage.removeItem('referrer');
								if (response.group) {
									window.location = base_url + 'account/view/group/' + response.group;
								}
							}
						});
					}
				}
			});
		}
	}
};

$(document).ready(function(){
	Home.init();
});



function submitGiveAnswer(){
  		for($i = 0 ;  $i < $questionData.length ; $i++){
				$questionData[$i]['answer'] = $('#answerId'+$questionData[$i]['id']).val();
 			}

	$.ajax({
		url: base_url + 'account/SaveAnswerofGroup',
		method:'POST',
		dataType: 'json',
		data:{ group_id: localStorage.getItem('group_id') , answer : $questionData  },
		success:function(response) {

			if(response == 'success'){
				Notify.success('Answers Submitted, Your admission is pending approval', {
					afterClose : function () {
						$('#AnserModal').modal('hide');
						Home.checkLocalStorage();
						return false;
					}
				});

			}else{
				Notify.error('Answer Not Submmited, try Again later', {
					afterClose : function () {
					 	return false;
					}
				});
			}


		}
	});
	return false;


}
$questionData = [];

function checkprivacybox(){
	$.ajax({
		url: base_url + 'account/submitprivacyform/',
		method:'POST',
		dataType: 'json',
		data:{ },
		success:function(response) {

			location.reload();

		}
	});
	return false;
}
