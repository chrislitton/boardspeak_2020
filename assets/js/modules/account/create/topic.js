var CreateTopic = {
	initial_load_subcategory : 0,
    initComponents : function() {
        $('.custom-select').select2();

        if($("#inp_category_id").val() == "") {
			let val = $("#selCategory").find("option:contains('Uncategorized')").val();
	        $('#selCategory').val(val).trigger('change');
        }

        $('#txtKeywords').on('beforeItemAdd', function(event) {
            let keyword = event.item;

            $.ajax({
                type: 'POST',
                url: base_url + 'account/validate_keyword/',
                data : {keyword: keyword},
            }).done(function(data) {
                data = $.parseJSON(data);
                if (data) {
                    if (data.Kw_Banned == 1) {
                        $('#txtKeywords').tagsinput('remove', event.item);
                    }
                }
            });
        });
    },

    initLoadSubCategory: function(categoryID) {
		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_subcategory/',
			data : {CatID: categoryID},
			dataType: 'json'
		}).done(function(data) {
			$("#selSubCategory").empty();
			$("#selSubCategory").select2({placeholder: 'Choose a subcategory', data: data});

			if (CreateTopic.initial_load_subcategory == 0) {
				if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
					$('#selSubCategory').val($("#inp_subcategory_id").val());
				}
				CreateTopic.initial_load_subcategory = 1;
			} else {
				let val = $("#selSubCategory").find("option:contains('Uncategorized')").val();

				if (val !== undefined) {
					$('#selSubCategory').val(val)
				} else {
					$('#selTopic').prop('selectedIndex', 0);
				}
			}

			$('#selSubCategory').trigger('change');
		});
    },
    
    initEvents: function() {
		$count = 0;
        $('#selCategory').on('change', function (e) {


			if($count <= 0){
				if ( $('#selCategory').val() == 0) {
					$('#selNewCategoryBox').show();
					$('#txtNewCategory').prop("disabled", false);
					$('#txtNewCategory').prop("required", true);
				}
				else {

					$('#selNewCategoryBox').hide();
					$('#txtNewCategory').prop("disabled", true);
					$('#txtNewCategory').prop("required", false);
				}
				$count++;
				CreateTopic.initLoadSubCategory( $('#selCategory').val());
				return;
			}

			if($("#selCategory").val() == 75 || $("#selCategory").val() == 76){
				if (  $('#selCategory').val() == 0) {
					$('#selNewCategoryBox').show();
					$('#txtNewCategory').prop("disabled", false);
					$('#txtNewCategory').prop("required", true);
				}
				else {
					$('#selNewCategoryBox').hide();
					$('#txtNewCategory').prop("disabled", true);
					$('#txtNewCategory').prop("required", false);
				}
				$counter = 0;
				CreateTopic.initLoadSubCategory(  $('#selCategory').val());
				return;
			}

			$.ajax({
				type: 'POST',
				url: base_url + 'account/check_catogry_validation_topic/',
				data : {cat_id: $("#selCategory").val() , GroupID: $("#selGroup").val()},
			}).done(function(data) {
				if(data == 1){
					if (  $('#selCategory').val() == 0) {
						$('#selNewCategoryBox').show();
						$('#txtNewCategory').prop("disabled", false);
						$('#txtNewCategory').prop("required", true);
					}
					else {
						$('#selNewCategoryBox').hide();
						$('#txtNewCategory').prop("disabled", true);
						$('#txtNewCategory').prop("required", false);
					}

					CreateTopic.initLoadSubCategory(  $('#selCategory').val());
				}else{
					if($('#limitation').val() > 0 ){

						if (  $('#selCategory').val() == 0) {
							$('#selNewCategoryBox').show();
							$('#txtNewCategory').prop("disabled", false);
							$('#txtNewCategory').prop("required", true);
						}
						else {
							$('#selNewCategoryBox').hide();
							$('#txtNewCategory').prop("disabled", true);
							$('#txtNewCategory').prop("required", false);
						}

						CreateTopic.initLoadSubCategory(  $('#selCategory').val());

					}else{

						Swal.fire(
							'You have reached max limit',
							'Catogeries are limited according to the plan you chose.  You may replace your current Categories or Subcategories selections, if you wish.',
							'info'
						)

						let val = $("#selCategory").find("option:contains('Uncategorized')").val();
						$('#selCategory').val(val).trigger('change');


						CreateTopic.initLoadSubCategory(  $('#selCategory').val());						$count++;
						return ;

					}
				}

			});
			// if ($(this).val() == 0) {
			// 	$('#selNewCategoryBox').show();
			// 	$('#txtNewCategory').prop("disabled", false);
			// 	$('#txtNewCategory').prop("required", true);
			// }
			// else {
			//
			// 	$('#selNewCategoryBox').hide();
			// 	$('#txtNewCategory').prop("disabled", true);
			// 	$('#txtNewCategory').prop("required", false);
			// }

		    // CreateTopic.initLoadSubCategory($(this).val());
		});
        $('#selCategory').trigger('change');

        $('#selSubCategory').on('change', function (e) {		    
			if ($(this).val() == '0') {
				$('#selNewSubCategoryBox').show();
				$('#txtNewSubCategory').prop("disabled", false);
				$('#txtNewSubCategory').prop("required", true);
			}				
			else {
				$('#selNewSubCategoryBox').hide();
				$('#txtNewSubCategory').prop("disabled", true);
				$('#txtNewSubCategory').prop("required", false);
			}
		});
    },

    init: function() {
        CreateTopic.initComponents();
        CreateTopic.initEvents();
        CreateTopic.load_keywords();

        $('#txtKeywords').on('beforeItemAdd', function(event) {
            let keyword = event.item;

            $.ajax({
                type: 'POST',
                url: base_url + 'account/validate_keyword/',
                data : {keyword: keyword},
            }).done(function(data) {
                data = $.parseJSON(data);
                if (data) {
                    if (data.Kw_Banned == 1) {
                        $('#txtKeywords').tagsinput('remove', event.item);
                    }
                }
            });
        });
    },

	load_keywords : function() {

        var keywords_list = new Bloodhound({
            datumTokenizer: function(datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                wildcard: '%QUERY',
                url: base_url + "account/get_keywords_list/%QUERY",
                    transform: function(response) {
                        return $.map(response, function(words) {
                            return { value: words['name'] };
                        });
                    }
            }
        });
        keywords_list.initialize();

        $('#txtKeywords').tagsinput({
            trimValue: true,
            typeaheadjs: {
                name: 'keywords_list',
                displayKey: 'value',
                valueKey: 'value',
                source: keywords_list.ttAdapter()
            }
        });

	    $(".tt-input").bind("paste", function(e) {
			let keyword = e.originalEvent.clipboardData.getData('text');
	    	$(this).prop('size', keyword.length);
	    });
	}
};

$(document).ready(function(){
    CreateTopic.init();
});



$(document).ready(function(){





	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio:  1200/630,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$(".topic-cover-image").css("background", "transparent url('"+base64data+"') no-repeat center center /cover");
				$(".topic-cover-image").css("background-size", "100% 100%");
				$("#userfile").val(base64data);
				$modal.modal('hide');
			};
		});
	});
	
});
