var CreateGroup = {
    initComponents : function() {
        $('.custom-select').select2();

        $('#txtKeywords').on('beforeItemAdd', function(event) {
            let keyword = event.item;

            $.ajax({
                type: 'POST',
                url: base_url + 'account/validate_keyword/',
                data : {keyword: keyword},
            }).done(function(data) {
                data = $.parseJSON(data);
                if (data) {
                    if (data.Kw_Banned == 1) {
                        $('#txtKeywords').tagsinput('remove', event.item);
                    }
                }
            });
        });
    },

    initLoadSubCategory: function(categoryID) {

		$("#selSubCategory").empty();

		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_subcategory/',
			data : {CatID: categoryID},
			dataType: 'json'
		}).done(function(data) {
			$("#selSubCategory").select2({placeholder: 'Choose a subcategory', data: data});
			
			let val = $("#selSubCategory").find("option:contains('Uncategorized')").val();

			if (val !== undefined) {
				$('#selSubCategory').val(val)
			} else {
				$('#selTopic').prop('selectedIndex', 0);
			}

			$('#selSubCategory').trigger('change');
		});
    },
    
    initEvents: function() {

        $('#selCategory').on('change', function (e) {
			if ($(this).val() == 0) {
				$('#selNewCategoryBox').show();
				$('#txtNewCategory').prop("disabled", false);
				$('#txtNewCategory').prop("required", true);
			}				
			else {
				$('#selNewCategoryBox').hide();
				$('#txtNewCategory').prop("disabled", true);
				$('#txtNewCategory').prop("required", false);
			}

		    CreateGroup.initLoadSubCategory($(this).val());
		});

		// let val = $("#selCategory").find("option:contains('Uncategorized')").val();
  //       $('#selCategory').val(val).trigger('change');

        $('#selSubCategory').on('change', function (e) {

			if ($(this).val() =='0') {
				$('#selNewSubCategoryBox').show();
				$('#txtNewSubCategory').prop("disabled", false);
				$('#txtNewSubCategory').prop("required", true);
			}				
			else {
				$('#selNewSubCategoryBox').hide();
				$('#txtNewSubCategory').prop("disabled", true);
				$('#txtNewSubCategory').prop("required", false);
			}
		});
	
		if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
			$("#selSubCategory").val($("#inp_subcategory_id").val());
		}
    },

    init: function() {
        CreateGroup.initComponents();
        CreateGroup.initEvents();
        CreateGroup.load_keywords();
    },

	load_keywords : function() {

        var keywords_list = new Bloodhound({
            datumTokenizer: function(datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                wildcard: '%QUERY',
                url: base_url + "account/get_keywords_list/%QUERY",
                    transform: function(response) {
                        return $.map(response, function(words) {
                            return { value: words['name'] };
                        });
                    }
            }
        });
        keywords_list.initialize();

        $('#txtKeywords').tagsinput({
            trimValue: true,
            typeaheadjs: {
                name: 'keywords_list',
                displayKey: 'value',
                valueKey: 'value',
                source: keywords_list.ttAdapter()
            }
        });

	    $(".tt-input").bind("paste", function(e) {
			let keyword = e.originalEvent.clipboardData.getData('text');
	    	$(this).prop('size', keyword.length);
	    });
	}
};

$(document).ready(function() {
	CreateGroup.init();
});


$(document).ready(function(){

	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio:  1200/630,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$(".group-cover-image").css("background", "transparent url('"+base64data+"') no-repeat center center /cover");
				$(".group-cover-image").css("background-size", "100% 100%");
				$("#userfile").val(base64data);
				$modal.modal('hide');
			};
		});
	});
	
});