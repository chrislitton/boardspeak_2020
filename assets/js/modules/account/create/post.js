/*var CreatePost = {
    initComponents: function() {
        $('#selGroup').select2();
        $('#selTopic').select2();
        $('#selCategory').select2({multiple: true, allowClear: true, placeholder: "Select Category"});
        $('#selCategory').val(null);
        $('#selSubCategory').select2({multiple: true, allowClear: true, placeholder: "Select category first"});
		$('#selNewCategoryBox').hide();
		$('#txtNewCategory').prop("disabled", true);
		$('#txtNewCategory').prop("required", false);
    },

    initLoadSubCategory: function(categoryID) {

		$("#selSubCategory").empty();

		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_subcategory/',
			data : {CatID: categoryID},
			dataType: 'json'
		}).done(function(data) {
			$("#selSubCategory").select2({placeholder: 'Choose a subcategory', data: data});

			$('#selSubCategory').trigger('change');
		});
    },

    initEvents: function() {

		$('#selGroup').on('change', function(){
			CreatePost.initLoadTopic($(this).val());
		});

		$('#selTopic').change(function(){
			CreatePost.initLoadCategory();
		});

    	$('#selGroup').trigger('change');
    	$('#selTopic').val($("#To_ID").val());
    	$('#selTopic').trigger('change');

        $('#selCategory').on('change', function (e) {
			if ($(this).val() == 0) {
				$('#selNewCategoryBox').show();
				$('#txtNewCategory').prop("disabled", false);
				$('#txtNewCategory').prop("required", true);
			}				
			else {
				$('#selNewCategoryBox').hide();
				$('#txtNewCategory').prop("disabled", true);
				$('#txtNewCategory').prop("required", false);
			}

		    CreatePost.initLoadSubCategory($(this).val());
		});

		if ($("#inp_category_id").val() != "") {
			$('#selCategory').val($("#inp_category_id").val());
		}
        $('#selCategory').trigger('change');

        $('#selSubCategory').on('select2:select, change.select2', function (e) {
		    var data = e.params.data;
			if (data.id=='0') {
				$('#selNewSubCategoryBox').show();
				$('#txtNewSubCategory').prop("disabled", false);
				$('#txtNewSubCategory').prop("required", true);
			}				
			else {
				$('#selNewSubCategoryBox').hide();
				$('#txtNewSubCategory').prop("disabled", true);
				$('#txtNewSubCategory').prop("required", false);
			}

			if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
				$("#selSubCategory").val($("#inp_subcategory_id").val());
			}
		});

		if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
			$('#selSubCategory').val($("#inp_subcategory_id").val());
		}
    },

	initLoadTopic : function() {
		$("#selTopic").empty();

		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_topics/',
			data : {GroupID: $("#selGroup").val()},
			dataType: 'json'
		}).done(function(data) {
			$("#selTopic").select2({placeholder: 'Choose a topic', data: data});

			$('#selTopic').trigger('change');
		});
	},

	initLoadCategory : function() {
		let type = 'post'; 
		if ($('#selTopic').val() == 0) {
			type = 'topic';
		}

		$("#selCategory").empty();

		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_category/',
			data : {type: type},
			dataType: 'json'
		}).done(function(data) {
			$("#selCategory").select2({placeholder: 'Choose a category', data: data});

			$('#selCategory').trigger('change');
		});
	},

    init: function() {
        CreatePost.initComponents();
        CreatePost.initEvents();
    }
};

$(document).ready(function(){
    CreatePost.init();
});*/

var CreatePost = {
	initial_load_topic : "",
	initial_load_type : "post",
	initial_load_category : 0,
	initial_load_subcategory : 0,
	current_topic_selection : 0,
    init : function() {
        CreatePost.initComponents();
        CreatePost.load_keywords();
    },

    initComponents : function() {
        $('.custom-select').select2();
		if ($("#hid_none_topic").val() == 'group'  ) {
			CreatePost.initial_load_type = 'topic';
			$('#selTopic').val('0');
		}
		// else if($('#createType').val() == 'post'){
		// 	CreatePost.initial_load_type = 'post';
		// 	$('#selTopic').val('');
		// 	$('#selTopic').trigger('change');
		//
		// }

		$('#selActionButton').on('change', function(){
			$("#selNewActionButton").hide();
			$("#txtActionButton").prop('required', false);
			
			$('#txtLandingPage').prop('disabled', true);
			$('#txtLandingPage').prop('required', false);
			
			if ($(this).val().length > 0) {
				if ($(this).val() == '0') {
					$("#selNewActionButton").show();
					$("#txtActionButton").prop('required', true);
					$('#txtLandingPage').prop('disabled', false);
					$('#txtLandingPage').prop('required', true);
				} else {
					$('#txtLandingPage').prop('disabled', false);
					$('#txtLandingPage').prop('required', true);
				}
			}
		});

		$('#selGroup').on('change', function(){

			CreatePost.load_topic($(this).val());
		});
		$('#selGroup').trigger("change");

		$('#selTopic').on('change', function(e){

			if ($("#selTopic").val() == '0' || $("#selTopic").val() ==  '') {
				CreatePost.initial_load_type = 'post';
				// CreatePost.load_category();
			} else {
				CreatePost.initial_load_type = 'topic';
				CreatePost.load_category();
			}

		});
			$count = 0;
        $('#selCategory').on('change', function (e) {

			// alert("changing subcatogery" + $(this).val());
					if($count < 2){
						if ($(this).val() == 0) {

							$('#selNewCategoryBox').show();
							$('#txtNewCategory').prop("disabled", false);
							$('#txtNewCategory').prop("required", true);
						}
						else {
							$('#selNewCategoryBox').hide();
							$('#txtNewCategory').prop("disabled", true);
							$('#txtNewCategory').prop("required", false);
						}

							$count++;
						CreatePost.load_subcategory(  $('#selCategory').val());
						return;
					}

			if($("#selCategory").val() == 75 || $("#selCategory").val() == 76){
				if (  $('#selCategory').val() == 0) {
					$('#selNewCategoryBox').show();
					$('#txtNewCategory').prop("disabled", false);
					$('#txtNewCategory').prop("required", true);
				}
				else {
					$('#selNewCategoryBox').hide();
					$('#txtNewCategory').prop("disabled", true);
					$('#txtNewCategory').prop("required", false);
				}
				$counter = 0;
				CreatePost.load_subcategory(  $('#selCategory').val());
				return;
			}

			$.ajax({
				type: 'POST',
				url: base_url + 'account/check_catogry_validation/',
				data : {cat_id: $("#selCategory").val() , GroupID: $("#selGroup").val(),
				TopicId :$("#To_ID").val()
				},
			}).done(function(data) {
				if(data == 1){
					if (  $('#selCategory').val() == 0) {
						$('#selNewCategoryBox').show();
						$('#txtNewCategory').prop("disabled", false);
						$('#txtNewCategory').prop("required", true);
					}
					else {
						$('#selNewCategoryBox').hide();
						$('#txtNewCategory').prop("disabled", true);
						$('#txtNewCategory').prop("required", false);
					}

					CreatePost.load_subcategory(  $('#selCategory').val());

				}else{
					if($('#limitation').val() > 0 ){
						if (  $('#selCategory').val() == 0) {
							$('#selNewCategoryBox').show();
							$('#txtNewCategory').prop("disabled", false);
							$('#txtNewCategory').prop("required", true);
						}
						else {
							$('#selNewCategoryBox').hide();
							$('#txtNewCategory').prop("disabled", true);
							$('#txtNewCategory').prop("required", false);
						}
						CreatePost.load_subcategory(  $('#selCategory').val());
					}else{

						if($('#grouppaakcage').val() == 1){

							Swal.fire({
								title: 'You have reached max limit',
								text : 'Categories are limited according to the plan you chose. You may replace current Categories/Subcategories or upgrade your plan.',

								icon: 'info',
								showCancelButton: true,
								confirmButtonText:'Ok',
								confirmButtonColor:  '#3085d6',
								cancelButtonColor:  '#d33',
								cancelButtonText: 'Upgrade Plan'
							}).then((result) => {
								if (result.isConfirmed) {

								}else if(result.dismiss == 'cancel'){
 										$('#packagesmodel').modal('show');

								}
							})
						}else{
							Swal.fire(
								'You have reached max limit',
								'Categories are limited according to the plan you chose. You may replace current Categories/Subcategories or upgrade your plan.',
								'info'
							)
						}

						let val = $("#selCategory").find("option:contains('Uncategorized')").val();
						$('#selCategory').val(val).trigger('change');
						CreatePost.load_subcategory(  $('#selCategory').val());
						$count++;
						return ;
					}
				}
			});



		});
		$counter = 0;
        $('#selSubCategory').on('change', function (e) {

				if($counter < 0) {
					if (  $('#selSubCategory').val() == 0) {
						$('#selNewSubCategoryBox').show();
						$('#txtNewSubCategory').prop("disabled", false);
						$('#txtNewSubCategory').prop("required", true);
					} else {
						$('#selNewSubCategoryBox').hide();
						$('#txtNewSubCategory').prop("disabled", true);
						$('#txtNewSubCategory').prop("required", false);
					}
					$counter++;
					return;
				}

				if(  $('#selSubCategory').val() == 221 ||   $('#selSubCategory').val() == 222) {

					if (  $('#selSubCategory').val() == 0) {
						$('#selNewSubCategoryBox').show();
						$('#txtNewSubCategory').prop("disabled", false);
						$('#txtNewSubCategory').prop("required", true);
					} else {
						$('#selNewSubCategoryBox').hide();
						$('#txtNewSubCategory').prop("disabled", true);
						$('#txtNewSubCategory').prop("required", false);
					}

					return;
				}

			$.ajax({
				type: 'POST',
				url: base_url + 'account/check_subcatogry_validation/',
				data : {sub_cat_id: $("#selSubCategory").val() , GroupID: $("#selGroup").val(),	TopicId :$("#To_ID").val()},
			}).done(function(data) {
				if(data == 1){
					if (  $('#selSubCategory').val() == 0) {
						$('#selNewSubCategoryBox').show();
						$('#txtNewSubCategory').prop("disabled", false);
						$('#txtNewSubCategory').prop("required", true);
					} else {
						$('#selNewSubCategoryBox').hide();
						$('#txtNewSubCategory').prop("disabled", true);
						$('#txtNewSubCategory').prop("required", false);
					}

				}else{
					if($('#limitation').val() > 0 ){

						if (  $('#selSubCategory').val() == 0) {
							$('#selNewSubCategoryBox').show();
							$('#txtNewSubCategory').prop("disabled", false);
							$('#txtNewSubCategory').prop("required", true);
						} else {
							$('#selNewSubCategoryBox').hide();
							$('#txtNewSubCategory').prop("disabled", true);
							$('#txtNewSubCategory').prop("required", false);
						}


					}else{

						if($('#grouppaakcage').val() == 1){

							Swal.fire({
								title: 'You have reached max limit',
								text : 'Categories are limited according to the plan you chose. You may replace current Categories/Subcategories or upgrade your plan.',

								icon: 'info',
								showCancelButton: true,
								confirmButtonText:'Ok',
								confirmButtonColor:  '#3085d6',
								cancelButtonColor:  '#d33',
								cancelButtonText: 'Upgrade Plan'
							}).then((result) => {
								if (result.isConfirmed) {

								}else if(result.dismiss == 'cancel'){
									$('#packagesmodel').modal('show');

								}
							})
						}else{
							Swal.fire(
								'You have reached max limit',
								'Categories are limited according to the plan you chose. You may replace current Categories/Subcategories or upgrade your plan.',
								'info'
							)
						}

						try{
							let val = $("#selSubCategory").find("option:contains('Uncategorized')").val();

							if(val == 221 || val == 222){
								$('#selSubCategory').val(val).trigger('change');

							}

						}catch (e) {
							
						}



						$counter++;
						return ;

					}
				}

			});

		});

        $('#txtKeywords').on('beforeItemAdd', function(event) {
            let keyword = event.item;

            $.ajax({
                type: 'POST',
                url: base_url + 'account/validate_keyword/',
                data : {keyword: keyword},
            }).done(function(data) {
                data = $.parseJSON(data);
                if (data) {
                    if (data.Kw_Banned == 1) {
                        $('#txtKeywords').tagsinput('remove', event.item);
                    }
                }
            });
        });
	},

	load_topic : function() {

		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_topics/',
			data : {GroupID: $("#selGroup").val()},
			dataType: 'json'
		}).done(function(data) {
				console.log("topic" , data );
				console.log($("#To_ID").val());
				console.log(CreatePost.initial_load_topic);
			$("#selTopic").empty();
			$("#selTopic").select2({placeholder: 'Choose subgroup or create new subgroup', data: data});

			if (CreatePost.initial_load_topic == "") {

				$("#selTopic").val($("#To_ID").val());
				CreatePost.initial_load_topic = 1;
			}

			$('#selTopic').trigger('change');


			// if (CreatePost.initial_load_type == 'topic') {
			// 	$('#selTopic').val('0').trigger('change');
			// }
		});
	},

	load_category : function() {

		var type = CreatePost.initial_load_type;

				// alert($("#selTopic").val() );
 		  if ( $("#selTopic").val() >  0 ) {
			  type = 'post'
 			}else{
 			  type = 'post';
			  // $("#selTopic").empty();
			  $('#selTopic').val('0').trigger('change');
		  }

		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_category/',
			data : {type: type},
			dataType: 'json'
		}).done(function(data) {
			if($("#inp_subcategory_id").val()){
				return;
			}
			$("#selCategory").empty();
			$("#selCategory").select2({placeholder: 'Choose a category', data: data});

			if ($("#inp_category_id").val() != "" && CreatePost.initial_load_category == 0) {

				$("#selCategory").val($("#inp_category_id").val());
				CreatePost.initial_load_category = 1;
			}
			else {
				let val = $("#selCategory").find("option:contains('Uncategorized')").val();
				$('#selCategory').val(val).trigger('change');
			}

			$('#selCategory').trigger('change');
			CreatePost.current_topic_selection = type;

		});
	},

	load_subcategory : function(categoryID) {

		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_subcategory/',
			data : {CatID: categoryID , limitaion : $('#limitation').val()},
			dataType: 'json'
		}).done(function(data) {


			$("#selSubCategory").empty();
			$("#selSubCategory").select2({placeholder: 'Choose a subcategory', data: data});

			if (CreatePost.initial_load_subcategory == 0) {

				if ($("#inp_subcategory_id").val() !== undefined) {
					if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
						$('#selSubCategory').val($("#inp_subcategory_id").val());
					}
				} else {

					let val = $("#selSubCategory").find("option:contains('Uncategorized')").val();

					if (val !== undefined) {
						$('#selSubCategory').val(val)
					} else {
						// $('#selTopic').prop('selectedIndex', 0);
					}
				}
				CreatePost.initial_load_subcategory = 1;
			} else {
				let val = $("#selSubCategory").find("option:contains('Uncategorized')").val();

				if (val !== undefined) {
					$('#selSubCategory').val(val)
				} else {
					// $('#selTopic').prop('selectedIndex', 0);
				}
			}
			$('#selSubCategory').trigger('change');
		});
	},

	load_keywords : function() {

        var keywords_list = new Bloodhound({
            datumTokenizer: function(datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                wildcard: '%QUERY',
                url: base_url + "account/get_keywords_list/%QUERY",
                    transform: function(response) {
                        return $.map(response, function(words) {
                            return { value: words['name'] };
                        });
                    }
            }
        });
        keywords_list.initialize();

        $('#txtKeywords').tagsinput({
            trimValue: true,
            typeaheadjs: {
                name: 'keywords_list',
                displayKey: 'value',
                valueKey: 'value',
                source: keywords_list.ttAdapter()
            }
        });

	    $(".tt-input").bind("paste", function(e) {
			let keyword = e.originalEvent.clipboardData.getData('text');
	    	$(this).prop('size', keyword.length);
	    });
	}
};

$(document).ready(function(){
    CreatePost.init();
});


$(document).ready(function(){


	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			// aspectRatio:  820/440,
			aspectRatio:  1200/630,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$(".post-cover-image").css("background", "transparent url('"+base64data+"') no-repeat center center /cover");
				$(".post-cover-image").css("background-size", "100% 100%");
				$("#userfile").val(base64data);
				$modal.modal('hide');
			};
		});
	});
	
});
