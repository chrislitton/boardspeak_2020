/* script for notification tabs */
$(document).ready(function(){

	$("#not_summary").show();
	$("#not_setting").hide();


  	$("#summary_tab_btn").click(function(){
  		
	  	$("#summary_tab_btn").addClass("active");
	  	$("#setting_tab_btn").removeClass("active");

	    $("#not_summary").show();
	    $("#not_setting").hide();
  	});
  
  	$("#setting_tab_btn").click(function(){
	  	$("#summary_tab_btn").removeClass("active");
	  	$("#setting_tab_btn").addClass("active");

	    $("#not_summary").hide();
	    $("#not_setting").show();
	});

});

/* notification filter tab slider */
$(document).ready(function() {
	var owl = $('.not_filter');
	owl.owlCarousel({
		autoWidth:true,
		margin: 0,
		nav: false,
		dots: false,
		autoplayTimeout: 2000,
		autoplayHoverPause: true

	});
})


/* notification switch script */
$(document).ready(function(){
    $(".not_setting_status").click(function(){
      $(this).toggleClass('on');
    });
});

$(document).on('click', '.group_action', function() {
	var groupId = $(this).data('group')
	var user_id = $(this).data('user');
	var notification_ID = $(this).data('notification');
	var action = $(this).data('action');
	var name =   $(this).data('name');
	var postinvite = $(this).data('postinvite');
	var notype = $(this).data('notype');
	let data= {};

	if(action == 'approvecreator' || action == 'rejectcreator'){
		var	username    =   $(this).data('username');
		data =	{
			groupid: user_id,
			notification_ID: notification_ID,
			userid: groupId,
			action: action
 					}
		$text = '';
		if(action == 'approvecreator'){
			$text ='accept';
		}else{
			$text = 'reject';
		}
			let encoid =  $(this).data('slug'); ;
			$link = "<a href='"+base_url+"account/view/group/"+encoid+"'>"+name+"</a>";

		Notify.confirm('Continue to '+$text+' request from '+username+' to be the new creator of '+$link+' ?', function () {

			$.ajax({
				url: base_url + 'group/approve_reject_join_request_from_notification_creator/',
				method: 'POST',
				dataType: 'json',
				data: data,
				success: function (response) {

					console.log(response);
					Notify.success(response.message, {
						afterClose : function () {
							location.reload();
						}
					});
				}
			});

		}, function (response) {

		});
	}
	else{


		if(notype == 'group_invite'){
			let carrier = user_id;
			user_id = groupId;
			groupId = carrier;
		}


			$nametype = '';
		if(postinvite){

			data =	{
				user_id: user_id,
				notification_ID: notification_ID,
				type_id: groupId,
				action: action,
				postinvite : postinvite,
				notype : notype
			}

			$nametype = 'post';
			if(notype == 'topic_join_via_link'){
				$nametype = 'topic';
			}

		}
		else{
			$nametype = 'group';
			data =	{
				user_id: user_id,
				notification_ID: notification_ID,
				type_id: groupId,
				action: action

			}
		}

		if($nametype == 'group'){

			if(notype == 'request_join'  || notype == 'group_join_via_link'){
				groupdate = {

					groupiss :  $(this).data('group')
				}
			}else{
				groupdate = {

					groupiss :  user_id
				}
			}



			$.ajax({
				url: base_url + 'group/getgroupname/',
				method: 'POST',
				dataType: 'json',
				data:groupdate,
				success: function (response) {


					let encoid =  response[0]['Gr_Slug'];
					$link = "<a href='"+base_url+"account/view/group/"+encoid+"'>"+response[0]['Gr_Name']+"</a>";

					Notify.confirm('Continue to '+action+' invite request by '+ name +' to join '+$link+'', function () {
						$.ajax({
							url: base_url + 'group/approve_reject_join_request_from_notification/',
							method: 'POST',
							dataType: 'json',
							data: data,
							success: function (response) {

								console.log(response);
								Notify.success(response.message, {
									afterClose : function () {
										location.reload();
										// $('#tab_' + from).trigger('click');
									}
								});
							}
						});
					}, function (response) {
						console.log(response);
					});
				}
			});
		}
		else if($nametype == 'post'){
			groupdate = {

				groupiss :  postinvite
			}

			$.ajax({
				url: base_url + 'group/getpostname/',
				method: 'POST',
				dataType: 'json',
				data:groupdate,
				success: function (response) {
					console.log(response);
					// console.log(response[0]['Po_Title']);

					let encoid =  response[0]['Po_Slug'];
					$link = "<a href='"+base_url+"account/view/post/"+encoid+"'>"+response[0]['Po_Title']+"</a>";

					Notify.confirm('Continue to '+action+' invite request by '+ response[0]['Us_Name'] +' to join '+$link+'', function () {
						$.ajax({
							url: base_url + 'group/approve_reject_join_request_from_notification/',
							method: 'POST',
							dataType: 'json',
							data: data,
							success: function (response) {

								console.log(response);
								Notify.success(response.message, {
									afterClose : function () {
										location.reload();

									}
								});
							}
						});
					}, function (response) {
						console.log(response);
					});
				}
			});
		}
		else if($nametype == 'topic'){
			groupdate = {

				groupiss :  postinvite
			}

			$.ajax({
				url: base_url + 'group/gettopicname/',
				method: 'POST',
				dataType: 'json',
				data:groupdate,
				success: function (response) {
					console.log(response);
					// console.log(response[0]['Po_Title']);

					let encoid =  response[0]['SLUG'];
					$link = "<a href='"+base_url+"account/view/topic/"+encoid+"'>"+response[0]['To_Name']+"</a>";

					Notify.confirm('Continue to '+action+' invite request by '+ response[0]['Us_Name'] +' to join '+$link+'', function () {
						$.ajax({
							url: base_url + 'group/approve_reject_join_request_from_notification/',
							method: 'POST',
							dataType: 'json',
							data: data,
							success: function (response) {

								console.log(response);
								Notify.success(response.message, {
									afterClose : function () {
										location.reload();

									}
								});
							}
						});
					}, function (response) {
						console.log(response);
					});
				}
			});
		}



	}



});

function clickactivity(){


	$('#acctivitycounter').text('(0)');
	$.ajax({
		url: base_url + 'account/hideactivtiyNotify/',
		method: 'POST',
		dataType: 'json',
		data: {},
		success: function (response) {

		 console.log(response);
		}
	});
}
/* customs script for filtering NOTIFICATION tabs */
$(document).ready(function(){
  $(".not_filter_btn").click(function(){
    var tab_id = $(this).attr('selected_tab_id');
    
    $(".not_filter_btn").removeClass("active");
    $(this).addClass("active");
    
    if (tab_id == "all_not") {
      
      $(".not_tab").show();
    }else{

      $(".not_tab").hide();
      $("." + tab_id).show();
    }
  });
});
