
var start = /@/ig; // @ Match
var word = /@(\w+)/ig; //@abc Match
var ViewPost = {
	initEvents : function() {
        
        $(".group_taggline").collapser({
            mode:'lines',
            truncate: 3,
            showText: '[Show More]',
            hideText: '[Show Less]',
            changeText: true,
        });

		$('.img_slider_container').owlCarousel({
			margin: 25,
			nav: true,
			dots: false,
			navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
			responsiveClass: true,
			responsive: {
				0: { items: 1 },
				768: { items: 3 },
				991: { items: 5}
			}
		});

		$("#btn_submit_comment").click(function(e) {
			e.preventDefault();

			let formData = new FormData();

			formData = $("form").serializeArray();



			 $("#btn_submit_comment").attr({ disabled:true, value:"Sending..." });

			$.ajax({
				url: base_url + 'account/savecomment/',
				method: 'POST',
				data: formData,
				success: function (response) {
					response = $.parseJSON(response);

						if(response['uploadornot'] == 0){
							Swal.fire({
								icon: 'info',
								title: 'Memory limit exceed',
								text: 'Uploading memory limit Exceed',

							})

						}

					let comment_box = $('.comment-row-clone').clone().removeClass('comment-row-clone').show();
					comment_box.find('.comment-date-posted').append(response['Co_DatePosted']);
					comment_box.find('.comment-user-name').append(response['Us_Name']);
					comment_box.find('.comment-message').append(response['Co_Message']);
					comment_box.find('.admin-photo').css("background", "transparent url('"+ response['Us_Photo'] +"') no-repeat center center /cover");

					$(".msgbox-board").append(comment_box);

					$("#txtMessage").val('');
					$("#btn_submit_comment").attr({ disabled:false, value:"Submit" });
				}
			});
		});

		$("#btn_comment_search").click(function(e) {
			e.preventDefault();

			ViewPost.initComments();
		});

		$("#btn_comment_search_reset").click(function(e) {
			e.preventDefault();
			$("#txtSearch").val('');
			ViewPost.initComments();
		});
		
		$(".secondery_btn").click(function(){
			// button class slided toggle
			$(this).toggleClass("slideOut");
			// parent class add
			$(this).parent().toggleClass("slided");
			// secondery menu toggle class
			$(this).next(".secondery_dropdown").toggleClass("slided");
		});

		// hide on mouse leave
		$(".secondery_item").mouseleave(function(){
			$(this).removeClass("slided");
			$(".secondery_btn").removeClass("slided");
			$(".secondery_dropdown").removeClass("slided");
		});

		$('#btn_follow_without_invite').click(function (){
			var data = {"groupid" : $('#groupID').val()  };



			var data = {"post_id" : $("#Post_ID").val(),  "post_role" : 'follower'  , "join_type" : 'group_direct_join'};
			Notify.confirm('Continue following this post?', function () {
				$.ajax({
					type: "POST",
					url: base_url + "account/checkuseralreadymemberforPOST/",
					data: data,
					dataType: 'json',
					success: function(response){

						if(response.status == 'success'){
							if(response.type == 'post'){
								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/post/' + response.id;
									}
								});
							}else if(response.type == 'group'){

								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/group/' + response.id;
									}
								});
							}

						}else {

							Notify.success(response.message, {

							});

						}

					}
				});
			}, function () {});
		})
		$('#btn_join_without_invite').click(function (){
			var data = {"groupid" : $('#groupID').val()  };

			$.ajax({
				type: "POST",
				url: base_url + "account/checkquestionofgroup/",
				data: data,
				dataType: 'json',
				success: function(response){

					if(response == 'notallowed'){
						$.ajax({
							url: base_url + 'account/getSaveQuestion',
							method:'POST',
							dataType: 'json',
							data:{ groupid: $('#groupIDx').val() },
							success:function(response) {

								if(response[0]['group_Question'] != '' || response[0]['group_Question'] != null ){

									$questionDatafooter = 	JSON.parse(response[0]['group_Question']);

									if($questionDatafooter != null && $questionDatafooter.length > 0 && $questionDatafooter != ''){
										$('#AnserModalfooter').modal('show');
										$appendDiv = '';
										//
										$('#GiveAnswerDivfooter').empty();
										for(var x = 0 ; x < $questionDatafooter.length ; x++){
											$appendDiv += '<div>'+

												'<div class="form-group col-sm-12">'+
												'<label>'+$questionDatafooter[x]['value']+':<span class="required ">*</span></label>'+
												'<div class="col-12  ">'+
												'<input type="text"  required   class="form-control form-control-solid "   id="answerId'+$questionDatafooter[x]['id']+'" placeholder="Enter Answer">'+
												'</div>'+
												'</div>'+
												'</div>';

										}
										$('#GiveAnswerDivfooter').append($appendDiv);

										$('#roleType').val('memberdirectjoin')
										$("#footeranswermodel").attr("onsubmit", "return testing()");

									}else{

									}

								}else{



								}

								return;

							}
						});
					}else if(response == 'allowed'){
						var data = {"post_id" : $("#Post_ID").val(),  "post_role" : 'member' , "join_type" : 'group_direct_join'};

						Notify.confirm('Continue join this post?', function () {
							$.ajax({
								type: "POST",
								url: base_url + "account/checkuseralreadymemberforPOST/",
								data: data,
								dataType: 'json',
								success: function(response){

									if(response.status == 'success'){
										if(response.type == 'post'){
											Notify.success(response.message, {
												afterClose : function () {

													window.location = base_url + 'account/view/post/' + response.id;
												}
											});
										}else if(response.type == 'group'){

											Notify.success(response.message, {
												afterClose : function () {

													window.location = base_url + 'account/view/group/' + response.id;
												}
											});
										}

									}else {

										Notify.success(response.message, {

										});

									}

								}
							});
						}, function () {});			//

					}

				}
			});



		})
		$('#btn_join').click(function () {

			var data = {"groupid" : $('#groupID').val()  };

			$.ajax({
				type: "POST",
				url: base_url + "account/checkquestionofgroup/",
				data: data,
				dataType: 'json',
				success: function(response){

				if(response == 'notallowed'){
					$.ajax({
						url: base_url + 'account/getSaveQuestion',
						method:'POST',
						dataType: 'json',
						data:{ groupid: $('#groupIDx').val() },
						success:function(response) {
							console.log(response);

							console.log(response[0]['group_Question']);
							if(response[0]['group_Question'] != '' || response[0]['group_Question'] != null ){

								$questionDatafooter = 	JSON.parse(response[0]['group_Question']);

								if($questionDatafooter != null && $questionDatafooter.length > 0 && $questionDatafooter != ''){
									$('#AnserModalfooter').modal('show');
									$appendDiv = '';
									//
									$('#GiveAnswerDivfooter').empty();
									for(var x = 0 ; x < $questionDatafooter.length ; x++){
										$appendDiv += '<div>'+

											'<div class="form-group col-sm-12">'+
											'<label>'+$questionDatafooter[x]['value']+':<span class="required ">*</span></label>'+
											'<div class="col-12  ">'+
											'<input type="text"  required   class="form-control form-control-solid "   id="answerId'+$questionDatafooter[x]['id']+'" placeholder="Enter Answer">'+
											'</div>'+
											'</div>'+
											'</div>';

									}
									$('#GiveAnswerDivfooter').append($appendDiv);

									$('#roleType').val('memberinvitejoin');
									$("#footeranswermodel").attr("onsubmit", "return testing()");

								}else{

								}

							}else{



							}

							return;

						}
					});
				}else if(response == 'allowed'){
					var data = {"post_id" : $("#Post_ID").val(),
					"post_referrer" : $("#post_refererer").val() ,
					"post_role" : $("#post_role").val()};
					 Notify.confirm('Continue join this post?', function () {
					$.ajax({
						type: "POST",
						url: base_url + "account/checkuseralreadymemberforPOST/",
						data: data,
						dataType: 'json',
						success: function(response){

							if(response.status == 'success'){
								if(response.type == 'post'){
									Notify.success(response.message, {
										afterClose : function () {

											window.location = base_url + 'account/view/post/' + response.id;
										}
									});
								}else if(response.type == 'group'){

									Notify.success(response.message, {
										afterClose : function () {

											window.location = base_url + 'account/view/group/' + response.id;
										}
									});
								}

							}else {

								Notify.success(response.message, {

								});

							}

						}
					});
				}, function () {});

				}

				}
			});
 			});

		$('#btn_follow').click(function () {

			var data = {"post_id" : $("#Post_ID").val(), "post_referrer" : $("#post_refererer").val() , "post_role" : $("#post_role").val()};
			Notify.confirm('Continue following this post?', function () {
				$.ajax({
					type: "POST",
					url: base_url + "account/checkuseralreadymemberforPOST/",
					data: data, 
					dataType: 'json',
					success: function(response){

						if(response.status == 'success'){
							if(response.type == 'post'){
								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/post/' + response.id;
									}
								});
							}else if(response.type == 'group'){

								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/group/' + response.id;
									}
								});
							}

						}else {

							Notify.success(response.message, {

							});

						}
						
					}
				});
			}, function () {});
		});

		$('#external_link_modal').on('shown.bs.modal',function(){      //correct here use 'shown.bs.modal' event which comes in bootstrap3
			$(this).find('iframe').attr('src','http://www.google.com')
		});
	},

	initComponents : function() {

	},

    init: function() {
        ViewPost.initComponents();
        ViewPost.initEvents();
        ViewPost.initComments();
    },

    initComments: function() {

		var saveComment = function(data) {


				console.log(data);

			// Convert pings to human readable format
			$(Object.keys(data.pings)).each(function(index, userId) {
			    var fullname = data.pings[userId];
			    var pingText = '@' + fullname;
			    data.content = data.content.replace(new RegExp('@' + userId, 'g'), pingText);
			});

			return data;
		}
		let noCommentsText = 'No comments';
		if ($("#txtSearch").val() != '') {
			noCommentsText = 'No results found';
		}



		$('#comments-container').comments({
			profilePictureURL: profile_picture,
			currentUserId: 1,
			roundProfilePictures: true,
			textareaRows: 2,
			enableAttachments: true,
			enableHashtags: true,
			enablePinging: true,
			noCommentsText: noCommentsText,
			searchUsers: function(term, success, error) {

				$.ajax({
					type: 'post',

					data: {keywords: term , groupId : $('#GroupUdId').val()},
					url: base_url + 'post/get_contacts',
					success: function(userArray) {

						userArray = $.parseJSON(userArray);
						let totalrecord = userArray.total
						userArray = userArray.data



						if(totalrecord == 0){
							if($('#post_type').val() == 'private'){

								Swal.fire({
									title: 'info',
									text: "You have to invite people in your private post "+$('.breadcrumb-item active').text()+" in order to tag or mention them in your comment.",
									icon: 'info',
									showCancelButton: true,
									confirmButtonText:'Invite',
									cancelButtonColor:  '#3085d6',
									confirmButtonColor:  '#d33',
									cancelButtonText: 'Close'
								}).then((result) => {
									if (result.isConfirmed) {
										// location.replace(base_url+'account/view/group/'+$('#Groupencode').val());
										$('.invite_member_btn').click();


									}else{

									}
								})
							}else
							if($('#post_type').val() == 'public'){

								Swal.fire({
									title: 'info',
									text: "You have to invite people in your group in order to tag or mention them in your comment",
									icon: 'info',
									showCancelButton: true,
									confirmButtonText:'invite',
									cancelButtonColor:  '#3085d6',
									confirmButtonColor:  '#d33',
									cancelButtonText: 'Close'
								}).then((result) => {
									if (result.isConfirmed) {
										if($('#post_type').val() == 'public'){
											location.replace(base_url+'account/view/group/'+$('#Groupencode').val());
										}
									}else{

									}
								})

							}
							return;
						}
						if(userArray.length == 0){

								// if($('#post_type').val() == 'private'){
								//
								// 	Swal.fire({
								// 		title: 'info',
								// 		text: "You have to invite people in your private post "+$('.breadcrumb-item active').text()+" in order to tag or mention them in your comment.",
								// 		icon: 'info',
								// 		showCancelButton: true,
								// 		confirmButtonText:'Invite',
								// 		cancelButtonColor:  '#3085d6',
								// 		confirmButtonColor:  '#d33',
								// 		cancelButtonText: 'Close'
								// 	}).then((result) => {
								// 		if (result.isConfirmed) {
								// 			// location.replace(base_url+'account/view/group/'+$('#Groupencode').val());
 								// 				$('.invite_member_btn').click();
								//
								//
								// 		}else{
								//
								// 		}
								// 	})
								// }else
								// 	if($('#post_type').val() == 'public'){
								//
								// 	Swal.fire({
								// 		title: 'info',
								// 		text: "You have to invite people in your group in order to tag or mention them in your comment",
								// 		icon: 'info',
								// 		showCancelButton: true,
								// 		confirmButtonText:'invite',
								// 		cancelButtonColor:  '#3085d6',
								// 		confirmButtonColor:  '#d33',
								// 		cancelButtonText: 'Close'
								// 	}).then((result) => {
								// 		if (result.isConfirmed) {
								// 			if($('#post_type').val() == 'public'){
								// 				location.replace(base_url+'account/view/group/'+$('#Groupencode').val());
								// 			}
  								// 		}else{
								//
								// 		}
								// 	})
								//
								// }

						}else{
							success(userArray);
 						}

					},
					error: error
				});
			},
			getComments: function(success, error) {
				$.ajax({
					type: 'post',
					data: {PostID: $("input[name='PostID']").val(),search_txt: $("#txtSearch").val()},
					url: base_url + 'post/get_comments',
					success: function(commentsArray) {
						commentsArray = $.parseJSON(commentsArray);
						console.log(commentsArray);

						success(commentsArray)
					},
					error: error
				});
			},
			postComment: function(commentJSON, success, error) {

				// Create form data and append all other fields but attachments
				var formData = new FormData();
				$(Object.keys(commentJSON)).each(function(index, key) {
					if(key != 'attachments' && key != 'pings') {
						var value = commentJSON[key];
						if(value) formData.append(key, value);
					}
					else if (key == 'pings') {
						$.each(commentJSON.pings, function(idx, val) {
							formData.append(`tags[${idx}]`, val);
						});
					}
				});

				// Append attachments to be created to the form data
				var attachmentsToBeCreated = commentJSON.attachments.filter(function(attachment){
					return !attachment.id
				});
				$(attachmentsToBeCreated).each(function(index, attachment) {
					formData.append('attachment[]', attachment.file);
				});

				var postID = $("input[name=PostID]").val();

				commentJSON = saveComment(commentJSON);


				let tagpersonid , tagpersonname;
				if($('#textareaforComment').children().length > 0){

					if($('#textareaforComment').children()[0].getAttribute('data-userid') != null || $('#textareaforComment').children()[0].getAttribute('data-userid') != ''){
						tagpersonid = $('#textareaforComment').children()[0].getAttribute('data-user-id');
						tagpersonname = $('#textareaforComment').children()[0].getAttribute('data-user-name');
						formData.append('taguserid', tagpersonid);
						formData.append('tagusername', tagpersonname);
						console.log(tagpersonname);
					}

				}


				formData.append('postID', postID);

				$.ajax({
					type: 'post',
					url: base_url + 'post/savecomment',
					data: formData,
					contentType: false,
					processData: false,
					success: function(comment) {
						comment = $.parseJSON(comment);
						if(comment['uploadornot'] == 0){
							Swal.fire({
								icon: 'info',
								title: 'Memory limit exceeded',
								text: 'Uploading memory limit Exceed',

							})

						}
						success(comment)
					},
					error: error
				});
			},
			putComment: function(commentJSON, success, error) {

				// Create form data and append all other fields but attachments
				var formData = new FormData();
				$(Object.keys(commentJSON)).each(function(index, key) {
					if(key != 'attachments' && key != 'pings') {
						var value = commentJSON[key];
						if(value) formData.append(key, value);
					}
					else if (key == 'pings') {
						$.each(commentJSON.pings, function(idx, val) {
							formData.append(`tags[${idx}]`, val);
						});
					}
					else {
						var attachments = commentJSON[key];

						$.each(attachments, function( index, value ) {
							if(value.id !== undefined) formData.append('existing_attachment[]', value.id);
						});
					}
				});

				// Append attachments to be created to the form data
				var attachmentsToBeCreated = commentJSON.attachments.filter(function(attachment){
					return !attachment.id
				});
				$(attachmentsToBeCreated).each(function(index, attachment) {		
					formData.append('attachment[]', attachment.file);
				});

				$.ajax({
					type: 'post',
					url: base_url + 'post/updatecomment',
					data: formData,
					contentType: false,
					processData: false,
					success: function(comment) {
						comment = $.parseJSON(comment);
						success(comment)
					},
					error: error
				});
			},
			deleteComment: function(commentJSON, success, error) {
				$.ajax({
					type: 'post',
					url:  base_url + 'post/deletecomment',
					data: commentJSON,
					success: success,
					error: error
				});
			},
			upvoteComment: function(commentJSON, success, error) {
				var commentURL = base_url + 'post/comment_upvote';

				$.ajax({
					type: 'post',
					url: commentURL,
					data: {
						comment_id: commentJSON.id,
						upvote: commentJSON.user_has_upvoted
					},
					success: function() {
						success(commentJSON)
					},
					error: error
				});
			},
			pinComment: function(commentJSON, success, error) {
				var commentURL = base_url + 'post/comment_pin';

				$.ajax({
					type: 'post',
					url: commentURL,
					data: {
						comment_id: commentJSON.id,
						is_pinned: commentJSON.is_pinned
					},
					success: function() {
						success(commentJSON)
					},
					error: error
				});
			},
			validateAttachments: function(attachments, callback) {
				setTimeout(function() {
					callback(attachments);
				}, 500);
			},
			timeFormatter: function(time) {
				return moment(time).fromNow();
			}
		});
		
		$(".more_options_button").click(function(){
			// button class slided toggle
			$(this).toggleClass("slided");
			// parent class add
			$(this).parent().toggleClass("slided");
			// secondery menu toggle class
			$(this).next(".more-action-dropdown").toggleClass("slided");
		});

		// hide on mouse leave
		$(".more-options").mouseleave(function(){
			$(this).removeClass("slided");
			$(".more_options_button").removeClass("slided");
			$(".more-action-dropdown").removeClass("slided");
		});
    }
};


			function addtodocommentReminder(CommentID){

				$.ajax({
					url: base_url + 'account/getPingComment/',
					method: 'POST',
					dataType: 'json',
					data: {
						commentId : CommentID,
						postId	: $("#Po_ID").val()
					},
					success: function (response) {

						if(response.length != 0){

							for(var x = 0 ;  x  < response.length; x++){

								$.ajax({
									url: base_url + 'account/addNotificationreminder/',
									method: 'POST',
									dataType: 'json',
									data: {
										userID : response[x].Ta_Us_ID,
										CommentId : response[x].Ta_Co_ID,
									},
									success: function (response) {

									}
								});

							}
							Swal.fire({
								icon: 'success',
								title: 'success',
								text: 'Reminder is assigned To the Tagged person',

							})

						}else{
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'No one is tagged @NAME in this comment',

							})
						}
					}
				});




			}
			function addtodocommentAssigned(CommentID){

				$.ajax({
					url: base_url + 'account/getPingComment/',
					method: 'POST',
					dataType: 'json',
					data: {
						commentId : CommentID,
						postId	: $("#Po_ID").val()
					},
					success: function (response) {

						if(response.length != 0){

							for(var x = 0 ;  x  < response.length; x++){

								$.ajax({
									url: base_url + 'account/addNotificationfotTag/',
									method: 'POST',
									dataType: 'json',
									data: {
										userID : response[x].Ta_Us_ID,
										CommentId : response[x].Ta_Co_ID,
									},
									success: function (response) {

									}
								});

							}
							Swal.fire({
								icon: 'success',
								title: 'success',
								text: 'Task is assigned To the Tagged person',

							})

						}else{
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'No one is tagged @NAME in this comment',

							})
						}
					}
				});

			}
		function addtodocomment(item){


			Notify.confirm('Are you sure. You want to add this in ToDo list', function () {
				$.ajax({
					url: base_url + 'account/addinTodo/',
					method: 'POST',
					dataType: 'json',
					data: {
						 commentId : item
					},
					success: function (response) {
						Notify.success(response, {
							afterClose : function () {

							}
						});
					}
				});
			}, function () {

			});


		}

function   beamember(){
						let lginuser =  $('#userID').val();
						// alert(lginuser);

					let redirectlink = 	document.getElementById('baseurl').value +'pages/view/signup';
					// alert(redirectlink);
	if(lginuser == ''){
		window.location.replace(redirectlink);
		return;
	}

	let data = {

		groupid : $('#GroupUdId').val(),
		PostUdId: $('#PostUdId').val(),
		poid : $('#Po_ID').val(),
		member_status : $('#userType').val()
	}
	Notify.confirm('Continue to join as member of this post to be able to comment?', function () {
		$.ajax({
			type: "POST",
			url: base_url + "account/beamember/",
			data: data,
			dataType: 'json',
			success: function(response){

				if(response.status == 'success'){

					$('#joingroupfromcomment').text('Pending Request');
					$('#joingroupfromcomment').css({
						"background": "green",
						"color" : "white"
					})
					if(response.type == 'post'){
						Notify.success(response.message, {
							afterClose : function () {


							}
						});
					}else if(response.type == 'group'){

						Notify.success(response.message, {
							afterClose : function () {


							}
						});
					}

				}else {

					Notify.success(response.message, {

					});

				}

			}
		});
	}, function () {});


}

$(document).ready(function () {


	$('.btn_select_role').click(function() {
		$('.btn_select_role').removeClass('active');

		$(this).addClass('active');
		modifyLink();


	});

	function modifyLink(){
 		$finalLink = '';
		$stringOfURl = $('#linkpost').data('url').split('/');

		$stringOfURl[8] = $('.btn_select_role.active').data('role');
		for(var x = 0 ; x < $stringOfURl.length ; x++){

			if(x == $stringOfURl.length -1){
				$finalLink +=	$stringOfURl[x];
			}else{
				$finalLink += $stringOfURl[x]+"/";
			}
		}

		$('.addthis_inline_share_toolbox').data('url', $finalLink).attr('data-url',$finalLink);
		$('#changeInviteLInk').val($finalLink);

	}

	$('#inviceButton').click(function() {
		/* Get the text field */
		var copyText = document.getElementById("changeInviteLInk");

		/* Select the text field */
		copyText.select();
		copyText.setSelectionRange(0, 99999); /* For mobile devices */

		/* Copy the text inside the text field */
		navigator.clipboard.writeText(copyText.value);
		/* Alert the copied text */
		// alert("Copied the text: "  );

		$('.invite_members').removeClass('popped');
		$('.group_info_container ').removeClass('popped');

		Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Link is Copied',
			showConfirmButton: false,
			timer: 3000
		})

	})

	$(".invite_members_closer").click(function(){
		$(".group_info_container").removeClass('popped');

		$(".invite_members").removeClass('popped');
		$(".modal_container").removeClass('popped');
		$(".user_role").removeClass('popped');
	});
	$(".invite_member_btn").click(function(){
		// alert("asdasdas");
		$(".group_info_container").addClass('popped');
		  $(".invite_members").addClass('popped');
		// $('#userInviteKey').val("");
		// $('#btnUserInviteSearch').trigger('click');
	});

	$("#textareaforComment").on("keyup", function () {
		 console.log("this");
		// var content = $(this).text(); //Content Box Data
		// console.log(content);
//         var go = content.match(start); //Content Matching @
//         var name = content.match(word); //Content Matching @abc
//         var dataString = 'searchword=' + name;
		//If @ available
//         if (go.length > 0) {
//             $("#msgbox").slideDown('show');
//             $("#msgbox").html('<img src="load.gif" />');
//             //if @abc avalable
//             if (name.length > 0) {
//                 $.ajax({
//                     type: "POST",
//                     url: "textareausernameinsert.php", // Database name search
//                     data: dataString,
//                     cache: false,
//                     success: function (data) {
//                         $("#msgbox").hide();
//                         $("#msgbox").html(data).show();
//                     }
//                 });
//             }
//         }
		return false;
	});



	//Adding result name to content box.
// 	$(".addname").live("click", function () {
// 		var username = $(this).attr('title');
//

// 		var content = old.replace(word, "@"); //replacing @abc to (" ") space
// 		$("#newmsg").html(content);
// 		var E = "<a contenteditable='false' href='#'>" + username + "</a>";
// 		$("#newmsg").append(E);
// 		$("#msgbox").hide();
//
// //         INSERT AJAX HERE TO SEND CONTENT TO DATABASE, REMOVED AS NOT NEEDED.
//
// 	});
});

function checkMeOUt(username , userID){


	// var username = data.attr('title');
	console.log(username);
	var old = $("#textareaforComment").html();
		console.log(word);
	// var userID =	data.data("userid")
	var content = old.replace(word, "@");
	// console.log(content);
	// console.log(content.length);
	// content = '';
	// content = content.slice(0, -1);
	// console.log(content);
	$("#textareaforComment").html(content);
    var E = "<a   value='@"+username+"' data-user-id="+userID+" data-user-name="+username+"   href='#'>" + username + "</a>";
	 $("#textareaforComment").append(E);
 	 $("#msgbox").hide();
}
function make_plus(){
	$.ajax({
		url: base_url + 'post/addfavourite',
		method:'POST',
		data:{PostID: $("input[name='PostID']").val()},
		success:function(data)
		{
			if(data == 'activate'){
				if($('.changefav').hasClass('fa-star-o')){
					$('.changefav').removeClass("fa-star-o");

					$('.changefav').addClass("fa-star");

				}
			}else {
				if($('.changefav').hasClass('fa-star')){
					$('.changefav').removeClass("fa-star");

					$('.changefav').addClass("fa-star-o");

				}
			}

		}
	});}
function counter_plus(){


	$.ajax({
		url: base_url + 'post/updateCounter',
		method:'POST',
		data:{PostID: $("input[name='PostID']").val()},
		success:function(data)
		{
				if(data == 'activate'){
						if($('.changeheart').hasClass('fa-heart-o')){
							$('.changeheart').removeClass("fa-heart-o");

							$('.changeheart').addClass("fa-heart");

						}

					$('#postcounter').show();
					$('#postcounter').text(	parseInt($('#postcounter').text()) + 1 );
					$('#counterheader').text(	parseInt($('#counterheader').text()) + 1 );

				}else{
					if($('.changeheart').hasClass('fa-heart')){
						$('.changeheart').addClass("fa-heart-o");

						$('.changeheart').removeClass("fa-heart");

					}



					$('#postcounter').show();
					$('#postcounter').text(	parseInt($('#postcounter').text()) - 1 );
					$('#counterheader').text(	parseInt($('#counterheader').text()) - 1 );
				}
			console.log(data);
	     }
	});

}

function ExtendPoll(){
	Notify.confirm('Are you sure. you want to Extend this Poll?', function () {
		var objdata = {
			Extendvalue: 0,
			PostID: $("input[name='PostID']").val()
		};
		$.ajax({
			url: base_url + 'account/endpoll',
			method:'POST',
			data:objdata,
			success:function(data)
			{
				$response =  JSON.parse(data);
				Notify.success($response.message, {
					afterClose : function () {
						location.reload();

					}
				});

			}
		});


	}, function () {});

}
function endPoll() {
	Notify.confirm('Are you sure. you want to End this Poll?', function () {
		var objdata = {
			Extendvalue: 1,
			PostID: $("input[name='PostID']").val()
		};
		$.ajax({
			url: base_url + 'account/endpoll',
			method:'POST',
			data:objdata,
			success:function(data)
			{
				$response =  JSON.parse(data);


				Swal.fire({
					title: 'Poll Ended?',
					text:  $response.message,
					icon: 'warning',
					showCancelButton: true,

					cancelButtonColor: '#15AFE8',
					confirmButtonColor:'#15AFE8',
					confirmButtonText : 'Okay',
					confirmButtonText: 'Extend Poll!',

				}).then((result) => {
					if (result.isConfirmed) {
						ExtendPoll();
					}else{
						location.reload();
					}})

			}
		});


	}, function () {});

}
function deletepool(){


	try{
		$('#resultanswer').modal('hide');
	}catch (e) {

	}
	try{
		$('#poolanswer').modal('hide');
	}catch (e) {

	}
	Notify.confirm('Are you sure. you want to delete this Poll?', function () {
		var objdata = {

			PostID: $("input[name='PostID']").val()
		};
		$.ajax({
			url: base_url + 'account/deletepoll',
			method:'POST',
			data:objdata,
			success:function(data)
			{
				$response =  JSON.parse(data);
				Notify.success($response.message, {
					afterClose : function () {
						location.reload();

					}
				});

			}
		});


	}, function () {});

}
function editpool(){
				try{
					$('#resultanswer').modal('hide');
				}catch (e) {

				}
				try{
					$('#poolanswer').modal('hide');
				}catch (e) {

				}
 				$('#editingpool').modal('show');
}
function createpool(){
			$('#poolmodel').modal('show');
	}

function editnewdnewAnswer(){

	var numberOfSpans = $('#editanswerques').children('div').length;
	numberOfSpans = numberOfSpans+1;
	// $newQuestion = 	'<div class="form-group">' +
	// 	'<input type="text" class="form-control answerinput" placeholder="Enter Question" id="answer'+numberOfSpans+1+'" name="answer'+numberOfSpans+1+'">' +
	// '</div>';
	$newQuestion =  '<div class="input-group mb-3 mt-2 col-sm-12" style="margin:10px" id="questionidedit'+numberOfSpans+'">' +
		'<input type="text" required  class="form-control answerinput" name="questionupdate[]" id="answer'+numberOfSpans+'"   placeholder="Enter Answer">\n' +
		'<div class="input-group-append" id="subbutton'+numberOfSpans+'">' +
		'<span class="input-group-text" onclick="removeItemedit('+numberOfSpans+')"><i class="fa fa-times"></i></span>' +
		'</div>' +
		'</div>'
	$('#editanswerques').append($newQuestion);
		return false;
}

function removeItemedit(itemID){
	$('#questionidedit'+itemID).remove();
	itemID = itemID+1;
	var numberOfSpans = $('#answerDiv').children('div').length;
	numberOfSpans = numberOfSpans+1;

	for(var x = itemID  ;   x <= numberOfSpans; x++){
		$newud = 	x-1

		$('#questionid'+x).prop('id', 'questionid'+$newud);

		$('#answer'+x).prop('id', 'answer'+$newud);
		$('#subbutton'+x).empty();
		$('#subbutton'+x).append('<span class="input-group-text" onclick="removeItem('+$newud+')"><i class="fa fa-times"></i></span>');
		$('#subbutton'+x).prop('id', 'subbutton'+$newud);
	}
}
const isValidUrl = urlString=> {
	try {
		return  /^(ftp|http|https):\/\/[^ "]+$/.test(urlString);
	}
	catch(e){
		return false;
	}
}
function checkthelink(val){



}
	function addnewAnswer(){
		var numberOfSpans = $('#answerDiv').children('div').length;
		numberOfSpans = numberOfSpans+1;
 		// $newQuestion = 	'<div class="form-group">' +
			// 	'<input type="text" class="form-control answerinput" placeholder="Enter Question" id="answer'+numberOfSpans+1+'" name="answer'+numberOfSpans+1+'">' +
			// '</div>';
		$newQuestion =  '<div class="input-group mb-3" id="questionid'+numberOfSpans+'">' +
		 '<input type="text" required  class="form-control answerinput" id="answer'+numberOfSpans+'"   placeholder="Enter Answer">\n' +
		 '<div class="input-group-append" id="subbutton'+numberOfSpans+'">' +
		 '<span class="input-group-text" onclick="removeItem('+numberOfSpans+')"><i class="fa fa-times"></i></span>' +
		 '</div>' +
		 '</div>'
		$('#answerDiv').append($newQuestion);

	}

	function removeItem(itemID){
			$('#questionid'+itemID).remove();
		itemID = itemID+1;
		var numberOfSpans = $('#answerDiv').children('div').length;
		numberOfSpans = numberOfSpans+1;

		for(var x = itemID  ;   x <= numberOfSpans; x++){
		$newud = 	x-1

			$('#questionid'+x).prop('id', 'questionid'+$newud);

			$('#answer'+x).prop('id', 'answer'+$newud);
			$('#subbutton'+x).empty();
			$('#subbutton'+x).append('<span class="input-group-text" onclick="removeItem('+$newud+')"><i class="fa fa-times"></i></span>');
			$('#subbutton'+x).prop('id', 'subbutton'+$newud);
		}
	}

	function answerupdatevalidation(){
		var values = [];
		$("input[name='questionupdate[]']").each(function() {
			values.push($(this).val());
		});

		// editpool
		var description = CKEDITOR.instances['editpool'].getData();

			var qesutttionmain = $('#pollquestionmain').val();
		var objdata = {
			pollID : $('#poolidx').val() ,
			description : description,
			qesutttionmain : qesutttionmain,
			answer : values,
			PostID: $("input[name='PostID']").val()
		};
		if($('#selActionButtonedit').val() == 0){

			objdata.serverybuttonname =  $('#txtActionButtonedit').val();
			objdata.serverybuttonlink  = $('#serveryLinkedit').val();
		}else{
			objdata.serverybuttonname =  $('#selActionButtonedit').val();
			objdata.serverybuttonlink = $('#serveryLinkedit').val();
		}


		if($('#serveryLinkedit').val() != ''){
			if( !isValidUrl($('#serveryLinkedit').val())){
				$('#errormessage1').show();
				return;
			}else{
				$('#errormessage1').hide();
			}
		}else{
			$('#errormessage1').hide();
		}


		$.ajax({
			url: base_url + 'account/pollanswerupdate',
			method:'POST',
			data:objdata,
			success:function(data)
			{
				$response =  JSON.parse(data);
				Notify.success($response.message, {
					afterClose : function () {
						$('#editingpool').modal('hide');
						location.reload();
					}
				});


			}
		});

		return false;
	}
		function answwercheckvailidation(){

			var radioValue = $("input[name='questionname']:checked").val();
			var answerNote  = $('#answerNote').val();
			// alert($('#answerNote').val());
			// return;
			var objdata = {
				pollID : $('#poolid').val() ,
				answer : radioValue,
				PostID: $("input[name='PostID']").val(),
				answernote : $('#answerNote').val()
			};
			$.ajax({
				url: base_url + 'account/answeraddpoll',
				method:'POST',
				data:objdata,
				success:function(data)
				{
					$response =  JSON.parse(data);
					Notify.success($response.message, {
						afterClose : function () {
							$('#poolanswer').modal('hide');
							location.reload();
						}
					});


				}
			});
			return false;
		}

		function viewresult(){
			$('#poolanswer').modal('hide');
 			$('#resultanswer').modal('show');
		}
	 function checkvailidation(){


		 var numberOfSpans = $('#answerDiv').children('div').length;
		 var answersheet = '';
		 for(var x = 1 ; x <= numberOfSpans; x++){
			 if(x == numberOfSpans){
				 answersheet+=$('#answer'+x).val();
			 }else{
				 answersheet+=$('#answer'+x).val()+'^';
			 }

		  }





		 var valuex = CKEDITOR.instances['pooldescription'].getData();

		 var objdata = {
			 pollquestion : $('#pool_question').val() ,
			 pooldescription : valuex,
			 answersheet : answersheet,
			 PostID: $("input[name='PostID']").val()
		 };
		 if($('#selActionButton').val() == 0){

			 objdata.serverybuttonname =  $('#txtActionButton').val();
			 objdata.serverybuttonlink  = $('#serveryLink').val();
		 }else{
			 objdata.serverybuttonname =  $('#selActionButton').val();
			 objdata.serverybuttonlink = $('#serveryLink').val();
		 }


		 if($('#serveryLink').val() != ''){
			if( !isValidUrl($('#serveryLink').val())){
				$('#errormessage').show();
				return;
			}else{
				$('#errormessage').hide();
			}
		 }else{
			 $('#errormessage').hide();
		 }




		 $.ajax({
			 url: base_url + 'account/addpoll',
			 method:'POST',
			 data:objdata,
			 success:function(data)
			 {
				$response =  JSON.parse(data);

				 Notify.success($response.message, {
					 afterClose : function () {
						 $('#poolmodel').modal('hide');
						 location.reload();
 					 }
				 });



			 }
		 });
			return false;
	 }

		function testing(){
			Notify.confirm('Continue to join this post', function () {
				for($i = 0 ;  $i < $questionDatafooter.length ; $i++){
					$questionDatafooter[$i]['answer'] = $('#answerId'+$questionDatafooter[$i]['id']).val();
				}


				$.ajax({
					url: base_url + 'account/SaveAnswerofGroupWithEncode',
					method:'POST',
					dataType: 'json',
					data:{ group_id: $("#groupIDx").val() , answer : $questionDatafooter , post_id : $('#Post_ID').val()  },
					success:function(response) {

						if(response == 'success'){
							Notify.success('Answers submitted, Your admission is pending approval', {
								afterClose : function () {
									$('#AnserModalfooter').modal('hide');
									if($('#roleType').val() == 'memberdirectjoin'){
										var data = {"post_id" : $("#Post_ID").val(),  "post_role" : 'member' , "join_type" : 'group_direct_join'};

										Notify.confirm('Continue join this post?', function () {
											$.ajax({
												type: "POST",
												url: base_url + "account/checkuseralreadymemberforPOST/",
												data: data,
												dataType: 'json',
												success: function(response){

													if(response.status == 'success'){
														if(response.type == 'post'){
															Notify.success(response.message, {
																afterClose : function () {

																	window.location = base_url + 'account/view/post/' + response.id;
																}
															});
														}else if(response.type == 'group'){

															Notify.success(response.message, {
																afterClose : function () {

																	window.location = base_url + 'account/view/group/' + response.id;
																}
															});
														}

													}else {

														Notify.success(response.message, {

														});

													}

												}
											});
										}, function () {});
									}else if($('#roleType').val() == 'followerdirectfollow'){

									}else if($('#roleType').val() == 'memberinvitejoin'){
										var data = {"post_id" : $("#Post_ID").val(),
											"post_referrer" : $("#post_refererer").val() ,
											"post_role" : $("#post_role").val()};
										Notify.confirm('Continue join this post?', function () {
											$.ajax({
												type: "POST",
												url: base_url + "account/checkuseralreadymemberforPOST/",
												data: data,
												dataType: 'json',
												success: function (response) {

													if (response.status == 'success') {
														if (response.type == 'post') {
															Notify.success(response.message, {
																afterClose: function () {

																	window.location = base_url + 'account/view/post/' + response.id;
																}
															});
														} else if (response.type == 'group') {

															Notify.success(response.message, {
																afterClose: function () {

																	window.location = base_url + 'account/view/group/' + response.id;
																}
															});
														}

													} else {

														Notify.success(response.message, {});

													}

												}
											});
										});
									}else if($('#roleType').val() == 'memberinvitefollow'){

									}
									return false;
								}
							});

						}else{
							Notify.error('Answer Not Submmited, try Again later', {
								afterClose : function () {
									return false;
								}
							});
						}


					}
				});

			}, function () {});

			return false;


		}
$(document).ready(function(){
    ViewPost.init();

	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio:  800/312,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:820,
			height:312
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$.ajax({
					url: base_url + 'post/upload_image',
					method:'POST',
					data:{PostID: $("input[name='PostID']").val(), image:base64data},
					success:function(data)
					{
						$modal.modal('hide');
						// $('#uploaded_image').attr('src', base_url + data.trim());
						$(".post-cover-image").css("background", "transparent url('"+base_url + data.trim()+"') no-repeat center center /cover");
						$(".post-cover-image").css("background-size", "100% 100%");
					}
				});
			};
		});
	});
});
