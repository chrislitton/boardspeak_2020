var ViewGroup = {
    initComponents : function() {

        $(".group_taggline").collapser({
            mode:'lines',
            truncate: 1,
			showText: '[Show More]',
			hideText: '[Show Less]',
			changeText: true
        });
        
		$("#btn_create_topic").click(function(e) {
			e.preventDefault();
 				let adminAccount =	 $('#admintype').val();
 			if(adminAccount == 1 || $('#packagedetail').val() == 3){

				var location = base_url + 'account/create/topic/';
				if ($('#Gr_ID').val() != '') {
					location = location + $('#Gr_ID').val() + "/";

					if ($('#inp_category_id').val() != '') {
						location = location + $('#inp_category_id').val() + "/";

						if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*')
							location = location + $('#inp_subcategory_id').val();
					}
				}

				window.location.href = location;
				// var location =  '<?php echo base_url(); ?>account/create/topic/';
				// window.location.href = location;
			}
			else
			{
				Swal.fire({
					title: 'Info',
					text: "Your plan has no access to Subgroups feature. Avail PRO plan with 90-day FREE trial.",
					icon: 'info',
					showCancelButton: true,
					confirmButtonText:'Contact Us',
					cancelButtonColor:  '#d33',
					confirmButtonColor:  '#3085d6',
					cancelButtonText: 'Change to PRO plan'
				}).then((result) => {

					if (result.isConfirmed) {
 						$('#createTopicModal').modal('hide');
						$('#ContactUsForm').modal('show');
					}else if(result.dismiss == 'cancel'){

						$('#exampleModal').modal('hide');
						$('#packagesmodel').modal('show');
						// window.location.href = base_url+'account/pricing';
					}
				})
			}



		});


		$(".btn_create_post_group").click(function(e) {
			e.preventDefault();


			var location = base_url + 'account/create/post/';
		
				location = location + $('#Gr_ID').val() + "";

				if($('#inp_category_id').val() != ''){
					location +=   "/" + $('#inp_category_id').val();


					if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*'){

						location += "/" +  $('#inp_subcategory_id').val();

					}

				}else{
					if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*'){

						location += "//" +  $('#inp_subcategory_id').val();

					}

				}
                  window.location.href = location;
		});

		$(".btn-category-chooser").click(function(e) {
			e.preventDefault();
			let category_id = $(this).prop('id');
			$(".categoryFilter .item").removeClass("selectedCategory");

			$('.group_post_filter_row').show();
			$(this).parent().addClass('selectedCategory');

			$("#inp_category_id").val(category_id);
			$("#inp_subcategory_id").val('');
			let address = base_url+"account/groupall/posts/topic/"+$('#groupID').val();

			if($('#inp_category_id').val() != ''){

				address += '/'+$('#inp_category_id').val();

				if($("#inp_subcategory_id").val() != '' ){
					address += '/'+$('#inp_subcategory_id').val();

				}
			}


			$('#allpostpage').attr('href' , address);



			ViewGroup.search_topics_per_category(category_id);
		});
        $(".secondery_btn").click(function(){
            // button class slided toggle
            $(this).toggleClass("slided");
            // parent class add
            $(this).parent().toggleClass("slided");
            // secondery menu toggle class
            $(this).next(".secondery_dropdown").toggleClass("slided");
        });

        // hide on mouse leave
        $(".secondery_item").mouseleave(function(){
            $(this).removeClass("slided");
            $(".secondery_btn").removeClass("slided");
            $(".secondery_dropdown").removeClass("slided");
        });

        $(".invite_member_btn").click(function(){
          $(".modal_container").addClass('popped');

          $(".invite_members").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".invite_members").removeClass('popped');
        });

        $(".user_role_btn").click(function(){
          $(".modal_container").addClass('popped');
          $(".user_role").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".user_role").removeClass('popped');
        });
        
        // open script
        $(".role_opener").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });
        $(".change_role_btn").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });

        // clsoe script
        $(".role_dropdown").mouseleave(function(){
            $(".role_dropdown").removeClass('dropped');
        });
        $(".role_item").click(function(){
            var value = $(this).html();
            
            var parent = $(this).parents(".role_dropdown");
            var role_opener = $(parent).children(".role_opener");
            
            $(role_opener).html(value)
            
            $(".role_dropdown").removeClass('dropped');
        });
    },
    
    initEvents: function() {
    	$("button.board-link").click(function(e) {
    		e.preventDefault();
    		let category_id = $(this).prop("id");
    	});
    	
		$("#btn-request-access").click(function(e){
			ViewGroup.request_grant_access('group');
		});

		$("#btn-request-topic-access").click(function(e){
			ViewGroup.request_grant_access('topic');
		});

		$("#btn-request-post-access").click(function(e){
			ViewGroup.request_grant_access('subtopic');
		});
		$('#groupAccessModal').on('show.bs.modal', function (e) {
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));

		});

		$( "#topicAccessModal" ).on('show.bs.modal', function(e){
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));
		});

		$( "#subtopicAccessModal" ).on('show.bs.modal', function(e){
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));
		});

		$("#btn_search_text").click(function(e) {
			e.preventDefault();

			ViewGroup.search_topics_per_category($("#inp_category_id").val());
		});



		$('#btn_follow').click(function () {
				if($('#Group_privacy').val() == 'private'){
				getQuestionSection( $("#Gr_ID").val() , 'follower');
			}else{
					var data = {"id" : $("#Gr_ID").val(), "type" : "group" , "role" : "follower"};
					Notify.confirm('You will become a FOLLOWER of this group.', function () {
						$.ajax({
							type: "POST",
							url: base_url + "account/join/",
							data: data,
							dataType: 'json',
							success: function(result){
								$('#btn_processing').unbind('click');
								$('#btn_processing').attr('id', 'btn_followed').text('Following');
								Notify.success(result.message, {
									afterClose : function () {
										if($('#Group_privacy').val() == 'public'){
											location.reload();
										}else{
											var url = '<?php echo base_url(); ?>';
											location.replace(url+"account/explore");
										}
									}
								});

							}
						});
					}, function () {});

				}

		});


		$('#btn_join').click(function () {
			if($('#Group_privacy').val() == 'private'){
				getQuestionSection( $("#Gr_ID").val() , 'member');
			}else{
				var data = {"id" : $("#Gr_ID").val(), "type" : "group" , "role" : "member"};
				Notify.confirm('You will become a MEMBER of this group.', function () {
					$.ajax({
						url: base_url + 'account/join/',
						method: 'POST',
						dataType: 'json',
						data: data,
						success: function (result) {

							console.log(result);
							$('#btn_processing').unbind('click');
							$('#btn_processing').attr('id', 'btn_invite').text('Invite Members');
							Notify.success(result.message, {
								afterClose : function () {
									if($('#Group_privacy').val() == 'public'){
										location.reload();
									}else{
										var url = '<?php echo base_url(); ?>';
										location.replace(url+"account/explore");
									}
									//
								}
							});

						}
					});
					// $.ajax({
					// 	type: "POST",
					// 	url: base_url + "account/join/",
					// 	data: data,
					// 	dataType: 'json',
					// 	success: function(result){
					//
					// 	}
					// });
				}, function () {});

			}

			});


		$(".invite_member_btn").click(function(){
            $(".group_info_container").addClass('popped');
            $(".invite_members").addClass('popped');
            $('#btnUserInviteSearch').trigger('click');
		});
		
		$(".invite_members_closer").click(function(){
            $(".group_info_container").removeClass('popped');
            $(".user_role").removeClass('popped');
            $(".invite_members").removeClass('popped');
		});

		$('.btn_select_role').click(function() {
			$('.btn_select_role').removeClass('active');

			$(this).addClass('active');
			modifyLink();


		});

		$('#inviceButton').click(function() {
			/* Get the text field */

		 
			var copyText = document.getElementById("changeInviteLInk");

			/* Select the text field */
			copyText.select();
			copyText.setSelectionRange(0, 99999); /* For mobile devices */

			/* Copy the text inside the text field */
			navigator.clipboard.writeText(copyText.value);
			/* Alert the copied text */
			// alert("Copied the text: "  );
			$("#invite_members").modal("hide");
			$('#invite_members ').hide();
			Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Link is Copied',
				showConfirmButton: false,
				timer: 3000
			})

		})

		function modifyLink(){
			// $('.at-icon-wrapper .at-share-btn .at-svc-link').hide();
			$finalLink = '';
			$stringOfURl = $('#linkpost').data('url').split('/');

			$stringOfURl[6] = $('.btn_select_role.active').data('role');
			for(var x = 0 ; x < $stringOfURl.length ; x++){

				if(x == $stringOfURl.length -1){
					$finalLink +=	$stringOfURl[x];
				}else{
					$finalLink += $stringOfURl[x]+"/";
				}
			}


			$('.addthis_inline_share_toolbox').data('url', $finalLink).attr('data-url',$finalLink);
			$('#changeInviteLInk').val($finalLink);






		}
		
		$(document).on('click', '.add_member', function() {
            var user_id = $(this).data('user');
            var name = $('#invite_user_' + user_id).find('.user_name_img .invite_name').text();
            $(this).hide();
            $('#invite_user_' + user_id).find('.add_member_loading').show();
            Notify.confirm('Continue inviting '+name+' to join the group?', function () {
                $.ajax({
                    url: base_url + 'group/invite_user_to_group/',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: $('#groupID').val(),
                        user_id: user_id,
                        role: $('.btn_select_role.active').data('role')
                    },
                    success: function (response) {
                        Notify.success(response.message, {
							afterClose : function () {
								$('#btnUserInviteSearch').trigger('click');
							}
						});
                    }
                });
            }, function () {
                $('#invite_user_' + user_id).find('.add_member').show();
                $('#invite_user_' + user_id).find('.add_member_loading').hide();
            });
        });

        $('#btnUserInviteSearch').click(function() {
            $('.invite_search_list').empty();
            $.ajax({
                url: base_url + 'group/search_invite_contacts/',
                method: 'POST',
                dataType: 'json',
                data: {
                    id: $('#groupID').val(),
                    keyword: $('#userInviteKey').val(),
                },
                success: function (response) {
                    if (response.totalRecords == 0) {
                        $item = $('.no_result_template').clone()
                                    .removeClass('no_result_template')
                                    .addClass('invite_search_list')
                                    .attr('id', 'invite_user_0')
                                    .show();
                                    $('.invite_search_list').append($item);
                    } else {
                        $.each(response.data, function(key, user) {
							console.log("userDetail" , user);
                            if (!$('#invite_user_' + user.Us_ID)[0]) {


                                $item = $('.user_invitation_template').clone()
                                            .removeClass('user_invitation_template')
                                            .addClass('invite_user')
                                            .attr('id', 'invite_user_' + user.Us_ID)
                                            .show();
                                $('.invite_search_list').append($item);
                                $('#invite_user_' + user.Us_ID).find('.user_name_img img').attr('src', user.Us_Thumb);
                                $('#invite_user_' + user.Us_ID).find('.user_name_img .invite_name').text(user.Us_Name);
                                if (user.Me_Status == true) {
                                    $('#invite_user_' + user.Us_ID).find('.added_member').show();
                                } else {
                                    $('#invite_user_' + user.Us_ID).find('.add_member').attr('data-user', user.Us_ID).show();
								}
								
								$('#invite_user_' + user.Us_ID).find('.user_name_img').on('click', function() {
                                    location.href = user.Us_URL;
                                });
                            }
                        });
                    }
                }
            });
		});
		
		$('.invite_response_btn').click(function() {
			var invite_response = $(this).data('action');
			Notify.confirm('Are you sure that you want to '+invite_response+' group invitation?', function () {
				$.ajax({
                    url: base_url + 'group/respond_to_group_invitation/',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: $('#groupID').val(),
                        action: invite_response
                    },
                    success: function (response) {
                        Notify.success(response.message, {
							afterClose : function () {
								location.reload();
							}
						});
                    }
                });
            }, function () {});
		});
    },

	request_grant_access: function(type) {
		var data = {"ParentID" : $("#group_id").val(), "Parent" : type};

	 	$.ajax({
	 		type: "POST",
		 	url: base_url + "account/request_grant_access/",
		 	data: data, 
		 	dataType: 'json',
		 	success: function(result){

		 		if (result === "success")
		    		alert('Access has been requested.');
		    	else
		    		alert('Request to access '+type+' already exist.');

		    	$('#'+type+'AccessModal').modal('toggle');
		  	}
	 	});
	}

	,search_topics_per_category: function(category_id , popoular = 'no' , favourite = 'no' , private = 'no' , subgroups = 'no') {
		let group_id = $("#Gr_ID").val();
		let search_text = $("#txtSearch").val();
		let sub_category_id = $('#inp_subcategory_id').val();
		$.ajax({
			url: base_url + 'group/show_group_topics/',
			method: 'POST',
			data: {
				ID: group_id,
				category_id: category_id,
				sub_category_id : sub_category_id,
				search_text: search_text ,
				popoular : popoular ,
				favourite: favourite,
				private : private,
				subgroups : subgroups
			},
			success: function (response) {
				response = $.parseJSON(response);

				let container = $(".group_topic_filter_container");
				let subcontainer;

				container.empty();
				let subcategory_create_button = $('.subcategory-create-button-clone').clone()
												.removeClass('subcategory-create-button-clone')
												.addClass('subcategory-create-button')
												.show();
				container.append(subcategory_create_button);
				$counter = 0;
				/*
				Add the topic items here
				*/


				if(response.data_items.length == 5){
					$('#buttonforMore').show();
				}

				$.each(response['data_items'], function (i, item){
					item_to_sc_id = (item.To_Sc_ID).replace(/ /gi, '_');
					item_to_ca_id = (item.To_Ca_ID).replace(/ /gi, '_');

					subcategory_create_button = $('.group-items-clone').clone()
					.removeClass('group-items-clone')
					.addClass('filter-' + item_to_ca_id + '-' + item_to_sc_id)
					.show();
					subcategory_create_button.find('.group-item-link').attr("href", item.To_Href);
					subcategory_create_button.find('.topic-thumbnail').attr("src", item.To_Thumb);

					subcategory_create_button.find('.board_likespan').html(item.Likes);

					if (item.Favourite == 1){
						subcategory_create_button.find('#showfavourite').addClass('fa-star');
					}

					if (item.To_Href == "#") {
						subcategory_create_button.find('.group-item-link').attr({"data-toggle": "modal", "data-target" : item.Target, "data-id" : item.EncodedID});
					}

					if (item.To_Privacy != 'public') {
						subcategory_create_button.find(".privacy-key").show();
					}
					subcategory_create_button.find('.group-item-name').html(item.To_Name);

					if($('#userType').val() == 'superadmin'){


						if(	$counter >= 5){



						}
						else{
							if(item.To_Pin == 0){

								subcategory_create_button.find('.makepinclass').show();
								subcategory_create_button.find('.unpinedclass').show();
								subcategory_create_button.find('.makepinclassforthumb').show();
								subcategory_create_button.find('.PinnedClass').hide();
								subcategory_create_button.find('.pinclassforthumb').hide();
								subcategory_create_button.find('.makepinclassforthumb').attr({"postitem":item.To_ID});
								subcategory_create_button.find('.makepinclassforthumb').attr({"posttype":item.item_Type});
							}
							else{
								subcategory_create_button.find('.makepinclass').show();
								subcategory_create_button.find('.PinnedClass').show();
								subcategory_create_button.find('.pinclassforthumb').show();
								subcategory_create_button.find('.makepinclassforthumb').hide();
								subcategory_create_button.find('.unpinedclass').hide();

								subcategory_create_button.find('.pinclassforthumb').attr({"postitem":item.To_ID});
								subcategory_create_button.find('.pinclassforthumb').attr({"posttype":item.item_Type});

							}

						}
						if(item.To_Pin == 1 ){
							$counter++;
						}

					}


					console.log(item.To_Gr_ID , $("#Gr_ID_decode").val())
					if(item.To_Gr_ID != $("#Gr_ID_decode").val()){

					}else{
						container.append(subcategory_create_button);
					}
					// container.append(subcategory_create_button);
				});

				/*
				Add the topic filter when there is a category selected
				*/
				if (category_id !=0) {

					if ($(".group_topic_filter").hasClass('slick-slider'))
						$('.group_topic_filter').slick('unslick');

					topic_filter_container = $(".group_topic_filter");
					topic_filter_container.empty();

					subcategory_item_current = $('.subcategory-item-current-clone').clone()
													.removeClass('subcategory-item-current-clone')
													.show();
					topic_filter_container.append(subcategory_item_current);
					$.each(response['subcategory_items'], function (i, item){

						// item_sc_id = (item.Sc_ID).replace(/ /gi, '_');
						// item_sc_ca_id = (item.Sc_Ca_ID).replace(/ /gi, '_');
						item_sc_id = item.Sc_ID;
						item_sc_ca_id = item.Sc_Ca_ID;
						subcategory_item = $('.subcategory-item-clone').clone()
														.removeClass('subcategory-item-clone')
														.attr('data-filter', '.filter-' + item_sc_ca_id + '-' + item_sc_id)
														.show();
						subcategory_item.find('.subCategory').attr({'dataCat': item_sc_ca_id, 'dataSubCat':item_sc_id});
						subcategory_item.find('.text-primary').html(item.Sc_Name);

						topic_filter_container.append(subcategory_item);
					});
				}

				ViewGroup.refresh_slick();
			}
		});
	},

	refresh_slick : function() {

		$(".group_topic_filter").slick({
			dots: false,
			infinite: false,
			slidesToShow: 7,
			slidesToScroll: 3,
			prevArrow: false,
			nextArrow: false,
			variableWidth: true,
			focusOnSelect: true,
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: false
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: false
				}
			}]
		});

			$('.makepinclassforthumb').click(function(){
				$poid = 	$(this).attr('postitem')
				$posttype =  $(this).attr('posttype')
				Notify.confirm("Are you sure. you want to pin this "+$(this).attr('posttype')+"", function () {
					$.ajax({
						url: base_url + 'group/group_pinning_post',
						method: 'POST',
						dataType: 'json',
						data: {
							post_item:  $poid,
							post_type: $posttype,
							post_decide :  1
						},
						success: function (response2) {
							Notify.success(response2 , {
								afterClose : function () {
									location.reload();
									// ViewGroup.search_topics_per_category($("#inp_category_id").val());
								}
							});
						}
					});
				}, function () {

				});
			});
		$('.pinclassforthumb').click(function(){
				$poid = 	$(this).attr('postitem')
				$posttype =  $(this).attr('posttype')
			Notify.confirm("Are you sure. you want to unpin this "+$(this).attr('posttype')+"", function () {
				$.ajax({
					url: base_url + 'group/group_pinning_post',
					method: 'POST',
					dataType: 'json',
					data: {
						post_item:  $poid,
						post_type: $posttype,
						post_decide :  0
					},
					success: function (response2) {
						Notify.success(response2 , {
							afterClose : function () {
								location.reload();
								// ViewGroup.search_topics_per_category($("#inp_category_id").val());
							}
						});
					}
				});
			}, function () {

			});
		});

		$('.group_topic_filter .item').click(function(){

			$('.group_topic_filter .item .current').removeClass('current');
			$('.group_topic_filter .item').removeClass('current');
			$(this).addClass('current');
			var selector = $(this).attr('data-filter');

			$('#inp_subcategory_id').val($(this).find('.subCategory').attr('datasubcat'));


			let address = base_url+"account/groupall/posts/topic/"+$('#groupID').val();

			if($('#inp_category_id').val() != ''){

				address += '/'+$('#inp_category_id').val();

				if($("#inp_subcategory_id").val() != '' ){
					address += '/'+$('#inp_subcategory_id').val();

				}
			}


			$('#allpostpage').attr('href' , address);

			// ViewGroup.search_topics_per_category($('#inp_category_id').val());
			// $(".subcategory-create-button").removeClass().addClass('col-4 subcategory-create-button').addClass(selector.substring(1));

			
			// $(".group_topic_filter_container").isotope({
			// 	filter: selector,
			// 	animationOptions: {
			// 		duration: 750,
			// 		easing: 'linear',
			// 		queue: false
			// 	}
			// });

			// $(".group_topic_filter_container").isotope( 'remove', $(".element"), function(){
	    		// $(".group_topic_filter_container").prepend($(".subcategory-create-button")).isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
			// });
			let group_id = $("#Gr_ID").val();
			let search_text = $("#txtSearch").val();
			let sub_category_id = $('#inp_subcategory_id').val();
			$.ajax({
				url: base_url + 'group/show_group_topics/',
				method: 'POST',
				data: {
					ID: group_id,
					category_id: $('#inp_category_id').val(),
					sub_category_id : sub_category_id ,
					search_text: search_text
				},
				success: function (response) {
					response = $.parseJSON(response);
					console.log("DATa:::::::::::::" , response);
					console.log("LENGTH:::::::::::::" , response.data_items.length);
					let container = $(".group_topic_filter_container");
					let subcontainer;



					  container.empty();
					let subcategory_create_button = $('.subcategory-create-button-clone').clone()
						.removeClass('subcategory-create-button-clone')
						.addClass('subcategory-create-button')
						.show();
				    container.append(subcategory_create_button);
					$counter = 0;
					/*
					Add the topic items here
					*/

					$('#group_topic_filter_container ').css({height: "auto"});
					if(response.data_items.length == 5){
						$('#buttonforMore').show();
					}

					$.each(response['data_items'], function (i, item){
						item_to_sc_id = (item.To_Sc_ID).replace(/ /gi, '_');
						item_to_ca_id = (item.To_Ca_ID).replace(/ /gi, '_');






						subcategory_create_button = $('.group-items-clone').clone()
							.removeClass('group-items-clone')
							.addClass('filter-' + item_to_ca_id + '-' + item_to_sc_id)
							.show();
						subcategory_create_button.find('.group-item-link').attr("href", item.To_Href);
						subcategory_create_button.find('.topic-thumbnail').attr("src", item.To_Thumb);

						subcategory_create_button.find('.board_likespan').html(item.Likes);


						if (item.To_Href == "#") {
							subcategory_create_button.find('.group-item-link').attr({"data-toggle": "modal", "data-target" : item.Target, "data-id" : item.EncodedID});
						}

						if (item.To_Privacy != 'public') {
							subcategory_create_button.find(".privacy-key").show();
						}
						subcategory_create_button.find('.group-item-name').html(item.To_Name);

						if($('#userType').val() == 'superadmin'){

							if(item.To_Pin == 1 ){
								$counter++;
							}
							if(	$counter >= 4){



							}else{
								if(item.To_Pin == 0){

									subcategory_create_button.find('.makepinclass').show();
									subcategory_create_button.find('.unpinedclass').show();
									subcategory_create_button.find('.makepinclassforthumb').show();
									subcategory_create_button.find('.PinnedClass').hide();
									subcategory_create_button.find('.pinclassforthumb').hide();
									subcategory_create_button.find('.makepinclassforthumb').attr({"postitem":item.To_ID});
									subcategory_create_button.find('.makepinclassforthumb').attr({"posttype":item.item_Type});
								}else{
									subcategory_create_button.find('.makepinclass').show();
									subcategory_create_button.find('.PinnedClass').show();
									subcategory_create_button.find('.pinclassforthumb').show();
									subcategory_create_button.find('.makepinclassforthumb').hide();
									subcategory_create_button.find('.unpinedclass').hide();

									subcategory_create_button.find('.pinclassforthumb').attr({"postitem":item.To_ID});
									subcategory_create_button.find('.pinclassforthumb').attr({"posttype":item.item_Type});

								}

							}


						}



						if(item.To_Gr_ID != $("#Gr_ID_decode").val()){

						}else{
							container.append(subcategory_create_button);
						}

					});

					/*
					Add the topic filter when there is a category selected
					*/



				}
			});
			// ViewGroup.refresh_slick();
			// return false;
		});
	},

    init: function() {
    	let inp_category_id = $("#inp_category_id").val();

        ViewGroup.initComponents();
        ViewGroup.initEvents();
        ViewGroup.search_topics_per_category(inp_category_id);

        if (inp_category_id != '')
        	$(".categoryFilter #"+inp_category_id).parent().addClass("selectedCategory");
    },
};

$(document).ready(function(){
    ViewGroup.init();
});
function choosePayment(packagetype){
	// packagetype
	// respond_to_group_invitation
	$.ajax({
		url: base_url + 'group/upgrageplan',
		method: 'POST',
		dataType: 'json',
		data: {
			id: $('#groupID').val(),
			packagetype: packagetype
		},
		success: function (response2) {
				if(response2 == 1){
					Swal.fire({
						icon: 'success',
						title: 'Package upgraded',
						text: 'success',

					});
					location.reload();
				}
		}
	});
}

$(document).ready(function(){


	if ($('#referrer_id').val() != 0) {
		$.ajax({
			url: base_url + 'group/add_to_group_by_link',
			method:'POST',
			dataType: 'json',
			data:{group_id: $('#Gr_ID').val(), referrer: $('#referrer_id').val(), encoded: true},
			success:function(response) {
				if (response.is_pending_invite) {
					Notify.confirm(response.message, function () {
						$.ajax({
							url: base_url + 'group/respond_to_group_invitation',
							method: 'POST',
							dataType: 'json',
							data: {
								id: response.group,
								action: 'accept'
							},
							success: function (response2) {
								Notify.success(response2.message, {
									afterClose : function () {
										window.location = base_url + 'account/view/group/' + response.group;
									}
								});
							}
						});
					}, function () {
						$.ajax({
							url: base_url + 'group/respond_to_group_invitation',
							method: 'POST',
							dataType: 'json',
							data: {
								id: response.group,
								action: 'decline'
							},
							success: function (response2) {
								Notify.success(response2.message, {
									afterClose : function () {
										location.reload();
									}
								});
							}
						});
					});
				} else {
					Notify.success(response.message, {
						afterClose : function () {
							if (response.group) {
								window.location = base_url + 'account/view/group/' + response.group;
							}
						}
					});
				}
			}
		});
	}

	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			console.log(url);
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio:  1200/630,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$.ajax({
					url: base_url + 'group/upload_image',
					method:'POST',
					data:{GroupID: $("input[name='GroupID']").val(), image:base64data},
					success:function(data)
					{
						$modal.modal('hide');
						// $('#uploaded_image').attr('src', base_url + data.trim());
						$(".group-cover-image").css("background", "transparent url('"+base_url + data.trim()+"') no-repeat center center /cover");
						$(".group-cover-image").css("background-size", "100% 100%");
					}
				});
			};
		});
	});
	
});
