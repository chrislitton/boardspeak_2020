var ViewTopic = {
    initEvents : function() {
        $(".group_taggline").shorten();
    	
		$("#btn-request-access").click(function(e){
			ViewGroup.request_grant_access('group');
		});

		$("#btn-request-topic-access").click(function(e){
			ViewGroup.request_grant_access('topic');
		});

		$("#btn-request-post-access").click(function(e){
			ViewGroup.request_grant_access('subtopic');
		});
		$('#groupAccessModal').on('show.bs.modal', function (e) {
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));

		});

		$( "#topicAccessModal" ).on('show.bs.modal', function(e){
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));
		});

		$( "#subtopicAccessModal" ).on('show.bs.modal', function(e){
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));
		});		

		$(".btn-category-chooser").click(function(e) {
			e.preventDefault();
			let category_id = $(this).prop('id');
			$(".categoryFilter .item").removeClass("selectedCategory");

			$('.topic_post_filter_row').show();
			$(this).parent().addClass('selectedCategory');

			$("#inp_category_id").val(category_id);
			ViewTopic.initLoadPosts(category_id);
		});

		$("#btn_search_text").click(function(e) {
			e.preventDefault();
			let inp_category_id = $("#inp_category_id").val();
			ViewTopic.initLoadPosts(inp_category_id);
		});

		// $('#btn_follow').click(function () {
		// 	var data = {"id" : $("#To_ID").val(), "type" : "topic"};
		// 	Notify.confirm('Continue following this topic?', function () {
		// 		$.ajax({
		// 			type: "POST",
		// 			url: base_url + "account/follow/",
		// 			data: data,
		// 			dataType: 'json',
		// 			success: function(result){
		// 				$('#btn_processing').unbind('click');
		// 				$('#btn_processing').attr('id', 'btn_followed').text('Following');
		// 				Notify.success('You are now following this topic', {
		// 					afterClose : function () {
		// 						location.reload();
		// 					}
		// 				});
		//
		// 			}
		// 		});
		// 	}, function () {});
		// });
		$('#btn_follow_without_invite').click(function (){

			var data = {"post_id" : $("#topicID").val(),  "post_role" : 'follower'  , "join_type" : 'group_direct_join'};
			Notify.confirm('Continue following this Topic?', function () {
				$.ajax({
					type: "POST",
					url: base_url + "account/checkuseralreadymemberfotopic/",
					data: data,
					dataType: 'json',
					success: function(response){

						if(response.status == 'success'){
							if(response.type == 'post'){
								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/topic/' + response.id;
									}
								});
							}else if(response.type == 'group'){

								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/group/' + response.id;
									}
								});
							}

						}else {

							Notify.success(response.message, {

							});

						}

					}
				});
			}, function () {});
		})
		$('#btn_join_without_invite').click(function (){
			var data = {"post_id" : $("#topicID").val(),  "post_role" : 'member' , "join_type" : 'group_direct_join'};

			Notify.confirm('Continue join this topic?', function () {
				$.ajax({
					type: "POST",
					url: base_url + "account/checkuseralreadymemberfotopic/",
					data: data,
					dataType: 'json',
					success: function(response){

						if(response.status == 'success'){
							if(response.type == 'post'){
								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/topic/' + response.id;
									}
								});
							}else if(response.type == 'group'){

								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/group/' + response.id;
									}
								});
							}

						}else {

							Notify.success(response.message, {

							});

						}

					}
				});
			}, function () {});


		})
		$('#btn_join').click(function () {
			var data = {"post_id" : $("#topicID").val(), "post_referrer" : $("#post_refererer").val() , "post_role" : $("#post_role").val()};
			Notify.confirm('Continue join this topic?', function () {
				$.ajax({
					type: "POST",
					url: base_url + "account/checkuseralreadymemberfotopic/",
					data: data,
					dataType: 'json',
					success: function(response){

						if(response.status == 'success'){
							if(response.type == 'post'){
								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/topic/' + response.id;
									}
								});
							}else if(response.type == 'group'){

								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/group/' + response.id;
									}
								});
							}

						}else {

							Notify.success(response.message, {

							});

						}

					}
				});
			}, function () {});
		});
		$('#btn_follow').click(function () {

			var data = {"post_id" : $("#topicID").val(), "post_referrer" : $("#post_refererer").val() , "post_role" : $("#post_role").val()};
			Notify.confirm('Continue following this post?', function () {
				$.ajax({
					type: "POST",
					url: base_url + "account/checkuseralreadymemberfotopic/",
					data: data,
					dataType: 'json',
					success: function(response){

						if(response.status == 'success'){
							if(response.type == 'post'){
								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/topic/' + response.id;
									}
								});
							}else if(response.type == 'group'){

								Notify.success(response.message, {
									afterClose : function () {

										window.location = base_url + 'account/view/group/' + response.id;
									}
								});
							}

						}else {

							Notify.success(response.message, {

							});

						}

					}
				});
			}, function () {});
		});

        $(".invite_member_btn").click(function(){

			$(".modal_container").addClass('popped');
			$(".invite_members").addClass('popped');

        });
    },

    initComponents : function() {
        $(".secondery_btn").click(function(){
            // button class slided toggle
            $(this).toggleClass("slided");
            // parent class add
            $(this).parent().toggleClass("slided");
            // secondery menu toggle class
            $(this).next(".secondery_dropdown").toggleClass("slided");
        });

        // hide on mouse leave
        $(".secondery_item").mouseleave(function(){
            $(this).removeClass("slided");
            $(".secondery_btn").removeClass("slided");
            $(".secondery_dropdown").removeClass("slided");
        });
		$('.btn_select_role').click(function() {
			$('.btn_select_role').removeClass('active');

			$(this).addClass('active');
			modifyLink();


		});
		function modifyLink(){
			$finalLink = '';
			$stringOfURl = $('#linkpost').data('url').split('/');

			$stringOfURl[6] = $('.btn_select_role.active').data('role');
			for(var x = 0 ; x < $stringOfURl.length ; x++){

				if(x == $stringOfURl.length -1){
					$finalLink +=	$stringOfURl[x];
				}else{
					$finalLink += $stringOfURl[x]+"/";
				}
			}

			$('.addthis_inline_share_toolbox').data('url', $finalLink).attr('data-url',$finalLink);
			$('#changeInviteLInk').val($finalLink);

		}
		$('#inviceButton').click(function() {
			/* Get the text field */
			var copyText = document.getElementById("changeInviteLInk");

			/* Select the text field */
			copyText.select();
			copyText.setSelectionRange(0, 99999); /* For mobile devices */

			/* Copy the text inside the text field */
			navigator.clipboard.writeText(copyText.value);
			/* Alert the copied text */
			// alert("Copied the text: "  );

			$('.invite_members').removeClass('popped');
			$('.group_info_container ').removeClass('popped');

			Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Link is Copied',
				showConfirmButton: false,
				timer: 3000
			})

		})
        $(".invite_member_btn").click(function(){

			$(".group_info_container").addClass('popped');
			$(".modal_container").addClass('popped');
            $(".invite_members").addClass('popped');

        });

        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".invite_members").removeClass('popped');
        });

        $(".user_role_btn").click(function(){
          $(".modal_container").addClass('popped');
          $(".user_role").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".user_role").removeClass('popped');
        });
        
        // open script
        $(".role_opener").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });
        $(".change_role_btn").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });

        // clsoe script
        $(".role_dropdown").mouseleave(function(){
            $(".role_dropdown").removeClass('dropped');
        });
        $(".role_item").click(function(){
            var value = $(this).html();
            
            var parent = $(this).parents(".role_dropdown");
            var role_opener = $(parent).children(".role_opener");
            
            $(role_opener).html(value)
            
            $(".role_dropdown").removeClass('dropped');
        });
    },

    initLoadPosts : function(inp_category_id) {


		let topic_id = $("#To_ID").val();
		let search_text = $("#txtSearch").val();
		let category_id = $("#inp_category_id").val();
		
		$.ajax({
			url: base_url + 'topic/show_topics_posts/',
			method: 'POST',
			data: {ID: topic_id, category_id: category_id, search_text: search_text},
			success: function (response) {
				response = $.parseJSON(response);

				let container = $(".topic_post_filter_container");
				let subcontainer;

				container.empty();
				let subcategory_create_button = $('.subcategory-create-button-clone').clone()
												.removeClass('subcategory-create-button-clone')
												.addClass('subcategory-create-button')
												.show();
				container.append(subcategory_create_button);
				$counter = 0;
				/*
				Add the posst items here
				*/
				$.each(response['data_items'], function (i, item){
					item_po_sc_id = (item.Po_Sc_ID).replace(/ /gi, '_');
					item_po_ca_id = (item.Po_Ca_ID).replace(/ /gi, '_');

					subcategory_create_button = $('.topic-items-clone').clone()
													.removeClass('topic-items-clone')
													.addClass('filter-' + item_po_ca_id + '-' + item_po_sc_id)
													.show();
					subcategory_create_button.find('.topic-item-link').attr("href", item.Po_Href);
					subcategory_create_button.find('.post-thumbnail').attr("src", item.Po_Thumb);

					if (item.Po_Href == "#") {
						subcategory_create_button.find('.topic-item-link').attr({"data-toggle": "modal", "data-target" : item.Target, "data-id" : item.EncodedID});
					}

					if (item.Po_Privacy != 'public') {
						subcategory_create_button.find(".privacy-key").show();
					}

					subcategory_create_button.find('.topic-item-name').html(item.Po_Title);


					if($('#memberinfo').val() == 'superadmin'){
				console.log("item" , item);


						if(	$counter >= 5){



						}
						else{
							if(item.To_Pin == 0){

								subcategory_create_button.find('.makepinclass').show();
								subcategory_create_button.find('.unpinedclass').show();
								subcategory_create_button.find('.makepinclassforthumb').show();
								subcategory_create_button.find('.PinnedClass').hide();
								subcategory_create_button.find('.pinclassforthumb').hide();
								subcategory_create_button.find('.makepinclassforthumb').attr({"postitem":item.Po_ID});
								// subcategory_create_button.find('.makepinclassforthumb').attr({"posttype":item.item_Type});
								subcategory_create_button.find('.pinclassforthumb').onclick = function(){ makepin(item.Po_ID,'Post'); } ;

							}
							else{
								subcategory_create_button.find('.makepinclass').show();
								subcategory_create_button.find('.PinnedClass').show();
								subcategory_create_button.find('.pinclassforthumb').show();
								subcategory_create_button.find('.makepinclassforthumb').hide();
								subcategory_create_button.find('.unpinedclass').hide();

								subcategory_create_button.find('.pinclassforthumb').onclick = function(){ makeunpin(item.Po_ID,'Post'); } ;
								subcategory_create_button.find('.pinclassforthumb').attr({"postitem":item.Po_ID});


							}

						}

						subcategory_create_button.find('.pinclassforthumb').attr({"posttype":'Post'});
						if(item.To_Pin == 1 ){
							$counter++;
						}

					}
					container.append(subcategory_create_button);
				});

				/*
				Add the post filter when there is a category selected
				*/
				if (category_id !=0) {

					if ($(".topic_post_filter").hasClass('slick-slider'))
						$('.topic_post_filter').slick('unslick');

					topic_filter_container = $(".topic_post_filter");
					topic_filter_container.empty();

					subcategory_item_current = $('.subcategory-item-current-clone').clone()
													.removeClass('subcategory-item-current-clone')
													.show();
					topic_filter_container.append(subcategory_item_current);
					$.each(response['subcategory_items'], function (i, item){
						// item_sc_id = (item.Sc_ID).replace(/ /gi, '_');
						// item_sc_ca_id = (item.Sc_Ca_ID).replace(/ /gi, '_');
						item_sc_id = item.Sc_ID;
						item_sc_ca_id = item.Sc_Ca_ID;
						subcategory_item = $('.subcategory-item-clone').clone()
														.removeClass('subcategory-item-clone')
														.attr('data-filter', '.filter-' + item_sc_ca_id + '-' + item_sc_id)
														.show();
						subcategory_item.find('.subCategory').attr({'dataCat': item_sc_ca_id, 'dataSubCat':item_sc_id});
						subcategory_item.find('.text-primary').html(item.Sc_Name);

						topic_filter_container.append(subcategory_item);
					});
				}

				ViewTopic.refresh_slick();
			}
		});
    }, 

	refresh_slick : function() {
		$(".topic_post_filter").slick({
			dots: false,
			infinite: false,
			slidesToShow: 7,
			slidesToScroll: 3,
			prevArrow: false,
			nextArrow: false,
			variableWidth: true,
			focusOnSelect: true,
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]
		});
			function makepin(){
				$poid = 	poid
				$posttype = type
				Notify.confirm("Are you sure. you want to pin this "+$posttype+"", function () {
					$.ajax({
						url: base_url + 'group/topic_pinning_post',
						method: 'POST',
						dataType: 'json',
						data: {
							post_item:  $poid,
							post_type: $posttype,
							post_decide :  1
						},
						success: function (response2) {
							Notify.success(response2 , {
								afterClose : function () {
									location.reload();
									let inp_category_id = $("#inp_category_id").val();

									ViewTopic.initLoadPosts(inp_category_id);
									// ViewGroup.search_topics_per_category($("#inp_category_id").val());
								}
							});
						}
					});
				}, function () {

				});
			}
			function makeunpin(poid , type){
				$poid = 	poid
				$posttype = type
				Notify.confirm("Are you sure. you want to unpin this "+$posttype+"", function () {
					$.ajax({
						url: base_url + 'group/topic_pinning_post',
						method: 'POST',
						dataType: 'json',
						data: {
							post_item:  $poid,
							post_type: $posttype,
							post_decide :  0
						},
						success: function (response2) {
							Notify.success(response2 , {
								afterClose : function () {
									 location.reload();
									let inp_category_id = $("#inp_category_id").val();

									ViewTopic.initLoadPosts(inp_category_id);
									// ViewGroup.search_topics_per_category($("#inp_category_id").val());
								}
							});
						}
					});
				}, function () {

				});
			}
		$('.makepinclassforthumb').click(function(){
			$poid = 	$(this).attr('postitem')
			$posttype =  $(this).attr('posttype')
			Notify.confirm("Are you sure. you want to pin this "+$(this).attr('posttype')+"", function () {
				$.ajax({
					url: base_url + 'group/topic_pinning_post',
					method: 'POST',
					dataType: 'json',
					data: {
						post_item:  $poid,
						post_type: $posttype,
						post_decide :  1
					},
					success: function (response2) {
						Notify.success(response2 , {
							afterClose : function () {
								location.reload();
								let inp_category_id = $("#inp_category_id").val();

								ViewTopic.initLoadPosts(inp_category_id);
								// ViewGroup.search_topics_per_category($("#inp_category_id").val());
							}
						});
					}
				});
			}, function () {

			});
		});
		$('.pinclassforthumb').click(function(){
			$poid = 	$(this).attr('postitem')
			$posttype =  $(this).attr('posttype')
			Notify.confirm("Are you sure. you want to unpin this "+$(this).attr('posttype')+"", function () {
				$.ajax({
					url: base_url + 'group/topic_pinning_post',
					method: 'POST',
					dataType: 'json',
					data: {
						post_item:  $poid,
						post_type: $posttype,
						post_decide :  0
					},
					success: function (response2) {
						Notify.success(response2 , {
							afterClose : function () {
								// location.reload();
								let inp_category_id = $("#inp_category_id").val();

								ViewTopic.initLoadPosts(inp_category_id);
								 // ViewGroup.search_topics_per_category($("#inp_category_id").val());
							}
						});
					}
				});
			}, function () {

			});
		});
		$('.topic_post_filter .item').click(function(){

			$('.topic_post_filter .item .current').removeClass('current');
			$('.topic_post_filter .item').removeClass('current');
			$(this).addClass('current');
			var selector = $(this).attr('data-filter');

			$('#inp_subcategory_id').val($(this).find('.subCategory').attr('datasubcat'));
			
			$(".subcategory-create-button").removeClass().addClass('col-4 subcategory-create-button').addClass(selector.substring(1));

			// $(".topic_post_filter_container").isotope({
			// 	filter: selector,
			// 	animationOptions: {
			// 		duration: 750,
			// 		easing: 'linear',
			// 		queue: false
			// 	}
			// });
			//
			// $(".topic_post_filter_container").isotope( 'remove', $(".element"), function(){
	    	// 	$(".topic_post_filter_container").prepend($(".subcategory-create-button")).isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });
			// });
			let topic_id = $("#To_ID").val();
			let search_text = $("#txtSearch").val();
			let category_id = $("#inp_category_id").val();
			let sub_category_id = $('#inp_subcategory_id').val();
			$.ajax({
				url: base_url + 'topic/show_topics_posts/',
				method: 'POST',
				data: {ID: topic_id, category_id: category_id, search_text: search_text , sub_category_id : sub_category_id},
				success: function (response) {
					response = $.parseJSON(response);

					let container = $(".topic_post_filter_container");
					let subcontainer;

					container.empty();
					let subcategory_create_button = $('.subcategory-create-button-clone').clone()
						.removeClass('subcategory-create-button-clone')
						.addClass('subcategory-create-button')
						.show();
					container.append(subcategory_create_button);
					$counter = 0;
					/*
					Add the posst items here
					*/
					$.each(response['data_items'], function (i, item){
						item_po_sc_id = (item.Po_Sc_ID).replace(/ /gi, '_');
						item_po_ca_id = (item.Po_Ca_ID).replace(/ /gi, '_');

						subcategory_create_button = $('.topic-items-clone').clone()
							.removeClass('topic-items-clone')
							.addClass('filter-' + item_po_ca_id + '-' + item_po_sc_id)
							.show();
						subcategory_create_button.find('.topic-item-link').attr("href", item.Po_Href);
						subcategory_create_button.find('.post-thumbnail').attr("src", item.Po_Thumb);

						if (item.Po_Href == "#") {
							subcategory_create_button.find('.topic-item-link').attr({"data-toggle": "modal", "data-target" : item.Target, "data-id" : item.EncodedID});
						}

						if (item.Po_Privacy != 'public') {
							subcategory_create_button.find(".privacy-key").show();
						}

						subcategory_create_button.find('.topic-item-name').html(item.Po_Title);


						if($('#memberinfo').val() == 'superadmin'){
							console.log("item" , item);


							if(	$counter >= 5){



							}
							else{
								if(item.To_Pin == 0){

									subcategory_create_button.find('.makepinclass').show();
									subcategory_create_button.find('.unpinedclass').show();
									subcategory_create_button.find('.makepinclassforthumb').show();
									subcategory_create_button.find('.PinnedClass').hide();
									subcategory_create_button.find('.pinclassforthumb').hide();
									subcategory_create_button.find('.makepinclassforthumb').attr({"postitem":item.Po_ID});
									// subcategory_create_button.find('.makepinclassforthumb').attr({"posttype":item.item_Type});
									subcategory_create_button.find('.pinclassforthumb').onclick = function(){ makepin(item.Po_ID,'Post'); } ;

								}
								else{
									subcategory_create_button.find('.makepinclass').show();
									subcategory_create_button.find('.PinnedClass').show();
									subcategory_create_button.find('.pinclassforthumb').show();
									subcategory_create_button.find('.makepinclassforthumb').hide();
									subcategory_create_button.find('.unpinedclass').hide();

									subcategory_create_button.find('.pinclassforthumb').onclick = function(){ makeunpin(item.Po_ID,'Post'); } ;
									subcategory_create_button.find('.pinclassforthumb').attr({"postitem":item.Po_ID});


								}

							}

							subcategory_create_button.find('.pinclassforthumb').attr({"posttype":'Post'});
							if(item.To_Pin == 1 ){
								$counter++;
							}

						}
						container.append(subcategory_create_button);
					});

					/*
					Add the post filter when there is a category selected
					*/
					// if (category_id !=0) {
					//
					// 	if ($(".topic_post_filter").hasClass('slick-slider'))
					// 		$('.topic_post_filter').slick('unslick');
					//
					// 	topic_filter_container = $(".topic_post_filter");
					// 	topic_filter_container.empty();
					//
					// 	subcategory_item_current = $('.subcategory-item-current-clone').clone()
					// 		.removeClass('subcategory-item-current-clone')
					// 		.show();
					// 	topic_filter_container.append(subcategory_item_current);
					// 	$.each(response['subcategory_items'], function (i, item){
					// 		// item_sc_id = (item.Sc_ID).replace(/ /gi, '_');
					// 		// item_sc_ca_id = (item.Sc_Ca_ID).replace(/ /gi, '_');
					// 		item_sc_id = item.Sc_ID;
					// 		item_sc_ca_id = item.Sc_Ca_ID;
					// 		subcategory_item = $('.subcategory-item-clone').clone()
					// 			.removeClass('subcategory-item-clone')
					// 			.attr('data-filter', '.filter-' + item_sc_ca_id + '-' + item_sc_id)
					// 			.show();
					// 		subcategory_item.find('.subCategory').attr({'dataCat': item_sc_ca_id, 'dataSubCat':item_sc_id});
					// 		subcategory_item.find('.text-primary').html(item.Sc_Name);
					//
					// 		topic_filter_container.append(subcategory_item);
					// 	});
					// }

					// ViewTopic.refresh_slick();
				}
			});
			// return false;
		});
		$(".invite_members_closer").click(function(){
			$(".modal_container").removeClass('popped');

			$(".group_info_container").removeClass('popped');
			$(".invite_members").removeClass('popped');
		});

		// $("#btn_create_post").click(function() {
		//
		//
		// 	alert("gan");
		// 			return;
		// 	if($('#memberinfo').val() == 'member' ){
		//
		//
		// 		Swal.fire({
		// 			title:'You have limited access',
		// 			text:  'Only Admins can post. Your role in this group is a '+$('#memberinfo').val() +'.',
 		// 			icon: 'info',
		// 			showCancelButton: true,
		// 			confirmButtonColor: '#3085d6',
		// 			cancelButtonColor: '#d33',
		// 			confirmButtonText: 'Be an Admin'
		// 		}).then((result) =>
		// 		{
		// 			if (result.isConfirmed) {
		// 				Swal.fire(
		// 					'info!',
		// 					'Your request to be an Admin of '+$('#grooupname').text()+' subgroup is pending approval.',
		// 					'info'
		// 				)
		//
		// 				var data = {"topicid" : $("#TopicID").val() };
		//
		// 				$.ajax({
		// 					type: "POST",
		// 					url: base_url + "account/beanadmin/",
		// 					data: data,
		// 					dataType: 'json',
		// 					success: function(result){
		// 						if (result === "success")
		// 							alert('Access has been requested.');
		// 						else
		// 							alert('Request to access '+type+' already exist.');
		//
		// 						$('#'+type+'AccessModal').modal('toggle');
		// 					}
		// 				});
		// 			}
		// 		})
		//
		//
		// 		return ;
		//
		// 	}
		//
		// 	var location = base_url + 'account/create/topicpost/';
		//
		// 	if ($('#To_ID').val() != '') {
		// 		location = location + $('#To_ID').val() + "/";
		//
		// 		if ($('#inp_category_id').val() != '') {
		// 			location = location + $('#inp_category_id').val() + "/";
		//
		// 			if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*')
		// 				location = location + $('#inp_subcategory_id').val();
		// 		}
		// 	}
		//
		// 	window.location.href = location;
		// });
	},

	request_grant_access: function(type) {
		var data = {"ParentID" : $("#group_id").val(), "Parent" : type};

	 	$.ajax({
	 		type: "POST",
			url: base_url + "account/request_grant_access/",
			data: data, 
			dataType: 'json',
			success: function(result){
				if (result === "success")
		    		alert('Access has been requested.');
		    	else
		    		alert('Request to access '+type+' already exist.');

		    	$('#'+type+'AccessModal').modal('toggle');
		  	}
	 	});
	},

    init: function() {

    	let inp_category_id = $("#inp_category_id").val();
        ViewTopic.initComponents();
        ViewTopic.initEvents();
        ViewTopic.initLoadPosts(inp_category_id);

        if (inp_category_id != '')
        	$(".categoryFilter #"+inp_category_id).parent().addClass("selectedCategory");
    }
};

$(document).ready(function(){
    ViewTopic.init();
});

		function getprivatepost(){
			let topic_id = $("#To_ID").val();
			let search_text = $("#txtSearch").val();
			let category_id = $("#inp_category_id").val();

			$.ajax({
				url: base_url + 'topic/show_topics_posts/',
				method: 'POST',
				data: {
					ID: topic_id,
					category_id: category_id,
					search_text: search_text,
					private : 'yes'
				},
				success: function (response) {
					response = $.parseJSON(response);

					let container = $(".topic_post_filter_container");
					let subcontainer;

					container.empty();
					let subcategory_create_button = $('.subcategory-create-button-clone').clone()
						.removeClass('subcategory-create-button-clone')
						.addClass('subcategory-create-button')
						.show();
					container.append(subcategory_create_button);

					/*
					Add the posst items here
					*/
					$.each(response['data_items'], function (i, item){
						item_po_sc_id = (item.Po_Sc_ID).replace(/ /gi, '_');
						item_po_ca_id = (item.Po_Ca_ID).replace(/ /gi, '_');

						subcategory_create_button = $('.topic-items-clone').clone()
							.removeClass('topic-items-clone')
							.addClass('filter-' + item_po_ca_id + '-' + item_po_sc_id)
							.show();
						subcategory_create_button.find('.topic-item-link').attr("href", item.Po_Href);
						subcategory_create_button.find('.post-thumbnail').attr("src", item.Po_Thumb);

						if (item.Po_Href == "#") {
							subcategory_create_button.find('.topic-item-link').attr({"data-toggle": "modal", "data-target" : item.Target, "data-id" : item.EncodedID});
						}

						if (item.Po_Privacy != 'public') {
							subcategory_create_button.find(".privacy-key").show();
						}

						subcategory_create_button.find('.topic-item-name').html(item.Po_Title);
						container.append(subcategory_create_button);
					});

					/*
					Add the post filter when there is a category selected
					*/
					if (category_id !=0) {

						if ($(".topic_post_filter").hasClass('slick-slider'))
							$('.topic_post_filter').slick('unslick');

						topic_filter_container = $(".topic_post_filter");
						topic_filter_container.empty();

						subcategory_item_current = $('.subcategory-item-current-clone').clone()
							.removeClass('subcategory-item-current-clone')
							.show();
						topic_filter_container.append(subcategory_item_current);
						$.each(response['subcategory_items'], function (i, item){
							// item_sc_id = (item.Sc_ID).replace(/ /gi, '_');
							// item_sc_ca_id = (item.Sc_Ca_ID).replace(/ /gi, '_');
							item_sc_id = item.Sc_ID;
							item_sc_ca_id = item.Sc_Ca_ID;
							subcategory_item = $('.subcategory-item-clone').clone()
								.removeClass('subcategory-item-clone')
								.attr('data-filter', '.filter-' + item_sc_ca_id + '-' + item_sc_id)
								.show();
							subcategory_item.find('.subCategory').attr({'dataCat': item_sc_ca_id, 'dataSubCat':item_sc_id});
							subcategory_item.find('.text-primary').html(item.Sc_Name);

							topic_filter_container.append(subcategory_item);
						});
					}

					ViewTopic.refresh_slick();
				}
			});
		}



	function checkpost(){

		if($('#memberinfo').val() == 'member' ){



 			Swal.fire({
				title:'You have limited access',
				text:  'Only Admins can post. Your role in this group is a '+$('#memberinfo').val() +'.',
				icon: 'info',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Be an Admin'
			}).then((result) =>
			{
				if (result.isConfirmed) {
					Swal.fire(
						'info!',
						'Your request to be an Admin of '+$('#grooupname').text()+' subgroup is pending approval.',
						'info'
					)

					var data = {"topicid" : $("#TopicID").val() };

					$.ajax({
						type: "POST",
						url: base_url + "account/beanadmin/",
						data: data,
						dataType: 'json',
						success: function(result){
							if (result === "success")
								alert('Access has been requested.');
							else
								alert('Request to access '+type+' already exist.');

							$('#'+type+'AccessModal').modal('toggle');
						}
					});
				}
			})


			return ;

		}

		var location = base_url + 'account/create/topicpost/';

		if ($('#To_ID').val() != '') {
			location = location + $('#To_ID').val() + "/";

			if ($('#inp_category_id').val() != '') {
				location = location + $('#inp_category_id').val() + "/";

				if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*')
					location = location + $('#inp_subcategory_id').val();
			}
		}

		window.location.href = location;
	}


$(document).ready(function(){

	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			console.log(url);
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio:  1200/630,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){



				var base64data = reader.result;
				$.ajax({
					url: base_url + 'topic/upload_image',
					method:'POST',
					data:{GroupID: $('#topicID').val(), image:base64data},
					success:function(data)
					{
						$modal.modal('hide');
						// $('#uploaded_image').attr('src', base_url + data.trim());
						$(".topic-cover-image").css("background", "transparent url('"+base_url + data.trim()+"') no-repeat center center /cover");
						$(".topic-cover-image").css("background-size", "100% 100%");
					}
				});
			};
		});
	});
	
});
