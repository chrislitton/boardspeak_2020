var UpdateTopic = {
	initComponents : function() {
		$('.custom-select').select2();
        $(".group_taggline").shorten();

        $('#txtKeywords').on('beforeItemAdd', function(event) {
            let keyword = event.item;

            $.ajax({
                type: 'POST',
                url: base_url + 'account/validate_keyword/',
                data : {keyword: keyword},
            }).done(function(data) {
                data = $.parseJSON(data);
                if (data) {
                    if (data.Kw_Banned == 1) {
                        $('#txtKeywords').tagsinput('remove', event.item);
                    }
                }
            });
        });
	},
	manageAllUsers: function (show_empty, role_tab) {
		UpdateTopic.populateuserList('', 'all_members', show_empty, role_tab);
	},

	manageSuperAdminUsers: function (show_empty, role_tab) {
		UpdateTopic.populateuserList('superadmin', 'super_admin', show_empty, role_tab);
	},
	manageAdminUsers: function (show_empty, role_tab) {
		UpdateTopic.populateuserList('admin', 'admin', show_empty, role_tab);
	},
	manageMemberUsers: function (show_empty, role_tab) {
		UpdateTopic.populateuserList('member', 'member', show_empty, role_tab);
	},
	manageFollowerUsers: function (show_empty, role_tab) {

		UpdateTopic.populateuserList('follower', 'follower', show_empty, role_tab);
	},
	manageNoRoleUsers: function (show_empty, role_tab) {
		UpdateTopic.populateuserList('', 'no_role', show_empty, role_tab);
	},
	managePendingUsers: function (show_empty, role_tab) {
		UpdateTopic.populateuserList('pending', 'pending', show_empty, role_tab);
	},
	populateuserList: function (role, list, show_empty, role_tab) {

		$('.user_list').empty();


		var filter_keyword = $('#user_filter').val();
		$.ajax({
			url: base_url + 'post/show_group_members/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: 0,
				topicdata: 'mk',
				pid: $('#topicID').val(),
				keyword: filter_keyword,
				role: role
			},
			success: function (response) {
				if (response.totalRecords == 0) {
					if (show_empty) {
						$item = $('.no_result_template').clone()
							.removeClass('no_result_template')
							.addClass('group_user_item')
							.attr('id', 'group_user_0')
							.show();
						$('#' + list).append($item);
					}
				} else {
					$.each(response.data, function(key, user) {
						if (user.Me_Role != "") {
							if (user.Me_Status != 'pending') {
								$item = $('.has_role_template').clone()
									.removeClass('has_role_template')
									.addClass('group_user_item')
									.attr('id', 'group_user_' + user.Me_Us_ID)
									.show();
							} else {
								$item = $('.pending_role_template').clone()
									.removeClass('pending_role_template')
									.addClass('group_user_item')
									.attr('id', 'group_user_' + user.Me_Us_ID)
									.show();
							}

						} else {
							$item = $('.no_role_template').clone()
								.removeClass('has_role_template')
								.addClass('group_user_item')
								.attr('id', 'group_user_' + user.Me_Us_ID)
								.show();
						}
						$('#' + list).append($item);



						$('#group_user_' + user.Me_Us_ID).find('.user_name_img img').attr('src', user.Us_Thumb);
						$('#group_user_' + user.Me_Us_ID).find('.user_name_img p .member_name').text(user.Us_Name);
						$('#group_user_' + user.Me_Us_ID).find('.user_name_img p .member_job').text(user.Us_JobTitle);
						if(user.Creator == 1 ){

							$thisx = 	$('#group_user_' + user.Me_Us_ID + ' .role_dropdown');
							$thisx.hide();

						}else {

							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown').attr('id', 'role_dropdown_' + user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').text(user.Rl_Type);
							$('#group_user_' + user.Me_Us_ID).find('.pending_action a.role_opener').text(user.Rl_Type);

							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').attr('data-id', user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown button.change_role_btn').attr('data-id', user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_secondery').attr('id', 'role_option_' + user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown .role_trigger').removeClass('role_trigger').addClass('role_trigger_' + user.Me_Us_ID);
							if (user.Me_Role == 'superadmin') {

								if (user.merole == 0) {
									if (user.Me_Us_ID == $('#userID').val()) {


									} else {
										$thisxz = $('#group_user_' + user.Me_Us_ID + ' .change_role_btn');
										$thisxz.hide();
										$thisxzz = $('#group_user_' + user.Me_Us_ID + ' .fa-angle-down');
										$thisxzz.hide();

									}


								}


							}
						}
						// $('#group_user_' + user.Me_Us_ID).find('.role_dropdown').attr('id', 'role_dropdown_' + user.Me_Us_ID);
						// $('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').text(user.Rl_Type);
						// $('#group_user_' + user.Me_Us_ID).find('.pending_action a.role_opener').text(user.Rl_Type);
						//
						// $('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').attr('data-id', user.Me_Us_ID);
						// $('#group_user_' + user.Me_Us_ID).find('.role_dropdown button.change_role_btn').attr('data-id', user.Me_Us_ID);
						// $('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_secondery').attr('id', 'role_option_' + user.Me_Us_ID);
						// $('#group_user_' + user.Me_Us_ID).find('.role_dropdown .role_trigger').removeClass('role_trigger').addClass('role_trigger_' + user.Me_Us_ID);

						$.ajax({
							url: base_url + 'group/show_roles_for_dropdown/',
							method: 'POST',
							dataType: 'json',
							data: {
								except : user.Me_Role
							},
							success: function (response) {
								$.each(response.data, function(key, role) {

									console.log('role', role);
									if(role.Rl_Code == "superadmin" ||   role.Rl_Code == "creator"  ){


									}
										else
									{
										$item = '<a class="role_item" data-from="'+role_tab+'" data-role="'+role.Rl_Code+'" data-user="'+user.Me_Us_ID+'">'+role.Rl_Label+'</a>';

										if(user.Me_Role != 'superadmin' || user.Me_Role != 'creator'){
											$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
										}

									}


								});

								if(user.Me_Role == 'superadmin'){

								}else{
									$item = '<a class="group_remove" data-from="'+role_tab+'" data-user="'+user.Me_Us_ID+'">Remove from the SubGroup</a>';
									$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
								}

							}
						});


						// $role_action_items = '<a  class="shownbutton" data-name="'+user.Us_Name+' "  data-id="'+user.Me_Us_ID+'">Check Q & A </a>';
						$role_action_items = '<a class="group_action" data-from="'+role_tab+'"  data-nofromtype="'+user.No_From_Type+'" data-notificationtype="'+user.notificationType+'" data-notification="'+user.notificationID+'" data-action="approve" data-user="'+user.Me_Us_ID+'">Approve</a>'
							+'<a class="group_action" data-from="'+role_tab+'" data-notification="'+user.notificationID+'"  data-nofromtype="'+user.No_From_Type+'" data-notificationtype="'+user.notificationType+'"  data-action="reject" data-user="'+user.Me_Us_ID+'">Reject</a>';
						$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_action').append($role_action_items);



						$(".role_trigger_" + user.Me_Us_ID).click(function(){
							$('#group_user_' + user.Me_Us_ID).find('#role_dropdown_' + user.Me_Us_ID).toggleClass('dropped');
						});
						$('#role_option_' + user.Me_Us_ID).mouseleave(function(){
							$('#group_user_' + user.Me_Us_ID).find('#role_dropdown_' + user.Me_Us_ID).removeClass('dropped');
						});
						$('#group_user_' + user.Me_Us_ID).find('.user_name_img').on('click', function() {
							location.href = user.Us_URL;
						});
					});
				}
			}
		});
	},
	initEvents : function() {

        $('#selCategory').on('change', function (e) {

        	if ($(this).prop("disabled") == false) {
				if ($(this).val() == 0) {
					$('#selNewCategoryBox').show();
					$('#txtNewCategory').prop("disabled", false);
					$('#txtNewCategory').prop("required", true);
				}				
				else {
					$('#selNewCategoryBox').hide();
					$('#txtNewCategory').prop("disabled", true);
					$('#txtNewCategory').prop("required", false);
				}
			}

		    UpdateTopic.initLoadSubCategory($(this).val());
		});       
        $('#selCategory').trigger('change');
		$(".role_option").click(function(){
			UpdateTopic.totalRecord = 0;
			var role_tab = $(this).attr('role_tab_id');
			$(".role_option").removeClass("active");
			$(this).addClass("active");


			if (role_tab == "all_members") {
				$(".suggest_members").show();
				UpdateTopic.manageAllUsers(true, role_tab);
			}else{
				$(".suggest_members").hide();
				$("#" + role_tab).show();
				if (role_tab == "super_admin") {
					UpdateTopic.manageSuperAdminUsers(true, role_tab);
				} else if (role_tab == "admin") {
					UpdateTopic.manageAdminUsers(true, role_tab);
				} else if (role_tab == "member") {
					UpdateTopic.manageMemberUsers(true, role_tab);
				} else if (role_tab == "follower") {
					UpdateTopic.manageFollowerUsers(true, role_tab);
				} else {
					UpdateTopic.managePendingUsers(true, role_tab);
				}
			}
		});
        $('#selSubCategory').on('change', function (e) {        	
			if ($(this).prop("disabled")) {
				$('#txtNewSubCategory').prop("disabled", true);
			}
			else {
				if ($(this).val() == '0') {
					$('#selNewSubCategoryBox').show();
					$('#txtNewSubCategory').prop("disabled", false);
					$('#txtNewSubCategory').prop("required", true);
				}				
				else {
					$('#selNewSubCategoryBox').hide();
					$('#txtNewSubCategory').prop("disabled", true);
					$('#txtNewSubCategory').prop("required", false);
				}
			}
		});
        $(".secondery_btn").click(function(){
            // button class slided toggle
            $(this).toggleClass("slided");
            // parent class add
            $(this).parent().toggleClass("slided");
            // secondery menu toggle class
            $(this).next(".secondery_dropdown").toggleClass("slided");
        });

        // hide on mouse leave
        $(".secondery_item").mouseleave(function(){
            $(this).removeClass("slided");
            $(".secondery_btn").removeClass("slided");
            $(".secondery_dropdown").removeClass("slided");
        });

        $(".invite_member_btn").click(function(){

          $(".modal_container").addClass('popped');
          $(".invite_members").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".invite_members").removeClass('popped');
        });

        $(".user_role_btn").click(function(){
          $(".modal_container").addClass('popped');
          $(".user_role").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".user_role").removeClass('popped');
        });
        
        // open script
        $(".role_opener").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });
        $(".change_role_btn").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });

        // clsoe script
        $(".role_dropdown").mouseleave(function(){
            $(".role_dropdown").removeClass('dropped');
        });
        $(".role_item").click(function(){
            var value = $(this).html();
            
            var parent = $(this).parents(".role_dropdown");
            var role_opener = $(parent).children(".role_opener");
            
            $(role_opener).html(value)
            
            $(".role_dropdown").removeClass('dropped');
        });
	},

    init: function() {
        UpdateTopic.initComponents();
        UpdateTopic.initEvents();
        UpdateTopic.load_keywords();
    },

	load_keywords : function() {

        var keywords_list = new Bloodhound({
            datumTokenizer: function(datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                wildcard: '%QUERY',
                url: base_url + "account/get_keywords_list/%QUERY",
                    transform: function(response) {
                        return $.map(response, function(words) {
                            return { value: words['name'] };
                        });
                    }
            }
        });
        keywords_list.initialize();

        $('#txtKeywords').tagsinput({
            trimValue: true,
            typeaheadjs: {
                name: 'keywords_list',
                displayKey: 'value',
                valueKey: 'value',
                source: keywords_list.ttAdapter()
            }
        });

        $(".tt-input").bind("paste", function(e) {
            let keyword = e.originalEvent.clipboardData.getData('text');
            $(this).prop('size', keyword.length);
        });
	},

    initLoadSubCategory: function(categoryID) {
		$.ajax({
			type: 'POST',
			url: base_url + 'account/get_subcategory/',
			data : {CatID: categoryID},
			dataType: 'json'
		}).done(function(data) {


			$("#selSubCategory").empty();
			$("#selSubCategory").select2({placeholder: 'Choose a subcategory', data: data});

			if (UpdateTopic.initial_load_subcategory == 0) {
				if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
					$('#selSubCategory').val($("#inp_subcategory_id").val());
				}

				UpdateTopic.initial_load_subcategory = 1;
			} else {
                let val = $("#selSubCategory").find("option:contains('Uncategorized')").val();

                if (val !== undefined) {
                    $('#selSubCategory').val(val)
                } else {
                    $('#selTopic').prop('selectedIndex', 0);
                }
			}

			$('#selSubCategory').trigger('change');
		});
    },
}
$(document).on('click', '.role_item', function() {
	var user_id = $(this).data('user');
	var new_role = $(this).data('role');
	var new_role_name = $(this).text().replace('Make ', '');
	var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
	var from = $(this).data('from');
	Notify.confirm('Are you sure you want to change role of '+name+' to '+new_role_name+'?', function () {
		$.ajax({
			url: base_url + 'group/assign_group_role/',
			method: 'POST',
			dataType: 'json',
			data: {
				user_id: user_id,
				type_id: $('#topicID').val(),
				post_id: '',
				role: new_role,
				type: 'topic',
			},
			success: function (response) {
				Notify.success('Role assignment successful!', {
					afterClose : function () {
						$('#tab_' + from).trigger('click');
					}
				});
			}
		});
	}, function () {});
});

$(document).on('click', '.group_action', function() {

	var user_id = $(this).data('user');
	var from = $(this).data('from');
	var action = $(this).data('action');
	var notification_ID = $(this).data('notification');

	var notificationType = $(this).data('notificationtype');
	var nofromtype = $(this).data('nofromtype');

	let data = {};
	if(notificationType == 'topic_join_via_be_a_member' || notificationType == 'topic_join_via_link'){
		data = {
			user_id: user_id,
			type_id: $('#topicID').val(),
			action: action,
			topicsec: action,
			notification_ID: notification_ID,
			postinvite : nofromtype,
			notype : notificationType
		}

	}else{
		data = {
			user_id: user_id,
			type_id: $('#topicID').val(),
			action: action,
			topicsec: action,
			notification_ID: notification_ID,
			notype : notificationType
		}

	}
	var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
	Notify.confirm('Continue to '+action+' request from '+name+' to join the topic?', function () {

		$.ajax({
			url: base_url + 'group/approve_reject_join_request/',
			// url: base_url+ 'group/approve_reject_join_request_from_notification',
			method: 'POST',
			dataType: 'json',
			data: data,
			success: function (response) {
				Notify.success(response.message, {
					afterClose : function () {
						$('#tab_' + from).trigger('click');
					}
				});
			}
		});
	}, function () {});
});
$(document).on('click', '.group_remove', function() {
	var user_id = $(this).data('user');
	var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
	var from = $(this).data('from');
	Notify.confirm('Continue removing '+name+' from the post?', function () {
		$.ajax({
			url: base_url + 'group/remove_group_user/',
			method: 'POST',
			dataType: 'json',
			data: {
				user_id: user_id,
				type_id:  $('#topicID').val(),
				postype : "topic",
				post_id : 0
			},
			success: function (response) {
				Notify.success('User removed successfully!', {
					afterClose : function () {
						$('#tab_' + from).trigger('click');
					}
				});
			}
		});
	}, function () {});
});
$(document).ready(function(){
    UpdateTopic.init();
	$('#btnUserSearch').click(function() {
		UpdateTopic.totalRecord = 0;
		var role_tab = $('.role_option.active').attr('role_tab_id');

		$('#tab_' + role_tab).trigger('click');
	})


	$("#post-info").click(function(){
		$(".nav-link").removeClass("active");
		$(".nav-container").hide();
		$("#post-info").addClass("active");
		$("#topic-info-container").show();
	});

	$("#manage-user").click(function(){
		$(".nav-link").removeClass("active");
		$(".nav-container").hide();
		$("#manage-user").addClass("active");
		$("#manage-user-container").show();

		UpdateTopic.totalRecord = 0;
		$('#user_filter').val("");
		$('#tab_all_members').trigger('click');

		$("#manage_user_container").show();
	});
});


$(document).ready(function(){

	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio:  1200/630,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$.ajax({
					url: base_url + 'topic/upload_image',
					method:'POST',
					data:{TopicID: $("input[name='TopicID']").val(), image:base64data},
					success:function(data)
					{
						$modal.modal('hide');
						// $('#uploaded_image').attr('src', base_url + data.trim());
						$(".topic-cover-image").css("background", "transparent url('"+base64data+"') no-repeat center center /cover");
						$(".topic-cover-image").css("background-size", "100% 100%");
					}
				});
			};
		});
	});
	
});
