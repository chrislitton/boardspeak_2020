let alluser ;
let totaluserlist ;
function removingfromtheGroup(){

	var radioValue = $("input[name='adminname']:checked").val();
	if (typeof $("input[name='adminname']:checked").val() === "undefined") {
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Please select one of the user',

		});
	}else {

		let userid = 	radioValue;
		let  user_id = $('#previousID').val();
 		var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
		var from = $(this).data('from');
		Notify.confirm('Continue removing   from the group?', function () {
			$.ajax({
				url: base_url + 'group/remove_group_user/',
				method: 'POST',
				dataType: 'json',
				data: {
					user_id: user_id,
					type_id: $('#groupID').val(),
					updatepersonId : userid
				},
				success: function (response) {
					Notify.success('User removed successfully!', {
						afterClose : function () {
							$('#myModalforadmin').modal('hide');
							$('#tab_' + from).trigger('click');
						}
					});
				}
			});
		}, function () {});
	}


}
function changeRoleofSuperAdmin(){

	var radioValue = $("input[name='adminname']:checked").val();
	if (typeof $("input[name='adminname']:checked").val() === "undefined") {
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Please select one of the user',

		})
	}else{
		let new_role = $('#newwrole').val();
		let userid = 	radioValue;
		let 	user_id = $('#previousID').val();

		if(new_role == 'leave'){
			$text = 'Are you sure you want to leave the group';
		}else{
			$text = 'Are you sure you want to change role to '+new_role+'?';
		}
		Notify.confirm($text, function () {
			$.ajax({
				url: base_url + 'group/assign_group_role/',
				method: 'POST',
				dataType: 'json',
				data: {
					user_id: user_id,
					type_id: $('#groupID').val(),
					role: new_role,
					type: 'group',
					updatetouser: userid,
				},
				success: function (response) {
					if(new_role ==  'creator') {
						Notify.success('Creator Role Request has been sent to '+name, {
							afterClose : function () {


							}
						});
					}else

					if(new_role == 'leave'){
						$texta = 'Successfully left the group';
					}else{
						$texta = 'Role assignment successful!';
					}
						Notify.success($texta, {
							afterClose : function () {
								if(new_role == 'leave'){
									location.replace(base_url);
								}else{
									$('#myModalforadmin').modal('hide');
									$('#btnUserSearch').click();
								}
								}


						});
					}


			});
		}, function () {});
	}

	return false;
}
var UpdateGroup = {
    totalRecord: 0,
	initComponents : function() {
        $('.custom-select').select2();

        $(".group_taggline").collapser({
            mode:'lines',
            truncate: 1,
            showText: '[Show More]',
            hideText: '[Show Less]',
            changeText: true
        });

        $('#txtKeywords').on('beforeItemAdd', function(event) {
            let keyword = event.item;

            $.ajax({
                type: 'POST',
                url: base_url + 'account/validate_keyword/',
                data : {keyword: keyword},
            }).done(function(data) {
                data = $.parseJSON(data);
                if (data) {
                    if (data.Kw_Banned == 1) {
                        $('#txtKeywords').tagsinput('remove', event.item);
                    }
                }
            });
        });   

        $("#group_info_tab").click(function(){
            $(".nav-link").removeClass("active");
            $(".nav-container").hide();
			$("#managed_tab").removeClass("active");
			$("#manage_user_container").hide();
            $("#group_info_tab").addClass("active");
            $("#group_info_tab-container").show();
        });




		$("#reward_tab").click(function(){

			$(".nav-link").removeClass("active");
			$(".nav-container").hide();
			try {
				$("#managed_tab").removeClass("active");
				$("#manage_user_container").hide();
			}catch (e) {

			}
			try{
				$("#group_info_tab").removeClass("active");
				$("#group_info_tab-container").hide();
			}catch (e) {

			}
			$("#reward_tab").addClass("active");
			$("#reward_tab_container").show();
		});

        $("#member_tab").click(function(){
            $(".nav-link").removeClass("active");
            $(".nav-container").hide();
			try {
				$("#managed_tab").removeClass("active");
				$("#manage_user_container").hide();
			}catch (e) {
				
			}
			try{
				$("#group_info_tab").removeClass("active");
				$("#group_info_tab-container").hide();
			}catch (e) {
				
			}
            $("#member_tab").addClass("active");
            $("#member_tab-container").show();
        });

		$("#managed_tab").click(function(){
			$(".nav-link").removeClass("active");
			$(".nav-container").hide();
			$("#group_info_tab").removeClass("active");
			$("#group_info_tab-container").hide();
			$("#member_tab").removeClass("active");
			$("#member_tab-container").hide();
			UpdateGroup.totalRecord = 0;
			$('#user_filter').val("");
			$('#tab_all_members').trigger('click');
			$("#managed_tab").addClass("active");
			$("#manage_user_container").show();
		});


	},
	initEvents : function() {

        $('#selCategory').on('change', function (e) {
            if ($(this).val() == 0) {
                $('#selNewCategoryBox').show();
                $('#txtNewCategory').prop("disabled", false);
                $('#txtNewCategory').prop("required", true);
            }               
            else {
                $('#selNewCategoryBox').hide();
                $('#txtNewCategory').prop("disabled", true);
                $('#txtNewCategory').prop("required", false);
            }

            UpdateGroup.initLoadSubCategory($(this).val());
        });

        $('#selSubCategory').on('change', function (e) {

            if ($(this).val() =='0') {
                $('#selNewSubCategoryBox').show();
                $('#txtNewSubCategory').prop("disabled", false);
                $('#txtNewSubCategory').prop("required", true);
            }               
            else {
                $('#selNewSubCategoryBox').hide();
                $('#txtNewSubCategory').prop("disabled", true);
                $('#txtNewSubCategory').prop("required", false);
            }
        });

        $(".user_role_btn").click(function(){
 		   $(".group_info_container").addClass('popped');
            $(".user_role").addClass('popped');
 		    UpdateGroup.totalRecord = 0;
            $('#user_filter').val("");
            $('#tab_all_members').trigger('click');
        });

        $(".invite_member_btn").click(function(){
            $(".group_info_container").addClass('popped');
            $(".invite_members").addClass('popped');
            $('#userInviteKey').val("");
              $('#btnUserInviteSearch').trigger('click');
        });

        $(".role_permission_btn").click(function(){
            $(".group_info_container").addClass('popped');
            $(".role_permission").addClass('popped');
            var role = $('.role_permission_option.active').attr('role_tab_id');
            UpdateGroup.manageRolePermissions(role);
        });

        $(".role_option").click(function(){
            UpdateGroup.totalRecord = 0;
            var role_tab = $(this).attr('role_tab_id');
            $(".role_option").removeClass("active");
            $(this).addClass("active");
            
            if (role_tab == "all_members") {
                $(".suggest_members").show();
                UpdateGroup.manageAllUsers(true, 'role_tab');
            }else{
                $(".suggest_members").hide();
                $("#" + role_tab).show();
                if (role_tab == "super_admin") {
                    UpdateGroup.manageSuperAdminUsers(true, role_tab);
                } else if (role_tab == "admin") {
                    UpdateGroup.manageAdminUsers(true, role_tab);
                } else if (role_tab == "member") {
                    UpdateGroup.manageMemberUsers(true, role_tab);
                } else if (role_tab == "follower") {
                    UpdateGroup.manageFollowerUsers(true, role_tab);
                } else {
                    UpdateGroup.managePendingUsers(true, role_tab);
                }
            }
        });

        $(".invite_members_closer").click(function(){
            $(".group_info_container").removeClass('popped');
            $(".user_role").removeClass('popped');
            $(".invite_members").removeClass('popped');
            $(".role_permission").removeClass('popped');
        });

        $(".role_permission_closer").click(function(){
            $(".group_info_container").removeClass('popped');
            $(".user_role").removeClass('popped');
            $(".invite_members").removeClass('popped');
            $(".role_permission").removeClass('popped');
        });

        $(document).on('click', '.role_item', function() {
            var user_id = $(this).data('user');

            var new_role = $(this).data('role');
				$('#newwrole').val(new_role);
			$('#previousID').val(user_id);
            var new_role_name = $(this).text().replace('Make ', '');
            var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
            var from = $(this).data('from');
			if($(this).data('roleold') == 'superadmin' && new_role != 'creator'){
				$('#updateuser').show();
				$('#deleteuser').hide();
 				$string = '';
				$('#adminuserList').empty();
					for(var x = 0 ;  x < alluser.length; x++){
						if(alluser[x]['Me_Role'] == 'superadmin' && alluser[x]['Me_Us_ID'] != user_id ){

							$string +=  ' <li class="list-group-item">\n' +
								' <div class="custom-control custom-radio">\n' +
								'  <input type="radio" name="adminname" value="'+alluser[x]['Me_Us_ID']+'" class="custom-control-input" id="check'+alluser[x]['Me_Us_ID']+'">\n' +
								' <label class="custom-control-label" for="check'+alluser[x]['Me_Us_ID']+'">'+alluser[x]['Us_Name'] + alluser[x]['Us_JobTitle'] +'</label>\n' +
								' </div>\n' +
								' </li>\n' ;
						}

					}

				$('#adminuserList').append($string);
				$('#textshow').text("To relinquish Superadmin privileges for the Posts and Topics "+name+" created, please choose among the following users");

				$('#myModalforadmin').modal('show');

			return;
			}

			if(new_role !=  'creator'  ){

				Notify.confirm('Are you sure you want to change role of '+name+' to '+new_role_name+'?', function () {
					$.ajax({
						url: base_url + 'group/assign_group_role/',
						method: 'POST',
						dataType: 'json',
						data: {
							user_id: user_id,
							type_id: $('#groupID').val(),
							role: new_role,
							type: 'group',
						},
						success: function (response) {
							if(new_role ==  'creator') {
								Notify.success('Creator Role Request has been sent to '+name, {
									afterClose : function () {
										$('#btnUserSearch').click();

									}
								});
							}else{
								Notify.success('Role assignment successful!', {
									afterClose : function () {
										$('#btnUserSearch').click();


									}
								});
							}

						}
					});
				}, function () {});

			}else{

 				Swal.fire({
					title: 'Warning',
					text: "Please confirm that you would like to assign a new Creator of ``"+$('#groupname').text()+"``. Assigning a new Creator will relinquish your superior rights and ownership of this group. ",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonText:'Confirm',
					cancelButtonColor:  '#d33',
					confirmButtonColor:  '#3085d6',
					cancelButtonText: 'Cancel'
				}).then((result) => {
					console.log(result);
					if (result.isConfirmed) {

						if($('#packagedetail').val() == 1){
							$.ajax({
								url: base_url + 'account/checkUserGroupValidation/',
								method: 'POST',
								dataType: 'json',
								data: {
									user_id: user_id,

								},
								success: function (response) {
									console.log(response);
									$found = false;
									// let data  = JSON.parse(response);
									for(var x = 0; x < response.length; x++){
										if(response[x]['Gr_packegtype'] == 1){
											$found = true;

										}

									}

									if($found){

										Swal.fire({
											title: 'info',
											text: name+" You want to assign as a new owner of the "+$('#groupname').text()+" has already availed  Free Plan (Limit of one group only)  ",
											icon: 'info',
											showCancelButton: true,
											confirmButtonText:'Upgrade current Group Plan',
											cancelButtonColor:  '#d33',
											confirmButtonColor:  '#3085d6',
											cancelButtonText: 'Assign Group Ownership to a different user'
										}).then((result) => {
											if (result.isConfirmed) {

											}

										});
									}
									else{
										$.ajax({
											url: base_url + 'group/assign_group_role/',
											method: 'POST',
											dataType: 'json',
											data: {
												user_id: user_id,
												type_id: $('#groupID').val(),
												role: new_role,
												type: 'group',
											},
											success: function (response) {
												if(new_role ==  'creator') {
													Notify.success('Creator role request has been sent to '+name, {
														afterClose : function () {
															$('#btnUserSearch').click();
														}
													});
												}else{
													Notify.success('Role assignment successful!', {
														afterClose : function () {

															// if(new_role ==  'creator'){
															$('#btnUserSearch').click();
															// $('#tab_' + from).trigger('click');
															// }

														}
													});
												}

											}
										});
									}
								}





							});
						}else{

							$.ajax({
								url: base_url + 'group/assign_group_role/',
								method: 'POST',
								dataType: 'json',
								data: {
									user_id: user_id,
									type_id: $('#groupID').val(),
									role: new_role,
									type: 'group',
								},
								success: function (response) {
									if(new_role ==  'creator') {
										Notify.success('Creator role request has been sent to '+name, {
											afterClose : function () {
												$('#btnUserSearch').click();
											}
										});
									}else{
										Notify.success('Role assignment successful!', {
											afterClose : function () {

												// if(new_role ==  'creator'){
												$('#btnUserSearch').click();
												// $('#tab_' + from).trigger('click');
												// }

											}
										});
									}

								}
							});

						}




					}else{

					}
				})

			}

        });

        $(document).on('click', '.group_remove', function() {
            var user_id = $(this).data('user');
            var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
            var from = $(this).data('from');


			if($(this).data('roleold') == 'superadmin'  ){
					$('#updateuser').hide();
				$('#deleteuser').show();
				$('#previousID').val(user_id);
				 $('#textshow').text("To relinquish Superadmin privileges for the Posts and Topics "+name+" created, please choose among the following users");

				$string = '';
				$('#adminuserList').empty();
				for(var x = 0 ;  x < alluser.length; x++){
					if(alluser[x]['Me_Role'] == 'superadmin' && alluser[x]['Me_Us_ID'] != user_id ){

						$string +=  ' <li class="list-group-item">\n' +
							' <div class="custom-control custom-radio">\n' +
							'  <input type="radio" name="adminname" value="'+alluser[x]['Me_Us_ID']+'" class="custom-control-input" id="check'+alluser[x]['Me_Us_ID']+'">\n' +
							' <label class="custom-control-label" for="check'+alluser[x]['Me_Us_ID']+'">'+alluser[x]['Us_Name'] + alluser[x]['Us_JobTitle'] +'</label>\n' +
							' </div>\n' +
							' </li>\n' ;
					}

				}

				$('#adminuserList').append($string);
				$('#textshow').text("To relinquish Superadmin privileges for the Posts and Topics "+name+" created, please choose among the following users");

				$('#myModalforadmin').modal('show');

				return;
			}

            Notify.confirm('Continue removing '+name+' from the group?', function () {
                $.ajax({
                    url: base_url + 'group/remove_group_user/',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        user_id: user_id,
                        type_id: $('#groupID').val()
                    },
                    success: function (response) {
                        Notify.success('User removed successfully!', {
							afterClose : function () {
								$('#tab_' + from).trigger('click');
							}
						});
                    }
                });
            }, function () {});
        });





		$(document).on('click', '.shownbutton', function() {

			var id = $(this).data('id');
			var name = $(this).data('name');
			var groupid = $('#groupID').val();

			showGiveAnswer1(name , id , groupid)
		});

        $(document).on('click', '.group_action', function() {

            var user_id = $(this).data('user');
            var from = $(this).data('from');
            var action = $(this).data('action');
			var notification_ID = $(this).data('notification');


			var notificationType = $(this).data('notificationtype');
			var nofromtype = $(this).data('nofromtype');

			let data = {};
			if(notificationType == 'post_join_via_be_a_member'){
				data = {
					user_id: user_id,
					type_id: $('#groupID').val(),
					action: action,
					notification_ID: notification_ID,
					postinvite : nofromtype,
					notype : notificationType
				}

			}else{
				data = {
					user_id: user_id,
					type_id: $('#groupID').val(),
					action: action,
					notification_ID: notification_ID,
					notype : notificationType
				}

			}
            var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
            Notify.confirm('Continue to '+action+' request from '+name+' to join the group?', function () {

                $.ajax({
                    url: base_url + 'group/approve_reject_join_request/',
					// url: base_url+ 'group/approve_reject_join_request_from_notification',
                    method: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (response) {
                        Notify.success(response.message, {
							afterClose : function () {
								$('#btnUserSearch').click();
								// $('#tab_' + from).trigger('click');
							}
						});
                    }
                });
            }, function () {});
        });

        $('#btnUserSearch').click(function() {
            UpdateGroup.totalRecord = 0;
            var role_tab = $('.role_option.active').attr('role_tab_id');

            $('#tab_' + role_tab).trigger('click');
        })

        $('.btn_select_role').click(function() {
            $('.btn_select_role').removeClass('active');

			$(this).addClass('active');
			modifyLink();


        });

		$('#inviceButton').click(function() {
			/* Get the text field */
			var copyText = document.getElementById("changeInviteLInk");

			/* Select the text field */
			copyText.select();
			copyText.setSelectionRange(0, 99999); /* For mobile devices */

			/* Copy the text inside the text field */
			navigator.clipboard.writeText(copyText.value);
			/* Alert the copied text */
			// alert("Copied the text: "  );
			$("#invite_members").modal("hide");
			 $('#invite_members ').hide();
			Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'Link is Copied',
				showConfirmButton: false,
				timer: 3000
			})

		})

			function modifyLink(){
				// $('.at-icon-wrapper .at-share-btn .at-svc-link').hide();
				$finalLink = '';
				$stringOfURl = $('#linkpost').data('url').split('/');

				$stringOfURl[6] = $('.btn_select_role.active').data('role');
				for(var x = 0 ; x < $stringOfURl.length ; x++){

					if(x == $stringOfURl.length -1){
						$finalLink +=	$stringOfURl[x];
					}else{
						$finalLink += $stringOfURl[x]+"/";
					}
				}

			     $('.addthis_inline_share_toolbox').data('url', $finalLink).attr('data-url',$finalLink);
				$('#changeInviteLInk').val($finalLink);

			}


        $(document).on('click', '.add_member', function() {
            var user_id = $(this).data('user');
            var name = $('#invite_user_' + user_id).find('.user_name_img .invite_name').text();
            $(this).hide();
            $('#invite_user_' + user_id).find('.add_member_loading').show();
            Notify.confirm('Continue inviting '+name+' to join the group?', function () {
                $.ajax({
                    url: base_url + 'group/invite_user_to_group/',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: $('#groupID').val(),
                        user_id: user_id,
                        role: $('.btn_select_role.active').data('role')
                    },
                    success: function (response) {
                        Notify.success(response.message, {
							afterClose : function () {
								$('#btnUserInviteSearch').trigger('click');
							}
						});
                    }
                });
            }, function () {
                $('#invite_user_' + user_id).find('.add_member').show();
                $('#invite_user_' + user_id).find('.add_member_loading').hide();
            });
        });

        $('#btnUserInviteSearch').click(function() {
            $('.invite_search_list').empty();
            $.ajax({
                url: base_url + 'group/search_invite_contacts/',
                method: 'POST',
                dataType: 'json',
                data: {
                    id: $('#groupID').val(),
                    keyword: $('#userInviteKey').val(),
                },
                success: function (response) {
                    if (response.totalRecords == 0) {
                        $item = $('.no_result_template').clone()
                                    .removeClass('no_result_template')
                                    .addClass('invite_search_list')
                                    .attr('id', 'invite_user_0')
                                    .show();
                                    $('.invite_search_list').append($item);
                    } else {
                        $.each(response.data, function(key, user) {
                            if (!$('#invite_user_' + user.Us_ID)[0]) {
                                $item = $('.user_invitation_template').clone()
                                            .removeClass('user_invitation_template')
                                            .addClass('invite_user')
                                            .attr('id', 'invite_user_' + user.Us_ID)
                                            .show();
                                $('.invite_search_list').append($item);
                                $('#invite_user_' + user.Us_ID).find('.user_name_img img').attr('src', user.Us_Thumb);
                                $('#invite_user_' + user.Us_ID).find('.user_name_img .invite_name').text(user.Us_Name);
                                if (user.Me_Status == true) {
                                    $('#invite_user_' + user.Us_ID).find('.added_member').show();
                                } else {
                                    $('#invite_user_' + user.Us_ID).find('.add_member').attr('data-user', user.Us_ID).show();
                                }

                                $('#invite_user_' + user.Us_ID).find('.user_name_img').on('click', function() {
                                    location.href = user.Us_URL;
                                });
                            }
                        });
                    }
                }
            });
        });

        $('.invite_response_btn').click(function() {
			var invite_response = $(this).data('action');
			Notify.confirm('Are you sure that you want to '+invite_response+' group invitation?', function () {
				$.ajax({
                    url: base_url + 'group/respond_to_group_invitation/',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: $('#groupID').val(),
                        action: invite_response
                    },
                    success: function (response) {
                        Notify.success(response.message, {
							afterClose : function () {
								location.reload();
							}
						});
                    }
                });
            }, function () {});
		});

        $(".secondery_btn").click(function(){
            // button class slided toggle
            $(this).toggleClass("slided");
            // parent class add
            $(this).parent().toggleClass("slided");
            // secondery menu toggle class
            $(this).next(".secondery_dropdown").toggleClass("slided");
        });

        // hide on mouse leave
        $(".secondery_item").mouseleave(function(){
            $(this).removeClass("slided");
            $(".secondery_btn").removeClass("slided");
            $(".secondery_dropdown").removeClass("slided");
        });

        $(".invite_member_btn").click(function(){
          $(".modal_container").addClass('popped');
          $(".invite_members").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".invite_members").removeClass('popped');
        });

        // $(".user_role_btn").click(function(){
        //
        //   $(".modal_container").addClass('popped');
        //   $(".user_role").addClass('popped');
        // });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".user_role").removeClass('popped');
        });
        
        // open script
        $(".role_opener").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });
        $(".change_role_btn").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });

        // clsoe script
        $(".role_dropdown").mouseleave(function(){
            $(".role_dropdown").removeClass('dropped');
        });
        $(".role_item").click(function(){
            var value = $(this).html();
            
            var parent = $(this).parents(".role_dropdown");
            var role_opener = $(parent).children(".role_opener");
            
            $(role_opener).html(value)
            
            $(".role_dropdown").removeClass('dropped');
        });

        $('.role_permission_option').click(function() {
            var role = $(this).attr('role_tab_id');
            $('.role_permission_option').removeClass('active');
            $(this).addClass('active');
            UpdateGroup.manageRolePermissions(role);
        });

        $('.save_group_permission').click(function() {
            var form = $('#permission_form');
            var role = $('.role_permission_option.active').attr('role_tab_id');
            var group = $('#groupID').val();
            var url = base_url + 'group/save_group_permissions/';

            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: {permission : form.serialize(), role: role, group_id: group},
                success: function(response){
                    Notify.success(response.message, {
                    });
                }
            });
        });
    },
    manageAllUsers: function (show_empty, role_tab) {
        UpdateGroup.populateuserList('', 'all_members', show_empty, role_tab);
    },
    manageSuperAdminUsers: function (show_empty, role_tab) {
        UpdateGroup.populateuserList('superadmin', 'super_admin', show_empty, role_tab);
    },
    manageAdminUsers: function (show_empty, role_tab) {
        UpdateGroup.populateuserList('admin', 'admin', show_empty, role_tab);
    },
    manageMemberUsers: function (show_empty, role_tab) {
        UpdateGroup.populateuserList('member', 'member', show_empty, role_tab);
    },
    manageFollowerUsers: function (show_empty, role_tab) {

        UpdateGroup.populateuserList('follower', 'follower', show_empty, role_tab);
    },
    manageNoRoleUsers: function (show_empty, role_tab) {
        UpdateGroup.populateuserList('', 'no_role', show_empty, role_tab);
    },
    managePendingUsers: function (show_empty, role_tab) {
        UpdateGroup.populateuserList('pending', 'pending', show_empty, role_tab);
    },
    populateuserList: function (role, list, show_empty, role_tab) {


        $('.user_list').empty();
        var filter_keyword = $('#user_filter').val();
        $.ajax({
			url: base_url + 'group/show_group_members/',
            method: 'POST',
            dataType: 'json',
			data: {
            id: $('#groupID').val(),
             keyword: filter_keyword,
             role: role
			},
			success: function (response) {
			 		totaluserlist = response.totalRecords;

                if (response.totalRecords == 0) {
                    if (show_empty) {
                        $item = $('.no_result_template').clone()
                                    .removeClass('no_result_template')
                                    .addClass('group_user_item')
                                    .attr('id', 'group_user_0')
                                    .show();
                                    $('#' + list).append($item);
                    }
                } else {
					alluser = 	response.data;
                    $.each(response.data, function(key, user) {

                        if (user.Me_Role != "") {
                            if (user.Me_Status != 'pending') {
                                $item = $('.has_role_template').clone()
                                    .removeClass('has_role_template')
                                    .addClass('group_user_item')
                                    .attr('id', 'group_user_' + user.Me_Us_ID)
                                    .show();
                            } else {
                                $item = $('.pending_role_template').clone()
                                    .removeClass('pending_role_template')
                                    .addClass('group_user_item')
                                    .attr('id', 'group_user_' + user.Me_Us_ID)
                                    .show();
                            }
                            
                        } else {
                            $item = $('.no_role_template').clone()
                                .removeClass('has_role_template')
                                .addClass('group_user_item')
                                .attr('id', 'group_user_' + user.Me_Us_ID)
                                .show();
                        }
                        $('#' + list).append($item);



                        $('#group_user_' + user.Me_Us_ID).find('.user_name_img img').attr('src', user.Us_Thumb);
                        $('#group_user_' + user.Me_Us_ID).find('.user_name_img p .member_name').text(user.Us_Name);
                        $('#group_user_' + user.Me_Us_ID).find('.user_name_img p .member_job').text(user.Us_JobTitle);
 						if(user.Creator == 1 ){

							$thisx = 	$('#group_user_' + user.Me_Us_ID + ' .role_dropdown');
							$thisx.hide();

						}else{
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown').attr('id', 'role_dropdown_' + user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').text(user.Rl_Type);
							$('#group_user_' + user.Me_Us_ID).find('.pending_action a.role_opener').text(user.Rl_Type);

							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').attr('data-id', user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown button.change_role_btn').attr('data-id', user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_secondery').attr('id', 'role_option_' + user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown .role_trigger').removeClass('role_trigger').addClass('role_trigger_' + user.Me_Us_ID);
							if(user.Me_Role == 'superadmin'){

								if(	user.merole == 0){
									if( user.Me_Us_ID == $('#userID').val() ){


									}else{
										$thisxz = 	$('#group_user_' + user.Me_Us_ID + ' .change_role_btn');
										$thisxz.hide();
										$thisxzz = 	$('#group_user_' + user.Me_Us_ID + ' .fa-angle-down');
										$thisxzz.hide();

									}


								}


							}
						}

                        $.ajax({
                            url: base_url + 'group/show_roles_for_dropdown/',
                            method: 'POST',
                            dataType: 'json',
                            data: {
                                except: user.Me_Role
                            },
                            success: function (response) {
                                $.each(response.data, function(key, role) {
									//
									// if(role.Rl_Code == 'creator'){
									//
									// 	if(	user.merole == 1   ) {
									// 		$item = '<a class="role_item" data-from="'+role_tab+'" data-role="'+role.Rl_Code+'" data-user="'+user.Me_Us_ID+'">'+role.Rl_Label+'</a>';
									// 		$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
									//
									// 	}
									// }else{
									// 	$item = '<a class="role_item" data-from="'+role_tab+'" data-role="'+role.Rl_Code+'" data-user="'+user.Me_Us_ID+'">'+role.Rl_Label+'</a>';
									// 	$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
									// }
									if(user.Creator == 1){

										if(	user.merole == 1   ) {

											$item = '<a class="role_item" data-from="'+role_tab+'" data-role="'+role.Rl_Code+'" data-roleold="'+user.Me_Role+'" data-user="'+user.Me_Us_ID+'">'+role.Rl_Label+'</a>';
											$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);

										}
									}else{


											if(user.Me_Role == 'superadmin'){
												if(	user.merole == 1 || user.Me_Us_ID == $('#userID').val()) {
													if(role.Rl_Code == 'creator'){
															if(user.Gr_Us_ID   == $('#userID').val() ){
																$item = '<a class="role_item" data-from="' + role_tab + '" data-roleold="'+user.Me_Role+'" data-role="' + role.Rl_Code + '" data-user="' + user.Me_Us_ID + '">' + role.Rl_Label + '</a>';
																$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);

															}
													}else{
														$item = '<a class="role_item" data-from="' + role_tab + '" data-roleold="'+user.Me_Role+'" data-role="' + role.Rl_Code + '" data-user="' + user.Me_Us_ID + '">' + role.Rl_Label + '</a>';
														$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
													}

												}
											}else{
												$item = '<a class="role_item" data-from="' + role_tab + '" data-roleold="'+user.Me_Role+'" data-role="' + role.Rl_Code + '" data-user="' + user.Me_Us_ID + '">' + role.Rl_Label + '</a>';
												$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);

											}



									}


                                });

								if(user.Me_Role == 'superadmin' && user.Creator != 1){
									if(	user.merole == 1 || user.Me_Us_ID == $('#userID').val()) {
										$item = '<a class="role_item" data-from="' + role_tab + '" data-roleold="'+user.Me_Role+'" data-role="leave" data-user="' + user.Me_Us_ID + '"> Leave Group</a>';
										$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);

									}
								}

                               if(user.Creator == 1){


							   }else{
								if(user.merole == 1){
								   $item = '<a class="group_remove" data-from="'+role_tab+'" data-roleold="'+user.Me_Role+'" data-user="'+user.Me_Us_ID+'">Remove from the Group</a>';

									$item += '<a  class="shownbutton" data-name="'+user.Us_Name+' "  data-id="'+user.Me_Us_ID+'">Check Q & A </a>';
								   $('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
								}
							   }

                            }
                        });
					 	$role_action_items = '<a  class="shownbutton" data-name="'+user.Us_Name+' "  data-id="'+user.Me_Us_ID+'">Check Q & A </a>';
						$role_action_items += '<a class="group_action" data-from="'+role_tab+'"  data-nofromtype="'+user.No_From_Type+'" data-notificationtype="'+user.notificationType+'" data-notification="'+user.notificationID+'" data-action="approve" data-user="'+user.Me_Us_ID+'">Approve</a>'
							+'<a class="group_action" data-from="'+role_tab+'" data-notification="'+user.notificationID+'"  data-nofromtype="'+user.No_From_Type+'" data-notificationtype="'+user.notificationType+'"  data-action="reject" data-user="'+user.Me_Us_ID+'">Reject</a>';

					    $('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_action').append($role_action_items);



                        $(".role_trigger_" + user.Me_Us_ID).click(function(){
                            $('#group_user_' + user.Me_Us_ID).find('#role_dropdown_' + user.Me_Us_ID).toggleClass('dropped');
                        });
                        $('#role_option_' + user.Me_Us_ID).mouseleave(function(){
                            $('#group_user_' + user.Me_Us_ID).find('#role_dropdown_' + user.Me_Us_ID).removeClass('dropped');
                        });
                        $('#group_user_' + user.Me_Us_ID).find('.user_name_img').on('click', function() {
                            location.href = user.Us_URL;
                        });
                    });
                }
			}
		});
    },

    init: function() {
        UpdateGroup.initComponents();
        UpdateGroup.initEvents();
        UpdateGroup.load_keywords();
    },

    load_keywords : function() {

        var keywords_list = new Bloodhound({
            datumTokenizer: function(datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                wildcard: '%QUERY',
                url: base_url + "account/get_keywords_list/%QUERY",
                    transform: function(response) {
                        return $.map(response, function(words) {
                            return { value: words['name'] };
                        });
                    }
            }
        });
        keywords_list.initialize();

        $('#txtKeywords').tagsinput({
            trimValue: true,
            typeaheadjs: {
                name: 'keywords_list',
                displayKey: 'value',
                valueKey: 'value',
                source: keywords_list.ttAdapter()
            }
        });

        $(".tt-input").bind("paste", function(e) {
            let keyword = e.originalEvent.clipboardData.getData('text');
            $(this).prop('size', keyword.length);
        });
    },

    initLoadSubCategory: function(categoryID) {

        $("#selSubCategory").empty();

        $.ajax({
            type: 'POST',
            url: base_url + 'account/get_subcategory/',
            data : {CatID: categoryID},
            dataType: 'json'
        }).done(function(data) {
            $("#selSubCategory").select2({placeholder: 'Choose a subcategory', data: data});
            
            let val = $("#selSubCategory").find("option:contains('Uncategorized')").val();

            if (val !== undefined) {
                $('#selSubCategory').val(val)
            } else {
                $('#selTopic').prop('selectedIndex', 0);
            }

            $('#selSubCategory').trigger('change');
        });
    },

    manageRolePermissions: function(role) {
        $.ajax({
            type: 'POST',
            url: base_url + 'group/show_group_permissions/',
            data : {group_id: $('#groupID').val(), role: role},
            dataType: 'json'
        }).done(function(response) {
            $('.permission_list').empty().append(response.data);
        });
    }
}

$(document).ready(function(){
    UpdateGroup.init();
});
function leavegroup() {
	Swal.fire({
		title: 'Warning',
		text: "Are you sure you want to leave " + $('#groupname').text() + " group?",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Leave',
		cancelButtonColor: '#d33',
		confirmButtonColor: '#3085d6',
		cancelButtonText: 'Cancel'
	}).then((result) => {
		if (result.isConfirmed) {
			$.ajax({
				url: base_url + 'group/member_leave_group',
				method:'POST',
				data:{GroupID: $("input[name='GroupID']").val() },
				success:function(data)
				{

					Notify.success('Successfully left the group', {
						afterClose : function () {
							location.replace(base_url);
						}
					});


				}
			});

		} else {


		}
	})
}

$(document).ready(function(){

	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;

	$('#upload_image').change(function(event){
		var files = event.target.files;

		var done = function(url){
			image.src = url;
			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
	});

	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			// aspectRatio:  1280/900,
			// zoom : true,
			//
			// viewMode: 0,
			// preview:'.preview'

			aspectRatio: 1,
			minCropBoxWidth: 512,
			minCropBoxHeight: 512,
			guides: true,
			highlight: false,
			dragCrop: true,
			cropBoxMovable: true,
			cropBoxResizable: true,
			responsive: true,
			background: false,
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
   		cropper = null;
	});

	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$.ajax({
					url: base_url + 'group/upload_image',
					method:'POST',
					data:{GroupID: $("input[name='GroupID']").val(), image:base64data},
					success:function(data)
					{
						$modal.modal('hide');
						// $('#uploaded_image').attr('src', base_url + data.trim());
						$(".group-cover-image").css("background", "transparent url('"+base_url + data.trim()+"') no-repeat center center /cover");
						$(".group-cover-image").css("background-size", "100% 100%");
					}
				});
			};
		});
	});
	
});
