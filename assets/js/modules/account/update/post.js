var UpdatePost = {
    initial_load_topic : "",
    initial_load_category : 0,
    initial_load_subcategory : 0,
    current_topic_selection : 0,
	initComponents : function() {


        $(".group_taggline").collapser({
            mode:'lines',
            truncate: 1,
            showText: '[Show More]',
            hideText: '[Show Less]',
            changeText: true
        });
        
        $("#post-images-container").hide();
        $("#post-videos-container").hide();
        $("#post-files-container").hide();
        $("#post-info-container").show();

		// $('.custom-select').select2();



		$(document).on('click', '.group_action', function() {

			var user_id = $(this).data('user');
			var from = $(this).data('from');
			var action = $(this).data('action');
			var notification_ID = $(this).data('notification');

			var notificationType = $(this).data('notificationtype');
			var nofromtype = $(this).data('nofromtype');

			let data = {};
			if(notificationType == 'post_join_via_be_a_member' || notificationType == 'post_join_via_link'){
				data = {
					user_id: user_id,
					type_id: $('#groupIDx').val(),
					action: action,
					notification_ID: notification_ID,
					postinvite : nofromtype,
					notype : notificationType
				}

			}else{
			data = {
				user_id: user_id,
				type_id: $('#groupIDx').val(),
				action: action,
				notification_ID: notification_ID,
				notype : notificationType
			}

			}
			var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
			Notify.confirm('Continue to '+action+' request from '+name+' to join the Post?', function () {

				$.ajax({
					url: base_url + 'group/approve_reject_join_request/',
					// url: base_url+ 'group/approve_reject_join_request_from_notification',
					method: 'POST',
					dataType: 'json',
					data: data,
					success: function (response) {
						Notify.success(response.message, {
							afterClose : function () {
								$('#tab_' + from).trigger('click');
							}
						});
					}
				});
			}, function () {});
		});

		$('#btnUserSearch').click(function() {
			UpdatePost.totalRecord = 0;
			var role_tab = $('.role_option.active').attr('role_tab_id');

			$('#tab_' + role_tab).trigger('click');
		})

		$(document).on('click', '.shownbutton', function() {

			var id = $(this).data('id');
			var name = $(this).data('name');
			var groupid = $('#groupIDx').val();


			 showGiveAnswer1(name , id , groupid)
		});

		$(document).on('click', '.role_item', function() {
			var user_id = $(this).data('user');
			var new_role = $(this).data('role');
			var new_role_name = $(this).text().replace('Make ', '');
			var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
			var from = $(this).data('from');
			Notify.confirm('Are you sure you want to change role of '+name+' to '+new_role_name+'?', function () {
				$.ajax({
					url: base_url + 'group/assign_group_role/',
					method: 'POST',
					dataType: 'json',
					data: {
						user_id: user_id,
						type_id: $('#groupIDx').val(),
						post_id: $('#Post_ID').val(),
						role: new_role,
						type: 'post',
					},
					success: function (response) {
						Notify.success('Role assignment successful!', {
							afterClose : function () {
								$('#tab_' + from).trigger('click');
							}
						});
					}
				});
			}, function () {});
		});

		$(document).on('click', '.group_remove', function() {
			var user_id = $(this).data('user');
			var name = $('#group_user_' + user_id).find('.user_name_img p .member_name').text();
			var from = $(this).data('from');
			Notify.confirm('Continue removing '+name+' from the post?', function () {
				$.ajax({
					url: base_url + 'group/remove_group_user/',
					method: 'POST',
					dataType: 'json',
					data: {
						user_id: user_id,
						type_id: $('#groupIDx').val(),
						postype : "post",
						post_id : $('#Post_ID').val()
					},
					success: function (response) {
						Notify.success('User removed successfully!', {
							afterClose : function () {
								$('#tab_' + from).trigger('click');
							}
						});
					}
				});
			}, function () {});
		});
		$(".role_option").click(function(){
			UpdatePost.totalRecord = 0;
			var role_tab = $(this).attr('role_tab_id');
			$(".role_option").removeClass("active");
			$(this).addClass("active");


			if (role_tab == "all_members") {
				$(".suggest_members").show();
				UpdatePost.manageAllUsers(true, role_tab);
			}else{
				$(".suggest_members").hide();
				$("#" + role_tab).show();
				if (role_tab == "super_admin") {
					UpdatePost.manageSuperAdminUsers(true, role_tab);
				} else if (role_tab == "admin") {
					UpdatePost.manageAdminUsers(true, role_tab);
				} else if (role_tab == "member") {
					UpdatePost.manageMemberUsers(true, role_tab);
				} else if (role_tab == "follower") {
					UpdatePost.manageFollowerUsers(true, role_tab);
				} else {
					UpdatePost.managePendingUsers(true, role_tab);
				}
			}
		});
        $('#selGroup').on('change', function(){
            UpdatePost.load_topic($(this).val());
        });

        $('#selTopic').on('change', function(e){
            UpdatePost.load_category();
        });

        $('#selCategory').on('change', function (e) {
            if ($(this).val() == 0) {
                $('#selNewCategoryBox').show();
                $('#txtNewCategory').prop("disabled", false);
                $('#txtNewCategory').prop("required", true);
            }               
            else {
                $('#selNewCategoryBox').hide();
                $('#txtNewCategory').prop("disabled", true);
                $('#txtNewCategory').prop("required", false);
            }

            UpdatePost.load_subcategory($(this).val());
        });

        $('#selSubCategory').on('change', function (e) {
            if ($(this).val() == 0) {
                $('#selNewSubCategoryBox').show();
                $('#txtNewSubCategory').prop("disabled", false);
                $('#txtNewSubCategory').prop("required", true);
            }               
            else {
                $('#selNewSubCategoryBox').hide();
                $('#txtNewSubCategory').prop("disabled", true);
                $('#txtNewSubCategory').prop("required", false);
            }
        });

        $('#txtKeywords').on('beforeItemAdd', function(event) {
            let keyword = event.item;

            $.ajax({
                type: 'POST',
                url: base_url + 'account/validate_keyword/',
                data : {keyword: keyword},
            }).done(function(data) {
                data = $.parseJSON(data);
                if (data) {
                    if (data.Kw_Banned == 1) {
                        $('#txtKeywords').tagsinput('remove', event.item);
                    }
                }
            });
        });
	},
	manageAllUsers: function (show_empty, role_tab) {
		UpdatePost.populateuserList('', 'all_members', show_empty, role_tab);
	},
	mutealluser : function (role, mutetype) {
							$text = '';
						if(mutetype == 0){
							$text = 'Unmute';
						}else{
							$text = 'mute';
						}
						if(role != 'all_members'){

							$text += ' all';

						}
		Notify.confirm('Are you sure you want to '+$text+'    '+ role.toUpperCase(), function () {
			$.ajax({
				type: 'POST',
				url: base_url + 'account/murealluser/',
				data : {
					role: role ,
					postid : $("input[name='PostID']").val(),
					mutetype : mutetype
				},
			}).done(function(data) {
						console.log(data)
				if(data == 2){
					Swal.fire(
						'error',
						 'limit is reached',
						'error'
					)
				}else{
					Swal.fire(
						'success',
						'All '+role+' are Mute',
						'success'
					)
				}

				$('#btnUserSearch').click();
			});
		}, function () {});


	},
	manageSuperAdminUsers: function (show_empty, role_tab) {
		UpdatePost.populateuserList('superadmin', 'super_admin', show_empty, role_tab);
	},
	manageAdminUsers: function (show_empty, role_tab) {
		UpdatePost.populateuserList('admin', 'admin', show_empty, role_tab);
	},
	manageMemberUsers: function (show_empty, role_tab) {
		UpdatePost.populateuserList('member', 'member', show_empty, role_tab);
	},
	manageFollowerUsers: function (show_empty, role_tab) {

		UpdatePost.populateuserList('follower', 'follower', show_empty, role_tab);
	},
	manageNoRoleUsers: function (show_empty, role_tab) {
		UpdatePost.populateuserList('', 'no_role', show_empty, role_tab);
	},
	managePendingUsers: function (show_empty, role_tab) {
		UpdatePost.populateuserList('pending', 'pending', show_empty, role_tab);
	},
	populateuserList: function (role, list, show_empty, role_tab) {
		$('#unmutemember').hide();
		$('#mutemember').hide();
 		$('.user_list').empty();
		var filter_keyword = $('#user_filter').val();
		$.ajax({
			url: base_url + 'post/show_group_members/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: $('#groupID').val(),
				pid: $('#Post_ID').val(),
				keyword: filter_keyword,
				role: role
			},
			success: function (response) {

				// alert(JSON.stringify(response));

				if(role == 'follower'){
					  $('#mutemember').show();
					$('#mutemember').text('Muted All The Time');
				}else if(role == 'pending'){
					$('#mutemember').hide();
					}else if(role == 'superadmin') {
					$('#mutemember').text('Mute Super Admin');
					// $('#mutemember').show();
				}else if(role == 'member'){
					$('#mutemember').text('Mute Member');
					// $('#mutemember').show();
				}
				else if(role == 'admin'){
					$('#mutemember').text('Mute Admin');
					// $('#mutemember').show();
				}else {
					$('#mutemember').text('Mute All');
					// $('#mutemember').show();
					}
				if (response.totalRecords == 0) {
					if (show_empty) {
						$item = $('.no_result_template').clone()
							.removeClass('no_result_template')
							.addClass('group_user_item')
							.attr('id', 'group_user_0')
							.show();
						$('#' + list).append($item);
					}
				} else {
					$.each(response.data, function(key, user) {
						if (user.Me_Role != "") {
							if (user.Me_Status != 'pending') {
								$item = $('.has_role_template').clone()
									.removeClass('has_role_template')
									.addClass('group_user_item')
									.attr('id', 'group_user_' + user.Me_Us_ID)
									.show();
							} else {
								$item = $('.pending_role_template').clone()
									.removeClass('pending_role_template')
									.addClass('group_user_item')
									.attr('id', 'group_user_' + user.Me_Us_ID)
									.show();
							}

						} else {
							$item = $('.no_role_template').clone()
								.removeClass('has_role_template')
								.addClass('group_user_item')
								.attr('id', 'group_user_' + user.Me_Us_ID)
								.show();
						}
						$('#' + list).append($item);


						if(user.group_admin == 1){
							$extra = '(Group Creator)';
						}else{
							$extra = '';
						}
						$('#group_user_' + user.Me_Us_ID).find('.user_name_img img').attr('src', user.Us_Thumb);
						$('#group_user_' + user.Me_Us_ID).find('.user_name_img  .member_name').text(user.Us_Name + 	$extra);
						$('#group_user_' + user.Me_Us_ID).find('.user_name_img  .member_job').text(user.Us_JobTitle);
						if(user.Creator == 1 || user.group_admin == 1  ) {

						}else{
							if(user.Me_Mute == 1) {

								$('#group_user_' + user.Me_Us_ID).find('  .mutejob').html('<i onclick="unmuteuser(\'' + user.Me_ID + '\', \'' + user.Us_Name + '\')" style="color:grey!important;" class="fas fa-microphone-slash"></i>');

								if(role != 'follower'){

									$('#unmutemember').show();
								}

							}else{
								$('#group_user_' + user.Me_Us_ID).find('  .mutejob').html('<i  onclick="muteuser(\'' + user.Me_ID + '\', \'' + user.Us_Name + '\')"  class="fas fa-microphone"></i>');
								$('#mutemember').show();
							}

						}

						if(user.Creator == 1 || user.group_admin == 1 ){

							$thisx = 	$('#group_user_' + user.Me_Us_ID + ' .role_dropdown');
							$thisx.hide();

						}else{


							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown').attr('id', 'role_dropdown_' + user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').text(user.Rl_Type);
							$('#group_user_' + user.Me_Us_ID).find('.pending_action a.role_opener').text(user.Rl_Type);

							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown a.role_opener').attr('data-id', user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown button.change_role_btn').attr('data-id', user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_secondery').attr('id', 'role_option_' + user.Me_Us_ID);
							$('#group_user_' + user.Me_Us_ID).find('.role_dropdown .role_trigger').removeClass('role_trigger').addClass('role_trigger_' + user.Me_Us_ID);

							if(user.Me_Role == 'superadmin'){

								if(	user.merole == 0){
									if( user.Me_Us_ID == $('#userID').val() ){


									}else{
										$thisxz = 	$('#group_user_' + user.Me_Us_ID + ' .change_role_btn');
										$thisxz.hide();
										$thisxzz = 	$('#group_user_' + user.Me_Us_ID + ' .fa-angle-down');
										$thisxzz.hide();

									 	 }


								}


							}
						}


						$.ajax({
							url: base_url + 'group/show_roles_for_dropdown/',
							method: 'POST',
							dataType: 'json',
							data: {
								except: user.Me_Role
							},
							success: function (response) {
								$.each(response.data, function(key, role) {

									if(user.Creator == 1){

										if(	user.merole == 1   ) {
											$item = '<a class="role_item" data-from="'+role_tab+'" data-role="'+role.Rl_Code+'" data-user="'+user.Me_Us_ID+'">'+role.Rl_Label+'</a>';
											$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);

										}
									}else{
										if(role.Rl_Code == "superadmin" || role.Rl_Code == "admin" || role.Rl_Code == "creator"){


										}else{

											if(user.Me_Role == 'superadmin'){
												if(	user.merole == 1 || user.Me_Us_ID == $('#userID').val()) {
													$item = '<a class="role_item" data-from="' + role_tab + '" data-role="' + role.Rl_Code + '" data-user="' + user.Me_Us_ID + '">' + role.Rl_Label + '</a>';
													$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
												}
												}else{
												$item  = '<a class="role_item" data-from="' + role_tab + '" data-role="' + role.Rl_Code + '" data-user="' + user.Me_Us_ID + '">' + role.Rl_Label + '</a>';
												$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
												}
											}


									}


								});

								if(user.Creator == 1){


								}else{
									if(user.merole == 1){
										$item = '<a class="group_remove" data-from="'+role_tab+'" data-user="'+user.Me_Us_ID+'">Remove from the post</a>';
										$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_options').append($item);
									}
								}

							}
						});


						// $role_action_items = '<a  class="shownbutton" data-name="'+user.Us_Name+' "  data-id="'+user.Me_Us_ID+'">Check Q & A </a>';
						$role_action_items = '<a class="group_action" data-from="'+role_tab+'"  data-nofromtype="'+user.No_From_Type+'" data-notificationtype="'+user.notificationType+'" data-notification="'+user.notificationID+'" data-action="approve" data-user="'+user.Me_Us_ID+'">Approve</a>'
							+'<a class="group_action" data-from="'+role_tab+'" data-notification="'+user.notificationID+'"  data-nofromtype="'+user.No_From_Type+'" data-notificationtype="'+user.notificationType+'"  data-action="reject" data-user="'+user.Me_Us_ID+'">Reject</a>';
						$('#group_user_' + user.Me_Us_ID).find('.role_dropdown div.role_action').append($role_action_items);



						$(".role_trigger_" + user.Me_Us_ID).click(function(){
							$('#group_user_' + user.Me_Us_ID).find('#role_dropdown_' + user.Me_Us_ID).toggleClass('dropped');
						});
						$('#role_option_' + user.Me_Us_ID).mouseleave(function(){
							$('#group_user_' + user.Me_Us_ID).find('#role_dropdown_' + user.Me_Us_ID).removeClass('dropped');
						});
						$('#group_user_' + user.Me_Us_ID).find('.user_name_img').on('click', function() {
							location.href = user.Us_URL;
						});
					});
				}
			}
		});
	},
	initEvents : function() {
        $("#post-info").click(function(){
            $(".nav-link").removeClass("active");
            $(".nav-container").hide();
            $("#post-info").addClass("active");
            $("#post-info-container").show();
        });

        $("#post-images").click(function(){
            $(".nav-link").removeClass("active");
            $(".nav-container").hide();
            $("#post-images").addClass("active");
            $("#post-images-container").show();
        });

        $("#post-videos").click(function(){
            $(".nav-link").removeClass("active");
            $(".nav-container").hide();
            $("#post-videos").addClass("active");
            $("#post-videos-container").show();
        });
        
        $("#post-files").click(function(){
            $(".nav-link").removeClass("active");
            $(".nav-container").hide();
            $("#post-files").addClass("active");
            $("#post-files-container").show();
        });
		$("#manage-user").click(function(){
			$(".nav-link").removeClass("active");
			$(".nav-container").hide();
			$("#manage-user").addClass("active");
			$("#manage-user-container").show();

			UpdatePost.totalRecord = 0;
			$('#user_filter').val("");
			$('#tab_all_members').trigger('click');

			$("#manage_user_container").show();
		});
        $(".secondery_btn").click(function(){
            // button class slided toggle
            $(this).toggleClass("slided");
            // parent class add
            $(this).parent().toggleClass("slided");
            // secondery menu toggle class
            $(this).next(".secondery_dropdown").toggleClass("slided");
        });

        // hide on mouse leave
        $(".secondery_item").mouseleave(function(){
            $(this).removeClass("slided");
            $(".secondery_btn").removeClass("slided");
            $(".secondery_dropdown").removeClass("slided");
        });

        $(".invite_member_btn").click(function(){
          $(".modal_container").addClass('popped');
          $(".invite_members").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){
            $(".modal_container").removeClass('popped');
            $(".invite_members").removeClass('popped');
        });

        $(".user_role_btn").click(function(){
          $(".modal_container").addClass('popped');
          $(".user_role").addClass('popped');
        });
      
        // close srcrpt
        $(".invite_members_closer").click(function(){

			$(".group_info_container").removeClass('popped');

			$(".invite_members").removeClass('popped');
			$(".modal_container").removeClass('popped');
            $(".user_role").removeClass('popped');
        });

        // open script
        $(".role_opener").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });
        $(".change_role_btn").click(function(){
            var parent = $(this).parents(".role_dropdown");
            $(parent).toggleClass('dropped');
        });

        // clsoe script
        $(".role_dropdown").mouseleave(function(){
            $(".role_dropdown").removeClass('dropped');
        });
        $(".role_item").click(function(){
            var value = $(this).html();
            
            var parent = $(this).parents(".role_dropdown");
            var role_opener = $(parent).children(".role_opener");
            
            $(role_opener).html(value)
            
            $(".role_dropdown").removeClass('dropped');
        });
	},

    init: function() {
        UpdatePost.initComponents();

        UpdatePost.initEvents();
        UpdatePost.load_keywords();
        UpdatePost.load_files();
    },

    load_keywords : function() {

        var keywords_list = new Bloodhound({
            datumTokenizer: function(datum) {
                return Bloodhound.tokenizers.whitespace(datum.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                wildcard: '%QUERY',
                url: base_url + "account/get_keywords_list/%QUERY",
                    transform: function(response) {
                        return $.map(response, function(words) {
                            return { value: words['name'] };
                        });
                    }
            }
        });
        keywords_list.initialize();

        $('#txtKeywords').tagsinput({
            trimValue: true,
            typeaheadjs: {
                name: 'keywords_list',
                displayKey: 'value',
                valueKey: 'value',
                source: keywords_list.ttAdapter()
            }
        });

        $(".tt-input").bind("paste", function(e) {
            let keyword = e.originalEvent.clipboardData.getData('text');
            $(this).prop('size', keyword.length);
        });
    },

    load_topic : function() {
        $.ajax({
            type: 'POST',
            url: base_url + 'account/get_topics/',
            data : {GroupID: $("#selGroup").val()},
            dataType: 'json'
        }).done(function(data) {
            $("#selTopic").empty();
            $("#selTopic").select2({placeholder: 'Choose subgroup or create new subgroup', data: data});

            if (UpdatePost.initial_load_topic == "") {
                $("#selTopic").val($("#To_ID").val());
                UpdatePost.initial_load_topic = 1;
            }

            $('#selTopic').trigger('change');
        });
    },

    load_category : function() {
        var type = 'post';

        if ($('#selTopic').val() == "0") {
            type = 'topic';
        }

        $.ajax({
            type: 'POST',
            url: base_url + 'account/get_category/',
            data : {type: type},
            dataType: 'json'
        }).done(function(data) {
            $("#selCategory").empty();
            $("#selCategory").select2({placeholder: 'Choose a category', data: data});

            if ($("#inp_category_id").val() != "" && UpdatePost.initial_load_category == 0) {
                $("#selCategory").val($("#inp_category_id").val());
                UpdatePost.initial_load_category = 1;
            }
            else {
                let val = $("#selCategory").find("option:contains('Uncategorized')").val();
                $('#selCategory').val(val).trigger('change');
            }

            $('#selCategory').trigger('change');
            UpdatePost.current_topic_selection = type;
        });
    },

    load_subcategory : function(categoryID) {
        $.ajax({
            type: 'POST',
            url: base_url + 'account/get_subcategory/',
            data : {CatID: categoryID},
            dataType: 'json'
        }).done(function(data) {
            $("#selSubCategory").empty();
            $("#selSubCategory").select2({placeholder: 'Choose a subcategory', data: data});

            if (UpdatePost.initial_load_subcategory == 0) {
                if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
                    $('#selSubCategory').val($("#inp_subcategory_id").val());
                }
                UpdatePost.initial_load_subcategory = 1;
            }
            $('#selSubCategory').trigger('change');
        });
    },

    load_files: function() {
        $.ajax({
            type: 'POST',
            url: base_url + 'post/get_post_uploaded_files/',
            data : {id: $("input[name='PostID']").val()},
            dataType: 'json'
        }).done(function(data) {

            $("#input_file_images").fileinput({
                allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg'],
                initialPreview: data.images.url,
                initialPreviewAsData: true,
                initialPreviewConfig: data.images.initialPreviewConfig,
                deleteUrl: base_url + 'post/delete_post_uploaded_file',
                overwriteInitial: false,
                showDrag: false,
                uploadUrl : base_url + 'post/upload_post_file',
                browseOnZoneClick: true,
                showRemove: false,
                showClose: false,
                browseLabel: 'Browse Image',
                uploadExtraData : {'groupid' : $('#groupID').val() , 'PostID': $("input[name='PostID']").val(), 'type': 'image', 'input': 'file_images'},
            }).on('fileuploaded', function(event, data) {


				if(data.response.reason){

					Swal.fire({
						title: 'Stop',
						text: " You have reached the FREE limit with maximum of one gigabyte (1GB) per group.",
						icon: 'warning',
						showCancelButton: true,
						confirmButtonText:'Contact Us',
						cancelButtonColor:  '#d33',
						confirmButtonColor:  '#3085d6',
						cancelButtonText: 'Close'
					}).then((result) => {
						if (result.isConfirmed) {


							$('#createTopicModal').modal('hide');
							$('#ContactUsForm').modal('show');
						}else{

						}
					})
					return false;
				}
                // console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
            }).on('fileuploaderror', function(event, data, msg) {
                try{
					console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId)
				}catch (e) {
						console.log(e.message);
				}
            }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
				try{
 				}catch (e) {
					console.log(e.message);
				}

                // console.log('File Batch Uploaded', preview, config, tags, extraData);
            });

            $("#input_file_others").fileinput({
                initialPreview: data.files.url,
                initialPreviewAsData: true,
                initialPreviewConfig: data.files.initialPreviewConfig,
                deleteUrl: base_url + 'post/delete_post_uploaded_file',
                overwriteInitial: false,
                showDrag: false,
                uploadUrl : base_url + 'post/upload_post_file',
                browseOnZoneClick: true,
                showBrowse: true,
                showRemove: false,
                showClose: false,
                hideThumbnailContent: false,
                browseLabel: 'Browse Files',
                uploadExtraData : {'groupid' : $('#groupID').val(),  'PostID': $("input[name='PostID']").val(), 'type': 'file', 'input': 'file_others'},
                preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
                previewFileIconSettings: { // configure your icon file extensions
                    'doc': '<i class="fas fa-file-word text-primary"></i>',
                    'xls': '<i class="fas fa-file-excel text-success"></i>',
                    'ppt': '<i class="fas fa-file-powerpoint text-danger"></i>',
                    'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
                    'zip': '<i class="fas fa-file-archive text-muted"></i>',
                    'htm': '<i class="fas fa-file-code text-info"></i>',
                    'txt': '<i class="fas fa-file-alt text-info"></i>',
                    'mov': '<i class="fas fa-file-video text-warning"></i>',
                    'mp3': '<i class="fas fa-file-audio text-warning"></i>',
                    // note for these file types below no extension determination logic 
                    // has been configured (the keys itself will be used as extensions)
                    'jpg': '<i class="fas fa-file-image text-danger"></i>', 
                    'gif': '<i class="fas fa-file-image text-muted"></i>', 
                    'png': '<i class="fas fa-file-image text-primary"></i>'    
                },
                previewFileExtSettings: { // configure the logic for determining icon file extensions
                    'doc': function(ext) {
                        return ext.match(/(doc|docx)$/i);
                    },
                    'xls': function(ext) {
                        return ext.match(/(xls|xlsx)$/i);
                    },
                    'ppt': function(ext) {
                        return ext.match(/(ppt|pptx)$/i);
                    },
                    'zip': function(ext) {
                        return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                    },
                    'htm': function(ext) {
                        return ext.match(/(htm|html)$/i);
                    },
                    'txt': function(ext) {
                        return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                    },
                    'mov': function(ext) {
                        return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                    },
                    'mp3': function(ext) {
                        return ext.match(/(mp3|wav)$/i);
                    }
                }
            }).on('fileuploaded', function(event,data) {
				if(data.response.reason){

					Swal.fire({
						title: 'Stop',
						text: " You have reached the FREE limit with maximum of one gigabyte (1GB) per group.",
						icon: 'warning',
						showCancelButton: true,
						confirmButtonText:'Contact Us',
						cancelButtonColor:  '#d33',
						confirmButtonColor:  '#3085d6',
						cancelButtonText: 'Close'
					}).then((result) => {
						if (result.isConfirmed) {


							$('#createTopicModal').modal('hide');
							$('#ContactUsForm').modal('show');
						}else{

						}
					})
					return false;
				}
                // console.log('Filel Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
            }).on('fileuploaderror', function(event, data, msg) {
				try{
					console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId)
				}catch (e) {
					console.log(e.message);
				}
            }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {

                // console.log('File Batch Uploaded', preview, config, tags, extraData);
            });
        });
    }
};

$(".invite_members_closer").click(function(){
	$(".modal_container").removeClass('popped');
	$(".invite_members").removeClass('popped');

});


function modifyLink(){
	// $('.at-icon-wrapper .at-share-btn .at-svc-link').hide();
	$finalLink = '';
	$stringOfURl = $('#linkpost').data('url').split('/');

	$stringOfURl[8] = $('.btn_select_role.active').data('role');
	for(var x = 0 ; x < $stringOfURl.length ; x++){

		if(x == $stringOfURl.length -1){
			$finalLink +=	$stringOfURl[x];
		}else{
			$finalLink += $stringOfURl[x]+"/";
		}
	}

	$('.addthis_inline_share_toolbox').data('url', $finalLink).attr('data-url',$finalLink);
	$('#changeInviteLInk').val($finalLink);

}
function make_plus(){
	$.ajax({
		url: base_url + 'post/addfavourite',
		method:'POST',
		data:{PostID: $("input[name='PostID']").val()},
		success:function(data)
		{
			if(data == 'activate'){
				if($('.changefav').hasClass('fa-star-o')){
					$('.changefav').removeClass("fa-star-o");

					$('.changefav').addClass("fa-star");

				}
			}else {
				if($('.changefav').hasClass('fa-star')){
					$('.changefav').removeClass("fa-star");

					$('.changefav').addClass("fa-star-o");

				}
			}

		}
	});}
	function counter_plus(){


		$.ajax({
			url: base_url + 'post/updateCounter',
			method:'POST',
			data:{PostID: $("input[name='PostID']").val()},
			success:function(data)
			{
				if(data == 'activate'){
					if($('.changeheart').hasClass('fa-heart-o')){
						$('.changeheart').removeClass("fa-heart-o");

						$('.changeheart').addClass("fa-heart");

					}

					$('#postcounter').show();
					$('#postcounter').text(	parseInt($('#postcounter').text()) + 1 );
					$('#counterheader').text(	parseInt($('#counterheader').text()) + 1 );

				}else{
					if($('.changeheart').hasClass('fa-heart')){
						$('.changeheart').addClass("fa-heart-o");

						$('.changeheart').removeClass("fa-heart");

					}
					$('#postcounter').show();
					$('#postcounter').text(	parseInt($('#postcounter').text()) - 1 );
					$('#counterheader').text(	parseInt($('#counterheader').text()) - 1 );
				}
				console.log(data);
			}
		});

	}

function unmuteuser(id , username){

	$link = "<a href="+$('#posttitle').attr('href')+">"+$('#posttitle').text()+"</a>";
	Notify.confirm('Are you sure you want to unmute '+username+'?', function () {
		$.ajax({
			type: 'POST',
			url: base_url + 'account/unmutesingleuser/',
			data : {
				postalid: $('#Post_ID').val(),
				postid : id
			},
		}).done(function(data) {
			if(data == 2){
				Swal.fire(
					'error',
					'limit is reached',
					'error'
				)
			}else{
				Swal.fire(
					'success',
					username +" is unmuted in "+$link,
					'success'
				)
			}


			$('#btnUserSearch').click();
		});
	}, function () {});

}
function muteuser(id , username){
	$link = "<a href="+$('#posttitle').attr('href')+">"+$('#posttitle').text()+"</a>";

	Notify.confirm('Are you sure you want to mute '+username+'?', function () {
		$.ajax({
			type: 'POST',
			url: base_url + 'account/muresingleuser/',
			data : {
				postalid: $('#Post_ID').val(),
				postid : id
			},
		}).done(function(data) {

			if(data == 2){
				Swal.fire(
					'error',
					'limit is reached',
					'error'
				)
			}else{
				Swal.fire(
					'success',
					username +" is unmuted in "+$link,
					'success'
				)
			}

			$('#btnUserSearch').click();
		});
	}, function () {});

}
$(document).ready(function(){
	// alert("hello world");
	try{
		UpdatePost.init();
	}catch (e) {
		console.log("Examption" , e);
	}

	$('#selActionButton').on('change',
		function(){

		$("#selNewActionButton").hide();
		$("#txtActionButton").prop('required', false);

		$('#txtLandingPage').prop('disabled', true);
		$('#txtLandingPage').prop('required', false);
		if($(this).val() == -1){
			$('#txtLandingPage').prop('disabled', false);
			$('#txtLandingPage').prop('required', false);
			$('#txtLandingPage').val('');
			$('#selActionButton').val('');
			return;
		}

		if ($(this).val().length > 0) {

			if ($(this).val() == "0") {

				$("#selNewActionButton").show();
				$("#txtActionButton").prop('required', true);
				$('#txtLandingPage').prop('disabled', false);
				$('#txtLandingPage').prop('required', true);
			}
			else if($(this).val() == ""){

				$("#txtActionButton").prop('required', true);
				$('#txtLandingPage').prop('disabled', false);
				$('#txtLandingPage').prop('required', true);

			}
			else {


				$('#txtLandingPage').prop('disabled', false);
				$('#txtLandingPage').prop('required', true);
			}
		}
		else{
			$('#txtLandingPage').prop('disabled', false);
			$('#txtLandingPage').prop('required', false);
			$('#txtLandingPage').val('');
		}
	}
	);
	$('#txtActionButton').on('change' , function (){

		$('#txtLandingPage').prop('disabled', false);
		$('#txtLandingPage').prop('required', true);

	})
	$('.btn_select_role').click(function() {
		$('.btn_select_role').removeClass('active');

		$(this).addClass('active');
		modifyLink();


	});

	$('#inviceButton').click(function() {
		/* Get the text field */
		var copyText = document.getElementById("changeInviteLInk");

		/* Select the text field */
		copyText.select();
		copyText.setSelectionRange(0, 99999); /* For mobile devices */

		/* Copy the text inside the text field */
		navigator.clipboard.writeText(copyText.value);
		/* Alert the copied text */
		// alert("Copied the text: "  );
		// $("#invite_members").modal("hide");
		$('.invite_members').removeClass('popped');
		$('.group_info_container ').removeClass('popped');

		Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Link is Copied',
			showConfirmButton: false,
			timer: 3000
		})

	})


	$(".invite_member_btn").click(function(){
		// alert("asdasdas");
		$(".group_info_container").addClass('popped');
		// $(".invite_members").addClass('popped');
		// $('#userInviteKey').val("");
		// $('#btnUserInviteSearch').trigger('click');
	});
	var $modal = $('#modal');

	var image = document.getElementById('crop_uploaded_image');

	var cropper;
	var coef = 0;
	$('#upload_image').change(function(event){

		var files = event.target.files;

		var done = function(url){
			image.src = url;

			$modal.modal('show');
		};

		if(files && files.length > 0)
		{
			reader = new FileReader();
			reader.onload = function(event)
			{
				done(reader.result);
			};
			reader.readAsDataURL(files[0]);
		}
		//
		// if(files && files.length > 0)
		// {
		//     reader = new FileReader();
		//     reader.onload = function(event)
		//     {
		//         done(reader.result);
		//
		// 		image.width>image.height ? coef = calculateCoeff(image.width,"width") : coef = calculateCoeff(image.height,"height");
		//     };
		//     reader.readAsDataURL(files[0]);
		// }
	});
	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio:  1200/630,
			viewMode: 3,
			preview:'.preview'
		});
	}).on('hidden.bs.modal', function(){
		cropper.destroy();
		cropper = null;
	});
	$('#crop').click(function(){
		canvas = cropper.getCroppedCanvas({
			width:1200,
			height:630
		});

		canvas.toBlob(function(blob){
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function(){
				var base64data = reader.result;
				$.ajax({
					url: base_url + 'post/upload_image',
					method:'POST',
					data:{PostID: $("input[name='PostID']").val(), image:base64data},
					success:function(data)
					{
						$modal.modal('hide');
						$(".post-cover-image").css("background", "transparent url('"+base64data+"') no-repeat center center /cover");
						$(".post-cover-image").css("background-size", "100% 100%");
					}
				});
			};
		});
	});
});
