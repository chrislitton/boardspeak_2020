


var Profile = {
	init : function() {

		Profile.populateCreatedTab();
		Profile.initComponents();
		Profile.initEvents();
		Profile.populatePopularExplorePosts();
	},
	initComponents : function() {
		$("#follow_slider_container").hide();
	 	$("#explore_slider_container").hide();
	 	$("#created_slider_container").show();

		$("#feat_slider_container").show();
		$("#sugst_slider_container").hide();

		Profile.populateProfileHeaders();
	},
	initEvents : function() {
		$("#follow_slider").click(function(){
			$("#explore_slider").removeClass("active");
			$("#created_slider").removeClass("active");
			$("#follow_slider").addClass("active");
			$("#follow_slider_container").show();
			$("#explore_slider_container").hide();
			$("#created_slider_container").hide();
			Profile.populateFollowTab();
		});

		$("#explore_slider").click(function(){
			$("#follow_slider").removeClass("active");
			$("#created_slider").removeClass("active");
			$("#explore_slider").addClass("active");
			$("#explore_slider_container").show();
			$("#follow_slider_container").hide();
			$("#created_slider_container").hide();
			Profile.populateExploreTab();
		});

		$("#created_slider").click(function(){
			$("#follow_slider").removeClass("active");
			$("#explore_slider").removeClass("active");
			$("#created_slider").addClass("active");
			$("#created_slider_container").show();
			$("#explore_slider_container").hide();
			$("#follow_slider_container").hide();
			Profile.populateCreatedTab();
		});

		$("#feat_slider").click(function(){
			$("#feat_slider").addClass("active");
			$("#sugst_slider").removeClass("active");
			$("#feat_slider_container").show();
			$("#sugst_slider_container").hide();
			Profile.populateFeaturedGroups();
		});

		$("#sugst_slider").click(function(){
			$("#feat_slider").removeClass("active");
			$("#sugst_slider").addClass("active");
			$("#feat_slider_container").hide();
			$("#sugst_slider_container").show();
			Profile.populateSuggestedGroups();
		});

		$('.filter_btn').on('click', function() {
			$(this).toggleClass('active');
			Profile.populateExploreTab();
		});

		$('.filter_select').on('change', function() {
			$(this).toggleClass('active');
			Profile.populateCreatedTab();
			Profile.populateFollowTab();
			Profile.populateExploreTab();
		});

		$('#filter_submit').on('click', function() {


			var scrollTop = $(window).scrollTop();
 			$('html, body').animate({scrollTop:900},'100');
			Profile.populateCreatedTab();
			Profile.populateFollowTab();
			Profile.populateExploreTab();
		});

		$(".contact_popup_opener").click(function(){
			$(".contact_popup").addClass('popped');
			$('.contact_popup_header').text($('.user_name_designation h1').text().toUpperCase());
			Profile.populateContactHeaders();
			var id = $('.contact_popup_nav li.active').attr('id');
			Profile.populateList(id);
		});

		$(".contact_popup_closer").click(function(){
			$(".contact_popup").removeClass('popped');
		});

		$(".contact_popup_nav > li").click(function(){
			var data = $(this).attr('contact_data');

			$(".contact_popup_nav > li").removeClass("active");
			$(this).addClass("active");
			
			$(".contact_content_item").removeClass('active');
			$("." + data).addClass('active');

			var id = $('.contact_popup_nav li.active').attr('id');
			Profile.populateList(id);
		});

		$('.contact_filter_list li').click(function() {
			$('.contact_filter_list li').removeClass('active');
			$(this).addClass('active');

			var id = $('.contact_popup_nav li.active').attr('id');
			Profile.populateList(id);
		});

		$(document).on('click', '.contact_request_btn', function() {
			var action = $(this).data('action');
			var user = $(this).data('user');
			Profile.contactRequestAction(user, action);
		});

		$(document).on('click', '.contact_add_btn', function() {
			var action = $(this).data('action');
			var user = $(this).data('user');
			Profile.contactSuggestAction(user, action);
		});

		$(document).on('click', '.contact_cancel_btn', function() {
			var action = $(this).data('action');
			var user = $(this).data('user');
			Profile.contactSuggestAction(user, action);
		});

		$('#btnUserSearch').click(function() {
			var id = $('.contact_popup_nav li.active').attr('id');
			Profile.populateList(id);
		});

		$(document).on('click', '.contact_action .dropdown-menu .action_favorite', function() {
			var action = $(this).data('action');
			var user = $(this).data('user');
			Profile.contactFavoriteAction(user, action);
		});

		$(document).on('click', '.contact_action .dropdown-menu .action_block', function() {
			var action = $(this).data('action');
			var user = $(this).data('user');
			Profile.contactBlockMessageAction(user, action);
		});

		$(document).on('click', '.contact_action .dropdown-menu .action_remove_contact', function() {
			var action = $(this).data('action');
			var user = $(this).data('user');
			Profile.contactRemoveAction(user, action);
		});

		$(".invite_members_closer").click(function(){
			$(".group_info_container").removeClass('popped');
			$(".user_role").removeClass('popped');
			$(".invite_members").removeClass('popped');
		});
		
		$('.btn_select_role').click(function() {
			$('.btn_select_role').removeClass('active');
			$(this).addClass('active');
		});

		$(document).on('click', '.add_member', function() {
            var user_id = $(this).data('user');
            var name = $('#invite_user_' + user_id).find('.user_name_img .invite_name').text();
            $(this).hide();
            $('#invite_user_' + user_id).find('.add_member_loading').show();
            Notify.confirm('Continue inviting '+name+' to join the group?', function () {
                $.ajax({
                    url: base_url + 'group/invite_user_to_group/',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        id: $('#currentGroup').val(),
                        user_id: user_id,
                        role: $('.btn_select_role.active').data('role')
                    },
                    success: function (response) {
                        Notify.success(response.message, {
							afterClose : function () {
								$('#btnUserInviteSearch').trigger('click');
							}
						});
                    }
                });
            }, function () {
                $('#invite_user_' + user_id).find('.add_member').show();
                $('#invite_user_' + user_id).find('.add_member_loading').hide();
            });
        });

		$('#btnUserInviteSearch').click(function() {
            $('.invite_search_list').empty();
            $.ajax({
                url: base_url + 'group/search_invite_contacts/',
                method: 'POST',
                dataType: 'json',
                data: {
                    id: $('#currentGroup').val(),
                    keyword: $('#userInviteKey').val(),
                },
                success: function (response) {
                    if (response.totalRecords == 0) {
                        $item = $('.no_result_template').clone()
                                    .removeClass('no_result_template')
                                    .addClass('invite_search_list')
                                    .attr('id', 'invite_user_0')
                                    .show();
                                    $('.invite_search_list').append($item);
                    } else {
                        $.each(response.data, function(key, user) {
                            if (!$('#invite_user_' + user.Us_ID)[0]) {
                                $item = $('.user_invitation_template').clone()
                                            .removeClass('user_invitation_template')
                                            .addClass('invite_user')
                                            .attr('id', 'invite_user_' + user.Us_ID)
                                            .show();
                                $('.invite_search_list').append($item);
                                $('#invite_user_' + user.Us_ID).find('.user_name_img img').attr('src', user.Us_Thumb);
                                $('#invite_user_' + user.Us_ID).find('.user_name_img .invite_name').text(user.Us_Name);
                                if (user.Me_Status == true) {
                                    $('#invite_user_' + user.Us_ID).find('.added_member').show();
                                } else {
                                    $('#invite_user_' + user.Us_ID).find('.add_member').attr('data-user', user.Us_ID).show();
								}
								
								$('#invite_user_' + user.Us_ID).find('.user_name_img').on('click', function() {
                                    location.href = user.Us_URL;
                                });
                            }
                        });
                    }
                }
            });
		});
	},
	contactRequestAction : function(user, action) {
		Notify.confirm('Are you sure that you want to '+action+' contact request?', function () {
			$.ajax({
				url: base_url + 'user/respond_to_user_contact_request/',
				method: 'POST',
				dataType: 'json',
				data: {
					user_id: user,
					action: action
				},
				success: function (response) {
					Notify.success(response.message, {
						afterClose : function () {
							Profile.populateContactHeaders();
							Profile.populateList('request');
						}
					});
				}
			});
		}, function () {
			Profile.populateContactHeaders();
		});
	},
	contactSuggestAction : function(user, action) {
		var url = 'user/add_block_user/';
		if (action == "add") {
			var message = "Are you sure that you want to send contact request to this user?";
		} else if (action == "block") {
			var message = "Clicking this button will block the user. Continue?";
		} else {
			var message = "Clicking this button will cancel contact request to selected user. Continue?";
			var url = 'user/cancel_pending_user_invite/';
		}
		Notify.confirm(message, function () {
			$.ajax({
				url: base_url + url,
				method: 'POST',
				dataType: 'json',
				data: {
					user_id: user,
					action: action
				},
				success: function (response) {
					Notify.success(response.message, {
						afterClose : function () {
							Profile.populateContactHeaders();
							Profile.populateList('suggest');
						}
					});
				}
			});
		}, function () {
			Profile.populateContactHeaders();
		});
	},
	contactFavoriteAction : function(user, action) {
		Notify.confirm('Are you sure that you want to '+action+' this contact as favourite?', function () {
			$.ajax({
				url: base_url + 'user/contact_favorite_action/',
				method: 'POST',
				dataType: 'json',
				data: {
					user_id: user,
					action: action
				},
				success: function (response) {
					Notify.success(response.message, {
						afterClose : function () {
							Profile.populateContactHeaders();
							Profile.populateList('all');
						}
					});
				}
			});
		}, function () {
			Profile.populateContactHeaders();
		});
	},
	contactBlockMessageAction : function(user, action) {
		Notify.confirm('Are you sure that you want to '+action+' this contact from sending messages?', function () {
			$.ajax({
				url: base_url + 'user/contact_block_message_action/',
				method: 'POST',
				dataType: 'json',
				data: {
					user_id: user,
					action: action
				},
				success: function (response) {
					Notify.success(response.message, {
						afterClose : function () {
							Profile.populateContactHeaders();
							Profile.populateList('all');
						}
					});
				}
			});
		}, function () {
			Profile.populateContactHeaders();
		});
	},
	contactRemoveAction : function(user, action) {
		Notify.confirm('Are you sure that you want to '+action+' this user from you contacts?', function () {
			$.ajax({
				url: base_url + 'user/remove_contact/',
				method: 'POST',
				dataType: 'json',
				data: {
					user_id: user,
					action: action
				},
				success: function (response) {
					Notify.success(response.message, {
						afterClose : function () {
							Profile.populateContactHeaders();
							Profile.populateList('all');
						}
					});
				}
			});
		}, function () {
			Profile.populateContactHeaders();
		});
	},
	populateProfileHeaders : function() {
		$.ajax({
			url: base_url + 'user/user_contact_count/',
			method: 'POST',
			data: {
				keyword: $('.searchKey').val()
			},
			dataType: 'json',
			success: function (response) {

				$('.contact_popup_opener #contacts').text(response.contacts);
				$('#followed').text(response.group_join);


			}
		});
	},
	populateContactHeaders : function() {
		$.ajax({
			url: base_url + 'user/user_contact_count/',
			method: 'POST',
			data: {
				keyword: $('.searchKey').val()
			},
			dataType: 'json',
			success: function (response) {
				$('.contact_popup_nav #request span').text(response.requests);
				$('.contact_popup_nav #suggest span').text(response.suggested);
				$('.contact_popup_nav #all span').text(response.contacts);
				$('.contact_popup_opener #contacts').text(response.contacts);
			}
		});
	},
	populateList : function (list_type) {


		if (list_type == "request") {
			Profile.populateContactDetails('user/show_contact_request_list/', list_type);
		} else if (list_type == "suggest") {
			Profile.populateContactDetails('user/show_contact_suggest_list/', list_type);
		}else if(list_type == "explore"){

			Profile.populateContactDetails('user/show_contact_explore_list/', list_type);
 			 } else {
			Profile.populateContactDetails('user/show_contact_list/', list_type);
		}
	},
	populateContactDetails : function(contact_url, type) {
		$.ajax({
			url: base_url + contact_url,
			method: 'POST',
			data: {
				keyword: $('.searchKey').val(),
				sort: $('.contact_filter_list li.active').data('sort'),
				type: type
			},
			dataType: 'json',
			success: function (response) {


				$('.contact_content_item .contacts_list').empty();
				$('.contact_popup_nav #'+type+' span').text(response.totalRecords);
				if (response.totalRecords == 0) {
					$item = $('.no_contact_template').clone()
								.removeClass('no_contact_template')
								.addClass('contact_item')
								.attr('id', 'contact_item_0')
								.show();
					$('.contact_content_item.active .contacts_list').append($item);
				} else {
					console.log(response.data);

					$.each(response.data, function(key, user) {
						$item = $('.contact_template').clone()
								.removeClass('contact_template')
								.addClass('contact_item contact_'+type+'_item')
								.attr('id', 'contact_'+type+'_item_' + user.Us_ID)
								.show();
						$('.contact_content_item.active .contacts_list').append($item);
						$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_profile').on('click', function() {
							location.href = user.Us_URL;
						});
						$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_profile img').attr('src', user.Us_Thumb);
						$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_profile .contact_info h6').text(user.Us_Name);
						$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_profile .contact_info p').text(user.Us_JobTitle);
						$('#contact_'+type+'_item_' + user.Us_ID + ' .'+type+'_action').show();

						if (type == "request") {
							$('#contact_'+type+'_item_' + user.Us_ID ).find('.request_action .request_day').text(user.Us_Request_Since);
							$('#contact_'+type+'_item_' + user.Us_ID ).find('.request_action .contact_request_btn').attr('data-user', user.Us_ID);
						}

						if (type == "explore") {

							$('#contact_'+type+'_item_' + user.Us_ID ).find('.request_action1 .request_day1').text('asdas');
							$('#contact_'+type+'_item_' + user.Us_ID ).find('.request_action1 .contact_send_btn').attr('data-user', user.Us_ID);
						}
 						if (type == "suggest") {
							if (user.Has_Pending_Invite) {
								$('#contact_'+type+'_item_' + user.Us_ID + ' .'+type+'_action').hide();
								$('#contact_'+type+'_item_' + user.Us_ID + ' .'+type+'_invited_action').show();
								$('#contact_'+type+'_item_' + user.Us_ID ).find('.suggest_invited_action .contact_cancel_btn').attr('data-user', user.Us_ID);
							} else {
								$('#contact_'+type+'_item_' + user.Us_ID ).find('.suggest_action .contact_add_btn').attr('data-user', user.Us_ID);
							}
						}

						if (type =="all") {
							$('#contact_'+type+'_item_' + user.Us_ID + ' .'+type+'_action').hide();
							$('#contact_'+type+'_item_' + user.Us_ID + ' .'+type+'_contact_action').show();
							$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_action .dropdown-menu .dropdown-item').attr('data-user', user.Us_ID);
							if (user.Co_Favorite == 1) {
								$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_action .dropdown-menu .action_favorite').hide(); 
								$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_action .dropdown-menu .action_favorite.unmark').show(); 
							}

							if (user.Co_Block_Message == 1) {
								$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_action .dropdown-menu .action_block').hide(); 
								$('#contact_'+type+'_item_' + user.Us_ID ).find('.contact_action .dropdown-menu .action_block.unblock').show(); 
							}
						}
					});
				}
			}
		});
	},

	populatePopularExplorePosts : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();

		$.ajax({
			url: base_url + 'post/show_public_popular_posts/',
			method: 'POST',
			data: {
				keyword: '',
				alpha: '',
				fav: '',
				popular: '',
				privacy: '',
				latest: ''
			},
			success: function (response) {
				var length = $('.explore_popule_post_item_fromHome').length;
				for (var i=0; i<length; i++) {
					$(".explore_popule_post_item_fromHome").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				console.log("Popular POST::::::::" , response);


				$('.explore_post_slider1').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.post_no_data_item_template').clone()
						.removeClass('post_no_data_item_template')
						.addClass('explore_popule_post_item_fromHome')
						.attr('id', 'post_0aa')
						.show();
					$('.explore_post_slider1').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.post_data_item_template').clone()
							.removeClass('post_data_item_template')
							.addClass('explore_popule_post_item_fromHome')
							.attr('id', 'post_'+item.Raw_ID)
							.show();
						$('.explore_post_slider1').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#post_'+item.Raw_ID).find('.post_img a.post_link').attr('href', item.Po_URL).find('img').attr('src', item.Po_Thumb);
						$('#post_'+item.Raw_ID).find('.post_summary h4 a#board_like span').text(item.Po_Likes);
						$('#post_'+item.Raw_ID).find('.post_summary a.post_link').attr('href', item.Po_URL).text(item.Po_Title);
						$('#post_'+item.Raw_ID).find('.post_summary p.post_text').text(Profile.getEllipsis(item.Po_Description, 100));
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name h5').text(item.Us_Name);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name p').text(item.Us_JobTitle);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info img').attr('src', item.Us_Thumb);

						$('#post_'+item.Raw_ID).find('.post_summary a.author_info').attr('href', item.Po_U_URL);

						if (item.Po_Privacy == "private") {
							$('#post_'+item.Raw_ID).find('.post_img div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	populateUserCreatedGroups : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();
		
		$.ajax({
			url: base_url + 'group/show_user_created_groups/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.created_group_item').length;
				for (var i=0; i<length; i++) {
					$(".created_group_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('#created_group_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				var default_group = $('.default_create_group_item').clone().addClass('created_group_item').show();
				$('#created_group_slider').owlCarousel().trigger('add.owl.carousel', default_group).trigger('refresh.owl.carousel');
				if (response.totalRecords == 0) {
					$item = $('.created_group_no_data_item_template').clone()
							.removeClass('created_group_no_data_item_template')
							.addClass('created_group_item')
							.attr('id', 'created_group_0')
							.show();
						$('#created_group_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.created_group_data_item_template').clone()
							.removeClass('created_group_data_item_template')
							.addClass('created_group_item')
							.attr('id', 'created_group_'+item.Raw_ID)
							.show();
						$('#created_group_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#created_group_'+item.Raw_ID).find('.board_item figure a.group_link').attr('href', item.Gr_URL).find('img').attr('src', item.Gr_Thumb);
						$('#created_group_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.Gr_Likes);
						$('#created_group_'+item.Raw_ID).find('.board_item summary h3 a.group_link').attr('href', item.Gr_URL).text(Profile.getEllipsis(item.Gr_Name, 35));
						if (item.Gr_Privacy == "private") {
							$('#created_group_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
						if (item.Me_Status == "active") {
							$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .invite_to_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
						} else {
							if (item.Me_Status == "pending") {
								$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .pending_join_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
							} else {
								$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .join_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
							}
						}

						$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .join_group').on('click', function() {

							if(item.Gr_Privacy == 'private'){

								getQuestionSection( item.Gr_ID , 'member');

							}else{
								var data = {"id" : item.Gr_ID, "type" : "group"};
								Notify.confirm('You will become a MEMBER of this group.', function () {
									$.ajax({
										type: "POST",
										url: base_url + "account/join/",
										data: data,
										dataType: 'json',
										success: function(result){
											Notify.success(result.message, {
												afterClose : function () {
													$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .join_group').addClass('d-none').removeClass('d-inline').attr('data-id', item.Raw_ID);
													if (result.join_status == "pending") {
														$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .pending_join_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
													} else {
														$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .invite_to_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
													}
												}
											});

										}
									});
								}, function () {});

							}

								});

						$('#created_group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .invite_to_group').click(function(){
							$(".group_info_container").addClass('popped');
							$(".invite_members").addClass('popped');
							$('#currentGroup').val(item.Gr_ID);
							$('#btnUserInviteSearch').trigger('click');
						});
					});
					if( window.innerWidth >= 600){
						//web section
					}else{
						$('#created_group_slider').trigger('to.owl.carousel', 1);
						$('#created_group_slider').trigger('to.owl.carousel', 1);

					}
				}
			}
		});
	},
	populateUserCreatedTopics : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();

		$.ajax({
			url: base_url + 'topic/show_user_created_topics/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.created_topic_item').length;
				for (var i=0; i<length; i++) {
					$(".created_topic_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('#created_topic_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				var default_topic = $('.default_create_topic_item').clone().addClass('created_topic_item').show();
				$('#created_topic_slider').owlCarousel().trigger('add.owl.carousel', default_topic).trigger('refresh.owl.carousel');
				if (response.totalRecords == 0) {
					$item = $('.created_topic_no_data_item_template').clone()
							.removeClass('created_topic_no_data_item_template')
							.addClass('created_topic_item')
							.attr('id', 'created_topic_0')
							.show();
						$('#created_topic_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.created_topic_data_item_template').clone()
							.removeClass('created_topic_data_item_template')
							.addClass('created_topic_item')
							.attr('id', 'created_topic_'+item.Raw_ID)
							.show();
						$('#created_topic_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#created_topic_'+item.Raw_ID).find('.board_item figure a.topic_link').attr('href', item.To_URL).find('img').attr('src', item.To_Thumb);
						$('#created_topic_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.To_Likes);
						$('#created_topic_'+item.Raw_ID).find('.board_item summary h3 a.topic_link').attr('href', item.To_URL).text(Profile.getEllipsis(item.To_Name, 35));
						if (item.To_Privacy == "private") {
							$('#created_topic_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
					});
					if( window.innerWidth >= 600){
						//web section
					}else{
						$('#created_topic_slider').trigger('to.owl.carousel', 1);
						$('#created_topic_slider').trigger('to.owl.carousel', 1);

					}
				}
			}
		});
	},
	populateUserCreatedPosts : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();
		
		$.ajax({
			url: base_url + 'post/show_user_created_posts/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.created_post_item').length;
				for (var i=0; i<length; i++) {
					$(".created_post_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('#created_post_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				var default_post = $('.default_create_post_item').clone().addClass('created_post_item').show();
				$('#created_post_slider').owlCarousel().trigger('add.owl.carousel', default_post).trigger('refresh.owl.carousel');
				if (response.totalRecords == 0) {
					$item = $('.created_post_no_data_item_template').clone()
							.removeClass('created_post_no_data_item_template')
							.addClass('created_post_item')
							.attr('id', 'created_post_0')
							.show();
						$('#created_post_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.created_post_data_item_template').clone()
							.removeClass('created_post_data_item_template')
							.addClass('created_post_item')
							.attr('id', 'created_post_'+item.Raw_ID)
							.show();
						$('#created_post_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#created_post_'+item.Raw_ID).find('.post_img a.post_link').attr('href', item.Po_URL).find('img').attr('src', item.Po_Thumb);
						$('#created_post_'+item.Raw_ID).find('.post_summary h4 a#board_like span').text(item.Po_Likes);
						$('#created_post_'+item.Raw_ID).find('.post_summary a.post_link').attr('href', item.Po_URL).text(item.Po_Title);
						$('#created_post_'+item.Raw_ID).find('.post_summary p.post_text').text(Profile.getEllipsis(item.Po_Description, 100));
						$('#created_post_'+item.Raw_ID).find('.post_summary a.author_info').attr('href', item.Po_U_URL);

						$('#created_post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name h5').text(item.Us_Name);
						$('#created_post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name p').text(item.Us_JobTitle);
						$('#created_post_'+item.Raw_ID).find('.post_summary a.author_info img').attr('src', item.Us_Thumb);
						if (item.Po_Privacy == "private") {
							$('#created_post_'+item.Raw_ID).find('.post_img div.lock_indicator').show();
						}
					});
					if( window.innerWidth >= 600){
						//web section
					}else{
						$('#created_post_slider').trigger('to.owl.carousel', 1);
						$('#created_post_slider').trigger('to.owl.carousel', 1);

					}
				}
			}
		});
	},
	populateFeaturedGroups : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();
		
		$.ajax({
			url: base_url + 'group/show_featured_groups/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.featured_group_item').length;
				for (var i=0; i<length; i++) {
					$(".featured_group_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.feat_slider_container').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.featured_group_no_data_item_template').clone()
							.removeClass('featured_group_no_data_item_template')
							.addClass('featured_group_item')
							.attr('id', 'featured_group_0')
							.show();
						$('.feat_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.featured_group_data_item_template').clone()
							.removeClass('featured_group_data_item_template')
							.addClass('featured_group_item')
							.attr('id', 'featured_group_'+item.Raw_ID)
							.show();
						$('.feat_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#featured_group_'+item.Raw_ID).find('.board_item figure a.group_link').attr('href', item.Gr_URL).find('img').attr('src', item.Gr_Thumb);
						$('#featured_group_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.Gr_Likes);
						$('#featured_group_'+item.Raw_ID).find('.board_item summary h3 a.group_link').attr('href', item.Gr_URL).text(Profile.getEllipsis(item.Gr_Name, 35));
						if (item.Gr_Privacy == "private") {
							$('#featured_group_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	populateSuggestedGroups : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();
		
		$.ajax({
			url: base_url + 'group/show_suggested_groups/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.suggested_group_item').length;
				for (var i=0; i<length; i++) {
					$(".suggested_group_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.feat_slider_container').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.suggested_group_no_data_item_template').clone()
							.removeClass('suggested_group_no_data_item_template')
							.addClass('suggested_group_item')
							.attr('id', 'suggested_group_0')
							.show();
						$('.sugst_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.suggested_group_data_item_template').clone()
							.removeClass('suggested_group_data_item_template')
							.addClass('suggested_group_item')
							.attr('id', 'suggested_group_'+item.Raw_ID)
							.show();
						$('.sugst_slider_container').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#suggested_group_'+item.Raw_ID).find('.board_item figure a.group_link').attr('href', item.Gr_URL).find('img').attr('src', item.Gr_Thumb);
						$('#suggested_group_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.Gr_Likes);
						$('#suggested_group_'+item.Raw_ID).find('.board_item summary h3 a.group_link').attr('href', item.Gr_URL).text(Profile.getEllipsis(item.Gr_Name, 35));
						if (item.Gr_Privacy == "private") {
							$('#suggested_group_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	populateExploreGroups : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();
		
		$.ajax({
			url: base_url + 'group/show_groups/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.group_item').length;
				for (var i=0; i<length; i++) {
					$(".group_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.explore_group_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.group_no_data_item_template').clone()
							.removeClass('group_no_data_item_template')
							.addClass('group_item')
							.attr('id', 'group_0')
							.show();
						$('.explore_group_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.group_data_item_template').clone()
							.removeClass('group_data_item_template')
							.addClass('group_item')
							.attr('id', 'group_'+item.Raw_ID)
							.show();
						$('.explore_group_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#group_'+item.Raw_ID).find('.board_item figure a.group_link').attr('href', item.Gr_URL).find('img').attr('src', item.Gr_Thumb);
						$('#group_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.Gr_Likes);
						$('#group_'+item.Raw_ID).find('.board_item summary h3 a.group_link').attr('href', item.Gr_URL).text(Profile.getEllipsis(item.Gr_Name, 35));
						if (item.Gr_Privacy == "private") {
							$('#group_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}

						if (item.Me_Status == "active") {
							$('#group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .invite_to_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
						} else {
							if (item.Me_Status == "pending") {
								$('#group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .pending_join_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
							} else {
								$('#group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .join_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
							}
						}

						$('#group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .join_group').on('click', function() {

								console.log(item);

							if(item.Gr_Privacy == 'private' && item.group_Question  != '' && item.group_Question != null){

								getQuestionSection( item.Gr_ID , 'member');

							}else {
								var data = {"id": item.Gr_ID, "type": "group"};
								Notify.confirm('You will become a MEMBER of this group.', function () {
									$.ajax({
										type: "POST",
										url: base_url + "account/join/",
										data: data,
										dataType: 'json',
										success: function (result) {
											Notify.success(result.message, {
												afterClose: function () {
													$('#group_' + item.Raw_ID).find('.board_item figure figcaption .save_follow .join_group').addClass('d-none').removeClass('d-inline').attr('data-id', item.Raw_ID);
													if (result.join_status == "pending") {
														$('#group_' + item.Raw_ID).find('.board_item figure figcaption .save_follow .pending_join_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
													} else {
														$('#group_' + item.Raw_ID).find('.board_item figure figcaption .save_follow .invite_to_group').addClass('d-inline').removeClass('d-none').attr('data-id', item.Raw_ID);
													}
												}
											});

										}
									});
								}, function () {
								});
							}
						});

						$('#group_'+item.Raw_ID).find('.board_item figure figcaption .save_follow .invite_to_group').click(function(){
							$(".group_info_container").addClass('popped');
							$(".invite_members").addClass('popped');
							$('#currentGroup').val(item.Gr_ID);
							$('#btnUserInviteSearch').trigger('click');
						});
					});
				}
			}
		});
	},
	populateExploreTopics : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();

		$.ajax({
			url: base_url + 'topic/show_topics/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.topic_item').length;
				for (var i=0; i<length; i++) {
					$(".topic_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.explore_topic_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				
				if (response.totalRecords == 0) {
					$item = $('.topic_no_data_item_template').clone()
							.removeClass('topic_no_data_item_template')
							.addClass('topic_item')
							.attr('id', 'topic_0')
							.show();
						$('.explore_topic_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.topic_data_item_template').clone()
							.removeClass('topic_data_item_template')
							.addClass('topic_item')
							.attr('id', 'topic_'+item.Raw_ID)
							.show();
						$('.explore_topic_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#topic_'+item.Raw_ID).find('.board_item figure a.topic_link').attr('href', item.To_URL).find('img').attr('src', item.To_Thumb);
						$('#topic_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.To_Likes);
						$('#topic_'+item.Raw_ID).find('.board_item summary h3 a.topic_link').attr('href', item.To_URL).text(Profile.getEllipsis(item.To_Name, 35));
						if (item.To_Privacy == "private") {
							$('#topic_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
					});
				}

				if (length == 0) {
					$(".follow_topic_slider .default_item.explore_item").parent().parent().hide();
				}
			}
		});
	},
	populateExplorePosts : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();
		
		$.ajax({
			url: base_url + 'post/show_posts/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.explore_post_item').length;
				for (var i=0; i<length; i++) {
					$(".explore_post_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.explore_post_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.post_no_data_item_template').clone()
							.removeClass('post_no_data_item_template')
							.addClass('explore_post_item')
							.attr('id', 'post_0')
							.show();
						$('.explore_post_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.post_data_item_template').clone()
							.removeClass('post_data_item_template')
							.addClass('explore_post_item')
							.attr('id', 'post_'+item.Raw_ID)
							.show();
						$('.explore_post_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#post_'+item.Raw_ID).find('.post_img a.post_link').attr('href', item.Po_URL).find('img').attr('src', item.Po_Thumb);
						$('#post_'+item.Raw_ID).find('.post_summary h4 a#board_like span').text(item.Po_Likes);
						$('#post_'+item.Raw_ID).find('.post_summary a.post_link').attr('href', item.Po_URL).text(item.Po_Title);
						$('#post_'+item.Raw_ID).find('.post_summary p.post_text').text(Profile.getEllipsis(item.Po_Description, 100));
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info').attr('href', item.Po_U_URL);

						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name h5').text(item.Us_Name);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name p').text(item.Us_JobTitle);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info img').attr('src', item.Us_Thumb);
						if (item.Po_Privacy == "private") {
							$('#post_'+item.Raw_ID).find('.post_img div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	populateUserFollowedGroups : function() {
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();
		
		$.ajax({
			url: base_url + 'group/show_user_followed_groups/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.followed_group_item').length;

				for (var i=0; i<length; i++) {
					$(".followed_group_item").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				$('.follow_group_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				var create_group = $('.group_create_data_item_template').clone().addClass('followed_group_item').show();
				$('.follow_group_slider').owlCarousel().trigger('add.owl.carousel', create_group).trigger('refresh.owl.carousel');
				var explore_group = $('.group_explore_data_item_template').clone().addClass('followed_group_item').show();
				$('.follow_group_slider').owlCarousel().trigger('add.owl.carousel', explore_group).trigger('refresh.owl.carousel');
				if (response.totalRecords == 0) {
					$item = $('.group_no_data_item_template').clone()
							.removeClass('group_no_data_item_template')
							.addClass('followed_group_item')
							.attr('id', 'follow_group_000')
							.show();
						$('.follow_group_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.group_data_item_template').clone()
							.removeClass('group_data_item_template')
							.addClass('followed_group_item')
							.attr('id', 'follow_group_'+item.Raw_ID)
							.show();
						$('.follow_group_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#follow_group_'+item.Raw_ID).find('.board_item figure a.group_link').attr('href', item.Gr_URL).find('img').attr('src', item.Gr_Thumb);
						$('#follow_group_'+item.Raw_ID).find('.board_item summary h4 a#board_like span').text(item.Gr_Likes);
						$('#follow_group_'+item.Raw_ID).find('.board_item summary h3 a.group_link').attr('href', item.Gr_URL).text(Profile.getEllipsis(item.Gr_Name, 35));
						if (item.Gr_Privacy == "private") {
							$('#follow_group_'+item.Raw_ID).find('.board_item figure div.lock_indicator').show();
						}
					});
				}
				if( window.innerWidth >= 600){
					//web section
				}else{

					$('.follow_group_slider').trigger('to.owl.carousel', 2);
					$('.follow_topic_slider').trigger('to.owl.carousel', 2);

				}
				if (length == 0) {
					$(".follow_group_slider .group_explore_data_item_template").parent().hide();
				}
			}
		});
	},
	populateExploreTab : function() {
		Profile.populateFeaturedGroups();
		Profile.populateExploreGroups();
		Profile.populateExploreTopics();
		Profile.populateExplorePosts();
	},
	populateCreatedTab : function() {
		Profile.populateUserCreatedGroups();
		Profile.populateUserCreatedTopics();
		Profile.populateUserCreatedPosts();
	},
	populateFollowTab : function() {
		Profile.populateUserFollowedGroups();
		Profile.populateUserFollowedGroupsPost();
	},
	populateUserFollowedGroupsPost: function(){
		// follow_post_slider
		var filter_alpha = $('#filter_alpha option:selected').val();
		var filter_fav = ($('#filter_fav').hasClass('active')) ? true : false;
		var filter_popular = ($('#filter_popular').hasClass('active')) ? true : false;
		var filter_latest = ($('#filter_latest').hasClass('active')) ? true : false;
		var filter_privacy = $('#filter_privacy option:selected').val();
		var filter_keyword = $('#filter_keyword').val();

		$.ajax({
			url: base_url + 'post/show_group_join_posts/',
			method: 'POST',
			data: {
				keyword: filter_keyword,
				alpha: filter_alpha,
				fav: filter_fav,
				popular: filter_popular,
				privacy: filter_privacy,
				latest: filter_latest
			},
			success: function (response) {
				var length = $('.explore_post_item').length;
				for (var i=0; i<length; i++) {
					$(".explore_post_item_joined").trigger('remove.owl.carousel', [i]).trigger('refresh.owl.carousel');
				}
				response = $.parseJSON(response);
				console.log(response);
				$('.follow_post_slider').owlCarousel({
					margin: 25,
					nav: true,
					dots: false,
					responsiveClass: true,
					responsive: {
						0: { items: 1 },
						768: { items: 2 },
						991: { items: 3}
					}
				});
				if (response.totalRecords == 0) {
					$item = $('.post_no_data_item_template').clone()
						.removeClass('post_no_data_item_template')
						.addClass('explore_post_item_joined')
						.attr('id', 'post_0')
						.show();
					$('.follow_post_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
				} else {
					$.each(response.data, function (i, item){
						$item = $('.post_data_item_template').clone()
							.removeClass('post_data_item_template')
							.addClass('explore_post_item_joined')
							.attr('id', 'post_'+item.Raw_ID)
							.show();
						$('.follow_post_slider').owlCarousel().trigger('add.owl.carousel', $item).trigger('refresh.owl.carousel');
						$('#post_'+item.Raw_ID).find('.post_img a.post_link').attr('href', item.Po_URL).find('img').attr('src', item.Po_Thumb);
						$('#post_'+item.Raw_ID).find('.post_summary h4 a#board_like span').text(item.Po_Likes);
						$('#post_'+item.Raw_ID).find('.post_summary a.post_link').attr('href', item.Po_URL).text(item.Po_Title);
						$('#post_'+item.Raw_ID).find('.post_summary p.post_text').text(Profile.getEllipsis(item.Po_Description, 100));
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name h5').text(item.Us_Name);

						$('#post_'+item.Raw_ID).find('.post_summary a.author_info').attr('href', item.Po_U_URL);

						$('#post_'+item.Raw_ID).find('.post_summary a.author_info div.author_name p').text(item.Us_JobTitle);
						$('#post_'+item.Raw_ID).find('.post_summary a.author_info img').attr('src', item.Us_Thumb);
						if (item.Po_Privacy == "private") {
							$('#post_'+item.Raw_ID).find('.post_img div.lock_indicator').show();
						}
					});
				}
			}
		});
	},
	getEllipsis : function(text, limit) {
		for (var i = text.length; i >= 0; i--) {
			if (text.substring(0, i).length < limit) {
				if (i < text.length) {
					text = text.substring(0, i) + "...";
				}
				return text;
			}
		}
	}
};

$(document).ready(function() {
	Profile.init();
	/* owl carousel for followed groups */
	$('.follow_group_slider').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
	/* owl carousel for followed topics */
	$('.follow_topic_slider').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
	/* owl carousel for followed posts */
	$('.follow_post_slider').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
	/* owl carousel for explore groups */
	$('.explore_group_slider').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
 	 /* owl carousel for explore topics */
	$('.explore_topic_slider').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
	/* owl carousel for explore posts */
	$('.explore_post_slider').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
	/* owl carousel for Popular posts */
	$('.explore_post_slider1').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
});

  
  /* ========================================================== */
  /* custom script to hide show followed and explore board */

  
  /* filter button slider */
  $(document).ready(function() {
  var owl = $('.follow_explore_filter_btn');
	 owl.owlCarousel({
		autoWidth:true,
		margin: 15,
		nav: false,
		dots: false
	 });
  })
  
  
  /* dropdown list script */
//   $(document).ready(function(){
// 		$(".filter_dropdown_btn").click(function(){
// 		  $(this).next().toggleClass('expanded_dropdown');
// 		  $(this).toggleClass('expanded_dropdown');
// 		});
// 		$(".filter_dropdown_btn").parent().mouseleave(function(){
// 		  $(".filter_dropdown_btn").next().removeClass('expanded_dropdown');
// 		  $(".filter_dropdown_btn").removeClass('expanded_dropdown');
// 	 });
//   });
  
  
  
  /* owl carousel for popular post section */
  $(document).ready(function() {
	$('.user_popular_post_container').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
		  0: {
			 items: 1
		  },
		  768: {
			 items: 2
		  },
		  991: {
			 items: 3
		  }
		}
	 })
  })
  
  
  /* customs script for filtering groups post topic */
  $(document).ready(function(){
	 $(".filter_board").click(function(){
		var data_board = $(this).attr('filtered_board');
		
		$(".filter_board").removeClass("active");
		$(this).addClass("active");
		
		if (data_board == "all_board") {
		  
		  $(".slider_outer").show();
		}else{
  
		  $(".slider_outer").hide();
		  $("." + data_board).show();
		}
	 });
  });
  
  
  
  /* owl carousel for ad section */
  /* =================================================== */
  /* owl carousel for add slider */
  $(document).ready(function() {
	 $('.user_ad_slider').owlCarousel({
		loop: true,
		margin: 25,
		autoplay: true,
		autoplayTimeout: 2500,
		autoWidth:true,
		autoplayHoverPause: true,
		nav: false,
		dots: false
	 })
  })
  
  
/* popup script */
/* =================================================== */
$(document).ready(function(){
  $("#todos_popup_opener").click(function(){
	 $(".todos_not_popup").addClass('popped');
  });

  $(".todos_popup_closer").click(function(){
	 $(".todos_not_popup").removeClass('popped');
  });
});

$(document).ready(function(){
  $(document).on("click", ".create_group_btn", function(){
	 $(".create_group_popup").addClass('popped');
  });

  $(".create_group_popup_closer").click(function(){
	 $(".create_group_popup").removeClass('popped');
  });
});
  
  
  $(document).ready(function(){
	 var searchPosition =  $("#sticky_search").position().top;
  
	 $(window).scroll(function () { 
		var scroll = $(window).scrollTop(); 
  
		if (scroll > searchPosition) { 
			 $("#sticky_search").addClass('sticky_search');
		} else { 
			$("#sticky_search").removeClass('sticky_search');
		} 
	 });
  
  });

/* owl carousel for category option */
$(document).ready(function() {
  var owl = $('.category_slider_container');
	 owl.owlCarousel({
		autoWidth:true,
		margin: 15,
		nav: false,
		dots: false,
		autoplayTimeout: 2000,
		autoplayHoverPause: true
  
	 });




	if($('#openTab').val() == 1){

		$('.contact_popup_opener').click();
	}
  })
  
  /* owl carousel for featured boards */
  $(document).ready(function() {
	 $('.feat_slider_container').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
		  0: {
			 items: 1
		  },
		  768: {
			 items: 2
		  },
		  991: {
			 items: 3
		  }
		}
	 })
  })
  
  
  /* owl carousel for suggested boards */
  $(document).ready(function() {
	 $('.sugst_slider_container').owlCarousel({
		margin: 25,
		nav: true,
		dots: false,
		responsiveClass: true,
		responsive: {
		  0: {
			 items: 1
		  },
		  768: {
			 items: 2
		  },
		  991: {
			 items: 3
		  }
		}
	 })
  })
  
/* owl carousel for popular post */
$(document).ready(function() {
  $('.ppplr_post_slider_container').owlCarousel({
	 margin: 25,
	 nav: true,
	 dots: false,
	 autoWidth: true,
	 responsiveClass: true,
	 responsive: {
		0: {
		  items: 2
		},
		768: {
		  items: 3
		},
		991: {
		  items: 4
		}
	 }
  })
})
// $(document).ready(function(){
//
//
//
//
// 	var $modal = $('#modal');
//
// 	var image = document.getElementById('crop_uploaded_image');
//
// 	var cropper;
//
// 	$('#upload_image').change(function(event){
// 		var files = event.target.files;
//
// 		var done = function(url){
// 			console.log(url);
// 			image.src = url;
// 			$modal.modal('show');
// 		};
//
// 		if(files && files.length > 0)
// 		{
// 			reader = new FileReader();
// 			reader.onload = function(event)
// 			{
// 				done(reader.result);
// 			};
// 			reader.readAsDataURL(files[0]);
// 		}
// 	});
//
// 	$modal.on('shown.bs.modal', function() {
// 		cropper = new Cropper(image, {
// 			aspectRatio:  1200/630,
// 			viewMode: 3,
// 			preview:'.preview'
// 		});
// 	}).on('hidden.bs.modal', function(){
// 		cropper.destroy();
// 		cropper = null;
// 	});
//
// 	$('#crop').click(function(){
// 		canvas = cropper.getCroppedCanvas({
// 			width:1200,
// 			height:630
// 		});
//
// 		canvas.toBlob(function(blob){
// 			url = URL.createObjectURL(blob);
// 			var reader = new FileReader();
// 			reader.readAsDataURL(blob);
// 			reader.onloadend = function(){
// 				var base64data = reader.result;
// 				$.ajax({
// 					url: base_url + 'group/upload_image',
// 					method:'POST',
// 					data:{GroupID: $("input[name='GroupID']").val(), image:base64data},
// 					success:function(data)
// 					{
// 						$modal.modal('hide');
//
// 						$(".group-cover-image").css("background", "transparent url('"+base_url + data.trim()+"') no-repeat center center /cover");
// 						$(".group-cover-image").css("background-size", "100% 100%");
// 					}
// 				});
// 			};
// 		});
// 	});
//
// });
