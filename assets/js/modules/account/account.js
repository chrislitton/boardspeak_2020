/* script for notification tabs */
$(document).ready(function(){
	Account.init();
});

var Account = {
	init: function() {
		$('#groupAccessModal').on('show.bs.modal', function (e) {
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));

		});

		$( "#topicAccessModal" ).on('show.bs.modal', function(e){
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));
		});

		$( "#subtopicAccessModal" ).on('show.bs.modal', function(e){
		    var invoker = $(e.relatedTarget);

		    $("#group_id").val(invoker.data("id"));
		});


    	$('.owl-carousel').owlCarousel({
          margin: 50,
          nav: true,
        });

		Account.button_events();
		Account.dropdown_events();
	},

	button_events: function() {
		$("#btn-request-access").click(function(e){
			Account.request_grant_access('group');
		});

		$("#btn-request-topic-access").click(function(e){
			Account.request_grant_access('topic');
		});

		$("#btn-request-post-access").click(function(e){
			Account.request_grant_access('subtopic');
		});

		$("#btn_create_post_group").click(function(e) {
			e.preventDefault();

			var location = base_url + 'account/create/post/';
		
				location = location + $('#Gr_ID').val() + "/group";

				if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*')
					location = location + "/" + $('#inp_subcategory_id').val();

			window.location.href = location;
		});

		$(".subCategory").click(function(e) {
			$("#inp_subcategory_id").val($(this).attr("datasubcat"));
		})
	},

	dropdown_events: function() {

		if ($('#selGroup').length) {
			if ($('#selGroup').find(":selected").val() != '')
				Account.load_topic($('#selGroup').find(":selected").val());

			$('#selGroup').change(function(){
				Account.load_topic($(this).val());
			});
		}

		if($('#selTopic').length) {
			
			$('#selTopic').change(function(){
				if ($(this).val() == '')
					Account.reload_category();
			});
		}

		if ($('#selCategory').length) {
			if ($('#selCategory').find(":selected").val() != '')
				Account.load_subcategory($('#selCategory').find(":selected").val());

			$('#selCategory').change(function(){
				Account.load_subcategory($(this).val());
			});
		}
	},

	request_grant_access: function(type) {
		var data = {"ParentID" : $("#group_id").val(), "Parent" : type};

	 	$.ajax({
	 		type: "POST",
		 	url: base_url + "account/request_grant_access/",
		 	data: data, 
		 	dataType: 'json',
		 	success: function(result){
		 		console.log(result);

		 		console.log(result === "success");

		 		if (result === "success")
		    		alert('Access has been requested.');
		    	else
		    		alert('Request to access '+type+' already exist.');

		    	$('#'+type+'AccessModal').modal('toggle');
		  	}
	 	});
	},

	load_topic : function(groupID) {
		$.ajax({
			url: base_url + 'account/get_topics/',
			method : "POST",
			data : {GroupID: groupID},
			async : true,
			dataType : 'json',
			success: function(result) {

				var html  = '<option value="">Select Topic</option>';
					html += '<option value="">None</option>';
				var i;
				var topicID = 0;
				var selected = '';
				if ($("#To_ID").length)
					topicID = $("#To_ID").val();

				for(i=0; i<result.length; i++){
					selected = '';
					if (topicID != 0) {
						if (result[i].To_ID == topicID)
							selected = "selected";
					} 

					html += '<option value='+result[i].To_ID + ' ' + selected +'>'+result[i].To_Name+'</option>';
				}

				$('#selTopic').html(html);

			},
			error: function(result) {
				$("#errormsg").html("Error");
			}
		});
	},

	load_subcategory : function(categoryID) {
		$.ajax({
			url: base_url + 'account/get_subcategory/',
			method : "POST",
			data : {CatID: categoryID},
			async : true,
			dataType : 'json',
			success: function(result){

				if (categoryID=='0')
				{
					$('#selNewCategoryBox').show();
					$('#txtNewCategory').prop("disabled", false);
					$('#txtNewCategory').prop("required", true);
				}				
				else 
				{
					$('#selNewCategoryBox').hide();
					$('#txtNewCategory').prop("disabled", true);
					$('#txtNewCategory').prop("required", false);
				}

				var html = '<option value="">Choose sub-category</option>';
				var i;
				for(i=0; i<result.length; i++){
					html += '<option value='+result[i].Sc_ID+'>'+result[i].Sc_Name+'</option>';
				}

				html += '<option value="0">Others, please specify</option>';
				$('#selSubCategory').html(html);


				if ($("#inp_subcategory_id").val() != "" && $("#inp_subcategory_id").val() != "*") {
					$("#selSubCategory").val($("#inp_subcategory_id").val());
				}

			},
			error: function(result)
			{
				$("#errormsg").html("Error");
			}
		});
	},

	reload_category : function() {
		$.ajax({
			url: base_url + 'account/get_category/',
			method : "POST",
			data : {type: 'topic'},
			async : true,
			dataType : 'json',
			success: function(result){

				var html = '<option value="">Choose category</option>';
				var i;
				for(i=0; i<result.length; i++){
					html += '<option value='+result[i].Ca_ID+'>'+result[i].Ca_Name+'</option>';
				}

				html += '<option value="0">Others, please specify</option>';
				$('#selCategory').html(html);


				if ($("#inp_category_id").val() != "" && $("#inp_category_id").val() != "*") {
					$("#selCategory").val($("#inp_category_id").val());
				}

			},
			error: function(result)
			{
				$("#errormsg").html("Error");
			}
		});
	},
};
