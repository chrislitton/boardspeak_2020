var Notify = {
    theme: 'metroui',
    timeout: 2000,
    progressBar: true,
    animation: {
        open: 'noty_effects_open',
        close: 'noty_effects_close' 
    },
    success : function (message, callback, position = 'topRight') {
        Notify.show('success', message, callback, position);
    },

    warning : function (message, callback, position = 'topRight') {
        Notify.show('warning', message, callback, position);
    },

    info : function (message, callback, position = 'topRight') {
        Notify.show('info', message, callback, position);
    },

    error : function (message, callback, position = 'topRight') {
        Notify.show('error', message, callback, position);
    },

    show : function (type, message, callback, position) {
        new Noty ({
            theme: Notify.theme,
            type: type,
            text: message,
            modal: true,
         	 timeout: Notify.timeout,
            progressBar: Notify.progressBar,
            animation: Notify.animation,
            callbacks: callback,
            layout: position
        }).show();
    },

    confirm : function (message, accept, deny) {
        var notif = new Noty({
            theme: Notify.theme,
            text: message,
            animation: Notify.animation,
            modal: true,
            buttons: [
                Noty.button('YES', 'btn btn-success', function () {
                    accept();
                    notif.close();
                }),
                Noty.button('NO', 'btn btn-error', function () {
                    deny();
                    notif.close();
                })
            ]
        });
        notif.show();
    }
}
