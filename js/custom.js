$(function () {

    $(".groupsloadmore").slice(0, 11).show();
    $("#groupsloadmore").on('click', function (e) {
        e.preventDefault();
        $(".groupsloadmore:hidden").slice(0, 9).slideDown();
        if ($(".groupsloadmore:hidden").length == 0) {
            $("#groupsloadmore").fadeOut('slow');
        }

    });

	$(".topicsloadmore").slice(0, 11).show();
    $("#topicsloadmore").on('click', function (e) {
        e.preventDefault();
        $(".topicsloadmore:hidden").slice(0, 9).slideDown();
        if ($(".topicsloadmore:hidden").length == 0) {
            $("#topicsloadmore").fadeOut('slow');
        }

    });

	$(".subtopicsloadmore").slice(0, 11).show();
    $("#subtopicsloadmore").on('click', function (e) {
        e.preventDefault();
        $(".subtopicsloadmore:hidden").slice(0, 9).slideDown();
        if ($(".subtopicsloadmore:hidden").length == 0) {
            $("#subtopicsloadmore").fadeOut('slow');
        }

    });

	$(".postsloadmore").slice(0, 11).show();
    $("#postsloadmore").on('click', function (e) {
        e.preventDefault();
        $(".postsloadmore:hidden").slice(0, 9).slideDown();
        if ($(".postsloadmore:hidden").length == 0) {
            $("#postsloadmore").fadeOut('slow');
        }

    });

	$(".surveyloadmore").slice(0, 11).show();
    $("#surveyloadmore").on('click', function (e) {
        e.preventDefault();
        $(".surveyloadmore:hidden").slice(0, 9).slideDown();
        if ($(".surveyloadmore:hidden").length == 0) {
            $("#surveyloadmore").fadeOut('slow');
        }

    });

	$(".loadmore").slice(0, 12).show();
    $("#btnload").on('click', function (e) {
        e.preventDefault();
        $(".loadmore:hidden").slice(0, 9).slideDown();
        if ($(".loadmore:hidden").length == 0) {
            $("#btnload").fadeOut('slow');
        }

    });

	$('#confirm-delete').on('show.bs.modal', function(e) {

 
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});


	$('#edit-member').on('show.bs.modal', function(e) {
		$(this).find('#txtEditMember').attr('value', $(e.relatedTarget).data('member'));
		$(this).find('#txtEditMemberID').attr('value', $(e.relatedTarget).data('memberid'));
		$(this).find('#txtEditID').attr('value', $(e.relatedTarget).data('id'));
		$(this).find('#selEditRole').val($(e.relatedTarget).data('role'));
		$(this).find('#selEditStatus').val($(e.relatedTarget).data('status'));
	});

	$('#edit-groupcategory').on('show.bs.modal', function(e) {
		$(this).find('#txtEditGroupCategoryName').attr('value', $(e.relatedTarget).data('category'));
		$(this).find('#txtEditGroupCategoryID').attr('value', $(e.relatedTarget).data('categoryid'));
		$(this).find('#txtEditGroupCategoryOrder').attr('value', $(e.relatedTarget).data('categoryorder'));
	});


	$(".bclink").on('click', function (e) {
		$("#flebg").val('');
		$("#fleName").val('');
		$("#site-banner").attr('style','');
		if ($("#site-banner").hasClass('redlinear')) $("#site-banner").removeClass('redlinear');
		if ($("#site-banner").hasClass('bluelinear')) $("#site-banner").removeClass('bluelinear');
		if ($("#site-banner").hasClass('teallinear')) $("#site-banner").removeClass('teallinear');

		$("#site-banner").addClass($(this).attr('data-value'));
		$("#txtbg").val($(this).attr('data-id'));
	});

	$(".bglink").on('click', function (e) {
		$("#flebg").val('');
		$("#fleName").val('');
		$("#site-banner").attr('style','');
		if ($("#site-banner").hasClass('redlinear')) $("#site-banner").removeClass('redlinear');
		if ($("#site-banner").hasClass('bluelinear')) $("#site-banner").removeClass('bluelinear');
		if ($("#site-banner").hasClass('teallinear')) $("#site-banner").removeClass('teallinear');
		$("#site-banner").attr('style',$(this).attr('data-value'));
		$("#txtbg").val($(this).attr('data-id'));
	});

	function readURLFile(input,img) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$(img).attr('style', "background:transparent url("+e.target.result+") no-repeat center center /cover");
			}

			reader.readAsDataURL(input.files[0]);
		}
	}


	$("#flebg").change(function(){
		$("#site-banner").attr('style','');
		if ($("#site-banner").hasClass('redlinear')) $("#site-banner").removeClass('redlinear');
		if ($("#site-banner").hasClass('bluelinear')) $("#site-banner").removeClass('bluelinear');
		if ($("#site-banner").hasClass('teallinear')) $("#site-banner").removeClass('teallinear');
		readURLFile(this,'#site-banner');
		$("#txtbg").val('');
	});



	$(".qtype").change(function() {
		var qItem = $(this).val();
		var dItem = $(this).attr('data-value');

		if (qItem=='multiple')
		{
			$(".qoptions"+dItem).attr('style','');
			$(".muloption"+dItem).attr('required', 'required');
		}
		else
		{
			$(".qoptions"+dItem).attr('style','display: none;');
			$(".muloption"+dItem).removeAttr("required");
		}

	});


	$(".carousel-center").slick({
		dots: false,
		infinite: true,
		centerMode: true,
		slidesToShow: 5,
		slidesToScroll: 3,
		responsive: [
		{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	  });

	 $(".carousel").slick({
		dots: false,
		infinite: true,
		centerMode: false,
		slidesToShow: 7,
		slidesToScroll: 3,
		responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	  });

	  $(".boardrow").slick({
        dots: false,
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		}
	  ]
	  });
	  
      $(".variable").slick({
        dots: false,
		infinite: false,
		slidesToShow: 7,
		slidesToScroll: 3,
		prevArrow: false,
		nextArrow: false,
        variableWidth: true,
		responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: false
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
			infinite: false
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false
		  }
		}
	  ]
      });


});

/* page loader script */
$(window).on('load', function(){
	$(".loader").addClass("hidden");
});

/* main mobile menu collapse */
$(document).ready(function(){
	$("#mobile_menu_collase").click(function(){
		$(this).toggleClass('expanded_icon');
	$("#mobile_nav").toggleClass('expanded_menu');
	});
});


/* more option toggle */
$(document).ready(function(){
	$(document).on("click", ".more_option", function(){
		$(this).children(".more_option_item").toggleClass('expanded_more');
	});
	$(document).on("mouseleave", ".more_option", function(){
	$(this).children(".more_option_item").removeClass('expanded_more');
	});
});

/* more option toggle */
$(document).ready(function(){

	//this is need to be push
	$(".sub_nav_container").click(function(){
		$(this).next().toggleClass('expanded_dropdown');
		$(this).toggleClass('expanded_dropdown');
	});
	$(".sub_nav_container").parent().mouseleave(function(){
	$(".sub_nav_container").next().removeClass('expanded_dropdown');
	$(".sub_nav_container").removeClass('expanded_dropdown');
	});
});

$(document).ready(function(){
	$(".mobile_sub").click(function(){
		$(this).next().toggleClass('expanded_menu');
		$(this).parent().toggleClass('expanded_menu');
	});

});

$(document).ready(function(){


	$('#searchbarmobilbe').on('keyup' ,function () {


		if($('#searchbarmobilbe').val().length == 0){
			$('#list').hide();
		}
		else{

			$.ajax({
				url: base_url + 'account/searchbyheader',
				method:'POST',
				dataType: 'json',
				data:{ text_search:  $('#searchbarmobilbe').val()  },
				success:function(response) {
					console.log(response);
					$string = '';
					$('#saerchlistsohow').empty();
					if(response.group.length > 0){
						$('#list').show();


						for(var x = 0 ; x < response.group.length ; x++){

							$string += ' <li>' +
								'<a href="'+base_url+'account/view/group/'+response.group[x]['Gr_Slug']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 19%;   object-fit: cover;" src="'+base_url+response.group[x]['Gr_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div style="margin-left:5px;">' +
								'<h6 style=" margin-left: 5px;     width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								'  font-size: 14px;">'+response.group[x]['Gr_Name']+'</h6>' +
								'  <span style=" margin-left: 5px; " class="text">'+response.group[x]['Gr_Privacy']+' Group </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow').append($string);
					}
					if(response.post.length > 0){

						$('#list').show();

						for(var x = 0 ; x < response.post.length ; x++){

							$string += ' <li>' +
								'<a href="'+base_url+'account/view/post/'+response.post[x]['encode']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 19%;   object-fit: cover;" src="'+base_url+response.post[x]['Po_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div style="margin-left:5px;">' +
								'<h6 style=" margin-left: 5px;       width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								' font-size: 14px;">'+response.post[x]['Po_Title']+'</h6>' +
								'  <span style=" margin-left: 5px; " class="text">'+response.post[x]['Po_Privacy']+' Post </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow').append($string);
					}
					if(response.topic.length > 0){
						$('#list').show();


						for(var x = 0 ; x < response.topic.length ; x++){

							$string += ' <li>' +
								'<a href="'+base_url+'account/view/topic/'+response.topic[x]['encode']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 19%;   object-fit: cover;" src="'+base_url+response.topic[x]['To_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div style="margin-left:5px;">' +
								'<h6 style=" margin-left: 5px;       width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								' font-size: 14px;">'+response.topic[x]['To_Name']+'</h6>' +
								'  <span style=" margin-left: 5px; " class="text">'+response.topic[x]['To_Privacy']+' Topic </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow').append($string);
					}
					if(response.profile.length > 0){
						$('#list').show();


						for(var x = 0 ; x < response.profile.length ; x++){
							let status = response.profile[x]['Us_IsPrivate'] == 1 ? 'Private' : 'Public';
							$string += ' <li>' +
								'<a href="'+base_url+'u/'+response.profile[x]['encode']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 50%;   object-fit: cover;" src="'+response.profile[x]['Us_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div  >' +
								'<h6 style=" margin-left: 5px;       width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								' font-size: 14px;">'+response.profile[x]['Us_Name']+'</h6>' +
								'  <span style=" margin-left: 5px; " class="text">'+status+' Profile </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow').append($string);
					}

				}
			});


		}



	})

	$('#searchfilterID').on('keyup' , function () {

		if($('#searchfilterID').val().length == 0){
			$('#list1').hide();
		}
		else{
			$.ajax({
				url: base_url + 'account/searchbyheader',
				method:'POST',
				dataType: 'json',
				data:{ text_search:  $('#searchfilterID').val()  },
				success:function(response) {
					console.log(response);
					$string = '';
					$('#saerchlistsohow1').empty();
					if(response.group.length > 0){
						$('#list1').show();


						for(var x = 0 ; x < response.group.length ; x++){


							$string += ' <li>' +
								'<a href="'+base_url+'account/view/group/'+response.group[x]['Gr_Slug']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 19%;     display: block;\n' +
								'    object-fit: cover;" src="'+base_url+response.group[x]['Gr_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div  style="margin-left:5px;">' +
								'<h6 align="left" style=" margin-left: 5px;   color: white;  width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								'  font-size: 14px;">'+response.group[x]['Gr_Name']+'</h6>' +
								'  <span align="left" style=" float: left; color: white; margin-left: 5px; " class="text">'+response.group[x]['Gr_Privacy']+' Group </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow1').append($string);
					}
					if(response.post.length > 0){

						$('#list1').show();

						for(var x = 0 ; x < response.post.length ; x++){

							$string += ' <li>' +
								'<a href="'+base_url+'account/view/post/'+response.post[x]['encode']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 19%;     display: block;\n' +
								'    object-fit: cover;" src="'+base_url+response.post[x]['Po_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div style="margin-left:5px;">' +
								'<h6  align="left" style=" margin-left: 5px;  color: white;     width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								' font-size: 14px;">'+response.post[x]['Po_Title']+'</h6>' +
								'  <span align="left" style="float: left; color: white; margin-left: 5px; " class="text">'+response.post[x]['Po_Privacy']+' Post </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow1').append($string);
					}
					if(response.topic.length > 0){
						$('#list1').show();


						for(var x = 0 ; x < response.topic.length ; x++){

							$string += ' <li>' +
								'<a href="'+base_url+'account/view/topic/'+response.topic[x]['encode']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 19%;     display: block;\n' +
								'    object-fit: cover;" src="'+base_url+response.topic[x]['To_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div style="margin-left:5px;">' +
								'<h6  align="left" style=" margin-left: 5px;   color: white;    width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								' font-size: 14px;">'+response.topic[x]['To_Name']+'</h6>' +
								'  <span align="left" style="float: left; color: white; margin-left: 5px; " class="text">'+response.topic[x]['To_Privacy']+' Topic </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow1').append($string);
					}
					if(response.profile.length > 0){
						$('#list1').show();


						for(var x = 0 ; x < response.profile.length ; x++){
							let status = response.profile[x]['Us_IsPrivate'] == 1 ? 'Private' : 'Public';
							$string += ' <li>' +
								'<a href="'+base_url+'u/'+response.profile[x]['encode']+'">' +
								'<div class="row" style="    padding: 0px 14px;">' +
								'<div>' +
								'<span class="item">' +
								'<span class="icon place">' +
								'<img style="width: 60px;' +
								'height: 60px;' +
								'padding: 5px;' +
								'border-radius: 50%; object-fit: cover;" src="'+response.profile[x]['Us_Thumb']+'"/>' +
								'</span>' +

								'</span>' +
								'</div>' +
								'<div  >' +
								'<h6 align="left" style=" margin-left: 5px;  color: white;    width: 215px;\n' +
								'    white-space: nowrap;\n' +
								'    overflow: hidden;\n' +
								'    text-overflow: ellipsis;\n' +
								' font-size: 14px;">'+response.profile[x]['Us_Name']+'</h6>' +
								'  <span align="left" style="float: left; color: white; margin-left: 5px; " class="text">'+status+' Profile </span>' +
								'</div>' +
								'</div>' +

								'</a>' +
								'</li>'
						}
						$('#saerchlistsohow1').append($string);
					}



					if(	 $('#list1 ul li').length == 0  ){

						$('#list1').hide();

					}
				}
			});
		}

	})


	$(".only_desk_item.search_box > i").click(function(){
		$(".only_desk_item.search_box").toggleClass('slided_search');
		$( ".only_desk_item.search_box > input" ).focus();
	});
	// hide search bar
	// $( ".only_desk_item.search_box > input " ).focusout(function() {
	// 	$(".only_desk_item.search_box").removeClass('slided_search');
	//
	// });
	//
	$('body').click(function(e) {
		$("#list1").hide();
	});
});

