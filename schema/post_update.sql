CREATE TABLE `post_update` (
							   `Pu_ID` int(11) NOT NULL,
							   `Pu_user_id` int(11) NOT NULL,
							   `Pu_post_id` int(11) NOT NULL,
							   `Pu_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
