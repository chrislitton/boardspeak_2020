DROP TABLE `bs_contacts`;
CREATE TABLE `bs_contacts` ( `Co_ID` INT NOT NULL AUTO_INCREMENT , `Co_Us_ID` INT NOT NULL , `Co_Contact_ID` INT NOT NULL , `Co_Status` VARCHAR(20) NOT NULL , `Co_Request_Date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `Co_Contact_Date` DATETIME NULL , PRIMARY KEY (`Co_ID`)) ENGINE = InnoDB;
ALTER TABLE `bs_users` ADD `Us_IsPrivate` TINYINT(1) NOT NULL DEFAULT '0' AFTER `Us_IsAdmin`;
ALTER TABLE `bs_contacts` ADD `Co_Block_Message` TINYINT(1) NOT NULL DEFAULT '0' AFTER `Co_Status`;
ALTER TABLE `bs_contacts` ADD `Co_Favorite` TINYINT(1) NOT NULL DEFAULT '0' AFTER `Co_Block_Message`;
CREATE TABLE `bs_inquiries` (
  `In_ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `In_FName` varchar(100) NOT NULL,
  `In_LName` varchar(100) NOT NULL,
  `In_Email` varchar(300) NOT NULL,
  `In_Subject` varchar(500) NOT NULL,
  `In_Message` varchar(1000) NOT NULL,
  `In_DatePosted` datetime NOT NULL DEFAULT current_timestamp(), PRIMARY KEY (`In_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `bs_followers` CHANGE `Fl_Item_Type` `Fl_Item_Type` ENUM('user','group','topic','post') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;