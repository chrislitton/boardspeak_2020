CREATE TABLE `bs_permissions` (
  `Pm_ID` int(11) NOT NULL,
  `Pm_Code` varchar(100) NOT NULL,
  `Pm_Description` varchar(255) NOT NULL,
  `Pm_Status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `bs_permissions` (`Pm_ID`, `Pm_Code`, `Pm_Description`, `Pm_Status`) VALUES
(1, 'group_invite', 'Invite to Group', 1),
(2, 'group_invite_link', 'Invite to Group via Link', 1),
(3, 'group_role_superadmin', 'Add/Invite/Assign Super Admin Role', 1),
(4, 'group_role_admin', 'Add/Invite/Assign Admin Role', 1),
(5, 'group_role_member', 'Add/Invite/Assign Member Role', 1),
(6, 'group_role_follower', 'Add/Invite/Assign Follower Role', 1),
(7, 'group_manage_user', 'Mange Group Users', 1),
(8, 'group_manage_role', 'Manage Group Roles', 1),
(9, 'group_edit_info', 'Edit Group Details', 1),
(10, 'topic_create', 'Create Topic', 1),
(11, 'topic_edit', 'Edit Topic', 1),
(12, 'topic_delete', 'Delete Topic', 1),
(13, 'topic_view', 'View Topic', 1),
(14, 'topic_invite', 'Invite Topic Members', 1),
(15, 'post_create', 'Create Post', 1),
(16, 'post_edit', 'Edit Post', 1),
(17, 'post_delete', 'Delete Post', 1),
(18, 'post_view', 'View Post', 1),
(19, 'post_comment', 'Comment on Post', 1),
(20, 'post_invite', 'Invite to Post', 1);

ALTER TABLE `bs_permissions`
  ADD PRIMARY KEY (`Pm_ID`);

ALTER TABLE `bs_permissions`
  MODIFY `Pm_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;


CREATE TABLE `bs_group_permissions` ( `Gp_ID` INT NOT NULL AUTO_INCREMENT , `Gp_GrID` INT NOT NULL , `Gp_Role` VARCHAR(255) NOT NULL , `Gp_Allowed` TINYINT NOT NULL DEFAULT '1' , PRIMARY KEY (`Gp_ID`)) ENGINE = InnoDB;
ALTER TABLE `bs_group_permissions` ADD `Gp_PermissionID` INT NOT NULL AFTER `Gp_GrID`;