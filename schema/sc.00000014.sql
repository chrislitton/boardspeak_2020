ALTER TABLE `bs_keywords` ADD `Kw_Banned` INT(1) NOT NULL AFTER `Kw_Keyword`;

CREATE TABLE `bs_comment_tag` (
 `Ta_ID` int(11) NOT NULL AUTO_INCREMENT,
 `Ta_Co_ID` int(11) NOT NULL,
 `Ta_Us_ID` int(11) NOT NULL,
 PRIMARY KEY (`Ta_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `bs_comment_attachment` (
 `At_ID` int(11) NOT NULL AUTO_INCREMENT,
 `At_Co_ID` int(11) NOT NULL,
 `At_Orig_Filename` varchar(255) NOT NULL,
 `At_Filename` varchar(255) NOT NULL,
 `At_File_Type` varchar(100) NOT NULL,
 PRIMARY KEY (`At_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;

