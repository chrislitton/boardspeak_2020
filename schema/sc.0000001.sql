CREATE TABLE `bs_medias` (
 `Me_ID` int(11) NOT NULL AUTO_INCREMENT,
 `Me_Parent` int(11) NOT NULL,
 `Me_Type` varchar(25) NOT NULL,
 `Me_Filename` VARCHAR(100) NOT NULL,
 `Me_Filetype` varchar(25) NOT NULL,
 `Me_Path` VARCHAR(250) NOT NULL,
 `Me_DateCreated` DATETIME NOT NULL,
 PRIMARY KEY (`Me_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
