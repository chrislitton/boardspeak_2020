DROP TABLE `bs_buttons`;

CREATE TABLE `bs_buttons` (
  `Bn_ID` int(11) NOT NULL,
  `Bn_Code` varchar(50) DEFAULT NULL,
  `Bn_Caption` varchar(50) DEFAULT NULL,
  `Bn_Page` varchar(100) DEFAULT NULL,
  `Bn_Icon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `bs_buttons` (`Bn_ID`, `Bn_Code`, `Bn_Caption`, `Bn_Page`, `Bn_Icon`) VALUES
(1, NULL, 'Shop Now', NULL, 'fa fa-shopping-cart'),
(2, NULL, 'Register Now', NULL, 'fas fa-clipboard-check'),
(3, NULL, 'Contact Us', NULL, 'fas fa-phone'),
(4, NULL, 'Book Now', NULL, 'fas fa-address-book');

ALTER TABLE `bs_buttons`
  ADD PRIMARY KEY (`Bn_ID`);

ALTER TABLE `bs_buttons`
  MODIFY `Bn_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

ALTER TABLE `bs_posts` ADD `Po_Bn_ID` INT(10) NULL, ADD `Po_Bn_Page` VARCHAR(255) NULL;