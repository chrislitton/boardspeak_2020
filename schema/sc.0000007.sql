ALTER TABLE `bs_notification` ADD `No_From_Type` VARCHAR(50) NOT NULL AFTER `No_From`;
ALTER TABLE `bs_notification` ADD `No_To_Type` VARCHAR(50) NOT NULL AFTER `No_To`;
ALTER TABLE `bs_notification` CHANGE `No_Message` `No_Message` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;