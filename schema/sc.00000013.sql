ALTER TABLE `bs_comments` ADD `Co_DateModified` DATE NULL DEFAULT NULL AFTER `Co_DatePosted`, ADD `Co_Likes` INT NOT NULL DEFAULT '0' AFTER `Co_DateModified`;

ALTER TABLE `bs_comments` CHANGE `Co_Likes` `Co_Parent_ID` INT(11) NOT NULL DEFAULT '0';

CREATE TABLE `bs_comment_like` (
 `Lk_ID` int(11) NOT NULL AUTO_INCREMENT,
 `Lk_Co_ID` int(11) NOT NULL,
 `Lk_Us_ID` int(11) NOT NULL,
 PRIMARY KEY (`Lk_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;