CREATE TABLE `bs_group_answer` (
  `ga_ID` int(11) NOT NULL,
  `ga_Answer` text DEFAULT NULL,
  `ga_Us_ID` int(11) NOT NULL,
  `ga_gr_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
