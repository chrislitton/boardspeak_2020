<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$ci =& get_instance();
$ci->load->library('encryption');

function encode_id($id)
{
	$ci =& get_instance();

	$encryption = $ci->encryption->encrypt($id);
	$safe_url = str_replace(array('+', '/', '='), array('-', '_', '~'), $encryption);

	return $safe_url ;
}

function decode_id($encrypted_id, $is_array = FALSE)
{
	$ci =& get_instance();

	$safe_url = str_replace(array('-', '_', '~'), array('+', '/', '='), $encrypted_id);
	$decrypted_id = $ci->encryption->decrypt($safe_url);

	if(is_numeric($decrypted_id) === true || $is_array)
	{
		return $decrypted_id;
	}
	else
	{
		header('Location: ' . base_url() . 'home');
		exit();
	}
}
