<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function check_user_profile_photo($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if( $httpCode == 200 ){
        return $url;
    } else {
        return base_url() . 'img/nophoto.png';
    }
}

function check_user_profile_background($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if( $httpCode == 200 ){
        return $url;
    } else {
        return base_url() . 'img/nobg.png';
    }
}
