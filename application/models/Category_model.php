<?php
class Category_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}

	public function get_Record($id)
	{
		$query = $this->db->get_where('bs_category', array('Ca_ID' => $id));
		return $query->row_array();
	}

	public function get_CategoryByType($type)
	{


		$this->db->where('Ca_Type', $type);
		$this->db->order_by('Ca_Name', 'ASC');
		$query = $this->db->get('bs_category');

		return $query->result_array();
		
	}

	public function get_Categories($type)
	{
		// $this->db->select('bs_category.*,bs_subcategory.Sc_Ca_ID,bs_subcategory.Sc_Name');
		$this->db->select('bs_category.*, GROUP_CONCAT(bs_subcategory.Sc_ID) AS Sc_IDs, GROUP_CONCAT(bs_subcategory.Sc_Name) AS Sc_Names');
		$this->db->where('Ca_Type', $type);
		$this->db->order_by('Ca_Name', 'ASC');
		$this->db->join('bs_subcategory', 'bs_subcategory.Sc_Ca_ID = bs_category.Ca_ID','left');
		$this->db->group_by('bs_category.Ca_ID');
		$query = $this->db->get('bs_category');
		return $query->result_array();
		
	}
	
	public function get_Category($id = false)
	{
		if ($id == false)
		{
			$this->db->order_by('Ca_Type', 'ASC');
			$this->db->order_by('Ca_Name', 'ASC');
			$query = $this->db->get('bs_category');
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_category', array('Ca_ID' => $id));
		return $query->row_array();
	}

	public function get_SubCategory($CatID, $id = false)
	{
		if ($id == false) {
			$this->db->order_by('Sc_Name', 'ASC');
			if (is_array($CatID)) {
				$this->db->where_in('Sc_Ca_ID', $CatID);
			} else {
				$this->db->where('Sc_Ca_ID', $CatID);
			}
			$this->db->join('bs_category', 'bs_category.Ca_ID = bs_subcategory.Sc_Ca_ID','left');
			$query = $this->db->get('bs_subcategory');
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_subcategory', array('Sc_ID' => $id));

		return $query->row_array();
	}


	public function Insert($Name, $Type,$Default=0)
	{

	    $this->load->helper('url');
	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'Ca_Name' => $Name,
		'Ca_Type' => $Type,
		'Ca_DatePosted' => $now,
		'Ca_Default' => $Default
	    );

	    $this->db->insert('bs_category', $data);
		return $this->db->insert_id();
	}


	public function Update($ID, $Name, $Type, $Default=0)
	{
	    $data = array(
		'Ca_Name' => $Name,
		'Ca_Type' => $Type,
		'Ca_Default' => $Default
	    );

		$this->db->where('Ca_ID', $ID);
	    return $this->db->update('bs_category', $data);
	}

	
	public function InsertSub($CatID, $Name)
	{

	    $this->load->helper('url');
	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'Sc_Ca_ID' => $CatID,
		'Sc_Name' => $Name,
		'Sc_DatePosted' => $now
	    );

	    $this->db->insert('bs_subcategory', $data);
		return $this->db->insert_id();
	}

	
	public function UpdateSub($ID, $Name)
	{
	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'Sc_Name' => $Name
	    );

		$this->db->where('Sc_ID', $ID);
	    return $this->db->update('bs_subcategory', $data);
	}


	public function delete_row($id)
	{
		$this->db->delete('bs_category', array('Ca_ID' => $id));
	}

	
	public function delete_sub($id)
	{
		$this->db->delete('bs_subcategory', array('Sc_ID' => $id));
	}


	

	public function get_menu_categories($type, $id) {
		$this->db->select('Ca_ID as ID, Ca_Name as Name')
					->from("bs_category")
					->where('Ca_Type', $type)
					->where('Ca_Default', 1)
					->get();
		$category_query = $this->db->last_query();

		$this->db->distinct()
					->select('IFNULL(Ca_ID, To_Ca_Name) as ID, IFNULL(Ca_Name, To_Ca_Name) as Name')
					->from('bs_topics')
					->join('bs_category', 'To_Ca_ID = Ca_ID AND Ca_Type="'.$type.'"', 'left')
					->where('To_Gr_ID', $id)
					->get();
		$topic_query = $this->db->last_query();

		$this->db->distinct()
					->select('IFNULL(Ca_ID, Po_Ca_Name) as ID, IFNULL(Ca_Name, Po_Ca_Name) as Name')
					->from('bs_posts')
					->join('bs_category', 'Po_Ca_ID = Ca_ID AND Ca_Type="'.$type.'"', 'left')
					->where('Po_To_ID', 0)
					->where('Po_Gr_ID', $id)
					->get();
		$none_topic_post_query = $this->db->last_query();

		$this->db->from("($category_query UNION $topic_query UNION $none_topic_post_query) as unionTable")
					->order_by('Name');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_group_subcategories($category, $id=0) {
		$this->db->distinct()
					->select('  IFNULL(bs_subcategory.Sc_ID, To_Sc_Name) as Sc_ID,   IFNULL(bs_subcategory.Sc_Name, To_Sc_Name) as Sc_Name, 
								(CASE 
									WHEN To_Ca_ID = 0 THEN To_Ca_Name
									ELSE To_Ca_ID
								END) as Sc_Ca_ID')
					->from('bs_topics')
					->join('bs_subcategory', 'bs_subcategory.Sc_ID = bs_topics.To_Sc_ID AND bs_topics.To_Ca_ID = bs_subcategory.Sc_Ca_ID', 'LEFT')
					->where('To_Gr_ID', $id)
					->group_start()
					->where('To_Ca_ID', $category)
					->or_where('To_Ca_Name', $category)
					->group_end()
					->get();
		$topic_subcategory_query = $this->db->last_query();

		$this->db->distinct()
					->select('  IFNULL(bs_subcategory.Sc_ID, Po_Sc_Name) as Sc_ID, IFNULL(bs_subcategory.Sc_Name, Po_Sc_Name) as Sc_Name, 
								(CASE 
									WHEN Po_Ca_ID = 0 THEN Po_Ca_Name
									ELSE Po_Ca_ID
								END) as Sc_Ca_ID'

					)

					->from('bs_posts')
					->join('bs_subcategory', 'bs_subcategory.Sc_ID = bs_posts.Po_Sc_ID AND bs_posts.Po_Ca_ID = bs_subcategory.Sc_Ca_ID', 'LEFT')
					->where('Po_Gr_ID', $id)
					->group_start()
					->where('Po_Ca_ID', $category)
					->or_where('Po_Ca_Name', $category)
					->group_end()
					->get();
		$subtopic_subcategory_query = $this->db->last_query();

//	print_r($category);
//	exit;
		$this->db->distinct();
		$this->db->from("($topic_subcategory_query UNION $subtopic_subcategory_query) as unionTable")
				->order_by('Sc_Name');
		$this->db->where('Sc_Ca_ID' ,$category );
		$this->db->group_by('Sc_ID');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_topic_categories($type, $id) {
		$this->db->select('Ca_ID as ID, Ca_Name as Name')
					->from("bs_category")
					->where('Ca_Type', $type)
					->where('Ca_Default', 1)
					->get();
		$category_query = $this->db->last_query();

		$this->db->distinct()
					->select('IFNULL(Ca_ID, Po_Ca_Name) as ID, IFNULL(Ca_Name, Po_Ca_Name) as Name')
					->from('bs_posts')
					->join('bs_category', 'Po_Ca_ID = Ca_ID AND Ca_Type="'.$type.'"', 'left')
					->where('Po_To_ID', $id)
					->get();
		$post_query = $this->db->last_query();

		$this->db->distinct();
		$this->db->from("($category_query UNION $post_query) as unionTable")
					->order_by('Name');
		$query = $this->db->get();

		return $query->result_array();
	}	

	public function get_topic_subcategories($category, $id=0) {

		$this->db->distinct()
					->select('IFNULL(bs_subcategory.Sc_ID, Po_Sc_Name) as Sc_ID, IFNULL(bs_subcategory.Sc_Name, Po_Sc_Name) as Sc_Name, 
								(CASE 
									WHEN Po_Ca_ID = 0 THEN Po_Ca_Name
									ELSE Po_Ca_ID
								END) as Sc_Ca_ID')
					->from('bs_posts')
					->join('bs_subcategory', 'bs_subcategory.Sc_ID = bs_posts.Po_Sc_ID AND bs_posts.Po_Ca_ID = bs_subcategory.Sc_Ca_ID', 'LEFT')
					->group_start()
					->where('Po_Ca_Name', $category)
					->or_where('Po_Ca_ID', $category)
					->group_end()
					->where('Po_To_ID', $id);
//			$this->db->group_by('Sc_Ca_ID');
		$query = $this->db->get();



	$data  = $query->result_array();
		if(is_string($category)){

			foreach ($query->result_array() as $key => $row) {

				if($row['Sc_Ca_ID'] == $category){

				}else{
					unset($data[$key]);
				}
			}

		}

		return $data;
	}
}
