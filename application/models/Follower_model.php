
<?php
class Follower_model extends CI_Model {

    private $tableName = 'bs_followers';
    private $LoggedInUser;

    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');

        if (isset($this->session->userdata['logged_in'])){
            $this->LoggedInUser = $this->session->userdata['logged_in']['bs_id'];
        }
    }

    function check_if_followed($itemID, $itemType, $encoded = FALSE) {

        if ($encoded) {
            $itemID = decode($itemID);
        }

        $this->db->select("f.*", FALSE);
		$this->db->from($this->tableName . " f");
        $this->db->where("f.Fl_User_ID", $this->LoggedInUser);
        $this->db->where("f.Fl_Item_ID", $itemID);
        $this->db->where("f.Fl_Item_Type", $itemType);

		$query = $this->db->get();
        $data = $query->result();
        
        if (sizeOf($data) > 0) {
            return true;
        }

		return false;
    }

    function follow($data, $encoded = FALSE) {

        if ($encoded) {
            $itemID = decode_id($data['id']);
        } else {
            $itemID = $data['id'];
        }

        $this->db->insert($this->tableName, array('Fl_User_ID' => $this->LoggedInUser, 'Fl_Item_ID' => $itemID, 'Fl_Item_Type' => $data['type']));

        return true;
    }

    function unfollow($data, $encoded = FALSE) {

        if ($encoded) {
            $itemID = decode_id($data['id']);
        } else {
            $itemID = $data['id'];
        }

        $this->db->where('Fl_User_ID', $this->LoggedInUser);
		$this->db->where('Fl_Item_ID', $itemID);
		$this->db->where('Fl_Item_Type', $data['type']);
        $this->db->delete($this->tableName);
        
        return true;
    }
}
?>
