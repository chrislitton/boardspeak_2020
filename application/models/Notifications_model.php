<?php
class Notifications_model extends CI_Model {

	public function __construct() {
			$this->load->database();
	}

    public function get_Notification($id) {
		$query = $this->db->get_where('bs_notification', array('No_ID' => $id));
		return $query->row_array();
	}

	public function comment_todo_notification_owen($id){

		$this->db->order_by('bs_notification.No_ID' , 'DESC' );
		$this->db->select('bs_notification.* , bs_comments.* ,bs_posts.* , bs_users.*');
		$this->db->from('bs_notification');
		$this->db->join('bs_comments' , 'bs_comments.Co_ID = bs_notification.No_To'  );
		$this->db->join('bs_posts' , 'bs_posts.Po_ID = bs_comments.Co_Po_ID'  );
		$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From_Type'  );
		$this->db->where('bs_notification.No_From' , $id);
		$this->db->where_in('bs_notification.No_Type' ,[   'todo_post_notification_own']);
		$this->db->where_in('bs_notification.No_Status' , ['unread' , 'done' , 'inprogress']);

		$query = 	$this->db->get()->result_array();

		return $query;
	}

	public function comment_todo_notification($id){

					$this->db->order_by('bs_notification.No_ID' , 'DESC' );
					$this->db->select('bs_notification.* , bs_comments.* ,bs_posts.* , bs_users.*');
					$this->db->from('bs_notification');
					$this->db->join('bs_comments' , 'bs_comments.Co_ID = bs_notification.No_To'  );
					$this->db->join('bs_posts' , 'bs_posts.Po_ID = bs_comments.Co_Po_ID'  );
					$this->db->join('bs_users' , 'bs_users.Us_ID = bs_comments.Co_Us_ID'  );
					$this->db->where('bs_notification.No_From' , $id);
					$this->db->where_in('bs_notification.No_Type' ,[ 'todo_post_notification' , 'todo_reminder_post_notification']);
					$this->db->where_in('bs_notification.No_Status' , ['unread' , 'done' , 'inprogress']);

					$query = 	$this->db->get()->result_array();

					return $query;
		}

	public function comment_menion_notification($id){

				$this->db->order_by('bs_notification.No_ID', 'DESC');
    			$this->db->select('bs_notification.* , bs_users.* , bs_comments.* , bs_posts.*')->from('bs_notification');
				$this->db->join('bs_comments' , 'bs_comments.Co_ID = bs_notification.No_To_Type');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From');
				$this->db->join('bs_posts' , 'bs_posts.Po_ID = bs_comments.Co_Po_ID'  );

      			$this->db->where('bs_notification.No_Type' , 'tag_in_comment_post_notification' );
    			$this->db->where('bs_notification.No_To' , $id);
      			$this->db->where_in('bs_notification.No_Status' , ['unread' ,'done']);

	   			 $query = $this->db->get();

     			 return $query->result_array() ;

	}
		public function comment_post_notification($id){

    		$this->db->order_by('bs_notification.No_ID', 'DESC');
        			$this->db->select('bs_notification.* , bs_users.* , bs_comments.* , bs_posts.*')->from('bs_notification');
					$this->db->join('bs_comments' , 'bs_comments.Co_ID = bs_notification.No_To_Type');
					$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From');
					$this->db->join('bs_posts' , 'bs_posts.Po_ID = bs_comments.Co_Po_ID'  );
          			$this->db->where('bs_notification.No_Type' , 'comment_post_notification' );
        			$this->db->where('bs_notification.No_To' , $id);
          			$this->db->where_in('bs_notification.No_Status' , ['unread' , 'done']);

        			 $query = $this->db->get();

         			 return $query->result_array() ;

    	}

    	public function get_user_Comment_Notification($userID){


    					$this->db->select('bs_notification.No_ID')->from('bs_notification');
    					$this->db->where('bs_notification.No_To' , $userID);
    					$this->db->where('bs_notification.No_Type' , 'comment_post_notification');
    					$this->db->where_in('bs_notification.No_Status' , ['unread' , 'done']);
    					$query = 	$this->db->get();

     					return	$query->num_rows();
    	}

    			public function addNoteForNotification(){
 				 $data = array('No_To_Type' =>  $_POST['noteText'] );
    			 $this->db->where('No_ID', $_POST['commentId']);
    			 $query =$this->db->update('bs_notification', $data);
				 return $query;
    			}

				function inprogress(){

				}
			public function DoneTodo(){

				if(isset($_POST['type']) && $_POST['type'] == 'done' || $_POST['type'] == 'unread'){
					if($_POST['type'] == 'unread'){
							$data = array('No_Status' => 'unread');
					}else{
							$data = array('No_Status' => 'done');
					}
				}else{
				    $data = array('No_Status' => 'read');
				}


				if(isset($_POST['noticomment'])){
					$this->db->where('No_To', $_POST['noticomment']);
				}else{
					$this->db->where('No_ID', $_POST['commentId']);
				}

    		$query = $this->db->update('bs_notification', $data);
			 return $query;
			}
    	public function get_user_todo_Notification($userID){
//					print_r($this->LoggedInUser);
//							print_r($userID);
//							exit;
    				$this->db->select('bs_notification.No_ID');
    				$this->db->from('bs_notification');
    				$this->db->where('bs_notification.No_To' , $userID);
    				$this->db->where_in('bs_notification.No_Type' , ['todo_post_notification' , 'todo_reminder_post_notification']);
    					$this->db->where_in('bs_notification.No_Status' , ['unread' , 'done']);
    				$query = $this->db->get();
    				return $query->num_rows();

    	}

		function send_user_voucher(){
 			$where = array(
				'Vr_From_user' => $this->LoggedInUser,

			);
			 $this->db->select('bs_vouchers.* , bs_users.*, bs_posts.Po_Thumb')->from('bs_vouchers');
			 $this->db->where($where);
				$this->db->join('bs_posts' , 'bs_posts.Po_ID = bs_vouchers.Vr_post_id');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_vouchers.Vr_Us_ID');
			$res =	$this->db->get()->result_array();

			return	 $res ;
//
//			$where = array(
//				'rs_rewardcoin_send.Rs_Us_ID' => 2,
//
//			);
//			 $this->db->select('rs_rewardcoin_send.* , bs_posts.Po_Thumb ')->from('rs_rewardcoin_send');
//					$this->db->where($where);
//					$this->db->join('bs_posts' , 'bs_posts.Po_ID = rs_rewardcoin_send.Rs_post_id')	;
////					$this->db->join('bs_vouchers' , 'bs_vouchers.Vr_post_id = rs_rewardcoin_send.Rs_post_id');
//					$res = 	$this->db->get()->result_array();
//
//			$data = array();
//			for($x = 0; $x < count($res); $x++){
//
//					 if(count(json_decode($res[$x]['Rs_userinsertID']))> 0 ){
//						array_push($data , $res[$x]);
//					 }
//			}
//
//			return	 $data ;
		}
		function get_user_voucher($userID){

 					$where = array(
					'Vr_Us_ID' => $userID,

				);
 					$res = $this->db->select('bs_vouchers.* , bs_posts.Po_Thumb')->from('bs_vouchers')->where($where)->join('bs_posts' , 'bs_posts.Po_ID = bs_vouchers.Vr_post_id'  )->get()->result_array();

				 return	 $res ;


		}
		public  function get_user_mention_Notification($userID){
						$this->db->select('bs_notification.No_ID');
						$this->db->from('bs_notification');
						$this->db->where('bs_notification.No_Type' , 'tag_in_comment_post_notification');
						$this->db->where('bs_notification.No_To' , $userID);
					    $this->db->where_in('bs_notification.No_Status' , array('unread' ,'done'));
						$query = $this->db->get();

						return $query->num_rows();

		}
	public function get_Useractivity_new($id){

		$this->db->order_by('bs_notification.No_ID', 'DESC');
		$this->db->select('bs_notification.* , bs_users.*')->from('bs_notification');
		$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From');
		$this->db->where('bs_notification.No_Type' ,  'followed_post_notification' );
		$this->db->where('bs_notification.No_To' , $id);
		$this->db->where_in('bs_notification.No_Status' , array('unread'  ));
		$query = $this->db->get();

		$this->db->order_by('bs_notification.No_ID', 'DESC');
		$this->db->select('bs_notification.* , bs_users.* , bs_groups.*')->from('bs_notification');
		$this->db->join('bs_groups' , 'bs_groups.Gr_ID = bs_notification.No_From');
		$this->db->join('bs_users' , 'bs_users.Us_ID = bs_groups.Gr_Us_ID');
		$this->db->where_in('bs_notification.No_Type' ,array( 'creator_role_invite' , 'approve_request' , 'reject_request', 'member_out' , 'assign_role') );
		$this->db->where('bs_notification.No_To' , $id);
		$this->db->where_in('bs_notification.No_Status' , array(    'approve' ,'approved' , 'rejected' , 'removed' , 'unread'));
		$query1 = $this->db->get();

		$this->db->order_by('bs_notification.No_ID', 'DESC');
		$this->db->select('bs_notification.* , bs_users.* , bs_groups.*')->from('bs_notification');
		$this->db->join('bs_groups' , 'bs_groups.Gr_ID = bs_notification.No_From');
		$this->db->join('bs_users' , 'bs_users.Us_ID = bs_groups.Gr_Us_ID');
		$this->db->where('bs_notification.No_To' , $id);
		$this->db->where_in('bs_notification.No_Type' , array('invite_accepted' ,'invite_rejected'  ) );
		$this->db->where_in('bs_notification.No_Status' , array( 'accept' , 'reject'));
		$query2   = $this->db->get();



		$this->db->order_by('bs_notification.No_ID', 'DESC');
		$this->db->select('bs_notification.* , bs_users.* ')->from('bs_notification');
		$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From');
		$this->db->where('bs_notification.No_To' , $id);
		$this->db->where_in('bs_notification.No_Type' , array(  'creator_role_invite_reject_me' , 'creator_role_invite_reject' , 'creator_role_invite_accept_me' , 'creator_role_invite_accept') );
		$this->db->where_in('bs_notification.No_Status' , array(  'accept' , 'reject'));
		$query3   = $this->db->get();



		$newarray =  array_merge( $query->result_array(), $query1->result_array());

		$newarray =  array_merge(   $newarray , 	 $query2->result_array() );
		$newarray =  array_merge(   $newarray , 	$query3->result_array() );
		function date_sortz($a, $b) {
			return  strtotime($b['No_DatePosted']) - strtotime($a['No_DatePosted']);
		}
		usort($newarray, "date_sortz");


		return   $newarray ;

	}
	public function get_Useractivity($id){

			$this->db->order_by('bs_notification.No_ID', 'DESC');
			$this->db->select('bs_notification.* , bs_users.*')->from('bs_notification');
			$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From');
  			$this->db->where('bs_notification.No_Type' ,  'followed_post_notification' );
			$this->db->where('bs_notification.No_To' , $id);
  			$this->db->where_in('bs_notification.No_Status' , array('unread' , 'seen'));
 			$query = $this->db->get();

			$this->db->order_by('bs_notification.No_ID', 'DESC');
			$this->db->select('bs_notification.* , bs_users.* , bs_groups.*')->from('bs_notification');
			$this->db->join('bs_groups' , 'bs_groups.Gr_ID = bs_notification.No_From');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_groups.Gr_Us_ID');
  			$this->db->where_in('bs_notification.No_Type' ,array( 'creator_role_invite' , 'approve_request' , 'reject_request', 'member_out' , 'assign_role') );
			$this->db->where('bs_notification.No_To' , $id);
  			$this->db->where_in('bs_notification.No_Status' , array(   'seen', 'approve' ,'approved' , 'rejected' , 'removed' , 'unread'));
 			 $query1 = $this->db->get();

			 	$this->db->order_by('bs_notification.No_ID', 'DESC');
				$this->db->select('bs_notification.* , bs_users.* , bs_groups.*')->from('bs_notification');
				$this->db->join('bs_groups' , 'bs_groups.Gr_ID = bs_notification.No_From');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_groups.Gr_Us_ID');
				$this->db->where('bs_notification.No_To' , $id);
				$this->db->where_in('bs_notification.No_Type' , array('invite_accepted' ,'invite_rejected'  ) );
 				$this->db->where_in('bs_notification.No_Status' , array( 'seen','accept' , 'reject'));
 				$query2   = $this->db->get();



				$this->db->order_by('bs_notification.No_ID', 'DESC');
				$this->db->select('bs_notification.* , bs_users.* ')->from('bs_notification');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From');
				$this->db->where('bs_notification.No_To' , $id);
				$this->db->where_in('bs_notification.No_Type' , array(  'creator_role_invite_reject_me' , 'creator_role_invite_reject' , 'creator_role_invite_accept_me' , 'creator_role_invite_accept') );
				$this->db->where_in('bs_notification.No_Status' , array( 'seen','accept' , 'reject'));
				$query3   = $this->db->get();


				$this->db->order_by('bs_notification.No_ID', 'DESC');
				$this->db->select('bs_notification.* , bs_users.* ')->from('bs_notification');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_To');
				$this->db->where('bs_notification.No_To' , $id);
				$this->db->where_in('bs_notification.No_Type' , array(  'leavegroup' , 'adminleave'   ) );
				$this->db->where_in('bs_notification.No_Status' , array( 'seen','unread'));
				$query4   = $this->db->get();
				$this->db->order_by('bs_notification.No_ID', 'DESC');
				$this->db->select('bs_notification.* , bs_users.* ')->from('bs_notification');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_notification.No_From');
				$this->db->where('bs_notification.No_To' , $id);
				$this->db->where_in('bs_notification.No_Type' , array(   'adminleave'   ) );
				$this->db->where_in('bs_notification.No_Status' , array( 'seen','unread'));
				$query5   = $this->db->get();
					   $newarray =  array_merge( $query->result_array(), $query1->result_array());

			   $newarray =  array_merge(   $newarray , 	 $query2->result_array() );
			   $newarray =  array_merge(   $newarray , 	$query3->result_array() );
				$newarray =  array_merge(   $newarray , 	$query4->result_array() );
					$newarray =  array_merge(   $newarray , 	$query5->result_array() );
				function date_sort($a, $b) {
					return  strtotime($b['No_DatePosted']) - strtotime($a['No_DatePosted']);
					}
				usort($newarray, "date_sort");


  				 return   $newarray ;

	}

		public function get_user_activies_counter($userID){
				$this->db->select('bs_notification.No_ID ')->from('bs_notification');
			    $this->db->where('bs_notification.No_To' , $userID);
				$this->db->where_in('bs_notification.No_Type' , ['followed_post_notification' ,'approve_request' , 'reject_request', 'member_out' , 'assign_role' ,'invite_accepted' ,'invite_rejected']);
				$this->db->where_in('bs_notification.No_Status' ,  ['accept' , 'reject' , 'unread', 'approve' ,'approved' , 'rejected' , 'removed' ]);
				$query = $this->db->get();
// 				echo "<pre>"
// 				print_r($query);
// 				exit;
				return $query->num_rows();

		}

		public function get_UserNotification_post_joins($id) {
     			$query = $this->db->get_where('bs_groups', array('Gr_Us_ID' => $id));
    			$querydata = $query->result_array();

    			$dataobjec = array();
    			$finalarraySend = array();
    			for($i = 0 ; $i <  count($querydata); $i++ ){

           		$this->db->order_by('bs_notification.No_ID', 'DESC');
    			$this->db->select('bs_notification.* ,bs_members.* , bs_users.Us_Name , bs_users.Us_Thumb ,bs_users.Us_Alias,
    			   bs_users.Us_ID as userinviteID')
    					 ->from('bs_notification')
    					 ->join('bs_users', 'bs_notification.No_From = bs_users.Us_ID')
     					->join('bs_members', 'bs_notification.No_From_Type = bs_members.Me_ID');

    		     $this->db->where('bs_notification.No_To' , $querydata[$i]['Gr_ID'] );
  		      	 $this->db->where('bs_notification.No_Status' , 'unread' );
      				     $this->db->where_in('bs_notification.No_Type', ['post_join_via_link' , 'post_join_via_be_a_member' , 'topic_join_via_link']);
    			 $query = $this->db->get();
     				array_push($dataobjec, $query->result_array());
    			}

      			for($i = 0 ; $i <  count($dataobjec); $i++ ){

     				 for($x = 0 ; $x <  count($dataobjec[$i] ); $x++ ){
     				     array_push($finalarraySend, $dataobjec[$i][$x]);

     				 }
    				}


    			  usort($finalarraySend, function ($one, $two) {
    				if ($one['No_DatePosted'] === $two['No_DatePosted']) {
    					return 0;
    				}
    				return $one['No_DatePosted'] > $two['No_DatePosted'] ? -1 : 1;
    			});

    			 return $finalarraySend;

    	}
			public	function get_group_invite_creator($id){

				$this->db->select('bs_notification.* , bs_groups.* , bs_users.*');
				$this->db->from('bs_notification');
				$this->db->join('bs_groups' , 'bs_groups.Gr_ID = bs_notification.No_From');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_groups.Gr_Us_ID');
				$this->db->where('bs_notification.No_To' , $id);
				$this->db->where('bs_notification.No_Type' , 'creator_role_request');
				$this->db->where('bs_notification.No_Status' , 'unread');
				$query = 	$this->db->get()->result_array();

				 return $query;

	}
   	 	public	function get_group_invite($id){


					$this->db->select('bs_notification.*,
					  bs_notification.No_Type,  bs_notification.No_Title, 
 	 			       
 	 			        bs_users.Us_ID,  bs_users.Us_Thumb,  bs_users.Us_Alias,  bs_users.Us_Name,
			  		    ,bs_groups.Gr_Slug,bs_groups.Gr_Name'


					);
                    $this->db->from('bs_notification');
// 					$this->db->join('bs_members' , 'bs_members.Me_Us_ID = '.$id );
					$this->db->join('bs_groups' , 'bs_groups.Gr_ID = bs_notification.No_From' );
					$this->db->join('bs_users' , 'bs_users.Us_ID = bs_groups.Gr_Us_ID' );
 					$this->db->where('bs_notification.No_To' , $id);
					$this->db->where('bs_notification.No_Type' , 'group_invite');
 				    $this->db->where('bs_notification.No_Status' , 'unread');

					$query = 	$this->db->get()->result_array();
//					echo '<pre>';
//
//					print_r($query);
//				exit;
 				return $query;
    	}
	public function get_UserNotification($id) {

 			$query = $this->db->get_where('bs_groups', array('Gr_Us_ID' => $id));
			$querydata = $query->result_array();
			$dataobjec = array();
			$finalarraySend = array();
			for($i = 0 ; $i <  count($querydata); $i++ ){

       		$this->db->order_by('bs_notification.No_ID', 'DESC');
			$this->db->select('bs_notification.* , bs_users.Us_Name , bs_users.Us_Thumb , bs_users.Us_Alias,
			   bs_users.Us_ID as userinviteID')
					 ->from('bs_notification')
					 ->join('bs_users', 'bs_notification.No_From = bs_users.Us_ID');
		 	         $this->db->where('bs_notification.No_To' , $querydata[$i]['Gr_ID'] );
 			     	 $this->db->where('bs_notification.No_Status' , 'unread' );
  				     $this->db->where_in('bs_notification.No_Type', ['request_join','group_join_via_follow','group_join_via_link','42','86']);
			 $query = $this->db->get();
 				array_push($dataobjec, $query->result_array());
			}

  			for($i = 0 ; $i <  count($dataobjec); $i++ ){

 				 for($x = 0 ; $x <  count($dataobjec[$i] ); $x++ ){
 				     array_push($finalarraySend, $dataobjec[$i][$x]);

 				 }
				}


			  usort($finalarraySend, function ($one, $two) {
				if ($one['No_DatePosted'] === $two['No_DatePosted']) {
					return 0;
				}
				return $one['No_DatePosted'] > $two['No_DatePosted'] ? -1 : 1;
			});

			 return $finalarraySend;

	}
public function counterUserNotification($id) {


 			$query = $this->db->get_where('bs_groups', array('Gr_Us_ID' => $id));
			$querydata = $query->result_array();


			$dataobjec = array();
			$finalarraySend = array();
			for($i = 0 ; $i <  count($querydata); $i++ ){

       		$this->db->order_by('bs_notification.No_ID', 'DESC');
			$this->db->select('bs_notification.* , bs_users.Us_Name , bs_users.Us_Thumb ,
		    bs_users.Us_ID as userinviteID')->from('bs_notification')->join('bs_users', 'bs_notification.No_From = bs_users.Us_ID');

 			     	 $this->db->where('bs_notification.No_Status' , 'unread' );
  				     $this->db->where_in('bs_notification.No_Type', array('request_join','group_join_via_follow','group_join_via_link' ));
 					 $this->db->where('bs_notification.No_To' , $querydata[$i]['Gr_ID'] );
//                 	 $this->db->or_where('bs_notification.No_To' , $querydata[$i]['Gr_Us_ID'] );
			 	     $query = $this->db->get();
 				array_push($dataobjec, $query->result_array());
			}

  			for($i = 0 ; $i <  count($dataobjec); $i++ ){

 				 for($x = 0 ; $x <  count($dataobjec[$i] ); $x++ ){
 				     array_push($finalarraySend, $dataobjec[$i][$x]);

 				 }
				}
			  usort($finalarraySend, function ($one, $two) {
				if ($one['No_DatePosted'] === $two['No_DatePosted']) {
					return 0;
				}
				return $one['No_DatePosted'] > $two['No_DatePosted'] ? -1 : 1;
			});

			 return $finalarraySend;
	}

	public function add_notification($type, $title, $message, $from, $from_type, $to, $to_type) {
	    $this->load->helper('url');
	    $this->load->helper('date'); // load Helper for Date
		$now = date('Y-m-d H:i:s');

			if($title == 'You have been approved'){
			$statusof = 'approved';
			}else if($title ==  'You have been removed'){
				$statusof = 'removed';
			}else if($title == 'You have been rejected'){
				$statusof = 'rejected';
			}else if($title == 'user accepet the request'){
			$statusof = 'accept';
			}else if($title == 'user reject the request'){
			$statusof = 'reject';
			}
			 else{
				$statusof = 'unread';
			}


	    $data = array(
			'No_Type' => $type,
			'No_Title' => $title,
			'No_Message' => $message,
			'No_From' => $from,
			'No_From_Type' => $from_type,
			'No_To' => $to,
			'No_To_Type' => $to_type,
			'No_Status' => $statusof,
			'No_DatePosted' => $now
	    );

	    $this->db->insert('bs_notification', $data);
		return $this->db->insert_id();
	}

	public function delete_row($id) {
		$this->db->delete('bs_notification', array('No_ID' => $id));
	}
}
