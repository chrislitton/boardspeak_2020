<?php
class Contacts_model extends CI_Model {

	private $tableName = "bs_contacts";
	private $LoggedInUser;

	public function __construct() {
		$this->load->database();
		$this->load->library('session');
		if (isset($this->session->userdata['logged_in'])){
            $this->LoggedInUser = $this->session->userdata['logged_in']['bs_id'];
        }

	}

	function show_user_contact_count($userID, $keyword) {
		$requests = $this->get_user_contact_requests_count($userID, $keyword);
		$suggested = $this->get_user_contact_of_contacts_count($userID, $keyword);
		$contacts = $this->get_user_contacts_count($userID, $keyword);
		$group_join = $this->get_user_group_join($userID, $keyword);
		$group_follow = $this->get_user_group_join($userID, $keyword);

		return array('requests' => $requests,
		 'suggested' => $suggested,
		 'group_follow' => $group_follow,
		 'group_join' => $group_join,
		 'contacts' => $contacts);
	}

	function get_user_contact_requests_count($userID, $keyword) {
		$this->db->select("c.*");
		$this->db->from($this->tableName . " c");
		$this->db->join("bs_users u", "c.Co_Contact_ID = u.Us_ID", "LEFT");
		$this->db->where("c.Co_Us_ID", $userID);
		if ($keyword != "") {
			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
			$this->db->group_end();
		}
		$this->db->where("c.Co_Status", "pending");

		return $this->db->count_all_results();
	}

	function get_user_contact_of_contacts_count($userID, $keyword) {
		$this->db->select("c.Co_Contact_ID");
		$this->db->from($this->tableName . " c");
		$this->db->where("c.Co_Us_ID", $userID);
		$this->db->where("c.Co_Status", "pending");

		$pending = $this->db->get()->result_array();

		$pending_list = array();
		foreach($pending as $key => $contact) {
			array_push($pending_list, $contact['Co_Contact_ID']);
		}

		$this->db->select("c.Co_Contact_ID");
		$this->db->from($this->tableName . " c");
		$this->db->where("c.Co_Us_ID", $userID);
		$this->db->where("c.Co_Status", "blocked");

		$blocked = $this->db->get()->result_array();

		$blocked_list = array();
		foreach($blocked as $key => $contact) {
			array_push($blocked_list, $contact['Co_Contact_ID']);
		}
		
		$this->db->flush_cache();

		$this->db->select("c.Co_Contact_ID");
		$this->db->from($this->tableName . " c");
		$this->db->where("c.Co_Us_ID", $userID);
		if (sizeOf($blocked_list) > 0)
			$this->db->where_not_in("c.Co_Contact_ID", $blocked_list);
		$this->db->where("c.Co_Status", "confirmed");

		$contacts = $this->db->get()->result_array();

		$contact_list = array();
		foreach($contacts as $key => $contact) {
			array_push($contact_list, $contact['Co_Contact_ID']);
		}

		$this->db->flush_cache();

		if (sizeof($contact_list) > 0) {
			$this->db->select("c.*");
			$this->db->from($this->tableName . " c");
			$this->db->join("bs_users u", "c.Co_Contact_ID = u.Us_ID", "LEFT");
			if ($keyword != "") {
				$this->db->group_start();
				$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
				$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
				$this->db->group_end();
			}
			if (sizeOf($contact_list) > 0)
				$this->db->where_not_in("u.Us_ID", $contact_list);
			if (sizeOf($blocked_list) > 0)
				$this->db->where_not_in("u.Us_ID", $blocked_list);
			if (sizeOf($pending_list) > 0)
				$this->db->where_not_in("u.Us_ID", $pending_list);
			$this->db->where("c.Co_Contact_ID != " . $userID);
			$this->db->where("u.Us_IsPrivate", 0);
			$this->db->where("c.Co_Status", "confirmed");
			$this->db->where("c.Co_Status != 'blocked'");
			$this->db->group_by("u.Us_ID");
			
			return $this->db->count_all_results();
		} else {
			return 0;
		}

	}

	function get_user_contacts_count($userID, $keyword) {
		$this->db->select("c.*");
		$this->db->from($this->tableName . " c");
		$this->db->join("bs_users u", "c.Co_Contact_ID = u.Us_ID", "LEFT");
		$this->db->where("c.Co_Us_ID", $userID);
		if ($keyword != "") {
			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
			$this->db->group_end();
		}
		$this->db->where("c.Co_Status", "confirmed");

		return $this->db->count_all_results();
	}
	function get_user_group_join($userID, $keyword){

	$this->db->select('bs_members.*');
	$this->db->from('bs_members');
	$this->db->where('Me_Us_ID' , $userID);
	$this->db->where('Me_Status' , 'active');
    $this->db->where('Me_Role' , 'member');
	 return $this->db->count_all_results();

	}

			function show_contact_explore_list($userID, $keyword, $sort){
			$result = $this->get_contact_explore_list($userID, $keyword, $sort, 'true');

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;
				$result[$key]->Us_URL = base_url() . 'u/' . encode_id($row->Us_ID);
			}
		}

		$grid = array(
			'totalRecords' => count($result),
			'data' => $result
		);

		return $grid;


			}
	function show_contact_request_list($userID, $keyword, $sort) {



		$result = $this->get_contact_request_list($userID, $keyword, $sort);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;
				$result[$key]->Us_Request_Since = get_time_between($row->Co_Request_Date);
				$result[$key]->Us_URL = base_url() . 'u/' . encode_id($row->Us_ID);
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_contact_request_list($userID, $keyword, $sort) {

		$this->db->select("u.*, c.*");
		$this->db->from($this->tableName . " c");
		$this->db->join("bs_users u", "c.Co_Contact_ID = u.Us_ID", "LEFT");
		$this->db->where("c.Co_Us_ID", $userID);
		if ($keyword != "") {
			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
			$this->db->group_end();
		}
		$this->db->where("c.Co_Status", "pending");

		if ($sort != "") {
			$data_sort = explode("_", $sort);
			$sort_by = "";
			if ($data_sort[0] == "name") {
				$sort_by = "u.Us_Name";
			} else {
				$sort_by = "c.Co_Request_Date";
			}
			$this->db->order_by($sort_by, $data_sort[1]);
		}

		$query = $this->db->get();
		$data = $query->result();

		$this->cnt = $this->get_user_contact_requests_count($userID, $keyword);

		return $data;
	}

	function show_contact_suggest_list($userID, $keyword, $sort) {
		$result = $this->get_contact_suggest_list($userID, $keyword, $sort);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;
				$result[$key]->Has_Pending_Invite = $this->has_pending_contact_invite($row->Us_ID);
				$result[$key]->Us_URL = base_url() . 'u/' . encode_id($row->Us_ID);
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_contact_suggest_list($userID, $keyword, $sort) {
		$this->db->select("c.Co_Contact_ID");
		$this->db->from($this->tableName . " c");
		$this->db->where("c.Co_Us_ID", $userID);
		$this->db->where("c.Co_Status", "pending");

		$pending = $this->db->get()->result_array();

		$pending_list = array();
		foreach($pending as $key => $contact) {
			array_push($pending_list, $contact['Co_Contact_ID']);
		}

		$this->db->select("c.Co_Contact_ID");
		$this->db->from($this->tableName . " c");
		$this->db->where("c.Co_Us_ID", $userID);
		$this->db->where("c.Co_Status", "blocked");

		$blocked = $this->db->get()->result_array();

		$blocked_list = array();
		foreach($blocked as $key => $contact) {
			array_push($blocked_list, $contact['Co_Contact_ID']);
		}

		$this->db->flush_cache();

		$this->db->select("c.Co_Contact_ID");
		$this->db->from($this->tableName . " c");
		$this->db->where("c.Co_Us_ID", $userID);
		if (sizeOf($blocked_list) > 0)
			$this->db->where_not_in("c.Co_Contact_ID", $blocked_list);
		$this->db->where("c.Co_Status", "confirmed");

		$contacts = $this->db->get()->result_array();
		$contact_list = array();
		foreach($contacts as $key => $contact) {
			array_push($contact_list, $contact['Co_Contact_ID']);
		}

		$this->db->flush_cache();
		$data = array();
		if (sizeof($contact_list) > 0) {
			$this->db->select("u.*, c.*");
			$this->db->from($this->tableName . " c");
			$this->db->join("bs_users u", "c.Co_Contact_ID = u.Us_ID", "LEFT");
			if ($keyword != "") {
				$this->db->group_start();
				$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
				$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
				$this->db->group_end();
			}
			if (sizeOf($contact_list) > 0)
			$this->db->where_not_in("u.Us_ID", $contact_list);
			if (sizeOf($blocked_list) > 0)
				$this->db->where_not_in("u.Us_ID", $blocked_list);
			if (sizeOf($pending_list) > 0)
				$this->db->where_not_in("u.Us_ID", $pending_list);
			$this->db->where("c.Co_Contact_ID != " . $userID);
			$this->db->where("u.Us_IsPrivate", 0);
			$this->db->where("c.Co_Status", "confirmed");
			$this->db->group_by("u.Us_ID");
			
			if ($sort != "") {
				$data_sort = explode("_", $sort);
				$sort_by = "";
				if ($data_sort[0] == "name") {
					$sort_by = "u.Us_Name";
					$this->db->order_by($sort_by, $data_sort[1]);
				}
			}
			
			$query = $this->db->get();
			$data = $query->result();
		}



		$this->cnt = $this->get_user_contact_of_contacts_count($userID, $keyword);

		return $data;
	}

	function show_contact_list($userID, $keyword, $sort, $showPrivate = "true") {
		$result = $this->get_contact_list($userID, $keyword, $sort, $showPrivate);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;
				$result[$key]->Us_URL = base_url() . 'u/' . encode_id($row->Us_ID);
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_contact_explore_list($userID, $keyword='', $sort='', $showPrivate='false') {
		$this->db->select("bs_users.*");
		$this->db->from('bs_users' );

	    if ($keyword != "") {
			$this->db->group_start();
			$this->db->where("Us_Name LIKE '%".$keyword."%'");
			$this->db->or_where("CONCAT(Us_FName, ' ', Us_LName) LIKE '%".$keyword."%'");
			$this->db->group_end();
		}
// 		$this->db->where("c.Co_Status", "confirmed");
// 		if ($showPrivate == "false") {
// 			$this->db->where("Us_IsPrivate != 1");
// 		}

// 		if ($sort != "") {
// 			$data_sort = explode("_", $sort);
// 			$sort_by = "";
// 			if ($data_sort[0] == "name") {
// 				$sort_by = "u.Us_Name";
// 			}
// 			$this->db->order_by($sort_by, $data_sort[1]);
// 		}

		$query = $this->db->get();
		$data = $query->result();

// 		$this->cnt = $this->get_user_contacts_count($userID, $keyword);

		return $data;
	}
	function get_contact_list($userID, $keyword='', $sort='', $showPrivate='false') {
		$this->db->select("u.*, c.*");
		$this->db->from($this->tableName . " c");
		$this->db->join("bs_users u", "c.Co_Contact_ID = u.Us_ID", "LEFT");
		$this->db->where("c.Co_Us_ID", $userID);
		if ($keyword != "") {
			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
			$this->db->group_end();
		}
		$this->db->where("c.Co_Status", "confirmed");
		if ($showPrivate == "false") {
			$this->db->where("u.Us_IsPrivate != 1");
		}

		if ($sort != "") {
			$data_sort = explode("_", $sort);
			$sort_by = "";
			if ($data_sort[0] == "name") {
				$sort_by = "u.Us_Name";
			} else {
				$sort_by = "c.Co_Contact_Date";
			}
			$this->db->order_by($sort_by, $data_sort[1]);
		}

		$query = $this->db->get();
		$data = $query->result();

		$this->cnt = $this->get_user_contacts_count($userID, $keyword);

		return $data;
	}

	function respond_to_user_contact_request($user_id, $action) {
		$user = $this->users_model->get_Users($this->LoggedInUser);
		if ($action == "accept") {
			$date = date("Y-m-d H:i:s");
			$data = array('Co_Status' => 'confirmed', 'Co_Contact_Date' => $date);
			$this->db->where('Co_Us_ID', $this->LoggedInUser);
			$this->db->where('Co_Contact_ID', $user_id);
			$this->db->update($this->tableName, $data);

			$contact_data = array('Co_Us_ID' => $user_id, 'Co_Contact_ID' => $this->LoggedInUser, 'Co_Status' => 'confirmed', 'Co_Contact_Date' => $date);
			$this->db->insert($this->tableName, $contact_data);

			$notif_type = "contact_accept";
			$subject = "Contact Request Accepted";
			$content =  $user['Us_Name'] . " accepted your contact request";
			$message = "Contact request accepted";

		} else {
			$this->db->where('Co_Us_ID', $this->LoggedInUser);
			$this->db->where('Co_Contact_ID', $user_id);
			$this->db->delete($this->tableName);

			$notif_type = "contact_decline";
			$subject = "Contact Request Decline";
			$content =  $user['Us_Name'] . " declined your contact request";
			$message = "Contact request decline";
		}

		$this->notifications_model->add_notification($notif_type, $subject, $content, $this->LoggedInUser, 'user', $user_id, 'user');

		$response = array('status' => 'success', 'message' => $message);

		return $response;
	}

	function add_block_user($user_id, $action) {
		$user = $this->users_model->get_Users($this->LoggedInUser);
		$date = date("Y-m-d H:i:s");
		if ($action == "add") {
			$data = array('Co_Us_ID' => $user_id, 'Co_Contact_ID' => $this->LoggedInUser, 'Co_Status' => 'pending', 'Co_Request_Date' => $date);
			$this->db->insert($this->tableName, $data);

			$notif_type = "contact_request";
			$subject = "Contact Request";
			$content =  $user['Us_Name'] . " sent a contact request";
			$message = "Contact request sent";

			$this->notifications_model->add_notification($notif_type, $subject, $content, $this->LoggedInUser, 'user', $user_id, 'user');

		} else {
			$data = array('Co_Us_ID' => $this->LoggedInUser, 'Co_Contact_ID' => $user_id, 'Co_Status' => 'blocked', 'Co_Block_Message' => 1, 'Co_Request_Date' => $date, 'Co_Contact_Date' => $date);
			$this->db->insert($this->tableName, $data);

			$notif_type = "user_blocked";
			$subject = "User Blocked";
			$content =  "User blocked";
			$message = "User blocked";
			
			$this->notifications_model->add_notification($notif_type, $subject, $content, $this->LoggedInUser, 'user', $this->LoggedInUser, 'user');
		}

		$response = array('status' => 'success', 'message' => $message);

		return $response;
	}

	function has_pending_contact_invite($user_id) {
		$this->db->select("c.Co_Contact_ID");
		$this->db->from($this->tableName . " c");
		$this->db->where("c.Co_Us_ID", $user_id);
		$this->db->where("c.Co_Contact_ID", $this->LoggedInUser);
		$this->db->where("c.Co_Status", "pending");

		$query = $this->db->get();
		$data = $query->result();

		return (sizeOf($data) > 0) ? true : false;
	}

	function cancel_pending_user_invite($user_id, $action) {
		$this->db->where('Co_Us_ID', $user_id);
		$this->db->where('Co_Contact_ID', $this->LoggedInUser);
		$this->db->delete($this->tableName);

		$response = array('status' => 'success', 'message' => 'Contact request cancelled');

		return $response;
	}

	function contact_favorite_action($user_id, $action) {
		$favorite = ($action == "mark") ? 1 : 0;

		$data = array('Co_Favorite' => $favorite);
		$this->db->where('Co_Us_ID', $this->LoggedInUser);
		$this->db->where('Co_Contact_ID', $user_id);
		$this->db->update($this->tableName, $data);

		$response = array('status' => 'success', 'message' => 'Contact '. $action .'ed as favourite');

		return $response;
	}

	function contact_block_message_action($user_id, $action) {
		$block = ($action == "block") ? 1 : 0;

		$data = array('Co_Block_Message' => $block);
		$this->db->where('Co_Us_ID', $this->LoggedInUser);
		$this->db->where('Co_Contact_ID', $user_id);
		$this->db->update($this->tableName, $data);

		$response = array('status' => 'success', 'message' => 'Contact message sending '. $action .'ed');

		return $response;
	}

	function remove_contact($user_id, $action) {
		$this->db->where('Co_Us_ID', $this->LoggedInUser);
		$this->db->where('Co_Contact_ID', $user_id);
		$this->db->delete($this->tableName);

		$this->db->where('Co_Us_ID', $user_id);
		$this->db->where('Co_Contact_ID', $this->LoggedInUser);
		$this->db->delete($this->tableName);

		$response = array('status' => 'success', 'message' => 'User removed from contacts list');

		return $response;
	}

	function get_contact_information($user_id) {
		$contact_info = array();

		// if I requested
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('Co_Us_ID', $user_id);
		$this->db->where('Co_Contact_ID', $this->LoggedInUser);

		$contact_section = $this->db->get()->result_array();

		if(sizeOf($contact_section) == 0) {
			$contact_info['to_user']['status'] = null;
			$contact_info['to_user']['message_blocked'] = 0;
			$contact_info['to_user']['favorite'] = 0;
		} else {
			$contact_info['to_user']['status'] = $contact_section[0]['Co_Status'];
			$contact_info['to_user']['message_blocked'] = $contact_section[0]['Co_Block_Message'];
			$contact_info['to_user']['favorite'] = $contact_section[0]['Co_Favorite'];

		}

		// if contact requested
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('Co_Us_ID', $this->LoggedInUser);
		$this->db->where('Co_Contact_ID', $user_id);

		$my_section = $this->db->get()->result_array();

		if (sizeOf($my_section) == 0) {
			$contact_info['to_me']['status'] = null;
			$contact_info['to_me']['message_blocked'] = 0;
			$contact_info['to_me']['favorite'] = 0;
		} else {
			$contact_info['to_me']['status'] = $my_section[0]['Co_Status'];
			$contact_info['to_me']['message_blocked'] = $my_section[0]['Co_Block_Message'];
			$contact_info['to_me']['favorite'] = $my_section[0]['Co_Favorite'];
		}

		return $contact_info;
	}

	function search_contact_for_group_invite($group, $keyword) {

		$result = $this->get_contact_for_group_invite($group, $keyword);

		if (!empty($result)) {
			foreach ($result as $key => $row) {

				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;
				$result[$key]->Me_Status = ($row->Me_Gr_ID == $group) ? TRUE : FALSE;
				$result[$key]->Us_URL = base_url() . 'u/' . encode_id($row->Us_ID);
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_contact_for_group_invite($group, $keyword) {

		$this->db->select("
		m.Me_Gr_ID,
		u.Us_ID, u.Us_Name, u.Us_FName, u.Us_LName,
		 u.Us_Email, u.Us_Company, u.Us_JobTitle, u.Us_photo, u.Us_Thumb  , c.Co_Us_ID , u.Us_ID");
		$this->db->from("bs_contacts c");
		$this->db->join("bs_users u", "u.Us_ID = c.Co_Contact_ID AND c.Co_Us_ID = " . $this->session->userdata['logged_in']['bs_id'], "LEFT");
 		$this->db->join("bs_members m", "u.Us_ID = m.Me_Us_ID", "LEFT");
 		$this->db->where("u.Us_Status", "approved");
		$this->db->where("c.Co_Status", "confirmed");
 		$this->db->where("m.Me_Parent", 'group');
 		$this->db->where("m.Me_Gr_ID", $group);
		$this->db->where("u.Us_ID != " . $this->session->userdata['logged_in']['bs_id']);
		$this->db->group_start();
		$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
		$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
		$this->db->or_where("u.Us_Email LIKE '%".$keyword."%'");
		$this->db->group_end();
		$this->db->group_by("u.Us_ID");

		$query = $this->db->get();
		$data = $query->result();
// 			print_r($query->result_array());
// 			exit;

 		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		$this->db->select("m.Me_Gr_ID, u.Us_ID, u.Us_Name, u.Us_FName,
		 u.Us_LName, u.Us_Email, u.Us_Company, u.Us_JobTitle, u.Us_photo, u.Us_Thumb , c.Co_Us_ID , u.Us_ID");
		$this->db->from("bs_contacts c");
		$this->db->join("bs_users u", "u.Us_ID = c.Co_Contact_ID AND
		 c.Co_Us_ID = " . $this->session->userdata['logged_in']['bs_id'], "LEFT");
		$this->db->join("bs_members m", "u.Us_ID = m.Me_Us_ID", "LEFT");
		$this->db->where("u.Us_Status", "approved");
		$this->db->where("c.Co_Status", "confirmed");
		$this->db->where("u.Us_ID != " . $this->session->userdata['logged_in']['bs_id']);
		$this->db->group_start();
		$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
		$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
		$this->db->or_where("u.Us_Email LIKE '%".$keyword."%'");
		$this->db->group_end();
		$this->db->group_by("u.Us_ID");

		$query2 = $this->db->get();
		$data2 = $query2->result();

		$query2 = $this->db->query('SELECT FOUND_ROWS() as COUNT');

		$this->cnt += $query2->row()->COUNT;

		return array_merge($data, $data2);
	}

}
