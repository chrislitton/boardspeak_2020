<?php
class Settings_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}



    public function get_Setting($id)
	{
		$query = $this->db->get_where('bs_settings', array('Se_ID' => $id));
		return $query->row_array();
	}


    public function get_Backcolor()
	{
		$this->db->order_by('Se_ID', 'DESC');
		$query = $this->db->get_where('bs_settings', array('Se_Type' => 'backcolor'));
		return $query->result_array();
	}
        
    public function get_Background($id = false)
	{
		if ($id === false)
		{
			$this->db->order_by('Se_ID', 'DESC');
			$query = $this->db->get_where('bs_settings', array('Se_Type' => 'background'));
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_settings', array('Se_ID' => $id));
		return $query->row_array();
	}
	
	public function delete_row($id)
	{
		$this->db->where('Se_ID',$id);
		$this->db->delete('bs_settings');
	}
	
	
	public function set_background($Name, $Value, $Active)
	{
	    $this->load->helper('url');	
			
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Se_Name' => $Name,
		'Se_Value' => $Value,
		'Se_Type' => 'Background',
		'Se_Active' => $Active,
		'Se_DatePosted' => $now
	    );
	
	    $this->db->insert('bs_settings', $data);
		return $this->db->insert_id();
	}
	
	public function set_photo($ID, $Type, $FileName)
	{	
		$Photo = base_url().'./uploads/'.$Type.'/'.$FileName;
		$Thumb = base_url().'./uploads/'.$Type.'/thumbnail/'.$FileName;
		
	    $data = array(
		'Se_Value' => $FileName,
		'Se_Photo' => $Photo,
		'Se_Thumb' => $Thumb
	    );
	
		$this->db->where('Se_ID', $ID);
	    return $this->db->update('bs_settings', $data);
	}
		
		
}
