<?php
class Background_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}

	public function get_Record($id = false)
	{
		if ($id === false)
		{
			$query = $this->db->order_by('BgC_Name', 'ASC');
			$query = $this->db->get('bs_bgcategory');
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_bgcategory', array('BgC_ID' => $id));
		return $query->row_array();
	}

	public function Insert($Name, $Order)
	{

	    $this->load->helper('url');
	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'BgC_Name' => $Name,
		'BgC_Order' => $Order,
		'BgC_DatePosted' => $now
	    );

	    $this->db->insert('bs_bgcategory', $data);
		return $this->db->insert_id();
	}


	public function Update($ID, $Name, $Order)
	{
	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'BgC_Name' => $Name,
		'BgC_Order' => $Order,
		'BgC_DatePosted' => $now
	    );

		$this->db->where('BgC_ID', $ID);
	    return $this->db->update('bs_bgcategory', $data);
	}

	public function delete_row($id)
	{
		$this->db->delete('bs_bgphotos', array('BgP_BgC_ID' => $id));
		$this->db->delete('bs_bgcategory', array('BgC_ID' => $id));
	}


	public function delete_bgphoto($id)
	{
		$this->db->delete('bs_bgphotos', array('BgP_ID' => $id));
	}


	public function get_PhotoRecord($id = false)
	{
		if ($id === false)
		{
			$query = $this->db->order_by('BgP_ID', 'DESC');
			$query = $this->db->get('bs_bgphotos');
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_bgphotos', array('BgP_ID' => $id));
		return $query->row_array();
	}


	public function get_BgPhotos($id)
	{
		$query = $this->db->get_where('bs_bgphotos', array('BgP_BgC_ID' => $id));
		return $query->result_array();
	}


	public function InsertPhoto($ID, $Name)
	{

	    $this->load->helper('url');
	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'BgP_BgC_ID' => $ID,
		'BgP_Name' => $Name,
		'BgP_DatePosted' => $now
	    );

	    $this->db->insert('bs_bgphotos', $data);
		return $this->db->insert_id();
	}

	public function set_photo($ID, $Type, $FileName)
	{
		$Photo = base_url().'./uploads/'.$Type.'/'.$FileName;
		$Thumb = base_url().'./uploads/'.$Type.'/thumbnail/'.$FileName;

	    $data = array(
		'BgP_Photo' => $Photo,
		'BgP_Thumb' => $Thumb
	    );

		$this->db->where('BgP_ID', $ID);
	    return $this->db->update('bs_bgphotos', $data);
	}

}
