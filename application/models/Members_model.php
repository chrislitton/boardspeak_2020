<?php
class Members_model extends CI_Model {

	private $Tablename = 'bs_members';
	private $LoggedInUser;

	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
		$this->load->library('EmailManager');
		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->LoggedInUser = $this->session->userdata['logged_in']['bs_id'];
		}
	}

	public function add_Member($id, $type, $role, $status, $userID = null, $isInvited = 0, $isViaLink = 0, $referrer = 0,$post_id = '',$topic_id = '') {

		$userID = ($userID == NULL) ? $this->LoggedInUser : $userID;
		$data = array(
						"Me_Parent" => $type,
						"Me_Gr_ID" => ($type == 'group' || $type == 'post' || $type == 'topic') ? $id : 0,
						"Me_To_ID" => $topic_id,
						"Me_St_ID" => $post_id ,
 					    "Me_Us_ID" => $userID,
						"Me_Role" => $role,
						"Me_IsInvited" => $isInvited,
						"Me_ViaLink" => $isViaLink,
						"Me_Ref_ID" => $referrer,
						"Me_Status" => $status,
						"Me_DatePosted" => date('Y-m-d h:i:s a', time())
					);

		$primaryID = $this->db->insert($this->Tablename, $data);
		 $lastid = $this->db->insert_id();
//
// 				print_r($lastid);
// 				exit;

		$this->updateMembersCount($type, $id);
		return $lastid;
	}

	public function updateMembersCount($type, $id) {
		if ($type == 'group') {
			$this->db->where('Me_Parent', $type);
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Status', 'active');
			$this->db->from($this->Tablename);
			$count = $this->db->count_all_results();
			$this->db->flush_cache();

			$data = array('Gr_Members' => $count);
			$this->db->where('Gr_ID', $id);
			$this->db->update('bs_groups', $data);
		}

		if ($type == 'topic') {
			$this->db->where('Me_Parent', $type);
			$this->db->where('Me_To_ID', $id);
			$this->db->where('Me_Status', 'active');
			$this->db->from($this->Tablename);
			$count = $this->db->count_all_results();
			$this->db->flush_cache();

			$data = array('To_Members' => $count);
			$this->db->where('To_ID', $id);
			$this->db->update('bs_topics', $data);
		}
	}

	public function check_if_member_record_access_exist($Parent, $GroupID, $TopicID = '', $SubTopicID = '', $Status="pending", $isInvited = NULL) {
		$this->db->select("bs_members.*");

		$this->db->from($this->Tablename);
 		$this->db->where('Me_Parent', $Parent);

		$this->db->where('Me_Gr_ID', $GroupID);
		if($TopicID){
		$this->db->where('Me_To_ID', $TopicID);
        }
		if($SubTopicID){
		$this->db->where('Me_St_ID', $SubTopicID);
        }

		$this->db->where('Me_Us_ID', $this->LoggedInUser);
 		$this->db->where('Me_Status', $Status);
//  		$this->db->where_in('Me_Status', $Status);

		if ($isInvited) {
			$this->db->where('Me_IsInvited', $isInvited);
		}

        $Count = $this->db->count_all_results();

        return $Count;
	}

	public function get_member_records($Parent, $Role="superadmin", $Status="active") {
		$return_array = array();

		$this->db->select("bs_members.*");
		$this->db->from('bs_members');
		$this->db->where('Me_Parent', $Parent);
		$this->db->where('Me_Us_ID', $this->LoggedInUser);
//		$this->db->where('Me_Role', $Role);
		$this->db->where('Me_Status', $Status);
        $data = $this->db->get()->result_array();

//		echo '<pre>';
//		print_r($data);
//		exit;
        if (!empty($data)) {
        	foreach ($data as $key => $row) {
		        if (strcasecmp($Parent, 'group') == 0) {
		        	$return_array[] = $row['Me_Gr_ID'];

		        } else if (strcasecmp($Parent, 'topic') == 0) {
		        	if($row['Me_To_ID'] != 0)
		        		$return_array[] = $row['Me_To_ID'];

		        } else if (strcasecmp($Parent, 'subtopic') == 0) {
		        	if($row['Me_St_ID'] != 0)
		        		$return_array[] = $row['Me_St_ID'];

		        }
        	}
        }

        return $return_array;
	}

	function get_admin_record($groupid , $userid){

		$this->db->select("bs_groups.*");
		$this->db->from('bs_groups');

		$this->db->where('Gr_Us_ID', $userid);
		$this->db->where('Gr_ID', $groupid);

		$data = $this->db->get()->result_array();
		if (sizeOf($data) > 0) {
			return array('status' => 'success', 'role' => 'creator'  );
		} else {
			return array('status' => 'non_member', 'role' => 'non_member' );
		}

	}
				function get_member_information_admin($id, $groupID){

					$userID =  $this->LoggedInUser  ;
					$this->db->select("bs_members.*");
							$this->db->from($this->Tablename);
							$this->db->where('Me_Parent', 'group');
							$this->db->where('Me_Gr_ID', $groupID);
							$this->db->where('Me_Us_ID', $userID);
							$this->db->where('Me_status' , 'active');
							$data = $this->db->get()->result_array();
				if (sizeOf($data) > 0) {
				   return array('status' => 'success', 'role' => $data[0]['Me_Role'], 'role_status' => $data[0]['Me_Status'], 'is_invited' => $data[0]['Me_IsInvited']);
						} else {
					return array('status' => 'non_member', 'role' => 'non_member' , 'role_status' => false, 'is_invited' => false);
				}
	}
		function get_member_information_POST ($id, $groupID){

// 						$userID = (isset($id)) ? $id :    ;

						$this->db->select("bs_members.*");
                        		$this->db->from($this->Tablename);
                        		$this->db->where('Me_Parent', 'post');
                        		$this->db->where('Me_Gr_ID', $groupID);
                        		$this->db->where('Me_St_ID', $id);
                         		$this->db->where('Me_Us_ID', $this->LoggedInUser);
                         		$this->db->where('Me_status' , 'active');
                        		$data = $this->db->get()->result_array();


							if (sizeOf($data) > 0) {
							   return array('status' => 'success', 'role' => $data[0]['Me_Role'], 'role_status' => $data[0]['Me_Status'], 'is_invited' => $data[0]['Me_IsInvited']);
									} else {
								return array('status' => 'non_member', 'role' => 'non_member' , 'role_status' => false, 'is_invited' => false);
							}
		}
			function get_member_POST_info ($id, $groupID ,$postID){
							$userID =  $this->LoggedInUser  ;
							$this->db->select("bs_members.*");
							$this->db->from($this->Tablename);

							$this->db->where('Me_Gr_ID', $groupID);
							$this->db->where('Me_St_ID' , $postID);
							$this->db->where('Me_Us_ID', $userID);
							$this->db->where('Me_Parent' , 'post');
							$this->db->where('Me_Status' , 'active');
							$data = $this->db->get()->result_array();


 							if (sizeOf($data) > 0) {
                               return array('status' => 'success', 'role' => $data[0]['Me_Role'], 'role_status' => $data[0]['Me_Status'], 'is_invited' => $data[0]['Me_IsInvited']);
                                    } else {
                            	return array('status' => 'non_member', 'role' => 'non_member' , 'role_status' => false, 'is_invited' => false);
                            }
        		}
	public function get_member_information($id, $type, $userID = NULL) {
		$userID = ($userID == NULL) ? $this->LoggedInUser : $userID;

		$this->db->select("bs_members.*");
		$this->db->from($this->Tablename);
		$this->db->where('Me_Parent', $type);

			if ($type == 'group') {

				$this->db->where('Me_Gr_ID', $id);

			} else if ($type == 'topic') {

				$this->db->where('Me_To_ID', $id);

			} else {

				$this->db->where('Me_St_ID', $id);

			}

 		$this->db->where('Me_Us_ID', $userID);
		$data = $this->db->get()->result_array();

		if (sizeOf($data) > 0) {
			return array('status' => 'success', 'role' => $data[0]['Me_Role'], 'role_status' => $data[0]['Me_Status'], 'is_invited' => $data[0]['Me_IsInvited'] , 'Me_ViaLink' => $data[0]['Me_ViaLink'], 'memberId' => $data[0]['Me_ID']);
 		} else {
			return array('status' => 'non_member', 'role' => 'non_member' , 'role_status' => false, 'is_invited' => false);
		}
	}


		public	function followfromtheprofile($data){
				$id =  $data['id'] ;
				$grop_admin_id =  $data['userName'] ;
				 $role = 'follower';
		         $userID = isset($data['user_id']) ? $data['user_id'] : $this->LoggedInUser;
		         	$user = $this->users_model->get_Users($userID);
					$referrer = $this->users_model->get_Users($grop_admin_id );
		         	$info = $this->get_member_information($id, 'group', $userID);
                    		$group = $this->groups_model->get_Record($id);
                    		$user = $this->users_model->get_Users($userID);
                 if ($info['status'] == "non_member") {
                    			$this->add_Member($id, 'group', $role, 'pending', $userID, 1, 1);
                    			$notif_type = "group_join_via_follow";
                    			$content =  $user['Us_Name'] . " joined " . $group['Gr_Name'] . " via profile  " . $referrer['Us_Name'];
                    			$this->notifications_model->add_notification($notif_type, 'A user joined the group via Profile Follow', $content, $userID, 'user', $id, 'group');
                    			$response = array('status' => 'success', 'message' => 'You are not the Member of this Group ' . strtoupper($group['Gr_Name']) . '. Once Your approved, you can follow the Posts of the Group.', 'group' => encode_id($id));
                  } else {

								if($info['role_status'] == 'active'){
								if($info['memberId']){

                                 	 $updatedata = array('Me_post_Approved' => 1);
                                           $this->db->where('Me_ID', $info['memberId']);
                                        $this->db->update('bs_members', $updatedata);
                                         $response = array('status' => 'success', 'message' => 'You are already following the Post of this Group ' . strtoupper($group['Gr_Name']) . '. You will be notified on new Posts.', 'group' => encode_id($id));

                                	 }
								}else{
								   $response = array('status' => 'success', 'message' => 'Your request to join has already been sent to this group ' . strtoupper($group['Gr_Name'] ). '. You will be notified once approved.'  , 'memberId' => $info['memberId']);

								}


            }
            	return $response;
	}

	  public  function join($data) {



		$id = decode_id($data['id']);
		$type = $data['type'];
		$role = 'member';

			$role = isset($data['role']) ? $data['role'] : $role;

		$userID = isset($data['user_id']) ? $data['user_id'] : $this->LoggedInUser;
		$user = $this->users_model->get_Users($userID);

		if ($type == 'group') {

			$group = $this->groups_model->get_Record($id);

			$role = ($group['Gr_Us_ID'] == $userID) ? 'superadmin' : 'member';
			$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';
			$status = ($type == 'superadmin') ? 'active' : $status;
			$notif_type = ($group['Gr_Privacy'] == 'public') ? 'join' : 'request_join';


			if($group['Gr_Privacy'] == 'public'){

				if($role == 'member'){
				$statusofjoin = 'join';
				  $join_status = 'pending';
                  $response_meesage = 'You have successfully joined the group.';
				}else if($role == 'follower'){
				$statusofjoin = 'follow';
					$join_status = 'pending';
                     $response_meesage = 'You have successfully follow the group.';
				}
			}else if($group['Gr_Privacy'] == 'private'){
						if($role == 'member'){
            				$statusofjoin = 'request to join';
            					$join_status = 'pending';
                               	$response_meesage = 'Your request to join the group has been sent.';
            		 }else if($role == 'follower'){
            		 	$statusofjoin = 'request to follow';
            		 	$join_status = 'pending';
            		 	$response_meesage = 'Your request to follow the group has been sent.';
             }

			}
			$linkmade = "<a href='".base_url()."account/view/group/".encode_id($id)."'>";
			$content =  ($group['Gr_Privacy'] == 'public') ?  $statusofjoin .' ' . $group['Gr_Name'] :   $statusofjoin .' ' . $group['Gr_Name']  ;
			$content =  $linkmade . $content . '</a>';

 			$this->add_Member($id, $type, $role, $status, $userID);

			$this->notifications_model->add_notification($notif_type, $content, $content, $userID, 'user', $id, 'group');

 			$response = array('status' => 'success', 'join_status' => $join_status , 'message' => $response_meesage);


		} else if ($type == 'topic') {

			$topic = $this->topics_model->get_Record($id);

		} else {

			$post = $this->posts_model->get_Record($id);

		}

		return $response;
	}

	function show_members($data) {



		$result = $this->get_members($data);


		foreach($result as $ko => $mem){


				if($mem->Me_To_ID != 0 && $mem->Me_Status == 'pending' ){

					array_splice($result, $ko);
				}
				if($mem->Me_St_ID != 0 && $mem->Me_Status == 'pending'  ){

					array_splice($result, $ko);

				}

		}

     	 $this->db->select('bs_notification.*');
 		 $this->db->from('bs_notification');
 		 $this->db->where('bs_notification.No_To' , decode_id($data['id']) );
			$query = $this->db->get()->result_array();
 					foreach ($result as $key => $row) {


 						foreach($query as $key1 => $row1){
								if($row1['No_From'] == $result[$key]->Me_Us_ID ){

								$result[$key]->notificationID = $row1['No_ID'];
								$result[$key]->notificationType = $row1['No_Type'];
								$result[$key]->No_From_Type =   $row1['No_From_Type'];
						         $result[$key]->No_To_Type = 	$row1['No_To_Type'] ;
//  								$row1['No_To_Type'] == 'notshown'? $result[$key]->Showing = 0 : $result[$key]->Showing = 1;

								}
						}
 					}
 		$roles = array("member" => 1, "admin" => 2, "superadmin" => 3);
		$group_id = decode_id($data['id']);
		$myMemberInfo = $this->get_member_information($group_id, 'group');
		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;
				$suffix = ($row->Gr_Us_ID == $row->Me_Us_ID) ? " (Creator)" : "";
				$result[$key]->Us_Name = $row->Us_Name . $suffix;
				$result[$key]->Creator = ($row->Gr_Us_ID == $row->Me_Us_ID) ? 1 : 0;
				$result[$key]->merole = ($row->Gr_Us_ID == $this->LoggedInUser) ? 1 : 0;
				$result[$key]->Us_URL = base_url() . 'u/' . encode_id($row->Me_Us_ID);
				$result[$key]->Rl_Type = ($row->Rl_Type == "") ? "Choose a role" : $row->Rl_Type;
				$result[$key]->Me_CanEdit = 0;
				$result[$key]->Is_Self = ($this->LoggedInUser == $row->Me_Us_ID) ? 1 : 0;

				if ($row->Gr_Us_ID == $this->LoggedInUser) {
					$result[$key]->Me_CanEdit = 1;
				} else {
					$loggedInRole = $roles[$myMemberInfo['role']];
					$recordRole = $roles[$myMemberInfo['role']];
					if ($loggedInRole > $recordRole) {
						$result[$key]->Me_CanEdit = 1;
					}
				}
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}


	public function getMutualFriend($data){

		$myFriendLISt = array();
		$this->db->select('bs_contacts.Co_Contact_ID')->from('bs_contacts');
		$this->db->where('Co_Us_ID' , $this->LoggedInUser);
		$this->db->where('Co_Status' , 'confirmed');
		$query = $this->db->get()->result_array();


		 foreach($query as $quer){
			array_push($myFriendLISt ,  $quer['Co_Contact_ID']);
		 }


		foreach($data as $key=>$value){
			if($value['Me_Role'] == 'superadmin'){
		    $data[$key]['MutualFriend'] = 0;
			$value['MutualFriend'] = 0;


			}else{
			$data[$key]['MutualFriend'] = 5;

			         $this->db->select('bs_contacts.Co_Contact_ID')->from('bs_contacts');
            		$this->db->where('Co_Us_ID' ,$value['Me_Us_ID']);
            		$this->db->where('Co_Status' , 'confirmed');
            		$query = $this->db->get()->result_array();

							$friendCouner = 0;
						foreach($query as $friend){


						if (in_array($friend['Co_Contact_ID'], $myFriendLISt))
                          {
                          $friendCouner++;
                          }
                        else
                          {

                          }

						}
				  $data[$key]['MutualFriend'] = $friendCouner;



			}

		}
		return $data;
	}
	public function get_members($data) {


		$group = decode_id($data['id']);
		$role = $data['role'];


		$this->db->select("  m.Me_Us_ID, m.Me_Role,

		m.Me_To_ID , m.Me_St_ID,
		 g.Gr_Us_ID, r.Rl_Type, m.Me_Status, m.Me_IsInvited, u.Us_Name, u.Us_FName, u.Us_LName, u.Us_Email, u.Us_Company, u.Us_JobTitle, u.Us_photo, u.Us_Thumb,u.Us_DateTime");
		$this->db->from($this->Tablename . " m");
		$this->db->join("bs_users u", "m.Me_Us_ID = u.Us_ID", "LEFT");
		$this->db->join("bs_groups g", "m.Me_Gr_ID = g.Gr_ID", "LEFT");
		$this->db->join("bs_roles r", "m.Me_Role = r.Rl_Code", "LEFT");
		$this->db->where("m.Me_Parent", 'group');
		$this->db->where("m.Me_Gr_ID", $group);


		if ($role != 'pending') {
			if ($role != '') {
				$this->db->where("m.Me_Role", $role);
				$this->db->where("m.Me_Status", 'active');
			}

		} else {
			$this->db->where("m.Me_Status", 'pending');
		}
		$this->db->where("m.Me_Status !=", 'invited');
		if ($data['keyword'] != "") {
			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$data['keyword']."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$data['keyword']."%'");
			$this->db->group_end();
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}
		function leave_group_role($id, $type, $user_id, $role ,$post=''){
			$info = $this->get_member_information($id, $type, $user_id);
			$group = $this->groups_model->get_Record($id);
			$user = $this->users_model->get_Users($user_id);



			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->delete('bs_members');

			$notif_type = 'leavegroup';
			$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
			$content =  "You left the group " . $linkmade ;


			$this->notifications_model->add_notification($notif_type, 'Leave group', $content,  $group['Gr_Us_ID'], 'group', $user_id, 'user');



		      $name = (!empty($user['Us_Alias'])) ?$user['Us_Alias']: $user['Us_Name'];

			$notif_type = 'adminleave';
			$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
			$userlinkmade = '<a href="'.base_url().'u/'.encode_id($user['Us_ID']).'">'.$name.'</a>';


			$content =  $userlinkmade . " left the group "  . $linkmade ;
			$this->notifications_model->add_notification($notif_type, 'Leave group', $content, $user_id, 'group', $group['Gr_Us_ID'], 'user');
		}
	function assign_group_role($id, $type, $user_id, $role ,$post='') {

			 	if($type == 'post'){
			 	$info = $this->get_member_information_POST($user_id, $id );
			 	}else if($type == 'topic'){
					$info = $this->get_member_information($id, $type, $user_id );
					}else{
			 	$info = $this->get_member_information($id, $type, $user_id);
			 	}

				if ($info['status'] == "non_member") {
					$this->add_Member($id, $type, $role, 'active', $user_id);
				} else {
					$data = array('Me_Role' => $role);
					if($type == 'topic'){
						$this->db->where('Me_To_ID', $id);
					}else if($type == 'post'){
 						$this->db->where('Me_St_ID', $post);
					}else{

						$this->db->where('Me_Gr_ID', $id);
					}
					$this->db->where('Me_Us_ID', $user_id);
					$this->db->update($this->Tablename, $data);
				}

					if($type == 'topic' || $type == 'post' ){
  						return;
					}
			$group = $this->groups_model->get_Record($id);
			$user = $this->users_model->get_Users($user_id);
			// notif from group to user
			$notif_type = 'assign_role';
			$userRole = str_replace("_", " ", $role);
			if($type == 'post'){

			 $posts_item = $this->posts_model->get_Post($_POST['post_id']);

 			 $postlinkmade = '<a href="'.base_url().'account/view/post/'.encode_id($_POST['post_id']).'">'. $posts_item['Po_Title'].'</a>';

             $linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';

			 $content =  "Your role in ". $postlinkmade ." under " . $linkmade  . " has been set to " . $userRole;
			}else{
				$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
            			$content =  "Your role in " . $linkmade  . " has been set to " . $userRole;
			}
//
// 			$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
// 			$content =  "Your role in " . $linkmade  . " has been set to " . $userRole;
			$this->notifications_model->add_notification($notif_type, 'Role Assignment', $content, $id, 'group', $user_id, 'user');
		}

	function remove_group_user($id, $user_id) {


		if(isset($_POST['postype']) && $_POST['postype'] == 'topic'){
			$this->db->where('Me_To_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);

		}else{
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
		}



    	$this->db->delete($this->Tablename);
		if(isset($_POST['postype']) &&   $_POST['postype'] == 'topic'){

			return;
		}
 		$group = $this->groups_model->get_Record($id);
		$this->updateMembersCount('group', $id);
     	$notif_type = 'member_out';

			if(isset($_POST['post_id'])){
			$posts_item = $this->posts_model->get_Post($_POST['post_id']);

		     $postlinkmade = '<a href="'.base_url().'account/view/post/'.encode_id($_POST['post_id']).'">'. $posts_item['Po_Title'].'</a>';
    		$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
		  	$content =  "You have been removed from post ". $postlinkmade ." under" . $linkmade;
			}else{
			$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
		   $content =  "You have been removed from " . $linkmade;
			}

		$this->notifications_model->add_notification($notif_type, 'You have been removed', $content, $id, 'group', $user_id, 'user');
	}

	public function approve_reject_join_request($id, $user_id, $action) {
		$group = $this->groups_model->get_Record($id);
		if ($action == 'approve') {
			$data = array('Me_Status' => 'active');
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->update($this->Tablename, $data);

			$notif_type = "approve_request";
			$content =  "Your request to join " . $group['Gr_Name'] . " has been approved";
			$this->notifications_model->add_notification($notif_type, 'You have been approved', $content, $id, 'group', $user_id, 'user');

			$response = array('status' => 'success', 'message' => 'User join request approved.');

		} else {
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->delete($this->Tablename);

			$notif_type = "reject_request";
			$content =  "Your request to join " . $group['Gr_Name'] . " has been rejected";
			$this->notifications_model->add_notification($notif_type, 'You have been rejected', $content, $id, 'group', $user_id, 'user');

			$response = array('status' => 'success', 'message' => 'User join request rejected.');
		}
		$this->updateMembersCount('group', $id);

		return $response;
	}

	function invite_user_to_group($id, $user_id, $role) {
		$group = $this->groups_model->get_Record($id);
		$this->add_Member($id, 'group', $role, 'invited', $user_id, 1);

		$notif_type = 'group_invite';
	 	$linkmade = "<a href='".base_url()."account/view/group/".encode_id($group['Gr_ID'])."'>";
		$content =  "invites you to join group " . $linkmade  . $group['Gr_Name'] . "</a>";
		$this->notifications_model->add_notification($notif_type, 'Group invite to join', $content, $id, 'group', $user_id, $this->LoggedInUser);

		$response = array('status' => 'success', 'message' => 'User invitation successful.');

		return $response;
	}

	function respond_to_group_invitation($id, $user_id, $action) {
		$group = $this->groups_model->get_Record($id);
		$user = $this->users_model->get_Users($user_id);

		if ($action == 'accept') {

			$data = array('Me_Status' => 'active');
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->update($this->Tablename, $data);

			$notif_type = "accept_invite";
			$content =  $user['Us_Name'] . " accepted the invitation to join " . $group['Gr_Name'];
			$this->notifications_model->add_notification($notif_type, 'Invitation accepted', $content, $user_id, 'user', $id, 'group');

			$response = array('status' => 'success', 'message' => 'Group join invitation accepted.');

		} else {
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->delete($this->Tablename);

			$notif_type = "reject_invite";
			$content =  $user['Us_Name'] . " declined the invitation to join " . $group['Gr_Name'];
			$this->notifications_model->add_notification($notif_type, 'Invitation declined', $content, $user_id, 'user', $id, 'group');

			$response = array('status' => 'success', 'message' => 'Group join invitation declined.');
		}
		$this->updateMembersCount('group', $id);

		return $response;
	}

	function add_to_group_by_link($group_id, $user_id, $referrer_id, $role) {

		$info = $this->get_member_information($group_id, 'group', $user_id);
		$group = $this->groups_model->get_Record($group_id);
		$user = $this->users_model->get_Users($user_id);
		$referrer = $this->users_model->get_Users($referrer_id);

		$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';


		if ($info['status'] == "non_member") {
 			$this->add_Member($group_id, 'group', $role, $status, $user_id, 1, 1, $referrer_id);
			$notif_type = "group_join_via_link";
			$linkmade = "<a href='".base_url()."account/view/group/".encode_id($group_id)."'>";
			$content = $linkmade. $user['Us_Name'] . " joined " . $group['Gr_Name'] . " via link shared by <a/><a href=".base_url()."u/".encode_id($referrer['Us_ID']).">".$referrer['Us_Name']."</a>";

	   		 $this->notifications_model->add_notification($notif_type, 'A user joined  '. $group['Gr_Name'] .' group via share link', $content, $user_id, 'user', $group_id, 'group');
 	 		 //$this->notifications_model->add_notification($notif_type, '<a href="'.base_url().'account/view/group/'.encode_id($group_id).'"A user joined  '. $group['Gr_Name'] .' group via share link >', $content, $user_id, 'user', $group_id, 'group');

			$response = array('status' => 'success', 'message' => 'You are now <font style="color:red";>'.strtoupper($role) .'</font>of ' . $group['Gr_Name'] . '. You will be redirected to the group.', 'group' => encode_id($group_id));


			$subject = 'You just joined the '. $group['Gr_Name'] .' via invite link';
			$data= array(
				'sEmail' => $user['Us_Email'],
				'sFName' =>$user['Us_FName'],
				'sLName' =>$user['Us_LName'],
				'gropname' => $group['Gr_Name'],
				'invitername' => $referrer['Us_Name'],
				'inviteuser' => $user['Us_Name']
			);

// 		$message = $this->load->view('templates/email/inviteLink', $data, TRUE);

// 		$responsex = $this->emailmanager->GeneralNotificationEmail( $data , $message , $subject);

			$data= array(
				'sEmail' => $referrer['Us_Email'],
				'sFName' => $referrer['Us_FName'],
				'sLName' => $referrer['Us_LName'],
				'gropname' =>  $group['Gr_Name'],
				'invitername' => $referrer['Us_Name'],
				'inviteuser' => $user['Us_Name']
			);

			$subject =  $user['Us_Name'].' has joined '. $group['Gr_Name'] .' via your invite link';
//			$message = $this->load->view('templates/email/referal_invite_alert', $data, TRUE);
//			$responsex = $this->emailmanager->GeneralNotificationEmail( $data , $message , $subject);

		} else {
			if ($info['role_status'] == "active") {
				$response = array('status' => 'success', 'message' => 'You are already a member of ' . $group['Gr_Name'] . '. You will be redirected to the group.', 'group' => encode_id($group_id));
			} else if($info['role_status'] == "pending" && $info['Me_ViaLink'] == 1){
					$response = array('status' => 'success', 'message' => 'Your Reguest to join this ' . $group['Gr_Name'] . '.has been sent.', 'group' => encode_id($group_id) , 'is_pending_link_invite' => true);

			} else{
				if ($info['is_invited'] == 1) {
					$response = array('status' => 'success', 'message' => 'You have a pending invite for ' . $group['Gr_Name'] . '. Approve this invite?', 'group' => encode_id($group_id), 'is_pending_invite' => true);
				} else {
					$response = array('status' => 'success', 'message' => 'You have a pending join request for ' . $group['Gr_Name'] . '. You will be able to view group once approved.');
				}
			}
		}

		return $response;
	}
}
?>
