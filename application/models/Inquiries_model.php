<?php
class Inquiries_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_Records($id = false)
	{
		if ($id == false)
		{
			$this->db->select('*');
			$this->db->from('bs_inquiries');
			$this->db->order_by('In_DatePosted', 'DESC');
			return $this->db->get()->result_array();
		}

		$query = $this->db->get_where('bs_inquiries', array('In_ID' => $id));
		return $query->row_array();
	}

	public function delete_row($id)
	{
		$this->db->where('In_ID',$id);
		$this->db->delete('bs_inquiries');
	}

	public function save_inquiry_information($data) {
		$this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

		unset($data['token']);
		$data['In_DatePosted'] = $now;

	    return $this->db->insert('bs_inquiries', $data);
	}

}
