<?php
class Survey_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}

	public function get_SurveyByPost($PostID)
	{
		$this->db->order_by('Su_ID', 'ASC');
		$query = $this->db->get_where('bs_survey', array('Su_Po_ID' => $PostID));
		return $query->result_array();
	}

	public function Insert($GroupID, $TopicID, $SubTopicID, $PostID, $UserID, $Item, $Type, $Question, $Options)
	{
	   $this->load->helper('url');

	   $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
			'Su_Gr_ID' => $GroupID,
			'Su_To_ID' => $TopicID,
			'Su_St_ID' => $SubTopicID,
			'Su_Po_ID' => $PostID,
			'Su_Us_ID' => $UserID,
			'Su_Item' => $Item,
			'Su_Type' => $Type,
			'Su_Question' => $Question,
			'Su_Options' => $Options,
			'Su_DatePosted' => $now
	    );

	   $this->db->insert('bs_survey', $data);
		return $this->db->insert_id();
	}



	public function get_AnswersByPost($UserID, $PostID)
	{
		$this->db->order_by('An_ID', 'ASC');
		$query = $this->db->get_where('bs_answers', array('An_Po_ID' => $PostID, 'An_Us_ID' => $UserID));
		return $query->result_array();
	}

	public function Answer($GroupID, $TopicID, $SubTopicID, $PostID, $UserID, $Item, $Answer)
	{
	   $this->load->helper('url');

	   $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
			'An_Gr_ID' => $GroupID,
			'An_To_ID' => $TopicID,
			'An_St_ID' => $SubTopicID,
			'An_Po_ID' => $PostID,
			'An_Us_ID' => $UserID,
			'An_Item' => $Item,
			'An_Answer' => $Answer,
			'An_DatePosted' => $now
	    );

	   $this->db->insert('bs_answers', $data);
		return $this->db->insert_id();
	}



}
