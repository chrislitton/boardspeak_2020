<?php
class Medias_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function insert($array)
	{
	    $this->load->helper('url');	
			
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');

	    $data = array(
			'Me_Type' => $array['type'],
			'Me_Parent' => $array['parentId'],
			'Me_Filetype' => $array['filetype'],
			'Me_Path' => $array['path'],
			'Me_Filename' => $array['filename'],
			'Me_DateCreated' => $now
	    );
	
	   	$this->db->insert('bs_medias', $data);
		$primaryID = $this->db->insert_id();

		return $primaryID;
	}

	public function get_PostMedia($id,$type=null)
	{
		$this->db->order_by('Me_ID', 'DESC');
		$this->db->where('Me_Parent', $id);
		$this->db->where('Me_Type', 'Post');
		if($type == 'files'){
			$this->db->where('Me_Filetype', 'file');
		}else if($type == 'images'){
			$this->db->where('Me_Filetype', 'image');
		}
		$query = $this->db->get('bs_medias');
		return $query->result_array();
	}	

	public function get_record($id)
	{
		$this->db->where('Me_ID', $id);
		$query = $this->db->get('bs_medias');
		return $query->row_array();
	}

	public function delete_record($id)
	{
		$this->db->delete('bs_medias', array('Me_ID' => $id));
		return true;
	}
}
