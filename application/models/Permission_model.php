<?php
class Permission_model extends CI_Model {

    private $permissionTable = 'bs_permissions';
    private $groupPermissionsTable = 'bs_group_permissions';
    private $LoggedInUser;

    public function __construct() {
		$this->load->database();
		$this->load->library('session');

		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->LoggedInUser = $this->session->userdata['logged_in']['bs_id'];
		}
    }

    function show_group_role_permission($group, $role) {
        $data = $this->get_group_role_permission($group, $role);

        $permissions = "";

        foreach($data as $key => $permission) {
            $allowed = ($permission->Gp_Allowed) ? "checked='checked'" : "";
            $denied = ($permission->Gp_Allowed) ? "" : "checked='checked'";
            $permissions .= "<tr class='permission_item'>
                                <th>" . $permission->Pm_Description . "</th>
                                <td style='text-align:center;'><input type='radio' class='permission_option_selector' name='permission_". $permission->Pm_ID ."' value=1 ". $allowed ."></td>
                                <td style='text-align:center;'><input type='radio' class='permission_option_selector' name='permission_". $permission->Pm_ID ."' value=0 ". $denied ."></td>
                            </tr>";
        }
        
        $grid = array(
            'data' => $permissions,
            'status' => 'success'
		);

        return $grid;

	}

	function get_group_role_permission($group, $role) {
        $this->db->select("p.Pm_ID, p.Pm_Code, p.Pm_Description, g.Gp_Allowed");
        $this->db->from($this->permissionTable . " p");
        $this->db->join($this->groupPermissionsTable . " g", "g.Gp_PermissionID = p.Pm_ID AND g.Gp_GrID = " . $group . " AND g.Gp_Role = '". $role ."'", "LEFT");

        $query = $this->db->get();
        $data = $query->result();
        
        return $data;
    }
    
    function save_group_role_permission($group, $role, $group_permissions) {
        $this->db->select("Pm_ID");
        $this->db->from($this->permissionTable);
        $query = $this->db->get();
        $permissions = $query->result();

        foreach($permissions as $key => $permission) {
            $this->db->select("Gp_ID");
            $this->db->from($this->groupPermissionsTable);
            $this->db->where("Gp_GrID", $group);
            $this->db->where("Gp_PermissionID",  $permission->Pm_ID);
            $this->db->where("Gp_Role", $role);
            $query = $this->db->get();
            $group_permission = $query->result();

            if (count($group_permission) > 0) {
                $data = array('Gp_Allowed' => $group_permissions['permission_' . $permission->Pm_ID]);
                $this->db->where("Gp_GrID", $group);
                $this->db->where("Gp_PermissionID",  $permission->Pm_ID);
                $this->db->where("Gp_Role", $role);
                $this->db->update($this->groupPermissionsTable, $data);	
            } else {
                $data = array(
                    "Gp_GrID" => $group,
                    "Gp_PermissionID" => $permission->Pm_ID,
                    "Gp_Role" => $role,
                    "Gp_Allowed" => $group_permissions['permission_' . $permission->Pm_ID]
                );

                $this->db->insert($this->groupPermissionsTable, $data);
            }

        }

        $response = array('status' => 'success', 'message' => 'Group role permission updated.');

        return $response;
    }

}
?>