<?php
class Events_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}
	
	public function get_Events($id = false)
	{
		if ($id === false)
		{
			$query = $this->db->order_by('Ev_ID', 'DESC');
			$query = $this->db->get('bs_events');			
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_events', array('Ev_ID' => $id));
		return $query->row_array();
	}	
	
	public function get_BoardEvents($id)
	{		
		$query = $this->db->get_where('bs_events', array('Ev_Gr_ID' => $id));
		return $query->result_array();
	}	
	
	public function get_UserEvents($id)
	{
		
		$query = $this->db->order_by('Ev_ID', 'DESC');
		$query = $this->db->get_where('bs_events', array('Ev_Us_ID' => $id));
		return $query->result_array();
	}	
	
	
	
	public function set_Insert($UserID, $GroupID, $Subject, $Location,$StartDate, $DueDate, $Priority, $Type)
	{
	    $this->load->helper('url');	
			
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Ev_Us_ID' => $UserID,
		'Ev_Gr_ID' => $GroupID,
		'Ev_Subject' => $Subject,
		'Ev_Location' => $Location,
		'Ev_StartDate' => $StartDate,
		'Ev_DueDate' => $DueDate,
		'Ev_Priority' => $Priority,
		'Ev_Type' => $Type,
		'Ev_Deleted' => false,
		'Ev_DatePosted' => $now
	    );
	
	    $this->db->insert('bs_events', $data);
		return $this->db->insert_id();
	}
	
	
	
	public function set_Update($EventID, $Subject, $Location,$StartDate, $DueDate, $Priority, $Type)
	{
	    $this->load->helper('url');	
			
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Ev_Subject' => $Subject,
		'Ev_Location' => $Location,
		'Ev_StartDate' => $StartDate,
		'Ev_DueDate' => $DueDate,
		'Ev_Priority' => $Priority,
		'Ev_Type' => $Type
	    );
	
	    
		$this->db->where('Ev_ID', $EventID);
		return $this->db->update('bs_events', $data);
	}
	
	
	public function set_Photo($EventID, $FileName, $Photo, $Thumb)
	{
		$data = array(
			'Ev_FileName' => $FileName,
			'Ev_Photo' => $Photo,
			'Ev_Thumb' => $Thumb
		);
	
		$this->db->where('Ev_ID', $EventID);
		return $this->db->update('bs_events', $data);
	}

	public function delete_row($id)
	{
		$this->db->where('Ev_ID',$id);
		$this->db->delete('bs_events');
	}
		
		
}
