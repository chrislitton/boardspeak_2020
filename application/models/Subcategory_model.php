<?php
class Subcategory_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}

		
	public function get_Name($id)
	{
		$query = $this->db->get_where('bs_subcategory', array('Sc_ID' => $id));
		return $query->row_array();
	}
		
}
