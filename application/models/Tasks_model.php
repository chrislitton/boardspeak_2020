<?php
class Tasks_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}
	
	public function get_Tasks($id = false)
	{
		if ($id === false)
		{
			$query = $this->db->order_by('Ta_ID', 'DESC');
			$query = $this->db->get('bs_tasks');			
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_tasks', array('Ta_ID' => $id));
		return $query->row_array();
	}	
	
	public function get_BoardTasks($id)
	{		
		$query = $this->db->get_where('bs_tasks', array('Ta_Gr_ID' => $id));
		return $query->result_array();
	}	
	
	public function get_UserTasks($id)
	{
		
		$query = $this->db->order_by('Ta_ID', 'DESC');
		$query = $this->db->get_where('bs_tasks', array('Ta_Us_ID' => $id));
		return $query->result_array();
	}	
	
	
	
	public function set_Insert($UserID, $GroupID, $Subject, $StartDate, $DueDate, $Priority, $Status, $Completed)
	{
	    $this->load->helper('url');	
			
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Ta_Us_ID' => $UserID,
		'Ta_Gr_ID' => $GroupID,
		'Ta_Subject' => $Subject,
		'Ta_StartDate' => $StartDate,
		'Ta_DueDate' => $DueDate,
		'Ta_Priority' => $Priority,		
		'Ta_Status' => $Status,		
		'Ta_Completed' => $Completed,
		'Ta_Deleted' => false,
		'Ta_DatePosted' => $now
	    );
	
	    $this->db->insert('bs_tasks', $data);
		return $this->db->insert_id();
	}
	
	
	
	public function set_Update($TaskID, $Subject, $StartDate, $DueDate, $Priority, $Status, $Completed)
	{
	    $this->load->helper('url');	
			
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Ta_Subject' => $Subject,
		'Ta_StartDate' => $StartDate,
		'Ta_DueDate' => $DueDate,
		'Ta_Priority' => $Priority,		
		'Ta_Status' => $Status,		
		'Ta_Completed' => $Completed
	    );
	
	    
		$this->db->where('Ta_ID', $TaskID);
		return $this->db->update('bs_tasks', $data);
	}
	
	
	public function set_Photo($TaskID, $FileName, $Photo,  $Thumb)
	{
		
		$data = array(
			'Ta_FileName' => $FileName,
			'Ta_Photo' => $Photo,
			'Ta_Thumb' => $Thumb
		);
	
		$this->db->where('Ta_ID', $TaskID);
		return $this->db->update('bs_tasks', $data);
	}

	public function delete_row($id)
	{
		$this->db->where('Ta_ID',$id);
		$this->db->delete('bs_tasks');
	}
		
		
}
