<?php
class Posts_model extends CI_Model {

	private $Tablename = 'bs_posts';
	private $LoggedInUser;

	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');

		$this->LoggedInUser = isset($this->session->userdata['logged_in']['bs_id']) ? $this->session->userdata['logged_in']['bs_id'] : '';
	}



	public function getPromotedPost($id){
		$this->db->select('promoted_post.Pp_ID');
		$this->db->from('promoted_post');
		$this->db->where('Pp_Post_ID', $id);
		$promotepost = $this->db->get()->result_array();
		return $promotepost;
	}
	public function resultupdate($id){
		$this->db->select("post_update.* ,bs_users.* , bs_posts.*");
		$this->db->from("post_update");

		$this->db->join('bs_users' , 'bs_users.Us_ID = post_update.Pu_user_id');

		$this->db->join('bs_posts' , 'bs_posts.Po_ID = post_update.Pu_post_id');
		$this->db->limit(1);
		$this->db->order_by('Pu_ID',"DESC");
		$this->db->where('Pu_post_id' , $id );
		$query = $this->db->get();
		$result_of_update = $query->result_array();
		return $result_of_update;
	}
	public function allpackages(){
		$this->db->select('coin_packages.*');
		$this->db->from('coin_packages');
		$ada = $this->db->get()->result_array();
		return $ada;
	}
	public function allpollrecord($id){
		$this->db->select('bs_posts_poll.*');
		$this->db->from('bs_posts_poll');
		$this->db->where('Pl_PO_ID' , $id);
		return $this->db->get()->result_array();
	}
	public function polluseraccount($id){
		$this->db->select('bs_poll_ans.*');
		$this->db->from('bs_poll_ans');
		$this->db->where('Poa_po_id' , $id);
		return $this->db->get()->result_array();
	}
	public function polluserDetails( $id ){
		$this->db->select('bs_poll_ans.*');
		$this->db->from('bs_poll_ans');
		$this->db->where('Poa_po_id' , $id);
		$this->db->where('Poa_Us_ID' , $this->LoggedInUser);
		$data  =	$this->db->get()->result_array();
		return $data;
	}
	public function get_Record($id)
	{
		$query = $this->db->get_where($this->Tablename, array('Po_ID' => $id));
		return $query->row_array();
	}

	public function searchPosts($SearchText)
	{
		$this->db->distinct();
		$this->db->select('bs_posts.*, bs_groups.Gr_Privacy, bs_topics.To_Privacy, bs_members.Me_Status, bs_topics.To_Us_ID, bs_groups.Gr_Us_ID');
		$this->db->from($this->Tablename);
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID AND bs_posts.Po_Gr_ID = bs_topics.To_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->like('Po_Title', $SearchText);
		$this->db->where_not_in('Po_SurveyStatus', 'pending');
		$this->db->order_by('Po_ID', 'DESC');
		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}


	public function get_AllTopic($search_text = null , $group_id = null , $cat_id = null , $subCat = null){
//        	$this->db->select('bs_topics.To_Name  as Po_Title ,
//        	  bs_topics.To_Sc_ID  as Po_Sc_ID ,
//
//        	  bs_topics.To_cat_id  as Po_Ca_ID ,
//        	  bs_topics.To_Gr_ID as Po_Gr_ID,
//        	 bs_groups.Gr_Privacy, bs_topics.To_Privacy, bs_members.Me_Status,
//        	  bs_topics.To_Us_ID, bs_groups.Gr_Us_ID');
// 		$this->db->from('bs_topics');
// 		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
//  		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
//
// 		$this->db->order_by('To_ID', 'DESC');
//
// 		if(isset($group_id ) && !empty($group_id)){
//
// 			$this->db->where('bs_topics.Po_Gr_ID' , decode_id($group_id));
//
// 		}
//
// 		if(isset($subCat) && !empty($subCat)){
// 		   $this->db->where('bs_topics.Po_Sc_ID' ,  $subCat);
// 		}else if(isset($cat_id ) && !empty($cat_id)){
//
// 			$this->db->where('bs_topics.Po_Ca_ID' ,  $cat_id);
//
// 		}
//
// 		if(!empty($search_text)){
// 			$this->db->like('bs_topics', $search_text, 'BOTH');
// 		}
//
//
// 		$data = $this->db->get()->result_array();
//
// 		$data = $this->format_data($data);
//
// 		return $data;
	}

	public function get_AllPosts($search_text = null , $group_id = null , $cat_id = null , $subCat = null)
	{



		$this->db->select('bs_posts.*, bs_groups.Gr_Privacy, bs_topics.To_Privacy, bs_members.Me_Status, bs_topics.To_Us_ID, bs_groups.Gr_Us_ID');
		$this->db->from($this->Tablename);
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID AND bs_posts.Po_Gr_ID = bs_topics.To_Gr_ID', 'LEFT');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where_not_in('Po_SurveyStatus', 'pending');
		$this->db->order_by('Po_ID', 'DESC');

		if(isset($group_id ) && !empty($group_id)){

			$this->db->where('bs_posts.Po_Gr_ID' , decode_id($group_id));

		}

		if(isset($subCat) && !empty($subCat)){
		   $this->db->where('bs_posts.Po_Sc_ID' ,  $subCat);
		}else if(isset($cat_id ) && !empty($cat_id)){

			$this->db->where('bs_posts.Po_Ca_ID' ,  $cat_id);

		}

		if(!empty($search_text)){
			$this->db->like('Po_Title', $search_text, 'BOTH');
		}


		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}


	public function get_promoted_post_data($postid){

		$where = array(
			'Pp_Post_ID'	=> $postid
		);
		$this->db->select('promoted_post.*');
		$this->db->from('promoted_post');
		$this->db->where($where);
		$res = 	$this->db->get()->result_array();

		 return $res;
	}
	public function postreward($Po_reward) {
		$this->db->select('bs_voucher_post.* , bs_posts.*');
		$this->db->from('bs_voucher_post');
		$this->db->join('bs_posts' , 'bs_posts.Po_ID  = Vp_post_id' );
		$this->db->where('Vp_id' , $Po_reward);
		$respons = $this->db->get()->result_array();
		return $respons;
	}
	public function get_promoted_post($postid){

					$where = array(
					'Pp_Post_ID'	=> $postid
					);
						$this->db->select('promoted_post.*');
						$this->db->from('promoted_post');
						$this->db->where($where);
					$res = 	$this->db->get()->result_array();

					if(count($res) > 0){
						return '1';
					}else{
						return '0';
					}
	}


   public function get_Post($id = false)
	{
		if ($id == false)
		{
			$this->db->order_by('Po_ID', 'DESC');
			$query = $this->db->get('bs_posts');
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_posts', array('Po_ID' => $id));
      	$row = $query->row();
		if (isset($row))
		{
			$GroupID = $row->Po_Gr_ID;
			$TopicID = $row->Po_To_ID;
			if (strlen($TopicID)==0) $Parent = 'group';
			else $Parent = 'topic';
		}

		$this->db->flush_cache();
		if (strcmp($Parent,'group')==0)
		{
			$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Alias,bs_users.Us_Photo,bs_groups.*,0 TopicID,"" TopicName,"" SubTopicName,0 SubTopicID, bs_buttons.*');
			$this->db->from('bs_posts');
			$this->db->where('Po_ID', $id);
			$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
			$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID','left');
			$this->db->join('bs_buttons', 'bs_buttons.Bn_ID = bs_posts.Po_Bn_ID','left');
			$query = $this->db->get();
			return $query->row_array();


		}
		else if (strcmp($Parent,'topic')==0)
		{
			$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Alias, bs_users.Us_Photo,bs_groups.*,bs_topics.To_ID TopicID,bs_topics.To_Name TopicName,"" SubTopicName,0 SubTopicID,bs_category.*, bs_buttons.*');
			$this->db->from('bs_posts');
			$this->db->where('Po_ID', $id);
			$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
			$this->db->join('bs_category', 'bs_category.Ca_ID = bs_posts.Po_Ca_ID','left');
			$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID','left');
			$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID','left');
			$this->db->join('bs_buttons', 'bs_buttons.Bn_ID = bs_posts.Po_Bn_ID','left');
			$query = $this->db->get();
			return $query->row_array();
		}

	}

	public function get_GroupPosts($id)
	{
		$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Photo');
		$this->db->from('bs_posts');
		$this->db->where('Po_Gr_ID', $id);
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
		$this->db->order_by('Po_ID', 'ASC');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_TopicPosts($id)
	{
		$this->db->select('bs_posts.*,IFNULL(bs_category.Ca_Name, bs_posts.Po_Ca_Name) as catName,bs_subcategory.Sc_Name as subCatName,bs_users.Us_Name,bs_users.Us_Photo, bs_groups.Gr_Privacy, bs_topics.To_Privacy, bs_members.Me_Status, bs_topics.To_Us_ID, bs_groups.Gr_Us_ID');
		$this->db->from($this->Tablename);
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID AND bs_posts.Po_Gr_ID = bs_topics.To_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('Po_To_ID', $id);
		$this->db->join('bs_category', 'bs_category.Ca_ID = bs_posts.Po_Ca_ID','left');
		$this->db->join('bs_subcategory', 'bs_subcategory.Sc_ID = bs_posts.Po_Sc_ID','left');
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
		$this->db->order_by('Po_ID', 'ASC');
		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}

	public function get_TopicPostsCategory($id,$catId)
	{
		$this->db->select('bs_posts.*,bs_category.Ca_Name as catName,bs_subcategory.Sc_Name as subCatName,bs_users.Us_Name,bs_users.Us_Photo, bs_groups.Gr_Privacy, bs_topics.To_Privacy, bs_members.Me_Status, bs_topics.To_Us_ID, bs_groups.Gr_Us_ID');
		$this->db->from('bs_posts');
		$this->db->where('Po_To_ID', $id);
		$this->db->where('Po_Ca_ID', $catId);
		$this->db->join('bs_category', 'bs_category.Ca_ID = bs_posts.Po_Ca_ID','left');
		$this->db->join('bs_subcategory', 'bs_subcategory.Sc_ID = bs_posts.Po_Sc_ID','left');
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID AND bs_posts.Po_Gr_ID = bs_topics.To_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->order_by('Po_ID', 'ASC');

		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}

	public function get_UserTopicPostsCategory($id,$catId)
	{
		$this->db->select('bs_posts.*, bs_groups.Gr_Privacy, bs_topics.To_Privacy, bs_members.Me_Status, bs_topics.To_Us_ID, bs_groups.Gr_Us_ID');
		$this->db->from('bs_posts');

		if(is_numeric($catId)){
			$this->db->join('bs_category', 'bs_category.Ca_ID = bs_posts.Po_Ca_ID','left');
			$this->db->where('Po_Ca_ID',$catId);
			$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Photo,bs_category.Ca_Name as catName');
		}else{
			$this->db->where('Po_Ca_ID',0);
			$this->db->where('Po_Ca_Name',$catId);
			$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Photo,bs_posts.Po_Ca_Name as catName');
		}
		$this->db->where('Po_To_ID', $id);
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID AND bs_posts.Po_Gr_ID = bs_topics.To_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->order_by('Po_ID', 'ASC');

		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}

	public function searchTopicPosts($id,$catId=null,$keyword)
	{
		$this->db->from('bs_posts');
		$this->db->like('bs_posts.Po_Title', $keyword);

		if($catId != null){
			if(is_numeric($catId)){
				$this->db->where('Po_Ca_ID', $catId);
				$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Photo,bs_category.Ca_Name as catName');
			}else{
				$this->db->where('Po_Ca_ID',0);
				$this->db->where('Po_Ca_Name',$catId);
				$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Photo,bs_posts.Po_Ca_Name as catName');
			}
		}else{
			$this->db->join('bs_category', 'bs_category.Ca_ID = bs_posts.Po_Ca_ID','left');
			$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Photo,bs_category.Ca_Name as catName');
		}
		$this->db->where('Po_To_ID', $id);
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
		$this->db->order_by('Po_ID', 'ASC');
		$query = $this->db->get();
		$data = $query->result_array();

		$data = $this->format_data($data);

		return $data;
	}

	public function get_SubTopicPosts($id)
	{
		$this->db->select('bs_posts.*,bs_users.Us_Name,bs_users.Us_Photo');
		$this->db->from('bs_posts');
		$this->db->where('Po_St_ID', $id);
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_posts.Po_Us_ID','left');
		$this->db->order_by('Po_ID', 'ASC');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_UserPosts($id)
	{
		$this->db->order_by('Po_ID', 'DESC');
		$query = $this->db->get_where('bs_posts', array('Po_Us_ID' => $id, 'Po_Survey' => 0));
		return $query->result_array();
	}


	public function get_UserSurvey($id)
	{
		$this->db->order_by('Po_ID', 'DESC');
		$this->db->from('bs_posts');
		$this->db->where('Po_Us_ID', $id);
		$this->db->where('Po_Survey >', 0);
		$query = $this->db->get();
		return $query->result_array();
	}




	public function get_FollowedPosts($id)
	{

		$this->db->order_by('Po_ID', 'DESC');
		$this->db->from('vw_followedposts');
		$this->db->where('Me_Us_ID',$id);
		$query = $this->db->get();
		return $query->result_array();
	}



	public function Insert($data)
	{
	    $this->load->helper('url');

	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

		$SurveyStatus = '';
		if ($data['Po_Survey']!=0) $SurveyStatus = 'pending';
		$primaryIDArr = [];
		/*if($CatID){
			foreach ($CatID as $keyC => $valueC) {
				$newCatId = $valueC; // set cat id
				// for sub ids
				if($SubID){
					if($SubID[$valueC]){
						foreach ($SubID[$valueC] as $keyS => $valueS) {
							$newSubID = $valueS; // set subcat id
							$data = array(
								'Po_Us_ID' => $UserID,
								'Po_Gr_ID' => $GroupID,
								'Po_To_ID' => $TopicID,
								'Po_Title' => $Title,
								'Po_Description' => $Description,
								'Po_Content' => $Content,
								'Po_Ca_ID' => $newCatId,
								'Po_Ca_Name' => '',
								'Po_Sc_ID' => $newSubID,
								'Po_Sc_Name' => '',
								'Po_Privacy' => $Privacy,
								'Po_Survey' => $Survey,
								'Po_SurveyStatus' => $SurveyStatus,
								'Po_DatePosted' => $now
							);

							$this->db->insert('bs_posts', $data);
							$primaryID = $this->db->insert_id();


							$this->db->where('Po_Us_ID',$UserID);
							$this->db->from('bs_posts');
								$Count = $this->db->count_all_results();


							$data = array(
							'Us_Posts' => $Count
							);

							$this->db->where('Us_ID', $UserID);
							$this->db->update('bs_users', $data);
							$primaryIDArr[] = $primaryID;
						}
					}
				}

				// for defined sub categories
				if($SubCategory){
					$newSubCategories = [];
					if($SubCategory[$valueC]){
						$newSubCategories = explode(',',$SubCategory[$valueC]);
						foreach ($newSubCategories as $keySc => $valueSc) {
							$newSubcatName = $valueSc; // set subcat name
							$data = array(
								'Po_Us_ID' => $UserID,
								'Po_Gr_ID' => $GroupID,
								'Po_To_ID' => $TopicID,
								'Po_Title' => $Title,
								'Po_Description' => $Description,
								'Po_Content' => $Content,
								'Po_Ca_ID' => $newCatId,
								'Po_Ca_Name' => '',
								'Po_Sc_ID' => '',
								'Po_Sc_Name' => $newSubcatName,
								'Po_Privacy' => $Privacy,
								'Po_Survey' => $Survey,
								'Po_SurveyStatus' => $SurveyStatus,
								'Po_DatePosted' => $now
							);

							$this->db->insert('bs_posts', $data);
							$primaryID = $this->db->insert_id();


							$this->db->where('Po_Us_ID',$UserID);
							$this->db->from('bs_posts');
								$Count = $this->db->count_all_results();


							$data = array(
							'Us_Posts' => $Count
							);

							$this->db->where('Us_ID', $UserID);
							$this->db->update('bs_users', $data);
							$primaryIDArr[] = $primaryID;
						}
					}
				}
			}
		}

		// user defined categories
		if($Category){
			$Category = explode(',',$Category);
			foreach ($Category as $keyC => $valueC) {
				$newCatName = $valueC; // set cat name
				// for defined sub categories
				if($otherTxtNewSubCategory){
					$newSubCategories = [];
					if($otherTxtNewSubCategory){
						$newSubCategories = explode(',',$otherTxtNewSubCategory);
						foreach ($newSubCategories as $keySc => $valueSc) {
							$newSubcatName = $valueSc; // set subcat name
							$data = array(
								'Po_Us_ID' => $UserID,
								'Po_Gr_ID' => $GroupID,
								'Po_To_ID' => $TopicID,
								'Po_Title' => $Title,
								'Po_Description' => $Description,
								'Po_Content' => $Content,
								'Po_Ca_ID' => '',
								'Po_Ca_Name' => $newCatName,
								'Po_Sc_ID' => '',
								'Po_Sc_Name' => $newSubcatName,
								'Po_Privacy' => $Privacy,
								'Po_Survey' => $Survey,
								'Po_SurveyStatus' => $SurveyStatus,
								'Po_DatePosted' => $now
							);

							$this->db->insert('bs_posts', $data);
							$primaryID = $this->db->insert_id();


							$this->db->where('Po_Us_ID',$UserID);
							$this->db->from('bs_posts');
								$Count = $this->db->count_all_results();


							$data = array(
							'Us_Posts' => $Count
							);

							$this->db->where('Us_ID', $UserID);
							$this->db->update('bs_users', $data);
							$primaryIDArr[] = $primaryID;
						}
					}
				}
			}
		}

		return $primaryIDArr;*/

		$this->db->insert('bs_posts', $data);
		$primaryID = $this->db->insert_id();


		$this->db->where('Po_Us_ID',$UserID);
		$this->db->from('bs_posts');
		$Count = $this->db->count_all_results();


		$data = array(
		'Us_Posts' => $Count
		);

		$this->db->where('Us_ID', $UserID);
		$this->db->update('bs_users', $data);
		return $primaryID;
	}

	public function Update($PostID, $data) {

//		print_r($data);
//		exit;
		$this->db->where('Po_ID', $PostID);
		return $this->db->update('bs_posts', $data);
	}

	public function UpdateSurveyStatus($PostID, $Status)
	{
		$data = array(
			'Po_SurveyStatus' => $Status
		);

		$this->db->where('Po_ID', $PostID);
		return $this->db->update('bs_posts', $data);
	}

	public function set_UpdateBackcolor($ID, $Backcolor)
	{
		$data = array(
			'Po_Backcolor' => $Backcolor,
			'Po_FileName' => '',
			'Po_Photo' => '',
			'Po_Thumb' => ''
		);

		$this->db->where('Po_ID', $ID);
	   return $this->db->update('bs_posts', $data);
	}


			function updatePOSTLIKE($PostID , $type){
						$points = 1;

					if($type == 'plus'){
					$this->db->set('Po_Likes', 'Po_Likes + ' . (int) $points, FALSE);
					}else if($type == 'minus'){
					$this->db->set('Po_Likes', 'Po_Likes - ' . (int) $points, FALSE);
					}
 		 			$this->db->where('Po_ID', $PostID);
   				 $this->db->update('bs_posts');
			}


			function addfavourite($PostID){
				$this->db->select('bs_favorite.*')->from('bs_favorite');
				$this->db->where('Fa_PO_ID' ,$PostID) ;
				$this->db->where('Fa_Us_ID' , $this->LoggedInUser);
				$query = $this->db->get()->result_array();


				if(count($query) > 0){
					if($query[0]['Fa_Status'] == 'active'){


						$updatedate = array(
							'Fa_Status' => 'unactive'
						);
						$compareData = array(
							'Fa_Us_ID' => $this->LoggedInUser,
							'Fa_PO_ID' => $PostID ,

						);
						$this->db->where(  $compareData);
						$this->db->update('bs_favorite', $updatedate);
						return 'deactivate';
					}else if( $query[0]['Fa_Status'] ==  'unactive'){

						$updatedate = array(
							'Fa_Status' => 'active'
						);
						$compareData = array(
							'Fa_Us_ID' => $this->LoggedInUser,
							'Fa_PO_ID' => $PostID ,

						);
						$this->db->where(  $compareData);
						$this->db->update('bs_favorite', $updatedate);
						return 'activate';
					}

				}
				else{

					$data = array(
						'Fa_Us_ID' => $this->LoggedInUser,
						'Fa_PO_ID' => $PostID ,
						'Fa_Status' => 'active'
					);

					$this->db->insert('bs_favorite', $data);
					$primaryID = $this->db->insert_id();
 				 	return 'activate';
				}

			}
			function updateCounter($PostID){

			$this->db->select('bs_post_like.*')->from('bs_post_like');
			$this->db->where('PL_PO_ID' ,$PostID) ;
			$this->db->where('PL_Us_ID' , $this->LoggedInUser);

		$query = $this->db->get()->result_array();


		if(count($query) > 0){
				if($query[0]['PL_Status'] == 'active'){

				$this->updatePOSTLIKE( $PostID , 'minus');
					$updatedate = array(
                			'PL_Status' => 'unactive'
                		);
			        $compareData = array(
							'PL_Us_ID' => $this->LoggedInUser,
							'PL_PO_ID' => $PostID ,

							);
                		$this->db->where(  $compareData);
                	    $this->db->update('bs_post_like', $updatedate);
 		   		 return 'deactivate';
				  }else if( $query[0]['PL_Status'] ==  'unactive'){
				$this->updatePOSTLIKE( $PostID , 'plus');
				$updatedate = array(
                			'PL_Status' => 'active'
                		);
			      $compareData = array(
							'PL_Us_ID' => $this->LoggedInUser,
							'PL_PO_ID' => $PostID ,

							);
                		$this->db->where(  $compareData);
                	  $this->db->update('bs_post_like', $updatedate);
   				 return 'activate';
				}

		}
		else{

			$data = array(
				'PL_Us_ID' => $this->LoggedInUser,
				'PL_PO_ID' => $PostID ,
				'PL_Status' => 'active'
				);

		$this->db->insert('bs_post_like', $data);
		 $primaryID = $this->db->insert_id();

		 	$this->updatePOSTLIKE( $PostID , 'plus');
					 return 'activate';
		}




			}
	public function set_changephoto($PostID, $FileName, $Photo, $Thumb)
	{
		$data = array(
			'Po_Backcolor' => '',
			'Po_FileName' => $FileName,
			'Po_Photo' => $Photo,
			'Po_Thumb' => $Thumb
		);

		$this->db->where('Po_ID', $PostID);
		return $this->db->update('bs_posts', $data);
	}

	public function delete_row($id)
	{

		$this->db->delete('post_update', array('Pu_post_id' => $id));
	 	$this->db->delete('bs_favorite', array('Fa_PO_ID' => $id));
		$this->db->delete('bs_medias', array('Me_Parent' => $id , 'Me_Type'=>	'post'));
		$this->db->delete('bs_members', array('Me_St_ID' => $id , 'Me_Parent'=>	'post'));
		$this->db->delete('bs_post_mute', array('Mu_Po_ID' => $id));
		$this->db->delete('bs_post_like', array('PL_PO_ID' => $id));
		$this->db->delete('bs_comments', array('Co_Po_ID' => $id));
		$this->db->delete('bs_posts', array('Po_ID' => $id));
	}

	public function get_UserPostCategory($id){
		$this->db->distinct('Po_Ca_Name');
		$this->db->from('bs_posts');
		$this->db->where('Po_To_ID',$id);
		$this->db->where('Po_Ca_ID',0);
		$this->db->select('Po_Ca_Name');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_UserPostSubCategory($id,$catId){
		$this->db->distinct();
		$this->db->select('bs_posts.*, bs_groups.Gr_Privacy, bs_topics.To_Privacy, bs_members.Me_Status');
		$this->db->from('bs_posts');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID AND bs_posts.Po_Gr_ID = bs_topics.To_Gr_ID', 'LEFT');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');

		if(is_numeric($catId)){
			$this->db->join('bs_category', 'bs_category.Ca_ID = bs_posts.Po_Ca_ID','left');
			$this->db->where('Po_Ca_ID',$catId);
			$this->db->select('Po_Sc_Name,bs_category.Ca_Name AS `catName`');
		}else{
			$this->db->where('Po_Ca_Name',$catId);
			$this->db->select('Po_Sc_Name,Po_Ca_Name AS `catName`');
		}
		$this->db->where('Po_To_ID',$id);
		$this->db->where('Po_Sc_ID',0);

		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}

	public function get_GroupTopicsNone($id, $category = null, $search_text = null)
	{
		$this->db->select('Po_ID, Po_Parent, Po_Gr_ID, Po_To_ID, Po_Us_ID, Po_Title, Po_Description, Po_Content,
							(CASE
								WHEN Po_Ca_ID = 0 THEN Po_Ca_Name
								ELSE Po_Ca_ID
							 END) as Po_Ca_ID,
							(CASE
								WHEN Po_Sc_ID = 0 THEN Po_Sc_Name
								ELSE Po_Sc_ID
							 END) as Po_Sc_ID,
							 Po_Ca_Name, Po_Sc_Name, Po_Privacy, Po_Survey, Po_SurveyStatus,
							 Po_FileName, Po_Photo, Po_Thumb, Po_Backcolor, Po_DatePosted, Po_Likes,
							bs_groups.Gr_Privacy, bs_members.Me_Status');
		$this->db->from('bs_posts');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('Po_To_ID', 0);
		$this->db->where('Po_Gr_ID', $id);

		if (!empty($category)) {
			$this->db->group_start();
			$this->db->where('Po_Ca_ID', $category);
			$this->db->or_where('Po_Ca_Name', $category);
			$this->db->group_end();
		}

		if (!empty($search_text)) {
			$this->db->like('Po_Title', $search_text, 'BOTH');
		}

		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}


	public function search_GroupTopicsNone($id,$keyword)
	{
		$this->db->select('bs_posts.*');
		$this->db->from('bs_posts');
		$this->db->where('Po_To_ID', 0);
		$this->db->like('Po_Title', $keyword);
		$this->db->where('Po_Gr_ID', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	private function format_data($data) {

		if (!(empty($data))) {
			foreach ($data as $k=>$row) {
				$data[$k]['AskForPermission'] = false;
				$data[$k]['Searchable'] = true;
				$data[$k]['EncodedID'] = encode_id($row['Po_ID']);

				if (empty($row['Me_Status'])) {
					if (strcasecmp($row['Gr_Privacy'], "public") != 0 ) {
						$count = $this->members_model->check_if_member_record_access_exist("group", $row['Po_Gr_ID'], 0, 0, "approved");

						if (empty($count) && $row['Po_Us_ID'] != $this->LoggedInUser) {
							$data[$k]['AskForPermission'] = true;
							$data[$k]['Searchable'] = false;
						}
					}

					if (!$data[$k]['AskForPermission'] && !empty($row['Po_To_ID'])) {

						if (strcasecmp($row['To_Privacy'], "public") != 0  && $row['To_Us_ID'] != $this->LoggedInUser) {
							$count = $this->members_model->check_if_member_record_access_exist("topic", $row['Po_Gr_ID'],  $row['Po_To_ID'], 0, "approved");

							if (empty($count)) {
								$data[$k]['AskForPermission'] = true;
								$data[$k]['Searchable'] = false;
							}
						}
					}


					if (!$data[$k]['AskForPermission']) {
						if (strcasecmp($row['Po_Privacy'], "private") == 0) {
							$data[$k]['AskForPermission'] = true;
						}

						if (strcasecmp($row['Po_Privacy'], "secret") == 0) {
							$data[$k]['Searchable'] = false;
						}
					}

				} else if (strcasecmp($row['Me_Status'], "pending") == 0) {
					$data[$k]['AskForPermission'] = true;

					if (strcasecmp($row['Po_Privacy'], "secret") == 0) {
						$data[$k]['Searchable'] = false;
					}

				}
			}
		}

		return $data;
	}



		function show_public_popular_posts_promote_Data($userID, $data){
				$result = $this->get_popular_posts_promotes($userID, $data);



				if (!empty($result)) {
                			foreach ($result as $key => $row) {


                				$result[$key]->Raw_ID = $row->Po_ID;
                				$result[$key]->Po_ID = encode_id($row->Po_ID);
                				$result[$key]->Po_URL = base_url() . 'account/view/post/' . $row->Po_Slug;
                				$result[$key]->Po_U_URL = base_url() . 'u/' . encode_id($row->Po_Us_ID);

								$result[$key]->Us_Name = (strlen($row->Us_Alias)==0)   ? $row->Us_Name : $row->Us_Alias ;


                				$result[$key]->Po_Thumb = base_url() . ((strlen($row->Po_Thumb)==0) ? "img/post.png" : str_replace(base_url(), '', $row->Po_Thumb));
                				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;



                			}
                		}

                		$grid = array(
                			'totalRecords' =>  $this->cnt,
                			'data' => $result
                		);

             	return $grid;

		}
		function show_public_popular_posts($userID, $data){
				$result = $this->get_popular_posts($userID, $data);

				if (!empty($result)) {
                			foreach ($result as $key => $row) {


                				$result[$key]->Raw_ID = $row->Po_ID;
                				$result[$key]->Po_ID = encode_id($row->Po_ID);
                				$result[$key]->Po_URL = base_url() . 'account/view/post/' . $row->Po_Slug;
                				$result[$key]->Po_U_URL = base_url() . 'u/' . encode_id($row->Po_Us_ID);

								$result[$key]->Us_Name = (strlen($row->Us_Alias)==0)   ? $row->Us_Name : $row->Us_Alias ;


                				$result[$key]->Po_Thumb = base_url() . ((strlen($row->Po_Thumb)==0) ? "img/post.png" : str_replace(base_url(), '', $row->Po_Thumb));
                				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;



                			}
                		}

                		$grid = array(
                			'totalRecords' =>  $this->cnt,
                			'data' => $result
                		);


                		return $grid;

		}

	function get_popular_posts_promotes($userID, $data) {


//		p.Po_Backcolor  , p.Po_Bn_ID ,
//        p.Po_Bn_Name  ,  p.Po_Bn_Page  , p.Po_Ca_ID  ,
//        p.Po_Ca_Name  , p.Po_Content  , p.Po_DatePosted  ,
//        p.Po_Sc_ID  ,   p.Po_Privacy  ,  p.Po_Pin  ,  p.Po_Photo  ,
//        p.Po_Parent  ,   p.Po_Likes  ,  p.Po_Keywords  ,
//         p.Po_ID  ,   p.Po_Gr_ID  ,    p.Po_Filename  ,
//            p.Po_Description  ,  p.Po_DatePosted  ,   p.Po_Keywords  ,
//               p.Po_reward  , p.Po_Us_ID  ,
//               p.Po_To_ID  , p.Po_Title  ,
//                 p.Po_Thumb  , p.Po_SurveyStatus  , p.Po_Survey  ,
//                 p.Po_Slug  , p.Po_Sc_Name  , p.Po_Sc_ID  ,
//                  p.Po_Survey  , p.Po_Survey  , p.Po_Survey  ,
//
//          u.Us_Alias , u.Us_Thumb , u.Us_Name , u.Us_JobTitle ,
//           g.Gr_Privacy ,



 		 $this->db->select('q.* , p.Po_ID, p.Po_Likes ,
 		  
 		   		p.Po_Us_ID,
					p.Po_Thumb,
					p.Po_Title,
					p.Po_Description,
					u.Us_Name,
					u.Us_JobTitle,
					u.Us_Thumb,
					p.Po_Slug,
					u.Us_Alias
					 ');
		$this->db->from('promoted_post q');


		$this->db->join('bs_posts p' , 'p.Po_ID = q.Pp_Post_ID');
		$this->db->join('bs_users u' , 'u.Us_ID = q.Pp_Us_ID');
		$this->db->join('bs_groups g' , 'g.Gr_ID  = q.Pp_group_ID');


		$this->db->where("q.Pp_status", 'active');
		$this->db->where("q.Pp_Expiry_Date >", date('Y-m-d'));
		$this->db->order_by('q.Pp_ID', 'DESC');
 		 $query = $this->db->get();
		$record = $query->result();


		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $record;

		//		$this->db->order_by('p.Po_Likes', 'DESC');
//		$this->db->order_by("p.Po_DatePosted", "DESC");
//		$this->db->select("SQL_CALC_FOUND_ROWS , q.*  ,  p.*, g.Gr_Privacy,
//
//		 u.Us_Name, u.Us_Alias ,  u.Us_JobTitle, u.Us_Thumb", FALSE);
//		$this->db->from("promoted_post q");
//
//		$this->db->join("bs_posts p", "p.Po_ID = q.Pp_Post_ID", "INNER");
//		$this->db->join("bs_users u", "u.Us_ID = p.Po_Us_ID", "INNER");
//		$this->db->join("bs_groups g", "g.Gr_ID = p.Po_Gr_ID", "INNER");
//		$this->db->where("q.Pp_Us_ID", $userID);
//	    $this->db->where("p.Po_Privacy", 'public');
//	    $this->db->where("g.Gr_Privacy", 'public');


//		if ($data['keyword']) {
//			$this->db->where("p.Po_Title LIKE '%".$data['keyword']."%'");
//		    $this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");
//		}
//
// 		if ($data['alpha']) {
//			$this->db->order_by("p.Po_Title", $data['alpha']);
//		}
//		if ($data['latest']) {
//			$this->db->order_by("p.Po_DatePosted", "DESC");
//		}

//		$query = $this->db->get();
//		$record = $query->result();
// 			print_r($data);
// 			print_r($record);
// 			exit;
//		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
//		$this->cnt = $query->row()->COUNT;
//
//		return $record;
	}
	function get_popular_posts($userID, $data) {

		$this->db->order_by('p.Po_Likes	', 'DESC');
		$this->db->order_by("p.Po_DatePosted", "DESC");
		$this->db->select("SQL_CALC_FOUND_ROWS p.Po_ID, p.*, g.Gr_Privacy,

		 u.Us_Name, u.Us_Alias ,  u.Us_JobTitle, u.Us_Thumb", FALSE);
		$this->db->from("bs_posts p");
		$this->db->join("bs_users u", "u.Us_ID = p.Po_Us_ID", "INNER");
		$this->db->join("bs_groups g", "g.Gr_ID = p.Po_Gr_ID", "INNER");
// 		$this->db->join("bs_topics t", "t.To_ID = p.Po_To_ID AND p.Po_Gr_ID = t.To_Gr_ID", "INNER");
	    $this->db->where("p.Po_Privacy", 'public');
	    $this->db->where("g.Gr_Privacy", 'public');
// 		$this->db->where("p.Po_Us_ID", $userID);

		if ($data['keyword']) {
			$this->db->where("p.Po_Title LIKE '%".$data['keyword']."%'");
		    $this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");
		}

 		if ($data['alpha']) {
			$this->db->order_by("p.Po_Title", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("p.Po_DatePosted", "DESC");
		}
// 		if ($data['popular']) {
// 			$this->db->order_by("p.Po_Likes", "DESC");
// 		}

		$query = $this->db->get();
		$record = $query->result();
// 			print_r($data);
// 			print_r($record);
// 			exit;
		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $record;
	}
function show_posts($userID, $data) {


		$result = $this->get_posts($userID, $data);



		if (!empty($result)) {
			foreach ($result as $key => $row) {


				$result[$key]->Raw_ID = $row->Po_ID;
				$result[$key]->Po_ID = encode_id($row->Po_ID);
				$result[$key]->Po_URL = base_url() . 'account/view/post/' . $row->Po_Slug;
			    $result[$key]->Po_U_URL = base_url() . 'u/' . encode_id($row->Po_Us_ID);
				$result[$key]->Us_Name = (strlen($row->Us_Alias)==0)   ? $row->Us_Name : $row->Us_Alias ;


				$result[$key]->Po_Thumb = base_url() . ((strlen($row->Po_Thumb)==0) ? "img/post.png" : str_replace(base_url(), '', $row->Po_Thumb));
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;

			}
		}

		$grid = array(
			'totalRecords' =>  $this->cnt,
			'data' => $result
		);

		return $grid;
	}



		  function get_member_information($id, $type, $userID = NULL) {



    		$userID = ($userID == NULL) ? $this->LoggedInUser : $userID;
    		$this->db->select("bs_members.*");
    		$this->db->from('bs_members');
    		$this->db->where('Me_Parent', $type);
    		if ($type == 'group') {
    			$this->db->where('Me_Gr_ID', $id);
    		} else if ($type == 'topic') {
    			$this->db->where('Me_To_ID', $id);
    		} else {
    			$this->db->where('Me_St_ID', $id);
    		}
     		$this->db->where('Me_Us_ID', $userID);
    		$data = $this->db->get()->result_array();

    		if (sizeOf($data) > 0) {
    			return array('status' => 'success', 'role' => $data[0]['Me_Role'], 'role_status' => $data[0]['Me_Status'], 'is_invited' => $data[0]['Me_IsInvited'] , 'Me_ViaLink' => $data[0]['Me_ViaLink'], 'memberId' => $data[0]['Me_ID']);
     		} else {
    			return array('status' => 'non_member', 'role' => 'non_member' , 'role_status' => false, 'is_invited' => false);
    		}
    	}


		function checkmutestatu($post_id , $userid , $groupid){
								$where = array(
									"Mu_Status" => 'active',
									"Mu_Po_ID" =>  $post_id,
									"Mu_Gr_ID" => $groupid,
									"Mu_Us_ID" => $userid
								);



		$muteinfo = $this->db->select('bs_post_mute.*')->from('bs_post_mute')->where($where)->get()->result_array();

				if(count($muteinfo) > 0){
					return true;
				}else{
					return false;
				}
	}
				public function show_members($data) {



				$this->db->select('bs_topics.*');
				$this->db->from('bs_topics');
				$this->db->where('To_ID' , $data['pid']);
				$datax = $this->db->get()->result_array();





 					if(!isset($data['topicdata'])) {
						$this->db->select('bs_posts.*');
						$this->db->from('bs_posts');
				  		$this->db->where('bs_posts.Po_ID' , $data['pid']);
						$adminpost = $this->db->get()->result_array();

						$Po_Privacy = $adminpost[0]['Po_Privacy'];
						$adminpost = $adminpost[0]['Po_Us_ID'];
 						$detailcre = " (Post Creator)";
					}else{
						$adminpost = $datax[0]['To_Us_ID'] ;
						$Po_Privacy = 'private';
						$detailcre = " (Topic Creator)";
					}




						if($data['role'] == ''){

							if($Po_Privacy == 'private'){
								$result1 = $this->get_members($data, $Po_Privacy, $adminpost);

//
//								$this->db->select('bs_users.* , bs_members.* , bs_groups.* , bs_roles.*');
//								$this->db->from('bs_members');
//								$this->db->join('bs_users' , 'bs_users.Us_ID = bs_members.Me_Us_ID');
//								$this->db->join('bs_groups' , 'bs_groups.Gr_ID = bs_members.Me_Gr_ID');
//								$this->db->join("bs_roles", "bs_members.Me_Role = Rl_Code", "LEFT");
////								$this->db->where('bs_members.Me_Role' , 'superadmin');
//								$this->db->where('bs_members.Me_Gr_ID' ,$_POST['id']);
////								$this->db->where('bs_members.Me_Us_ID' , $datax[0]['To_Us_ID']);
//								$datares = $this->db->get()->result_array();

//
								$result = $this->get_members_for_post($data, $Po_Privacy);
//								echo '<pre>';
//								print_r($result);
//								exit;
								$result = array_merge($result1, $result);
//								$result = array_merge($datares, $result);

							}else {
								$result = $this->get_members_for_post($data ,$Po_Privacy);
//								echo '<pre>';
//								print_r($result);

							}



						}else{
							$result = $this->get_members_for_post($data , $Po_Privacy);

						}


					 $this->db->select('bs_notification.*');
					 $this->db->from('bs_notification');
							if(isset($data['topicdata'])) {
					 $this->db->where('bs_notification.No_To' ,$datax[0]['To_Gr_ID']  );
					}else{
					 $this->db->where('bs_notification.No_To' ,$data['id'] );
					 }

	 				$query = $this->db->get()->result_array();
     					foreach ($result as $key => $row) {

							foreach($query as $key1 => $row1){
									if($row1['No_From'] == $result[$key]['Me_Us_ID'] ){
										$result[$key]['notificationID'] = $row1['No_From'];
										$result[$key]['notificationType'] = $row1['No_Type'];
										$result[$key]['No_From_Type'] =   $row1['No_From_Type'];
										$result[$key]['No_To_Type'] = 	$row1['No_To_Type'] ;
									}
							}
   					}
     		$roles = array("member" => 1, "admin" => 2, "superadmin" => 3);

    		$group_id = $data['id'] ;
    		$myMemberInfo = $this->get_member_information($group_id, 'group');
    		if (!empty($result)) {
    			foreach ($result as $key => $row) {
    				$result[$key]['Us_Thumb'] = (strlen($row['Us_Thumb'])==0) ? base_url()."img/nophoto.png" : $row['Us_Thumb'];
    				$suffix = ($row['Me_Us_ID'] == $adminpost) ? $detailcre : "";
    				$result[$key]['Us_Name'] = $row['Us_Name'] . $suffix;
					$result[$key]['Creator'] = ($adminpost == $row['Me_Us_ID']) ? 1 : 0;
					$result[$key]['merole'] = ($adminpost == $this->LoggedInUser) ? 1 : 0;
					$result[$key]['group_admin'] = ($row['Me_Us_ID'] == $row['Gr_Us_ID']) ? 1 : 0;
    				$result[$key]['Us_URL'] = base_url() . 'u/' . encode_id($row['Me_Us_ID']);
    				$result[$key]['Rl_Type'] = ($row['Rl_Type'] == "") ? "Choose a role" : $row['Rl_Type'];
    				$result[$key]['Me_CanEdit'] = 0;
    				$result[$key]['Is_Self'] = ($this->LoggedInUser == $row['Me_Us_ID']) ? 1 : 0;
					if(!isset($data['topicdata'])) {
						$result[$key]['Me_Mute'] = $this->checkmutestatu( $data['pid'] , $row['Me_Us_ID'] , $row['Me_Gr_ID']) ? 1 : 0;

					}
    				if ($row['Gr_Us_ID'] == $this->LoggedInUser) {
    					$result[$key]['Me_CanEdit'] = 1;
    				} else {
    					$loggedInRole = $roles[$myMemberInfo['role']];
    					$recordRole = $roles[$myMemberInfo['role']];
    					if ($loggedInRole > $recordRole) {
    						$result[$key]['Me_CanEdit'] = 1;
    					}
    				}
    			}
    		}

    		$grid = array(
    			'totalRecords' => count($result),
    			'data' => $result
    		);

    		return $grid;
    	}

	function get_join_group_Post_modal($userID, $data) {

 		$result = $this->get_join_group_Post($userID, $data);

		if($data['fav'] == 'true'){

			$result = $this->topic_format_privacy_favourite_array(	$result , $userID);
		}
 				if (!empty($result)) {
			foreach ($result as $key => $row) {


				$result[$key]['Raw_ID'] = $row['Po_ID'];
				$result[$key]['Po_ID'] = encode_id($row['Po_ID']);
				$result[$key]['Po_URL'] = base_url() . 'account/view/post/' .$row['Po_Slug'];
				$result[$key]['Us_Name'] = (strlen($row['Us_Alias'])==0)   ? $row['Us_Name'] : $row['Us_Alias'] ;
				$result[$key]['Po_U_URL'] = base_url() . 'u/' . encode_id($row['Po_Us_ID']);

				$result[$key]['Po_Thumb'] = base_url() . (str_replace(base_url(), '', ((strlen($row['Po_Thumb'])==0) ? "img/post.png" : $row['Po_Thumb'])));
				$result[$key]['Us_Thumb'] = (strlen($row['Us_Thumb'])==0) ? base_url()."img/nophoto.png" : $row['Us_Thumb'];


			}
		}

		$grid = array(
			'totalRecords' =>  count($result),
			'data' => $result
		);

		return $grid;
	}
function get_posts_user($userID, $data) {


		$this->db->select("SQL_CALC_FOUND_ROWS p.Po_ID, p.*, g.Gr_Privacy,  u.Us_Name, u.Us_Alias ,  u.Us_JobTitle, u.Us_Thumb", FALSE);
		$this->db->from("bs_posts p");
		$this->db->join("bs_users u", "u.Us_ID = p.Po_Us_ID", "INNER");
		$this->db->join("bs_groups g", "g.Gr_ID = p.Po_Gr_ID", "INNER");
// 		$this->db->join("bs_topics t", "t.To_ID = p.Po_To_ID AND p.Po_Gr_ID = t.To_Gr_ID", "INNER");
// 		 $this->db->where("p.Po_Privacy", 'public');
      	$this->db->where("p.Po_Us_ID", $userID);

		if ($data['keyword']) {
			$this->db->where("p.Po_Title LIKE '%".$data['keyword']."%'");
// 			$this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");


		}

// 		if ($data['privacy']) {
// 			$this->db->where("p.Po_Privacy", $data['privacy']);
// 		}
		if ($data['alpha']) {
			$this->db->order_by("p.Po_Title", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("p.Po_DatePosted", "DESC");
		}
		if ($data['popular']) {
			$this->db->order_by("p.Po_Likes", "DESC");
		}

		$this->db->limit(10);
		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}

	function get_posts($userID, $data) {



		$this->db->select("SQL_CALC_FOUND_ROWS p.Po_ID, p.*, g.Gr_Privacy,
		u.Us_Name, u.Us_Alias ,
		 u.Us_JobTitle, u.Us_Thumb", FALSE);
		$this->db->from("bs_posts p");
		$this->db->join("bs_users u", "u.Us_ID = p.Po_Us_ID", "INNER");
		$this->db->join("bs_groups g", "g.Gr_ID = p.Po_Gr_ID", "INNER");
// 		$this->db->join("bs_topics t", "t.To_ID = p.Po_To_ID AND p.Po_Gr_ID = t.To_Gr_ID", "INNER");
		 $this->db->where("g.Gr_Privacy", 'public');
		 $this->db->where("p.Po_Privacy", 'public');



		if ($data['keyword']) {
			$this->db->where("p.Po_Title LIKE '%".$data['keyword']."%'");
		    $this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");
		}
// 		if ($data['privacy']) {
// 			$this->db->where("p.Po_Privacy", $data['privacy']);
// 		}
		if ($data['alpha']) {
			$this->db->order_by("p.Po_Title", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("p.Po_DatePosted", "DESC");
		}
		if ($data['popular']) {
			$this->db->order_by("p.Po_Likes", "DESC");
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}
	function topic_format_privacy_favourite($result , $userID){
		if (!empty($result)) {
			foreach ($result as $key => $row) {

				if($this->checkfavourite($row->Po_ID , $userID) == 0){
					unset($result[$key]);
				}

			}
		}

    	return $result;
	}
	function topic_format_privacy_favourite_array($result , $userID){
		if (!empty($result)) {
			foreach ($result as $key => $row) {

				if($this->checkfavourite($row['Po_ID'] , $userID) == 0){
					unset($result[$key]);
				}

			}
		}

		return $result;
	}
	function checkfavourite($postid , $LoggedInUser){
		$this->db->select('bs_favorite.*')->from('bs_favorite');
		$this->db->where('Fa_PO_ID' ,$postid) ;
		$this->db->where('Fa_Us_ID' ,  $LoggedInUser);
		$this->db->where('Fa_Status' , 'active');
		$query = $this->db->get()->result_array();


		if(count($query) > 0){
			return 1;
		}else{
			return 0;
		}
	}
	function show_user_created_posts($userID, $data) {

		$result = $this->get_posts_user($userID, $data);




		if($data['fav'] == 'true'){

			$result = $this->topic_format_privacy_favourite(	$result , $userID);
		}


  			if (!empty($result)) {
			foreach ($result as $key => $row) {


				$result[$key]->Raw_ID = $row->Po_ID;
				$result[$key]->Po_ID = encode_id($row->Po_ID);
				$result[$key]->Po_URL = base_url() . 'account/view/post/' . $row->Po_Slug;
				$result[$key]->Us_Name = (strlen($row->Us_Alias)==0)   ? $row->Us_Name : $row->Us_Alias ;
				$result[$key]->Po_U_URL = base_url() . 'u/' . encode_id($row->Po_Us_ID);

				$result[$key]->Po_Thumb = base_url() . ((strlen($row->Po_Thumb)==0) ? "img/post.png" : str_replace(base_url(), '', $row->Po_Thumb));
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;


			}
		}

		$grid = array(
			'totalRecords' => count($result),
			'data' => $result
		);

		return $grid;
	}
	function get_members_for_post($data , $priavacy) {


		$group =  $data['id'];

		$role = $data['role'];
		$pid = $data['pid'];

		$this->db->select("u.Us_ID ,  m.Me_Gr_ID , g.Gr_ID , m.Me_Parent, m.Me_St_ID, m.Me_Mute , m.Me_ID ,
		   m.Me_Us_ID, m.Me_Role, g.Gr_Us_ID, r.Rl_Type, m.Me_Status, m.Me_IsInvited,
		 u.Us_Name, u.Us_FName, u.Us_LName, u.Us_Email, u.Us_Company, u.Us_JobTitle, u.Us_photo, u.Us_Thumb,u.Us_DateTime");
		$this->db->from('bs_members' . " m");
		$this->db->join("bs_users u", "m.Me_Us_ID = u.Us_ID", "LEFT");
		$this->db->join("bs_groups g", "m.Me_Gr_ID = g.Gr_ID", "LEFT");
		$this->db->join("bs_roles r", "m.Me_Role = r.Rl_Code", "LEFT");


		if(isset($data['topicdata'])){

			$this->db->where("m.Me_To_ID", $pid );
			$this->db->where("m.Me_Parent", 'topic');
		}else{

			if($role == ''){
				if($priavacy == 'private'){
					$dataw = array(

						'm.Me_St_ID' => $pid,
						'm.Me_Status' => 'active',
						'm.Me_Parent'=>  'post'
					);
				}else{
					$dataw = array(

						'm.Me_Gr_ID' => $group,
						'm.Me_Status' => 'active',
						'm.Me_Parent'=>  'group'
					);
				}



				$this->db->where($dataw);


			}else if( $role == 'superadmin' || $role == 'admin'   ){
				if($priavacy == 'public'){
					$this->db->where("m.Me_Gr_ID", $group);
					$this->db->where("m.Me_Parent", 'group');
				}else{
					$this->db->where("m.Me_St_ID", $pid);
					$this->db->where("m.Me_Parent", 'post');
				}

			}else{
				if($priavacy == 'public'){
					$this->db->where("m.Me_Gr_ID", $group);
					$this->db->where("m.Me_Parent", 'group');
				}else{
					$this->db->where("m.Me_St_ID", $pid);
					$this->db->where("m.Me_Parent", 'post');
				}


			}


		}

		if ($role != 'pending') {
			if ($role != '') {
				$this->db->where("m.Me_Role", $role);
				$this->db->where("m.Me_Status", 'active');
			}

		} else {
			$this->db->where("m.Me_Status", 'pending');
		}
		if ($data['keyword'] != "") {
			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$data['keyword']."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$data['keyword']."%'");
			$this->db->group_end();
		}

		$query = $this->db->get();
		$data = $query->result_array();



		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}
 function get_members($data , $Po_Privacy , $adminpost) {


		$group =  $data['id'];
		$role = $data['role'];
	    $pid = $data['pid'];

		$this->db->select("u.Us_ID ,m.Me_Gr_ID , g.Gr_ID ,  m.Me_Parent, m.Me_St_ID, m.Me_Mute , m.Me_ID ,   m.Me_Us_ID, m.Me_Role, g.Gr_Us_ID, r.Rl_Type, m.Me_Status, m.Me_IsInvited,
		 u.Us_Name, u.Us_FName, u.Us_LName, u.Us_Email, u.Us_Company, u.Us_JobTitle, u.Us_photo, u.Us_Thumb,u.Us_DateTime");
		$this->db->from('bs_members' . " m");
		$this->db->join("bs_users u", "m.Me_Us_ID = u.Us_ID", "LEFT");
		$this->db->join("bs_groups g", "m.Me_Gr_ID = g.Gr_ID", "LEFT");
		$this->db->join("bs_roles r", "m.Me_Role = r.Rl_Code", "LEFT");


			if(isset($data['topicdata'])){

				$this->db->where("m.Me_To_ID", $pid );
				$this->db->where("m.Me_Parent", 'topic');
			}else{

					if($role == ''){


				if($Po_Privacy == 'private'){
					$dataw = array(

						'm.Me_Us_ID' => $adminpost,
						'm.Me_Gr_ID' => $group,
					    "m.Me_Parent" => 'group'
					);

				}else{
					$dataw =array(

						'm.Me_Gr_ID' => $group,
						'm.Me_Status' => 'active',
						'm.Me_St_ID' => '0' ,
						"m.Me_Parent" => 'group'
					);
				}
 					$this->db->where($dataw);
 						}else if( $role == 'superadmin' || $role == 'admin'   ){
						$this->db->where("m.Me_Gr_ID", $group);

						$this->db->where("m.Me_Parent", 'group');
					}else{
						$this->db->where("m.Me_St_ID", $pid);
						$this->db->where("m.Me_Parent", 'post');
					}


			}

		if ($role != 'pending') {
			if ($role != '') {
				$this->db->where("m.Me_Role", $role);
				$this->db->where("m.Me_Status", 'active');
			}

		} else {
			$this->db->where("m.Me_Status", 'pending');
		}
		if ($data['keyword'] != "") {
			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$data['keyword']."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$data['keyword']."%'");
			$this->db->group_end();
		}

		$query = $this->db->get();
		$data = $query->result_array();


//			print_r($data);
//			exit;
		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}

		function get_join_group_Post($userID, $data){

  						$this->db->select('bs_members.* ');
        				$this->db->from('bs_members');

        				$this->db->where('Me_Us_ID' , $userID);
        				$this->db->where('Me_Role !=' , 'superadmin' );
        				$this->db->where('Me_Role !=' , 'admin' );
         			    $res = $this->db->get()->result_array();



						 $alldata = [];

        				for($x = 0 ; $x < count($res); $x++){
 								$this->db->select("SQL_CALC_FOUND_ROWS p.Po_ID, p.*, g.Gr_Privacy,  u.Us_Name, u.Us_Alias ,  u.Us_JobTitle, u.Us_Thumb", FALSE);
                                		$this->db->from("bs_posts p");
                                		$this->db->join("bs_users u", "u.Us_ID = p.Po_Us_ID", "INNER");
                                		$this->db->join("bs_groups g", "g.Gr_ID = p.Po_Gr_ID", "INNER");
//                                 		$this->db->join("bs_topics t", "t.To_ID = p.Po_To_ID AND p.Po_Gr_ID = t.To_Gr_ID", "INNER");
 										$this->db->where('p.Po_Gr_ID' , $res[$x]['Me_Gr_ID']);


										if ($data['keyword']) {
												$this->db->where("p.Po_Title LIKE '%".$data['keyword']."%'");
												$this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");

										}
                                		if ($data['alpha']) {
                                			$this->db->order_by("p.Po_Title", $data['alpha']);
                                		}
                                		if ($data['latest']) {
                                			$this->db->order_by("p.Po_DatePosted", "DESC");
                                		}
                                		if ($data['popular']) {
                                			$this->db->order_by("p.Po_Likes", "DESC");
                                		}
									    if ($data['popular']) {
                                			$this->db->order_by("p.Po_Likes", "DESC");
                                		}

                                		$query = $this->db->get()->result_array();


										for($o = 0 ; $o < count($query); $o++ ){
 											array_push($alldata, $query[$o]);
										}
 								}
 					 return $alldata;
		}
	function get_user_created_posts($userID, $data) {


     	$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}


	function show_topics_post($id, $category = null, $search_text = null, $subcatogery = null) {

		$post_items = $this->get_topics_post($id, $category, $search_text, $subcatogery);
		$post_items = $this->format_data($post_items);

		$data = array();
		$ctr = 0;
		if (!empty($post_items)) {
			foreach ($post_items as $item) {
//				if ($item['Searchable'] || $item['To_Privacy'] == 'public') {

					$item_href = base_url() .'account/view/post/'.$item['Po_Slug'];
					$target = "";
					if ($item['AskForPermission']) {
						$target = '#subtopicAccessModal';
					}

					$thumb = str_replace(base_url(), '', $item['Po_Thumb']);
					$thumb = base_url() . ((empty($item['Po_Thumb'])) ? 'img/post.png' : $thumb);

					$data[$ctr++] = array(
										'Po_ID' => $item['Po_ID'],
										'EncodedID' => $item['EncodedID'],
										'Po_Title' => (strlen($item['Po_Title'])>=50) ? substr($item['Po_Title'], 0, 50)."... " : $item['Po_Title'],
										'Po_Ca_ID' => $item['Po_Ca_ID'],
										'Po_Sc_ID' => $item['Po_Sc_ID'],
										'Po_Thumb' => $thumb,
										'Searchable' => $item['Searchable'],
										'AskForPermission' => $item['AskForPermission'],
										'Po_Backcolor' => $item['Po_Backcolor'],
										'Po_Href' => $item_href,
										'Target' => $target,
										'To_Pin' => $item['Po_Pin'],
										'Po_Privacy' => $item['Po_Privacy'],
										'To_Privacy' => $item['To_Privacy'],
										'To_Us_ID' => $item['To_Us_ID'],
									);
				}
			}
//		}

		return $data;
	}

	function get_topics_post($id, $category = null, $search_text = null , $subcat = null) {

		$this->db->distinct();
		$this->db->select("Po_ID, Po_Pin ,Po_Slug ,   Po_Us_ID, Po_Gr_ID, Po_Title, Po_Description,
							(CASE
								WHEN Po_Ca_ID = 0 THEN Po_Ca_Name
								ELSE Po_Ca_ID
							 END) as Po_Ca_ID,
							(CASE
								WHEN Po_Sc_ID = 0 THEN Po_Sc_Name
								ELSE Po_Sc_ID
							END) as Po_Sc_ID,
							Po_Ca_Name, Po_Sc_Name, Po_Privacy,
							Po_FileName, Po_Photo, Po_Thumb, Po_Backcolor, Po_DatePosted, Po_Likes,
							bs_groups.Gr_Privacy, bs_members.Me_Status,
							bs_topics.To_ID, bs_topics.To_Us_ID, bs_topics.To_Privacy");
		$this->db->from('bs_posts');
		$this->db->join('bs_topics', 'bs_topics.To_ID = bs_posts.Po_To_ID', 'INNER');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID AND bs_posts.Po_To_ID = bs_members.Me_To_ID AND bs_posts.Po_ID = bs_members.Me_St_ID AND bs_members.Me_Parent='subtopic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('Po_To_ID', $id);





		if(isset($_POST['private']) &&  $_POST['private'] == 'yes'){
 				$this->db->where('Po_Privacy', 'private');
 			}

		if (!empty($category)) {
			$this->db->group_start();
			if(!empty(	$subcat)){
				$this->db->where('Po_Sc_ID', $subcat);
				$this->db->where('Po_Sc_Name', $subcat);
			}else{
				$this->db->where('Po_Ca_ID', $category);
				$this->db->or_where('Po_Ca_Name', $category);
			}


			$this->db->group_end();
		} else if(!empty(	$subcat)){

			$this->db->group_start();
			if(is_numeric($subcat)){
				$this->db->where('Po_Sc_ID', $subcat);
			}else{


				$subcatogery = explode("_",$subcat);
				if(count($subcatogery) > 1){
					$stringSub = '';
					foreach ($subcatogery as $key => $row) {
						$stringSub .= $row.' ';

					}
					$this->db->or_where('Po_Sc_Name',$stringSub);

				}else{
					$this->db->or_where('Po_Sc_Name', $subcatogery[0]);

				}

			}


			$this->db->group_end();

		}


		if (!empty($search_text)) {
			$this->db->like('Po_Title', $search_text, 'BOTH');
		}
		$this->db->order_by('Po_Pin DESC');
		$this->db->order_by('Po_DatePosted DESC');
		$this->db->limit(5);
		$query = $this->db->get();


			$ress = $query->result_array();

		if($category != ''){

			foreach ($ress as $key => $row) {

				if($row['Po_Ca_ID'] == $category){

				}else{
					unset($ress[$key]);
				}
			}

		}
		return $ress;
	}
}
