<?php
class Category_model extends CI_Model {

	private $Tablename = 'bs_privacy';

	public function __construct()
	{
			$this->load->database();

	}

	public function get_RecordByType($type)
	{
		$this->db->where('Pr_type', $type);
		$query = $this->db->get($this->Tablename);

		return $query->result_array();		
	}

	public function get_type_privacy_settings($level, $type) {
		$this->db->where('Pr_Type', $type);
		$this->db->where('Pr_level', $level);
		$query = $this->db->get($this->Tablename);


		return $query->row();	
	}
}