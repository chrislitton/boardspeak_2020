<?php
class Keyword_model extends CI_Model {

	private $Tablename = 'bs_keywords';
	private $LoggedInUser;

	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
	}

	public function get_records() {

		$this->db->select('*');
		$this->db->from($this->Tablename);
		$this->db->order_by('Kw_Keyword');

		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_record($id) {
		$this->db->from($this->Tablename);
		$this->db->where('Kw_ID', $id);

		return $this->db->get()->row_array();
	}

	public function get_keyword($keyword) {
		$this->db->where('Kw_Keyword', $keyword);
		$this->db->from($this->Tablename);
        return $this->db->get()->row_array();
	}

	public function get_list($query = '', $filter_banned = true) {

		$this->db->select('Kw_Keyword as name');
		$this->db->from($this->Tablename);
		$this->db->like("Kw_Keyword", $query, 'BOTH');

		if ($filter_banned) {
			$this->db->where("Kw_banned", 0);
		}

		$this->db->order_by('Kw_Keyword');

		$query = $this->db->get();
		return $query->result_array();
	}

	public function add_keyword($keyword) {
	    $data = array(
				'Kw_Keyword' => $keyword,
				'Kw_Banned' => 0
		    );

	    return $this->insert($data);
	}

	public function insert($data) {
	    $this->db->insert($this->Tablename, $data);
		return $this->db->insert_id();
	}


	public function update($ID, $data) {
		$this->db->where('Kw_ID', $ID);
	    return $this->db->update($this->Tablename, $data);
	}

	public function delete($id)
	{
		$this->db->delete($this->Tablename, array('Kw_ID' => $id));
	}
	public function get_keywords_occurence($keyword) {

		$this->db->select('Gr_ID')->from("bs_groups")->like('Gr_Keywords', $keyword, 'both')->get();
		$group_keyword = $this->db->last_query();

		$this->db->select('To_ID')->from("bs_topics")->like('To_Keywords', $keyword, 'both')->get();
		$topic_keyword = $this->db->last_query();

		$this->db->select('Po_ID')->from("bs_posts")->like('Po_Keywords', $keyword, 'both')->get();
		$post_keyword = $this->db->last_query();

		$this->db->from("($group_keyword UNION $topic_keyword UNION $post_keyword) as unionTable");
		$query = $this->db->get();
		return $query->result_array();
	}
}