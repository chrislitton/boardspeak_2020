<?php
class Groups_model extends CI_Model {

	protected $Tablename = 'bs_groups';
	protected $PrimaryID = "Gr_ID";
	private $LoggedInUser;

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('members_model');

		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->LoggedInUser = $this->session->userdata['logged_in']['bs_id'];
		}
	}

	public function searchGroup($searchText)
	{
		$this->db->select('bs_groups.*,bs_category.Ca_Name, bs_users.Us_Name');
		$this->db->from('bs_groups');
		$this->db->join('bs_category', 'bs_category.Ca_ID = bs_groups.Gr_Ca_ID','left');
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_groups.Gr_Us_ID','left');
		$this->db->like('bs_groups.Gr_Name', $searchText);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_UserGroupsCount($UserID) {

		$this->db->where('Gr_Us_ID',$UserID);
		$this->db->from('bs_groups');
        return $this->db->count_all_results();
    }
		public function getPingComment(){

    		$this->db->select('bs_comment_tag.*', 'bs_users.*');
    		$this->db->from('bs_comment_tag');
			$this->db->where('Ta_Co_ID' , $_POST['commentId']);
   			$query =	$this->db->get()->result_array();
			return $query;

		}

	public function get_UserGroupsFiltered($UserID, $CatID)
	{
		$this->db->select('bs_groups.*,bs_category.Ca_Name');
		$this->db->from('bs_groups');
		$this->db->join('bs_category', 'bs_category.Ca_ID = bs_groups.Gr_Ca_ID','left');
		$this->db->where('bs_groups.Gr_Us_ID', $UserID);
		if ($CatID!=0) $this->db->where('bs_category.Ca_ID', $CatID);
		$this->db->order_by('Gr_ID', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
    }

    public function get_UserGroupsLimit($UserID, $limit, $start)
	{
        $this->db->limit($limit, $start);
		$this->db->order_by('Gr_ID', 'DESC');
		$query = $this->db->get_where('bs_groups', array('Gr_Us_ID' => $UserID));
		return $query->result_array();
    }

    public function get_Record($id)
	{
		$query = $this->db->get_where('bs_groups', array('Gr_ID' => $id));
		return $query->row_array();
	}

	public function get_GroupRows($id = false)
	{
		if ($id === false)
		{
			$query = $this->db->order_by('Gr_ID', 'DESC');
			$query = $this->db->get('bs_groups');
			return $query->result_array();
		}

		$query = $this->db->get_where('bs_groups', array('Gr_ID' => $id));
		return $query->result_array();
	}

	public function get_Group($id)
	{
		$this->db->select('bs_groups.*,bs_users.Us_Name,bs_users.Us_Photo,bs_category.Ca_Name');
		$this->db->from('bs_groups');
		$this->db->where('Gr_ID', $id);
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_groups.Gr_Us_ID','left');
		$this->db->join('bs_category', 'bs_category.Ca_ID = bs_groups.Gr_Ca_ID','left');
		$query = $this->db->get();


		return $query->row_array();
	}

public function counterofpost($id){

			$this->db->select('bs_posts.*');
			$this->db->from('bs_posts');
			$this->db->where('Po_Gr_ID', $id);
			$data = $this->db->get()->result_array();
			return $data;
		}

		function get_promoted_group($id)
		{
			$where = array(
				'Pg_group_ID'	=> $id
			);
			$this->db->select('promoted_group.*');
			$this->db->from('promoted_group');
			$this->db->where($where);
			$res = 	$this->db->get()->result_array();

			if(count($res) > 0){
				return '1';
			}else{
				return '0';
			}
		}

		public function counterofMember( $id , $type=''){

    	$this->db->select('bs_members.*');
    	$this->db->from('bs_members');

		if($type == 'post'){
			$this->db->where('Me_St_ID', $id);
		}else{
			$this->db->where('Me_Gr_ID', $id);
		}

		$this->db->where('Me_Parent', 'group');
        $this->db->where_in('Me_Status', array('approved' , 'active'));
		$data = $this->db->get()->result_array();


		return $data;
	}
	public function get_GroupByType($Type=false, $SearchText = false)
	{
		$this->db->distinct();
		$this->db->select('bs_groups.*, bs_members.Me_Status');
		$this->db->from($this->Tablename , ' as bs_groups ');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_groups.Gr_ID AND bs_members.Me_Parent='group' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');

		if (strlen($Type)>0) $this->db->where('bs_groups.Gr_Privacy', $Type);
		if ($SearchText==true) $this->db->like('bs_groups.Gr_Name', $SearchText);

		$this->db->order_by('Gr_ID', 'DESC');

		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}

	public function get_UserGroups($UserID)
	{
		$this->db->select('bs_groups.*');
		$this->db->from('bs_groups');
		$this->db->where('Gr_Us_ID', $UserID);
		$this->db->order_by('Gr_ID', 'DESC');
		return $this->db->get()->result_array();
	}

		function checkexist($is){
		$this->db->select('bs_members.*');
		$this->db->from('bs_members');
		$this->db->where('Me_Gr_ID'  , $is);
		$this->db->where('Me_Us_ID' , $this->LoggedInUser);
		$rezs = $this->db->get()->result_array();
			return $rezs;
		}
	function show_groups($data) {

		$result = $this->get_groups($data);
//		echo '<pre>';
//		print_r($result);
//			exit;
		if (!empty($result)) {
			foreach ($result as $key => $row) {
//				print_r(count($this->checkexist($row->Gr_ID)));

				if(count($this->checkexist($row->Gr_ID)) > 0){
					unset($result[$key]);
					continue;
				}else{
					$result[$key]->Raw_ID = $row->Gr_ID;
					$result[$key]->Gr_ID = encode_id($row->Gr_ID);
					$result[$key]->Normal_Gr_ID =  $row->Gr_ID;
					$result[$key]->Gr_URL = base_url() . 'account/view/group/' . $row->Gr_Slug;
					$result[$key]->Gr_Thumb = base_url() . (str_replace(base_url(), '', ((strlen($row->Gr_Thumb)==0) ? "img/group.png" : $row->Gr_Thumb)));

				}
				}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_groups($data) {
            		$this->db->select("SQL_CALC_FOUND_ROWS g.Gr_ID, g.*,u.*", FALSE);
            		$this->db->from("bs_groups g");
            		$this->db->join('bs_users u ' , 'u.Us_ID = g.Gr_Us_ID' );
					$this->db->where('g.Gr_Us_ID !=' , $this->LoggedInUser );
//            		$this->db->join('bs_members m', 'm.Me_Parent = "group"
//            		AND m.Me_Gr_ID = g.Gr_ID AND m.Me_Us_ID != '. $this->LoggedInUser, 'LEFT');

            		if ($data['keyword']) {
            			$this->db->where("g.Gr_Name LIKE '%".$data['keyword']."%'");
            		    $this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");
            		}
            		if ($data['privacy']) {
            			$this->db->where("g.Gr_Privacy", $data['privacy']);
            		}
            		if ($data['alpha']) {
            			$this->db->order_by("g.Gr_Name", $data['alpha']);
            		}
            		if ($data['latest']) {
            			$this->db->order_by("g.Gr_DatePosted", "DESC");
            		}
            		if ($data['popular']) {
            			$this->db->order_by("g.Gr_Likes", "DESC");
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}

	function show_user_created_groups($userID, $data) {


		$result = $this->get_user_created_groups($userID, $data);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Raw_ID = $row->Gr_ID;
				$result[$key]->Gr_ID = encode_id($row->Gr_ID);
				$result[$key]->Gr_URL = base_url() . 'account/view/group/' . $row->Gr_Slug;
				$result[$key]->Gr_Thumb = base_url() . (str_replace(base_url(), '', ((strlen($row->Gr_Thumb)==0) ? "img/group.png" : $row->Gr_Thumb)));
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_user_created_groups($userID, $data) {
//		print_r($data);
//		print_r($userID);
//		exit;
		$this->db->select("SQL_CALC_FOUND_ROWS g.Gr_ID, g.*, u.*", FALSE);
		$this->db->from("bs_groups g");
		$this->db->join('bs_users u' , 'u.Us_ID = g.Gr_Us_ID' );
//		$this->db->join('bs_members m', 'm.Me_Parent = "group" AND m.Me_Gr_ID = g.Gr_ID AND m.Me_Us_ID = '. $this->LoggedInUser, 'LEFT');
		$this->db->where("g.Gr_Us_ID", $userID);

		if ($data['keyword']) {
			$this->db->where("g.Gr_Name LIKE '%".$data['keyword']."%'");

		}
		if ($data['privacy']) {
			$this->db->where("g.Gr_Privacy", $data['privacy']);
		}
		if ($data['alpha']) {
			$this->db->order_by("g.Gr_Name", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("g.Gr_DatePosted", "DESC");
		}
		if ($data['popular']) {
			$this->db->order_by("g.Gr_Likes", "DESC");
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}

	public function get_FollowedGroups($id)
	{
        $this->db->limit(10);
		$this->db->select('bs_groups.*');
		$this->db->from('bs_groups');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_groups.Gr_ID AND bs_members.Me_Parent='group' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('bs_members.Me_Parent', 'group');
		$this->db->where('bs_members.Me_Us_ID', $id);
		$this->db->where_in('bs_members.Me_Role', array('member','moderator'));
		$this->db->where('bs_members.Me_Status', 'approved');
		$query = $this->db->get();
		return $query->result_array();
	}

		function getuserDetailForallcomment($group){

			$this->db->select('bs_members.* , bs_users.*');
			$this->db->from('bs_members');
			$this->db->join('bs_users' , 'bs_users.Us_ID = bs_members.Me_Us_ID' , 'left');

			$this->db->where('bs_members.Me_Gr_ID' , $group);
			$this->db->where_in('bs_members.Me_Role' , ['member' , 'superadmin' , 'admin']);
 			$query =	$this->db->get()->result_array();
			return $query;

		}
	function getUserDetailForComment($group , $text){

//		$data[$key]['id'] = $row['Us_ID'];
//		$data[$key]['fullname'] = (strlen( $row['Us_Alias'])==0) ?$row['Us_Name'] : $row['Us_Alias'];
//		$data[$key]['email'] = $row['Us_Email'];
//		$data[$key]['profile_picture_url'] = (strlen($row['Us_Thumb'])==0) ? base_url()."img/nophoto.png" : $row['Us_Thumb'];

		$this->db->select('bs_members.Me_Gr_ID ,bs_members.Me_Us_ID ,  bs_members.Me_Role ,
		 bs_users.Us_Thumb , bs_users.Us_Email , bs_users.Us_ID ,
		  bs_users.Us_Alias , bs_users.Us_Name ')->distinct('bs_users.Us_ID');

			 $this->db->from('bs_members');
 			$this->db->where_in('bs_members.Me_Role' , ['member' , 'superadmin' , 'admin']);
			$this->db->join('bs_users' , 'bs_users.Us_ID = bs_members.Me_Us_ID'  );
	 		$this->db->like('bs_users.Us_Name', $text ,'both' );
 			$this->db->where('bs_members.Me_Gr_ID' , $group);
		 	$this->db->limit(4);
 			 $query =	$this->db->get()->result_array();
 			return $query;

	}
		public function get_usetContect($userID){



			$this->db->select('bs_contacts.*');
			$this->db->from('bs_contacts');
			$this->db->where('Co_Us_ID' , $userID);
			$this->db->where('Co_Status' , 'confirmed');
			$query = $this->db->get()->result_array();
 				return count($query);

		}


	public function Insert($UserID, $Name, $Description, $CatID, $Category, $SubID, $SubCategory, $Privacy, $keywords='' , $checkPackage)
	{
	    $this->load->helper('url');

	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'Gr_Us_ID' => $UserID,
		'Gr_Name' => $Name,
		'Gr_Slug' => $this->slug($Name , '_').'_'.rand(0,9999),
		'Gr_Description' => $Description,
		'Gr_Ca_ID' => $CatID,
		'Gr_Ca_Name' => $Category,
		'Gr_Sc_ID' => $SubID,
		'Gr_Sc_Name' => $SubCategory,
		'Gr_Privacy' => $Privacy,
		'Gr_DatePosted' => $now,
		'Gr_Keywords' => $keywords,
		'Gr_packegtype' =>	$checkPackage
	    );

	    $this->db->insert('bs_groups', $data);
		$primaryID = $this->db->insert_id();

		// adding creator to memebers
		$this->members_model->add_Member($primaryID, 'group', 'superadmin', 'active');
		$this->updateUserGroupsCount($UserID);
		return $primaryID;
	}
		function slug($string, $spaceRepl = "-")
{
    $string = str_replace("&", "and", $string);

    $string = preg_replace("/[^a-zA-Z0-9 _-]/", "", $string);

    $string = strtolower($string);

    $string = preg_replace("/[ ]+/", " ", $string);

    $string = str_replace(" ", $spaceRepl, $string);

    return $string;
}

	public function updateUserGroupsCount($UserID)
	{
		$this->db->where('Gr_Us_ID',$UserID);
		$this->db->from('bs_groups');
        $Count = $this->db->count_all_results();
		$this->db->flush_cache();

		$data = array('Us_Groups' => $Count);
		$this->db->where('Us_ID', $UserID);
	    $this->db->update('bs_users', $data);
	}


	public function Update($GroupID, $Name, $Description, $CatID, $Category, $SubID, $SubCategory, $Privacy, $keywords='' ,  $groupplan)
	{
	    $data = array(
			'Gr_Name' => $Name,
			'Gr_Description' => $Description,
			'Gr_Ca_ID' => $CatID,
			'Gr_Ca_Name' => $Category,
			'Gr_Sc_ID' => $SubID,
			'Gr_Sc_Name' => $SubCategory,
			'Gr_Privacy' => $Privacy,
			'Gr_Keywords' => $keywords,
			'Gr_packegtype' => $groupplan
		);

		$this->db->where('Gr_ID', $GroupID);
	    return $this->db->update('bs_groups', $data);
	}

	public function set_UpdateBackcolor($ID, $Backcolor)
	{
	    $data = array(
		'Gr_Backcolor' => $Backcolor,
		'Gr_FileName' => '',
		'Gr_Photo' => '',
		'Gr_Thumb' => ''
	    );

		$this->db->where('Gr_ID', $ID);
	    return $this->db->update('bs_groups', $data);
	}


	public function set_changephoto($GroupID, $FileName, $Photo, $Thumb)
	{
	    $data = array(
		'Gr_FileName' => $FileName,
		'Gr_Photo' => $Photo,
		'Gr_Thumb' => $Thumb,
		'Gr_Backcolor' => ''
	    );

		$this->db->where('Gr_ID', $GroupID);
	    return $this->db->update('bs_groups', $data);
	}



	public function delete_row($id)
	{
		$this->db->delete('bs_comments', array('Co_Gr_ID' => $id));
		$this->db->delete('bs_members', array('Me_Gr_ID' => $id));
		$this->db->delete('bs_posts', array('Po_Gr_ID' => $id));
		$this->db->delete('bs_topics', array('To_Gr_ID' => $id));
		$this->db->delete('bs_groups', array('Gr_ID' => $id));
	}

	private function format_data($data) {

		if (!(empty($data))) {
			foreach ($data as $k=>$row) {
				$data[$k]['AskForPermission'] = false;
				$data[$k]['Searchable'] = true;
				$data[$k]['EncodedID'] = encode_id($row['Gr_ID']);

				if (strcasecmp($row['Gr_Privacy'], "public") != 0) {

					if (empty($row['Me_Status'])) {
						$data[$k]['AskForPermission'] = true;

						if (strcasecmp($row['Gr_Privacy'], "secret") == 0) {
							$data[$k]['Searchable'] = false;
						}

					} else if (strcasecmp($row['Me_Status'], "pending") == 0) {
						$data[$k]['AskForPermission'] = true;

						if (strcasecmp($row['Gr_Privacy'], "secret") == 0) {
							$data[$k]['Searchable'] = false;
						}
					}
				}
			}
		}

		return $data;
	}


	public	function get_group_user($id){
		$this->db->select('bs_groups.Gr_ID , bs_groups.Gr_Name ,bs_groups.Gr_Slug ');
		$this->db->from($this->Tablename);
		$this->db->where('Gr_Us_ID' , $id );
		$this->db->order_by('Gr_ID', 'DESC');
       return $this->db->get()->result_array();
		}
	public function get_user_group_access($UserID, $Role=NULL)
	{
			 if($Role == NULL){
				 $Role = 'superadmin';
			 }
		$memberData = $this->members_model->get_member_records('group', $Role);

//			 print_r($memberData);
//			 exit;
		$this->db->select('bs_groups.*');
		$this->db->from($this->Tablename);

		//filter
		$this->db->where('bs_groups.Gr_Privacy' , 'public');
		$this->db->or_group_start();
		$this->db->where('bs_groups.Gr_Privacy !=' , 'public');
		$this->db->where('Gr_Us_ID', $UserID);
		$this->db->group_end();

		if (!empty($memberData)) {
			$this->db->or_group_start();
			$this->db->where('bs_groups.Gr_Privacy !=' , 'public');
			$this->db->where_in('Gr_ID', $memberData);
			$this->db->group_end();
		}

		$this->db->order_by('Gr_ID', 'DESC');
		return $this->db->get()->result_array();
	}

	function show_featured_groups($data) {

		$result = $this->get_featured_groups($data);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Raw_ID = $row->Gr_ID;
				$result[$key]->Gr_ID = encode_id($row->Gr_ID);
				$result[$key]->Gr_URL = base_url() . 'account/view/group/' . $row->Gr_Slug;
				$result[$key]->Gr_Thumb = base_url() . (str_replace(base_url(), '', ((strlen($row->Gr_Thumb)==0) ? "img/group.png" : $row->Gr_Thumb)));
			}
		}




		$grid = array(
			'totalRecords' => count($result),
			'data' => $result
		);

		return $grid;
	}



	function get_featured_groups($data) {







//						 u.aggrement_one , u.aggrement_three , u.aggrement_two ,
//		  u.Us_Topics , u.Us_Thumb , u.Us_Status ,
//		  u.Us_Posts , u.Us_Photo , u.Us_Phone ,
//		  u.Us_NoOfEmployees , u.Us_Name , u.Us_Mute_Limit ,
//		   u.Us_LName , u.Us_JobTitle , u.Us_IsPrivate ,
//		    u.Us_IsAdmin , u.Us_ID , u.Us_Groups ,
//		     u.Us_Filename , u.Us_Features , u.Us_FName ,
//		      u.Us_Email , u.Us_DateTime , u.Us_Company ,
//		      u.Us_Coins , u.Us_BirthDate , u.Us_Background ,
//		      u.Us_Backcolor , u.Us_Alias  ,

//		g.group_Question,g.Gr_packegtype,g.Gr_Us_ID,
//		  g.Gr_Topics,g.Gr_Thumb,
//		g.Gr_Slug,g.Gr_Sc_Name,g.Gr_Sc_ID,
//		g.Gr_Privacy,g.Gr_Photo,g.Gr_Name,
//
//		g.Gr_Members,g.Gr_Likes,g.Gr_Keywords,
//		g.Gr_ID,g.Gr_FileName,g.Gr_Description,
//
//		g.Gr_DatePosted,g.Gr_Ca_Name,g.Gr_Ca_ID,
//		g.Gr_Backcolor,g.Gr_Active,
  		$this->db->select('
  		g.Gr_ID,
		 g.Gr_Thumb,
		 g.Gr_Likes,
		 g.Gr_Name,
		g.Gr_Privacy,
		g.Gr_Slug,u.Us_Posts,
		  q.* ');
		$this->db->from('promoted_group q');

		$this->db->join('bs_users u' , 'u.Us_ID  = q.Pg_Us_ID');
		$this->db->join('bs_groups g' , 'g.Gr_ID  = q.Pg_group_ID');


//		$this->db->where("q.Pg_status", 'active');
		$query = $this->db->get();
		$record = $query->result();

//		echo "<pre>";
//
//		print_r($record);
//		exit;

		return $record;

//
//		$this->db->select("SQL_CALC_FOUND_ROWS g.Gr_ID, g.* ,u.*  ", FALSE );
//		$this->db->from("bs_groups g");
//		$this->db->join('bs_users u ' , 'u.Us_ID = g.Gr_Us_ID' );
//		$this->db->where("g.Gr_Privacy", 'public');
//
//		if ($data['keyword']) {
//			$this->db->where("g.Gr_Name LIKE '%".$data['keyword']."%'");
//			$this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");
//
//		}
//		if ($data['alpha']) {
//			$this->db->order_by("g.Gr_Name", $data['alpha']);
//		}
//		if ($data['latest']) {
//			$this->db->order_by("g.Gr_DatePosted", "DESC");
//		}
//		if ($data['popular']) {
//			$this->db->order_by("g.Gr_Likes", "DESC");
//		}
//
//		$query = $this->db->get();
//		$data = $query->result();
//
//		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
//		$this->cnt = $query->row()->COUNT;
//
//		return $data;
	}

	function topic_pinning_post(){

		if($_POST['post_type'] == 'Post'){
			$this->db->where('Po_ID', $_POST['post_item']);
			$data = array(
				'Po_Pin' => $_POST['post_decide']
			);
			$resp =  $this->db->update('bs_posts',  $data);
		}else{
			$this->db->where('To_ID', $_POST['post_item']);
			$data = array(
				'To_Pin' => $_POST['post_decide']
			);
			$resp =  $this->db->update('bs_topics',  $data);
		}
		$status = 	$_POST['post_decide'] == 1 ? ' Pinned ' : ' Unpinned ';
		if($resp){
			return $_POST['post_type']." is ". $status . "Successfully";
		}else{
			return $_POST['post_type']." is not ". $status;
		}

	}
		function group_pinning_post(){

			   if($_POST['post_type'] == 'Post'){
			    $this->db->where('Po_ID', $_POST['post_item']);
			   $data = array(
 						'Po_Pin' => $_POST['post_decide']
               	 );
			    $resp =  $this->db->update('bs_posts',  $data);
			   }else{
			    $this->db->where('To_ID', $_POST['post_item']);
			    $data = array(
                 'To_Pin' => $_POST['post_decide']
                   );
			    $resp =  $this->db->update('bs_topics',  $data);
			   }
				$status = 	$_POST['post_decide'] == 1 ? ' Pinned ' : ' Unpinned ';
				if($resp){
				return $_POST['post_type']." is ". $status . "Successfully";
				}else{
				return $_POST['post_type']." is not ". $status;
				}

		}
	function show_suggested_groups($data) {

		$result = $this->get_suggested_groups($data);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Raw_ID = $row->Gr_ID;
				$result[$key]->Gr_ID = encode_id($row->Gr_ID);
				$result[$key]->Gr_URL = base_url() . 'account/view/group/' . $row->Gr_Slug;
				$result[$key]->Gr_Thumb = base_url() . (str_replace(base_url(), '', ((strlen($row->Gr_Thumb)==0) ? "img/group.png" : $row->Gr_Thumb)));
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_suggested_groups($data) {
		$this->db->select("SQL_CALC_FOUND_ROWS g.Gr_ID, g.*", FALSE);
		$this->db->from("bs_groups g");
		$this->db->where("g.Gr_Privacy", 'public');

		if ($data['keyword']) {
			$this->db->where("g.Gr_Name LIKE '%".$data['keyword']."%'");
		}
		if ($data['alpha']) {
			$this->db->order_by("g.Gr_Name", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("g.Gr_DatePosted", "DESC");
		}
		if ($data['popular']) {
			$this->db->order_by("g.Gr_Likes", "DESC");
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}


			function searchbyheaderprofile($dat){
					$this->db->select('bs_users.*')->from('bs_users');
					$this->db->where("Us_Name LIKE '%".$dat."%'");
				   $this->db->or_where("Us_Alias LIKE '%".$dat."%'");
					return	$this->db->get()->result_array();

			}
		function searchbyheadertopic($dat){

			$this->db->select('bs_topics.*')->from('bs_topics');
			$this->db->where("To_Name LIKE '%".$dat."%'");
		    $this->db->or_where("To_Keywords LIKE '%".$dat."%'");


			return	$this->db->get()->result_array();
		}


			function searchbyheadercatogry($dat){
				$this->db->select('bs_category.*');
				$this->db->from('bs_category');
				$this->db->where("Ca_Name LIKE '%".$dat."%'");
				return	$this->db->get()->result_array();
			}
			function searchbyheaderpost($dat){
			$this->db->select('bs_posts.* , bs_groups.*')->from('bs_posts');
			  $this->db->join('bs_groups' , 'bs_groups.Gr_ID  = bs_posts.Po_Gr_ID');
			  $this->db->where('bs_groups.Gr_Privacy', 'public');
				$this->db->where("bs_posts.Po_Title LIKE '%".$dat."%'");
				$this->db->or_where("bs_posts.Po_Keywords LIKE '%".$dat."%'");
			return	$this->db->get()->result_array();
			}

		function searchbyheadergroup($dat){

				$this->db->select('bs_groups.*')->from('bs_groups');
				$this->db->where("Gr_Name LIKE '%".$dat."%'");
				$this->db->or_where("Gr_Keywords LIKE '%".$dat."%'");
				return	$this->db->get()->result_array();

		}

		function member_leave_group($user_id , $groupid){


			$res = $this->db->delete('bs_members', array('Me_Gr_ID' => $groupid , 'Me_Us_ID' => $user_id));
			return $res;
		}
	function show_user_followed_groups($userID, $data) {


		$result = $this->get_user_followed_groups($userID, $data);


		if (!empty($result)) {
			foreach ($result as $key => $row) {





					$result[$key]->Raw_ID = $row->Gr_ID;
					$result[$key]->Gr_ID = encode_id($row->Gr_ID);
					$result[$key]->Gr_URL = base_url() . 'account/view/group/' . $row->Gr_Slug;
					$result[$key]->Gr_Thumb = base_url() . (str_replace(base_url(), '', ((strlen($row->Gr_Thumb)==0) ? "img/group.png" : $row->Gr_Thumb)));


			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_user_followed_groups($userID, $data) {


		$this->db->select('m.* , g.* , u.*');
		$this->db->from("bs_members m");
		$this->db->join('bs_groups g', 'm.Me_Gr_ID = g.Gr_ID');
		$this->db->join('bs_users u' , 'u.Us_ID = g.Gr_Us_ID');

		$this->db->where("g.Gr_Us_ID !=", $userID);
		$this->db->where("m.Me_Us_ID", $userID);
// 		$this->db->where("m.Me_To_ID", $userID);
//  		$this->db->where("m.Me_Role !=", 'superadmin');
//  		$this->db->where("m.Me_Role !=", 'admin');
//  		 $this->db->where("m.Me_Role", 'member');


		$this->db->where("m.Me_Parent", 'group');
		if ($data['keyword']) {
			$this->db->where("g.Gr_Name LIKE '%".$data['keyword']."%'");
			$this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");
		}
		if ($data['privacy']) {
			$this->db->where("g.Gr_Privacy", $data['privacy']);
		}
		if ($data['alpha']) {
			$this->db->order_by("g.Gr_Name", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("g.Gr_DatePosted", "DESC");
		}
		if ($data['popular']) {
			$this->db->order_by("g.Gr_Likes", "DESC");
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}


function SaveAnswerofGroupWithEncode (){

	$groupid = decode_id( $_POST['group_id']);
	$answer = $_POST['answer'];

	$answer = json_encode($answer);

	if(isset($_POST['post_id'])){
		$data = array(
			'ga_Answer' => 	$answer  ,
			'ga_Us_ID' => $this->LoggedInUser,
			'ga_gr_ID' => $_POST['post_id'],
			'ga_answer_type' => 'post',
		);
	}else{
		$data = array(
			'ga_Answer' => 	$answer  ,
			'ga_Us_ID' => $this->LoggedInUser,
			'ga_gr_ID' => $groupid,
			'ga_answer_type' => 'group',
		);
	}

		$res = 	$this->db->insert('bs_group_answer', $data);
    	 return 		$res ;
	}
	function SaveAnswerofGroup (){

	$groupid = $_POST['group_id'];
	$answer = $_POST['answer'];

	$answer = json_encode($answer);
 		$data = array(
 			'ga_Answer' => 	$answer  ,
			'ga_Us_ID' => $this->LoggedInUser,
			'ga_gr_ID' => $groupid,
			'ga_answer_type' => 'group',
		);
		$res = 	$this->db->insert('bs_group_answer', $data);
    	 return 		$res ;
	}

	function deleteQuestion(){


			$groupid = $_POST['groupid'];
 			$this->db->select('bs_groups.group_Question');
            $this->db->from('bs_groups');
            $this->db->where('Gr_ID', decode_id($groupid));
            $query = 	$this->db->get();
            $res =  $query->result_array();

			if(!empty($res[0]['group_Question'])){
			$questionArray = 	json_decode($res[0]['group_Question'] , true);
 					for($x = 0 ; $x < count(json_decode($res[0]['group_Question'] , true)) ; $x++){
 					if($_POST['question_Id'] == json_decode($res[0]['group_Question'] , true)[$x]['id']){

 						 unset($questionArray[$x]);
 						 break;
 					}

	  				}
 			 }

			$newquestion = array();

 			foreach ($questionArray as $value) {
				    array_push($newquestion, $value);
				}

			 $data = array(
			  'group_Question'	=>  json_encode($newquestion)
			 );


 		   $this->db->where('Gr_ID', decode_id($groupid));
            return $this->db->update('bs_groups',  $data);




	}

	function updateQuestion(){



			$groupid = $_POST['groupid'];
 			$this->db->select('bs_groups.group_Question');
            $this->db->from('bs_groups');
            $this->db->where('Gr_ID', decode_id($groupid));
            $query = 	$this->db->get();
            $res =  $query->result_array();

			if(!empty($res[0]['group_Question'])){
			$questionArray = 	json_decode($res[0]['group_Question'] , true);
 					for($x = 0 ; $x < count(json_decode($res[0]['group_Question'] , true)) ; $x++){
 					if($_POST['question_Id'] == json_decode($res[0]['group_Question'] , true)[$x]['id']){

 						 $questionArray[$x]['value'] = $_POST['updateQuestion'];
 						 break;
 					}

	  				}
 			 }

			$newquestion = array();

 			foreach ($questionArray as $value) {
				    array_push($newquestion, $value);
				}

			 $data = array(
			  'group_Question'	=>  json_encode($newquestion)
			 );


 		   $this->db->where('Gr_ID', decode_id($groupid));
            return $this->db->update('bs_groups',  $data);
	}


		function GetAnswerOfUserwithOUt(){
		         $groupid = $_POST['groupid'];
         			$this->db->select('bs_group_answer.*');
                    $this->db->from('bs_group_answer');
                    $this->db->where('ga_gr_ID', decode_id( $groupid) );
                    $this->db->where('ga_Us_ID',  $_POST['userId'] );
			$this->db->where('ga_answer_type',  'group' );

                    $query = 	$this->db->get();
                    $res =  $query->result_array();

        		return $res;
		}
	function GetAnswerOfUser(){


		if(isset($_POST['fromtype'])){

			$this->db->select('bs_members.Me_St_ID');
			$this->db->from('bs_members');
			$this->db->where('Me_ID ',  $_POST['fromtype']);
			$ress = $this->db->get()->result_array();



			$this->db->select('bs_group_answer.*');
			$this->db->from('bs_group_answer');
			$this->db->where('ga_gr_ID',  $ress[0]['Me_St_ID'] );
			$this->db->where('ga_Us_ID',  $_POST['userId'] );
			$this->db->where('ga_answer_type',  'post' );
			$query = 	$this->db->get();
		}else{
			$groupid = $_POST['groupid'];
			$this->db->select('bs_group_answer.*');
			$this->db->from('bs_group_answer');
			$this->db->where('ga_gr_ID',  $groupid );
			$this->db->where('ga_Us_ID',  $_POST['userId'] );
			$this->db->where('ga_answer_type',  'group' );
			$query = 	$this->db->get();
		}

            $res =  $query->result_array();

		return $res;
	}
	function getSaveQuestionwithEncode($data){

			$groupid = $_POST['group_id'];
 			$this->db->select('bs_groups.group_Question');
            $this->db->from('bs_groups');
            $this->db->where('Gr_ID', $groupid);
            $query = 	$this->db->get();
            $res =  $query->result_array();
 			 return $res;
	}
	function getSaveQuestion($data){

			$groupid = decode_id($data['groupid']);

			$this->db->select('bs_groups.group_Question');
            $this->db->from('bs_groups');
            $this->db->where('Gr_ID', $groupid);
            $query = $this->db->get();
            $res =  $query->result_array();


            return $res;
	}
	function saveQuestion($data){

		$groupid = decode_id($data['groupid']);
// 		$question = 	implode(",",$data['question']);
		$question = 	$data['question'];



 		$this->db->select('bs_groups.group_Question');
 		$this->db->from('bs_groups');
 			$this->db->where('Gr_ID', $groupid);
 	      $query = 	$this->db->get();
 	     $res =  $query->result_array();
	  		if(!empty($res[0]['group_Question'])){

	  				for($x = 0 ; $x < count(json_decode($res[0]['group_Question'] , true)) ; $x++){
						 array_push($question, json_decode($res[0]['group_Question'] , true)[$x]);
	  				}


		 }


			 $data = array(
			  'group_Question'	=>  json_encode($question)
			 );
 		   $this->db->where('Gr_ID', $groupid);
            return $this->db->update('bs_groups',  $data);

	}
	function show_group_topics($id, $category = null, $search_text = null, $all = null , $subcat =null) {

		$topic_items = $this->get_group_topics($id, $category, $search_text , $all , $subcat);
		$topic_items = $this->topic_format_privacy($topic_items);
		$topic_items = $this->topic_format_privacy_favourite($topic_items);

		$data = array();
		$ctr = 0;

		if (!empty($topic_items)) {
			foreach ($topic_items as $item) {

					$itemType = '';
				if ($item['Searchable']) {

					if ($item['Item_Type'] == 'Topic'){
						$itemType  = 'Topic';
						$item_href = base_url() .'account/view/topic/'.$item['To_Slugs'];
					}else{
						$itemType  = 'Post';
						$item_href = base_url() .'account/view/post/'.$item['To_Slugs'];
						}


					$target = "";
					if ($item['AskForPermission']) {
						// $item_href =  '<a class="board-link" href="#" data-toggle="modal" data-target="#topicAccessModal" data-id="'.$item['EncodedID'].'">';
						$item_href =  '#';
						if ($item['Item_Type'] == 'Topic')
							$target = '#topicAccessModal';
						else
							$target = '#subtopicAccessModal';
					}

					$thumb = str_replace(base_url(), '', $item['To_Thumb']);
					$thumb = base_url() . ((empty($item['To_Thumb'])) ? 'img/topic.png' : $thumb);
					if(isset($_POST['favourite']) && $_POST['favourite'] == 'yes') {
						 if($item['favourite'] == 1){
							 $data[$ctr++] = array(
								 'To_ID' => $item['To_ID'],
								 'EncodedID' => $item['EncodedID'],
								 'To_Name' => (strlen($item['To_Name'])>=50) ? substr($item['To_Name'], 0, 50)."... " : $item['To_Name'],
								 'To_Ca_ID' => $item['To_Ca_ID'],
								 'To_Sc_ID' => $item['To_Sc_ID'],
								 'To_Gr_ID' => $item['To_Gr_ID'],
								 'To_Thumb' => $thumb,
								 'Searchable' => $item['Searchable'],
								 'AskForPermission' => $item['AskForPermission'],
								 'To_Backcolor' => $item['To_Backcolor'],
								 'To_Href' => $item_href,
								 'To_Pin' => $item['To_Pin'],
								 'item_Type'=> $itemType,
								 'Target' => $target,
								 'Likes' => $item['To_Likes'],
								 'Favourite' => $item['favourite'],
								 'To_Privacy' => $item['To_Privacy']
							 );

						 }
					}else{
						$data[$ctr++] = array(
							'To_ID' => $item['To_ID'],
							'EncodedID' => $item['EncodedID'],
							'To_Name' => (strlen($item['To_Name'])>=50) ? substr($item['To_Name'], 0, 50)."... " : $item['To_Name'],
							'To_Ca_ID' => $item['To_Ca_ID'],
							'To_Sc_ID' => $item['To_Sc_ID'],
							'To_Gr_ID' => $item['To_Gr_ID'],
							'To_Thumb' => $thumb,
							'Searchable' => $item['Searchable'],
							'AskForPermission' => $item['AskForPermission'],
							'To_Backcolor' => $item['To_Backcolor'],
							'To_Href' => $item_href,
							'To_Pin' => $item['To_Pin'],
							'item_Type'=> $itemType,
							'Target' => $target,
							'Likes' => $item['To_Likes'],
							'Favourite' => $item['favourite'],
							'To_Privacy' => $item['To_Privacy']
						);
					}

				}
			}
		}

		return $data;
	}

	function get_group_topics($id, $category = null, $search_text = null , $all = null ,$subcat= null) {



  		$this->db->distinct();
		$this->db->select(" To_Pin , To_Slugs,  To_ID,   To_Us_ID, To_Gr_ID, 
		To_Name, To_Description, To_Keywords,
							(CASE
								WHEN bs_topics.To_Ca_ID = 0 THEN bs_topics.To_Ca_Name
								ELSE bs_topics.To_Ca_ID
							END) as To_Ca_ID,
							(CASE
								WHEN bs_topics.To_Sc_ID = 0 THEN bs_topics.To_Sc_Name
								ELSE bs_topics.To_Sc_ID
							END) as To_Sc_ID,
							To_Ca_Name, To_Sc_Name, To_Privacy, To_Filename, To_Photo, To_Thumb,
							To_Backcolor, To_DatePosted, To_Likes, 'Topic' as Item_Type,
							bs_groups.Gr_Privacy, bs_members.Me_Status");
		$this->db->from('bs_topics');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_topics.To_Gr_ID AND
		 bs_topics.To_ID = bs_members.Me_To_ID AND bs_members.Me_Parent = 'topic' 
		 AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('bs_topics.To_Gr_ID', $id);

       $this->db->get();

		$topic_query = $this->db->last_query();


		$this->db->distinct();
		$this->db->select("  Po_Pin as To_Pin, Po_Slug as To_Slugs ,   Po_ID as To_ID,
		 Po_Us_ID as To_Us_ID,
		  Po_Gr_ID as To_Gr_ID, 
		  Po_Title as To_Name,  Po_Description as To_Description, Po_Keywords,
							(CASE
								WHEN Po_Ca_ID = 0 THEN Po_Ca_Name
								ELSE Po_Ca_ID
							 END) as To_Ca_ID,
							(CASE
								WHEN Po_Sc_ID = 0 THEN Po_Sc_Name
								ELSE Po_Sc_ID
							 END) as To_Sc_ID,
							 Po_Ca_Name as To_Ca_Name, Po_Sc_Name as To_Sc_Name, Po_Privacy as To_Privacy,
							 Po_FileName as To_Filename, Po_Photo as To_Photo, Po_Thumb as To_Thumb, Po_Backcolor as To_Backcolor, Po_DatePosted as To_DatePosted, Po_Likes as To_Likes, 'Post' as Item_Type,
							bs_groups.Gr_Privacy, bs_members.Me_Status");
		$this->db->from('bs_posts');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_posts.Po_Gr_ID', 'INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_posts.Po_Gr_ID 
		AND bs_posts.Po_To_ID = bs_members.Me_To_ID 
		AND bs_posts.Po_ID = bs_members.Me_St_ID 
		AND bs_members.Me_Parent = 'subtopic'
		 AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
 		$this->db->where('Po_To_ID', 0);
		$this->db->where('bs_posts.Po_Gr_ID', $id);

		$this->db->get();
		$post_query = $this->db->last_query();

 			$this->db->distinct();
			$this->db->from("($topic_query UNION $post_query) as unionTable");


		if(!empty(	$subcat)){

			$this->db->group_start();
			if(is_numeric($subcat)){
				$this->db->where('To_Sc_ID', $subcat);
			}else{
 				$subcatogery = explode("_",$subcat);
			if(count($subcatogery) > 1){
				$stringSub = '';
				foreach ($subcatogery as $key => $row) {
					$stringSub .= $row.' ';

				}
				$this->db->or_where('To_Sc_Name',$stringSub);

			}else{
				$this->db->or_where('To_Sc_Name', $subcatogery[0]);

			}

			}


			$this->db->group_end();

		}
		else 	if (!empty($category)) {
			$this->db->group_start();
			if(is_numeric($category)){
				$this->db->where('To_Ca_ID', $category);
			}else{
				$this->db->or_where('To_Ca_Name', $category);
			}
 			$this->db->group_end();
		}

		if (!empty($search_text)) {
			$this->db->group_start();
			$this->db->like('To_Name', $search_text, 'BOTH');
			$this->db->or_like('To_Keywords', $search_text, 'BOTH');
			$this->db->group_end();
		}


		if(!empty($_POST['private']) && $_POST['private'] == 'yes'){
			$this->db->group_start();
			$this->db->where('To_Privacy', 'private');

			$this->db->group_end();

		}

		if(isset($_POST['popoular']) && $_POST['popoular'] == 'yes'){
	        $this->db->order_by('To_Likes DESC');
		}
 		$this->db->order_by('To_Pin DESC');
 		$this->db->order_by('To_DatePosted DESC');


		 if($all ==  'all'){


		 }else{
			 if(isset($_POST['favourite']) && $_POST['favourite'] == 'yes'){

			 }else{
				 if(isset($_POST['subgroups']) && $_POST['subgroups'] == 'yes'){

				 }else{
					 $this->db->limit(5);
				 }

			 }

		 }


		$query = $this->db->get();

		return $query->result_array();
	}

			function checkfavourite($postid){
				$this->db->select('bs_favorite.*')->from('bs_favorite');
				$this->db->where('Fa_PO_ID' ,$postid) ;
				$this->db->where('Fa_Us_ID' , $this->LoggedInUser);
				$this->db->where('Fa_Status' , 'active');
				$query = $this->db->get()->result_array();


				if(count($query) > 0){
					return 1;
				}else{
					return 0;
				}
			}
		function topic_format_privacy_favourite($data){
			if (!(empty($data))) {
				foreach ($data as $k => $row) {
					if(isset($_POST['subgroups']) && $_POST['subgroups'] == 'yes'){
						if ($row['Item_Type'] == 'Topic') {
							$data[$k]['favourite'] = 0;
						}else{
							unset($data[$k]);
//							$data[$k]['favourite']	=	$this->checkfavourite($row['To_ID']);
						}
						}else{
						if ($row['Item_Type'] == 'Topic') {
							$data[$k]['favourite'] = 0;
						}else{
							$data[$k]['favourite']	=	$this->checkfavourite($row['To_ID']);
						}
						}

					}
				}

				return $data;
		}
	private function topic_format_privacy($data) {

		if (!(empty($data))) {
			foreach ($data as $k=>$row) {
				$data[$k]['AskForPermission'] = false;
				$data[$k]['Searchable'] = true;
				$data[$k]['EncodedID'] = encode_id($row['To_ID']);

				if ($this->LoggedInUser != $row['To_Us_ID']) {
					if (empty($row['Me_Status'])) {
						if (strcasecmp($row['Gr_Privacy'], "public") != 0 ) {

							$count = $this->members_model->check_if_member_record_access_exist("group", $row['To_Gr_ID'], 0, 0, "active");

							if (empty($count)) {
								$data[$k]['Searchable'] = false;
								$data[$k]['AskForPermission'] = true;
							}
						}

						if (!$data[$k]['AskForPermission']) {
							if (strcasecmp($row['To_Privacy'], "private") == 0) {
//  							 $general_Count   = $this->members_model->check_if_member_record_access_exist("post", $row['To_Gr_ID'], '',  $row['To_ID'],   "active");
//
//  									if($general_Count > 0){

 										$data[$k]['AskForPermission'] = false;
//  									}else{
//  										 $data[$k]['AskForPermission'] = true;
//  									 }

							}

							if (strcasecmp($row['To_Privacy'], "secret") == 0) {
								$data[$k]['Searchable'] = false;
							}
						}

					} else if (strcasecmp($row['Me_Status'], "pending") == 0) {
						$data[$k]['AskForPermission'] = true;

						if (strcasecmp($row['To_Privacy'], "secret") == 0) {
							$data[$k]['Searchable'] = false;
						}

					}
				}
			}
		}

		return $data;
	}
}
