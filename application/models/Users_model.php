<?php
class Users_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	
	public function delete_row($id)
	{
		$this->db->where('Us_ID',$id);
		$this->db->delete('bs_users');
	}

	public function searchUsers($SearchText, $UserID)
	{
		$this->db->where_not_in('Us_ID', array($UserID));
		if (strlen($SearchText)>0) $this->db->like('Us_Name', $SearchText);
		else $this->db->like('Us_Name', 'XXX');
		$query = $this->db->get('bs_users');
		return $query->result_array();
	}

        public function getContect($id){

  					 $this->db->select('bs_contacts.* , bs_users.*')->from('bs_contacts');
  					 $this->db->join('bs_users' , 'bs_users.Us_ID = bs_contacts.Co_Contact_ID' );
            		$this->db->where('Co_Us_ID' ,$id);
            		$this->db->where('Co_Status' , 'confirmed');
            		$query = $this->db->get()->result_array();
				   return $query;

        }


		public function removeadmin($id)
		{

			$where = array(
				'Us_ID' => $id
			);
			$update = array(
				'Us_IsAdmin' => 0
			);
			$this->db->where($where);
				$res = $this->db->update('bs_users' , $update);
				return $res;
		}

	public function get_admins($id = false, $admin = false)
	{


		if ($id === false) {
			$this->db->select('bs_users.Us_ID ,bs_users.Us_Name ,bs_users.Us_FName ,bs_users.Us_LName ,
			 bs_users.Us_Alias ,bs_users.Us_Email ,bs_users.Us_Thumb ,bs_users.Us_Photo , bs_users.Us_Background
			  ,bs_users.Us_DateTime , bs_users.Us_IsAdmin, bs_users.Us_Status , bs_users.Us_Phone');
			$this->db->from('bs_users');
			$this->db->where('Us_IsAdmin' , 1);
			$this->db->where('Us_Status' , 'approved');
			$query = $this->db->get();
			$data = $query->result_array();

			foreach ($data as $key => $row) {
				$data[$key]['Us_Thumb'] = ($row['Us_Thumb'] == "") ? base_url() . 'img/nophoto.png' : $row['Us_Thumb'];
//				$data[$key]['Us_Thumb'] = check_user_profile_photo($row['Us_Thumb']);
				$data[$key]['Us_Photo'] = ($row['Us_Thumb'] == "") ? base_url() . 'img/nophoto.png' : $row['Us_Photo'];
//				$data[$key]['Us_Photo'] = check_user_profile_photo($row['Us_Photo']);
				$data[$key]['Us_Background'] = ($row['Us_Background'] == "") ? base_url() . 'img/nobg.png' : $row['Us_Background'];
//				$data[$key]['Us_Background'] = check_user_profile_background($row['Us_Background']);
			}

			return $data;
		}
	}
	public function get_Users($id = '' )
	{


		if ($id == '') {
			$this->db->select('
			bs_users.Us_ID,
			bs_users.Us_Name ,
			bs_users.Us_FName ,
			bs_users.Us_LName ,
			bs_users.Us_Alias ,
			bs_users.Us_Email ,
			bs_users.Us_Phone ,
			bs_users.Us_Company ,
			bs_users.Us_JobTitle ,
			bs_users.Us_NoOfEmployees ,
			bs_users.Us_Features ,
			bs_users.Us_Thumb ,
			bs_users.Us_Photo ,
			bs_users.Us_Backcolor,
			bs_users.Us_Background ,
			bs_users.Us_Status ,
			bs_users.Us_IsAdmin ,
			bs_users.Us_Mute_Limit ,
			bs_users.Us_ShowCreateGroupPopUp ,
			bs_users.Us_Coins ,
			bs_users.Us_DateTime ,
			bs_users.Us_Groups,
			bs_users.Us_Topics,
			 bs_users.Us_Posts,
		 	bs_users.Us_IsPrivate,
			');
			$this->db->from('bs_users');
			$this->db->where('Us_Status' , 'approved');
			$query = $this->db->get();

			$data = $query->result_array();


			foreach ($data as $key => $row) {
				$data[$key]['Us_Thumb'] = ($row['Us_Thumb'] == "") ? base_url() . 'img/nophoto.png' : $row['Us_Thumb'];
//				$data[$key]['Us_Thumb'] = check_user_profile_photo($row['Us_Thumb']);
				$data[$key]['Us_Photo'] = ($row['Us_Thumb'] == "") ? base_url() . 'img/nophoto.png' : $row['Us_Photo'];

				$data[$key]['Us_Background'] = ($row['Us_Background'] == "") ? base_url() . 'img/nobg.png' : $row['Us_Background'];

			}

			print_r($data);
				exit;
			return  $data;
		}
		$this->db->select('bs_users.Us_Name ,
			bs_users.Us_FName ,
			bs_users.Us_LName ,
			bs_users.Us_Alias ,
			bs_users.Us_Email ,
				bs_users.Us_ID,
			bs_users.Us_Phone ,
			bs_users.Us_Company ,
			bs_users.Us_JobTitle ,
			bs_users.Us_Backcolor,
			bs_users.Us_NoOfEmployees ,
			bs_users.Us_Features ,
			bs_users.Us_Thumb ,
			bs_users.Us_Photo ,
			bs_users.Us_Background ,
			bs_users.Us_Status ,
			bs_users.Us_IsAdmin ,
			bs_users.Us_Mute_Limit ,
			bs_users.Us_ShowCreateGroupPopUp ,
			bs_users.Us_Coins ,
				bs_users.Us_DateTime ,
			bs_users.Us_Groups,
			bs_users.Us_Topics,
			bs_users.Us_Posts,
		 	bs_users.Us_IsPrivate,
			');
		$this->db->from('bs_users');
		$this->db->where( array(
			'Us_Status'=> 'approved',
			'Us_ID' => $id));

		$query = $this->db->get();
		$data = $query->result_array();
 			foreach ($data as $key => $row) {
			$data[$key]['Us_Thumb'] = ($row['Us_Thumb'] == "") ? base_url() . 'img/nophoto.png' : $row['Us_Thumb'];
//			$data[$key]['Us_Thumb'] = check_user_profile_photo($row['Us_Thumb']);
			$data[$key]['Us_Photo'] = ($row['Us_Thumb'] == "") ? base_url() . 'img/nophoto.png' : $row['Us_Photo'];
//			$data[$key]['Us_Photo'] = check_user_profile_photo($row['Us_Photo']);
			$data[$key]['Us_Background'] = ($row['Us_Background'] == "") ? base_url() . 'img/nobg.png' : $row['Us_Background'];
//			$data[$key]['Us_Background'] = check_user_profile_background($row['Us_Background']);
			$data[$key]['Us_Name'] =  $row['Us_Name'];

		}

		$data = is_array($data[0]) ? $data[0] : $data;
		return $data;
	}

	public function select_Users($id)
	{

		$this->db->order_by('Us_Name', 'ASC');
		$this->db->where_not_in('Us_ID', array($id));
		$query = $this->db->get('bs_users');
		return $query->result_array();
	}
	
	
	public function get_UserByEmail($email)
	{	        
		$query = $this->db->get_where('bs_users', array('Us_Email' => $email));
		return $query->num_rows();
	}

	public function get_UserByPhone($phone)
	{	        
		$query = $this->db->get_where('bs_users', array('Us_Phone' => $phone));
		return $query->num_rows();
	}
	
	public function set_Register($FName, $LName, $Email, $Password, $Phone,
								 $BirthDate, $Company,
								 $JobTitle, $NoOfEmployees, $Features,
								 $privacy1, $privacy2, $privacy3)
	{

		$Name = $FName . ' ' . $LName;
	    
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
			'aggrement_one' => $privacy1,
		'aggrement_two' =>  $privacy2,
		'aggrement_three' => $privacy3,
		'Us_Name' => $Name,
		'Us_FName' => $FName,
		'Us_LName' => $LName,
		'Us_Email' => $Email,
		'Us_Password' => $Password,
		'Us_Phone' => $Phone,
		'Us_BirthDate' => $BirthDate,
		'Us_Company' => $Company,
		'Us_JobTitle' => $JobTitle,
		'Us_NoOfEmployees' => $NoOfEmployees,
		'Us_Features' => $Features,
		'Us_DateTime' => $now
	    );
	
		$this->db->insert('bs_users', $data);
		
		return $this->db->insert_id();
	}
			function get_UserSignInonlyEmail(){
				$this->db->select('bs_users.*');
				$this->db->from('bs_users');
				$this->db->where('Us_Email' , $this->input->post('txtEmail'));

				$query  = 	$this->db->get()->result_array();
				if(count($query) > 0){

					return $query[0];
				}else{

					return array();
				}
			}
	public function get_UserSignIn()
	{

//
//                 $hash = password_hash($plaintext_password,
//                           PASSWORD_DEFAULT);
//
		$this->db->select('bs_users.*');
		$this->db->from('bs_users');
		$this->db->where('Us_Email' , $this->input->post('txtEmail'));
// 				$this->db->where('Us_Status' ,  'approved');
		$query  = 	$this->db->get()->result_array();
		if(count($query) > 0){

				return $query[0];


		}else{

			return array();
		}

//
//
//


// 				if($hash == '$2y$10$xeEkbkG8c3uc7LGLveN9yOQ'){
//
// 				echo "match";
// 				}else{
// 				echo "not match";
// 				}

//
// 							$data = array(
// 							'Us_Email' => $this->input->post('txtEmail')
// 							);
//
// 							$update = array(
// 							'Us_Password' => $hash
// 							);
// 						  $this->db->where($data);
//                           $this->db->update('bs_users' , $update );


// 		$this->load->helper('url');
//  		$query = $this->db->get_where('bs_users', array('Us_Email' => $this->input->post('txtEmail'), 'Us_Password' => $this->input->post('txtPassword'), 'Us_Status' => 'approved'));
// 		return $query->row_array();
	}
	
	public function set_UpdateProfile($ID, $Name, $Alias, $Email, $Phone, $Company, $JobTitle, $NoOfEmployees, $Features, $isPrivate)
	{
	    
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Us_Name' => $Name,
		'Us_Alias' => $Alias,
		'Us_Email' => $Email,
		'Us_Phone' => $Phone,
		'Us_Company' => $Company,
		'Us_JobTitle' => $JobTitle,
		'Us_NoOfEmployees' => $NoOfEmployees,
		'Us_Features' => $Features,
		'Us_IsPrivate' => $isPrivate
	    );
	    
		$this->db->where('Us_ID', $ID);
	    return $this->db->update('bs_users', $data);
	}
	
	public function set_UpdatePassword($ID, $Password)
	{
	    
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Us_Password' => $Password
	    );
	    
		$this->db->where('Us_ID', $ID);
	    return $this->db->update('bs_users', $data);
	}
	
	public function set_UpdatePhoto($ID, $Photo, $Thumb)
	{
	    
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');
	
	    $data = array(
		'Us_Photo' => $Photo,
		'Us_Thumb' => $Thumb
	    );
	
		$this->db->where('Us_ID', $ID);
	    return $this->db->update('bs_users', $data);
	}
	
	public function set_UpdateBackground($ID, $Background)
	{
	
	    $data = array(
		'Us_Background' => $Background,
		'Us_Backcolor' => ''
	    );
	
		$this->db->where('Us_ID', $ID);
	    return $this->db->update('bs_users', $data);
	}


	public function set_UpdateBackcolor($ID, $Backcolor)
	{
	    $data = array(
		'Us_Backcolor' => $Backcolor,
		'Us_Background' => ''
	    );

		$this->db->where('Us_ID', $ID);
	    return $this->db->update('bs_users', $data);
	}


	public function get_Member($id)
	{
		$query = $this->db->get_where('bs_members', array('Me_ID' => $id));

		return $query->row_array();
	}



	public function check_Members($Parent, $GroupID, $TopicID, $SubTopicID, $UserID)
	{
		$query = $this->db->get_where('bs_members', array('Me_Parent' => $Parent, 'Me_Gr_ID' => $GroupID, 'Me_To_ID' => $TopicID, 'Me_St_ID' => $SubTopicID, 'Me_Us_ID' => $UserID));
		if ($query->num_rows() == 0) return '';
		else return 'd-none';
	}

	public function get_Members($Parent, $ParentID )
	{
		$this->db->select('bs_members.*,bs_users.Us_Name,bs_users.Us_Photo , bs_users.Us_Alias , bs_users.Us_DateTime ');
		$this->db->from('bs_members');
		$this->db->where('Me_Parent', $Parent);
		if (strcmp($Parent,'group')==0)
		{
			$this->db->where('Me_Gr_ID', $ParentID);
// 			$this->db->where('Me_To_ID', 0);
// 			$this->db->where('Me_St_ID', 0);
		}
		else if (strcmp($Parent,'topic')==0)
		{
			$this->db->where('Me_To_ID', $ParentID);
// 			$this->db->where('Me_St_ID', 0);
		}
		else if (strcmp($Parent,'subtopic')==0)
		{			
			$this->db->where('Me_St_ID', $ParentID);
		}

		$this->db->join('bs_users', 'bs_users.Us_ID = bs_members.Me_Us_ID','left');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function insertMember($Parent, $GroupID, $TopicID, $SubTopicID, $UserID, $Role, $Status)
	{
		$query = $this->db->get_where('bs_members', array('Me_Parent' => $Parent, 'Me_Gr_ID' => $GroupID, 'Me_To_ID' => $TopicID, 'Me_St_ID' => $SubTopicID, 'Me_Us_ID' => $UserID));
        if($query->num_rows() == 0){

			$this->load->helper('url');
			$this->load->helper('date'); // load Helper for Date
			date_default_timezone_set("Asia/Manila");
			$now = date('Y-m-d H:i:s');

			$data = array(
			'Me_Parent' => $Parent,
			'Me_Gr_ID' => $GroupID,
			'Me_To_ID' => $TopicID,
			'Me_St_ID' => $SubTopicID,
			'Me_Us_ID' => $UserID,
			'Me_Role' => $Role,
			'Me_Status' => $Status,
			'Me_DatePOsted' => $now
			);

			$this->db->insert('bs_members', $data);
			return $this->db->insert_id();
		}

		return 0;
	}

	public function add_Admin($Parent, $GroupID, $TopicID, $SubTopicID, $UserID)
	{
		$primaryID = $this->insertMember($Parent, $GroupID, $TopicID, $SubTopicID, $UserID, 'admin', 'approved');
		$this->updateMembersCount('group', $GroupID, $TopicID, $SubTopicID);
		return $primaryID;
	}

	public function add_Member($Parent, $GroupID, $TopicID, $SubTopicID, $UserID)
	{
		$primaryID = $this->insertMember($Parent, $GroupID, $TopicID, $SubTopicID, $UserID, 'member', 'pending');		
		$this->updateMembersCount('group', $GroupID, $TopicID, $SubTopicID);		
		return $primaryID;
	}

	public function add_Invite($Parent, $GroupID, $TopicID, $SubTopicID, $UserID)
	{
		$primaryID = $this->insertMember($Parent, $GroupID, $TopicID, $SubTopicID, $UserID, 'member', 'invited');
		$this->updateMembersCount('group', $GroupID, $TopicID, $SubTopicID);
		return $primaryID;
	}

	public function remove_Members($Parent, $GroupID, $TopicID, $SubTopicID, $UserID)
	{
		$query = $this->db->get_where('bs_members', array('Me_Parent' => $Parent, 'Me_Gr_ID' => $GroupID, 'Me_To_ID' => $TopicID, 'Me_St_ID' => $SubTopicID, 'Me_Us_ID' => $UserID));
        if($query->num_rows() > 0)
		{
			$this->db->delete('bs_members', array('Me_Parent' => $Parent, 'Me_Gr_ID' => $GroupID, 'Me_To_ID' => $TopicID, 'Me_St_ID' => $SubTopicID, 'Me_Us_ID' => $UserID));
			$this->updateMembersCount('group', $GroupID, $TopicID, $SubTopicID);
			return 1;
		}

		return 0;
	}


	public function updateStatus($ID, $Status)
	{
		$data = array('Me_Status' => $Status);
		$this->db->where('Me_ID', $ID);
		$this->db->update('bs_members', $data);
	}

	public function updateRole($ID, $Role, $Status)
	{
		$data = array('Me_Role' => $Role, 'Me_Status' => $Status);
		$this->db->where('Me_ID', $ID);
		$this->db->update('bs_members', $data);
	}

	public function updateMembersCount($Parent, $GroupID, $TopicID, $SubTopicID)
	{
		$this->db->where('Me_Parent',$Parent);
		$this->db->where('Me_Gr_ID',$GroupID);
		$this->db->where('Me_To_ID',$TopicID);
		$this->db->where('Me_St_ID',$SubTopicID);
		$this->db->from('bs_members');
        $Count = $this->db->count_all_results();
		$this->db->flush_cache();

		if (strcmp($Parent,'group')==0)
		{
			$data = array('Gr_Members' => $Count);
			$this->db->where('Gr_ID', $GroupID);
			$this->db->update('bs_groups', $data);
		}
		else
		{
			$data = array('To_Members' => $Count);
			$this->db->where('To_ID', $TopicID);
			$this->db->update('bs_topics', $data);
		}


	}

	public function enable_disable_user_create_group_popup($userID, $disablePopUp) {
		$this->db->where('Us_ID', $userID)
				->update('bs_users', array('Us_ShowCreateGroupPopUp'=> $disablePopUp));

		return true;
	}

	public function count_user_created_groups($userID) {
		$this->db->select('COUNT(*) as created_count');
		$this->db->from('bs_groups');
		$this->db->where('Gr_Us_ID', $userID);
		$groups =  $this->db->get()->result();

		return $groups[0]->created_count;
	}

	function search_user_for_group_invite($group, $keyword) {

		$result = $this->get_user_for_group_invite($group, $keyword);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Us_Thumb = (strlen($row->Us_Thumb)==0) ? base_url()."img/nophoto.png" : $row->Us_Thumb;
				$result[$key]->Me_Status = ($row->Me_Gr_ID == $group) ? TRUE : FALSE;
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_user_for_group_invite($group, $keyword) {

			$this->db->select("m.Me_Gr_ID, u.Us_ID, u.Us_Name, u.Us_FName, u.Us_LName, u.Us_Email, u.Us_Company, u.Us_JobTitle, u.Us_photo, u.Us_Thumb");
			$this->db->from("bs_users u");
			$this->db->join("bs_members m", "u.Us_ID = m.Me_Us_ID", "LEFT");
			$this->db->where("u.Us_Status", "approved");
			$this->db->where("m.Me_Parent", 'group');
			$this->db->where("m.Me_Gr_ID", $group);
			$this->db->where("u.Us_ID != " . $this->session->userdata['logged_in']['bs_id']);

			$this->db->group_start();
			$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
			$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
			$this->db->or_where("u.Us_Email LIKE '%".$keyword."%'");
			$this->db->group_end();
			$this->db->group_by("u.Us_ID");

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		$this->db->select("m.Me_Gr_ID, u.Us_ID, u.Us_Name, u.Us_FName, u.Us_LName, u.Us_Email, u.Us_Company, u.Us_JobTitle, u.Us_photo, u.Us_Thumb");
		$this->db->from("bs_users u");
		$this->db->join("bs_members m", "u.Us_ID = m.Me_Us_ID", "LEFT");
		$this->db->where("u.Us_Status", "approved");
		$this->db->where("u.Us_ID != " . $this->session->userdata['logged_in']['bs_id']);
		$this->db->group_start();
		$this->db->where("u.Us_Name LIKE '%".$keyword."%'");
		$this->db->or_where("CONCAT(u.Us_FName, ' ', u.Us_LName) LIKE '%".$keyword."%'");
		$this->db->or_where("u.Us_Email LIKE '%".$keyword."%'");
		$this->db->group_end();
		$this->db->group_by("u.Us_ID");

		$query2 = $this->db->get();
		$data2 = $query2->result();

		$query2 = $this->db->query('SELECT FOUND_ROWS() as COUNT');

		$this->cnt += $query2->row()->COUNT;

		return array_merge($data, $data2);
	}

		function getuserfav($user_id , $post_id){

			$this->db->select('bs_favorite.*');
			$this->db->from('bs_favorite');
			$this->db->where('Fa_Status' , 'active');
			$this->db->where('Fa_Us_ID' , $user_id);
			$this->db->where('Fa_PO_ID' , $post_id);
			$query =	$this->db->get()->result_array();
			return  count($query);

		}
		function getuserLike($user_id , $post_id){

				$this->db->select('bs_post_like.*');
				$this->db->from('bs_post_like');
				$this->db->where('PL_Status' , 'active');
					$this->db->where('PL_Us_ID' , $user_id);
					$this->db->where('PL_PO_ID' , $post_id);
				$query =	$this->db->get()->result_array();
				return  count($query);


		}
		function activate_user_account($userID) {
			$data = array('Us_Status' => 'approved');
			$this->db->where('Us_ID', $userID);
			$this->db->update('bs_users', $data);

			$data = $this->get_Users($userID);

			return $data;
		}
}
