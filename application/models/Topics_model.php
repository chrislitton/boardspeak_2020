<?php
class Topics_model extends CI_Model {

	private $Tablename = 'bs_topics';
	private $LoggedInUser;

	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
		$this->load->model('members_model');

		$this->LoggedInUser = isset($this->session->userdata['logged_in']['bs_id']) ? $this->session->userdata['logged_in']['bs_id'] : '';
	}

	public function get_Record($id)
	{
		$query = $this->db->get_where('bs_topics', array('To_ID' => $id));
		return $query->row_array();
	}
		public	function topics_member($id){

 				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_To_ID' , $id);
// 				$this->db->where('Me_Parent' , 'topic');
				$this->db->where('Me_Status' , 'active');
				$query =	$this->db->get()->result_array();
				return count($query) ;

 		}
        
	public function get_Topic($id = false)
	{
		if ($id === false)
		{
			$query = $this->db->order_by('To_ID', 'DESC');
			$query = $this->db->get('bs_topics');
			
			return $query->result_array();
		}
		
		
		$this->db->select('bs_topics.*,IFNULL(bs_category.Ca_Name, bs_topics.To_Ca_Name) as catName,IFNULL(bs_category.Ca_ID, bs_topics.To_Ca_ID) as catId,bs_users.Us_Name,bs_users.Us_Photo,bs_groups.Gr_ID,bs_groups.Gr_Name');
		$this->db->from($this->Tablename);
		$this->db->where('To_ID', $id);		
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_topics.To_Us_ID','left');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID','left');
		$this->db->join('bs_category', 'bs_category.Ca_ID = bs_topics.To_Ca_ID','left');
		$query = $this->db->get();
		return $query->row_array();
	}



	public function get_TopicSubTopicByID($id)
	{
		$this->db->select('bs_topics.*,bs_users.Us_Name,bs_users.Us_Photo,bs_groups.Gr_ID,bs_groups.Gr_Name');
		$this->db->from('bs_topics');
		$this->db->where('To_ID', $id);
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_topics.To_Us_ID','left');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID','left');
		$query = $this->db->get();
		return $query->row_array();
	}

	public function get_SubTopic($id)
	{
		$this->db->select('sub.*,bs_users.Us_Name,bs_users.Us_Photo,bs_groups.Gr_Name, top.To_Name Topic');
		$this->db->from('bs_topics sub');
		$this->db->where('sub.To_ID', $id);
		$this->db->join('bs_users', 'bs_users.Us_ID = sub.To_Us_ID','left');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = sub.To_Gr_ID','left');		
		$query = $this->db->get();
		return $query->row_array();
	}



	public function get_SubTopics($id)
	{
		$this->db->select('bs_topics.*,bs_users.Us_Name,bs_users.Us_Photo,bs_groups.Gr_Name');
		$this->db->from('bs_topics');		
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_topics.To_Us_ID','left');
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID','left');
		$query = $this->db->get(); 
		return $query->result_array();
	}	
	
	public function get_GroupTopics($id, $category = null, $search_text = null) {
		$this->db->distinct();
		$this->db->select("To_ID, To_Slugs, To_Us_ID, To_Gr_ID, To_Name, To_Description, 
							(CASE 
								WHEN bs_topics.To_Ca_ID = 0 THEN bs_topics.To_Ca_Name
								ELSE bs_topics.To_Ca_ID
							END) as To_Ca_ID, 
							(CASE 
								WHEN bs_topics.To_Sc_ID = 0 THEN bs_topics.To_Sc_Name
								ELSE bs_topics.To_Sc_ID
							END) as To_Sc_ID, 
							To_Ca_Name, To_Sc_Name, To_Privacy, To_Filename, To_Photo, To_Thumb,
							To_Backcolor, To_Members, To_SubTopics, To_DatePosted, To_Likes,
							bs_groups.Gr_Privacy, bs_members.Me_Status");
		$this->db->from($this->Tablename);	
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID', 'INNER');	
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_topics.To_Gr_ID AND bs_topics.To_ID = bs_members.Me_To_ID AND bs_members.Me_Parent = 'topic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('To_Gr_ID', $id);

		if (!empty($category)) {
			$this->db->group_start();
			$this->db->where('To_Ca_ID', $category);
			$this->db->or_where('To_Ca_Name', $category);
			$this->db->group_end();
		}

		if (!empty($search_text)) {
			$this->db->like('To_Name', $search_text, 'BOTH');
		}

		$data = $this->db->get()->result_array(); 

		$data = $this->format_data($data);

		return $data;
	}

	public function get_GroupTopicsCategory($id,$catID)
	{
		$this->db->distinct();
		$this->db->select('bs_topics.*, bs_groups.Gr_Privacy, bs_members.Me_Status');
		$this->db->from('bs_topics');		
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID', 'INNER');	
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_topics.To_Gr_ID AND bs_topics.To_ID = bs_members.Me_To_ID AND bs_members.Me_Parent = 'topic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('To_Gr_ID', $id);		
		$this->db->where('To_Ca_ID', $catID);

		$data = $this->db->get()->result_array(); 

		$data = $this->format_data($data);

		return $data;
	}

	public function searchGroupTopics($id,$catId=null,$keyword)
	{
		$this->db->select('bs_topics.*');
		$this->db->from('bs_topics');		
		$this->db->like('To_Name', $keyword);
		$this->db->where('To_Gr_ID', $id);	
		if(is_numeric($catId)){	
			$this->db->where('To_Ca_ID', $catId);	
		}

		$query = $this->db->get(); 
		return $query->result_array();
	}

	

	public function get_TopicsByType($Type=false, $SearchText = false)
	{

		$this->db->distinct();
		$this->db->select('bs_topics.*,bs_groups.Gr_Privacy, bs_members.Me_Status ');
		$this->db->from($this->Tablename , ' as bs_topics ');
		$this->db->join('bs_groups as bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID','INNER');
		$this->db->join('bs_members', "bs_members.Me_Gr_ID = bs_topics.To_Gr_ID AND bs_topics.To_ID = bs_members.Me_To_ID AND bs_members.Me_Parent = 'topic' AND bs_members.Me_Us_ID = ". $this->LoggedInUser, 'LEFT');
		$this->db->where('bs_groups.Gr_Privacy != ', 'secret');
		$this->db->where('bs_topics.To_Privacy != ', 'secret');
		$this->db->order_by('To_ID', 'DESC');	

		if (strlen($Type)>0) $this->db->where('To_Privacy', $Type);
		if ($SearchText==true) $this->db->like('To_Name', $SearchText);

		$data = $this->db->get()->result_array();

		$data = $this->format_data($data);

		return $data;
	}

	
	public function get_UserTopics($id)
	{
		
		
		$this->db->select('bs_topics.*,bs_groups.Gr_Name');		
		$this->db->from('bs_topics');		
		$this->db->where('To_Us_ID', $id);		
		$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_topics.To_Gr_ID','left');
		$this->db->order_by('To_ID', 'DESC');
		$query = $this->db->get(); 
		return $query->result_array();

	}

	function show_topics($userID, $data) {

 
		$result = $this->get_explore_topic(  $data);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Raw_ID = $row->To_ID;
				$result[$key]->To_ID = encode_id($row->To_ID);
				$result[$key]->To_URL = base_url() . 'account/view/topic/' . $row->To_Slugs;
				$result[$key]->To_Thumb = base_url() . (str_replace(base_url(), '', ((strlen($row->To_Thumb)==0) ? "img/topic.png" : $row->To_Thumb)));
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}

	function get_topics($userID, $data) {
		$this->db->select("SQL_CALC_FOUND_ROWS t.To_ID, t.*, g.Gr_Name", FALSE);
		$this->db->from("bs_topics t");
		$this->db->join('bs_groups g', 'g.Gr_ID = t.To_Gr_ID','left');

		if ($data['keyword']) {
			$this->db->where("t.To_Name LIKE '%".$data['keyword']."%'");
		}
		if ($data['privacy']) {
			$this->db->where("t.To_Privacy", $data['privacy']);
		}
		if ($data['alpha']) {
			$this->db->order_by("t.To_Name", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("t.To_DatePosted", "DESC");
		}
		if ($data['popular']) {
			$this->db->order_by("t.To_Likes", "DESC");
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}
	
	function show_user_created_topics($userID, $data) {

		$result = $this->get_user_created_topics($userID, $data);

		if (!empty($result)) {
			foreach ($result as $key => $row) {
				$result[$key]->Raw_ID = $row->To_ID;
				$result[$key]->To_ID = encode_id($row->To_ID);
				$result[$key]->To_URL = base_url() . 'account/view/topic/' . $row->To_Slugs;
				$result[$key]->To_Thumb = base_url() . (str_replace(base_url(), '', ((strlen($row->To_Thumb)==0) ? "img/topic.png" : $row->To_Thumb)));
			}
		}

		$grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
	}


			
			function get_explore_topic($data){
					$this->db->select("SQL_CALC_FOUND_ROWS t.To_ID, t.*, g.Gr_Name ,
					 u.*,
					 g.Gr_Privacy", FALSE);
            		$this->db->from("bs_topics t");
            		$this->db->join('bs_groups g', 'g.Gr_ID = t.To_Gr_ID','left');
            		$this->db->join('bs_users u' , 'u.Us_ID = t.To_Us_ID');
            		$this->db->where("g.Gr_Privacy", 'public');
					$this->db->where("t.To_Privacy", 'public');
					if ($data['keyword']) {
						$this->db->where("t.To_Name LIKE '%".$data['keyword']."%'");
						$this->db->or_where("u.Us_Name LIKE '%".$data['keyword']."%'");

					}
// 					if ($data['privacy']) {
// 						$this->db->where("t.To_Privacy", $data['privacy']);
// 					}
					if ($data['alpha']) {
						$this->db->order_by("t.To_Name", $data['alpha']);
					}
					if ($data['latest']) {
						$this->db->order_by("t.To_DatePosted", "DESC");
					}
					if ($data['popular']) {
						$this->db->order_by("t.To_Likes", "DESC");
					}

					$query = $this->db->get();
					$data = $query->result();
					$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
					$this->cnt = $query->row()->COUNT;

					return $data;
			
			
			
			}
	function get_user_created_topics($userID, $data) {


		$this->db->select("SQL_CALC_FOUND_ROWS t.To_ID, t.*, u.* ,  g.Gr_Name", FALSE);
		$this->db->from("bs_topics t");
		$this->db->join('bs_users u' , 'u.Us_ID = t.To_Us_ID');
		$this->db->join('bs_groups g', 'g.Gr_ID = t.To_Gr_ID','left');

		$this->db->where("t.To_Us_ID", $userID);

		if ($data['keyword']) {
			$this->db->where("t.To_Name LIKE '%".$data['keyword']."%'");

		}
		if ($data['privacy']) {
			$this->db->where("t.To_Privacy", $data['privacy']);
		}
		if ($data['alpha']) {
			$this->db->order_by("t.To_Name", $data['alpha']);
		}
		if ($data['latest']) {
			$this->db->order_by("t.To_DatePosted", "DESC");
		}
		if ($data['popular']) {
			$this->db->order_by("t.To_Likes", "DESC");
		}

		$query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}

	public function get_FollowedTopics($id)
	{
        $this->db->limit(10);
		$this->db->select('bs_topics.*');
		$this->db->from('bs_topics');
		$this->db->join('bs_members', 'bs_members.Me_To_ID = bs_topics.To_ID and bs_members.Me_St_ID=0','left');
		$this->db->where('bs_members.Me_Parent', 'topic');
		$this->db->where('bs_members.Me_Us_ID', $id);
		$this->db->where_in('bs_members.Me_Role', array('member','moderator'));
		$this->db->where('bs_members.Me_Status', 'approved');
		$query = $this->db->get();
		return $query->result_array();
	}

	
	
	public function Insert($UserID, $GroupID, $Name, $Description, $CatID, $Category, $SubID, $SubCategory, $Privacy, $keywords='' , $topic_slug)
	{
	    $this->load->helper('url');	
			
	    $this->load->helper('date'); // load Helper for Date 
		date_default_timezone_set("Asia/Manila");	    
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'To_Us_ID' => $UserID,
		'To_Gr_ID' => $GroupID,
		'To_Name' => $Name,
		'To_Description' => $Description,
		'To_Ca_ID' => $CatID,
		'To_Ca_Name' => $Category,
		'To_Sc_ID' => $SubID,
		'To_Sc_Name' => $SubCategory,
		'To_Privacy' => $Privacy,
		'To_DatePosted' => $now,
		'To_Keywords' => $keywords,
			'To_Slugs' => $topic_slug
	    );
	
	    $this->db->insert('bs_topics', $data);
		$primaryID = $this->db->insert_id();

		$this->updateUserTopicsCount($UserID);
		$this->updateGroupTopicsCount($GroupID);

		return $primaryID;
	}

	public function updateUserTopicsCount($UserID)
	{
		$this->db->where('To_Us_ID',$UserID);
		$this->db->from('bs_topics');
        $Count = $this->db->count_all_results();
		$this->db->flush_cache();

		$data = array('Us_Topics' => $Count);
		$this->db->where('Us_ID', $UserID);
	    $this->db->update('bs_users', $data);
	}

	public function updateGroupTopicsCount($GroupID)
	{
		$this->db->where('To_Gr_ID',$GroupID);
		$this->db->from('bs_topics');
        $Count = $this->db->count_all_results();
		$this->db->flush_cache();

		$data = array('Gr_Topics' => $Count);
		$this->db->where('Gr_ID', $GroupID);
	    $this->db->update('bs_groups', $data);
	}

	
	
	public function Update($TopicID, $GroupID, $Name, $Description, $CatID, $Category, $SubID, $SubCategory, $Privacy, $keywords='')
	{
	    $data = array(
			'To_Gr_ID' => $GroupID,
			'To_Name' => $Name,
			'To_Description' => $Description,
			'To_Ca_ID' => $CatID,
			'To_Ca_Name' => $Category,
			'To_Sc_ID' => $SubID,
			'To_Sc_Name' => $SubCategory,
			'To_Privacy' => $Privacy,
			'To_Keywords' => $keywords
		);	
	    
		$this->db->where('To_ID', $TopicID);
		return $this->db->update('bs_topics', $data);
	}
	
	public function set_UpdateBackcolor($ID, $Backcolor)
	{
	    $data = array(
		'To_Backcolor' => $Backcolor,
		'To_FileName' => '',
		'To_Photo' => '',
		'To_Thumb' => ''
	    );

		$this->db->where('To_ID', $ID);
	    return $this->db->update('bs_topics', $data);
	}


	
	public function set_changephoto($TopicID, $FileName, $Photo, $Thumb)
	{
		$data = array(
			'To_FileName' => $FileName,
			'To_Photo' => $Photo,
			'To_Thumb' => $Thumb,
			'To_Backcolor' => ''
		);
	
		$this->db->where('To_ID', $TopicID);
		return $this->db->update('bs_topics', $data);
	}

	public function delete_topic($id)
	{
		$this->db->delete('bs_comments', array('Co_To_ID' => $id));
		$this->db->delete('bs_members', array('Me_To_ID' => $id));
		$this->db->delete('bs_posts', array('Po_To_ID' => $id));		
		$this->db->delete('bs_topics', array('To_ID' => $id));
	}

	public function delete_subtopic($id)
	{
		$this->db->delete('bs_comments', array('Co_St_ID' => $id));
		$this->db->delete('bs_members', array('Me_St_ID' => $id));		
		$this->db->delete('bs_topics', array('To_ID' => $id));
	}
	
	private function format_data($data) {
		if (!(empty($data))) {
			foreach ($data as $k=>$row) {
				$data[$k]['AskForPermission'] = false;
				$data[$k]['Searchable'] = true;
				$data[$k]['EncodedID'] = encode_id($row['To_ID']);

				if (empty($row['Me_Status'])) {
					if (strcasecmp($row['Gr_Privacy'], "public") != 0 ) {
						$count = $this->members_model->check_if_member_record_access_exist("group", $row['To_Gr_ID'], 0, 0, "approved");

						if (empty($count)) {
							$data[$k]['Searchable'] = false;
							$data[$k]['AskForPermission'] = true;
						}
					} 

					if (!$data[$k]['AskForPermission']) {
						if (strcasecmp($row['To_Privacy'], "private") == 0) {
							$data[$k]['AskForPermission'] = true;
						}

						if (strcasecmp($row['To_Privacy'], "secret") == 0) {
							$data[$k]['Searchable'] = false;
						}
					}

				} else if (strcasecmp($row['Me_Status'], "pending") == 0) {
					$data[$k]['AskForPermission'] = true;

					if (strcasecmp($row['To_Privacy'], "secret") == 0) {
						$data[$k]['Searchable'] = false;
					}

				}
			}
		}

		return $data;
	}
	
	public function get_user_topic_per_group($GroupID, $Role=NULL)
	{
		$memberData = $this->members_model->get_member_records('topic', $Role);

		$this->db->select('bs_topics.*');
		$this->db->from($this->Tablename);

		//filter
		$this->db->group_start();
		$this->db->where('bs_topics.To_Privacy' , 'public');

		$this->db->or_group_start();
		$this->db->where('bs_topics.To_Privacy !=' , 'public');
		$this->db->where('To_Us_ID', $this->LoggedInUser);
		$this->db->group_end();

		if (!empty($memberData)) {
			$this->db->or_group_start();
			$this->db->where('bs_topics.To_Privacy !=' , 'public');
			$this->db->where_in('To_ID', $memberData);
			$this->db->group_end();
		}
		$this->db->group_end();

		$this->db->where('To_Gr_ID', $GroupID);

		$this->db->order_by('bs_topics.To_Name', 'ASC');
		return $this->db->get()->result_array();
	}
}
