<?php
class Policy_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}



    public function get_Policy($id)
	{
		$query = $this->db->get_where('bs_policy', array('Po_ID' => $id));
		return $query->row_array();
	}

	public function get_GroupPolicy($id)
	{
		$query = $this->db->get_where('bs_policy', array('Po_Gr_ID' => $id, 'Po_To_ID' => 0, 'Po_St_ID' => 0));
		return $query->result_array();
	}

	public function get_TopicPolicy($id)
	{
		$query = $this->db->get_where('bs_policy', array('Po_To_ID' => $id, 'Po_St_ID' => 0));
		return $query->result_array();
	}

	public function get_SubTopicPolicy($id)
	{
		$query = $this->db->get_where('bs_policy', array('Po_St_ID' => $id));
		return $query->result_array();
	}

	public function Insert($GroupID, $TopicID, $SubTopicID, $Title, $Description)
	{
	    $this->load->helper('url');

	    $this->load->helper('date'); // load Helper for Date
		date_default_timezone_set("Asia/Manila");
		$now = date('Y-m-d H:i:s');

	    $data = array(
		'Po_Gr_ID' => $GroupID,
		'Po_To_ID' => $TopicID,
		'Po_St_ID' => $SubTopicID,
		'Po_Title' => $Title,
		'Po_Description' => $Description,
		'Po_DatePosted' => $now
	    );

	    $this->db->insert('bs_policy', $data);
		return $this->db->insert_id();
	}

	public function delete_row($id)
	{
		$this->db->delete('bs_policy', array('Po_ID' => $id));
	}
}
