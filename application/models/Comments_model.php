<?php
class Comments_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}

   public function get_Comments($PostID, $commentID = '')
	{
		$this->db->select('bs_comments.*,bs_users.Us_Name,bs_users.Us_Alias,bs_users.Us_Photo');
		$this->db->from('bs_comments');
		$this->db->where('Co_Po_ID', $PostID);
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_comments.Co_Us_ID','left');
		$this->db->join('bs_posts', 'bs_posts.Po_ID = bs_comments.Co_Po_ID','left');
		$this->db->order_by('bs_comments.Co_Pinned DESC');
		$this->db->order_by('bs_comments.Co_Parent_ID ASC');
		$this->db->order_by('bs_comments.Co_DatePosted DESC');

		if (!empty($commentID)) {
			$this->db->where('Co_ID', $commentID);
		}

		$query = $this->db->get();
		return $query->result_array();
	}


	public function Insert($data)
	{
		$this->db->insert('bs_comments', $data);
		return $this->db->insert_id();
	}

   public function get_comments_by_search($PostID, $searchTxt)
	{
		$this->db->select('bs_comments.*,bs_users.Us_Name,bs_users.Us_Alias,bs_users.Us_Photo');
		$this->db->from('bs_comments');
		$this->db->where('Co_Po_ID', $PostID);
		$this->db->order_by('bs_comments.Co_Pinned DESC');
		$this->db->join('bs_users', 'bs_users.Us_ID = bs_comments.Co_Us_ID','left');
		$this->db->join('bs_posts', 'bs_posts.Po_ID = bs_comments.Co_Po_ID','left');

		if (!empty($searchTxt)) {
			$this->db->like('Co_Message', $searchTxt, 'BOTH');
		}

		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_comment_likes($id) {
		$this->db->select('bs_comment_like.*');
		$this->db->from('bs_comment_like');
		$this->db->where('Lk_Co_ID', $id);
		
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_user_comment_like($id, $user_id) {
		$this->db->select('bs_comment_like.*');
		$this->db->from('bs_comment_like');
		$this->db->where('Lk_Co_ID', $id);
		$this->db->where('Lk_Us_ID', $user_id);
		
		$query = $this->db->get();
		return $query->result_array();
	}

	public function insert_like($comment_id) {
		$data = array(
			'Lk_Co_ID' => $comment_id,
			'Lk_Us_ID' => $this->session->userdata['logged_in']['bs_id']
		);

		$this->db->insert('bs_comment_like', $data);
		return $this->db->insert_id();
	}

	public function delete_like($comment_id) {
		$this->db->delete('bs_comment_like', array('Lk_Co_ID' => $comment_id, 'Lk_Us_ID'=>$this->session->userdata['logged_in']['bs_id']));
		return true;
	}

	public function update_comment($id, $data) {

		$this->db->where('Co_ID', $id);
		$this->db->update('bs_comments', $data);

		return true;
	}
	
	public function delete_comment($id) {
		$this->db->delete('bs_comment_like', array('Lk_Co_ID' => $id));
		$this->db->delete('bs_comments', array('Co_Parent_ID' => $id));
		$this->db->delete('bs_comments', array('Co_ID' => $id));
		
		return true;
	}
	
	public function insert_attachment($data) {
		$this->db->insert('bs_comment_attachment', $data);
		
		return true;
	}

	public function get_attachments($comment_id, $ids=array()) {
		$this->db->select('bs_comment_attachment.*');
		$this->db->from('bs_comment_attachment');
		$this->db->where('At_Co_ID', $comment_id);

		if (!empty($ids)) {
			$this->db->where_not_in('At_ID', $ids);
		}
		
		$query = $this->db->get();
		return $query->result_array();
	}

	public function show_comment_attachment($id) {
		$attachments = $this->get_attachments($id);

		$data = array();

		foreach ($attachments as $key => $row) {
			$data[$key]['id'] = $row['At_ID'];
			$data[$key]['file'] = base_url() . 'uploads/post_attachment/' . $row['At_Co_ID'] . '/' . $row['At_Filename'];
			$data[$key]['mime_type'] = $row['At_File_Type'];
		}

		return $data;
	}

	public function delete_attachment($id) {
		$this->db->delete('bs_comment_attachment', array('At_ID' => $id));
	}

	public function insert_comment_tag($data) {
		$this->db->insert('bs_comment_tag', $data);
		return true;
	}

	public function get_comment_tag($comment_id) {
		$this->db->select('*');
		$this->db->from('bs_comment_tag AS bct');
		$this->db->join('bs_users AS bu', 'bct.Ta_Us_ID = bu.Us_ID', 'INNER');
		$this->db->where('Ta_Co_ID', $comment_id);
		
		$query = $this->db->get();
		return $query->result_array();
	}

	public function delete_tags($id) {
		$this->db->delete('bs_comment_tag', array('Ta_Co_ID' => $id));
	}
}
