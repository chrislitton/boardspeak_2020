<?php
class Role_model extends CI_Model {

	private $Tablename = 'bs_roles';
	private $LoggedInUser;

	public function __construct() {
		$this->load->database();
		$this->load->library('session');

		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->LoggedInUser = $this->session->userdata['logged_in']['bs_id'];
		}
    }

    function show_roles_for_dropdown($data) {
        $result = $this->get_roles($data);

        if (!empty($result)) {
			foreach ($result as $key => $row) {
                $result[$key]->Rl_Label = "Make " . $row->Rl_Type;
            }
        }

        $grid = array(
			'totalRecords' => $this->cnt,
			'data' => $result
		);

		return $grid;
    }

    function get_roles($data) {
        $this->db->select("SQL_CALC_FOUND_ROWS r.*", FALSE);
        $this->db->from($this->Tablename . " r");

        if (isset($data['except'])) {
            $this->db->where("r.Rl_Code != '".$data['except']."'");
        }
        
        $query = $this->db->get();
		$data = $query->result();

		$query = $this->db->query('SELECT FOUND_ROWS() as COUNT');
		$this->cnt = $query->row()->COUNT;

		return $data;
	}
}
?>