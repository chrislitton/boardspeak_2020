<?php
class Buttons_model extends CI_Model {

	public function __construct()
	{
			$this->load->database();
	}

	public function get_record($id='')
	{
		if (!empty($id))
			$this->db->where('Bn_ID', $id);

		$this->db->order_by('Bn_Caption', 'ASC');
		$query = $this->db->get('bs_buttons');
		return $query->result_array();
	}
}