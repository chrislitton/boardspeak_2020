<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['u/(:any)'] = 'user/profile/$1';
$route['g/(:any)/(:num)/(:any)'] = 'group/info/$1/$2/$3';
$route['inviteLink/(:any)/(:any)/(:any)'] = 'group/info/$1/$2/$3';
$route['invitepostLink/(:any)/(:any)/(:any)/(:any)'] = 'group/postinfo/$1/$2/$3/$4';
$route['invitetopicLink/(:any)/(:any)/(:any)'] = 'group/topicinfo/$1/$2/$3';
$route['verify/(:any)'] = 'user/email_verification/$1';

$route['passwordUpdate/(:any)'] = 'user/reset_email_verification/$1';
$route['users/success'] = 'users/success';
$route['users/create'] = 'users/create';
$route['users/(:any)'] = 'users/view/$1';
$route['users'] = 'users';


$route['admin/users'] = 'admin/users';
$route['admin'] = 'admin';



$route['account/setsubtopicphoto/(:num)/(:any)'] = 'account/setsubtopicphoto/$1/$2';
$route['account/settopicphoto/(:num)/(:any)'] = 'account/settopicphoto/$1/$2';
$route['account/setgroupphoto/(:num)/(:any)'] = 'account/setgroupphoto/$1/$2';
$route['account/setbackground/(:num)/(:any)'] = 'account/setbackground/$1/$2';

$route['account/createsubtopic/(:any)'] = 'account/createsubtopic/$1';
$route['account/subtopic/(:any)'] = 'account/subtopic/$1';
$route['account/task/(:any)'] = 'account/task/$1';
$route['account/event/(:any)'] = 'account/event/$1';
$route['account/post/(:any)'] = 'account/post/$1';
$route['account/topic/(:any)'] = 'account/topic/$1';

$route['account/group/(:any)'] = 'account/group/$1';
$route['account/createtopic/(:any)'] = 'account/createtopic/$1';
$route['account'] = 'account';

$route['home'] = 'home/index';

$route['default_controller'] = 'pages/view';
$route['(:any)'] = 'pages/view/$1';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


