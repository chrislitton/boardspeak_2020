<section class="user-section entry-section" id="entryform">
<div class="container">
    <div class="row">                  
        <div class="col-lg-12">                          
            <?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
            <?php if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>


                        <?php
                            echo validation_errors();
                            echo form_open('account/settings/changepassword');
                            echo form_hidden('UserID', $users_item['Us_ID']);
                        ?>


                        <div class="control-group">
                            <div class="admin-form-group controls mb-0 pb-1">
                                <label>Old Password</label>
                                <input class="form-control" id="txtOldPassword" name="txtOldPassword" type="password" placeholder="Old Password" required="required" data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="admin-form-group controls mb-0 pb-1">
                                <label>New Password</label>
                                <input class="form-control" id="txtNewPassword" name="txtNewPassword" type="password" placeholder="New Password" required="required" data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="admin-form-group controls mb-0 pb-1">
                                <label>Re-Type Password</label>
                                <input class="form-control" id="txtRetypePassword" name="txtRetypePassword" type="password" placeholder="Re-type Password" required="required" data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <br>
                        <div id="success"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-xl btn-primary" id="sendMessageButton">Save Password</button>
                        </div>
                    </form>


        </div>
    </div>
</div>
</section>