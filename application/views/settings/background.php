<section class="user-section entry-section" id="entryform">
<div class="container">
    <div class="row">                  
        <div class="col-lg-12">                          
            <?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
            <?php if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>



                            <?php
                                echo validation_errors();
                                echo form_open_multipart('account/settings/changebackground');
                            ?>

                            <div class="control-group d-none">
                                <div class="admin-form-group controls mb-0 pb-1">
                                <input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="admin-form-group controls mb-0 pb-1">
                                    <label>Choose Background</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                                    <span class="input-group-text rounded-0 btn btn-default btn-file">Upload Photo
                                                            <input type="file" id="flePhoto" name="userfile">
                                                        </span>
                                        </div>
                                        <input type="text" class="form-control" readonly>
                                        <img id='img-upload'/>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div id="success"></div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Save Background</button>
                            </div>
                        </form>


        </div>
    </div>
</div>
</section>