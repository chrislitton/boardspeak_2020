<link rel="stylesheet" href="//cdn.tutorialjinni.com/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdn.tutorialjinni.com/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://mojoaxel.github.io/bootstrap-select-country/dist/css/bootstrap-select-country.min.css" />

<section class="user-section entry-section" id="entryform">
<div class="container">
    <div class="row">                  
        <div class="col-lg-12">                          
            <?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
            <?php if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>
                                            
            <?php
                echo validation_errors();
                echo form_open('account/settings/saveprofile');
                echo form_hidden('UserID', $users_item['Us_ID']);
            ?>

                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Name</label>
                        <input class="form-control" id="txtName" name="txtName" type="text" value="<?php echo $users_item['Us_Name'];?>" placeholder="Name" required="required" data-validation-required-message="Please enter your name.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Alias / Username</label>
                        <input class="form-control" id="txtAlias" name="txtAlias" type="text" value="<?php echo $users_item['Us_Alias'];?>" placeholder="This is optional for anonimity or your name is public" required="required" data-validation-required-message="Please enter your phone number.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Email Address</label>
                        <input class="form-control" id="email" name="txtEmail" type="email" value="<?php echo $users_item['Us_Email'];?>" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Phone Number</label>
                        <input class="form-control" id="phone" name="txtPhone" type="tel" value="<?php echo $users_item['Us_Phone'];?>" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Privacy</label>
                        <div class="custom-control custom-radio pb-2">
                            <input type="radio" class="custom-control-input" id="radPublic" name="txtPrivacy" value=0 <?php echo ($users_item['Us_IsPrivate'] == 0) ? 'checked' : '';?>>
                            <label class="custom-control-label" for="radPublic">Public</label>
                        </div>
                        <div class="custom-control custom-radio pb-2">
                            <input type="radio" class="custom-control-input" id="radPrivate" name="txtPrivacy" value=1  <?php echo ($users_item['Us_IsPrivate'] == 1) ? 'checked' : '';?>>
                            <label class="custom-control-label" for="radPrivate">Private</label>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Company</label>
                        <input class="form-control" id="company" name="txtCompany" type="text" value="<?php echo $users_item['Us_Company'];?>" placeholder="Company" required="required" data-validation-required-message="Please enter your company.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Job Title</label>
                        <input class="form-control" id="jobtitle" name="txtJobTitle" type="text" value="<?php echo $users_item['Us_JobTitle'];?>" placeholder="Job Title" required="required" data-validation-required-message="Please enter your job title.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
						<label>Country</label>
						<select class="selectpicker countrypicker" data-flag="true"></select>

						<!--<input class="form-control" id="employee" name="txtNoOfEmployees" type="text" value="--><?php //echo $users_item['Us_NoOfEmployees'];?><!--" placeholder="Number of Employee" required="required" data-validation-required-message="Please enter number of employee.">-->
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="admin-form-group controls mb-0 pb-1">
                        <label>Why did you use BoardSpeak?</label>
                        <textarea class="form-control" id="features" name="txtFeatures" rows="3" placeholder="Features you need in order to have efficient team communication" required="required" data-validation-required-message="Please enter a message."><?php echo $users_item['Us_Features'];?></textarea>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Save Profile</button>
                </div>
            </form>


        </div>
    </div>
</div>
</section>




