<section class="user-section entry-section" id="entryform">
<div class="container">
    <div class="row">                  
        <div class="col-lg-12">                          
            <?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
            <?php if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>
                    
                    
                    <?php
                        echo validation_errors();
                        echo form_open_multipart('account/settings/changephoto');
                    ?>

                            <div class="control-group d-none">
                                <div class="admin-form-group controls mb-0 pb-1">
                                <input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="admin-form-group controls mb-0 pb-1">
                                    <label>Choose Photo</label>
                                    <div class="input-group">
                                        <div class=" ">
                                                    <span class="input-group-text   btn btn-default btn-file">Upload Photo
                                                            <input type="file" id="flePhoto" name="userfile">
                                                        </span>
                                        </div>
										<div class="form-group">
											<button type="submit" class="btn btn-primary ml-3 " id="sendMessageButton">Save Photo</button>
										</div>
                                        <input type="text" class="form-control" style="display: none;" readonly>

                                    </div>
									<div class="container">
										<img id='img-upload' crossorigin="ml-4" style="height: 149px!important; width: auto!important;" />
									</div>
                                </div>
                            </div>
                            <br>
                            <div id="success"></div>
                           
                        </form>

        </div>
    </div>
</div>
</section>
