<style>
body {
  background: #388cb3;
}
</style>

<div class="container pt-3">
    <div class="row">
      <div class="col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
          	<div class="card-logo d-flex justify-content-between">
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>img/logo.png" class="imglogo"></a>
				<?php   if( base_url() == 'http://localhost/boardspeak_2020/' ||  base_url() == 'https://beta.boardspeak.com/' ){ ?>
					<h6 style="    text-align: center;
    border: 3px solid #388cb3;
    padding: 10px;">We're in BETA</h6>
				<?php } ?>
			</div>
            <h5 class="card-title text-center">Welcome, please log in</h5>
						<p>We are still in BETA.  Be an important part of the app’s development. Your feedback will help us release a better version. Thank you!</p>
            <div class="card-message"><?php echo $error_message;?></div>
            <?php
//			if(isset($_GET['getback'])){
//				echo form_open('pages/view/signin/'.$_GET['getback'].'/'.$_GET['pageID']);
//			}else{
				echo form_open('pages/view/signin');
//			}

			?>

			 <?php  if(isset($_GET['getback'])){ ?>
			  <input type="hidden" name="getback"  value="<?php if(isset($_GET['getback'])){
				  echo $_GET['getback'];
			  } ?>">
			  <input type="hidden" name="pageID"  value="<?php if(isset($_GET['pageID'])){
				  echo $_GET['pageID'];
			  } ?>">
			  <?php } ?>
              <div class="form-label-group">
                <input type="email" id="txtEmail" name="txtEmail" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputEmail">Email address</label>
              </div>


							<p class="mb-1"><a href="#" id="show-password">Show Password</a></p>
              <div class="form-label-group">
                <input type="password" id="txtPassword" name="txtPassword" class="form-control" placeholder="Password" required>
                <label for="inputPassword">Password</label>
              </div>
			  <div class="d-flex justify-content-between">
				  <p class="mb-1"><a href="<?php echo base_url(); ?>/pages/view/forgot">Forgot Password</a></p>
				  <p class="mb-1"><a href="<?php echo base_url(); ?>/pages/view/resendemail"> Resend Activation Email</a></p>

			  </div>
<!--				<p class="mb-1"><a href="--><?php //echo base_url(); ?><!--/pages/view/forgot">Forgot Password</a></p>-->
<!--			    <p class="mb-1"><a href="--><?php //echo base_url(); ?><!--/pages/view/resendemail"> Resend Activation Email</a></p>-->
							<div class="form-check mb-4">
								<input type="checkbox" class="form-check-input" id="chkRemember">
								<label class="form-check-label" for="chkRemember">Remember Me</label>
							</div>

              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Log in</button>
            </form>
        	<p class="text-center mt-2">New to BoardSpeak? <a class="" href="<?php echo base_url(); ?>pages/view/signup">Sign up here</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

	<!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url(); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="<?php echo base_url(); ?>js/jqBootstrapValidation.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url(); ?>js/freelancer.min.js"></script>
	<script src="<?php echo base_url(); ?>js/custom.js"></script>

  <script>
	$(document).ready( function() {

			$("#show-password").on('click', function(event) {
				event.preventDefault();

        if($('#txtPassword').attr("type") == "text"){
            $('#txtPassword').attr('type', 'password');
						$("#show-password").html('Show Password');
        }else if($('#txtPassword').attr("type") == "password"){
            $('#txtPassword').attr('type', 'text');
						$("#show-password").html('Hide Password');
        }
			});




		});



	</script>

</body>

</html>

