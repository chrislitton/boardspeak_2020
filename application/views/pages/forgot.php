<style>
body {
  background: #388cb3;
}
</style>

<div class="container pt-3">
    <div class="row">
      <div class="col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
          	<div class="card-logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>img/logo.png" class="imglogo"></a></div>
             <a  href="<?php echo base_url(); ?>pages/view/signin">Back to Login</a>

            <h3 class=" "><strong>Forgot Password</strong></h3>
			<p> Send a link to your email to reset your password.</p>
            <div class="card-message"><?php echo $error_message;?></div>
           		 <?php echo form_open('pages/view/reset-password'); ?>
				  <div class="form-label-group">
					<input type="email" id="txtEmail" name="txtEmail" class="form-control"
					 placeholder="Email address" required autofocus>
					<label for="inputEmail">Email address</label>
				  </div>
 				  <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Send Reset Link</button>
            </form>
        	<p class="text-center mt-2">New to BoardSpeak? <a class="" href="<?php echo base_url(); ?>pages/view/signup">Sign up here</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>

</html>
