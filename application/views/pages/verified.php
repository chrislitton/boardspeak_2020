<style>
body {
  background: #388cb3;
}
</style>

<div class="container pt-3">

    <div class="row">

      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
          	<div class="card-logo"><img src="<?php echo base_url(); ?>img/logo.png" class="imglogo"></div>
            <h5 class="card-title text-center">Account activation successful!</h5>
                <p class="text-center mt-2" style="  background: #388cb314; padding: 10px">You may now sign in and use temporary password in your activation email. Thank you!</p>
                <p class="text-center mt-2">Go back to home page <a class="" href="<?php echo base_url(); ?>">click here</a></p>
                <p class="text-center mt-2"><a class="" href="<?php echo base_url(); ?>pages/view/signin">Sign in here</a></p>
          </div>
        </div>
      </div>
    </div>
		
  </div>
  
</body>

</html>
