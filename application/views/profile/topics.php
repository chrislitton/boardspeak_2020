<!-- Topics Section -->
<section class="portfolio" id="topics">
    <div class="container">
        <div class="row">            

            <div class="col-md-6 col-lg-4 admin-img-box groupsloadmore">
                <div class="admin-img">
                    <a class="board-link" href="<?php echo base_url(); ?>account/create/topic">
                        <img class="img-fluid" src="<?php echo base_url();?>img/newtopic.png" alt="image"/>
                    </a>
                </div>
            </div>
            
            
            <?php foreach ($topics_items as $item): ?>
                <div class="col-md-6 col-lg-4 admin-img-box topicsloadmore">
                    <div class="admin-img <?php  if (strlen($item['To_Thumb'])==0) echo 'bg-topic'; echo ' '.$item['To_Backcolor']; ?>">
                        <a class="board-link" href="<?php echo base_url(); ?>account/view/topic/<?php echo $item['To_ID'];?>">                            	
                            <img class="img-fluid" src="<?php echo $item['To_Thumb']; ?>" alt="">
                        </a>
                        <div class="admin-img-label shadow-sm">  
                            <div class="row">     
                                <div class="col-md-12 col-lg-12">
                                    <a class="board-link" href="<?php echo base_url(); ?>account/view/topic/<?php echo $item['To_ID'];?>">
                                    <p class="board-title text-center"><?php echo $item['To_Name']; ?></p>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="btn-social-box-left">
                                        <a class="btn admin-btn-social mx-0" href="#">
                                            <i class="fa fa-users mr-2"></i> Invite
                                        </a>
                                    </div>
                                    <div class="btn-social-box-right">
                                        <a class="btn admin-btn-social mx-0" href="#">
                                            <i class="fa fa-share mr-2"></i> Share
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>  
                </div>

        <?php endforeach; ?>
        <div class="col-md-12 col-lg-12 mt-3 text-center <?php if (count($topics_items)<=12) echo 'd-none';?>">
            <a class="btn btn-primary px-5" href="#" id="topicsloadmore">Load More</a>
        </div>


        </div>	
    </div> 
</section>