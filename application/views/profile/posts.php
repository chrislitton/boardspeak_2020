<!-- posts Section -->
<section class="portfolio" id="posts">
    <div class="container">
        <div class="row">    

            <div class="col-md-6 col-lg-4 admin-img-box groupsloadmore">
                <div class="admin-img">
                    <a class="board-link" href="<?php echo base_url(); ?>account/create/post">
                        <img class="img-fluid" src="<?php echo base_url();?>img/newpost.png" alt="image"/>
                    </a>
                </div>
            </div>
            
            <?php foreach ($posts_items as $item): ?>


                <div class="col-md-6 col-lg-4 admin-img-box postsloadmore">
                    <div class="admin-img <?php  if (strlen($item['Po_Thumb'])==0) echo 'bg-post'; echo ' '.$item['Po_Backcolor']; ?>">									
                        <a class="board-link" href="<?php echo base_url(); ?>account/view/post/<?php echo $item['Po_ID'];?>">
                            <img class="img-fluid" src="<?php echo $item['Po_Thumb']; ?>" alt="">
                        </a>

                        <div class="admin-img-label shadow-sm">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <a class="board-link" href="<?php echo base_url(); ?>account/view/post/<?php echo $item['Po_ID'];?>">
                                    <p class="board-title text-center"><?php echo $item['Po_Title']; ?></p>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="btn-social-box-left">
                                        <a class="btn admin-btn-social mx-0" href="#">
                                            <i class="fa fa-users mr-2"></i> Invite
                                        </a>
                                    </div>
                                    <div class="btn-social-box-right">
                                        <a class="btn admin-btn-social mx-0" href="#">
                                            <i class="fa fa-share mr-2"></i> Share
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            <?php endforeach; ?>
            <div class="col-md-12 col-lg-12 mt-3 text-center <?php if (count($posts_items)<=12) echo 'd-none';?>">
                <a class="btn btn-primary px-5" href="#" id="postsloadmore">Load More</a>
            </div>

    </div>
</div> 
</section>