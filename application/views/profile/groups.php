<section class="portfolio" id="groups">
        <div class="container">
            <div class="row" id="groupresult">

                <div class="col-md-6 col-lg-4 admin-img-box groupsloadmore">
                    <div class="admin-img">
                        <a class="board-link" href="<?php echo base_url(); ?>account/create/group">
                            <img class="img-fluid" src="<?php echo base_url();?>img/newgroup.png" alt="image"/>
                        </a>
                    </div>
                </div>

                <?php foreach ($groups_items as $item): ?>

                        <div class="col-md-6 col-lg-4 admin-img-box groupsloadmore">
                            <div class="admin-img <?php  if (strlen($item['Gr_Thumb'])==0) echo 'bg-group'; echo ' '.$item['Gr_Backcolor']; ?>">
                                    <a class="board-link" href="<?php echo base_url(); ?>account/view/group/<?php echo $item['Gr_ID'];?>">
                                        <img class="img-fluid" src="<?php echo $item['Gr_Thumb'];?>" alt="">
                                    </a>
                                    <div class="admin-img-label shadow-sm">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <a class="board-link" href="<?php echo base_url(); ?>account/view/group/<?php echo $item['Gr_ID'];?>">
                                                <p class="board-title text-center"><?php echo $item['Gr_Name']; ?></p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="btn-social-box-left">
                                                    <a class="btn admin-btn-social mx-0" href="#">
                                                        <i class="fa fa-users mr-2"></i> Invite
                                                    </a>
                                                </div>
                                                <div class="btn-social-box-right">
                                                    <a class="btn admin-btn-social mx-0" href="#">
                                                        <i class="fa fa-share mr-2"></i> Share
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>      

            <?php endforeach; ?>


            <div class="col-md-12 col-lg-12 mt-3 text-center <?php if (count($groups_items)<=12) echo 'd-none';?>">
                <a class="btn btn-xl btn-primary px-5" href="#" id="groupsloadmore">Load More</a>
            </div>

        </div>

    </div> 
</section>