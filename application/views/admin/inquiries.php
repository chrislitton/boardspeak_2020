<section class="page-section my-0 py-0 admin_tab">
    <div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="row admin_title">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Messages</h5>
					</div>
				</div>
				<table id="contacts" class="datatable table stripe">
					<thead class="thead-light">
						<tr>
							<th>Name</th>
							<th>Subject</th>
							<th>Date Posted</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($inquiries_item as  $item) {


						 ?>


							<tr>
								<td>
									<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/message/detail/<?php echo $item['In_ID'];?>">
										<?php echo $item['In_FName'].' '.$item['In_LName']; ?>
									</a>
								</td>
								<td>
									<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/message/detail/<?php echo $item['In_ID'];?>">
										<?php echo $item['In_Subject']; ?>
									</a>
								</td>
								<td>
									<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/message/detail/<?php echo $item['In_ID'];?>">
										<?php echo $item['In_DatePosted']; ?>
									</a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
