<section class="page-section my-0 py-0 admin_tab">
    <div class="container">
	   	<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="row admin_title">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Keyword
						<span class="float-right">
							<a class="hlink" href="<?php echo base_url(); ?>admin/keyword">Show List</a>
							<a class="hlink ml-2" href="<?php echo base_url(); ?>admin/keyword/addnew">Add New</a>
						</span>
						</h5>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 col-lg-12">
						<table id="categories" class="datatable table stripe">
							<thead class="thead-light">
								<tr>
									<th scope="col" style="width:30px;">#</th>
									<th scope="col">Name</th>
									<th scope="col" class="text-center">Banned</th>
									<th class="text-right" scope="col" style="width: 80px;">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($keyword_items as $item): ?>
								<tr>
									<th scope="row"><?php echo $item['Kw_ID']; ?></th>
									<td><a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/keyword/sub/<?php echo $item['Kw_ID'];?>"><?php echo $item['Kw_Keyword']; ?></a></td>
									<td class="text-center">
										<?php if($item['Kw_Banned']== 1){ ?>
										<i class="fa fa-check"></i>
										<?php } ?>
									</td>
									<td class="text-right">
										<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/keyword/detail/<?php echo $item['Kw_ID'];?>">
											<i class="fa fa-edit text-dark"></i>
										</a>
										<a class="admin-btn-social"  href="#" data-href="<?php echo base_url(); ?>admin/keyword/delete/<?php echo $item['Kw_ID'];?>" data-toggle="modal" data-target="#confirm-delete">
											<i class="fa fa-trash text-danger"></i>
										</a>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
