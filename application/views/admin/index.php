<section class="page-section my-0 py-0 admin_tab">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">

				<div class="row admin_title">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Admins</h5>
					</div>
				</div>
				<!--			--><?php // print_r($users_item); ?>
				<table id="users" class="datatable table stripe">
					<thead class="thead-light">
					<tr>
						<th scope="col" style="width:30px;">#</th>
						<th scope="col">Name</th>
						<th scope="col">Email</th>
						<th scope="col">Phone</th>
						<th scope="col" style="width:150px;">Date Posted</th>
						<th scope="col"  >Action</th>

					</tr>
					</thead>
					<tbody>
					<?php foreach ($users_item as $item): ?>
						<tr>
							<th scope="row">
								<?php echo $item['Us_ID']; ?>
							</th>

							<td>
								<img width="40px" src="<?php echo $item['Us_Photo'] ?>">
								<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/users/detail/<?php echo $item['Us_ID'];?>"></a></td>
							<td><?php echo $item['Us_Email'] ?></td>
							<td><?php echo $item['Us_Phone'] ?></td>
							<td><?php echo date('M j, Y H:m', strtotime($item['Us_DateTime'])); ?></td>
							<td>
								 <button class="btn btn-info" data-username = '<?php echo $item['Us_Alias']; ?>'
										 onclick='removeFromAdmin(<?php echo $item['Us_ID']; ?> , this  )'>Remove</button>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</section>
<div class = "modal fade" id = "coinmodel" role = "dialog">
	<div class = "modal-dialog modal-md">
		<div class = "modal-content">
			<div class = "modal-header">
				<h6 class = "modal-title">Coin Credit Section</h6>
				<button type = "button" class = "close" data-dismiss = "modal">×</button>

			</div>
			<div class = "modal-body">
				<input type="hidden" id="useriD">
				<input type="hidden" id="username">
				<h6 id="userdetail"></h6>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="basic-addon1"><i class="fas fa-coins"></i></span>
					</div>
					<input style="width: 80%;" id="coinammount" type="number" class="form-control" placeholder="Enter Coins here" aria-label="Username" aria-describedby="basic-addon1">
				</div>
				<button onclick="sendCoin()" style="    float: right;" class="btn btn-info">Send Coins</button>
			</div>

		</div>
	</div>
</div>
<script>

	function removeFromAdmin(id  , button){
			let name = 'malik';
			console.log(button.data('username'));
			console.log(button.attr("data-username");)
		// alert($(this).attr("data-username"));
		// 	alert(button.data("username"));
			return;
		Swal.fire({
			title: 'Are you sure?',
			text: "You want to send Remove "+name+" From Admin Status",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Remove !'
		}).then((result) => {
			if (result.isConfirmed) {

				$.ajax({
					type: 'POST',
					url: base_url + 'admin/removeadmin/',
					data : {id:id },
				}).done(
					function(dataz) {
						console.log(dataz);

					});

			}
		})

	}
	function sendCoin(){

		if($('#coinammount').val() == ''){
			return;
		}
		Swal.fire({
			title: 'Are you sure?',
			text: "You want to send "+$('#coinammount').val()+" coin to "+$('#username').val(),
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, send coins!'
		}).then((result) => {
			if (result.isConfirmed) {

				// alert();

			}
		})
	}
	function showcoinspop(id , name){

		$('#coinmodel').modal('show');
		$('#useriD').val(id);
		$('#username').val(name);

		$('#userdetail').text('Send coins to '+name);
	}
</script>
