<section class="page-section my-0 py-0 admin_tab">
    <div class="container">
   	<div class="row">
	<div class="col-md-12 col-lg-12">

		<div class="row admin_title">
			<div class="col-md-12 col-lg-12">
				<h5 class="text-secondary">Users</h5>
			</div>
		</div>
<!--			--><?php // print_r($users_item); ?>
		<table id="users" class="datatable table stripe">
		<thead class="thead-light">
			<tr>
				<th scope="col" style="width:30px;">#</th>
				<th scope="col">Name</th>
				<th scope="col">Email</th>
				<th scope="col">Phone</th>
				<th scope="col" style="width:150px;">Date Posted</th>
				<th scope="col"  >Action</th>

			</tr>
		</thead>
		<tbody>
			<?php foreach ($users_item as $item): ?>
			<tr>
				<th scope="row"><?php echo $item['Us_ID']; ?></th>

				<td>
					<img width="40px" src="<?php echo $item['Us_Photo'] ?>">
					<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/users/detail/<?php echo $item['Us_ID'];?>"></a></td>
				<td><?php echo $item['Us_Email'] ?></td>
				<td><?php echo $item['Us_Phone'] ?></td>
				<td><?php echo date('M j, Y H:m', strtotime($item['Us_DateTime'])); ?></td>
				<td><button class="btn" onclick="showcoinspop('<?php echo $item['Us_ID']; ?>' , '<?php echo $item['Us_Name']; ?>')"><i style="color: orange" class="fas fa-coins"></i></button>
					<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/users/detail/<?php echo $item['Us_ID'];?>"><i style="blue" class="fa fa-edit"></i></a>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>

	</div>
	</div>
	</div>
</section>
<div class = "modal fade" id = "coinmodel" role = "dialog">
	<div class = "modal-dialog modal-md">
		<div class = "modal-content">
			<div class = "modal-header">
				<h6 class = "modal-title">Coin Credit Section</h6>
				<button type = "button" class = "close" data-dismiss = "modal">×</button>

			</div>
			<div class = "modal-body">
				<input type="hidden" id="useriD">
				<input type="hidden" id="username">
 				<h6 id="userdetail"></h6>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text" id="basic-addon1"><i class="fas fa-coins"></i></span>
					</div>
					<input style="width: 80%;" id="coinammount" type="number" class="form-control" placeholder="Enter Coins here" aria-label="Username" aria-describedby="basic-addon1">
				</div>
				<button onclick="sendCoin()" style="    float: right;" class="btn btn-info">Send Coins</button>
 			</div>

		</div>
	</div>
</div>
<script>
	function sendCoin(){

		if($('#coinammount').val() == ''){
			return;
		}
		Swal.fire({
			title: 'Are you sure?',
			text: "You want to send "+$('#coinammount').val()+" coin to "+$('#username').val(),
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, send coins!'
		}).then((result) => {
			if (result.isConfirmed) {
 	$.ajax({
					type: 'POST',
					url: base_url + 'account/coin_creted/',
					data : {id:$('#useriD').val() , coins : $('#coinammount').val()  },
				}).done(function(dataz) {
					let 	data  = JSON.parse(dataz);

					if(data == 'success'){
						$('#coinmodel').modal('hide');
						$('#coinammount').val('');
						Swal.fire(
							'success!',
							'Coins Credit successfully.',
							'success'
						)
					}else{
						Swal.fire(
							'error!',
							'Try again. Something went Wrong.',
							'error'
						)
					}
				});
				// alert();

			}
		})
	}
	function showcoinspop(id , name){

		$('#coinmodel').modal('show');
		$('#useriD').val(id);$('#username').val(name);

		$('#userdetail').text('Send coins to '+name);
	}
</script>
