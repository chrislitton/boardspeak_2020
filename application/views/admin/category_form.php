<section class="page-section my-0 py-0">
    <div class="container">
   	<div class="row">
	<div class="col-md-12 col-lg-12">


				<div class="row">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Category
						<span class="float-right">
							<a class="hlink" href="<?php echo base_url(); ?>admin/category">Show List</a>
							<a class="hlink ml-2" href="<?php echo base_url(); ?>admin/category/addnew">Add New</a>
						</span>
						</h5>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 mx-auto mb-1">
						<?php if (strlen($error)>0) echo '<p class="text-center text-danger">'.$error.'</p>';?>
						<?php if (strlen($notification)>0) echo '<p class="text-center text-success">'.$notification.'</p>';?>
					</div>
				</div>


				<div class="row">
					<div class="col-md-12 col-lg-12">
						<?php
							echo form_open('admin/category/save');
							echo form_hidden('txtID',$txtID);
						?>

						

						<div class="row">
							<div class="col-md-12 col-lg-12">


							
								<div class="control-group mb-1">
									<label class="text-muted mb-1">Type</label>
									<div class="admin-form-group controls pb-1">
									<div class="input-group">
											<select class="custom-select" id="selType" name="selType" required="required">
												<option value="">Choose Type</option>
												<option value="group" <?php if (strcmp($selType,'group')==0) echo 'selected'?>>Group</option>
												<option value="topic" <?php if (strcmp($selType,'topic')==0) echo 'selected'?>>Topic</option>
												<option value="post" <?php if (strcmp($selType,'post')==0) echo 'selected'?>>Post</option>
											</select>
									</div>
									</div>
								</div>


								<div class="control-group mb-3">
									<label class="text-muted mb-1">Category</label>
									<div class="admin-form-group controls pb-0">
										<input class="form-control" id="txtName" name="txtName" value="<?php echo $txtName;?>" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter name.">
									</div>
								</div>

								<div class="control-group mb-1">
									<div class="admin-form-group controls pb-0">
										<input type="checkbox" id="default" name="default" <?php if($default == 1){ echo 'checked'; }?>>
										<label class="form-check-label" for="default">Default</label>
									</div>
								</div>
								
							</div>
						</div>


						<div class="admin-form-group text-right mt-3">
							<a class="btn btn-light btn-xl mr-2" href="<?php echo base_url(); ?>admin/category">Cancel</a>
							<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Submit</button>
						</div>

						</form>
					</div>
				</div>





	</div>
	</div>
	</div>
</section>
