<section class="page-section my-0 py-0">
    <div class="container">
   	<div class="row">
	<div class="col-md-12 col-lg-12">


    <div class="row">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Category
						<span class="float-right">
							<a class="hlink" href="<?php echo base_url(); ?>admin/category">Show List</a>
							<a class="hlink ml-2" href="<?php echo base_url(); ?>admin/category/addnew">Add New</a>
						</span>
						</h5>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 mx-auto mb-1">
						<?php if (strlen($error)>0) echo '<p class="text-center text-danger">'.$error.'</p>';?>
						<?php if (strlen($notification)>0) echo '<p class="text-center text-success">'.$notification.'</p>';?>
					</div>
				</div>


				<div class="row mt-3">
					<div class="col-md-12 col-lg-12 mb-5">

						<?php
							echo form_open('admin/category/savesub');
							echo form_hidden('txtID',$txtID);
							echo form_hidden('txtSubID',$txtSubID);
						?>

						<p><span class="text-muted mr-3"><?php echo ucfirst($selType);?> Category:</span><b><?php echo $txtName;?></b></p>

						<div class="control-group">
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
							<div class="input-group-prepend">
								<label class="input-group-text rounded-0" for="txtSubName">New Sub-Category</label>
							</div>
							<input class="form-control" id="txtSubName" name="txtSubName" value="<?php echo $txtSubName;?>" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter name.">
							<div class="input-group-append">
								<button type="submit" class="btn btn-primary rounded-0" id="sendMessageButton">Submit</button>
							</div>
						</div>
						</div>
						</div>
						</form>


						
					</div>
					<div class="col-md-12 col-lg-12">


						<table class="table table-striped table-hover">
						<thead class="thead-light">
							<tr>
								<th scope="col" style="width:30px;">#</th>
								<th scope="col">Name</th>
								<th class="text-right" scope="col" style="width: 80px;">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($subcategory_items as $item): ?>
							<tr>
								<th scope="row"><?php echo $item['Sc_ID']; ?></th>
								<td><?php echo $item['Sc_Name']; ?></td>
								<td class="text-right">
									<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/category/sub/<?php echo $item['Sc_Ca_ID'];?>/<?php echo $item['Sc_ID'];?>">
										<i class="fa fa-edit text-dark"></i>
									</a>
									<a class="admin-btn-social"  href="#" data-href="<?php echo base_url(); ?>admin/category/deletesub/<?php echo $item['Sc_Ca_ID'];?>/<?php echo $item['Sc_ID'];?>" data-toggle="modal" data-target="#confirm-delete">
										<i class="fa fa-trash text-danger"></i>
									</a>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						</table>
						
					</div>
				</div>





	</div>
	</div>
	</div>
</section>
