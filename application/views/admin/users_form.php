<section class="page-section my-0 py-0">
    <div class="container">
   	<div class="row">
	<div class="col-md-12 col-lg-12">


				<div class="row">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Users Form</h5>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 mx-auto mb-1">
						<?php if (strlen($error)>0) echo '<p class="text-center text-danger">'.$error.'</p>';?>
						<?php if (strlen($notification)>0) echo '<p class="text-center text-success">'.$notification.'</p>';?>
					</div>
				</div>


				<div class="row mt-3">
					<div class="col-12 col-lg-12 mb-4">

						<div class="control-group row">
							<div class="admin-form-group controls mb-2 pb-1 col">
							<label class="text-muted">First Name</label>
							<input class="form-control" id="txtFirstName" name="txtFirstName" value="<?php echo $txtFirstName;?>" type="text" readonly>
							</div>

							<div class="admin-form-group controls mb-2 pb-1 col">
							<label class="text-muted">Last Name</label>
							<input class="form-control" id="txtLastName" name="txtLastName" value="<?php echo $txtLastName;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group">
							<div class="admin-form-group controls mb-2 pb-1">
							<label class="text-muted">Email</label>
							<input class="form-control" id="txtEmail" name="txtEmail" value="<?php echo $txtEmail;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group">
							<div class="admin-form-group controls mb-2 pb-1">
							<label class="text-muted">Phone</label>
							<input class="form-control" id="txtPhone" name="txtPhone" value="<?php echo $txtPhone;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group">
							<div class="admin-form-group controls mb-2 pb-1">
							<label class="text-muted">Company/ Community</label>
							<input class="form-control" id="txtCompany" name="txtCompany" value="<?php echo $txtCompany;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group" about="">
							<div class="admin-form-group controls mb-2 pb-1">
							<label class="text-muted">Number Of Members</label>
							<input class="form-control" id="txtNoOfEmployees" name="txtNoOfEmployees" value="<?php echo $txtNoOfEmployees;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group">
							<div class="admin-form-group controls mb-2 pb-1">
							<label class="text-muted">Job Title/ Position</label>
							<input class="form-control" id="txtJobTitle" name="txtJobTitle" value="<?php echo $txtJobTitle;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group">
							<div class="admin-form-group controls mb-1 pb-1">
							<label class="text-muted">Features</label>
							  <textarea class="form-control" id="txtMessage" name="txtMessage" rows="5" readonly><?php echo $txtFeatures;?></textarea>
							</div>
						</div>

						<p class="m-0"><span class="text-muted">Status: </span> <?php echo ucfirst($txtStatus);?></p>
					</div>

					<div class="col-12 col-lg-12 text-right">
						<a class="btn btn-primary btn-xl mr-2" href="<?php echo base_url(); ?>admin/users">Back</a>
						<a class="btn btn-danger btn-xl"  href="#" data-href="<?php echo base_url(); ?>admin/users/delete/<?php echo $txtID;?>" data-toggle="modal" data-target="#confirm-delete">Delete</a>
					</div>
				</div>





	</div>
	</div>
	</div>
</section>
