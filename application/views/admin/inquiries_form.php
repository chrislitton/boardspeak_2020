<section class="page-section my-0 py-0 admin_tab">
    <div class="container">
   		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="row">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Inquiry Form</h5>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 mx-auto mb-1">
						<?php if (strlen($error)>0) echo '<p class="text-center text-danger">'.$error.'</p>';?>
						<?php if (strlen($notification)>0) echo '<p class="text-center text-success">'.$notification.'</p>';?>
					</div>
				</div>


				<div class="row mt-3">
					<div class="col-12 col-lg-12 mb-4">

						<div class="control-group row">
							<div class="admin-form-group controls mb-2 pb-1 col">
							<label class="text-muted">First Name</label>
							<input class="form-control" id="txtFirstName" name="txtFirstName" value="<?php echo $txtFirstName;?>" type="text" readonly>
							</div>

							<div class="admin-form-group controls mb-2 pb-1 col">
							<label class="text-muted">Last Name</label>
							<input class="form-control" id="txtLastName" name="txtLastName" value="<?php echo $txtLastName;?>" type="text" readonly>
							</div>
						</div>


						<div class="control-group">
							<div class="admin-form-group controls mb-2 pb-1">
							<label class="text-muted">Email</label>
							<input class="form-control" id="txtEmail" name="txtEmail" value="<?php echo $txtEmail;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group">
							<div class="admin-form-group controls mb-2 pb-1">
								<label class="text-muted">Subject</label>
								<input class="form-control" id="txtSubject" name="txtSubject" value="<?php echo $txtSubject;?>" type="text" readonly>
							</div>
						</div>

						<div class="control-group">
							<div class="admin-form-group controls mb-0 pb-1">
								<label class="text-muted">Message</label>
							  <textarea class="form-control" id="txtMessage" name="txtMessage" rows="5" readonly><?php echo $txtMessage;?></textarea>
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-12 text-right">
						<a class="btn btn-primary btn-xl mr-2" href="<?php echo base_url(); ?>admin/message">Back</a>
						<a class="btn btn-primary btn-xl mr-2" href="<?php echo base_url(); ?>admin/message/reply/<?php echo $txtID; ?>">Reply</a>
						<a class="btn btn-danger btn-xl"  href="#" data-toggle="modal" data-target="#confirm-delete-message">Delete</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="confirm-delete-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
							<span class="text-muted">Delete Confirmation</span>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="fas fa-times"></i>
								</span>
							</button>
			</div>
			<div class="modal-body text-center py-2 pt-5">
				<p class="lead text-danger">Are you sure you want to delete this record?</p>
			</div>
			<div class="modal-footer pt-5 pb-4">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<a class="btn btn-danger btn-ok" href="<?php echo base_url(); ?>admin/message/delete/<?php echo $txtID;?>">Delete</a>
			</div>
		</div>
	</div>
</div>
