<section class="page-section my-0 py-0">
    <div class="container">
   	<div class="row">
	<div class="col-md-12 col-lg-12">


				<div class="row">
					<div class="col-md-12 col-lg-12">
						<h5 class="text-secondary">Background Form
						<span class="float-right">
							<a class="hlink" href="<?php echo base_url(); ?>admin/background">Show List</a>
							<a class="hlink ml-2" href="<?php echo base_url(); ?>admin/background/addnew">Add New</a>
						</span>
						</h5>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 mx-auto mb-1">
						<?php if (strlen($error)>0) echo '<p class="text-center text-danger">'.$error.'</p>';?>
						<?php if (strlen($notification)>0) echo '<p class="text-center text-success">'.$notification.'</p>';?>
					</div>
				</div>


				<div class="row mt-3">
					<div class="col-md-12 col-lg-12">
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<p><span class="text-muted mr-3">Library:</span><b><?php echo $txtName;?></b></p>
							</div>
						</div>


						<div class="row">
							<div class="col-lg-9">

								<div class="row">
								<?php foreach ($bgphotos_items as $item): ?>

									<div class="col-6 col-md-6 col-lg-4 admin-img-box">
										<div class="admin-img">

												<img class="img-fluid" src="<?php echo $item['BgP_Thumb']; ?>" alt="" onerror="this.src='<?php echo base_url(); ?>img/nobg.png'">
												<div class="bg-light">
													<div class="row">
														<div class="col-md-6 col-lg-6">
															<p class="lead board-category"><?php echo $item['BgP_Name']; ?></p>
														</div>
														<div class="col-md-6 col-lg-6 text-right">
															<a class="btn admin-btn-social mx-1"  href="#" data-href="<?php echo base_url(); ?>admin/background/deletephoto/<?php echo $txtID; ?>/<?php echo $item['BgP_ID'];?>" data-toggle="modal" data-target="#confirm-delete">
																<i class="fa fa-trash text-danger"></i>
															</a>
													</div>
													</div>
												</div>

										</div>
									</div>

								<?php endforeach; ?>
								</div>

							</div>
							<div class="col-lg-3">


										<?php
											echo form_open_multipart('admin/background/savephoto');
											echo form_hidden('txtID',$txtID);
										?>
										<div class="row">


											<div class="col-md-12 col-lg-12">
												<h5 class="text-secondary">Upload Photos</h5>
											</div>

											<div class="col-md-12 col-lg-12">
												<div class="control-group">
													<div class="admin-form-group controls mb-2 pb-1">
														<div class="input-group">
															<div class="input-group-prepend">
																<span class="input-group-text rounded-0 btn btn-default btn-file">Upload Image
																	<input type="file" id="userfile" name="userfile">
																</span>
															</div>
															<input type="text" class="form-control" readonly>
															<img id='img-upload' src="<?php echo base_url(); ?>img/nobg.png"/>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-md-12 col-lg-12">

												<div class="control-group">
													<div class="admin-form-group controls mb-3 pb-1">
														<input class="form-control" id="txtName" name="txtName" type="text" placeholder="Title" required="required" data-validation-required-message="Please enter name.">
													</div>
												</div>
												<div class="admin-form-group text-right">
													<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Submit</button>
												</div>

											</div>
										</div>
										</form>


						</div>
					</div>
				</div>





	</div>
	</div>
	</div>
</section>
