<section class="page-section my-0 py-0 admin_tab">
    <div class="container">
   	<div class="row">
	<div class="col-md-12 col-lg-12">

		<div class="row admin_title">
			<div class="col-md-12 col-lg-12">
				<h5 class="text-secondary">Background
				<span class="float-right">
					<a class="hlink" href="<?php echo base_url(); ?>admin/background">Show List</a>
					<a class="hlink ml-2" href="<?php echo base_url(); ?>admin/background/addnew">Add New</a>
				</span>
				</h5>
			</div>
		</div>


		<table id="background" class="datatable table stripe">
		<thead class="thead-light">
			<tr>
				<th scope="col" style="width:30px;">#</th>
				<th scope="col">Library</th>
				<th class="text-right" scope="col" style="width: 60px;">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($bgcategory_items as $item): ?>
			<tr>
				<th scope="row"><?php echo $item['BgC_ID'];?></th>
				<td>
					<p class="m-0"><a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/background/photos/<?php echo $item['BgC_ID'];?>"><?php echo $item['BgC_Name']; ?></a></p>
				</td>
				<td class="text-right">

					<a class="admin-btn-social mr-2" href="<?php echo base_url(); ?>admin/background/detail/<?php echo $item['BgC_ID'];?>">
						<i class="fa fa-edit text-dark"></i>
					</a>
					<a class="admin-btn-social"  href="#" data-href="<?php echo base_url(); ?>admin/background/delete/<?php echo $item['BgC_ID'];?>" data-toggle="modal" data-target="#confirm-delete">
						<i class="fa fa-trash text-danger"></i>
					</a>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>

	</div>
	</div>
	</div>
</section>
