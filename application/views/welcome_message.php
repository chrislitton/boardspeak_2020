<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  $this->load->helper('url');
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" type="images/icon" href="<?php echo base_url(); ?>img/icon.png" />
  <meta name="description" content="">
  <meta name="author" content="">

  <title>BoardSpeak - Get more things done!</title>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9842180059459508"
			crossorigin="anonymous"></script>
  <!-- Custom fonts for this theme -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/noty/lib/noty.css" rel="stylesheet" type="text/css">

  <!-- Theme CSS -->
  <link href="css/freelancer.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/signin.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-mainmenu text-uppercase fixed-top" id="mainNav">
	<div class="container">
	  <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="img/logo.png" class="imglogo"></a>
	  <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		Menu
		<i class="fas fa-bars"></i>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarResponsive">
		<ul class="navbar-nav ml-auto">
		  <li class="nav-item mx-0 mx-lg-1">
			<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#page-top">Home</a>
		  </li>
		  <li class="nav-item mx-0 mx-lg-1">
			<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Features</a>
		  </li>
		  <li class="nav-item mx-0 mx-lg-1">
			<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#howitworks">How It Works</a>
		  </li>
		  <li class="nav-item mx-0 mx-lg-1">
			<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">Why Us</a>
		  </li>
		  <li class="nav-item mx-0 mx-lg-1">
			<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contact Us</a>
		  </li>
		  <li class="nav-item mx-0 mx-lg-1">

			  <?php  if(base_url() == 'https://boardspeak.com/' || base_url() == 'http://localhost/boardspeak_2020/'){ ?>
				  <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger nav-link-active" href="<?php echo 'https://beta.boardspeak.com/pages/view/signup' ?>" target="_blank">Sign Up</a>

			  <?php  }else{ ?>
				  <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger nav-link-active" href="<?php echo base_url(); ?>pages/view/signup" target="_blank">Sign Up</a>

			  <?php   } ?>
		  </li>
		  <li class="nav-item mx-0 mx-lg-1">
			  <?php  if(base_url() == 'https://boardspeak.com/' || base_url() == 'http://localhost/boardspeak_2020/'){ ?>
				  <a class="nav-link py-3 px-0 px-lg-3 rounded" href="<?php echo 'https://beta.boardspeak.com/pages/view/signin'; ?>"   >Log In  </a>

			  <?php  }else{ ?>
				 <a class="nav-link py-3 px-0 px-lg-3 rounded" href="#" data-href="" data-toggle="modal" data-target="#login" >Log In</a>

			  <?php   } ?>

		  </li>
		</ul>
	  </div>
	</div>
  </nav>

  <!-- Masthead -->
  <header class="masthead bg-primary text-white text-center">
	<!-- <div class="container d-flex align-items-center flex-column"> -->
	<div class="container">
	
	
  <div class="row">
  <div class="col-md-12 col-lg-12">
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="7000">
	  <ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	  </ol>
	  <div class="carousel-inner">
		<div class="carousel-item active">
		  <div class="row">
			<div class="col-md-6 col-lg-6">
				<img class="d-block w-100" src="img/home-banner.png" alt="Build Accountability">
				<a class="btn btn-xl btn-outline-light mb-3" href="<?php echo base_url(); ?>index.php/pages/view/signup" target="_blank">
				  <i class="fas fa-play mr-2"></i>
				  Start a Group
				</a>
			  </div>
			  <div class="col-md-6 col-lg-6">
			  <h1 class="text-uppercase mb-0">Build accountability in teams. Get more things done!</h1>
			  <!-- Icon Divider -->
			  <div class="divider-custom divider-light">
				<div class="divider-custom-line"></div>
			  </div>

			  <!-- Masthead Subheading -->
			  <p class="masthead-subheading font-weight-light mb-0">Efficiently keep track of assigned tasks acted upon and messages read by members.</p>

			  <!-- Icon Divider -->
			  <div class="divider-custom divider-light">
				<div class="divider-custom-line"></div>
			  </div>

			  <p class="masthead-subheading font-weight-light mb-0">BoardSpeak helps organize your team communication with categorized posts by topics and labeling features.</p>
			</div>
		  </div>
		</div>
		<div class="carousel-item">
		  <div class="row">
			<div class="col-md-6 col-lg-6">
				<img class="d-block w-100" src="img/take-control.png" alt="Take Control">
				<a class="btn btn-xl btn-outline-light mb-3" href="<?php echo base_url(); ?>index.php/pages/view/signup" target="_blank">
				  <i class="fas fa-play mr-2"></i>
				  Start a Group
				</a>
			  </div>
			  <div class="col-md-6 col-lg-6">
			  <h1 class="text-uppercase mb-0">Take control of your time</h1>
			  <!-- Icon Divider -->
			  <div class="divider-custom divider-light">
				<div class="divider-custom-line"></div>
			  </div>

			  <!-- Masthead Subheading -->
			  <p class="masthead-subheading font-weight-light mb-0">Enough of information overload in chaotic group chats. Our visual interactive bulletin board provides your team with enhanced admin controls for focused discussion and relevant content on every thread.</p>

			</div>
		  </div>
		</div>
		<div class="carousel-item">
		  <div class="row">
			<div class="col-md-6 col-lg-6">
				<img class="d-block w-100" src="img/be-organize.png" alt="Engage & Retain Customers">
				<a class="btn btn-xl btn-outline-light mb-3" href="<?php echo base_url(); ?>index.php/pages/view/signup" target="_blank">
				  <i class="fas fa-play mr-2"></i>
				  Start a Group
				</a>
			  </div>
			  <div class="col-md-6 col-lg-6">
			  <h1 class="text-uppercase mb-0">Engage & retain customers with positive sentiment towards your brand!</h1>
			  <!-- Icon Divider -->
			  <div class="divider-custom divider-light">
				<div class="divider-custom-line"></div>
			  </div>

			  <!-- Masthead Subheading -->
			  <p class="masthead-subheading font-weight-light mb-0">Promote your brand and sell your products</p>

			</div>
		  </div>

		</div>
	  </div>
	  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	  </a>
	</div>
  </div>    
  </div>

	
  </div>
  </header>
  
  <!-- Sign Up for BETA Section -->
  <section class="page-sub-section bg-free text-white" id="freesignup">
	<div class="container">
   <div class="row">

	<div class="col-md-6 col-lg-6 mt-3">

	  <h4>Get access and exclusive in app perks, by signing up before our official launch!</h4>
	</div>

		<div class="col-md-6 col-lg-6">
		<div class="text-center mt-3">
		  <a class="btn btn-xl btn-outline-light js-scroll-trigger" href="<?php echo base_url(); ?>index.php/pages/view/signup" target="_blank">
			<i class="fas fa-check mr-2"></i>
			Sign Up for BETA, It's <font color="Red">FREE!</font>
		  </a>
		</div>
	   </div> 
	  </div> 
  </section>

  <!-- Portfolio Section -->
  <section class="page-section portfolio" id="portfolio">
	
	
	<div class="container">

	  <!-- Portfolio Section Heading -->
	  <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Features</h2>

	  <!-- Icon Divider -->
	  <div class="divider-custom">
		<div class="divider-custom-line"></div>
	  </div>

	  <!-- Portfolio Grid Items -->
	  <div class="row">

		<!-- Portfolio Item 1 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/schedule.png" alt=""></div>
		  <h4>Schedule Manager</h4>
		  <p class="lead">Set an order of priority for assigned tasks with notifications and due dates</p>
		</div>

		<!-- Portfolio Item 2 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/advertising.png" alt=""></div>
		  <h4>Board of Fame Employee Recognition</h4>
		  <p class="lead">Send morale skyrocketing by highlighting performance showing progress with levels of badges</p>
		</div>

		<!-- Portfolio Item 3 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/list.png" alt=""></div>
		  <h4>Easy-to-use Task Manager</h4>
		  <p class="lead">Instantly delegate the task by turning conversations into actions</p>
		</div>
		
		
		
		<!-- Portfolio Item 1 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/dashboard.png" alt=""></div>
		  <h4>Customizable Dashboard Display</h4>
		  <p class="lead">A cloud-based visual dashboard you can customize depending on task and goal priority</p>
		</div>

		<!-- Portfolio Item 2 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/orig_filters.png" alt=""></div>
		  <h4>Multiple Search Filters</h4>
		  <p class="lead">Quickly find what you are looking for</p>
		</div>

		<!-- Portfolio Item 3 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/notification.png" alt=""></div>
		  <h4>Selective Notification</h4>
		  <p class="lead">Choose topics you care about and throw away clutter & noise</p>
		</div>
		
		
		
		<!-- Portfolio Item 1 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/id-card.png" alt=""></div>
		  <h4>White Labeling Social Collaboration</h4>
		  <p class="lead">Build brand visibility and widen customer reach with visual & interactive bulletin board</p>
		</div>

		<!-- Portfolio Item 2 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/chat.png" alt=""></div>
		  <h4>AI Chatbots</h4>
		  <p class="lead">Automate customer service to immediately reply to customer queries even when everyone is busy</p>
		</div>

		<!-- Portfolio Item 3 -->
		<div class="col-md-6 col-lg-4">
		  <div class="text-center"><img class="img-fluid" src="img/home/shopping.png" alt=""></div>
		  <h4>Ecommerce</h4>
		  <p class="lead">Boost sales with targeted audience in Community Marketplace</p>
		</div>

	  </div>
	  <!-- /.row -->
	  
	   <div class="text-center mt-4">
		<a class="btn btn-xl btn-primary js-scroll-trigger" href="<?php echo base_url(); ?>pages/view/signup">
		  <i class="fas fa-check mr-2"></i>
		  Sign Up Now!
		</a>
	  </div>

	</div>
  </section>
  
  
  <!-- Portfolio Section -->
  <section class="page-section howitworks bg-lightgrey" id="howitworks">
  
	<div class="container">

	  <!-- Portfolio Section Heading -->
	  <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">How it works</h2>

	  <!-- Icon Divider -->
	  <div class="divider-custom">
		<div class="divider-custom-line"></div>
	  </div>

	  <!-- Portfolio Grid Items -->
	  <div class="row mb-3">
	  
		  <!-- Portfolio Item 1 -->
		  <div class="col-md-6 col-lg-6">
			<div class="text-center"><img class="img-fluid" src="img/how/same-board.png" alt=""></div>
		   </div>
  
		  <!-- Portfolio Item 2 -->
		  <div class="col-md-6 col-lg-6">
			<h4>Get your team on the same board!</h4>
			<p class="lead">Create a visual interactive bulletin board with collaboration tools for your group's organized communication. Experience deeper engagement among team members!</p>
		  </div>
	  </div>

	  <!-- Portfolio Grid Items -->
	  <div class="row">

		  <!-- Portfolio Item 2 -->
		  <div class="col-md-6 col-lg-6">
			<h4>Keep group conversations relevant and focused!</h4>
			<p class="lead">To keep discussions organized and on-topic, group creator can assign admins to arrange posts by categories and manage members' access and roles.</p>
		  </div>
		  <!-- Portfolio Item 1 -->
		  <div class="col-md-6 col-lg-6">
			<div class="text-center"><img class="img-fluid" src="img/how/conversation.png" alt=""></div>
		  </div>

	  </div>
	
	
	   <div class="row mt-5">
		<!-- Portfolio Item 1 -->
		<div class="col-md-6 col-lg-6">
			<h4>Make FOMO (fear of missing out) a thing of the past!</h4>
			<p class="lead">Stop chaos and noise in your board by categorizing everyone's posts and contents by topics. You can customize your dashboard's display settings to view or read what's only important to you!</p>
			<br>
		  <h4>Shh! What happens in Vegas stays in Vegas!</h4>
			<p class="lead">That's right, there are just some things that the world doesn't need to know. BoardSpeak allows you to set some topics in a private mode that only select members can view.</p>
		
	</div>

		<!-- Portfolio Item 2 -->
		<div class="col-md-6 col-lg-6">
		  <div class="text-center"><img class="img-fluid" src="img/how/fomo.png" alt=""></div>
		  </div>
	   
	  </div>
	 <div class="row mt-5"> 
	  
		
		<!-- Portfolio Item 1 -->
		<div class="col-md-6 col-lg-6">
		  <div class="text-center"><img class="img-fluid" src="img/how/messages.png" alt=""></div>          
		</div>

		<!-- Portfolio Item 2 -->
		<div class="col-md-6 col-lg-6">
		  <h4>Track your messages & customize your notifications!</h4>
		  <p class="lead">You don't have to be 007 or the CIA just to know if your messages were received. Efficiently track messages if they were read or acted upon. Get notified only on topics and posts that are important to you.</p>
		  <br>
		  <h4>No more excuses just checked boxes!</h4>
		  <p class="lead">There's no "I" in team! Accomplishing tasks doesn't have to be a one man show. Team members can readily create and view assigned tasks in their respective dashboards and calendar.</p>
		</div>
		
	  </div>

	</div>
  </section>
  
  
  <!-- About Section -->
  <section class="page-section bg-primary text-white mb-0" id="about">
	<div class="container">

	  <!-- About Section Heading -->
	  <h2 class="page-section-heading text-center text-uppercase text-white">Why BoardSpeak</h2>

	  <!-- Icon Divider -->
	  <div class="divider-custom divider-light">
		<div class="divider-custom-line"></div>
	  </div>

	  <!-- About Section Content -->
	  <div class="row">
		<div class="col-lg-6 mr-auto">
		  <img class="img-fluid" src="img/home/why-bs.png" alt="">
		</div>
		<div class="col-lg-6 ml-auto">
		  <p class="lead">Are you still using email and group chat for team collaboration? </p>
		  <p class="lead">Are you looking for a better way to organize and search for important information within your group conversations? </p>
		  <p class="lead">Are you using enterprise collaboration tools and wished that it was more user-friendly to convince more team members to join in and stay active? </p>
		  <p class="lead">Fret not. BoardSpeak is here. And its absolutely FREE! There's no limit in the number of members. You only pay for the more advanced features that you will actually use. With enhanced admin controls, it gives you freedom of choice, to filter what you only want to read, without the rest! </p>
		  <p class="lead">BoardSpeak is also ideal for virtual community collaboration. Collaboration that builds personal relationships with real life interaction, where you  get to know the people in your own town, become friends, create partnerships and share resources. </p>
		  <p class="lead">Built by people who simply want more out of LIFE!</p>
		</div>
	  </div>

	  <!-- About Section Button -->
	  <div class="text-center mt-4">
		<a class="btn btn-xl btn-outline-light js-scroll-trigger" href="<?php echo base_url(); ?>index.php/pages/view/signup" target="_blank">
		  <i class="fas fa-check mr-2"></i>
		  Sign Up Now!
		</a>
	  </div>

	</div>
  </section>


  <!-- Contact -->
  <section class="page-section bg-white" id="contact">
	<div class="container">
	  <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Us</h2>

	  <div class="row">
		<div class="col-lg-5 mx-auto">
		  <form id="contactForm" name="sentMessage">
			<div class="row">
			  <div class="col-md-12">
				<div class="row">
				  <div class="col pr-1">
					<div class="form-group mb-1">
					  <input class="form-control py-0" id="txtFirstName" type="text" placeholder="First Name *" required="required" data-validation-required-message="Please enter your first name.">
					</div>
				  </div>
				  <div class="col pl-0">
					<div class="form-group mb-1">
					  <input class="form-control py-0" id="txtLastName" type="text" placeholder="Last Name *" required="required" data-validation-required-message="Please enter your last name.">
					</div>
				  </div>
				</div>
			  </div>
			  <div class="col-md-12">
				<div class="form-group mb-1">
				  <input class="form-control py-0" id="txtEmail" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
				</div>
			  </div>
			  <div class="col-md-12">
				<div class="form-group mb-1">
				  <input class="form-control py-0" id="txtSubject" type="text" placeholder="Your Subject *" required="required" data-validation-required-message="Please enter your subject.">
				</div>
			  </div>
			  <div class="col-md-12">
				<div class="form-group mb-4">
				  <textarea class="form-control py-0" id="txtMessage" rows="2" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
				</div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-lg-12 text-center">
				<div id="success"></div>
				<?php if(($_SERVER['HTTP_HOST'] == "testing.boardspeak.com") || ($_SERVER['HTTP_HOST'] == "boardspeak.com") || ($_SERVER['HTTP_HOST'] == "beta.boardspeak.com")){ ?>
				  <button id="sendMessageButton" class="g-recaptcha btn btn-primary btn-xl text-uppercase" data-sitekey="6LeUTdQZAAAAACUwYTih2-h6aa9BacLFdC8hELW1" data-callback='onSubmit' data-action='submit' type="submit">Send Message</button>
				<?php } else { ?>
				  <button id="sendMessageButton" class="g-recaptcha btn btn-primary btn-xl text-uppercase" data-sitekey="6LebxNMZAAAAAGeYXg_sEzVUXlQtlKIZGmYVm20i" data-callback='onSubmit' data-action='submit' type="submit">Send Message</button>
				<?php } ?>
			  </div>
			</div>
		  </form>
		</div>
	  </div>
	</div>
  </section>

  

  <!-- Footer -->
  <footer class="footer text-center">
	<div class="container">
	  <div class="row">

		<!-- Footer Location -->
		<div class="col-lg-4 mb-5 mb-lg-0">
		  <h4 class="text-uppercase mb-4">Contact Info</h4>
		  <p class="lead mb-0">Sta. Rosa, Laguna
			<br>Philippines
			<br><a href="mailto:info@boardspeak.com">info@boardspeak.com</a></p>
		</div>

		<!-- Footer Social Icons -->
		<div class="col-lg-4 mb-5 mb-lg-0">
		  <h4 class="text-uppercase mb-4">Around the Web</h4>
		  <a class="btn btn-outline-light btn-social mx-1" href="#">
			<i class="fab fa-fw fa-facebook-f"></i>
		  </a>
		  <a class="btn btn-outline-light btn-social mx-1" href="#">
			<i class="fab fa-fw fa-twitter"></i>
		  </a>
		  <a class="btn btn-outline-light btn-social mx-1" href="#">
			<i class="fab fa-fw fa-linkedin-in"></i>
		  </a>
		  <a class="btn btn-outline-light btn-social mx-1" href="#">
			<i class="fab fa-fw fa-dribbble"></i>
		  </a>
		</div>

		<!-- Footer About Text -->
		<div class="col-lg-4">
		  <h4 class="text-uppercase mb-4">About BoardSpeak</h4>
		  <p class="lead mb-0">BoardSpeak helps organize your team communication. It's your sales and marketing community bulletin board with enterprise social collaboration tools.</p>
		</div>

	  </div>
	</div>
  </footer>

  <!-- Copyright Section -->
  <section class="copyright py-4 text-center text-white">
	<div class="container">
			<?php if(base_url() == 'https://boardspeak.com/' || base_url() == 'http://localhost/boardspeak_2020/'){ ?>
			 <a href="#" data-href="" data-toggle="modal" data-target="#login">Copyright &copy; BoardSpeak 2020</a>

			<?php }else{ ?>
				Copyright &copy; BoardSpeak 2020
			 <?php } ?>


	</div>
  </section>

  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
	<a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
	  <i class="fa fa-chevron-up"></i>
	</a>
  </div>

  <!-- Portfolio Modals -->

  <!-- Portfolio Modal 1 -->
  <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-labelledby="portfolioModal1Label" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
	  <div class="modal-content">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">
			<i class="fas fa-times"></i>
		  </span>
		</button>
		<div class="modal-body text-center">
		  <div class="container">
			<div class="row justify-content-center">
			  <div class="col-lg-8">
				<!-- Portfolio Modal - Title -->
				<h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">Log Cabin</h2>
				<!-- Icon Divider -->
				<div class="divider-custom">
				  <div class="divider-custom-line"></div>
				  <div class="divider-custom-icon">
					<i class="fas fa-star"></i>
				  </div>
				  <div class="divider-custom-line"></div>
				</div>
				<!-- Portfolio Modal - Image -->
				<img class="img-fluid rounded mb-5" src="img/portfolio/cabin.png" alt="">
				<!-- Portfolio Modal - Text -->
				<p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
				<button class="btn btn-primary" href="#" data-dismiss="modal">
				  <i class="fas fa-times fa-fw"></i>
				  Close Window
				</button>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>



  <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		<div class="card-signin">
		  <div class="card-body">

			<?php echo form_open('pages/view/signin'); ?>
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">
				  <i class="fas fa-times"></i>
				</span>
			  </button>
			</div>

			<div class="modal-body text-left px-3 py-0">
				<?php if (base_url() == 'https://boardspeak.com/' || base_url() == 'http://localhost/boardspeak_2020/'){
				?>
				<h1 style="font-weight: 900;
   					 color: #398cb3;" class="card-title text-center mb-4">This Login is only For Admin</h1>
				<?php }  ?>
				<h5 class="card-title text-center mb-4">Welcome, please sign in</h5>
				<p>Log-in function is currently disabled. We will send your default password to your email when the site is ready for beta testing. Thank you!</p>

			  <div class="form-label-group">
				<input type="email" id="txtEmail" name="txtEmail" class="form-control" placeholder="Email address" required autofocus>
				<label for="inputEmail">Email address</label>
			  </div>

			 <p class="mb-1"><a href="#" id="show-password">Show Password</a></p>
			  <div class="form-label-group">
				<input type="password" id="txtPassword" name="txtPassword" class="form-control" placeholder="Password" required>
				<label for="inputPassword">Password</label>
			  </div>

			<div class="d-flex justify-content-between">
					<p class="mb-1"><a href="<?php echo base_url(); ?>/pages/view/forgot">Forgot Password</a></p>
					<p class="mb-1"><a href="<?php echo base_url(); ?>/pages/view/resendemail"> Resend Activation Email</a></p>

				</div>
			  <div class="form-check mb-4">
				<input type="checkbox" class="form-check-input" id="chkRemember">
				<label class="form-check-label" for="chkRemember">Remember Me</label>
			  </div>


				<button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
				<p class="text-center mt-2">New to BoardSpeak? <a class="" href="<?php echo base_url(); ?>pages/view/signup">Sign up here</a></p>
			</div>
			</form>

		</div>
		</div>
		</div>
	</div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <!-- <script src="js/contact_me.js"></script> -->

  <!-- Custom scripts for this template -->
  <script src="js/freelancer.min.js"></script><!-- Bootstrap core JavaScript -->
  <script src="js/custom.js"></script>
  <script src="<?php echo version_url('assets/js/modules/index/index.js'); ?>"></script>
  <!-- Script for Google Recaptcha V3 -->
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <script src="<?php echo base_url(); ?>assets/noty/lib/noty.js" type="text/javascript" charset="utf-8"></script>
  <script src="<?php echo base_url(); ?>assets/js/notify.js" type="text/javascript" charset="utf-8"></script>
  <script>
   function ca(token) {
		var fname = $("input#txtFirstName").val();
		var lname = $("input#txtLastName").val();
		var email = $("input#txtEmail").val();
		var subject = $("input#txtSubject").val();
		var message = $("textarea#txtMessage").val();
			
		var url = 'https://boardspeak.com';
		var captcha = '6LeUTdQZAAAAACUwYTih2-h6aa9BacLFdC8hELW1';
		var host = window.location.hostname;
		if (host == 'localhost') {
			url = 'http://localhost/boardspeak_2020/';
			captcha = '6LebxNMZAAAAAGeYXg_sEzVUXlQtlKIZGmYVm20i';
		} else {
			url = 'https://'+host+'/';
		}
		$('#contactForm').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');	
		
   }

   function onSubmit(token) {
	return new Promise(function(resolve, reject) {  
		if (grecaptcha === undefined) {
			reject();
			Notify.error('Undefined recaptcha!', {
					afterClose : function () {
						window.location = url;
					}
				});
		}

		var response = grecaptcha.getResponse();

		if (!response) {
			reject(); 
			grecaptcha.reset();
			Notify.error('Coud not get recaptcha response!', {
					afterClose : function () {
						window.location = url;
					}
				});
		}

		var fname = $("input#txtFirstName").val();
		var lname = $("input#txtLastName").val();
		var email = $("input#txtEmail").val();
		var subject = $("input#txtSubject").val();
		var message = $("textarea#txtMessage").val();
			
		var url = 'https://boardspeak.com';
		var captcha = '6LeUTdQZAAAAACUwYTih2-h6aa9BacLFdC8hELW1';
		var host = window.location.hostname;
		if (host == 'localhost') {
			url = 'http://localhost/boardspeak_2020/';
			captcha = '6LebxNMZAAAAAGeYXg_sEzVUXlQtlKIZGmYVm20i';
		} else {
			url = 'https://'+host+'/';
		}
		$('#contactForm').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');

		$.ajax({
			url: url+'pages/save_inquiry',
			method: 'POST',
			data: {In_FName: fname, In_LName: lname, In_Email: email, In_Subject: subject, In_Message: message, token: token},
			success: function (result) {
				resolve();
				grecaptcha.reset();
				Notify.success('Thank you for reaching us!', {
					afterClose : function () {
						window.location = url;
					}
				});
			},
			error : function(request, error) {
				reject(); 
				grecaptcha.reset();
				Notify.error('Request: '+JSON.stringify(request), {
					afterClose : function () {
						window.location = url;
					}
				});
			}
		});

	});
   }

  $(document).ready( function() {

	  $("#show-password").on('click', function(event) {
		event.preventDefault();

		if($('#txtPassword').attr("type") == "text"){
			$('#txtPassword').attr('type', 'password');
			$("#show-password").html('Show Password');
		}else if($('#txtPassword').attr("type") == "password"){
			$('#txtPassword').attr('type', 'text');
			$("#show-password").html('Hide Password');
		}
	  });
	});
  </script>
</body>
</html>
