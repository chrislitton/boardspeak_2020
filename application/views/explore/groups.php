<!-- Masthead -->
<header class="dethead bg-primary text-white text-center">
    <!-- <div class="container d-flex align-items-center flex-column"> -->
    <div class="container">

		<div class="row">
	      	<div class="col-md-12 col-lg-12">
	    	  <h3 class="text-uppercase mb-0"><?php echo $title; ?> (<?php echo count($groups_items) ?>)</h3>
	    	  <!-- Icon Divider -->
		      <div class="divider-custom divider-light">
		        <div class="divider-custom-line"></div>
		      </div>
     		</div>
     	</div>

	</div>
</header>

  <section class="user-search-section" id="usersearch">
  	<div class="container">

		<div class="row">
				<div class="col-lg-12">
				<?php
					if (strlen($ParamType)==0) echo form_open('account/all/groups');
					else echo form_open('account/all/groups/'.$ParamType);
				?>
					<div class="mb-5">
						<div class="input-group">
							<input type="text" id="txtSearchGroup" name="txtSearchGroup"  value="<?php echo $SearchText; ?>" class="form-control input-box" placeholder="Search" required='required'>
							<div class="input-group-append">
								<button class="btn btn-primary btn-x" type="submit">Search</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="row">

			<?php foreach ($groups_items as $item): ?>

				<div class="col-md-6 col-lg-4 admin-img-box loadmore">
					<div class="admin-img
					<?php if(!empty($item['Gr_Thumb'])){ ?>

					<?php }else{ ?>
						bg-group

					<?php }?>
					 <?php echo $item['Gr_Backcolor']; ?>">
						<a class="board-link" href="<?php echo base_url(); ?>account/view/group/<?php echo $item['Gr_ID'];?>">
							<?php if(!empty($item['Gr_Thumb'])){ ?>
								<img class="board-link-thumbnail img-fluid" src="<?php echo base_url(). $item['Gr_Thumb']; ?>" alt="">

							<?php }else{ ?>
								<img class="board-link-thumbnail img-fluid" src=" " alt="">

							<?php }?>

							<?php if (strcasecmp($item['Gr_Privacy'], "public") != 0) : ?>
								<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image"/>
							<?php endif; ?>
						</a>

						<div class="admin-img-label shadow-sm">
							<div class="row">
								<div class="col-md-12 col-lg-12">
								<?php
									if ($item['AskForPermission']) 
										echo '<a class="board-link" href="#" data-toggle="modal" data-target="#groupAccessModal" data-id="'.$item['Gr_ID'].'">';
									else
										echo '<a class="board-link" href="'. base_url() .'account/view/group/'.$item['EncodedID'].'">';
								?>
									<!-- <a class="board-link" href="<?php echo base_url(); ?>account/view/group/<?php echo $item['Gr_ID'];?>"> -->
										<p class="board-title text-center"><?php echo $item['Gr_Name']; ?></p>
									</a>
								</div>
							</div>

							<div class="row">

								<div class="col-md-12 col-lg-12">
									<div class="btn-social-box-left">
											<p class="lead board-category"><?php echo ucfirst($item['Gr_Privacy']); ?></p>
									</div>
									<div class="btn-social-box-right">
									<?php
										if (!$item['AskForPermission'])  :
									?>
											<a class="btn admin-btn-social mx-0" href="#">
												<i class="fa fa-arrow-right mr-2"></i> Share
											</a>
									<?php
										endif;
									?>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

			<?php endforeach; ?>
			<?php $hide = (count($groups_items)<9)? 'd-none' : ''; ?>
			<div class="col-md-12 col-lg-12 mt-3 text-center <?php echo $hide; ?>">
				<a class="btn btn-primary px-5" href="#" id="btnload">Load More</a>
			</div>
		</div>
  	</div>
</section>
<input type="hidden" id="group_id" value="" />

<!-- START OF NO ACCESS TO THE GROUP MODAL -->
<div class="modal fade" id="groupAccessModal" tabindex="-1" aria-labelledby="groupAccessModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-90 m-auto">
      <div class="modal-body pt-2 pb-4">
		<div class="mt-3">
			<h6 class="modal-header-text text-center">This is a Private Group.</h6>
			<div class="text-center viewb_all">
				<button class="btn btn-lg pt-1 col-12 col-lg-8" id="btn-request-access">Request Access</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- END OF NO ACCESS TO THE GROUP MODAL -->
