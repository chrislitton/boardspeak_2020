<!-- Masthead -->
<header class="dethead bg-primary text-white text-center" style="background: #15AFE8!important;">
	<!-- <div class="container d-flex align-items-center flex-column"> -->
	<div class="container">

		<div class="row">
		  <button onclick="clickme()"   class="goingback btn btn-info"   style=" background:#06cbd1 ; border:0px;    z-index: 1; font-size: 30px;
                                                                     margin-left: 20px;
                                                                     margin-top: 7px;
                                                                     position: absolute;"><i class="fas fa-less-than "></i></button>

	      	<div class="col-md-12 col-lg-12">

				<h3 class="text-uppercase mb-0">Posts (<?php echo count($posts_items) ?>)</h3>
				<!-- Icon Divider -->
				<div class="divider-custom divider-light">
					<div class="divider-custom-line"></div>


				</div>
	 		</div>
	 	</div>
	</div>
</header>
 <style>
	 #mainnavbar{
		 display: inline-flex;
	 }
 </style>
<section class="user-search-section" id="usersearch">
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
				<?php echo form_open('account/all/posts'); ?>
					<div class="mb-5">
						<div class="input-group">
							<input type="text" id="txtSearchPost" style="width: 50%!important" name="txtSearchPost"  value="<?php echo $SearchText; ?>" class="form-control input-box" placeholder="Search" required='required'>
							<div class="input-group-append">
								<button class="btn btn-primary btn-x" style="background: #15AFE8!important; border:0px;" type="submit">Search</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="row">

			<?php

		//	echo "<pre>";
		//	print_r($posts_items);
		//	exit;
				foreach ($posts_items as $item): 

					if ($item['Searchable']) :

						$Survey = "";
						$PostLink = base_url().'account/view/post/'. $item['EncodedID'];

						if ($item['AskForPermission']) 
							$anchor_link =  '<a class="board-link" href="#" data-toggle="modal" data-target="#subtopicAccessModal" data-id="'.$item['Po_ID'].'">';
						else
							$anchor_link =  '<a class="board-link" href="'. $PostLink.'">';

			?>
						<!--<div class="col-md-6 col-lg-4 admin-img-box loadmore">
						 	<?php echo $Survey;?>
							<div class="admin-img bg-post <?php echo $item['Po_Backcolor']; ?>">
								<?php echo $anchor_link; ?>
									<img class="board-link-thumbnail img-fluid" src="<?php echo $item['Po_Thumb']; ?>" alt="">

									<?php if (strcasecmp($item['Po_Privacy'], "public") != 0) : ?>
										<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image"/>
									<?php endif; ?>
								</a>

								<div class="admin-img-label shadow-sm">
									<div class="row">
										<div class="col-md-12 col-lg-12">
											<?php echo $anchor_link; ?>
											<p class="board-title text-center"><?php echo $item['Po_Title']; ?></p>
											</a>
										</div>
									</div>

									<div class="row">

										<div class="col-md-12 col-lg-12">
											<div class="btn-social-box-left">
												<p class="lead board-category"><?php echo date( "M d, Y", strtotime($item['Po_DatePosted']) ); ?></p>
											</div>
											<div class="btn-social-box-right">
												<?php if (!$item['AskForPermission']) : ?>
													<a class="btn admin-btn-social mx-0" href="#">
														<i class="fa fa-arrow-right mr-2"></i> Share
													</a>
												<?php endif; ?>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>-->
					<div class="group-items-clone col-sm-4 col-md-4 col-lg-4" style="display: block;">
                    	<label class="makepinclass" style="

                    	display:none; background: #80808078; padding: 5px; border-radius: 6px;   position: absolute; color: white;">
                    	<span class="unpinedclass" style="font-size: 14px;">
                    	<i class="makepinclassforthumb fas fa-thumbtack">
                        	</i>
                        	Pin
                    	</span>
                    	<span class="PinnedClass"  style="font-size: 14px;"><i style="display:none; color:#616172;" class="pinclassforthumb fas fa-thumbtack"></i>
                    	Pinned</span></label>
                         <a class="board-link group-item-link" href="<?php  echo $PostLink; ?>">
                    		<div>
                    			<img style="height: 220px;
                                                border-radius: 15px;
                                                <!--box-shadow: 1px 2px 9px 4px;-->
                                                "
                                                src="<?php if(!empty($item['Po_Thumb'])){ ?>
                    			 <?php echo base_url().$item['Po_Thumb']; ?>
                    			<?php }else{ ?>
								 <?php echo base_url();?>img/filler.png
                    			<?php } ?>" class="post-thumbnail w-100">
                    			<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image" style="display:none;" />

                    		</div>
                    		<div class="row" style="margin-top:15px;">
                    		<div class='col-sm-2'>
                    		<a id="board_like"> <span class="board_likespan"><?php echo $item['Po_Likes'] ?></span> <i class="fa fa-heart" style="color:#06cbd1"></i>
                                   </a>
                    		</div>
                    		<div class="col-sm-8">
                    		    <a class="board-link group-item-link" href="<?php  echo $PostLink; ?>"><p style=" text-align: center; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" class="group-item-name"> <?php echo $item['Po_Title'] ?></p>
                    		</a>
                    		</div>

                    		</div>

                    	</a>
                    </div>
			<?php
					endif;
				endforeach; 
				$hide = (count($posts_items)<9)? 'd-none' : ''; 
			?>
			<div class="col-md-12 col-lg-12 mt-3 text-center <?php echo $hide; ?>">
				<a class="btn btn-primary px-5" href="#" id="btnload">Load More</a>
			</div>
		</div>
	</div>
</section>
<input type="hidden" id="group_id" value="" />

<!-- START OF NO ACCESS TO THE SUBTOPIC MODAL -->
<div class="modal fade" id="subtopicAccessModal" tabindex="-1" aria-labelledby="subtopicAccessModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-90 m-auto">
      <div class="modal-body pt-2 pb-4">
		<div class="mt-3">
			<h6 class="modal-header-text text-center">This is a Private Post.</h6>
			<div class="text-center viewb_all">
				<button class="btn btn-lg pt-1 col-12 col-lg-8" id="btn-request-post-access">Request Access</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- END OF NO ACCESS TO THE SUBTOPIC MODAL -->
	<script>
 				function clickme(){

 		 			window.history.back(-1);
 				}
          	</script>
