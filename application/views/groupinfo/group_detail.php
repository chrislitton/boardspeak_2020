<style>* {
	scroll-behavior: smooth;
		font-family: Arial, sans-serif;
	}
	
	.no-underline{
	  text-decoration: none;
	  color: black;
	}
	.mobile-display-none{
	  display: none;
	}
	.cursor-pointer{
	  cursor: pointer;
	}
	.mobile-selector{
	  display: none;
	  
	}
	.download-img{
	  width: 22px;
		  }
	.mobile-after-heading-content{
	  display: none;
	}
	#mobilehr{
	  display: none;
	}
	.mobile-headings{
	  display: none;
	}
	/*Scroll Bar*/
	.popup::-webkit-scrollbar {
	  width: 5px;
	 
	}
	
	/* Track */
	.popup::-webkit-scrollbar-track {
	  background:white;
	 border-radius: 50px;
	 margin-top: 20px;
	}
	
	/* Handle */
	.popup::-webkit-scrollbar-thumb {
	 /* background: #00d6d6; */
	 background: #388cb3;
	 border-radius: 10px;
	
	}
	  
	  .button {
		font-size: 1em;
		padding: 10px;
		color: black;
		border: 2px solid #388cb3;
		/* border: 2px solid #06D85F; */
		border-radius: 20px/50px;
		text-decoration: none;
		cursor: pointer;
		transition: all 0.3s ease-out;
	  }
	  .button:hover {
		background: #06D85F;
	  }
	  
	  .overlay {
		position: fixed;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background: rgba(0, 0, 0, 0.7);
		transition: opacity 500ms;
		/* visibility: hidden;
		opacity: 0; */
	  }
	  .overlay:target {
		/* visibility: visible;
		opacity: 1; */
	  }
	  
	  .popup {
		margin: 3% auto;
		padding: 20px;
		background: #fff;
		border-radius: 5px;
	   overflow: auto;
		position: relative;
		width: 65%;
		height: 100%;
		
		border: 3px solid #00d6d6;
		border-radius: 24px;
		transition: all 5s ease-in-out;
	  }
	  #popup1{
	   opacity: 0;
	   visibility: hidden;
	  }
	  
	  .first-popup-h2{
		display: flex;
		justify-content: space-between;
		margin-top: 0;
		color: #2bd3f8;
		font-family: Tahoma, Arial, sans-serif;
	  }
	  .popup .close {
		position: absolute;
		top: 0px;
		right: 12px;
		transition: all 200ms;
		font-size: 30px;
		font-weight: bold;
		text-decoration: none;
		color: #333;
	  }
	  .popup .close:hover {
		color: #00d6d6;
	  }
	  .popup .content {
		/* display: none; */
		height: 120%;
		
	  }
	  .content-close-btn{
		transition: all 200ms;
		margin-left: 22px;
		cursor: pointer;
	  }
	  .content-close-btn:hover{
		color:#388cb3;
		/* color: #00d6d6; */
	  }
	  .full-search-sec{
		width: 100%;
	 
		display: flex;
	 justify-content: space-between;
	 align-items: center;
	 flex-wrap: wrap;
	   
	  }
	  .left-search-sec{
		display: flex;
		justify-content: space-between;
		align-items: center;
		width: 40%;
	  }
	  .left-search-content{
		display: flex;
		background-color:transparent;
		/* background-color: rgb(239 232 232); */
		width: 100%;
		justify-content:space-between;
		flex-direction: row-reverse;
		border: 1px solid #388cb3;
		border-radius: 0.25rem;
		max-height: 36px;
	
	}
	  .left-search-content i{
	   
	 
		padding: 11px;
		padding-right: 10px;
		padding-left: 10px;
	
	   border-radius: 0.25rem;
		color: white;
		background-color: #388cb3;
		cursor: pointer;
	
	  }
	  .left-search-sec input{
		display: flex;
		width: 100%;
		border-radius: 0.25rem;
		outline: none;
		border:none;
		background: transparent;
		margin-left: 3%;
	}
	
	  
	  .right-search-sec{
		display: flex;
		justify-content: space-between;
		align-items: center;
		margin-right: 62px;
		
	  }
	  .right-search-content{
		display: flex;
		justify-content: right;
		align-items: center;
		margin-right: 19%;
		
	  }
	  .export-btn{
		display: flex;
		justify-content: center;
		align-items: center;
		padding-right: 20px;
		background-color: transparent;
		color: #388cb3;
		border: 1px solid #388cb3;
		transition: all 0.5s ease;
		border-radius: 6px;
		padding: 2px;
		padding-right: 12px;
		padding-left: 8px;
		margin-left: 38px;
	  }
	.export-btn:hover{
	  /* box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.4); */
	background: #388cb3;
	color: white;
	}
	.reload-btn{
	  display: flex;
	  align-items: center;
	  background: transparent;
	  justify-content: center;
	  border: 1px solid #388cb3;
	  border-radius: 6px;
	  margin-right: 13px;
	color: #388cb3;
	  font-weight: 600;
	  cursor: pointer;
	}
	.reload-btn:hover{
		background-color: #388cb3;
		color: white;
	}
	  #reload-img{
		width: 30px;
		margin-right: 3%;
		font-size: 24px;
	
		cursor: pointer;
		/* margin-right: 11px; */
		/* display: none; */
	}

	  #white-download-img{
		display: none;
	  }
	.export-btn:hover #white-download-img{
	  display: block;
	}
	.export-btn:hover .download-img{
	  display: none;
	}
	  .all-headings{
		margin-top: 3%;
	display: flex;
	justify-content: space-evenly;
	width: 100%;
	/* color:  #00d6d6; */
	color:  #388cb3;
	font-weight: 600;
	font-size: 12px;
	  }
	  .col1,.col2,.col3,.col4,.col5,.col6,.col7,.col8{
		width: 12.5%;
		display: flex;
		align-items: center;
		justify-content: center;
	
	  }
	  .after-heading-content{
		
		font-size: 13px;
		
		display: flex;
		flex-direction: column;
	
	  }
	  .row1,.row2,.row3,.row4,.row5,.row6{
		height: 14%;
		display: flex;
		width: 100%;
	justify-content: space-evenly;
	margin-top: 4%;
	  }
	  .after-heading-content img{
		width:40% ;
		border-radius: 50%;
	  }
	  
	  #after-headings-hr{
		border: 1px solid  #00d6d6;
	  }
	  .pink-color{
		color: #ec92a2;
		
	  }
	  .coins-img{
		width: 20%;
	  }
	.page-picker{
	  
	  width: 42%;
	  margin: auto;
	  display: flex;
	  justify-content: space-evenly;
	}
	.page-picker img{
	  width: 4%;
	  cursor:pointer;
	}
	.page-picker-icons{
	  width: 12%;
	  display: flex;
	}
	.page-picker-icons img{
	  width: 23%;
	}
	
	#left-picker-span{
	  display: flex;
	}
	/*DropDown*/
	 .dropbtn{
	
	  
	  width: 224%;
	  border-radius: 0.25rem;
	
	  background: transparent;
	  border: 1px solid #388cb3;
	  color: #388cb3;
	  
	  padding: 5px;
	  padding-right: 7px;
	  padding-left: 8px;
	}
	.dropbtn:hover{
	background-color: #388cb3;
	color: white;
	
	}
	
	.dropdown {
	
	  display: inline-block;
	  width: min-content;
	  margin-right: 10%;
	}
	.dropdown img{
	  width: 10%;
	}
	
	.dropdown-content {
	  display: none;
	  position: absolute;
	  background-color: #ffff;
	  
	  /* box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); */
	  z-index: 1;
	}
	
	.dropdown-content a {
	  color: black;
	  padding: 12px 16px;
	  text-decoration: none;
	  display: block;
	  
	  border-radius: 0.25rem
	}
	.dropdown-content a:hover{
	  
	  background-color: #ddd;
	}
	
	.dropdown:hover .dropdown-content{display: block;}
	
	
	
	.reward-header{
	  display: flex;
	  justify-content: space-between;
	}
	.history-button{
	  position: relative;
	  right: 10%;
	  padding: 10px;
	  padding-left: 30px;
	  padding-right: 30px;
	  border-radius: 10px;
	  border: none;
	  background-color: #2bd3f8;
	  color: white;
	  transition: 0.2s all;
	  font-size: 1.2rem;
	  text-decoration: none;
	  align-items: center;
	
		display: flex;
	}
	.history-button:hover{
	  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.4);
	  transform: scale(1.04);
	  cursor: pointer;
	}
	  
	  @media screen and (max-width: 1000px){
		.underline-reward{
		  color: #388cb3;
		  text-decoration: underline;
		  text-decoration-color:#388cb3;
		  text-underline-offset: 2px;
		  text-decoration-thickness: 2px;
		}
		.row1,.row2,.row3,.row4,.row5,.row6{
		  width: 100%;
		}
		.mobile-rows{
		  width: 200%;
		}
		#mobilehr{
		  display: block;
		}
	
		.box{
		  width: 70%;
		}
		.popup{
		  width: 70%;
		  height: 50%;
		  overflow-x: hidden;
		}
		.after-heading-content{
		  display: none;
		}
		.all-headings{
		  display: none;
		}
		.mobile-selector{
		  display: flex;
		  justify-content: space-evenly;
		  font-size: 17px;
		  margin-top: 4%;
		}
		#after-headings-hr{
		  display: none;
		}
		.mobile-headings{
		  margin-top: 3%;
		  display: flex;
		  justify-content: space-evenly;
		  width: 100%;
		  color:  #388cb3;
		  /* color:  #00d6d6; */
		  font-weight: 600;
		  font-size: 12px;
		  
		}
		.all-elm .mobile-headings{
		  margin-top: 3%;
		  display: flex;
		  justify-content: space-evenly;
		  width: 200%;
		  color:  #388cb3;
		  /* color:  #00d6d6; */
		  font-weight: 600;
		  font-size: 12px;
		  transition: 1s all;
		  transform: translateX(1px);
		}
		.mobile-after-heading-content{
		  height: 100%;
		  font-size: 13px;
		  transition: 1s all;
		  display: flex;
		  flex-direction: column;
		  transform: translateX(1px);
		  
		}
		
		.users-img{
		  width:67px ;
		  border-radius: 50%;
		}
		
		#popup1#all .m-all-selected{
		  text-decoration: underline;
		}
	   
		.unclaimed-flex{
		  display: none;
		}
		.mobile-navigation-icons{
		  position: absolute;
		  width: 97.1%;
		  display: flex;
		  justify-content:right;
		
		  bottom: 52%;
		  right: 8%;
		}
		.mobile-navigation-icons-sec{
		  position: absolute;
		  width: 100%;
		  left: 100%;
		  display: flex;
		  justify-content: right;
		  bottom: 50%;
		}
		.mobile-navigation-icons img{
		  transform: scale(0.8);
		  /* position: fixed; */
		  position: sticky;
		}
		.mobile-navigation-icons:before{
		  content: '';
		  display: inline-block;
		  width: 30px;
		  height: 30px;
		  border-radius: 15px;
		  background-color: white;
		  position: absolute;
		  top: -6px;
		right: -9px;
		box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
	
		}
		.mobile-navigation-icons-sec:before{
		  content: '';
		  display: inline-block;
		  width: 30px;
		  height: 30px;
		  border-radius: 15px;
		  background-color: white;
		  position: absolute;
		  top: -6px;
		right: -9px;
		box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
	
		}
		.mobile-navigation-icons-sec img{
		  transform: scale(0.8);
		  /* position: fixed; */
		
		}
	   
	  
	  }
	@media screen and (max-width:825px) {
	  .full-search-sec{
	flex-direction: column;
	justify-content: center;
	  }
	  .left-search-sec {
	  width: 90%;
	  }
	  .left-search-content{
		max-height: 30px;
	  }
	  .left-search-content i{
		padding-top: 7px;
		padding-right: 7px;
		padding-left: 7px;
	  }
	  .right-search-sec {
	
		margin-top: 13px;
	  }
	  .right-search-content {
	justify-content: center;
	margin-left: 10%;
	
	  }
	  
	}
	  @media screen and (max-width:631px) {
		.mobile-selector a{
		 font-size: 14px;
		}
		.page-picker{
		  width: 100%;
		}
	   
		.users-img{
		  width:47px ;
		
		}
	   
		.popup{
		  width: 90%;
		}
	
	
		.all-elm .mobile-headings{
		  width: 212%;
		}
		.mobile-navigation-icons img{
		  transform: scale(0.7);
		}
		.mobile-navigation-icons-sec img{
		  transform: scale(0.7);
		}
	  }
	
	  @media only screen and (max-width:425px) {
	
		.users-img{
		  width:37px ;
		
		}
		.mobile-navigation-icons img{
		  transform: scale(0.6);
		}
		.mobile-navigation-icons-sec img{
		  transform: scale(0.6);
		}
	   .mobile-col-edit{
		position: relative;
		left: 4%;
	   }
	
		
	  }
	  @media only screen and (max-width:472px){
		.first-popup-h2{
		  font-size: 1.4rem;
		font-weight: 500;
		width: 50%;
		color: #36c7fc;}
		.header{
		  align-items: center;
		}
		.popup .close{
		  top: -6px;
		  right: 4px;
		}
	.reward-header{
	  display: flex;
	  justify-content: space-between;
	
	
	  position: relative;
	  bottom: -17px;
	}
	.history-button{
	  height: 26px;
	}
	.full-search-sec{
	  flex-wrap: wrap;
	  justify-content: center;
	}
	.left-search-sec{
	  width: 95%;
	}
	.right-search-sec{
	  margin-top: 14px;
	}
	  }
</style>



<style>
	.graphs{
		width: 100%;
		height: 100%;
		display: flex;
		/* justify-content: space-between; */
		align-items: center;
	   
	}
	
	.pie-chart{
	/* transform: scale(2); */
	width: 60%;
	min-height: 400px;
	
	}
	.column-chart{
		width: 50%;
	z-index: 333;
		height: 50%;
	}
	
	
	  /* CARDS */
	  
	  .cards {
		display: flex;
		flex-wrap: nowrap;
		justify-content: space-evenly;
		/* max-height: 100px; */
		
	  }
	  
	  .card {
		margin: 10px;
		color: white;
		padding: 10px;
		width: 50%;
		
		/* display: grid;
		grid-template-rows: 20px 50px 1fr 50px; */
		border-radius: 10px;
		box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.25);
		transition: all 0.2s;
	   
		max-height: 135px;
	  }
	  
	  .card:hover {
		box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.4);
		transform: scale(1.01);
	  }
	  .cards-head{
		display: flex;
		justify-content: space-between;
	
	  }
	.card-bars{
	width: 20%;
	
	display: flex;
	align-items: center;
	justify-content: flex-end;
	}
	.card-bar{
	background-color: white;
	z-index: 5;
	height: 50%;
	width: 50%;
	margin: 3%;
	}
	.bar-1{
	height: 20%;
	}
	.bar-2{
		height: 40%;
	}
	.bar-3{
		height: 30%;
	}
	
	
	  
	  .card__title {
	font-size: 1.3rem;
		color: #ffffff;
	  }
	  
	  .card__apply {
	display: flex;
		align-items: center;
	
	  }
	  .card__apply p{
	margin-left: 1%;
	  }
	  
	  /* CARD BACKGROUNDS */
	  
	  .card-1 {
		background: radial-gradient(#1fe4f5, #3fbafe);
	  }
	  
	  .card-2 {
		background: radial-gradient(#fbc1cc, #fa99b2);
	  }
	  
	  .card-3 {
		background: radial-gradient(#76b2fe, #b69efe);
	  }
	  @media screen and (max-width:1265px){
		.graphs{
		 display: block;
		 margin-top: 5%;
		}
		.pie-chart{
		  /* transform: scale(2); */
		  width: 70%;
		  min-height: 400px;
		  margin: auto;
		  }
		  .column-chart{
			  width: 50%;
		  z-index: 333;
			  height: 50%;
			  margin: auto;
		  }
		.cards{
			/* flex-wrap: wrap; */
		}
	  }
	  @media screen and (max-width:924px){
		.graphs{
		  justify-content: center;
		}
	  
		.pie-chart{
	
		  width: 100%;
		  min-height: 400px;
		  margin: auto;
		  }
		  .column-chart{
			/* transform: scale(0.8); */
			  width: 100%;
		  z-index: 333;
			  height: 50%;
			  margin: auto;
		  }
		.card-bars{display: none;}
	  }
	  @media screen and (max-width:800px) {
		.card__apply{
		  position: relative;
		  top: -29px;
		}
		.card__apply img{
		  margin-left: 3%;
		}
		.card__apply p {
		  margin-left: 4px;
		}
		.card hr{
		  position: relative;
	  top: -16px;
	
		}
	  }
	  @media screen and (max-width:535px) {
		
		.pie-chart{
		  width: 130%;
	  
		
		  }
		  .column-chart{
		 
			  /* width: 300px;
			  height: 300px; */
		  }
	  }
	  @media screen and (max-width:419px){
		.pie-chart{
		  width: 140%;
	  
		
		  }
		  .card{
			margin: 5px;
			color: white;
			max-height: 130px;
			/* padding: 2px; */
	
		  }
		  /* .card hr{
			position: relative;
		top: -16px;
	
		  } */
		  /* .card__apply{
			position: relative;
			top: -29px;
		  }
		  .card__apply img{
			margin-left: 3%;
		  }
		  .card__apply p {
			margin-left: 4px;
		  } */
		  .graphs{
			margin-top: 17%;
		  }
		  .card {padding-top: 0;}
	  }
	  @media screen and (max-width:385px){
		.pie-chart{
	width: 555px;
	transform: scale(0.8);
	position: relative;
	right: 100px
		  }
	  
	  }
	  @media screen and (max-width:370px) {
		.card{margin: 0;}
	  }
</style>
		<section class="group_info" >
			<form class="div_group_info_container container">
				<!-- tab nave -->
				<div class="grup_info_nav">
					<ul>
						<li class="nav-item">
							<a id="group_info_tab" class="nav-link active">
								Group Info
							</a>
						</li>

 						<?php
					    if( $member_info['role_status'] != 'pending'){
 						 if(  $member_info['role'] == 'admin' ||  $member_info['role'] == 'member' || $member_info['role'] == 'superadmin'  ){   ?>
						<li class="nav-item">
							<a id="member_tab" class="nav-link">
								Members
							</a>
						</li>
						<?php } ?>
						<?php   if( $member_info['role'] == 'superadmin'   ){  ?>
						<li class="nav-item">
							<a id="managed_tab" class="nav-link">
								Manage Users
							</a>
						</li>
							<li class="nav-item">
								<a id="reward_tab" class="nav-link">
									Reward
								</a>
							</li>
						<?php }
						} ?>
					</ul>
				</div>

				<div id="group_info_tab-container" class="nav-container">
					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Category</label>
						<div class="admin-form-group controls pb-1">
							<div class="input-group">
								<select class="custom-select" id="selCategory" name="selCategory" required="required" disabled="disabled">
									<option value="">Choose category</option>
									<?php foreach ($category_items as $item): ?>
										<option value="<?php echo $item['Ca_ID'];?>" <?php if (strcmp($groups_items['Gr_Ca_ID'],$item['Ca_ID'])==0) echo 'selected';?>><?php echo $item['Ca_Name'];?> </option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($groups_items['Gr_Ca_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
								</select>
							</div>
						</div>
					</div>

					<div class="control-group mb-2 form-group" id="selNewCategoryBox" style="<?php if (strcmp($groups_items['Gr_Ca_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">
							<input class="form-control" id="txtNewCategory" name="txtNewCategory" type="text" value="<?php echo $groups_items['Gr_Ca_Name'];?>" placeholder="Enter new category" disabled="disabled">
						</div>
					</div>


					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Sub-Category</label>
						<div class="admin-form-group controls pb-1">
							<div class="input-group">
								<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required" disabled="disabled">
									<option value="">Choose sub-category</option>
									<?php foreach ($subcategory_items as $item): ?>
									<option value="<?php echo $item['Sc_ID'];?>" <?php if (strcmp($groups_items['Gr_Sc_ID'],$item['Sc_ID'])==0) echo 'selected';?>><?php echo $item['Sc_Name'];?></option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($groups_items['Gr_Sc_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
								</select>
							</div>
						</div>
					</div>
												
					<div class="control-group mb-2 form-group" id="selNewSubCategoryBox" style="<?php if (strcmp($groups_items['Gr_Sc_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">
							<input class="form-control" id="txtNewSubCategory" name="txtNewSubCategory" type="text" value="<?php echo $groups_items['Gr_Sc_Name'];?>" placeholder="Enter new sub-category" disabled="disabled">
						</div>
					</div>
												
					<div class="control-group mb-2 form-group">
						<div class="admin-form-group controls pb-1">
							<label class="font-weight-bold label-h6">Group</label>
							<input class="form-control" id="txtTitle" name="txtTitle" type="text" value="<?php echo $groups_items['Gr_Name'];?>" placeholder="Enter group name"  disabled="disabled">
						</div>
					</div>

								
					<div class="control-group mb-4 form-group">
						<div class="admin-form-group controls pb-1">
							<label class="font-weight-bold label-h6">Description</label>
							<input class="form-control" id="txtDescription" name="txtDescription" type="text" value="<?php echo $groups_items['Gr_Description'];?>" placeholder="Enter group's mission"  disabled="disabled">
						</div>
					</div>
					<div class="control-group mb-4 form-group">
						<div class="admin-form-group controls pb-1">
							<label class="font-weight-bold label-h6">Group Package</label>

							<input class="form-control"    type="text" value="<?php

							if($groups_items['Gr_packegtype'] == 1){
								echo 'Free Plan';
							}else if($groups_items['Gr_packegtype'] == 2){
								echo 'Plus Plan';
							}else if($groups_items['Gr_packegtype'] == 3){
								echo 'Pro Plan';
							} ?>
						"  disabled="disabled">

						</div>
					</div>
					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Keyword Tags</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtKeywords" name="txtKeywords" type="text" value="<?=$groups_items['Gr_Keywords'];?>" disabled>
						</div>
					</div>

					<div class="control-group mb-4 form-group">
						<div class="admin-form-group controls mb-0 pb-1 radio_btn_group">
							<label class="font-weight-bold label-h6">Privacy</label>

							<!-- Default unchecked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public" disabled="disabled" <?php if (strcmp($groups_items['Gr_Privacy'],'public')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPublic">Public </label><br>Anyone can find, follow and join in your group
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" disabled="disabled" <?php if (strcmp($groups_items['Gr_Privacy'],'private')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPrivate">Private </label><br>Anyone can find your group. But admin permission but admin permission is required to follow and join in your group.
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" disabled="disabled" <?php if (strcmp($groups_items['Gr_Privacy'],'secret')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radSecret">Secret </label><br>Only those with link and unique password can find & join your group.
							</div>
					<?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>
									<div class="admin-form-group">
										<a href="<?php echo base_url(); ?>account/view/group/<?php echo $EncodedID;?>" class="btn btn-xl btn-light mr-2">Cancel</a>
										<a style="background: white;
													  color: #398cb3;" href="<?php echo base_url(); ?>account/groupinfo/edit/<?php echo $EncodedID;?>" class="btn btn-xl btn-primary mr-2">Edit Group Info</a>
										<a href="#" id="deletegroup" style="display: none;" data-href="<?php echo base_url(); ?>account/groupinfo/delete/<?php echo $EncodedID;?>" class="btn btn-xl btn-danger"  data-toggle="modal" data-target="#confirm-delete">Delete Group</a>

										<a href="#" data-href="<?php echo base_url(); ?>account/groupinfo/delete/<?php echo $EncodedID;?>" class="btn btn-xl btn-danger"  data-toggle="modal" data-target="#myModaldelete">Delete Group</a>
									</div>
									<?php } ?>
						</div>
					</div>



					<div class="form-group member_roles">
					<?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>
							<!--<h6 id="member_count">
							<span>1 Member and 2 Admins</span>
							<span>
								<a class="invite_member_btn">
									Add
								</a>
							</span>
						<span>
								<a class="user_role_btn">
									Manage Users
								</a>
							</span>
							<span>
								<a class="role_permission_btn">
									Permissions
								</a>
							</span>
						</h6>-->
						<div class="members_list" style="display:none;">
							<div class="members_img">
								<img  src="src/image/use
						r_profile.jpeg" alt="member">
							</div>
							<p>
								<span>Marry</span> is a member.
							</p>
						</div>
						<div class="admin_list" style="display:none;">
							<div class="admin_img">
								<img  src="src/image/user_profile.jpeg" alt="member">
								<img  src="src/image/user_background.jpg" alt="member">
							</div>
							<p>
								<span>Shushant</span> and <span>Jamal</span> are admin.
							</p>
						</div>
					</div>
					<?php }	 ?>
				</div>


            </div>
			</form>
			<div id="manage_user_container" class="nav-container" style="display: none;">

				<div class="modal_item user_role">
					<h6>
						Manage user roles
					</h6>
					<div class="search_member form-group">
						<input class="form-control" type="text" id="user_filter">
						<button class="btn btn-info" type="button" id="btnUserSearch">
							search
						</button>
					</div>
					<ul class="user_role_list">
						<li>
							<a disabled>Roles <i class="fas fa-filter"></i></a>
						</li>
						<li>
							<a class="role_option active" id="tab_all_members" role_tab_id="all_members">All</a>
						</li>
						<li>
							<a class="role_option" id="tab_super_admin" role_tab_id="super_admin">Super Admin</a>
						</li>
						<li>
							<a class="role_option" id="tab_admin" role_tab_id="admin">Admin</a>
						</li>
						<li>
							<a class="role_option" id="tab_member" role_tab_id="member">Members</a>
						</li>
						<li>
							<a class="role_option" id="tab_follower" role_tab_id="follower">Followers</a>
						</li>
						<li>
							<a class="role_option" id="tab_pending" role_tab_id="pending">Join Requests / Invites</a>
						</li>
					</ul>
					<div class="scroll_body">
						<div class="suggest_member_tabs">
							<ul id="all_members" class="user_list suggest_members"></ul>
							<ul id="super_admin" class="user_list suggest_members"></ul>
							<ul id="admin" class="user_list suggest_members"></ul>
							<ul id="member" class="user_list suggest_members"></ul>
							<ul id="follower" class="user_list suggest_members"></ul>
							<ul id="pending" class="user_list suggest_members"></ul>
							<ul id="no_role" class="user_list suggest_members"></ul>
						</div>
					</div>
				</div>

			</div>
			<div id="member_tab-container" class="nav-container" style="display: none;">
				<div id="member_list" class="not_tab member_list">
					<div class="member_list_container">
						<?php


						foreach ($members_items as $item_m):?>
							<div class="member_item">
								<div class="member_img">
									<a href='<?php
									$break = "u/";
									echo base_url()?><?php echo $break; ?><?php echo encode_id($item_m['Me_Us_ID']) ?>' >
										<img src="<?php
										if(!empty($item_m['Us_Photo'])){
											echo $item_m['Us_Photo'];
										}else{

										 echo base_url() . 'img/nophoto.png';
										}

										?>" alt="">
									</a>
								</div>

								<div class="member_text">
									<h4>
											<span>
												<?php ?>
												<?php if($item_m['Us_Alias'] != ''){
													echo  $item_m['Us_Alias'] ;
												}else{
													echo $item_m['Us_Name'] ;
												}


												?>
											</span>

									</h4>
									<?php  if($item_m['Me_Us_ID'] ==
												  $this->LoggedInUser && $item_m['Me_Role'] != 'superadmin'){ ?>

											<?php
										if($groups_items['Gr_Us_ID']  == $item_m['Me_Us_ID'] ){ ?>


									<?php 	}else{?>
											<button onclick="leavegroup()" style="padding: 0px 10px;" class="btn btn-outline-primary">Leave group</button>

										<?php 	}

										?>
										<?php
									} ?>
									<p>
										<?php echo date( "F d, Y h:i", strtotime($item_m['Us_DateTime']) ); ?>
									</p>
									<p style="color:blue;" >

										<?php
										if($item_m['MutualFriend'] > 0){
											echo  $item_m['MutualFriend']." Mutual Contact" ;
										}
										?>
									</p>
								</div>
								<div class="row">
									<h4>
											<span class="member_badge" style="font-size:14px;">
													  <?php echo  $item_m['Me_Status']  ?>
											</span>

									</h4>
									<p>
										<?php echo date( "F d, Y h:i", strtotime($item_m['Me_DatePosted']) ); ?>
									</p>
								</div>
								<div class="member_control">
									<p class="member_badge">
										<?php
										if($groups_items['Gr_Us_ID']  == $item_m['Me_Us_ID'] ){

											echo 'Creator';
										}else{
											echo  $item_m['Me_Role'];
										}

										 ?>
									</p>
								</div>

							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<div id="reward_tab_container" class="nav-container" style="display: none;">
				<ul class="nav  ">
					<li class="active show"><a data-toggle="tab" href="#home" class="active">Reward Voucher</a></li>

					<li><a data-toggle="tab" href="#menu2">Vouchers for sale</a></li>
				</ul>

				<div class="tab-content" style="    margin-top: 16px;">
					<div id="home" class="tab-pane fade in active show">
						<div class="col-sm-12 row">

						<?php

						foreach($voucherofpost as $voucher){

							$totalavaile = $voucher['totalvoucherused'] +  $voucher['totalvoucherpurchased'] + $voucher['totalvoucheractive'];
							$reaninig = $voucher['Vp_vounchercount'] - $totalavaile;

							?>
 							 <div class="col-sm-4  " style=" margin:20px; border: 1px solid grey; border-radius: 10px">
								<img id="imageofcoucher<?php echo $voucher['Vp_post_id'] ?>" style="width: 100%;   margin-top: 10px; margin-bottom: 10px; border-right: 10px; height: 250px;" src="<?php echo base_url().$voucher['Po_Photo'] ?>">
								<h6  ><?php    echo $voucher['Vp_title']; ?></h6>
								 <span><b>Earn <?php echo $voucher['Vp_radeemprice'] ?> Coins</b></span>
								 <h6><b>Term & condition:</b></h6>
								 <p style="    font-weight: 100;
									min-height: 20px;
									max-height: 100px;
									overflow: auto;
									color: grey;"><?php  echo $voucher['Vp_decription']; ?></p>
								 <p><b><?php echo $reaninig ?> </b>available offer of  <b><?php echo $voucher['Vp_radeemprice'] ?></b>  coins</p>
								 <p> <b><?php echo $totalavaile ?> </b> availed offer of  <b><?php echo $voucher['Vp_radeemprice'] ?></b>  coins</p>

								 <span>
							 </span>
						 	<div class="d-flex justify-content-around">
							<div>
				 <p style="font-weight: 300;">Expiration :</br> <?php echo  $voucher['Vp_deadline']; ?></p>
								<label  style="background:

								<?php
								if(date('Y-m-d') >= $voucher['Vp_deadline'] ){
									echo 'red';
								}else{
									echo '#15AFE8';
								}
								?>

								 ;
						padding: 5px;
						border-radius: 4px;
						color:


						 white;

"><?php
					 if(date('Y-m-d') >= $voucher['Vp_deadline'] ){
						 	echo 'Expired';
							}else{
						    echo $voucher['Vp_Status'];
							}
								 ?></label>
							</div>
								<div>
									<?php
									if(date('Y-m-d') > $voucher['Vp_deadline'] ){ ?>
										<button class="btn btn-primary mt-2 mb-3" style="       margin: 4px!important; width: 100%;
    padding: 0px;"  >Expired</button>
								 <?php 	}else{ ?>
										<button class="btn btn-primary mt-2 mb-3" style="       margin: 4px!important; width: 100%;
    padding: 0px;" onclick="validatevoucher('<?php echo $voucher['Vp_post_id'] ?>')">Validate</button>
									<?php }?>

									<a  href="#popup1" class="button btn btn-info mt-2 mb-3" style="       margin: 4px!important; width: 100%;
    padding: 0px;"  >More detail</a>

								</div>
							</div>
						 	</div>
						<?php	} ?>
					</div>
					</div>
				  	<div id="menu2" class="tab-pane fade">
						<h3>Menu 2</h3>
						<p>Some content in menu 2.</p>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php $this->load->view('templates/reward_detail_popup.php'); ?>

<input type="hidden" name="GroupID" value="<?=$EncodedID?>" />

		<div id="validatevouchermodal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<input  type="hidden" id="postid" >
					<img id="imageshow" style="height: 300px; border-radius: 10px 10px 0px 0px;" src="">

							<h6 align="center" style="color: red" class="mt-3"> Validate Voucher Code </h6>
								<div class="d-flex justify-content-around">
									<div class="input-group mb-3" style="    padding: 0px 20%;">
										<input  id="vouchercode" style="    width: 79%;" type="text" class="form-control" placeholder="Enter Voucher Code">
										<div class="input-group-append">
											<span class="input-group-text" onclick="verifiedvoucher()">></span>
										</div>
									</div>
								</div>

								<h6 align="center" id="response"></h6>

								<div id="useddiv"  style="  display: none;  padding: 0px 20%;">
									<form onsubmit="return  makeitused()">
										<div class="form-group">
											<label>Branch Code/Name <font style="color:red;">*</font></label>
											<input type="text" required class="form-control" id="branchcode">
										</div>
										<div class="form-group">
											<label>Merchant Name <font style="color:red;">*</font></label>
											<input type="text"  required class="form-control" id="murchentname">
										</div>
										<div class="d-flex justify-content-center">
											<button style="width: 100%;
										font-size: 16px;
										padding: 0px;
										margin-bottom: 15px;" type="submit" class="btn btn-success"  >Already Availed</button>
										</div>

									</form>
								</div>
								<div>
									<h6 align="center" class="mt-3"> <a href="javascript:void(0)"  data-toggle="collapse"  data-target="#demo">Voucher Information</a>
									</h6>

									<div id="demo" style="    padding: 0px 15%;" class="collapse">
											<div class="form-group">
										  <label>Full Name:</label>

											 <input type="text" class="form-control" readonly id="userfullname" value=" ">
											</div>
										<div class="form-group">
											<label>Email:</label>
											<input type="email" class="form-control" readonly id="useremail"  value=" ">
										</div>
										<div class="form-group">
											<label>Phone:</label>
											<input type="number" class="form-control"  readonly id="userphone"  value=" ">
										</div>
										<div class="form-group">
											<label>Branch Code:</label>
											<input type="text" class="form-control" readonly id="userbranch"  value=" ">
										</div>
										<div class="form-group">
											<label>Used Date:</label>
											<input type="text" class="form-control" readonly id="usedDate"  value=" ">
										</div>

									</div>
								</div>

				</div>
			</div>
		</div>

<?php $this->load->view('account/group_users_pop_up'); ?>
<script>
	function getvoucherdetail(){
		if($('#vouchercode').val() == ''){
			return;
		}
		$.ajax({
			url: base_url + 'account/getvoucherdetail/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: $('#postid').val(),
				vouchercode: $('#vouchercode').val(),


			},
			success: function (response) {


				$('#userfullname').val(response[0]['Us_Name']);
				$('#useremail').val(response[0]['Us_Email']);
				$('#userphone').val(response[0]['Us_Phone']);
				$('#userbranch').val(response[0]['branchcode']);
				$('#usedDate').val(response[0]['usedDate'])


			}
		});
	}
	function makeitused(){

 	$.ajax({
			url: base_url + 'account/makevoucherused/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: $('#postid').val(),
				vouchercode: $('#vouchercode').val(),
				murchentname: $('#murchentname').val(),
				branchcode: $('#branchcode').val(),

			},
			success: function (response) {
				$response =   response;
				if($response.action == 'success'){
					$('#useddiv').hide();
				}

				Notify.success($response.message, {
					afterClose : function () {

						getvoucherdetail();
					}
				});
				return false;
			}
		});
		return false;
	}
	function VoucherInformation(){

	}
	function validatevoucher(voucherid){
			$('#postid').val(voucherid);
		  $('#imageshow').attr('src' , $('#imageofcoucher'+voucherid).attr('src'));
			$('#validatevouchermodal').modal('show')



	}
	function verifiedvoucher(){


		$.ajax({
			url: base_url + 'account/getvoucherverifiaction/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: $('#postid').val(),
				vouchercode: $('#vouchercode').val(),

			},
			success: function (response) {
			$('#useddiv').hide();
				 console.log(response);

				 if(response.length == 0){
					 $('#response').text('No Voucher Found');
					 $('#response').css('color' , 'red')
					 $('#userfullname').val('No Record Found');
					 $('#useremail').val('No Record Found');
					 $('#userphone').val('No Record Found');
					 $('#userbranch').val('No Record Found');
					 $('#usedDate').val('No Record Found')
				 }else{
					 if(response[0].Vr_Status == 'active'){
						 $('#response').text('Not Yet Redeemed');
						 $('#response').css('color' , 'grey')
					 }else if(response[0].Vr_Status == 'purchased'){
						 $('#response').text('Voucher is Valid');
						 $('#useddiv').show();
						 $('#response').css('color' , 'green')
					 }else if(response[0].Vr_Status == 'used'){
						 $('#response').text('Voucher is Already Used ,  Thank');
						 $('#response').css('color' , 'red')
					 }
					 getvoucherdetail();
				 }
			}
		});
	}
</script>
<style type="text/css">
	.bootstrap-tagsinput { 
	    border: none !important;
	    box-shadow: none !important;
	    background-color: transparent !important;
	    padding: 0 !important;
	}
	div#reward_tab_container ul li a{
		padding: 10px;
		margin-bottom: 16px!important;
		font-size: 20px;
	}

	div#reward_tab_container ul li a.active {
		/* background: #0000ff2e; */
		border-bottom: 1px solid #3dafe8;
	}
	.bootstrap-tagsinput span[data-role="remove"] {
	    display:none !important;
}
</style>


		<!-- The Modal -->
		<div class="modal" id="myModaldelete">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title"  style="    padding: 22px;">Why do you want to delete this group?</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body" style="padding: 0px  45px;">

 						<ul style="list-style: none;">
							<li>
								<input type="radio" name="reason" value="Lack of Activity">&nbsp&nbsp&nbsp&nbsp Lack of activity </inpu>
							</li>
							<li>
								<input type="radio" name="reason" value="Lack of activity">&nbsp&nbsp&nbsp&nbsp Conflict in group </inpu>
							</li>
							<li>
								<input type="radio" name="reason" value="Lack of Activity">&nbsp&nbsp&nbsp&nbsp Hard to manage </inpu>
							</li>
							<li>
								<input type="radio" name="reason" value="Lack of Activity">&nbsp&nbsp&nbsp&nbsp Want to create a new group </inpu>
							</li>
							<li>
								<input type="radio" name="reason" value="Lack of Activity">&nbsp&nbsp&nbsp&nbsp Not interested any more </inpu>
							</li>
							<li>
								<input type="radio" name="reason" value="Lack of Activity">&nbsp&nbsp&nbsp&nbsp Need a break </inpu>
							</li>
							<li>
								<input type="radio"  name="reason" value="Lack of Activity">&nbsp&nbsp&nbsp&nbsp Need time to catch up </inpu>
							</li>
							<li>
								<input type="radio" name="reason" value="Lack of Activity">&nbsp&nbsp&nbsp&nbsp other </inpu>
							</li>
						</ul>
						<button class="btn btn-primary" onclick="managedtab()">Continue</button>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>

		<script>
			function managedtab(){
				if ($("input[name='reason']").is(':checked')) {


				}
				else {
					Swal.fire(
						'info',
						'Please select one of the following reason.',
						'info'
					)
					return ;
				}

				$.ajax({
					url: base_url + 'group/show_group_members/',
					method: 'POST',
					dataType: 'json',
					data: {
						id: $('#groupID').val(),
						keyword: '',
						role: ''
					},
					success: function (response) {
					 			if( response.totalRecords > 1){
									Swal.fire(
										'Deleting a group',
										' In order to delete your group,  you will first empty the members in your group ',
										'error'
									)
									 $('#myModaldelete').modal('hide');
									 $('#managed_tab').click();
								}else{
									$('#myModaldelete').modal('hide');
								    $('#deletegroup').click();
								}
					}
				});
 				}

		</script>

