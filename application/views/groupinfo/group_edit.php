<section class="user-section entry-section" id="entryform">
	<div class="container">
	    <div class="row">
	        <div class="col-lg-12">                   
	            <?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
	            <?php if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>

                <?php
                    echo validation_errors();
                    echo form_open('account/groupinfo/updatedetail/'.$EncodedID);
                    echo form_hidden('GroupID', $EncodedID);
                ?>

                    <div class="control-group mb-2 form-group">
						<h6 class="form-required font-weight-bold label-h6">Category</h6>
						<div class="admin-form-group controls pb-1">
							<div class="input-group">
								<select class="custom-select" id="selCategory" name="selCategory" required="required">
									<option value="">Choose category</option>
									<?php foreach ($category_items as $item): ?>
										<option value="<?php echo $item['Ca_ID'];?>" <?php if (strcmp($groups_items['Gr_Ca_ID'],$item['Ca_ID'])==0) echo 'selected';?>><?php echo $item['Ca_Name'];?></option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($groups_items['Gr_Ca_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
								</select>
							</div>
						</div>
					</div>
											
					<div class="control-group mb-2 form-group" id="selNewCategoryBox" style="<?php if (strcmp($groups_items['Gr_Ca_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">									
							<input class="form-control" id="txtNewCategory" name="txtNewCategory" type="text" value="<?php echo $groups_items['Gr_Ca_Name'];?>" placeholder="Enter new category">
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="form-required font-weight-bold label-h6">Sub-Category</label>
						<div class="admin-form-group controls pb-1">
							<div class="input-group">
								<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required">
									<option value="">Choose sub-category</option>
									<?php foreach ($subcategory_items as $item): ?>
									<option value="<?php echo $item['Sc_ID'];?>" <?php if (strcmp($groups_items['Gr_Sc_ID'],$item['Sc_ID'])==0) echo 'selected';?>><?php echo $item['Sc_Name'];?></option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($groups_items['Gr_Sc_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
								</select>
							</div>
						</div>
					</div>
											
					<div class="control-group mb-2 form-group" id="selNewSubCategoryBox" style="<?php if (strcmp($groups_items['Gr_Sc_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">									
							<input class="form-control" id="txtNewSubCategory" name="txtNewSubCategory" type="text" value="<?php echo $groups_items['Gr_Sc_Name'];?>" placeholder="Enter new sub-category">
						</div>
					</div>
											
					<div class="control-group mb-2 form-group">
						<div class="admin-form-group controls pb-1">
							<label class="form-required font-weight-bold label-h6">Group</label>
							<input class="form-control" id="txtTitle" name="txtTitle" type="text" value="<?php echo $groups_items['Gr_Name'];?>" placeholder="Enter group name" >
						</div>
					</div>
											
					<div class="control-group mb-4 form-group">
						<div class="admin-form-group controls pb-1">
							<label class="font-weight-bold label-h6">Description</label><span class="ml-1 text-primary font-weight-normal text-small">You may skip and fill this up later.</span>
							<input class="form-control" id="txtDescription" name="txtDescription" type="text" value="<?php echo $groups_items['Gr_Description'];?>" placeholder="Enter group's mission" >
						</div>
					</div>


				<div class="control-group mb-2 form-group">
					<label class="font-weight-bold label-h6">Group Plan</label>
					<div class="admin-form-group controls mb-0 pb-1">	<div class="input-group">
							<input type="hidden" id="previouspalan" value="<?php  echo $groups_items['Gr_packegtype']; ?>">
						<select id="planchange" class="form-control" name="groupplan" onchange="ChangePackage(this.value);" >

							<option value="1" <?php if($groups_items['Gr_packegtype'] == 1){echo 'selected';} ?>>Free Plan</option>
							<option value="2" <?php if($groups_items['Gr_packegtype'] == 2){echo 'selected';} ?>>Plus Plan</option>
							<option value="3" <?php if($groups_items['Gr_packegtype'] == 3){echo 'selected';} ?>>Pro Plan</option>
						</select>
 					</div></div>
				</div>
					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Keyword Tags</label><span class="ml-1 text-primary font-weight-normal text-small">Tags will help people find your group. Separate tags with comma.</span>
						<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtKeywords" name="txtKeywords" type="text" value="<?php echo $groups_items['Gr_Keywords'];?>">
						</div>
					</div>

					<div class="control-group mb-4 form-group">
						<div class="admin-form-group controls mb-0 pb-1 radio_btn_group radio_btn_group_edit">
							<label class="form-required font-weight-bold label-h6">Privacy</label>

							<!-- Default unchecked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public" <?php if (strcmp($groups_items['Gr_Privacy'],'public')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPublic">Public </label><br>Anyone can find, follow and join in your group
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" <?php if (strcmp($groups_items['Gr_Privacy'],'private')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPrivate">Private </label><br>Anyone can find your group. But admin permission but admin permission is required to follow and join in your group.
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" <?php if (strcmp($groups_items['Gr_Privacy'],'secret')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radSecret">Secret </label><br>Only those with link and unique password can find & join your group.
							</div>
						</div>
					</div>

                    <div class="admin-form-group">
                        <a href="<?php echo base_url(); ?>account/groupinfo/detail/<?php echo $EncodedID;?>" class="btn btn-xl btn-light">Cancel</a>
                        <button type="submit" class="btn btn-xl btn-primary">Save Changes</button>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</section>

<script>

	function ChangePackage(csvalue){

		$("#packagesmodel1").modal('show');
	}
		function choosePayment1(vall){
			Swal.fire({
				title: 'info',
				text: "Are You Sure , You want to Change the Plan",
				icon: 'info',
				showCancelButton: true,
				confirmButtonText:'Yes Change It',
				cancelButtonColor:  '#3085d6',
				confirmButtonColor:  '#d33',
				cancelButtonText: 'No'
			}).then((result) => {
				if (result.isConfirmed) {
 				 $('#planchange').val(vall);
					$("#packagesmodel1").modal('hide');

				}else{
					$('#planchange').val($('#previouspalan').val());
					$("#packagesmodel1").modal('hide');
				}
			})

		}
</script>
