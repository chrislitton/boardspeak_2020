	<?php
		$ProfileMenu = (strcmp($selMenu,'Profile')==0) ? ' active' : '';
		$PasswordMenu = (strcmp($selMenu,'Password')==0) ? ' active' : '';
		$PhotoMenu = (strcmp($selMenu,'Photo')==0) ? ' active' : '';
		$BackgroundMenu = (strcmp($selMenu,'Background')==0) ? ' active' : '';
	?>

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/xcode.min.css" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/countries/dist/css/bootstrap-select-country.min.css" />
					<section class="user-section entry-section" id="entryform">
					<div class="container">
						<div class="row">

							<div class="col-lg-3 mx-auto">
							<nav class="text-uppercase" id="boardsubmenu">
							<ul class="navbar-nav nav" role="tablist" id="navsubtab">
									<li class="nav-item mx-0 mx-lg-1 mb-1">
										<a class="btn-primary px-2 nav-link<?php echo $ProfileMenu;?>" id="nav-editprofile-tab" data-toggle="tab" href="#nav-editprofile" role="tab" aria-controls="nav-editprofile" aria-selected="true">Edit Profile</a>
									</li>
									<li class="nav-item mx-0 mx-lg-1 mb-1">
										<a class="btn-primary px-2 nav-link<?php echo $PasswordMenu;?>" id="nav-changepassword-tab" data-toggle="tab" href="#nav-changepassword" role="tab" aria-controls="nav-changepassword" aria-selected="true">Change Password</a>
									</li>
									<li class="nav-item mx-0 mx-lg-1 mb-1">
										<a class="btn-primary px-2 nav-link<?php echo $PhotoMenu;?>" id="nav-changephoto-tab" data-toggle="tab" href="#nav-changephoto" role="tab" aria-controls="nav-changephoto" aria-selected="true">Change Photo</a>
									</li>
									<li class="nav-item mx-0 mx-lg-1 mb-1">
										<a class="btn-primary px-2 nav-link<?php echo $BackgroundMenu;?>" id="nav-changebackground-tab" data-toggle="tab" href="#nav-changebackground" role="tab" aria-controls="nav-changebackground" aria-selected="true">Change Background</a>
									</li>
								</ul>
							</nav>
							</div>

							<div class="col-lg-9 mx-auto">

									<div class="tab-content" id="nav-subtabContent">
										<div class="tab-pane fade show<?php echo $ProfileMenu;?>" id="nav-editprofile" role="tabpanel" aria-labelledby="nav-editprofile-tab">
										<div class="row">
											<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
											<?php echo $errorProfile;?>
											</div>
										</div>

											<?php
												echo validation_errors();
												echo form_open('account/saveprofile');
												echo form_hidden('UserID', $users_item['Us_ID']);
											?>

												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Name</label>
														<input class="form-control" id="txtName" name="txtName" type="text" value="<?php echo $users_item['Us_Name'];?>" placeholder="Name" required="required" data-validation-required-message="Please enter your name.">
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Email Address</label>
														<input class="form-control" id="email" name="txtEmail" type="email" value="<?php echo $users_item['Us_Email'];?>" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address.">
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Phone Number</label>
														<input class="form-control" id="phone" name="txtPhone" type="tel" value="<?php echo $users_item['Us_Phone'];?>" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number.">
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Company</label>
														<input class="form-control" id="company" name="txtCompany" type="text" value="<?php echo $users_item['Us_Company'];?>" placeholder="Company" required="required" data-validation-required-message="Please enter your company.">
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Job Title</label>
														<input class="form-control" id="jobtitle" name="txtJobTitle" type="text" value="<?php echo $users_item['Us_JobTitle'];?>" placeholder="Job Title" required="required" data-validation-required-message="Please enter your job title.">
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>City, country</label>
														<select class="selectpicker countrypicker"  id="employee" data-flag="true" id="employee" name="txtNoOfEmployees" name="txtNoOfEmployees"  ></select>

														<!--														<input class="form-control" id="employee" name="txtNoOfEmployees" type="text" value="--><?php //echo $users_item['Us_NoOfEmployees'];?><!--" placeholder="Number of Employee" required="required" data-validation-required-message="Please enter number of employee.">-->
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Why did you use BoardSpeak?</label>
														<textarea class="form-control" id="features" name="txtFeatures" rows="3" placeholder="Features you need in order to have efficient team communication" required="required" data-validation-required-message="Please enter a message."><?php echo $users_item['Us_Features'];?></textarea>
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<br>
												<div id="success"></div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Submit</button>
												</div>
											</form>


										</div>
										<div class="tab-pane fade show<?php echo $PasswordMenu;?>" id="nav-changepassword" role="tabpanel" aria-labelledby="nav-changepassword-tab">
												<div class="row">
													<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
													<?php echo $errorPassword;?>
													</div>
												</div>

													<?php
														echo validation_errors();
														echo form_open('account/changepassword');
														echo form_hidden('UserID', $users_item['Us_ID']);
													?>


													<div class="control-group">
														<div class="admin-form-group controls mb-0 pb-1">
															<label>Old Password</label>
															<input class="form-control" id="txtOldPassword" name="txtOldPassword" type="password" placeholder="Old Password" required="required" data-validation-required-message="Please enter your name.">
															<p class="help-block text-danger"></p>
														</div>
													</div>

													<div class="control-group">
														<div class="admin-form-group controls mb-0 pb-1">
															<label>New Password</label>
															<input class="form-control" id="txtNewPassword" name="txtNewPassword" type="password" placeholder="New Password" required="required" data-validation-required-message="Please enter your name.">
															<p class="help-block text-danger"></p>
														</div>
													</div>
													<div class="control-group">
														<div class="admin-form-group controls mb-0 pb-1">
															<label>Re-Type Password</label>
															<input class="form-control" id="txtRetypePassword" name="txtRetypePassword" type="password" placeholder="Re-type Password" required="required" data-validation-required-message="Please enter your email address.">
															<p class="help-block text-danger"></p>
														</div>
													</div>

													<br>
													<div id="success"></div>
													<div class="form-group">
														<button type="submit" class="btn btn-primary" id="sendMessageButton">Save Changes</button>
													</div>
												</form>

										</div>
										<div class="tab-pane fade show<?php echo $PhotoMenu;?>" id="nav-changephoto" role="tabpanel" aria-labelledby="nav-changephoto-tab">
												<div class="row">
													<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
													<?php echo $errorPhoto;?>
													</div>
												</div>

												<?php
													echo validation_errors();
													echo form_open_multipart('account/changephoto');
													echo form_hidden('UserID', $users_item['Us_ID']);
													echo form_hidden('UploadFile', 'flePhoto');
												?>

														<div class="control-group">
															<div class="admin-form-group controls mb-0 pb-1">
																<label>Choose Photo</label>
																<div class="input-group">
																	<div class="input-group-prepend">
																				<span class="input-group-text rounded-0 btn btn-default btn-file">Upload Photo
																						<input type="file" id="flePhoto" name="flePhoto">
																					</span>
																	</div>
																	<input type="text" class="form-control" readonly>
																	<img id='img-upload'/>
																</div>
															</div>
														</div>
														<br>
														<div id="success"></div>
														<div class="form-group">
															<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Submit</button>
														</div>
													</form>
										</div>
										<div class="tab-pane fade show<?php echo $BackgroundMenu;?>" id="nav-changebackground" role="tabpanel" aria-labelledby="nav-changebackground-tab">

												<div class="row">
													<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
													<?php echo $errorBackground;?>
													</div>
												</div>

												<div class="row">
													<div class="col-lg-12 mx-auto text-center mb-1">
													<h5>Choose Background</h5>
													</div>
												</div>


												<div class="row">
												<?php foreach ($backcolors_items as $item): ?>

													<div class="col-md-6 col-lg-4 admin-img-box">
														<a class="board-link" href="<?php echo base_url(); ?>account/setbackcolor/<?php echo $users_item['Us_ID'];?>/<?php echo $item['Se_ID'];?>">
														<div class="admin-colorbg <?php echo $item['Se_Value']; ?>">
																<p>&nbsp;</p>
																<div class="admin-img-label shadow-sm">
																	<div class="row">
																		<div class="col-md-12 col-lg-12">
																			<p class="lead board-category"><?php echo $item['Se_Name']; ?></p>
																	</div>
																	</div>
																</div>

														</div>
														</a>
													</div>

												<?php endforeach; ?>
												</div>

												<div class="row">
														<?php foreach ($backgrounds_items as $item): ?>

														<div class="col-md-6 col-lg-4 admin-img-box">
															<div class="admin-img">
																<a class="board-link" href="<?php echo base_url(); ?>account/setbackground/<?php echo $users_item['Us_ID'];?>/<?php echo $item['Se_ID'];?>">
																	<img class="img-fluid" src="<?php echo $item['Se_Photo']; ?>" alt="">
																</a>
																<div class="admin-img-label shadow-sm">
																		<div class="row">
																			<div class="col-md-12 col-lg-12">
																				<p class="lead board-category"><?php echo $item['Se_Name']; ?></p>
																			</div>
																		</div>
																	</div>

															</div>
														</div>
														<?php endforeach; ?>


														</div>


												<?php
													echo validation_errors();
													echo form_open_multipart('account/changebackground');
													echo form_hidden('UserID', $users_item['Us_ID']);
													echo form_hidden('UploadFile', 'fleBackground');
												?>

															<div class="control-group">
																<div class="admin-form-group controls mb-0 pb-1">
																		<div class="input-group">
																				<div class="input-group-prepend">
																							<span class="input-group-text rounded-0 btn btn-default btn-file">Upload Background
																									<input type="file" id="fleBackground" name="fleBackground">
																								</span>
																				</div>
																				<input type="text" class="form-control" readonly>
																				<img id='img-background'/>
																		</div>
																</div>
															</div>
															<br>
															<div id="success"></div>
															<div class="form-group">
																<button type="submit" class="btn btn-primary" id="sendMessageButton">Submit Background</button>
															</div>
														</form>
										</div>
									</div>

							</div>
						</div>

					</div>
				</section>

	<script src="<?php echo base_url(); ?>assets/countries/dist/js/bootstrap-select-country.min.js"></script>
