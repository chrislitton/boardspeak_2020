<?php echo form_open_multipart('account/saveevent'); ?>




<section  class="dethead">
    <div class="container">
			<div class="row">
				<div class="col-lg-9">


<!-- Masthead -->
<header id="groupbanner" class="mb-4">
   <div class="container">
  			<div class="row">
        		<div class="col-md-12 col-lg-12 board-banner text-right bg-primary" id="site-banner">
						<div class="post-title text-white">
							New Event
						</div>
						<div class="mt-3 changebgbtn">

							<div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
								<div class="input-group">
									<div class="input-group-prepend">
												<span class="input-group-text rounded-0 btn btn-default btn-file">Upload Image
													<input type="file" id="flebg" name="userfile">
												</span>
									</div>
									<input id="fleName" type="text" class="form-control" readonly>
								</div>
							</div>
							</div>

						</div>

  				</div>
	     	</div>
	</div>
</header>


<!-- Contact Section -->
  <section class="user-section entry-section" id="contact">
    <div class="container">
		


      <div class="row">
        <div class="col-lg-12 mx-auto text-center mb-1 text-danger">
      	<?php echo $error;?>
      	</div>
      </div>

      <div class="row">
        <div class="col-lg-12 mx-auto">

            <div class="control-group d-none">
								<div class="admin-form-group controls mb-0 pb-1">
                <input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
		            </div>
            </div>
          	
            <div class="control-group">
								<div class="admin-form-group controls mb-0 pb-1">
                <input class="form-control" id="txtSubject" name="txtSubject" type="text" placeholder="Subject" required="required" data-validation-required-message="Please enter your subject.">
		            </div>
            </div>

						<div class="control-group">
								<div class="admin-form-group controls mb-0 pb-1">
                <input class="form-control" id="txtLocation" name="txtLocation" type="text" placeholder="Location" required="required" data-validation-required-message="Please enter your location.">
		            </div>
            </div>		
			
            <div class="control-group">
            	<div class="row">
        			<div class="col-lg-6 mx-auto">
        			               
		              <div class="admin-form-group controls mb-0 pb-1">
                		<div class="input-group">
											<div class="input-group-prepend">
												<label class="input-group-text rounded-0" for="txtStartDate">Start Date</label>
											</div>
		                	<input class="form-control" id="txtStartDate" name="txtStartDate" type="date" placeholder="Start Date" required="required" data-validation-required-message="Please enter your start date.">
										</div>
		              </div>
		           	</div>


								<div class="col-lg-6 mx-auto">
										<div class="admin-form-group controls mb-0 pb-1">
												<div class="input-group">
													<div class="input-group-prepend">
														<label class="input-group-text rounded-0" for="txtDueDate">Due Date</label>
													</div>
													<input class="form-control" id="txtDueDate" name="txtDueDate" type="date" placeholder="Due Date" required="required" data-validation-required-message="Please enter your due date.">
												</div>
										</div>
								</div>
							
		         </div>
            </div>
            
            
            <div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
                <div class="input-group">
								<div class="input-group-prepend">
									<label class="input-group-text rounded-0" for="selPriority">Priority</label>
								</div>
								<select class="custom-select" id="selPriority" name="selPriority">
									<option value="Low">Low</option>
									<option value="Normal" selected>Normal</option>
									<option value="High">High</option>
								</select>
							</div>          
	      		</div>
            </div>
            
            <div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
                <div class="input-group">
								<div class="input-group-prepend">
									<label class="input-group-text rounded-0" for="selType">Type</label>
								</div>
								<select class="custom-select" id="selType" name="selType">
									<option value="Private">Private</option>
									<option value="Broadcast">Broadcast</option>
									<option value="Public">Public</option>
								</select>
							</div>          
							</div>
            </div>

						<div class="control-group mt-2">
							<div class="admin-form-group">
								<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Create</button>
							</div>
            </div>


				</div>

      </div>  
      

      </form> 

    </div>
  </section>



			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5">
				<h5 class="font-weight-normal text-primary">Creating Event</h5>
				<ol>
					<li>Instruction 1</li>
					<li>Instruction 2</li>
					<li>Instruction 3</li>
					<li>Instruction 4</li>
					<li>Instruction 5</li>
				</ol>
				</div>


			</div>
    </div>
    </div>
  </section>




