<div class="popup_container create_group_popup">
    <div class="popup_item create_group_popup_item" style="background : url('<?php echo base_url()."img/explainer_bg.jpg"?>');">
        <span class="popup_closer create_group_popup_closer">
            <i class="cross fas fa-times"></i>
        </span>
        <h2 class="create_header_bar">
            Create Group
        </h2>
        <div class="divider"></div>
        <h3 align="center" class=" ">
            ORGANIZE YOUR GROUP COMMUNICATION
        </h3>
        <div class="divider"></div>
        <div class="group_upper text-center">
            <p class="mt-4 mb-4" style="    font-size: 19px;">
                Fortify your team by assigning Super Admin (to help you manage your group) <br> and Admins (to help manage multiple topics and post discussions), <br> All in One Place!
            </p>
            <img class="w-100" src="<?php echo base_url()."img/group_explainer_img_1.png"?>" alt="">
        </div>
        <div class="group_middler row align-items-center">
            <div class="col-12 pb-4 middler_inner" style=" background-image: url('<?php echo base_url()."img/group_explainer_img_2.png"?>') ">
                <p  style="    font-size: 19px;">
                    To enhance admin controls, <br> you may invite <b>Members</b> <br> (who can only comment, <br> but not post) and <b>Followers</b> <br> (who can only view).
                </p>
                <a class="learn_more_btn btn" href="" title="">Learn More</a>
            </div>

            <div class="col-12" style="text-align: center;">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="not_again">
                    <label class="form-check-label ml-1" for="not_again">
                        Do not show this again on next log in.
                    </label>
                </div>
            </div>
        </div>
        <div class="get_started">
            <a class="get_started_btn btn btn-info w-100" href="<?php echo base_url().'account/create/group'; ?>" title="">Get Started</a>
            <p class="info_text">
                By clicking Get Started you agree to the
                <a href="">BoardSpeak Terms</a>
            </p>
        </div>
    </div>
</div>
