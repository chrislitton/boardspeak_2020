<?php
		$BoardsMenu = (strcmp($selMenu,'Groups')==0) ? ' active' : '';
		$TopicsMenu = (strcmp($selMenu,'Topics')==0) ? ' active' : '';
		$PostsMenu = (strcmp($selMenu,'Posts')==0) ? ' active' : '';
		$SurveyMenu = (strcmp($selMenu,'Survey')==0) ? ' active' : '';
		$EventsMenu = (strcmp($selMenu,'Events')==0) ? ' active' : '';
		$CalendarMenu = (strcmp($selMenu,'Calendar')==0) ? ' active' : '';
	?>

	<?php
	$show_groups_load_btn = "";
	$show_topics_load_btn = "";
	$show_posts_load_btn = "";
	$show_surveys_load_btn = "";

	if (count($groups_items)<12) $show_groups_load_btn = " d-none";
	if (count($topics_items)<12) $show_topics_load_btn = " d-none";
	if (count($posts_items)<12) $show_posts_load_btn = " d-none";
	if (count($survey_item)<12) $show_surveys_load_btn = " d-none";
?>


	<section class="board-menu-section boards-menu mt-4" id="boardmenu">
  	<div class="container">
   	<div class="row bg-light">			
	    	<div class="col-md-12 col-lg-12">
					<nav class="navbar navbar-expand-lg navbar-light justify-content-end">
								<!-- Navigation -->
								<button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-white rounded" type="button" data-toggle="collapse" data-target="#navSubMenu" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">        
									More <i class="fas fa-bars"></i>
								</button>
								<div class="collapse navbar-collapse" id="navSubMenu">
									<ul class="navbar-nav nav" role="tablist" id="navtab">
										<li class="nav-item mx-0 mx-lg-1">
											<a class="nav-link<?php echo $BoardsMenu;?>" id="nav-groups-tab" data-toggle="tab" href="#nav-groups" role="tab" aria-controls="nav-groups" aria-selected="true">Group</a>
										</li>
										<li class="nav-item mx-0 mx-lg-1">
											<a class="nav-link<?php echo $TopicsMenu;?>" id="nav-topics-tab" data-toggle="tab" href="#nav-topics" role="tab" aria-controls="nav-topics" aria-selected="false">Topics</a>		
										</li>
										 <li class="nav-item mx-0 mx-lg-1">
												<a class="nav-link<?php echo $PostsMenu;?>" id="nav-posts-tab" data-toggle="tab" href="#nav-posts" role="tab" aria-controls="nav-posts" aria-selected="false">Posts</a>
										</li>
										 <li class="nav-item mx-0 mx-lg-1">
												<a class="nav-link<?php echo $SurveyMenu;?>" id="nav-survey-tab" data-toggle="tab" href="#nav-survey" role="tab" aria-controls="nav-survey" aria-selected="false">Survey</a>
										</li>
										 <li class="nav-item mx-0 mx-lg-1">
												<a class="nav-link<?php echo $EventsMenu;?>" id="nav-events-tab" data-toggle="tab" href="#nav-events" role="tab" aria-controls="nav-events" aria-selected="false">Events</a>
										</li>
										 <li class="nav-item mx-0 mx-lg-1">
												<a class="nav-link<?php echo $CalendarMenu;?>" id="nav-calendar-tab" data-toggle="tab" href="#nav-calendar" role="tab" aria-controls="nav-calendar" aria-selected="false">Tasks</a>
										</li>
									</ul>
								</div>
					</nav>
      		</div>			
	</div>
	</div>
	</section>
	
	


		<div class="tab-content" id="nav-tabContent">
			<div class="tab-pane fade show<?php echo $BoardsMenu;?>" id="nav-groups" role="tabpanel" aria-labelledby="nav-groups-tab">

						<section class="portfolio" id="groups">
						<div class="container">



							<div class="row" id="groupresult">

								<div class="col-md-6 col-lg-4 admin-img-box bg-primary add-new-bg">
									<div class="admin-img">
									<a class="board-link" href="<?php echo base_url(); ?>account/creategroup">
										<i class="fa fa-plus add-new-icon"></i>
										<p class="add-new-label">Add New</p>
									</a>
									</div>
								</div>

								<?php foreach ($groups_items as $item): ?>

										<div class="col-md-6 col-lg-4 admin-img-box groupsloadmore">
											<div class="admin-img <?php  if (strlen($item['Gr_Thumb'])==0) echo 'bg-group'; echo ' '.$item['Gr_Backcolor']; ?>">
													<a class="board-link" href="<?php echo base_url(); ?>account/view/group/<?php echo $item['Gr_ID'];?>">
														<img class="img-fluid" src="<?php echo $item['Gr_Thumb'];?>" alt="">
													</a>
													<div class="admin-img-label shadow-sm">
														<div class="row">
															<div class="col-md-12 col-lg-12">
																<a class="board-link" href="<?php echo base_url(); ?>account/view/group/<?php echo $item['Gr_ID'];?>">
																<p class="board-title text-center"><?php echo $item['Gr_Name']; ?></p>
																</a>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12 col-lg-12">

																<div class="btn-social-box-left">
																		<p class="board-category"><?php echo ucfirst($item['Gr_Privacy']); ?></p>
																</div>
																<div class="btn-social-box-right">
																	<a class="btn admin-btn-social mx-0" href="#">
																		<i class="fa fa-arrow-right mr-2"></i> Share
																	</a>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>      

							<?php endforeach; ?>
						<div class="col-md-12 col-lg-12 mt-3 text-center <?php echo $show_groups_load_btn;?>">
							<a class="btn btn-primary px-5" href="#" id="groupsloadmore">Load More</a>
						</div>

						</div>

					</div> 
				</section>
						

		</div>
			<div class="tab-pane fade show<?php echo $TopicsMenu;?>" id="nav-topics" role="tabpanel" aria-labelledby="nav-topics-tab">
			
			
				<!-- Topics Section -->
				<section class="portfolio" id="topics">
					<div class="container">
						<div class="row">            
						
							<div class="col-md-6 col-lg-4 admin-img-box bg-primary add-new-bg">
								<div class="admin-img">  
								<a class="board-link" href="<?php echo base_url(); ?>account/createtopic">
									<i class="fa fa-plus add-new-icon"></i>
									<p class="add-new-label">Add New</p>
								</a>	
								</div>
							</div>    
							
							
							<?php foreach ($topics_items as $item): ?>
								<div class="col-md-6 col-lg-4 admin-img-box topicsloadmore">
									<div class="admin-img <?php  if (strlen($item['To_Thumb'])==0) echo 'bg-topic'; echo ' '.$item['To_Backcolor']; ?>">
										<a class="board-link" href="<?php echo base_url(); ?>account/view/topic/<?php echo $item['To_ID'];?>">                            	
											<img class="img-fluid" src="<?php echo $item['To_Thumb']; ?>" alt="">
										</a>
										<div class="admin-img-label shadow-sm">  
											<div class="row">     
												<div class="col-md-12 col-lg-12">
													<a class="board-link" href="<?php echo base_url(); ?>account/view/topic/<?php echo $item['To_ID'];?>">
													<p class="board-title text-center"><?php echo $item['To_Name']; ?></p>
													</a>
												</div>
											</div>
											
											<div class="row">
												
												<div class="col-md-12 col-lg-12">
											
													<div class="btn-social-box-left">
															<p class="board-category"><?php echo $item['Gr_Name']; ?></p>
													</div>
													<div class="btn-social-box-right">
														<a class="btn admin-btn-social mx-0" href="#">
															<i class="fa fa-arrow-right mr-2"></i> Share
														</a>
													</div>
												</div>
												
												
											</div>
										</div>
									
									</div>  
								</div>
				
						<?php endforeach; ?>
						<div class="col-md-12 col-lg-12 mt-3 text-center <?php echo $show_topics_load_btn;?>">
							<a class="btn btn-primary px-5" href="#" id="topicsloadmore">Load More</a>
						</div>


						</div>	
					</div> 
				</section>
				
		</div>	
			<div class="tab-pane fade show<?php echo $PostsMenu;?>" id="nav-posts" role="tabpanel" aria-labelledby="nav-posts-tab">
				
				<!-- posts Section -->
				<section class="portfolio" id="posts">
					<div class="container">
						<div class="row">

							<div class="col-md-6 col-lg-4 admin-img-box bg-primary add-new-bg">
								<div class="admin-img">
								<a class="board-link" href="<?php echo base_url(); ?>account/createpost">
									<i class="fa fa-plus add-new-icon"></i>
									<p class="add-new-label">Add New</p>
								</a>
								</div>
							</div>

							
							<?php foreach ($posts_items as $item): ?>


								<div class="col-md-6 col-lg-4 admin-img-box postsloadmore">
									<div class="admin-img <?php  if (strlen($item['Po_Thumb'])==0) echo 'bg-post'; echo ' '.$item['Po_Backcolor']; ?>">									
										<a class="board-link" href="<?php echo base_url(); ?>account/view/post/<?php echo $item['Po_ID'];?>">
											<img class="img-fluid" src="<?php echo $item['Po_Thumb']; ?>" alt="">
										</a>

										<div class="admin-img-label shadow-sm">
											<div class="row">
												<div class="col-md-12 col-lg-12">
													<a class="board-link" href="<?php echo base_url(); ?>account/view/post/<?php echo $item['Po_ID'];?>">
													<p class="board-title text-center"><?php echo $item['Po_Title']; ?></p>
													</a>
												</div>
											</div>

											<div class="row">

												<div class="col-md-12 col-lg-12">
													<div class="btn-social-box-left">
															<p class="lead board-category"><?php echo date( "M d, Y", strtotime($item['Po_DatePosted']) ); ?></p>
													</div>
													<div class="btn-social-box-right">
														<a class="btn admin-btn-social mx-0" href="#">
															<i class="fa fa-arrow-right mr-2"></i> Share
														</a>
													</div>
											</div>
											</div>
										</div>

									</div>
								</div>

							<?php endforeach; ?>
							<div class="col-md-12 col-lg-12 mt-3 text-center <?php echo $show_posts_load_btn;?>">
								<a class="btn btn-primary px-5" href="#" id="postsloadmore">Load More</a>
							</div>

					</div>
				</div> 
				</section>
					
			</div>
			<div class="tab-pane fade show<?php echo $SurveyMenu;?>" id="nav-survey" role="tabpanel" aria-labelledby="nav-survey-tab">

				<!-- posts Section -->
				<section class="portfolio" id="posts">
					<div class="container">
						<div class="row">

							<div class="col-md-6 col-lg-4 admin-img-box bg-primary add-new-bg">
								<div class="admin-img">
								<a class="board-link" href="<?php echo base_url(); ?>account/createsurvey/group/0">
									<i class="fa fa-plus add-new-icon"></i>
									<p class="add-new-label">Add New</p>
								</a>
								</div>
							</div>


							<?php foreach ($survey_item as $item): ?>

							<?php
								$Link = base_url().'account/survey/edit/'. $item['Po_ID'];
								if (strcmp($item['Po_SurveyStatus'],'started')==0) $Link = base_url().'account/survey/start/'. $item['Po_ID'];
							?>


								<div class="col-md-6 col-lg-4 admin-img-box surveyloadmore">
									<div class="admin-img <?php  if (strlen($item['Po_Thumb'])==0) echo 'bg-post'; echo ' '.$item['Po_Backcolor']; ?>">
										<a class="board-link" href="<?php echo $Link ?>">
											<img class="img-fluid" src="<?php echo $item['Po_Thumb']; ?>" alt="">
										</a>

										<div class="admin-img-label shadow-sm">
											<div class="row">
												<div class="col-md-12 col-lg-12">
													<a class="board-link" href="<?php echo $Link ?>">
													<p class="board-title text-center"><?php echo $item['Po_Title']; ?></p>
													</a>
												</div>
											</div>

											<div class="row">

												<div class="col-md-12 col-lg-12">
													<div class="btn-social-box-left">
															<p class="lead board-category"><?php echo date( "M d, Y", strtotime($item['Po_DatePosted']) ); ?></p>
													</div>
													<div class="btn-social-box-right">
														<a class="btn admin-btn-social mx-0" href="#">
															<i class="fa fa-arrow-right mr-2"></i> Share
														</a>
													</div>
											</div>
											</div>
										</div>

									</div>
								</div>

							<?php endforeach; ?>
							<div class="col-md-12 col-lg-12 mt-3 text-center <?php echo $show_surveys_load_btn;?>">
								<a class="btn btn-primary px-5" href="#" id="surveyloadmore">Load More</a>
							</div>

					</div>
				</div>
				</section>

			</div>	
			<div class="tab-pane fade show<?php echo $EventsMenu;?>" id="nav-events" role="tabpanel" aria-labelledby="nav-events-tab">
				

				<!-- Events Section -->
				<section class="portfolio" id="events">
					<div class="container">

						<div class="row mb-3">
								<div class="col-md-12 col-lg-3">
									<img class="img-fluid" src="<?php echo base_url().'img/take-control.png'; ?>" alt="">

									<div class="admin-newbg bg-primary">
										<a class="board-link" href="<?php echo base_url(); ?>account/createevent">
											<i class="fa fa-plus add-new-icon"></i>
											<p class="add-new-label">Add New</p>
										</a>
									</div>
								</div>
								<div class="col-lg-9">
											<h3>Add Event</h3>
								</div>
						</div>
						
						
							<?php foreach ($events_items as $item): ?>

							<div class="row mb-3">
								<div class="col-lg-3 mx-auto">
									<div class="admin-small-img bg-primary">
										<a class="board-link" href="<?php echo base_url(); ?>account/event/<?php echo $item['Ev_ID']; ?>">
										<img class="img-fluid" src="<?php echo $item['Ev_Photo']; ?>" alt="" onerror="this.src='<?php echo base_url(); ?>img/primary.png'">
										</a>
									</div>
								</div>
								<div class="col-lg-9">
											<a class="board-link" href="<?php echo base_url(); ?>account/event/<?php echo $item['Ev_ID']; ?>">
												<h3 class="text-left"><?php echo $item['Ev_Subject']; ?></h3>
											</a>
											<p class="lead board-date">Posted On <?php echo date( "F d, Y h:i", strtotime($item['Ev_DatePosted']) ); ?> </p>
											<p class="board-detail"><?php echo $item['Ev_Type']; ?> Event | <?php echo $item['Ev_Priority']; ?> Priority</p>
											<p class="board-detail"><?php echo 'Date: '. date( "F d, Y h:i", strtotime($item['Ev_StartDate']) ).' to '.date( "F d, Y h:i", strtotime($item['Ev_DueDate']) ); ?></p>
											<p class="board-detail">Location: <?php echo $item['Ev_Location']; ?></p>
								</div>
							</div>

							<?php endforeach; ?>
							
				
				</div> 
				</section>
					
			</div>	
			<div class="tab-pane fade show<?php echo $CalendarMenu;?>" id="nav-calendar" role="tabpanel" aria-labelledby="nav-calendar-tab">

				
					
				<!-- Calendar Section -->
				<section class="portfolio" id="calendar">
					<div class="container">


							<div class="row mb-3">
								<div class="col-md-12 col-lg-3">
									<img class="img-fluid" src="<?php echo base_url().'img/take-control.png'; ?>" alt="">
									<div class="admin-newbg bg-primary">
										<a class="board-link" href="<?php echo base_url(); ?>account/createtask">
											<i class="fa fa-plus add-new-icon"></i>
										</a>
									</div>
								</div>
								<div class="col-lg-9">
									<a class="board-link" href="<?php echo base_url(); ?>account/createtask">
									<h3 class="text-left">New Task</h3>
									</a>
									<p class="board-detail">Create new task</p>
								</div>
							</div>
						
						
							<?php foreach ($tasks_items as $item): ?>

								<div class="row mb-3">
									<div class="col-lg-3">
											<div class="admin-small-img bg-primary">
												<a class="board-link" href="<?php echo base_url(); ?>account/task/<?php echo $item['Ta_ID'];?>">
												<img class="img-fluid" src="<?php echo $item['Ta_Photo']; ?>" alt="" alt="" onerror="this.src='<?php echo base_url(); ?>img/primary.png'">
												</a>
											</div>
									</div>
									<div class="col-lg-7">
												<a class="board-link" href="<?php echo base_url(); ?>account/task/<?php echo $item['Ta_ID'];?>">
													<h3 class="text-left"><?php echo $item['Ta_Subject']; ?></h3>
												</a>
												<p class="lead board-date">Posted On <?php echo date( "F d, Y h:i", strtotime($item['Ta_DatePosted']) ); ?> </p>
												<p class="board-detail">Status <?php echo $item['Ta_Status']; ?>  | <?php echo $item['Ta_Priority']; ?> Priority | <?php echo $item['Ta_Completed']; ?> % Completed</p>
												<p class="board-detail"><?php echo 'Date: '. date( "F d, Y h:i", strtotime($item['Ta_StartDate']) ).' to '.date( "F d, Y h:i", strtotime($item['Ta_DueDate']) ); ?></p>

									</div>
									<div class="col-lg-2 mx-auto">
											<div class="board-cal text-center">
												<h1><?php echo date( "d", strtotime($item['Ta_StartDate'])); ?></h1>
												<p><?php echo date( "F Y", strtotime($item['Ta_StartDate']) );?></p>
											</div>
									</div>
								</div>
								
						<?php endforeach; ?>	   
							
				</div> 
				</section>
	
					
			</div>	
