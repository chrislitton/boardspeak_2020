
	<section class="user-menu-section" id="breadcrumb">
  	<div class="container">
			<div class="row">				
					<nav aria-label="breadcrumb">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/profile">Profile</a></li>
							<li class="breadcrumb-item active" aria-current="page"><?php echo $events_item['Ev_Subject']; ?></li>
						</ul>
					</nav>
			</div>			
	</div>
  </section>
    
  <!-- Portfolio Section -->
  <section class="portfolio" id="portfolio">
  	<div class="container">
   		<div class="row">		
				<div class="col-lg-3 mx-auto">
						<img class="img-fluid" src="<?php echo $events_item['Ev_Photo']; ?>" alt="" onerror="this.src='<?php echo base_url(); ?>img/primary.png'">
				</div>
        <div class="col-lg-9">
							<h3><?php echo $events_item['Ev_Subject']; ?></h3>
							<p class="lead board-date">Posted On <?php echo date( "F d, Y h:i", strtotime($events_item['Ev_DatePosted']) ); ?> </p>
							<p class="board-detail"><?php echo $events_item['Ev_Type']; ?> Event | <?php echo $events_item['Ev_Priority']; ?> Priority |
								<a class="text-danger" href="#" data-href="<?php echo base_url(); ?>account/deleteevent/<?php echo $events_item['Ev_ID'];?>" data-toggle="modal" data-target="#confirm-delete">Delete</a>
							</p>
							<p class="board-detail"><?php echo 'Date: '. date( "F d, Y h:i", strtotime($events_item['Ev_StartDate']) ).' to '.date( "F d, Y h:i", strtotime($events_item['Ev_DueDate']) ); ?></p>		
							<p class="board-detail">Location: <?php echo $events_item['Ev_Location']; ?></p>
				</div>
			</div>
		</div>
  </section>
