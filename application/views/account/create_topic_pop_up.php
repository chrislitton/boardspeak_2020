<!-- create topic or post Modal -->
<div class="modal fade" id="createTopicModal" tabindex="-1" aria-labelledby="createTopicModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-75 m-auto">
      <div class="modal-body pt-2 pb-4">
        <img src="<?php echo base_url(); ?>img/create-post-topic.png" width="100%">
        <div class="mt-3">
            <h6 class="text-info  text-center">Why do I need to create a SubGroup?</h6>
            <div class="mt-3">
            Create SUBGROUPS to keep multiple posts focused and relevant!
            <br><br>
            SUBGROUPS in BoardSpeak is a collection of various posts relevant to the subject in hand, to achieve focused content and discussion.
            </div>
            <div class="text-center my-3">
                <button type="button" id="btn_create_topic_start" class="btn btn-info btn-lg pt-1 col-12 col-lg-8">Create Subgroup</button>
                <br>
                <button type="button" id="btn_create_post_start" class="btn btn-default btn-lg pt-1 col-12 col-lg-8 text-info my-2" style="cursor: pointer;">
                    <label class="col-12" style="margin-bottom:0;cursor: pointer;">Create Post</label>
                    <small class="text-dark font-weight-bold" style="font-size: 65%;cursor: pointer;">If you do not need a Subgroup</small>
                </button>
            </div>

        </div>
      </div>
    </div>
  </div>
</div>


