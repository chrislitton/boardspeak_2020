<script>
	let data ;
	function firstmethod(){
		$.ajax({
			type: 'POST',
			url: base_url + 'account/validation_for_group/',
			data : { },
		}).done(function(dataz) {
			data  = JSON.parse(dataz);
		});
	}

	function choosePayment(paymentType){

		let plan = 'FREE';
		if(paymentType == 1){
			plan  = 'FREE';
		}else if(paymentType  == 2){
			plan = 'PLUS';
		}else if(paymentType == 3){
			plan = 'PRO';
		}
		Swal.fire({
			title: 'info',
			text: "You have chosen the "+plan+" plan",
			icon: 'info',
			showCancelButton: true,
			confirmButtonText:'Confirm',
			cancelButtonColor:  '#d33',
			confirmButtonColor:  '#3085d6',
			cancelButtonText: 'cancel'
		}).then((result) => {
			if (result.isConfirmed) {

				$("#pacckageid").val(paymentType)
				$('#packagesmodel').modal('hide');
				$('#sendMessageButton').click();
			}else{

			}
		})
	}
	function validationForm(){

		if(data.length > 0 ){

			if($("#pacckageid").val() != ''){
				return true;
			}
			else
			{

				Swal.fire({
					title: 'Info',
					text: "You have reached the FREE limit with maximum of one (1) group per user.",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonText:'Choose Package',
					cancelButtonColor:  '#d33',
					confirmButtonColor:  '#3085d6',
					cancelButtonText: 'Close'
				}).then((result) => {
					if (result.isConfirmed) {

						$found = false;
						for(var x = 0; x < data.length; x++){

							if(data[x]['Gr_packegtype'] == 1){
								$found = true;
								break;
							}
						}

						if($found == true){

							$('#firstpage_button').attr('disabled', true);
							$('#firstpage_button').text('Already Availed');
						}
						$('#packagesmodel').modal('show');
						return false;
						// $('#createTopicModal').modal('hide');
						// 	 $('#ContactUsForm').modal('show');
					}else{

					}
				})
				// $('#packagesmodel').modal('show');
				return false;
			}



		}
		else{

			if($("#pacckageid").val() != ''){
 					return true;
			}else{

				$('#packagesmodel').modal('show');
				return false;
			}


		}

	}

</script>

<style>
.fomr{justify-content:space-evenly;}
.right-column{
	width: 28%;
    background: #f5f5f5;
    padding: 29px;
	border-radius: 1rem;
	margin-top: 25px;"
}
.right-column img{margin:auto;}
@media screen and (max-width:973px) {
	.fomr{
		flex-direction:column;
		
	}
	.right-column{
	width:100%;
}
}</style>
<div class=" fomr d-flex ">
	<form   action="<?php echo base_url() ?>account/create/savegroup" id="created_gruop_Form" onsubmit="return validationForm(this)" method="POST" enctype="multipart/form-data">
		<section  class="dethead">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">

						<!-- Masthead -->
						<header id="groupbanner" class="mb-4">
							<div class="container">
								<div class="row">

									<div class="col-md-12 col-lg-12 text-center">
										<h3 class="text-primary">Create Group</h3>
									</div>
									<div class="col-md-12 col-lg-12 board-banner text-right bg-group group-cover-image" id="site-banner">
										<!-- <div class="post-title text-white">

										</div> -->
										<div style="box-shadow:none;" class="col-md-12 col-lg-12 board-banner text-center">
											<div class="col-md-12 col-lg-12 text-center">
												<!--  You Have To Put the directory of image  here Iftikhar  -->
 												 <div class="btn btn-primary mt-3" style="background: #398cb3a6!important;
                                                                                         border: 2px solid white!important;
                                                                                        
                                                                                         color: white!important;
                                                                                         font-weight: 600!important;

																						 border-radius: 9px!important;">

													<form method="post">
																												<label  class="title-tip title-tip-left"  title="Change Board Cover" for="upload_image" style="margin-bottom:0;">
															<img src="" id="uploaded_image" class="img-responsive img-circle" />
<!--															<i class="fas fa-camera "></i>-->
															<img src="<?php echo base_url(); ?>assetsofpop/img/replaceCamera.png" alt="Upload">
															<input type="file" name="image" class="image" id="upload_image" style="display:none" accept="image/*" />
															<input type="hidden" name="userfile" id="userfile" />
														</label>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</header>

						<section class="user-section entry-section">
							<div class="container">
								<?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>

								<div class="row">
									<div class="col-lg-12">
										<div class="control-group d-none">
											<div class="admin-form-group controls mb-0 pb-1">
												<input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
											</div>
										</div>
										<div class="control-group mb-2 form-group">
											<label class="form-required font-weight-bold label-h6">Category</label>
											<div class="admin-form-group controls pb-1">
												<div class="input-group">
													<select class="custom-select" id="selCategory" name="selCategory" required="required">
														<option value="">Choose category</option>
														<?php foreach ($category_items as $item): ?>
															<?php echo '<option value="'.$item['Ca_ID'].'">'.$item['Ca_Name'].'</option>'; ?>
														<?php endforeach; ?>
														<option value="0">Others, please specify</option>
													</select>
												</div>
											</div>
										</div>

										<div class="control-group mb-2" id="selNewCategoryBox" style="display:none;">
											<div class="admin-form-group controls pb-1">
												<input type="hidden" name="checkPackage" value="" id="pacckageid">
												<input class="form-control" id="txtNewCategory" name="txtNewCategory" type="text" placeholder="Enter new category" disabled="disabled">
											</div>
										</div>

										<div class="control-group mb-2 form-group">
											<label class="form-required font-weight-bold label-h6">Sub-Category</label>
											<div class="admin-form-group controls pb-1">
												<div class="input-group">
													<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required">
														<option value="">Choose a category first</option>
													</select>
												</div>
											</div>
										</div>

										<div class="control-group mb-2 form-group" id="selNewSubCategoryBox" style="display:none;">
											<div class="admin-form-group controls pb-1">
												<input class="form-control" id="txtNewSubCategory" name="txtNewSubCategory" type="text" placeholder="Enter new sub-category" disabled="disabled">
											</div>
										</div>

										<div class="control-group mb-2 form-group">
											<div class="admin-form-group controls pb-1">
												<label class="form-required font-weight-bold label-h6">Group</label>
												<input class="form-control" id="txtTitle" name="txtTitle" type="text" placeholder="Enter group name" required="required">
											</div>
										</div>

										<div class="control-group mb-4 form-group">
											<div class="admin-form-group controls pb-1">

												<label class="font-weight-bold label-h6">Description <span class="ml-1 text-primary font-weight-normal text-small">You may skip and fill this up later.</span></label>

												<input class="form-control" id="txtDescription" name="txtDescription" type="text" placeholder="Enter group's mission">
											</div>
										</div>

										<div class="control-group mb-2 form-group">
											<label class="font-weight-bold label-h6">Keyword Tags</label><span class="ml-1 text-primary font-weight-normal text-small">Tags will help people find your group. Separate tags with comma.</span>
											<div class="admin-form-group controls mb-0 pb-1">
												<input class="form-control" id="txtKeywords" name="txtKeywords" type="text">
											</div>
										</div>

										<div class="control-group mb-4 form-group">
											<div class="admin-form-group controls mb-0 pb-1 radio_btn_group radio_btn_group_create">
												<label class="form-required font-weight-bold label-h6">Privacy</label>

												<!-- Default unchecked -->
												<div class="custom-control custom-radio pb-2 radio_btn_item">
													<input type="radio" class="custom-control-input" id="radPublic"
														   name="radPrivacy" value="public" >
													<label class="custom-control-label" for="radPublic">Public </label><br>Anyone in your group can find, access and join the conversation.
												</div>

												<!-- Default checked -->
												<div class="custom-control custom-radio pb-2 radio_btn_item">
													<input type="radio" class="custom-control-input"
														   id="radPrivate" name="radPrivacy" value="private" checked>
													<label class="custom-control-label" for="radPrivate">Private </label><br>Anyone can find your group. But admin permission is required to follow and join in your group.
												</div>

												<!-- Default checked -->
												<div class="custom-control custom-radio pb-2 radio_btn_item">
													<input type="radio" class="custom-control-input" id="radSecret"
														   name="radPrivacy" value="secret">
													<label class="custom-control-label" for="radSecret">Secret </label><br>Only those with link and unique password can find & join your group.
												</div>

											</div>
										</div>

										<div class="control-group mt-5 text-center">
											<div class="admin-form-group">
												<a class="btn btn-light btn-xl mr-1" href="">Cancel</a>
												<button type="submit" class="btn btn-primary btn-xl ml-1" id="sendMessageButton">Create</button>
											</div>
										</div>
									</div>

								</div>

							</div>
						</section>
					</div>
				</div>
			</section>
			<div class="right-column">
							<img style="border-radius: 20px; " src="http://localhost/boardspeak_2020/assetsofpop/img/add1.png" alt="">
							</div>
		</div>
	</form>
														</div>
													



<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
        		<div class="img-container">
            		<div class="row">
                		<div class="col-md-8">
                    		<img src="" id="crop_uploaded_image" />
                		</div>
                		<div class="col-md-4">
                    		<div class="preview"></div>
				      		<div class="modal-footer" style='padding-right: 0px;'>
				      			<button type="button" id="crop" class="btn btn-primary">Upload</button>
				        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				      		</div>
                		</div>
            		</div>
        		</div>
      		</div>
    	</div>
  	</div>
</div>
<script>

		function contatus(){
			$('#packagesmodel').modal('hide');
			$('#ContactUsForm').modal('show');
		}

</script>

<div class="modal fade bd-example-modal-lg" id="packagesmodel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- This snippet uses Font Awesome 5 Free as a dependency. You can download it at fontawesome.io! -->
		 	<section class="pricing py-5">
				<div class="container">
					<h4 align="center" style="color: white; margin-bottom: 20px;">Choose Your Plan</h4>
				 <h6 align="center" style="margin-bottom: 22px;"><font style="color: #ffffff;" onclick="contatus()">Contact Us </font> for a more customized plan </h6>
					<div class="row">
						<!-- Free Tier -->
						<div class="col " id="firstPackege">
							<div class="card mb-5 mb-lg-0">
								<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">FREE</h4>
								<div class="card-body">

									<h5 class="card-title text-muted text-uppercase text-center">Free</h5>
									<h6 class="card-title text-center">PHP<span
												class="card-price text-center">0<span class="period">/month</span></h6>
									<h6 class="card-title text-muted text-uppercase text-center">No Payment Required</h6>
									<hr>
									<ul class="fa-ul">
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>1GB Storage</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting in Private Mode (with Select Members of the Group only)</li>

										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 6 Group Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting with Mute/Unmute Select Members Feature</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 5 Task Assignments/To Do's per Registered Member</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>

										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Priority Support </li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Monthly Subscription</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Customize your group page (background and fonts)</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>

									</ul>
									<div class="d-grid">
										<button  style="width: 100%;background: #1887f2 !important; "  onclick="choosePayment(1)" class="btn btn-primary text-uppercase" id="firstpage_button">Choose</button>
									</div>
								</div>
							</div>
						</div>
						<!-- Plus Tier -->
						<div class="col ">
							<div class="card mb-5 mb-lg-0">
								<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
								<div class="card-body">

									<h5 class="card-title text-muted text-uppercase text-center">Plus</h5>
									<h6 class="card-title text-center">PHP<span
												class="card-price text-center">599<span class="period">/month</span></h6>

									<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

									<hr>
									<ul class="fa-ul">
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>10GB Storage</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


									</ul>
									<div class="d-grid">
										<a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment(2)" class="btn btn-primary text-uppercase">Choose</a>
									</div>
								</div>
							</div>
						</div>
						<!-- Pro Tier -->
						<div class="col ">
							<div class="card mb-5 mb-lg-0">
								<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
								<div class="card-body">
									<h5 class="card-title text-muted text-uppercase text-center">PRO</h5>
									<h6 class="card-title text-center">PHP<span
												class="card-price text-center">899<span class="period">/month</span></h6>

									<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

									<hr>
									<ul class="fa-ul">
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>30GB Storage</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>5 Sub-groups for multiple divisions / department's use</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Sub-groups own set of 6 Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


									</ul>
									<div class="d-grid">
										<a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment(3)" class="btn btn-primary text-uppercase">Choose</a>
									</div>
								</div>
							</div>
						</div>
			</section>

			<style>
				section.pricing {
					background: #007bff;
					background: linear-gradient(to right, #33AEFF, #398cb3);
				}

				.pricing .card {
					border: none;
					border-radius: 1rem;
					transition: all 0.2s;
					box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
				}

				.pricing hr {
					margin: 1.5rem 0;
				}

				.pricing .card-title {
					margin: 0.5rem 0;
					font-size: 0.9rem;
					letter-spacing: .1rem;
					font-weight: bold;
				}

				.pricing .card-price {
					font-size: 3rem;
					margin: 0;
				}

				.pricing .card-price .period {
					font-size: 0.8rem;
				}

				.pricing ul li {
					margin-bottom: 1rem;
				}

				.pricing .text-muted {
					opacity: 0.7;
				}

				.pricing .btn {
					font-size: 80%;
					border-radius: 5rem;
					letter-spacing: .1rem;
					font-weight: bold;
					padding: 1rem;
					opacity: 0.7;
					transition: all 0.2s;
				}

				/* Hover Effects on Card */

				@media (min-width: 992px) {
					.pricing .card:hover {
						margin-top: -.25rem;
						margin-bottom: .25rem;
						box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.3);
					}

					.pricing .card:hover .btn {
						opacity: 1;
					}
				}
			</style>

		</div>
	</div>
</div>
<script>


</script>
