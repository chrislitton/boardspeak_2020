<div class="item created_topic_data_item_template" style="display:none;">
    <div class="board_item">
        <figure>
            <div class="lock_indicator" style="display:none">
				<i class="fas fa-lock"></i>
			</div>
            <figcaption>
                <div class="save_follow">
                    <a href="">
                        <i class="fas fa-file-download"></i>
                        save
                    </a>
                    <a href="" class="d-none">
                        <i class="fas fa-user-plus"></i>
                        follow
                    </a>
                    <a href="" class="d-inline">
                        <i class="fas fa-user-plus"></i>
                        Invite
                    </a>
                </div>
                <div class="more_option">
                    more
                    <i class="fas fa-ellipsis-v"></i>

                    <ul class="more_option_item">
                        <li>
                            <a href="">Share</a>
                        </li>
                        <li>
                            <a href="">Pin</a>
                        </li>
                        <li>
                            <a href="">Add to Favorites</a>
                        </li>
                        <li>
                            <a href="">Quick View</a>
                        </li>
                    </ul>
                </div>
            </figcaption>
            <a class="topic_link" href="">
                <img class="img-fluid" src="" alt="board-img">
            </a>
        </figure>
        <summary class="board_info">
            <h4 class="board_metta">
                <a href="">Subgroup</a>
                <a class="d-none" id="pinned_board">
                    <i class="fas fa-thumbtack"></i>
                </a>
                <a id="board_comment">
                    <span>0</span> <i class="fas fa-comment"></i>
                </a>
                <a id="board_like">
                    <span></span> <i class="fas fa-heart"></i>
                </a>
            </h4>
            <h3 class="board_title">
                <a class="topic_link topic_name" href=""></a>
            </h3>
        </summary>
    </div>
</div>

<div class="item created_topic_no_data_item_template" style="display:none;">
    <div class="default_item create_item" style="    background-color: rgb(255 255 255 / 34%);
    box-shadow: 1px 1px 1px 1px #d5cece;
    margin: 1px;">
        <div class="default_text" style="padding-bottom: 70px; background-image: url(<?php echo base_url() . 'img/home/filters.png'; ?>);">
            <h4 style="color: #3e3c3c; !important;">
                No subgroups found
            </h4>
        </div>
    </div>
</div>

<div class="item default_create_topic_item"  style="display:none;">
    <div class="default_item create_topic">
        <div class="default_text">
            <h4>
                Stop chaos and noise!
            </h4>
            <p>
                Create SubGroups for your select divisions or departments.
                <br/>
                <sub>You may select members for your private SubGroups.</sub>
            </p>
        </div>
        <div class="default_btn">
            <a class="btn" href="<?php echo base_url(); ?>account/create/topic" title="">
                Create SubGroup
            </a>
        </div>
    </div>
</div>
