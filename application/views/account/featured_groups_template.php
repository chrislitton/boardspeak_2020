
<div class="item featured_group_data_item_template" style="display:none;">
    <div class="board_item" style="border:1px solid #ccc;">
        <figure>
            <div class="lock_indicator" style="display:none">
				<i class="fas fa-lock"></i>
			</div>
            <figcaption>
                <div class="save_follow">
                    <a href="">
                        <i class="fas fa-file-download"></i>
                        save
                    </a>
                    <a href="" class="d-none">
                        <i class="fas fa-user-plus"></i>
                        follow
                    </a>
                    <a href="" class="d-inline">
                        <i class="fas fa-user-plus"></i>
                        Invite
                    </a>
                </div>
                <div class="more_option">
                    more
                    <i class="fas fa-ellipsis-v"></i>
                    <ul class="more_option_item">
                        <li>
                            <a href="">Share</a>
                        </li>
                        <li>
                            <a href="">Pin</a>
                        </li>
                        <li>
                            <a href="">Add to Favorites</a>
                        </li>
                        <li>
                            <a href="">Quick View</a>
                        </li>
                    </ul>
                </div>
            </figcaption>
            <a class="group_link" href="">
                <img class="img-fluid" src="" alt="board-img">
            </a>
        </figure>
        <summary class="board_info">
            <h4 class="board_metta">
                <a href="">Group</a>
                <a class="d-none" id="pinned_board">
                    <i class="fas fa-thumbtack"></i>
                </a>
                <a id="board_comment">
                    <span>0</span> <i class="fas fa-comment"></i>
                </a>
                <a id="board_like">
                    <span></span> <i class="fas fa-heart"></i>
                </a>
            </h4>
            <h3 class="board_title">
                <a class="group_link group_name" href="">
                </a>

            </h3>
			<div class="text-center pb-4 pt-4">
				<button type="button" class="btn btn-create-post" style="background-color: #F8B84C; border-color: #004C83; color: #656565!important;
							 border-radius: 50px!important; width: 250px; height: 50px; font-size: 20px;font-family: lato-regular;">Join group</button>
			</div>

        </summary>
    </div>
</div>

<div class="item featured_group_no_data_item_template" style="display:none;">
    <div class="default_item create_item" style="    background-color: rgb(255 255 255 / 34%);
    box-shadow: 1px 1px 1px 1px #d5cece;
    margin: 1px;;">
        <div class="default_text" style="padding-bottom: 70px; background-image: url(<?php echo base_url() . 'img/home/filters.png'; ?>);">
            <h4 style="color:  #30bfd8; !important;">
                No groups found
            </h4>
            <p>
                Explore and join groups
            </p>
        </div>
    </div>
</div>
