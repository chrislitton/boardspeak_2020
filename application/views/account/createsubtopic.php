<?php
	echo form_open_multipart('account/savesubtopic');
	echo form_hidden('GroupID', $GroupID);
	echo form_hidden('TopicID', $TopicID);
?>



<section  class="dethead">
    <div class="container">
			<div class="row">
				<div class="col-lg-9">


<!-- Masthead -->
<header id="groupbanner" class="mb-4">
   <div class="container">
  			<div class="row">
        		<div class="col-md-12 col-lg-12 board-banner text-right bg-subtopic" id="site-banner">
						<div class="post-title text-white">
							New Sub-Topic
						</div>
						<div class="mt-3 changebgbtn">

							<div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
								<div class="input-group">
									<div class="input-group-prepend">
												<span class="input-group-text rounded-0 btn btn-default btn-file">Upload Image
													<input type="file" id="flebg" name="userfile">
												</span>
									</div>
									<input id="fleName" type="text" class="form-control" readonly>
								</div>
							</div>
							</div>

						</div>
  				</div>
	     	</div>
	</div>
</header>
 
  
<!-- Contact Section -->
  <section class="entry-section">
    <div class="container">
      
      <div class="row">
        <div class="col-lg-12 mx-auto text-center mb-1 text-danger">
      	<?php echo $error;?>
      	</div>
      </div>

      <div class="row">
        <div class="col-lg-12">

            <div class="control-group d-none">
								<div class="admin-form-group controls mb-0 pb-1">
                <input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
		            </div>
            </div>


						<div class="control-group">
								<div class="admin-form-group controls mb-0 pb-1">
                <input class="form-control" id="txtTitle" name="txtTitle" type="text" placeholder="Enter Sub-Topic Name" required="required">
		            </div>
            </div>

					<div class="control-group mb-3">
              <div class="admin-form-group controls mb-0 pb-1">
								<h5 class="text-muted pt-3 pb-0 mb-0">Privacy</h5>

								<!-- Default unchecked -->
								<div class="custom-control custom-radio py-2">
									<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="private" checked>
									<label class="custom-control-label" for="radPublic">Public <br><i class="text-muted">Anyone can find the group, see who's in it ang what they post</i></label>
								</div>

								<!-- Default checked -->
								<div class="custom-control custom-radio pb-2">
									<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="public">
									<label class="custom-control-label" for="radPrivate">Private <br><i class="text-muted">Anyone can find the group and see who runs it. Only members can see who's in it and what they post.</i></label>
								</div>

								<!-- Default checked -->
								<div class="custom-control custom-radio pb-2">
									<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret">
									<label class="custom-control-label" for="radSecret">Secret <br><i class="text-muted">Only members can see the group, who's in it and what they post.</i></label>
								</div>

						</div>
					</div>



						<div class="control-group mt-2">
								<div class="admin-form-group">
									<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Create</button>
								</div>
						</div>
			
          
            


				</div>
				
      </div>  
      
      </form> 

    </div>
  </section>


    
			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5">
				<h5 class="font-weight-normal text-primary">Creating Sub-Topic</h5>
				<ol>
					<li>Instruction 1</li>
					<li>Instruction 2</li>
					<li>Instruction 3</li>
					<li>Instruction 4</li>
					<li>Instruction 5</li>
				</ol>
				</div>


				<div class="sidebar bg-light">
				<h5 class="font-weight-normal text-primary">Categories</h5>
				<ul>
					<li>Instruction 1</li>
					<li>Instruction 2</li>
					<li>Instruction 3</li>
					<li>Instruction 4</li>
					<li>Instruction 5</li>
				</ul>
				</div>

			
			</div>
    </div>
    </div>
  </section>


