<div class="created_post_data_item_template post_item card" style="display:none">
    <div class="post_img">
        <div class="lock_indicator" style="display:none">
            <i class="fas fa-lock"></i>
        </div>
        <a class="post_link post_title" href="">
            <img class="card-img-top" src="" alt="Card image cap">
        </a>
        <div class="post_img_inner">
            <div class="save_follow">
                <a href="">
                    <i class="fas fa-file-download"></i>
                    save
                </a>
                <a href="" class="d-inline">
                    <i class="fas fa-user-plus"></i>
                    follow
                </a>
                <a href="" class="d-none">
                    <i class="fas fa-user-plus"></i>
                    Invite
                </a>
            </div>
            <div class="more_option">
                more
                <i class="fas fa-ellipsis-v"></i>
                <ul class="more_option_item">
                    <li>
                        <a href="">Share</a>
                    </li>
                    <li>
                        <a href="">Pin</a>
                    </li>
                    <li>
                        <a href="">Add to Favorites</a>
                    </li>
                    <li>
                        <a href="">Quick View</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="post_summary">
        <h4 class="board_metta">
            <a href="">Post</a>
            <a class="d-none" id="pinned_board">
                <i class="fas fa-thumbtack"></i>
            </a>
            <a id="board_comment">
                <span>0</span> <i class="fas fa-comment"></i>
            </a>
            <a id="board_like">
                <span></span> <i class="fas fa-heart"></i>
            </a>
        </h4>
        <p style=" width:100%;  white-space: nowrap;     overflow: hidden;" ><a class="post_title post_link"  href="" ></a>
        </p>
        <p class="post_text post_description"></p>
        <a href="" class="author_info">
            <img class="img-fluid"  src="" alt="author">
            <div class="author_name">
                <h5></h5>
                <p ></p>
            </div>
        </a>
    </div>
</div>

<div class="item created_post_no_data_item_template" style="display:none;">
    <div class="default_item create_item" style="    background-color: rgb(255 255 255 / 34%);
    box-shadow: 1px 1px 1px 1px #d5cece;
    margin: 1px;">
        <div class="default_text" style="padding-bottom: 70px; background-image: url(<?php echo base_url() . 'img/home/filters.png'; ?>);">
            <h4 style="color: #30bfd8; !important;">
                No posts found
            </h4>
        </div>
    </div>
</div>

<div class="item default_create_post_item" style="display:none;">
    <div class="default_item create_post">
        <div class="default_text">
            <h4>
                Refine your Group Conversations
            </h4>
            <p>
                With focused and relevant content
            </p>
        </div>
        <div class="default_btn">
        <a class="btn" href="<?php echo base_url(); ?>account/create/post" title="">
                Create Post
            </a>
        </div>
    </div>
</div>
