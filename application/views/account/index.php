<!-- Masthead -->
<header class="dethead mb-0 pb-0">
    <div class="container-fluid px-5">
		<div class="row mb-3">
			<div class="col-lg-12 text-center">
				<h5 class="py-3">
					<a class="d-inline under-link px-2" href="<?php echo base_url(); ?>account/followed">Followed</a>
					<a class="d-inline under-link px-2 active underlined" href="<?php echo base_url(); ?>account">Explore</a>
				</h5>
			</div>
		</div>
	</div>
</header>

<section class="board-section" id="boards">
  	<div class="container-fluid px-5">

	  	<div class="carousel-standard">

	  		<!-- START OF POST -->
			<div class="row mb-2">
				<div class="col-lg-9">
					<a href="<?php echo base_url(); ?>account/all/posts" class="muted-link">
						<h5 class="pt-2">Posts <i class="fa fa-chevron-right ml-2 showall-icon"></i><span class="item-count">see all</span></h5>

					</a>
				</div>
				<div class="col-lg-3">

					<?php echo form_open('account/all/posts'); ?>
					<div class="search-small">
						<div class="input-group">
							<input type="text" id="txtSearchPost" name="txtSearchPost" class="form-control input-box" placeholder="Search" required='required'>						
							<div class="input-group-append">
								<button type="submit" class="btn btn-primary"style="padding:0;"><i class="fa fa-search py-1 px-2 text-white"></i></button>
							</div>
						</div>
					</div>

				</div>
			</div>

	  		<div class="row mb-4">
				<div class="col-lg-12">
					<div class="carousel slider">

		          	<?php
		              	foreach($posts_items as $item):
		              		if ($item['Searchable']) :
								$Name = $item['Po_Title'];
								if (strlen($Name)>=50) $Name = substr($Name,0,50)."... ";

								$Survey = "";
								$PostLink = base_url().'account/view/post/'. $item['EncodedID'];
								if ($item['Po_Survey']!=0)
								{
									$Survey = "<div class='admin-img-box-survey bg-success text-white px-2 py-1'>Survey</div>";
									if (strcmp($item['Po_SurveyStatus'],'started')==0) $PostLink = base_url().'account/survey/start/'. $item['Po_ID'];
									else  $PostLink = base_url().'account/survey/edit/'. $item['EncodedID'];
								}
					?>
								<div class="item position-relative">
								<?php echo $Survey;?>

									<?php
										if ($item['AskForPermission']) 
											echo '<a class="board-link" href="#" data-toggle="modal" data-target="#subtopicAccessModal" data-id="'.$item['Po_ID'].'">';
										else
											echo '<a class="board-link" href="'. $PostLink.'">';
									?>
									<!-- <a class="board-link" href="<?php echo $PostLink;?>"> -->
										<div class="shadow-sm bg-post <?php echo $item['Po_Backcolor']; ?> board-link-content" style="background:transparent url('<?php echo $item['Po_Thumb'];?>') no-repeat center center /cover">
											<img class="bg-post" src="<?php echo $item['Po_Thumb'];?>" alt="image" onerror="this.src='<?php echo base_url(); ?>img/post.png'"/>

											<?php if (strcasecmp($item['Po_Privacy'], "public") != 0) : ?>
												<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image"/>
											<?php endif; ?>
										</div>
										<p><?php echo $Name;?></p>
									</a>
								</div>
					<?php 
							endif;
						endforeach; 
					?>
			       	</div>
		  		</div>
		  	</div>

		  	<!-- START OF PUBLIC GROUPS -->
			<div class="row mb-2">
				<div class="col-lg-9">
					<a href="<?php echo base_url(); ?>account/all/groups/public" class="muted-link">
						<h5>Public Groups <i class="fa fa-chevron-right ml-2 showall-icon"></i><span class="item-count">see all</span></h5>
					</a>
				</div>
				<div class="col-lg-3">

					<?php	echo form_open('account/all/groups/public');?>
					<div class="search-small">
						<div class="input-group">						
							<input type="text" id="txtSearchGroup" name="txtSearchGroup" class="form-control input-box" placeholder="Search" required='required'>						
							<div class="input-group-append">
								<button type="submit"class="btn btn-primary"style="padding:0;"><i class="fa fa-search py-1 px-2 text-white"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>

		  	<div class="row mb-4">
		  		<div class="col-lg-12">
		    	  	<div class="carousel slider">

              		<?php foreach($public_items as $item) : ?>
						<div class="item">
							<a class="board-link" href="<?php echo base_url(); ?>account/view/group/<?php echo $item['EncodedID'];?>">
								<div class="shadow-sm bg-group <?php echo $item['Gr_Backcolor']; ?> board-link-content" style="background:transparent url('<?php echo $item['Gr_Thumb'];?>') no-repeat center center /cover">
									<img class="bg-group" src="<?php echo $item['Gr_Thumb'];?>" alt="image" onerror="this.src='<?php echo base_url(); ?>img/group.png'"/>
								</div>
							<p><?php echo $item['Gr_Name'];?></p>
							</a>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
	     	</div>
	     	<!-- END OF PUBLIC GROUPS -->

		  	<!-- START OF PRIVATE GROUPS -->
			<div class="row mb-2">
				<div class="col-lg-9">
						<a href="<?php echo base_url(); ?>account/all/groups/private" class="muted-link">
							<h5>Private Groups <i class="fa fa-chevron-right ml-2 showall-icon"></i><span class="item-count">see all</span></h5>
						</a>
				</div>
				<div class="col-lg-3">

					<?php	echo form_open('account/all/groups/private');?>
					<div class="search-small">
						<div class="input-group">
							<input type="text" id="txtSearchGroup" name="txtSearchGroup" class="form-control input-box" placeholder="Search" required='required'>
							<div class="input-group-append">
								<button type="submit"class="btn btn-primary"style="padding:0;"><i class="fa fa-search py-1 px-2 text-white"></i></button>
							</div>
						</div>
					</div>
					</form>

				</div>
			</div>


		  	<div class="row mb-4">
		  		<div class="col-lg-12">
					<div class="carousel slider">

		         	<?php
			            foreach($private_items as $item) : 
							$Name = $item['Gr_Name'];
							if (strlen($Name)>=50) $Name = substr($Name,0,50)."... ";
					?>
							<div class="item">

								<?php
									if ($item['AskForPermission']) 
										echo '<a class="board-link" href="#" data-toggle="modal" data-target="#groupAccessModal" data-id="'.$item['Gr_ID'].'">';
									else
										echo '<a class="board-link" href="'. base_url() .'account/view/group/'.$item['EncodedID'].'">';
								?>
									<div class="shadow-sm bg-group <?php echo $item['Gr_Backcolor']; ?> board-link-content" style="background:transparent url('<?php echo $item['Gr_Thumb'];?>') no-repeat center center /cover">
										<img class="bg-group" src="<?php echo $item['Gr_Thumb'];?>" alt="image" onerror="this.src='<?php echo base_url(); ?>img/group.png'"/>

										<?php if (strcasecmp($item['Gr_Privacy'], "public") != 0) : ?>
											<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image"/>
										<?php endif; ?>
									</div>
									<p><?php echo $Name;?></p>
								</a>
							</div>
		           	<?php endforeach; ?>
			       	</div>
		  		</div>
		  	</div>
		  	<!-- END OF PRIVATE GROUPS -->


		  	<!-- START OF PUBLIC TOPICS -->
			<div class="row mb-2">
				<div class="col-lg-9">
					<a href="<?php echo base_url(); ?>account/all/topics/public" class="muted-link">
						<h5>Public Topics <i class="fa fa-chevron-right ml-2 showall-icon"></i><span class="item-count">see all</span></h5>
					</a>
				</div>
				<div class="col-lg-3">

					<?php	echo form_open('account/all/topics/public'); ?>
					<div class="search-small">
						<div class="input-group">
							<input type="text" id="txtSearchPublicTopic" name="txtSearchTopic" class="form-control input-box" placeholder="Search" required='required'>
							<div class="input-group-append">
								<button type="submit"class="btn btn-primary"style="padding:0;"><i class="fa fa-search py-1 px-2 text-white"></i></button>
							</div>
						</div>
					</div>
					</form>

				</div>
			</div>

			<div class="row mb-4">
		  		<div class="col-lg-12">
					<div class="carousel slider">

		        	<?php
		                foreach($publictopics_items as $item) :

							if ($item['Searchable']) :
								$Name = $item['To_Name'];
								if (strlen($Name)>=50) $Name = substr($Name,0,50)."... ";
					?>
								<div class="item">

										<?php
												if ($item['AskForPermission']) 
													echo '<a class="board-link" href="#" data-toggle="modal" data-target="#topicAccessModal" data-id="'.$item['To_ID'].'">';
												else
													echo '<a class="board-link" href="'. base_url() .'account/view/topic/'.$item['EncodedID'].'">'
										?>
										<div class="shadow-sm bg-topic <?php echo $item['To_Backcolor']; ?> board-link-content" style="background:transparent url('<?php echo $item['To_Thumb'];?>') no-repeat center center /cover">
											<img class="bg-topic" src="<?php echo $item['To_Thumb'];?>" alt="image" onerror="this.src='<?php echo base_url(); ?>img/topic.png'"/>
										</div>
									<p><?php echo $Name;?></p>
									</a>
								</div>
		          	<?php 
		          			endif;
		          		endforeach; 
		          	?>
		       		</div>
		  		</div>
		  	</div>
		  	<!-- END OF PUBLIC TOPICS -->


		  	<!-- START OF PRIVATE TOPICS -->
			<div class="row mb-2">
				<div class="col-lg-9">
						<a href="<?php echo base_url(); ?>account/all/topics/private" class="muted-link">
							<h5>Private Topics <i class="fa fa-chevron-right ml-2 showall-icon"></i><span class="item-count">see all</span></h5>
						</a>
				</div>
				<div class="col-lg-3">

					<?php echo form_open('account/all/topics/private');?>
					<div class="search-small">
						<div class="input-group">
							<input type="text" id="txtSearchPrivateTopic" name="txtSearchTopic" class="form-control input-box" placeholder="Search" required='required'>
							<div class="input-group-append">
								<button type="submit"class="btn btn-primary"style="padding:0;"><i class="fa fa-search py-1 px-2 text-white"></i></button>
							</div>
						</div>
					</div>
					</form>

				</div>
			</div>

			<div class="row mb-4">
		  		<div class="col-lg-12">
						<div class="carousel slider">

		         <?php
		                foreach($privatetopics_items as $item) :

							if ($item['Searchable']) :
								$Name = $item['To_Name'];
								if (strlen($Name)>=50) $Name = substr($Name,0,50)."... ";
						?>
								<div class="item">

									<?php
											if ($item['AskForPermission']) 
												echo '<a class="board-link" href="#" data-toggle="modal" data-target="#topicAccessModal" data-id="'.$item['To_ID'].'">';
											else
												echo '<a class="board-link" href="'. base_url() .'account/view/topic/'.$item['EncodedID'].'">';
									?>
									<!-- <a class="board-link" href="<?php echo base_url(); ?>account/view/topic/<?php echo $item['To_ID'];?>"> -->
										<div class="shadow-sm bg-topic <?php echo $item['To_Backcolor']; ?> board-link-content" style="background:transparent url('<?php echo $item['To_Thumb'];?>') no-repeat center center /cover">
											<img class="bg-topic" src="<?php echo $item['To_Thumb'];?>" alt="image" onerror="this.src='<?php echo base_url(); ?>img/topic.png'"/>

											<?php if (strcasecmp($item['To_Privacy'], "public") != 0) : ?>
												<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image"/>
											<?php endif; ?>
										</div>
									<p><?php echo $Name;?></p>
									</a>
								</div>
		          <?php 
		          			endif;
		      			endforeach; 
		      		?>
		       	</div>
		  		</div>
		  	</div>
		  	<!-- END OF PRIVATE TOPICS -->
		</div>
	</div>
</section>

<input type="hidden" id="group_id" value="" />

<!-- START OF NO ACCESS TO THE GROUP MODAL -->
<div class="modal fade" id="groupAccessModal" tabindex="-1" aria-labelledby="groupAccessModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-90 m-auto">
      <div class="modal-body pt-2 pb-4">
		<div class="mt-3">
			<h6 class="modal-header-text text-center">This is a Private Group.</h6>
			<div class="text-center viewb_all">
				<button class="btn btn-lg pt-1 col-12 col-lg-8" id="btn-request-access">Request Access</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- END OF NO ACCESS TO THE GROUP MODAL -->

<!-- START OF NO ACCESS TO THE TOPIC MODAL -->
<div class="modal fade" id="topicAccessModal" tabindex="-1" aria-labelledby="topicAccessModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-90 m-auto">
      <div class="modal-body pt-2 pb-4">
		<div class="mt-3">
			<h6 class="modal-header-text text-center">This is a Private Topic.</h6>
			<div class="text-center viewb_all">
				<button class="btn btn-lg pt-1 col-12 col-lg-8" id="btn-request-topic-access">Request Access</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- END OF NO ACCESS TO THE TOPIC MODAL -->

<!-- START OF NO ACCESS TO THE SUBTOPIC MODAL -->
<div class="modal fade" id="subtopicAccessModal" tabindex="-1" aria-labelledby="subtopicAccessModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-90 m-auto">
      <div class="modal-body pt-2 pb-4">
		<div class="mt-3">
			<h6 class="modal-header-text text-center">This is a Private Post.</h6>
			<div class="text-center viewb_all">
				<button class="btn btn-lg pt-1 col-12 col-lg-8" id="btn-request-post-access">Request Access</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- END OF NO ACCESS TO THE SUBTOPIC MODAL -->
