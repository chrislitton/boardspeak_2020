<div class="group_info_container">    <!-- add meber popup -->
    <div class="modal_item invite_members">
        <span class="modal_closer invite_members_closer">
            <i class="fas fa-times"></i>
        </span>
        <h6>
            Add contacts
        </h6>
        <div class="invite_option">
            <span>
                <button class="btn btn_select_role btn-light" data-role="superadmin">
                    Add as Super Admin
                </button>
                <button class="btn btn_select_role btn-light" data-role="admin">
                    Add as Admin
                </button>
                <button class="btn btn_select_role btn-light active" data-role="member">
                    Add as Member
                </button>
                <button class="btn btn_select_role btn-light" data-role="follower">
                    Add as Follower
                </button>
            </span>
        </div>
        <input type="hidden" id="loggedInID" value="<?php echo $this->session->userdata['logged_in']['bs_id']?>">
        <input type="hidden" id="currentGroup">
        <div style="    margin-bottom: 20px; border: 1px solid #ced4da; padding: 5px;"> Note : Please go to group details page to access social media sharing</div>
        <div class="search_member form-group">
            <input class="form-control" type="text" id="userInviteKey">
            <button class="btn btn-info" type="button" id="btnUserInviteSearch">
                search
            </button>
        </div>
        <div class="scroll_body">
            <ul class="suggest_members invite_search_list"></ul>
        </div>
    </div>
    <!-- member role popup -->
    <div class="modal_item user_role">
        <span class="modal_closer invite_members_closer">
            <i class="fas fa-times"></i>
        </span>
        <h6>
            Manage user roles
        </h6>
        <div class="search_member form-group">
            <input class="form-control" type="text" id="user_filter">
            <button class="btn btn-info" type="button" id="btnUserSearch">
                search
            </button>
        </div>
        <ul class="user_role_list">
            <li>
                <a disabled>Roles <i class="fas fa-filter"></i></a>
            </li>
            <li>
                <a class="role_option active" id="tab_all_members" role_tab_id="all_members">All</a>
            </li>
            <li>
                <a class="role_option" id="tab_super_admin" role_tab_id="super_admin">Super Admin</a>
            </li>
            <li>
                <a class="role_option" id="tab_admin" role_tab_id="admin">Admin</a>
            </li>
            <li>
                <a class="role_option" id="tab_member" role_tab_id="member">Members</a>
            </li>
            <li>
                <a class="role_option" id="tab_follower" role_tab_id="follower">Followers</a>
            </li>
            <li>
                <a class="role_option" id="tab_pending" role_tab_id="pending">Join Requests</a>
            </li>
        </ul>
        <div class="scroll_body">
            <div class="suggest_member_tabs">
                <ul id="all_members" class="user_list suggest_members"></ul>
                <ul id="super_admin" class="user_list suggest_members"></ul>
                <ul id="admin" class="user_list suggest_members"></ul>
                <ul id="member" class="user_list suggest_members"></ul>
                <ul id="follower" class="user_list suggest_members"></ul>
                <ul id="pending" class="user_list suggest_members"></ul>
                <ul id="no_role" class="user_list suggest_members"></ul>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('account/user_roles_template'); ?>
