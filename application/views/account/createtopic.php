<style type="text/css">
i.fa-camera {
	margin-right: 10px;
}
</style>
<?php
	echo form_open_multipart('account/create/savetopic');
?>
<div class="fomr d-flex">
	<section  class="dethead">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">

					<!-- Masthead -->
					<header id="groupbanner" class="mb-4">
						<div class="container">
							<div class="row">
								<div class="col-md-12 col-lg-12 text-center">
									<?php

										foreach ($groups_items as $item){
											if( $selecetd == $item['Gr_ID']){ ?>
												<a style="  position: absolute;
    left: 0;
    top: -22px;  float: left;" href="<?php echo base_url().'account/view/group/'.encode_id($item['Gr_ID']) ?>"> <  <?php echo $item['Gr_Name'] ;?></a>

											<?php  }



										}


									?>
									<h3 class="text-primary">Create SubGroup</h3>
								</div>
								<div class="col-md-12 col-lg-12 board-banner text-right bg-topic topic-cover-image" id="site-banner"><!-- 
									<div class="post-title text-white">

									</div> -->
									<div style="box-shadow:none;" class="col-md-12 col-lg-12 board-banner text-center">
										<div class="col-md-12 col-lg-12 text-center">
											<div class="btn btn-primary mt-3"
											style="background: #398cb3a6!important;
                                                                                         border: 2px solid white!important;
                                                                                        
                                                                                         color: white!important;
                                                                                         font-weight: 600!important;

																						 border-radius: 9px!important;"                                                                            border: 2px solid white!important;
                                                                                                                                                                       border-radius: 0px!important;
                                                                                                                                                                       color: white!important;
                                                                                                                                                                       font-weight: 600!important;">
												<form method="post">
													<label class="title-tip title-tip-left"  title="Change Board Cover" for="upload_image"
														   style="margin-bottom:0;">
														<img src="" id="uploaded_image" class="img-responsive img-circle" />
														<!-- <i class="fas fa-camera"></i> -->
														<img src="<?php echo base_url(); ?>assetsofpop/img/replaceCamera.png" alt="Upload">
														<input type="file" name="image" class="image" id="upload_image" style="display:none" accept="image/*" />
														<input type="hidden" name="userfile" id="userfile" />
													</label>
												</form>
											</div>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</header>

					<!-- Contact Section -->
					<section class="entry-section">
						<div class="container">

							<?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>

							
							<div class="row">
								<div class="col-lg-12">

									<div class="control-group d-none">
										<div class="admin-form-group controls mb-0 pb-1">
										<input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
										</div>
									</div>

									<div class="control-group form-group">							
										<label class="form-required font-weight-bold label-h6">Group/Community Name</label>
										<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<select class="custom-select" id="selGroup" name="selGroup" required="required">
													<option value="">Choose group or create new group </option>
													<?php 
														foreach ($groups_items as $item): 
															if ($this->session->userdata['logged_in']['bs_id'] == $item['Gr_Us_ID'] || $selecetd == $item['Gr_ID']) :
																$selected = ($group_details['Gr_ID'] == $item['Gr_ID']) ? "selected='selected'" : '';  
																echo '<option value="'.$item['Gr_ID'].'" '.$selected.' >'.$item['Gr_Name'].'</option>';
															endif;
														endforeach; 
													?>
												</select>
											</div>
										</div>
										<div class="text-right">
											<a class="" href="<?php echo base_url(); ?>account/create/group">Create New Group</a>
										</div>
									</div>

														
									<div class="control-group mb-2 form-group">
										<label class="form-required font-weight-bold label-h6">Subgroup</label>
										<div class="admin-form-group controls mb-0 pb-1">
										<input class="form-control" id="txtTitle" name="txtTitle" type="text" placeholder="Enter Subgroup name" required="required">
										</div>
									</div>

														
									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Description </label><span class="ml-1 text-primary font-weight-normal text-small">You may skip and fill this up later.</span>
										
										<div class="admin-form-group controls mb-0 pb-1">
											<input class="form-control" id="txtDescription" name="txtDescription" type="text" placeholder="What is your Subgroup about?">
										</div>
									</div>

									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Category</label> <span class="ml-1 text-primary font-weight-normal text-small">Categorize your Topic to organize your group communication</span>
										<div class="admin-form-group controls pb-1">
											<div class="input-group">
													<select class="custom-select" id="selCategory" name="selCategory" required="required">
														<?php 
															foreach ($category_items as $item): 
																$selected = (!empty($catId) && $item['Ca_ID'] == $catId) ? "selected" : "";

																echo '<option value="'.$item['Ca_ID'].'" '.$selected.' >'.$item['Ca_Name'].'</option>'; 
															endforeach; 
														?>
														<option value="0">Others, please specify</option>
													</select>
													
											</div>
										</div>
									</div>
												
									<div class="control-group mb-2 form-group" id="selNewCategoryBox" style="display:none;">
										<div class="admin-form-group controls pb-1">
											<input class="form-control" id="txtNewCategory" name="txtNewCategory" type="text" placeholder="Enter new category" disabled="disabled">
										</div>
									</div>

									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Sub-Category</label> <span class="ml-1 text-primary font-weight-normal text-small">Sub-categorize your Topic to organize your group communication</span>
										<div class="admin-form-group controls pb-1">
										<div class="input-group">
												<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required">
												</select>
										</div>
										</div>
									</div>
												
									<div class="control-group mb-2 form-group" id="selNewSubCategoryBox" style="display:none;">
										<div class="admin-form-group controls pb-1">
											<input class="form-control" id="txtNewSubCategory" name="txtNewSubCategory" type="text" placeholder="Enter new sub-category" disabled="disabled">
										</div>
									</div>

									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Keyword Tags</label><span class="ml-1 text-primary font-weight-normal text-small">Tags will help people find your subgroup. Separate tags with comma.</span>
										<div class="admin-form-group controls mb-0 pb-1">
											<input class="form-control" id="txtKeywords" name="txtKeywords" type="text">
										</div>
									</div>

									<div class="control-group mb-4 form-group">
											<div class="admin-form-group controls mb-0 pb-1 radio_btn_group">
												<label class="form-required font-weight-bold label-h6">Privacy</label>

												<!-- Default unchecked -->
												<div class="custom-control custom-radio pb-2 radio_btn_item">
													<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public" checked>
													<label class="custom-control-label" for="radPublic">Public </label><br>Anyone in your group can access and join the conversation.
												</div>

												<!-- Default checked -->
												<div class="custom-control custom-radio pb-2 radio_btn_item">
													<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private">
													<label class="custom-control-label" for="radPrivate">Private </label><br>Anyone in your group can find your subgroup. But access to join conversations, is limited to invited members and approved by admin.
												</div>

												<!-- Default checked -->
												<div class="custom-control custom-radio pb-2 radio_btn_item">
													<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret">
													<label class="custom-control-label" for="radSecret">Secret </label><br>Only those with link and unique password in your group, can find and join your subgroup.
												</div>

										</div>
									</div>



									<div class="control-group mt-5 text-center">
										<div class="admin-form-group">
											<a class="btn btn-light btn-xl mr-1" href="">Cancel</a>
											<button type="submit" class="btn btn-primary btn-xl ml-1" id="sendMessageButton">Create</button>
										</div>
									</div>
									
								
									
								</div>
							</div>

						</div>
					</section>
				</div>
			</div>
		</div>
	</section>
	<div class="right-column">
						<img style="border-radius: 20px; " src="http://localhost/boardspeak_2020/assetsofpop/img/GruppoAd_sidebar.png" alt="">
						</div>
					</div>
	
<style>
.fomr{justify-content:space-evenly;}
.right-column{
	width: 28%;
    background: #f5f5f5;
    padding: 29px;
	border-radius: 1rem;
	margin-top: 25px;"
}
.right-column img{margin:auto;}
@media screen and (max-width:973px) {
	.fomr{
		flex-direction:column;
		
	}
	.right-column{
	width:100%;
}
}</style>

<input type="hidden" name="Gr_ID" id="Gr_ID" value="<?=$ID?>" /> 
<input type="hidden" name="inp_category_id" id="inp_category_id" value="<?=$catId?>" /> 
<input type="hidden" name="inp_subcategory_id" id="inp_subcategory_id" value="<?=$subcatId?>" />
<input type="hidden" name="privatepostlimit" id="privatepostlimit" value="<?php echo $privatepostlimit; ?>">
<input type="hidden" name="limitation" id="limitation" value="<?php echo $cat_limit ?>"
</form> 
  
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
        		<div class="img-container">
            		<div class="row">
                		<div class="col-md-8">
                    		<img src="" id="crop_uploaded_image" />
                		</div>
                		<div class="col-md-4">
                    		<div class="preview"></div>
				      		<div class="modal-footer" style='padding-right: 0px;'>
				      			<button type="button" id="crop" class="btn btn-primary">Upload</button>
				        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				      		</div>
                		</div>
            		</div>
        		</div>
      		</div>
    	</div>
  	</div>
</div>
