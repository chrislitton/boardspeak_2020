<?php
	$show_load_btn = "";
	if (count($posts_items)<12) $show_load_btn = " d-none";
?>

<section class="mt-4">
    <div class="container">
			<div class="row">
				<div class="col-lg-9">






					<!-- Topic Section -->
					<section class="portfolio mb-5" id="topics">
					<div class="container">
						<div class="row bg-light py-1 mb-4">
							<div class="col-lg-4">
								<h5 class="px-2 pt-2">Invite Members</h5>
							</div>
							<div class="col-lg-8 text-right">
									<?php	echo form_open('account/invite/group/'.$groups_items['Gr_ID']);?>
									<div class="search-small">
										<div class="input-group">
											<input type="text" id="txtSearchMember" name="txtSearchMember" value="<?php echo $SearchMember; ?>" class="form-control input-box" placeholder="Search" required='required'>
											<div class="input-group-append">
												<button class="btn btn-primary btn-x" type="submit">Search</button>
											</div>
										</div>
									</div>
									</form>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">

									<?php foreach ($invites_items as $item): ?>

									<?php
										$ShowAction = true;
										$MemberRole = '';
										foreach ($members_items as $member):
											if (strcmp($item['Us_ID'],$member['Me_Us_ID'])==0)
											{
												$ShowAction = false;
												$MemberRole = $member['Me_Role'].'/'.$member['Me_Status'];
												break;
											}
										endforeach;
									?>


									<?php	echo form_open('account/invite/group/'.$groups_items['Gr_ID']);?>
									<?php
										echo form_hidden('txtSearchMember', $SearchMember);
										echo form_hidden('txtInviteUserID', $item['Us_ID']);

										
									?>

											<div class="members">
												<div class="rounded-circle members-photo" style="background:transparent url('<?php echo base_url(); ?>img/nophoto.png') no-repeat center center /cover">
												</div>
												<div class="rounded-circle members-photo" style="background:transparent url('<?php echo $item['Us_Photo']; ?>') no-repeat center center /cover"></div>

												<?php if ($ShowAction==true) { ?>
												<button type="submit" class="btn btn-success btn-small rounded-0 pos-right <?php echo $ShowInviteAction;?>" id="sendMessageButton">Send Invite</button>
												<?php } else { ?>
												<p class='text-muted pos-right'><?php echo $MemberRole; ?></p>
												<?php } ?>

												<p class="members-name">
														<?php echo $item['Us_Name']; ?>
												</p>
											</div>
											
									</form>
									<?php endforeach; ?>


							</div>
						</div>
				</div>
				</section>








			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5">
					<h5 class="font-weight-normal text-primary mb-4">Topics <span class="item-count"><?php echo $groups_items['Gr_Topics']; ?></span>
						<a class="btn btn-primary btn-small rounded-0 pos-right <?php echo $ShowCreateAction;?>" href="<?php echo base_url(); ?>account/createtopic/<?php echo $groups_items['Gr_ID']; ?>"><i class="fa fa-plus px-1"></i> Add</a>
					</h5>

					<?php foreach ($topics_items as $item): ?>

							<div class="members">
								<a class="board-link text-left" href="<?php echo base_url(); ?>account/topic/<?php echo $item['To_ID'];?>">
								<div class="rounded-circle members-photo bg-topic <?php echo $item['To_Backcolor']; ?>" style="background:transparent url('<?php echo $item['To_Thumb']; ?>') no-repeat center center /cover"></div>
								<p class="members-name"><?php echo $item['To_Name']; ?></p>
								</a>
							</div>

					<?php endforeach; ?>
				</div>

				<div class="sidebar bg-light mb-5">
					<h5 class="font-weight-normal text-primary">Members <span class="item-count"><?php echo $groups_items['Gr_Members']; ?></span>
						<a class="btn btn-primary btn-small rounded-0 pos-right <?php echo $ShowInviteAction;?>" href="<?php echo base_url(); ?>account/invite/group/<?php echo $groups_items['Gr_ID'];?>"><i class="fa fa-share px-1"></i> Invite</a>
					</h5>
					<?php foreach ($members_items as $item): ?>

							<div class="members">
								<div class="rounded-circle members-photo" style="background:transparent url('<?php echo base_url(); ?>img/nophoto.png') no-repeat center center /cover">
								</div>
								<div class="rounded-circle members-photo" style="background:transparent url('<?php echo $item['Us_Photo']; ?>') no-repeat center center /cover"></div>
								<p class="members-name">
										<?php echo $item['Us_Name']; ?>
										<?php if (strcmp($item['Me_Role'],'admin')!=0){ ?>
										<a class="editoption p-0 m-0 float-right <?php echo $ShowPolicyAction;?>"  href="#" data-member="<?php echo $item['Us_Name'];?>" data-role="<?php echo $item['Me_Role'];?>" data-memberid="<?php echo $item['Me_Us_ID'];?>" data-id="<?php echo $item['Me_ID'];?>" data-status="<?php echo $item['Me_Status'];?>" data-toggle="modal" data-target="#edit-member">
											<i class="fa fa-edit"></i>
										</a>
										<?php } ?>
										<span class="members-role"><?php echo $item['Me_Role']; ?></span>

										<?php if (strcmp($item['Me_Status'],'approved')!=0){ ?>
										<span class="members-role"><?php echo $item['Me_Status']; ?></span>
										<?php } ?>
							</p>
							</div>

					<?php endforeach; ?>

				</div>

				<div class="sidebar bg-light mb-5">
				<h5 class="font-weight-normal text-primary">Community Policy
						<a class="btn btn-primary btn-small rounded-0 pos-right <?php echo $ShowPolicyAction;?>" href="<?php echo base_url(); ?>account/createpolicy/group/<?php echo $groups_items['Gr_ID']; ?>"><i class="fa fa-plus px-1"></i></a>
					</h5>
				<ol>
					<?php foreach ($policy_items as $item): ?>
							<li>
								<p class="p-0 m-0"><?php echo $item['Po_Title']; ?>
										<a class="btn admin-btn-social p-0 m-0 float-right <?php echo $ShowPolicyAction;?>"  href="#" data-href="<?php echo base_url(); ?>account/deletepolicy/group/<?php echo $item['Po_ID'];?>" data-toggle="modal" data-target="#confirm-delete">
											<i class="fa fa-times text-danger"></i>
										</a>
								</p>
								<p class="p-0 m-0 text-muted"><?php echo $item['Po_Description']; ?></p>
							</li>
					<?php endforeach; ?>
				</ol>
				</div>



			</div>
    </div>
    </div>
  </section>
