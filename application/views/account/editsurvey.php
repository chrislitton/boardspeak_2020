<?php

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$showjoin = "";
?>






					<!-- Posts Section -->
					<section class="user-section entry-section" id="entryform">
						<div class="container">





							<div class="row">
								<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
								<?php echo $error;?>
								</div>
							</div>


							<?php
								echo validation_errors();
								echo form_open_multipart('account/startsurvey');
								echo form_hidden('GroupID', $posts_item['Po_Gr_ID']);
								echo form_hidden('TopicID', $posts_item['Po_To_ID']);
								echo form_hidden('SubTopicID', $posts_item['Po_St_ID']);
								echo form_hidden('PostID', $posts_item['Po_ID']);
							?>


							<div class="row mb-5">
								<div class="col-lg-12">

									<div class="control-group d-none">
										<div class="admin-form-group controls mb-0 pb-1">
											<input class="form-control" id="txtSurvey" name="txtSurvey" value="<?php echo $posts_item['Po_Survey']; ?>" type="text" placeholder="Survey" readonly>
										</div>
									</div>
								</div>

							</div>

							<?php for ($i=1; $i<= $posts_item['Po_Survey']; $i++) { ?>

							<div class="row mb-5">
								<div class="col-lg-12">


									<div class="control-group mt-3">
										<label class="rounded-0 text-muted font-weight-bold" for="txtQuestion<?php echo $i;?>">Question <?php echo $i;?></label>
										<div class="admin-form-group controls mb-0 pb-1">
											<textarea class="form-control" id="txtQuestion<?php echo $i;?>" name="txtQuestion<?php echo $i;?>" rows="3" placeholder="Write your question here" required="required"></textarea>
										</div>
									</div>

									<div class="control-group">
									  <div class="admin-form-group controls mb-0 pb-1">
										<div class="input-group">
												<div class="input-group-prepend">
													<label class="input-group-text rounded-0" for="selType<?php echo $i;?>">Type</label>
												</div>
												<select class="custom-select qtype" id="selType<?php echo $i;?>" name="selType<?php echo $i;?>" data-value="<?php echo $i;?>">
													<option value="multiple">Multiple Choice</option>
													<option value="trueorfalse">True or False</option>
													<option value="fill">Fill In The Box</option>
												</select>
										</div>
									  </div>
									</div>




									<div class="qoptions<?php echo $i;?>">
										<div class="control-group">
											<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<div class="input-group-prepend">
													<label class="input-group-text rounded-0" for="txtA<?php echo $i;?>">1</label>
												</div>
												<input class="form-control muloption<?php echo $i;?>" id="txtA<?php echo $i;?>" name="txtA<?php echo $i;?>" type="text" placeholder="Option 1" required="required">
											</div>
											</div>
										</div>

										<div class="control-group">
											<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<div class="input-group-prepend">
													<label class="input-group-text rounded-0" for="txtB<?php echo $i;?>">2</label>
												</div>
												<input class="form-control muloption<?php echo $i;?>" id="txtB<?php echo $i;?>" name="txtB<?php echo $i;?>" type="text" placeholder="Option 2" required="required">
											</div>
											</div>
										</div>

										<div class="control-group">
											<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<div class="input-group-prepend">
													<label class="input-group-text rounded-0" for="txtC<?php echo $i;?>">3</label>
												</div>
												<input class="form-control muloption<?php echo $i;?>" id="txtC<?php echo $i;?>" name="txtC<?php echo $i;?>" type="text" placeholder="Option 3">
											</div>
											</div>
										</div>

										<div class="control-group">
											<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<div class="input-group-prepend">
													<label class="input-group-text rounded-0" for="txtD<?php echo $i;?>">4</label>
												</div>
												<input class="form-control muloption<?php echo $i;?>" id="txtD<?php echo $i;?>" name="txtD<?php echo $i;?>" type="text" placeholder="Option 4">
											</div>
											</div>
										</div>
									</div>


								</div>
							</div>

							<?php } ?>

							<div class="row mb-5">
								<div class="col-lg-12">
									<div class="admin-form-group">
										<button type="submit" class="btn btn-primary px-4" id="sendMessageButton">Submit and Start Survey</button>
									</div>
								</div>
							</div>

							</form>

							<div class="row msgbox">

								<div class="col-md-12 col-lg-12">


									<?php foreach ($comments_items as $item): ?>

									<div class="row">

										<div class="col-md-12 col-lg-12">
											<?php if(strcmp($UserID,$item['Co_Us_ID'])==0) {?>
												<div class="rounded-circle my-photo" style="background:transparent url('<?php echo $item['Us_Photo']; ?>') no-repeat center center /cover"></div>
												<div class="my-msg">
													<p class="my-title pb-3"><span class="msguser"></span><?php echo $item['Co_Message']; ?></p>
													<p class="my-date my-top-left"><?php echo date( "F d, Y h:i", strtotime($item['Co_DatePosted']) ); ?></p>
												</div>
											<?php }else{?>
												<div class="rounded-circle admin-photo" style="background:transparent url('<?php echo $item['Us_Photo']; ?>') no-repeat center center /cover"></div>
												<div class="admin-msg">
													<p class="board-title pb-3"><span class="msguser"></span><?php echo $item['Co_Message']; ?></p>
													<p class="board-date board-top-right"><?php echo date( "F d, Y h:i", strtotime($item['Co_DatePosted']) ); ?></p>
												</div>
											<?php }?>
										</div>

									</div>

								<?php endforeach; ?>
								</div>
							</div>


						</div>
					</section>







			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5 text-center">

					<a href="<?php echo base_url(); ?>account/group/<?php echo $posts_item['Gr_ID'];?>">
					<div class="board-banner text-right bg-group <?php echo $posts_item['Gr_Backcolor']; ?>"  style="background:transparent url('<?php echo $posts_item['Gr_Thumb'];?>') no-repeat center center /cover">
						<div class="post-title text-white">&nbsp;</div>
					</div>
					</a>

					<h5 class="pt-3"><a href="<?php echo base_url(); ?>account/group/<?php echo $posts_item['Gr_ID'];?>"><?php echo $posts_item['Gr_Name']; ?></a></h5>
					<a class="btn btn-danger w-100 mt-3 <?php echo $showjoin; ?>" href="<?php echo base_url(); ?>account/join/<?php echo $posts_item['Gr_ID']; ?>">Join Group</a>
				</div>


				<div class="sidebar bg-light">
				<h5 class="font-weight-normal text-primary">More Topics</h5>
				<?php foreach ($topics_items as $item): ?>

							<div class="members">
								<a class="board-link text-left" href="<?php echo base_url(); ?>account/topic/<?php echo $item['To_ID'];?>">
								<div class="rounded-circle members-photo bg-topic <?php echo $item['To_Backcolor']; ?>" style="background:transparent url('<?php echo $item['To_Thumb']; ?>') no-repeat center center /cover"></div>
								<p class="members-name"><?php echo $item['To_Name']; ?></p>
								</a>
							</div>

				<?php endforeach; ?>
				</div>


			</div>
    </div>
    </div>
  </section>




