<?php
	$UserID = ($this->session->userdata['logged_in']['bs_id']);
	$show_load_btn = "";
	if (count($posts_items)<12) $show_load_btn = " d-none";
?>

<section class="mt-4">
    <div class="container">
			<div class="row">
				<div class="col-lg-9">

			

					<!-- Posts Section -->
					<section class="user-section entry-section" id="posts">
						<div class="container">

						<div class="row bg-light py-1 mb-4">
							<div class="col-lg-8">
								<h5 class="px-2 pt-2">Posts</h5>
							</div>
							<div class="col-lg-4 text-right">
								<a class="btn btn-outline-primary rounded-0 <?php echo $ShowCreatePostAction;?>" href="<?php echo base_url(); ?>account/createpost/subtopic/<?php echo $subtopics_items['To_ID']; ?>"><i class="fa fa-edit px-1"></i> Create Post</a>
							</div>
						</div>



							<div class="row">

							<?php foreach ($posts_items as $item): ?>


								<div class="col-md-6 col-lg-4 admin-img-box loadmore">
									<div class="admin-img <?php  if (strlen($item['Po_Thumb'])==0) echo 'bg-post'; echo ' '.$item['Po_Backcolor']; ?>">
										<a class="board-link" href="<?php echo base_url(); ?>account/post/<?php echo $item['Po_ID'];?>">
											<img class="img-fluid" src="<?php echo $item['Po_Thumb']; ?>" alt="">
										</a>

										<div class="admin-img-label shadow-sm">
											<div class="row">
												<div class="col-md-12 col-lg-12">
													<a class="board-link" href="<?php echo base_url(); ?>account/post/<?php echo $item['Po_ID'];?>">
													<p class="board-title"><?php echo $item['Po_Title']; ?></p>
													</a>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12 col-lg-12">
													<p class="lead post-info-small text-left text-muted">Posted on <?php echo date( "M d, Y", strtotime($item['Po_DatePosted']) ); ?></p>
												</div>
											</div>

										</div>

								</div>
								</div>

							<?php endforeach; ?>
							<div class="col-md-12 col-lg-12 mt-3 text-center <?php echo $show_load_btn;?>">
								<a class="btn btn-primary px-5" href="#" id="btnload">Load More</a>
							</div>

					</div>

							
				
						</div>
				
						
					</section>
					
					









			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5">
					<h5 class="font-weight-normal text-primary">Members <span class="item-count"><?php echo count($members_items); ?></span>
						<a class="btn btn-primary btn-small rounded-0 pos-right <?php echo $ShowInviteAction;?>" href="#"><i class="fa fa-share px-1"></i> Invite</a>
					</h5>
					<?php foreach ($members_items as $item): ?>

							<div class="members">
								<div class="rounded-circle members-photo" style="background:transparent url('<?php echo base_url(); ?>img/nophoto.png') no-repeat center center /cover">
								</div>
								<div class="rounded-circle members-photo" style="background:transparent url('<?php echo $item['Us_Photo']; ?>') no-repeat center center /cover"></div>
								<p class="members-name">
								<?php echo $item['Us_Name']; ?>
								<?php if (strcmp($item['Me_Role'],'admin')!=0){ ?>
								<a class="editoption p-0 m-0 float-right <?php echo $ShowPolicyAction;?>"  href="#" data-member="<?php echo $item['Us_Name'];?>" data-role="<?php echo $item['Me_Role'];?>" data-memberid="<?php echo $item['Me_Us_ID'];?>" data-id="<?php echo $item['Me_ID'];?>" data-status="<?php echo $item['Me_Status'];?>" data-toggle="modal" data-target="#edit-member">
									<i class="fa fa-edit"></i>
								</a>
								<?php } ?>
								<span class="members-role"><?php echo $item['Me_Role']; ?></span>

										<?php if (strcmp($item['Me_Status'],'pending')==0){ ?>
										<span class="members-role"><?php echo $item['Me_Status']; ?></span>
										<?php } ?>
								</p>
							</div>

					<?php endforeach; ?>
				</div>

				<div class="sidebar bg-light mb-5">
				<h5 class="font-weight-normal text-primary">Sub-Topic Policy
						<a class="btn btn-primary btn-small rounded-0 pos-right <?php echo $ShowPolicyAction;?>" href="<?php echo base_url(); ?>account/createpolicy/subtopic/<?php echo $subtopics_items['To_ID']; ?>"><i class="fa fa-plus px-1"></i></a>
					</h5>
				<ol>
					<?php foreach ($policy_items as $item): ?>
							<li>
								<p class="p-0 m-0"><?php echo $item['Po_Title']; ?>
										<a class="btn admin-btn-social p-0 m-0 float-right <?php echo $ShowPolicyAction;?>"  href="#" data-href="<?php echo base_url(); ?>account/deletepolicy/subtopic/<?php echo $item['Po_ID'];?>" data-toggle="modal" data-target="#confirm-delete">
											<i class="fa fa-times text-danger"></i>
										</a>
								</p>
								<p class="p-0 m-0 text-muted"><?php echo $item['Po_Description']; ?></p>
							</li>
					<?php endforeach; ?>
				</ol>
				</div>



			</div>
    </div>
    </div>
  </section>
