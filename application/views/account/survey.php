<?php

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$showjoin = "";
?>






					<!-- Posts Section -->
					<section class="user-section entry-section" id="entryform">
						<div class="container">





							<div class="row">
								<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
								<?php echo $error;?>
								</div>
							</div>


							<?php
								echo validation_errors();
								echo form_open_multipart('account/submitsurvey');
								echo form_hidden('GroupID', $posts_item['Po_Gr_ID']);
								echo form_hidden('TopicID', $posts_item['Po_To_ID']);
								echo form_hidden('SubTopicID', $posts_item['Po_St_ID']);
								echo form_hidden('PostID', $posts_item['Po_ID']);
							?>


							<div class="row mb-3 d-none">
								<div class="col-lg-12">

									<div class="control-group">
										<div class="admin-form-group controls mb-0 pb-1">
											<input class="form-control" id="txtSurvey" name="txtSurvey" value="<?php echo $posts_item['Po_Survey']; ?>" type="text" placeholder="Survey" readonly>
										</div>
									</div>
								</div>

							</div>


							<?php foreach ($survey_items as $item): ?>

							<div class="row mb-5">
								<div class="col-lg-12">

									<?php
										$i = $item['Su_Item'];
										echo form_hidden('selType'.$i , $item['Su_Type']);
									?>

									<?php
										$Answer = '';
										foreach ($answers_items as $ans):
											if (strcmp($ans['An_Item'],$i)==0)
											{
												$Answer = $ans['An_Answer'];
											}
										endforeach;
									?>


								
									<div class="control-group mt-3">
										<label class="rounded-0 text-muted font-weight-bold" for="txtQuestion<?php echo $i;?>">Question <?php echo $i;?></label>
										<div class="admin-form-group controls mb-0 pb-4">
											<?php echo $item['Su_Question'];?>
										</div>
									</div>

									<?php if (strcmp($item['Su_Type'],'multiple')==0) { ?>
									<?php

											$Options = explode ("|", $item['Su_Options']);
											for ($opt=1; $opt<=count($Options); $opt++)
											{

												$optSelected = '';
												if (strlen($Answer)!=0)
												{
													$optSelected = ' disabled';
													if ($opt==$Answer) $optSelected = ' checked disabled';
												}

									?>

									<!-- Default unchecked -->
									<div class="custom-control custom-radio py-2">
										<input type="radio" class="custom-control-input" id="radOption<?php echo $i;?><?php echo $opt;?>" name="radOption<?php echo $i;?>" value="<?php echo $opt;?>" <?php echo $optSelected;?>>
										<label class="custom-control-label" for="radOption<?php echo $i;?><?php echo $opt;?>"><?php echo $Options[$opt-1];?></label>
									</div>



									<?php } ?>

									<?php
									}
									else if (strcmp($item['Su_Type'],'trueorfalse')==0)
									{

												$radTrueSelected = '';
												$radFalseSelected = '';
												if (strlen($Answer)!=0)
												{
													$radTrueSelected = ' disabled';
													$radFalseSelected = ' disabled';
													if (strcmp($Answer,'true')==0) $radTrueSelected = ' checked disabled';
													else $radFalseSelected = ' checked disabled';
												}
									?>

									<div class="qtruefalse<?php echo $i;?> mb-3">

											<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">

														<div class="custom-control custom-radio py-2">
															<input type="radio" class="custom-control-input" id="radTrue<?php echo $i;?>" name="radTrueFalse<?php echo $i;?>" value="true" <?php echo $radTrueSelected;?>>
															<label class="custom-control-label" for="radTrue<?php echo $i;?>">True</label>
														</div>

														<div class="custom-control custom-radio pb-2">
															<input type="radio" class="custom-control-input" id="radFalse<?php echo $i;?>" name="radTrueFalse<?php echo $i;?>" value="false" <?php echo $radFalseSelected;?>>
															<label class="custom-control-label" for="radFalse<?php echo $i;?>">False</label>
														</div>

													</div>
											</div>

									</div>
									
									<?php
									}
									else
									{
												$textAnswer = '';
												$textEntered = '';
												if (strlen($Answer)!=0)
												{
													$textEntered = ' disabled';
													$textAnswer = $Answer;
												}
										?>

									<div class="qfill<?php echo $i;?> mb-3">

										<div class="control-group">
											<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<div class="input-group-prepend">
													<label class="input-group-text rounded-0" for="txtAnswer<?php echo $i;?>">Answer</label>
												</div>
												<input class="form-control" id="txtAnswer<?php echo $i;?>" name="txtAnswer<?php echo $i;?>" type="text" placeholder="Type your answer here" value="<?php echo $textAnswer;?>" <?php echo $textEntered;?>>
											</div>
											</div>
										</div>

									</div>
									
									<?php } ?>

									
								</div>
							</div>

							<?php endforeach; ?>
							

							<?php if (count($answers_items)==0) { ?>

							<div class="row mb-5">
								<div class="col-lg-12">
									<div class="admin-form-group">
										<button type="submit" class="btn btn-primary px-4" id="sendMessageButton">Submit</button>
									</div>
								</div>
							</div>

							<?php } ?>

							</form>

						</div>
					</section>







			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5 text-center">

					<a href="<?php echo base_url(); ?>account/group/<?php echo $posts_item['Gr_ID'];?>">
					<div class="board-banner text-right bg-group <?php echo $posts_item['Gr_Backcolor']; ?>"  style="background:transparent url('<?php echo $posts_item['Gr_Thumb'];?>') no-repeat center center /cover">
						<div class="post-title text-white">&nbsp;</div>
					</div>
					</a>

					<h5 class="pt-3"><a href="<?php echo base_url(); ?>account/group/<?php echo $posts_item['Gr_ID'];?>"><?php echo $posts_item['Gr_Name']; ?></a></h5>
					<a class="btn btn-danger w-100 mt-3 <?php echo $showjoin; ?>" href="<?php echo base_url(); ?>account/join/<?php echo $posts_item['Gr_ID']; ?>">Join Group</a>
				</div>


				<div class="sidebar bg-light">
				<h5 class="font-weight-normal text-primary">More Topics</h5>
				<?php foreach ($topics_items as $item): ?>

							<div class="members">
								<a class="board-link text-left" href="<?php echo base_url(); ?>account/topic/<?php echo $item['To_ID'];?>">
								<div class="rounded-circle members-photo bg-topic <?php echo $item['To_Backcolor']; ?>" style="background:transparent url('<?php echo $item['To_Thumb']; ?>') no-repeat center center /cover"></div>
								<p class="members-name"><?php echo $item['To_Name']; ?></p>
								</a>
							</div>

				<?php endforeach; ?>
				</div>


			</div>
    </div>
    </div>
  </section>




