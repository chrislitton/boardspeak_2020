<!-- followed explore section -->
<section class="follow_explore_section" style="margin-top: 20px;">

	<div class="follow_explore_header">
		<div class="follow_explore_header_container container">
			<ul class="nav text-center">

				<li class="nav-item">
					<a class="nav-link" id="created_slider">
						Created
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="follow_slider">
						Joined
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link active" id="explore_slider">
						Explore
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="follow_explore_filter">
		<div class="follow_explore_search_view" id="sticky_search">

		<script>
		 function submitform(thisx){

        		 $('#filter_submit').click();
        		  return false;
        	  }
		</script>
			<form class="form-group" onSubmit="return submitform( this) ">
				<input class="form-control" type="text" name="search" id="filter_keyword">
				<button class="btn" type="button" id="filter_submit" style="border-top-left-radius: 0 !important; border-bottom-left-radius: 0 !important;">
 				 search
				</button>
			</form>
			<div class="follow_explore_view" style="display:none">
				<a class="" href="" title="">
					<span>
						List <br> View
					</span>
					<i class="fas fa-list"></i>
				</a>
				<a class="d-none" href="" title="">
					<span>
						Grid <br> View
					</span>
					<i class="fas fa-th"></i>
				</a>
			</div>
		</div>

		<div class="filter_btn_container container">
			<div class="follow_explore_filter_btn text-center owl-carousel owl-theme">
				<button class="btn filter_board filter_btn active" filtered_board="all_board" type="button">
					All
				</button>
				<button class="btn filter_board filter_btn" filtered_board="group_board" type="button">
					Groups
				</button>
				<button class="btn filter_board filter_btn" filtered_board="topic_board" type="button">
					SubGroup
				</button>
				<button class="btn filter_board filter_btn" filtered_board="post_board" type="button">
					Post
				</button>
				<!-- <button class="btn filter_btn" filtered_board="form_board" type="button">
					Forms
				</button>
				<button class="btn filter_btn" filtered_board="event_board" type="button">
					Event
				</button>
				<button class="btn filter_btn" filtered_board="task_board" type="button">
					Task
				</button>
				<button class="btn filter_btn" filtered_board="calender_board" type="button">
					Calender
				</button>
				<button class="btn filter_btn" filtered_board="image_board" type="button">
					Image
				</button> -->
			</div>
		</div>

		<div class="filter_dropdown_container container">
			<div class="filter_dropdown">
				<ul class="main_dropdown_list">
					<li>
						<select id="filter_alpha" class="filter_select filter_dropdown_btn">
							<option value="">Sort Title</option>
							<option value="ASC">A to Z</option>
							<option value="DESC">Z to A</option>
						</select>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_admin">
							By Admin
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_fav">
							Favourite
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_latest">
							Latest
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_saved">
							Saved
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_popular">
							Popular
						</a>
					</li>
					<li>
						<select id="filter_privacy" class="filter_select filter_dropdown_btn">
							<option value="">All Boards</option>
							<option value="public">Public</option>
							<option value="private">Private</option>
							<option value="secret">Secret</option>

						</select>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="follow_explore_container">
		<!-- created board slider -->
		<div id="created_slider_container" class="container">
			<!-- items for filter button -->
			<div class="slider_outer group_board">
				<div id="created_group_slider" class="created_group_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="<?php echo base_url(); ?>account/all/groups" title="">
						View All
					</a>
					<a class="<?= $create_group_btn_class; ?> btn active" href="<?= $create_group_btn_link; ?>">
						Create Group
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer topic_board">
				<div id="created_topic_slider" class="created_topic_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="<?php echo base_url(); ?>account/all/topics" title="">
						View All
					</a>
					<a class="btn active" href="<?php echo base_url(); ?>account/create/topic" title="">
						Create SubGroup
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer post_board">
				<div id="created_post_slider" class="created_post_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="<?php echo base_url(); ?>account/all/posts" title="">
						View All
					</a>
					<a class="btn active" href="<?php echo base_url(); ?>account/create/post" title="">
						Create Post
					</a>
				</div>
			</div>
		</div>

		<!-- followed board slider -->
		<div id="follow_slider_container" class="container">
			<!-- items for filter button -->
			<div class="slider_outer group_board">
				<div class="follow_group_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="create_group_btn btn active">
						Create Group
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer topic_board">
				<div class="follow_topic_slider owl-carousel owl-theme">
					<div class="item">
						<div class="default_item create_topic">
							<div class="default_text">
								<h4>
									Organize your group communication.
								</h4>
								<p>
									Categorize each post and discussions by topic.
								</p>
							</div>
							<div class="default_btn">
								<a class="btn" href="" title="">
									Create SubGroup
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="default_item explore_item">
							<div class="default_text">
								<h4>
									You are not following any boards yet.
								</h4>
								<p>
									Follow or join a topic board and get involved.
								</p>
							</div>
							<div class="default_btn">
								<a class="btn" href="" title="">
									Explore
								</a>
							</div>
						</div>
					</div>
					<!--
					 <div class="item">
                     						<div class="board_item">
                     							<figure>
                     								<figcaption>
                     									<div class="save_follow">
                     										<a href="">
                     											<i class="fas fa-file-download"></i>
                     											save
                     										</a>
                     										<a href="" class="d-none">
                     											<i class="fas fa-user-plus"></i>
                     											follow
                     										</a>
                     										<a href="" class="d-inline">
                     											<i class="fas fa-user-plus"></i>
                     											Invite
                     										</a>
                     									</div>
                     									<div class="more_option">
                     										more
                     										<i class="fas fa-ellipsis-v"></i>

                     										<ul class="more_option_item">
                     											<li>
                     												<a href="">Share</a>
                     											</li>
                     											<li>
                     												<a href="">Pin</a>
                     											</li>
                     											<li>
                     												<a href="">Join</a>
                     											</li>
                     											<li>
                     												<a href="">Quick View</a>
                     											</li>
                     										</ul>
                     									</div>
                     								</figcaption>
                     								<img class="img-fluid" src="<?php echo base_url()."img/topic.png"; ?>" alt="board-img">
                     							</figure>
                     							<summary class="board_info">
                     								<h4 class="board_metta">
                     									<a href="">Subgroup</a>
                     									<a class="d-none" id="pinned_board">
                     										<i class="fas fa-thumbtack"></i>
                     									</a>
                     									<a id="board_comment">
                     										<span>0</span> <i class="fas fa-comment"></i>
                     									</a>
                     									<a id="board_like">
                     										<span>0</span> <i class="fas fa-heart"></i>
                     									</a>
                     								</h4>
                     								<h3 class="board_title">
                     									<a href="">
                     										Your Title Goes Here
                     									</a>
                     								</h3>
                     							</summary>
                     						</div>
                     					</div>
                     					<div class="item">
                     						<div class="board_item">
                     							<figure>
                     								<figcaption>
                     									<div class="save_follow">
                     										<a href="">
                     											<i class="fas fa-file-download"></i>
                     											save
                     										</a>
                     										<a href="" class="d-inline">
                     											<i class="fas fa-user-plus"></i>
                     											follow
                     										</a>
                     										<a href="" class="d-none">
                     											<i class="fas fa-user-plus"></i>
                     											Invite
                     										</a>
                     									</div>
                     									<div class="more_option">
                     										more
                     										<i class="fas fa-ellipsis-v"></i>

                     										<ul class="more_option_item">
                     											<li>
                     												<a href="">Share</a>
                     											</li>
                     											<li>
                     												<a href="">Pin</a>
                     											</li>
                     											<li>
                     												<a href="">Join</a>
                     											</li>
                     											<li>
                     												<a href="">Quick View</a>
                     											</li>
                     										</ul>
                     									</div>
                     								</figcaption>
                     								<img class="img-fluid" src="<?php echo base_url()."img/topic.png"; ?>" alt="board-img">
                     							</figure>
                     							<summary class="board_info">
                     								<h4 class="board_metta">
                     									<a href="">Subgroup</a>
                     									<a class="d-none" id="pinned_board">
                     										<i class="fas fa-thumbtack"></i>
                     									</a>
                     									<a id="board_comment">
                     										<span>0</span> <i class="fas fa-comment"></i>
                     									</a>
                     									<a id="board_like">
                     										<span>0</span> <i class="fas fa-heart"></i>
                     									</a>
                     								</h4>
                     								<h3 class="board_title">
                     									<a href="">
                     										Your Title Goes Here
                     									</a>
                     								</h3>
                     							</summary>
                     						</div>
                     					</div>
                     					<div class="item">
                     						<div class="board_item">
                     							<figure>
                     								<figcaption>
                     									<div class="save_follow">
                     										<a href="">
                     											<i class="fas fa-file-download"></i>
                     											save
                     										</a>
                     										<a href="" class="d-none">
                     											<i class="fas fa-user-plus"></i>
                     											follow
                     										</a>
                     										<a href="" class="d-inline">
                     											<i class="fas fa-user-plus"></i>
                     											Invite
                     										</a>
                     									</div>
                     									<div class="more_option">
                     										more
                     										<i class="fas fa-ellipsis-v"></i>

                     										<ul class="more_option_item">
                     											<li>
                     												<a href="">Share</a>
                     											</li>
                     											<li>
                     												<a href="">Pin</a>
                     											</li>
                     											<li>
                     												<a href="">Join</a>
                     											</li>
                     											<li>
                     												<a href="">Quick View</a>
                     											</li>
                     										</ul>
                     									</div>
                     								</figcaption>
                     								<img class="img-fluid" src="<?php echo base_url()."img/topic.png"; ?>" alt="board-img">
                     							</figure>
                     							<summary class="board_info">
                     								<h4 class="board_metta">
                     									<a href="">Subgroup</a>
                     									<a class="d-none" id="pinned_board">
                     										<i class="fas fa-thumbtack"></i>
                     									</a>
                     									<a id="board_comment">
                     										<span>0</span> <i class="fas fa-comment"></i>
                     									</a>
                     									<a id="board_like">
                     										<span>0</span> <i class="fas fa-heart"></i>
                     									</a>
                     								</h4>
                     								<h3 class="board_title">
                     									<a href="">
                     										Your Title Goes Here
                     									</a>
                     								</h3>
                     							</summary>
                     						</div>
                     					</div>
					 -->
				</div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create SubGroup
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer post_board">
				<div class="follow_post_slider owl-carousel owl-theme">
				 </div>

				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create Posts
					</a>
				</div>
			</div>
		</div>
		
		<!-- explore board slider -->
		<div id="explore_slider_container">

			<section class="user_popular_post" style="display:block;">
				<div class="user_popular_post_header pb-5">
					<h2 class="text-center mb-2">
						Featured Post
					</h2>
				</div>
				<div class="slider_outer topic_board container">
					<div class="explore_popular_post_slider11  owl-carousel owl-theme">


					</div>
				</div>
			</section>
			<!-- featured suggested section -->
			<div class="feat_sugst_section">

				<div class="feat_sugst_header">
					<div class="feat_sugst_header_container container">
						<ul class="nav text-center">
							<li class="nav-item">
								<a class="nav-link active" id="feat_slider">
									Featured
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="sugst_slider">
									Suggested
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="feat_sugst_container container">
					<!-- featured board slider -->
					<div id="feat_slider_container">
						<div class="feat_slider_container owl-carousel owl-theme">
						</div>
					</div>
					
					<!-- suggested board slider -->
					<div id="sugst_slider_container">
						<div class="sugst_slider_container owl-carousel owl-theme">
						</div>
					</div>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer group_board container">
				<div class="explore_group_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="create_group_btn btn active">
						Create Group
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer topic_board container">
				<div class="explore_topic_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create SubGroup
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer post_board container">
				<div class="explore_post_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create Posts
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ad section
<section class="ad_section">
	<div class="container">
		<div class="user_ad_slider owl-carousel owl-theme">
			
		</div>
	</div>
</section>

  popular post section -->


<section class="user_popular_post" style="display:block;">
	<div class="user_popular_post_header pb-5">
		<h2 class="text-center mb-2">
			Popular Post
		</h2>
	</div>
	<div class="slider_outer topic_board container">
	<div class="explore_popular_post_slider1  owl-carousel owl-theme">


	</div>
	</div>
</section>
</main><!-- /maincontent -->

<!-- popup content -->
<div class="popup_container todos_not_popup">
	<div id="todos_not" class="popup_item todos_not">
		<span class="popup_closer todos_popup_closer">
			<i class="cross fas fa-times"></i>
		</span>
		<ul>
			<h4 class="todos_header">
				To do's
			</h4>

			<li>
				<h4 class="todos_name">
					<i class="fas fa-cog"></i>
					<span>
						Mark the to do here.
					</span>
				</h4>
				<div class="todos_action_delete">
					<a href="" id="todos_action" type="button">
						action
					</a>
					<a href="" id="todos_delete" type="button">
						remove
					</a>
				</div>
			</li>
			<li>
				<h4 class="todos_name">
					<i class="fas fa-cog"></i>
					<span>
						This is the second to do.
					</span>
				</h4>
				<div class="todos_action_delete">
					<a href="" id="todos_action" type="button">
						action
					</a>
					<a href="" id="todos_delete" type="button">
						remove
					</a>
				</div>
			</li>
			<li>
				<h4 class="todos_name">
					<i class="fas fa-cog"></i>
					<span>
						This to do is not done yet.
					</span>
				</h4>
				<div class="todos_action_delete">
					<a href="" id="todos_action" type="button">
						action
					</a>
					<a href="" id="todos_delete" type="button">
						remove
					</a>
				</div>
			</li>
			<li>
				<h4 class="todos_name">
					<i class="fas fa-cog"></i>
					<span>
						Try this now.
					</span>
				</h4>
				<div class="todos_action_delete">
					<a href="" id="todos_action" type="button">
						action
					</a>
					<a href="" id="todos_delete" type="button">
						remove
					</a>
				</div>
			</li>
		</ul>
	</div>
</div>
<input type="hidden" id="user_id" value="<?= $CurrUserID; ?>">
<?php $this->load->view('account/created_groups_template'); ?>
<?php $this->load->view('account/created_topics_template'); ?>
<?php $this->load->view('account/created_posts_template'); ?>
<?php $this->load->view('account/featured_groups_template'); ?>
<?php $this->load->view('account/suggested_groups_template'); ?>
<?php $this->load->view('account/item_template'); ?>
<?php $this->load->view('account/group_users_pop_up_on_list'); ?>


<style>

.featured_group_item{
	width:450px!important;
	height:350px!important;

}
.featured_group_item .board_item{

	    height: 390px!important;

}
.featured_group_item .board_item img{

	    height: 293px!important;
	    max-height : 293px!important;

}



</style>
