
<li class="has_role_template" style="display:none">
    <div class="user_name_img">
        <img src="" alt="user">
        <p>
            <span class="member_name"></span>
            <span class="member_job"></span>
        </p>
    </div>
    <div class="role_dropdown exist_role">
        <span>
            <i class="fas fa-angle-down"></i>
        </span>
        <a class="role_opener role_trigger"></a>
        <a class="change_role_btn role_trigger">
            Change Role
        </a>
        <div class="dropdown-menu role_secondery role_options"></div>
    </div>
    <div class="role_label" style="display:none"></div>
</li>

<li class="no_role_template" style="display:none">
    <div class="user_name_img">
        <img src="src/image/user_profile.jpeg" alt="user">
        <p>
            <span class="member_name"></span>
            <span class="member_job"></span>
        </p>
    </div>
    <div id="role_dropdown" class="role_dropdown">
        <span>
            <i class="fas fa-angle-down"></i>
        </span>
        <a class="role_opener role_trigger">
            Choose a role
        </a>
        <div class="dropdown-menu role_secondery role_options"></div>
    </div>
    <div class="role_label" style="display:none"></div>
</li>

<li class="no_result_template" style="display:none;">
    <div class="user_name_img">
        <p>
            <span class="member_none">No users found.</span>
        </p>
    </div>
</li>

<li class="pending_role_template" style="display:none">
    <div class="user_name_img">
        <img src="" alt="user">
        <p>
            <span class="member_name"></span>
            <span class="member_job"></span>
        </p>
    </div>

    <div class="role_dropdown exist_role">
        <span>
            <i class="fas fa-angle-down"></i>
        </span>
        <a class="role_opener role_trigger"></a>
        <a class="change_role_btn role_trigger" style="background: #17a2b8; color: white;">
            Action
        </a>
        <div class="role_secondery role_action"></div>
    </div>
    <div class="role_label" style="display:none"></div>
</li>

<li class="user_invitation_template" style="display:none;">
    <div class="user_name_img">
        <img src="" alt="user">
        <p class="invite_name"></p>
    </div>
    <button class="add_member_btn added_member btn" style="display:none">
        <i class="fas fa-check"></i> Invited
    </button>
    <button class="add_member_btn add_member btn btn-light" style="display:none">
        Invite
    </button>
    <button class="add_member_btn add_member_loading btn btn-light" style="display:none">
        <i class="fas fa-circle-notch fa-spin"></i>
    </button>
</li>
