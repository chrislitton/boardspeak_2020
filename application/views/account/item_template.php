<!-- GROUP ITEM TEMPLATE -->
<div class="item group_data_item_template" style="display:none;">
    <div class="board_item">
        <figure>
            <div class="lock_indicator" style="display:none">
				<i class="fas fa-lock"></i>
			</div>
            <figcaption>
                <div class="save_follow">
                    <a class="d-none">
                        <i class="fas fa-file-download"></i>
                        save
                    </a>
                    <a class="d-none join_group">
                        <i class="fas fa-user-plus"></i>
                        join
                    </a>
                    <a class="d-none invite_to_group">
                        <i class="fas fa-user-plus"></i>
                        invite
                    </a>
                    <a class="d-none pending_join_group">
                        <i class="fas fa-user-plus"></i>
                        pending
                    </a>
                    <a class="d-none follow_group">
                        <i class="fas fa-user-plus"></i>
                        follow
                    </a>
                </div>
                <div class="more_option">
                    more
                    <i class="fas fa-ellipsis-v"></i>
                    <ul class="more_option_item">
                        <li>
                            <a href="">Share</a>
                        </li>
                        <li>
                            <a href="">Pin</a>
                        </li>
                        <li>
                            <a href="">Add to Favorites</a>
                        </li>
                        <li>
                            <a href="">Quick View</a>
                        </li>
                    </ul>
                </div>
            </figcaption>
            <a class="group_link" href="">
                <img class="img-fluid" src="" alt="board-img">
            </a>
        </figure>
        <summary class="board_info">
            <h4 class="board_metta">
                <a href="">Group</a>
                <a class="d-none" id="pinned_board">
                    <i class="fas fa-thumbtack"></i>
                </a>
                <a id="board_comment">
                    <span>0</span> <i class="fas fa-comment"></i>
                </a>
                <a id="board_like">
                    <span></span> <i class="fas fa-heart"></i>
                </a>
            </h4>
            <h3 class="board_title">
                <a class="group_link group_name" href="">
                </a>
            </h3>
        </summary>
    </div>
</div>

<div class="item group_no_data_item_template" style="display:none;">
    <div class="default_item create_item" style="    background-color: rgb(255 255 255 / 34%);
    box-shadow: 1px 1px 1px 1px #d5cece;
    margin: 1px;;">
        <div class="default_text" style="padding-bottom: 70px; background-image: url(<?php echo base_url() . 'img/home/filters.png'; ?>);">
            <h4 style="color: #30bfd8; !important;">
                No groups found
            </h4>
            <p>
                Explore and join groups
			</p>
        </div>
    </div>
</div>

<div class="item group_create_data_item_template" style="display:none;">
    <div class="default_item create_item">
        <div class="default_text">
            <h4>
                Organize your group communication.
            </h4>
            <p>
                Categorize each post and discussions by topic.
            </p>
        </div>
        <div class="default_btn">
            <a class="create_group_btn btn">
                Create Group
            </a>
        </div>
    </div>
</div>

<div class="item group_explore_data_item_template" id="miniDiv" style="display:none;">
    <div class="default_item explore_item">
        <div class="default_text">
            <h4>
                You are not following any boards yet.
            </h4>
            <p>
                Follow or join a group board and get involved.
            </p>
        </div>
        <div class="default_btn">
            <a class="btn" href="" title="">
                Explore
            </a>
        </div>
    </div>
</div>
<!-- END GROUP ITEM TEMPLATE -->

<!-- TOPIC ITEM TEMPLATE -->
<div class="item topic_data_item_template" style="display:none;">
    <div class="board_item">
        <figure>
            <div class="lock_indicator" style="display:none">
				<i class="fas fa-lock"></i>
			</div>
            <figcaption>
                <div class="save_follow">
                    <a href="">
                        <i class="fas fa-file-download"></i>
                        save
                    </a>
                    <a href="" class="d-none">
                        <i class="fas fa-user-plus"></i>
                        follow
                    </a>
                    <a href="" class="d-inline">
                        <i class="fas fa-user-plus"></i>
                        Invite
                    </a>
                </div>
                <div class="more_option">
                    more
                    <i class="fas fa-ellipsis-v"></i>

                    <ul class="more_option_item">
                        <li>
                            <a href="">Share</a>
                        </li>
                        <li>
                            <a href="">Pin</a>
                        </li>
                        <li>
                            <a href="">Add to Favorites</a>
                        </li>
                        <li>
                            <a href="">Quick View</a>
                        </li>
                    </ul>
                </div>
            </figcaption>
            <a class="topic_link" href="">
                <img class="img-fluid" src="" alt="board-img">
            </a>
        </figure>
        <summary class="board_info">
            <h4 class="board_metta">
                <a href="">Subgroup</a>
                <a class="d-none" id="pinned_board">
                    <i class="fas fa-thumbtack"></i>
                </a>
                <a id="board_comment">
                    <span>0</span> <i class="fas fa-comment"></i>
                </a>
                <a id="board_like">
                    <span></span> <i class="fas fa-heart"></i>
                </a>
            </h4>
            <h3 class="board_title">
                <a class="topic_link topic_name" href=""></a>
            </h3>
        </summary>
    </div>
</div>

<div class="item topic_no_data_item_template" style="display:none;">
    <div class="default_item create_item" style="    background-color: rgb(255 255 255 / 34%);
    box-shadow: 1px 1px 1px 1px #d5cece;
    margin: 1px;">
        <div class="default_text" style="padding-bottom: 70px; background-image: url(<?php echo base_url() . 'img/home/filters.png'; ?>);">
            <h4 style="color: #3e3c3c; !important;">
                No subgroups found
            </h4>
        </div>
    </div>
</div>
<!-- END TOPIC ITEM TEMPLATE -->

<!-- POST ITEM TEMPLATE -->
<div class="post_data_item_template post_item card" style="display:none">
    <div class="post_img">
        <div class="lock_indicator" style="display:none">
            <i class="fas fa-lock"></i>
        </div>
        <a class="post_link post_title" href="">
            <img class="card-img-top" src="" alt="Card image cap">
        </a>
        <div class="post_img_inner">
            <div class="save_follow">
                <a href="">
                    <i class="fas fa-file-download"></i>
                    save
                </a>
                <a href="" class="d-inline">
                    <i class="fas fa-user-plus"></i>
                    follow
                </a>
                <a href="" class="d-none">
                    <i class="fas fa-user-plus"></i>
                    Invite
                </a>
            </div>
            <div class="more_option">
                more
                <i class="fas fa-ellipsis-v"></i>
                <ul class="more_option_item">
                    <li>
                        <a href="">Share</a>
                    </li>
                    <li>
                        <a href="">Pin</a>
                    </li>
                    <li>
                        <a href="">Add to Favorites</a>
                    </li>
                    <li>
                        <a href="">Quick View</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="post_summary">
        <h4 class="board_metta">
            <a href="">Post</a>
            <a class="d-none" id="pinned_board">
                <i class="fas fa-thumbtack"></i>
            </a>
            <a id="board_comment">
                <span>0</span> <i class="fas fa-comment"></i>
            </a>
            <a id="board_like">
                <span></span> <i class="fas fa-heart"></i>
            </a>
        </h4>
         <p  style="   white-space: nowrap;     overflow: hidden;"><a class="post_title post_link"  href=""></a>
              <p>
        <p class="post_text post_description"></p>
        <a href="" class="author_info">
            <img class="img-fluid"  src="" alt="author">
            <div class="author_name">
                <h5></h5>
                <p></p>
            </div>
        </a>
    </div>
</div>

<div class="item post_no_data_item_template" style="display:none;">
    <div class="default_item create_item" style= "    background-color: rgb(255 255 255 / 34%);
	box-shadow: 1px 1px 1px 1px #d5cece;
	margin: 1px;">
        <div class="default_text" style="padding-bottom: 70px; background-image: url(<?php echo base_url() . 'img/home/filters.png'; ?>);">
            <h4 style="color: #30bfd8; !important;">
                No posts found
            </h4>
        </div>
    </div>
</div>
<!-- END POST ITEM TEMPLATE -->
