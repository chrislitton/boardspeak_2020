	<?php
		$DetailMenu = (strcmp($selSubMenu,'TopicDetail')==0) ? ' active' : '';
		$PhotoMenu = (strcmp($selSubMenu,'Photo')==0) ? ' active' : '';

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
	?>

				<section class="user-section entry-section" id="entryform">
					<div class="container">
						<div class="row">

								<div class="col-lg-3 mx-auto">
								<nav class="text-uppercase" id="boardsubmenu">
								<ul class="navbar-nav nav" role="tablist" id="navsubtab">
										<li class="nav-item mx-0 mx-lg-1 mb-1">
											<a class="btn-primary px-2 nav-link<?php echo $DetailMenu;?>" id="nav-topicdetail-tab" data-toggle="tab" href="#nav-topicdetail" role="tab" aria-controls="nav-topicdetail" aria-selected="true">Topic Detail</a>
										</li>
										<li class="nav-item mx-0 mx-lg-1 mb-1">
											<a class="btn-primary px-2 nav-link<?php echo $PhotoMenu;?>" id="nav-topicphoto-tab" data-toggle="tab" href="#nav-topicphoto" role="tab" aria-controls="nav-topicphoto" aria-selected="true">Background</a>
										</li>
									</ul>
							</nav>
							</div>


							<div class="col-lg-9 mx-auto">

									<div class="tab-content" id="nav-subtabContent">
										<div class="tab-pane fade show<?php echo $DetailMenu;?>" id="nav-topicdetail" role="tabpanel" aria-labelledby="nav-topicdetail-tab">

											<div class="row">
												<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
												<?php echo $errorTopicDetail;?>
												</div>
											</div>

											<?php
												echo validation_errors();
												echo form_open('account/updatetopicdetail');
												echo form_hidden('TopicID', $topics_items['To_ID']);

												$radPublic = (strcmp($topics_items['To_Privacy'],'public')==0) ? ' checked' : '';
												$radPrivate = (strcmp($topics_items['To_Privacy'],'private')==0) ? ' checked' : '';
												$radSecret = (strcmp($topics_items['To_Privacy'],'secret')==0) ? ' checked' : '';


											?>

											<div class="control-group">
												<div class="admin-form-group controls mb-0 pb-1">
													<label>Topic Name</label>
													<input class="form-control" id="txtName" name="txtName" value="<?php echo $topics_items['To_Name'];?>" type="text" placeholder="Topic Name" required="required" data-validation-required-message="Please enter your topic name.">
												</div>
											</div>

											<div class="control-group mb-3">
													<div class="admin-form-group controls mb-0 pb-1">
														<h5 class="text-muted pt-3 pb-0 mb-0">Privacy</h5>

														<!-- Default unchecked -->
														<div class="custom-control custom-radio py-2">
															<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public" <?php echo $radPublic; ?>>
															<label class="custom-control-label" for="radPublic">Public <br><i class="text-muted">Anyone can find the group, see who's in it ang what they post</i></label>
														</div>

														<!-- Default checked -->
														<div class="custom-control custom-radio pb-2">
															<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" <?php echo $radPrivate; ?>>
															<label class="custom-control-label" for="radPrivate">Private <br><i class="text-muted">Anyone can find the group and see who runs it. Only members can see who's in it and what they post.</i></label>
														</div>

														<!-- Default checked -->
														<div class="custom-control custom-radio pb-2">
															<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" <?php echo $radSecret; ?>>
															<label class="custom-control-label" for="radSecret">Secret <br><i class="text-muted">Only members can see the group, who's in it and what they post.</i></label>
														</div>

												</div>
											</div>


											<br>
											<div id="success"></div>
											<div class="admin-form-group">
												<button type="submit" class="btn btn-primary" id="sendMessageButton">Save Changes</button>
											</div>
											</form>



										</div>
										<div class="tab-pane fade show<?php echo $PhotoMenu;?>" id="nav-topicphoto" role="tabpanel" aria-labelledby="nav-topicphoto-tab">

												<div class="row">
													<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
													<?php echo $errorTopicPhoto;?>
													</div>
												</div>


											<div class="row">
													<div class="col-lg-12 mx-auto text-center mb-1">
													<h5>Choose Background</h5>
													</div>
												</div>


												<div class="row">
												<?php foreach ($backcolors_items as $item): ?>

													<div class="col-md-6 col-lg-4 admin-img-box">
														<a class="board-link" href="<?php echo base_url(); ?>account/settopicbackcolor/<?php echo $topics_items['To_ID'];?>/<?php echo $item['Se_ID'];?>">
														<div class="admin-colorbg <?php echo $item['Se_Value']; ?>">
																<p>&nbsp;</p>
																<div class="admin-img-label shadow-sm">
																	<div class="row">
																		<div class="col-md-12 col-lg-12">
																			<p class="lead board-category"><?php echo $item['Se_Name']; ?></p>
																	</div>
																	</div>
																</div>

														</div>
														</a>
													</div>

												<?php endforeach; ?>
												</div>

														<div class="row">
																<?php foreach ($backgrounds_items as $item): ?>

																<div class="col-md-6 col-lg-4 admin-img-box">
																	<div class="admin-img">
																		<a class="board-link" href="<?php echo base_url(); ?>account/settopicphoto/<?php echo $topics_items['To_ID'];?>/<?php echo $item['Se_ID'];?>">
																			<img class="img-fluid" src="<?php echo $item['Se_Photo']; ?>" alt="">
																		</a>
																		<div class="admin-img-label shadow-sm">
																				<div class="row">
																					<div class="col-md-12 col-lg-12">
																						<p class="lead board-category"><?php echo $item['Se_Name']; ?></p>
																					</div>
																				</div>
																			</div>

																	</div>
																</div>
																<?php endforeach; ?>


														</div>

												<?php
													echo validation_errors();
													echo form_open_multipart('account/changetopicphoto');
													echo form_hidden('GroupID', $topics_items['To_Gr_ID']);
													echo form_hidden('TopicID', $topics_items['To_ID']);
												?>

													<div class="control-group">
														<div class="admin-form-group controls mb-0 pb-1">
															<label>Click upload to select image from your computer</label>
															<div class="input-group">
																<div class="input-group-prepend">
																		<span class="input-group-text rounded-0 btn btn-default btn-file">Upload
																			<input type="file" id="userfile" name="userfile">
																		</span>
																</div>
																<input type="text" class="form-control" readonly>
																<img id='img-upload'/>
															</div>
														</div>
													</div>


													<br>
													<div id="success"></div>
													<div class="admin-form-group">
														<button type="submit" class="btn btn-primary" id="sendMessageButton">Submit</button>
													</div>
												</form>


										</div>
									</div>

							</div>
						</div>
					</div>
				</section>
