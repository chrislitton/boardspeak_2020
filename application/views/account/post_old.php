<?php
if(isset($selMenu)){
	$MessagesMenu = (strcmp($selMenu,'Messages')==0) ? ' active' : '';
	$SettingsMenu = (strcmp($selMenu,'Settings')==0) ? ' active' : '';

	$DetailMenu = (strcmp($selSubMenu,'Detail')==0) ? ' active' : '';
	$PhotoMenu = (strcmp($selSubMenu,'Photo')==0) ? ' active' : '';

	$UserID = ($this->session->userdata['logged_in']['bs_id']);
}

?>
<style>@media screen and (max-width:450px) {
	.nav-pills-mobilee{
		justify-content:center;
	}
}</style>

<!-- Posts Section -->
<section class="user-section entry-section" id="entryform">
	<div class="container">

		<div class="col-md-12 col-lg-12">

			<!-- photos and files-->
			<ul style="white-space: nowrap;
    flex-wrap: nowrap;" class="nav nav-pills-mobilee nav-pills mb-3" id="pills-tab" role="tablist">

				<?php



				if(isset($polldata) && !empty($polldata)){ ?>
					<button class="btn btn-outline-primary" id="pollQuestionButton" data-toggle="modal" data-target="#<?php

					if ($polldata[0]['poll_stop'] == 0){

						if (isset($polluserDetail) && !empty($polluserDetail)){echo 'resultanswer';}else{echo 'poolanswer';}
					}else{
						echo 'resultanswer';
					}
 									?>"  style="margin-bottom: 10px ; margin-right:10px;  height: 41px"><i class="fas fa-poll" style="font-size: 25px;    color: #06cbd1;"></i><span> &nbsp;&nbsp;<?php
							if ($polldata[0]['poll_stop'] == 0){
								if (isset($polluserDetail) && !empty($polluserDetail)){echo 'Poll Result';}else{echo 'Answer Poll';}
							}else{
								echo 'Poll Result';
							}
                      ?></span></button>

				<?php } ?>
				<?php if(count($posts_images) > 0){ ?>
					<li class="nav-item" role="presentation" style="line-height: 10px!important;">
						<a class="nav-link <?php if(count($posts_images) > 0){ ?> active <?php } ?>" id="pills-media-tab" data-toggle="pill" href="#pills-media" role="tab" aria-controls="pills-media" aria-selected="true">
						<span class="mr-1 small">Media</span>
						<span class="badge badge-pill badge-danger small"><?php echo count($posts_images);?></span>
						</a>
					</li>
				<?php } ?>
				<?php if(count($posts_files) > 0){ ?>
					<li class="nav-item" role="presentation" style="line-height: 10px!important;">
						<a class="nav-link <?php if(count($posts_files) > 0 && count($posts_images) == 0){ ?> active <?php } ?>" id="pills-file-tab" data-toggle="pill" href="#pills-file" role="tab" aria-controls="pills-file" aria-selected="false">
							<span class="mr-1 small">Files</span>
							<span class="badge badge-pill badge-danger small"><?php echo count($posts_files);?></span>
						</a>
					</li>
				<?php } ?>
			</ul>

			<div class="tab-content" id="pills-tabContent">

				<div class="tab-pane fade <?php if(count($posts_images) > 0){ ?> show active <?php } ?>" id="pills-media" role="tabpanel" aria-labelledby="pills-media-tab">
					<div id="img_slider_container">
						<div class="img_slider_container owl-carousel owl-theme">
							<?php if(count($posts_images) > 0){ ?>
								<?php foreach ($posts_images as $postImage) { ?>
									<div class="col-auto px-0">
										<a href="<?php echo $postImage['Me_Path'];?>" data-toggle="lightbox" data-gallery="image-gallery">
											<img style="margin:auto;" src="<?php echo $postImage['Me_Path'];?>" class="img-thumbnail p-0 postImagex">
										</a>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</div>

				<div class="tab-pane fade <?php if(count($posts_files) > 0 && count($posts_images) == 0){ ?> show active <?php } ?>" id="pills-file" role="tabpanel" aria-labelledby="pills-file-tab">
					<div id="img_slider_container">
						<div class="img_slider_container owl-carousel owl-theme">
						<?php if(count($posts_files) > 0){ ?>
							<?php foreach ($posts_files as $postFiles) { ?>
								<div class="col-auto px-0">
								<?php
									if ($postFiles['Me_Filetype']  == 'file') {


										?>
										<div class="img-thumbnail p-0 postFile">
											<div class="row">
												<div class="col-5">

													<i style="color:<?php echo getFileTypecolor($postFiles['Me_Path']) ?>" class="fa-4x pl-3 pt-2 pb-3 mt-1 fas fa-file-<?php echo getFileType($postFiles['Me_Path']); ?>"></i>


												</div>
												<div class="col-6 small text-truncate pt-4 pl-0 w-75 pr-0">
													<span class="font-weight-bold"><?php echo $postFiles['Me_Filename'];?></span>
													<div><a href="<?php echo $postFiles['Me_Path'];?>" download="<?php echo $postFiles['Me_Filename'];?>">Download</a></div>
												</div>
											</div>
										</div>
									<?php } ?>

								</div>
							<?php } ?>
						<?php } ?>
					</div>
					</div>
				</div>
			</div>
			<p class="lead"><?php echo $posts_item['Po_Content']; ?></p>
		</div>
		<div class="row mb-1 form-group">

 			<div class="col-md-12 col-lg-12 px-0 py-3">

				<?php echo form_open('account/view/post/'.$EncodedID); ?>
					<div class="control-group input-group col pr-0">

						<input style="width: 69%;" class="form-control" id="txtSearch" name="txtSearch" value="<?php echo isset($txtSearch) ?  $txtSearch : '' ;?>" type="text" placeholder="Search">
						<div class="input-group-append">
							<button id="btn_comment_search"class="btn btn-primary"style="padding:0;" style='border-radius: 0 !important;'><i class="fa fa-search py-1 px-2 text-white"></i></button>
							<button id="btn_comment_search_reset" class="btn btn-danger p-1" style='border-top-left-radius: 0 !important; border-bottom-left-radius: 0 !important;'><i class="fa fas fa-sync  py-1 px-2 text-white"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div id="comments-container" class="jquery-comments"></div>
	</div>
</section>

<input type="hidden" name="Po_ID" id="Po_ID" value="<?=$EncodedID?>" />
<input type="hidden" name="OwnerId" id="OwnerId" value="<?= isset($this->session->userdata['logged_in']['bs_id']) ? $this->session->userdata['logged_in']['bs_id'] : '' ?> "
 />
<input type="hidden" name="PostUdId" id="PostUdId" value="<?=$posts_item['Po_Us_ID']?>"
 />
 <input type="hidden" name="GroupUdId" id="GroupUdId" value="<?=$posts_item['Po_Gr_ID']?>"
  />
  <input type="hidden" name="Groupencode" id="Groupencode" value="<?php echo encode_id($posts_item['Po_Gr_ID']);?>"
    />
   <input type="hidden" name="post_type" id="post_type" value="<?=$posts_item['Po_Privacy']?>"
    />
<input type="hidden" name="userType" id="userType" value="<?php if(isset($userType)){ echo $userType; }?>"/>
<script type="text/javascript">
	var profile_picture = "<?=(!empty($user_data['Us_Photo'])) ? $user_data['Us_Photo'] : ''; ?>";
</script>
<?php
function getFileTypecolor(string $url):string{
	$filename=explode('.',$url);
	$extension=end($filename);

	switch($extension){
		case 'pdf':
			$type='red';
			break;
		case 'docx':
		case 'doc':
			$type='#18a2b8';
			break;
		case 'xls':
		case 'xlsx':
		case 'csv':
			$type='green';
			break;
		case 'mp3':
		case 'ogg':
		case 'wav':
			$type='orange';
			break;
		case 'mp4':
		case 'mov':
			$type='#18a2b8';
			break;
		case 'zip':
		case '7z':
		case 'rar':
			$type='red';
			break;
		case 'jpg':
		case 'jpeg':
		case 'png':
			$type='green';
			break;
		default:
			$type='#18a2b8';
	}

	return $type;
}
function getFileType(string $url):string{
	$filename=explode('.',$url);
	$extension=end($filename);

	switch($extension){
		case 'pdf':
			$type=$extension;
			break;
		case 'docx':
		case 'doc':
			$type='word';
			break;
		case 'xls':
		case 'xlsx':
			$type='excel';
			break;
		case 'mp3':
		case 'ogg':
		case 'wav':
			$type='audio';
			break;
		case 'mp4':
		case 'mov':
			$type='video';
			break;
		case 'zip':
		case '7z':
		case 'rar':
			$type='archive';
			break;
		case 'jpg':
		case 'jpeg':
		case 'png':
			$type='image';
			break;
		default:
			$type='alt';
	}

	return $type;
}
?>

