<!-- main content start here -->
<main id="main" class="main">
<input type="hidden" value="<?php echo $openTab; ?>" id="openTab" >
<!-- banner section -->
<section class="user_banner" id="user_banner">
	<div class="user_banner_container">
		<div class="user_banner_background" style="background: transparent url('<?php echo $users_item['Us_Background']; ?>') no-repeat center center /cover">
<!--			<a href="--><?php //echo base_url(); ?><!--account/settings/background">-->
			<a href="javascript:void(0)"  data-toggle="modal" data-target="#myModal1">
				<button type="button" class="btn btn-info">
					<i class="fas fa-camera"></i>
					<span>
						Change Background
					</span>
				</button>
			</a>
		</div>

		<div class="user_banner_info container">
			<div class="user_profile_img">
				<img class="img-fluid rounded-circle" src="<?php echo $users_item['Us_Thumb']; ?>" alt="">
<!--				<a href="--><?php //echo base_url(); ?><!--account/settings/photo">-->
				<a href="javascript:void(0)"  onclick="changepicker()" data-toggle="modal" data-target="#myModal">
					<i class="fas fa-camera"></i>
					Change Photo
				</a>
			</div>
			<script>
				function  changepicker(){
					$('#flePhoto').show();
					$('#sendMessageButton').hide();
				}
			</script>
			<div class="modal fade" 	style="margin-top: 195px;"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Upload  Image</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
						 	<div class="container">
								<section class="user-section entry-section" id="entryform">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">
<!--												--><?php //if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
<!--												--><?php //if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>


												<?php
												echo validation_errors();
												echo form_open_multipart('account/settings/changephoto');
												?>

												<div class="control-group d-none">
													<div class="admin-form-group controls mb-0 pb-1">
														<input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
													</div>
												</div>

												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Choose Photo</label>
														<div class="input-group">
															<div class=" ">
                                                    <span class="input-group-text   btn btn-default btn-file">Upload Photo
                                                            <input type="file" id="flePhoto" name="userfile">
                                                        </span>
															</div>
															<div class="form-group">
																<button type="submit" class="btn btn-primary ml-3 " id="sendMessageButton">Save Photo</button>
															</div>
															<input type="text" class="form-control" style="display: none;" readonly>

														</div>
														<div class="container">
															<img id='img-upload' crossorigin="ml-4" style="height: 149px!important; width: auto!important;" />
														</div>
													</div>
												</div>
												<br>
												<div id="success"></div>

												</form>

											</div>
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" 	style="margin-top: 195px;"  id="myModal1" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Upload  Image</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
							<div class="container">
								<section class="user-section entry-section" id="entryform">
									<div class="container">
										<div class="row">
											<div class="col-lg-12">


												<?php
												echo validation_errors();
												echo form_open_multipart('account/settings/changebackground');
												?>

												<div class="control-group d-none">
													<div class="admin-form-group controls mb-0 pb-1">
														<input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
													</div>
												</div>

												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Choose Background</label>
														<div class="input-group">
															<div class="input-group-prepend">
                                                    <span class="input-group-text rounded-0 btn btn-default btn-file">Upload Photo
                                                            <input type="file" id="flePhoto" name="userfile">
                                                        </span>
															</div>
															<input type="text" class="form-control" readonly>
															<img id='img-upload'/>
														</div>
													</div>
												</div>
												<br>
												<div id="success"></div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Save Background</button>
												</div>
												</form>


											</div>
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="user_name_designation">
				<h1><?php echo (!empty($users_item['Us_Alias'])) ? $users_item['Us_Alias'] : $users_item['Us_Name']; ?></h1>
				<p>
				<?php echo $users_item['Us_JobTitle']; ?>
					<a href="<?php echo base_url(); ?>account/settings" title="">
						<i class="fas fa-edit"></i>
						Edit Profile
					</a>
				</p>
				<p class="join_for_public">
					<span>Joined <?php echo date('F j, Y', strtotime($users_item['Us_DateTime'])); ?>.</span>
					<span>California, USA</span>
				</p>
				<div class="user_ext_number">
					<a class="contact_popup_opener" href="javascript:void(0)" title="">
						Contacts
						<span id="contacts"></span>
					</a>
					<a href="" title="">
						Joined
						<span id="followed">

						</span>
					</a>
					<a href="" title="">
						Following
						<span id="following">
							1541
						</span>
					</a>
				</div>
				<div class="btn_for_public" style="display:none">

					<div class="d-inline-block">
						<a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							message
						</a>

						<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							<a class="dropdown-item" href="#">
								Add User as Contact
							</a>
							<a class="dropdown-item" href="#">
								Message User
							</a>
						</div>
					</div>

					<a class="btn btn-small" href="" title="">
						follow
					</a>
				</div>
			</div>

			<div class="user_metta">
				<div class="user_metta_1">
					<a href="" title="">
						<span><?php echo $users_item['Us_Groups']; ?></span>
						groups
					</a>
					<a href="" title="">
						<span><?php echo $users_item['Us_Topics']; ?></span>
						Subgroups
					</a>
					<a href="" title="">
						<span><?php echo $users_item['Us_Posts']; ?></span>
						post
					</a>
				</div>
				<div class="user_metta_2">
					<a id="todos_popup_opener" title="">
						to do's
						<span id="todos_popup_counter">
							6
						</span>
					</a>
					<a href="<?php echo base_url(); ?>account/notifications" title="">
						notification
						<span id="newNotification">
							0
						</span>
					</a>
					<a href="<?php echo base_url(); ?>account/notifications" title="">
						Credit
						<span  >
							0
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- followed explore section -->
<section class="follow_explore_section">

	<div class="follow_explore_header">
		<div class="follow_explore_header_container container">
			<ul class="nav text-center">

				<li class="nav-item">
					<a class="nav-link active" id="created_slider">
						Created
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="follow_slider">
						Joined
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="explore_slider">
						Explore
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="follow_explore_filter">
	<script>

	function submitform( ){
		 		$('#filter_submit').click();
			return false;
	}
	</script>
		<div class="follow_explore_search_view" id="sticky_search">
			<form class="form-group"   onSubmit="return submitform( );">
				<input class="form-control" type="text" name="search" id="filter_keyword">
				<button class="btn" type="button" id="filter_submit" style="border-top-left-radius: 0; border-bottom-left-radius: 0;">
					search
				</button>
			</form>
			<div class="follow_explore_view" style="display:none">
				<a class="" href="" title="">
					<span>
						List <br> View
					</span>
					<i class="fas fa-list"></i>
				</a>
				<a class="d-none" href="" title="">
					<span>
						Grid <br> View
					</span>
					<i class="fas fa-th"></i>
				</a>
			</div>
		</div>

		<div class="filter_btn_container container">
			<div class="follow_explore_filter_btn text-center owl-carousel owl-theme">
				<button class="btn filter_board filter_btn active" filtered_board="all_board" type="button">
					All
				</button>
				<button class="btn filter_board filter_btn" filtered_board="group_board" type="button">
					Groups
				</button>
				<button class="btn filter_board filter_btn" filtered_board="topic_board" type="button">
					SubGroup
				</button>
				<button class="btn filter_board filter_btn" filtered_board="post_board" type="button">
					Post
				</button>
				<!-- <button class="btn filter_btn" filtered_board="form_board" type="button">
					Forms
				</button>
				<button class="btn filter_btn" filtered_board="event_board" type="button">
					Event
				</button>
				<button class="btn filter_btn" filtered_board="task_board" type="button">
					Task
				</button>
				<button class="btn filter_btn" filtered_board="calender_board" type="button">
					Calender
				</button>
				<button class="btn filter_btn" filtered_board="image_board" type="button">
					Image
				</button> -->
			</div>
		</div>

		<div class="filter_dropdown_container container">
			<div class="filter_dropdown">
				<ul class="main_dropdown_list">
					<li>
						<select id="filter_alpha" class="filter_select filter_dropdown_btn">
							<option value="">Sort Title</option>
							<option value="ASC">A to Z</option>
							<option value="DESC">Z to A</option>
						</select>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_admin">
							By Admin
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_fav">
							Favourite
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_latest">
							Latest
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_saved">
							Saved
						</a>
					</li>
					<li>
						<a class="filter_btn filter_dropdown_btn" id="filter_popular">
							Popular
						</a>
					</li>
					<li>
						<select id="filter_privacy" class="filter_select filter_dropdown_btn">
							 <option value="">All Boards</option>
							<option value="public">Public</option>
							<option value="private">Private</option>
							<option value="secret">Secret</option>
						</select>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="follow_explore_container">
		<!-- created board slider -->
		<div id="created_slider_container" class="container">
			<!-- items for filter button -->
			<div class="slider_outer group_board">
				<div id="created_group_slider" class="created_group_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="<?php echo base_url(); ?>account/all/groups" title="">
						View All
					</a>
					<a class="<?= $create_group_btn_class; ?> btn active" href="<?= $create_group_btn_link; ?>">
						Create Group
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer topic_board">
				<div id="created_topic_slider" class="created_topic_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="<?php echo base_url(); ?>account/all/topics" title="">
						View All
					</a>
					<a class="btn active" href="<?php echo base_url(); ?>account/create/topic" title="">
						Create SubGroup
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer post_board">
				<div id="created_post_slider" class="created_post_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="<?php echo base_url(); ?>account/all/posts" title="">
						View All
					</a>
					<a class="btn active" href="<?php echo base_url(); ?>account/create/post" title="">
						Create Post
					</a>
				</div>
			</div>
		</div>

		<!-- followed board slider -->
		<div id="follow_slider_container" class="container">
			<!-- items for filter button -->
			<div class="slider_outer group_board">
				<div class="follow_group_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="create_group_btn btn active">
						Create Group
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer topic_board">
				<div class="follow_topic_slider owl-carousel owl-theme">
					<div class="item">
						<div class="default_item create_topic">
							<div class="default_text">
								<h4>
									Organize your group communication.
								</h4>
								<p>
									Categorize each post and discussions by topic.
								</p>
							</div>
							<div class="default_btn">
								<a class="btn" href="" title="">
									Create SubGroup
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="default_item explore_item">
							<div class="default_text">
								<h4>
									You are not following any boards yet.
								</h4>
								<p>
									Follow or join a group board and get involved.
								</p>
							</div>
							<div class="default_btn">
								<a class="btn" href="" title="">
									Explore
								</a>
							</div>
						</div>
					</div>
					<!--
					 <div class="item">
                     						<div class="board_item">
                     							<figure>
                     								<figcaption>
                     									<div class="save_follow">
                     										<a href="">
                     											<i class="fas fa-file-download"></i>
                     											save
                     										</a>
                     										<a href="" class="d-none">
                     											<i class="fas fa-user-plus"></i>
                     											follow
                     										</a>
                     										<a href="" class="d-inline">
                     											<i class="fas fa-user-plus"></i>
                     											Invite
                     										</a>
                     									</div>
                     									<div class="more_option">
                     										more
                     										<i class="fas fa-ellipsis-v"></i>

                     										<ul class="more_option_item">
                     											<li>
                     												<a href="">Share</a>
                     											</li>
                     											<li>
                     												<a href="">Pin</a>
                     											</li>
                     											<li>
                     												<a href="">Join</a>
                     											</li>
                     											<li>
                     												<a href="">Quick View</a>
                     											</li>
                     										</ul>
                     									</div>
                     								</figcaption>
                     								<img class="img-fluid" src="<?php echo base_url()."img/topic.png"; ?>" alt="board-img">
                     							</figure>
                     							<summary class="board_info">
                     								<h4 class="board_metta">
                     									<a href="">Subgroup</a>
                     									<a class="d-none" id="pinned_board">
                     										<i class="fas fa-thumbtack"></i>
                     									</a>
                     									<a id="board_comment">
                     										<span>0</span> <i class="fas fa-comment"></i>
                     									</a>
                     									<a id="board_like">
                     										<span>0</span> <i class="fas fa-heart"></i>
                     									</a>
                     								</h4>
                     								<h3 class="board_title">
                     									<a href="">
                     										Your Title Goes Here
                     									</a>
                     								</h3>
                     							</summary>
                     						</div>
                     					</div>
                     					<div class="item">
                     						<div class="board_item">
                     							<figure>
                     								<figcaption>
                     									<div class="save_follow">
                     										<a href="">
                     											<i class="fas fa-file-download"></i>
                     											save
                     										</a>
                     										<a href="" class="d-inline">
                     											<i class="fas fa-user-plus"></i>
                     											follow
                     										</a>
                     										<a href="" class="d-none">
                     											<i class="fas fa-user-plus"></i>
                     											Invite
                     										</a>
                     									</div>
                     									<div class="more_option">
                     										more
                     										<i class="fas fa-ellipsis-v"></i>

                     										<ul class="more_option_item">
                     											<li>
                     												<a href="">Share</a>
                     											</li>
                     											<li>
                     												<a href="">Pin</a>
                     											</li>
                     											<li>
                     												<a href="">Join</a>
                     											</li>
                     											<li>
                     												<a href="">Quick View</a>
                     											</li>
                     										</ul>
                     									</div>
                     								</figcaption>
                     								<img class="img-fluid" src="<?php echo base_url()."img/topic.png"; ?>" alt="board-img">
                     							</figure>
                     							<summary class="board_info">
                     								<h4 class="board_metta">
                     									<a href="">Subgroup</a>
                     									<a class="d-none" id="pinned_board">
                     										<i class="fas fa-thumbtack"></i>
                     									</a>
                     									<a id="board_comment">
                     										<span>0</span> <i class="fas fa-comment"></i>
                     									</a>
                     									<a id="board_like">
                     										<span>0</span> <i class="fas fa-heart"></i>
                     									</a>
                     								</h4>
                     								<h3 class="board_title">
                     									<a href="">
                     										Your Title Goes Here
                     									</a>
                     								</h3>
                     							</summary>
                     						</div>
                     					</div>
                     					<div class="item">
                     						<div class="board_item">
                     							<figure>
                     								<figcaption>
                     									<div class="save_follow">
                     										<a href="">
                     											<i class="fas fa-file-download"></i>
                     											save
                     										</a>
                     										<a href="" class="d-none">
                     											<i class="fas fa-user-plus"></i>
                     											follow
                     										</a>
                     										<a href="" class="d-inline">
                     											<i class="fas fa-user-plus"></i>
                     											Invite
                     										</a>
                     									</div>
                     									<div class="more_option">
                     										more
                     										<i class="fas fa-ellipsis-v"></i>

                     										<ul class="more_option_item">
                     											<li>
                     												<a href="">Share</a>
                     											</li>
                     											<li>
                     												<a href="">Pin</a>
                     											</li>
                     											<li>
                     												<a href="">Join</a>
                     											</li>
                     											<li>
                     												<a href="">Quick View</a>
                     											</li>
                     										</ul>
                     									</div>
                     								</figcaption>
                     								<img class="img-fluid" src="<?php echo base_url()."img/topic.png"; ?>" alt="board-img">
                     							</figure>
                     							<summary class="board_info">
                     								<h4 class="board_metta">
                     									<a href="">Subgroup</a>
                     									<a class="d-none" id="pinned_board">
                     										<i class="fas fa-thumbtack"></i>
                     									</a>
                     									<a id="board_comment">
                     										<span>0</span> <i class="fas fa-comment"></i>
                     									</a>
                     									<a id="board_like">
                     										<span>0</span> <i class="fas fa-heart"></i>
                     									</a>
                     								</h4>
                     								<h3 class="board_title">
                     									<a href="">
                     										Your Title Goes Here
                     									</a>
                     								</h3>
                     							</summary>
                     						</div>
                     					</div>
					 -->
				</div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create SubGroup
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer post_board">
				<div class="follow_post_slider owl-carousel owl-theme"> </div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create Posts
					</a>
				</div>
			</div>
		</div>

		<!-- explore board slider -->
		<div id="explore_slider_container">
			<!-- featured suggested section -->
			<div class="feat_sugst_section">

				<div class="feat_sugst_header">
					<div class="feat_sugst_header_container container">
						<ul class="nav text-center">
							<li class="nav-item">
								<a class="nav-link active" id="feat_slider">
									Featured
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="sugst_slider">
									Suggested
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="feat_sugst_container container">
					<!-- featured board slider -->
					<div id="feat_slider_container">
						<div class="feat_slider_container owl-carousel owl-theme">
						</div>
					</div>

					<!-- suggested board slider -->
					<div id="sugst_slider_container">
						<div class="sugst_slider_container owl-carousel owl-theme">
						</div>
					</div>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer group_board container">
				<div class="explore_group_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="create_group_btn btn active">
						Create Group
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer topic_board container">
				<div class="explore_topic_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create SubGroup
					</a>
				</div>
			</div>
			<!-- items for filter button -->
			<div class="slider_outer post_board container">
				<div class="explore_post_slider owl-carousel owl-theme"></div>
				<div class="view_all">
					<a class="btn" href="" title="">
						View All
					</a>
					<a class="btn active" href="" title="">
						Create Posts
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ad section -->
<section class="ad_section">
	<div class="container">
		<div class="user_ad_slider owl-carousel owl-theme">

		</div>
	</div>
</section>

<!-- popular post section -->
<section class="user_popular_post">
	<div class="user_popular_post_header pb-5">
		<h2 class="text-center mb-2">
			Popular Post
		</h2>
	</div>
	 	<div class="slider_outer topic_board container">
     	<div class="explore_post_slider1  owl-carousel owl-theme">


     	</div>
     	</div>
</section>
</main><!-- /maincontent -->

<!-- popup content -->
<div class="popup_container todos_not_popup">
	<div id="todos_not" class="popup_item todos_not">
		<span class="popup_closer todos_popup_closer">
			<i class="cross fas fa-times"></i>
		</span>
		<ul>
			<h4 class="todos_header">
				To do's List
			</h4>

				<?php



				foreach($comment_todo_notification as $todo){ ?>
				<li id="todoComment<?php  echo $todo['No_ID'] ?>">
					<div class="col-sm-10">
					<h4 class="todos_name">
					<?php if($todo['No_Status'] == 'done'){ ?>
						<i class="fa fa-check-square" aria-hidden="true" style="color:green"></i>
					<?php }else{ ?>
						<i class="fas fa-cog"></i>
					<?php } ?>
					<span>
					<a href="<?php echo base_url() ?>u/<?php echo encode_id($todo['Us_ID']); ?>"><?php echo $todo['Us_Name'] ?></a> Comment	<?php echo $todo['Co_Message'] ?>  on <a href="<?php echo base_url()?>account/view/post/<?php echo encode_id($todo['Co_Po_ID']); ?>"><?php echo $todo['Po_Title'] ?> Post</a>
					</span>
					</h4>
					<span style="display:none;"  id="commentxx<?php echo $todo['No_ID'] ?>"> <?php echo $todo['No_To_Type'] ?> </span>
					</div>

						<div class="col-sm-2">

							<div class="dropdown">
							  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
							   Action
							  </button>
							  <div class="dropdown-menu">
							  <a class="dropdown-item" href="#" onclick="addnoteComment( <?php echo $todo['No_ID'] ?>  )">
								<?php if( $todo['No_To_Type'] == 'user'){ ?>
								Add Note
								<?php }else{ ?>
								Notes
								<?php } ?>
								</a>
								<a class="dropdown-item" href="#" onclick="removeComment(<?php echo $todo['No_ID']; ?>)">Delete</a>
<a class="dropdown-item"  id="donebutton<?php echo $todo['No_ID'] ?>" data-type="<?php if($todo['No_Status'] == 'done'){ ?>unread<?php }else{ ?>done<?php } ?>" href="#" onclick="doneComment(<?php echo $todo['No_ID']; ?>)"><?php if($todo['No_Status'] == 'done'){ ?>
                                                                                                                       						 Un Done
                                                                                                                       					<?php }else{ ?>
                                                                                                                       						Done
                                                                                                                       					<?php } ?></a>

							  </div>
							</div>

						</div>


				</li>
					<?php } ?>
		</ul>
	</div>
</div>
<input type="hidden" id="user_id" value="<?= $users_item['Us_ID']; ?>">
<?php $this->load->view('account/created_groups_template'); ?>
<?php $this->load->view('account/created_topics_template'); ?>
<?php $this->load->view('account/created_posts_template'); ?>
<?php $this->load->view('account/featured_groups_template'); ?>
<?php $this->load->view('account/suggested_groups_template'); ?>
<?php $this->load->view('account/item_template'); ?>
<?php $this->load->view('account/profile_popup'); ?>
<?php $this->load->view('account/group_users_pop_up_on_list'); ?>

<style>
	.modal-backdrop.fade.show {
		display: none!important;
	}
</style>
