				<section class="user-section entry-section" id="entryform">
					<div class="container">
						<div class="row">


								<div class="col-lg-12">


											<div class="row">
												<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
												<?php echo $errorGroupDetail;?>
												</div>
											</div>


											<?php
												echo validation_errors();
												echo form_open('account/updategroupdetail');
												echo form_hidden('GroupID', $groups_items['Gr_ID']);

												$radPublic = (strcmp($groups_items['Gr_Privacy'],'public')==0) ? ' checked' : '';
												$radPrivate = (strcmp($groups_items['Gr_Privacy'],'private')==0) ? ' checked' : '';
												$radSecret = (strcmp($groups_items['Gr_Privacy'],'secret')==0) ? ' checked' : '';
											?>

												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<label>Community Name</label>
														<input class="form-control" id="txtName" name="txtName" value="<?php echo $groups_items['Gr_Name'];?>" type="text" placeholder="Group Name" required="required" data-validation-required-message="Please enter your name.">
													</div>
												</div>

												<div class="control-group">
													<div class="admin-form-group controls mb-0 pb-1">
														<div class="input-group">
															<div class="input-group-prepend">
																<label class="input-group-text rounded-0" for="selCategory">Category</label>
															</div>
															<select class="custom-select" id="selCategory" name="selCategory">
																<option value="0">Select</option>
																<?php foreach ($category_items as $item): ?>
																			<?php
																				if (strcmp($groups_items['Gr_Ca_ID'],$item['Ca_ID'])==0)
																					echo '<option value="'.$item['Ca_ID'].'" selected>'.$item['Ca_Name'].'</option>';
																				else
																					echo '<option value="'.$item['Ca_ID'].'">'.$item['Ca_Name'].'</option>';
																			?>
															<?php endforeach; ?>
															</select>
														</div>
													</div>
												</div>

												<div class="control-group mb-3">
														<div class="admin-form-group controls mb-0 pb-1">
															<h5 class="text-muted pt-3 pb-0 mb-0">Privacy</h5>

															<!-- Default unchecked -->
															<div class="custom-control custom-radio py-2">
																<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public" <?php echo $radPublic; ?>>
																<label class="custom-control-label" for="radPublic">Public <br><i class="text-muted">Anyone can find the group, see who's in it ang what they post</i></label>
															</div>

															<!-- Default checked -->
															<div class="custom-control custom-radio pb-2">
																<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" <?php echo $radPrivate; ?>>
																<label class="custom-control-label" for="radPrivate">Private <br><i class="text-muted">Anyone can find the group and see who runs it. Only members can see who's in it and what they post.</i></label>
															</div>

															<!-- Default checked -->
															<div class="custom-control custom-radio pb-2">
																<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" <?php echo $radSecret; ?>>
																<label class="custom-control-label" for="radSecret">Secret <br><i class="text-muted">Only members can see the group, who's in it and what they post.</i></label>
															</div>

													</div>
												</div>


												<br>
												<div id="success"></div>
												<div class="admin-form-group">
													<button type="submit" class="btn btn-primary" id="sendMessageButton">Save Changes</button>
												</div>
											</form>


							
											<div class="row">
												<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
												<?php echo $errorGroupPhoto;?>
												</div>
											</div>


											<div class="row">
													<div class="col-lg-12 mx-auto text-center mb-1">
													<h5>Choose Background</h5>
													</div>
												</div>


												<div class="row">
												<?php foreach ($backcolors_items as $item): ?>

													<div class="col-md-6 col-lg-4 admin-img-box">
														<a class="board-link" href="<?php echo base_url(); ?>account/setgroupbackcolor/<?php echo $groups_items['Gr_ID'];?>/<?php echo $item['Se_ID'];?>">
														<div class="admin-colorbg <?php echo $item['Se_Value']; ?>">
																<p>&nbsp;</p>
																<div class="admin-img-label shadow-sm">
																	<div class="row">
																		<div class="col-md-12 col-lg-12">
																			<p class="lead board-category"><?php echo $item['Se_Name']; ?></p>
																	</div>
																	</div>
																</div>

														</div>
														</a>
													</div>

												<?php endforeach; ?>
												</div>

														<div class="row">
																<?php foreach ($backgrounds_items as $item): ?>

																<div class="col-md-6 col-lg-4 admin-img-box">
																	<div class="admin-img">
																		<a class="board-link" href="<?php echo base_url(); ?>account/setgroupphoto/<?php echo $groups_items['Gr_ID'];?>/<?php echo $item['Se_ID'];?>">
																			<img class="img-fluid" src="<?php echo $item['Se_Photo']; ?>" alt="">
																		</a>
																		<div class="admin-img-label shadow-sm">
																				<div class="row">
																					<div class="col-md-12 col-lg-12">
																						<p class="lead board-category"><?php echo $item['Se_Name']; ?></p>
																					</div>
																				</div>
																			</div>

																	</div>
																</div>
																<?php endforeach; ?>


														</div>


												<?php
													echo validation_errors();
													echo form_open_multipart('account/changegroupphoto');
													echo form_hidden('GroupID', $groups_items['Gr_ID']);
												?>

													<div class="control-group">
														<div class="admin-form-group controls mb-0 pb-1">
															<label>Click upload to select image from your computer</label>
															<div class="input-group">
																<div class="input-group-prepend">
																		<span class="input-group-text rounded-0 btn btn-default btn-file">Upload
																				<input type="file" id="userfile" name="userfile">
																			</span>
																</div>
																<input type="text" class="form-control" readonly>
																<img id='img-upload'/>
															</div>
														</div>
													</div>


													<br>
													<div id="success"></div>
													<div class="admin-form-group">
														<button type="submit" class="btn btn-primary" id="sendMessageButton">Submit</button>
													</div>
												</form>
										
									</div>

							</div>

						</div>
					</div>
				</section>
