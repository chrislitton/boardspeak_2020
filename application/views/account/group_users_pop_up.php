<?php
    $photo = (strlen($groups_items['Gr_Photo'])>0) ? base_url() . $groups_items['Gr_Photo'] : base_url() . 'img/banner/create-group.jpg';
?>
<div class="group_info_container">
    <!-- add meber popup -->
    <div class="modal_item invite_members">
        <span class="modal_closer invite_members_closer">
            <i class="fas fa-times"></i>
        </span>
        <h6>
            Invite Contacts
        </h6>
        <div class="invite_option">
            <span>
              <?php  if($groups_items['Gr_Us_ID'] == $this->LoggedInUser) { ?>
 				 <button class="btn btn_select_role btn-light" data-role="superadmin">
                    Add as Super Admin
                </button>
<!--                <button class="btn btn_select_role btn-light" data-role="admin">-->
<!--                    Add as Admin-->
<!--                </button>-->

            		<?php 	} ?>

                <button class="btn btn_select_role btn-light active" data-role="member">
                    Add as Member
                </button>
                <button class="btn btn_select_role btn-light" data-role="follower">
                    Add as Follower
                </button>

                <?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>
                  <button class="btn btn-info"  data-toggle="modal" data-target="#questionmodal" >
                   <?php
				 if($groups_items['group_Question'] != null || $groups_items['group_Question'] != ''){
				 echo 'Edit Questionnaire';
				 }else{
					echo 'Create Questionnaire';
				 } ?>
 		 </button>


                  <?php  }?>
            </span>
        </div>
  	<!---->
        <div style="margin-bottom:10px" id="linkpost"
 			  data-url="<?php echo base_url() . 'inviteLink/' . $groups_items['Gr_Slug'] . '/' . $this->session->userdata['logged_in']['bs_id']. '/' .'member' ?>"
           class="addthis_inline_share_toolbox"
            data-title="<?php echo $groups_items['Gr_Name']; ?>"
            data-media="<?php echo $photo; ?>"></div>
            <input class="form-control" type="text" id="changeInviteLInk" value="<?php echo base_url() . 'inviteLink/' . $groups_items['Gr_Slug'] . '/' . $this->session->userdata['logged_in']['bs_id']. '/' .'member' ?>" >
            <button  class="btn btn-success"  id="inviceButton">Copy  Invite Link </button>
        <div class="search_member form-group">
            <input class="form-control" type="text" id="userInviteKey">
            <button class="btn btn-info" type="button" id="btnUserInviteSearch">
                search
            </button>
        </div>
        <div class="scroll_body">
            <ul class="suggest_members invite_search_list"></ul>
        </div>
    </div>
    <!-- member role popup
    <div class="modal_item user_role">
        <span class="modal_closer invite_members_closer">
            <i class="fas fa-times"></i>
        </span>
        <h6>
            Manage user roles
        </h6>
        <div class="search_member form-group">
            <input class="form-control" type="text" id="user_filter">
            <button class="btn btn-info" type="button" id="btnUserSearch">
                search
            </button>
        </div>
        <ul class="user_role_list">
            <li>
                <a disabled>Roles <i class="fas fa-filter"></i></a>
            </li>
            <li>
                <a class="role_option active" id="tab_all_members" role_tab_id="all_members">All</a>
            </li>
            <li>
                <a class="role_option" id="tab_super_admin" role_tab_id="super_admin">Super Admin</a>
            </li>
            <li>
                <a class="role_option" id="tab_admin" role_tab_id="admin">Admin</a>
            </li>
            <li>
                <a class="role_option" id="tab_member" role_tab_id="member">Members</a>
            </li>
            <li>
                <a class="role_option" id="tab_follower" role_tab_id="follower">Followers</a>
            </li>
            <li>
                <a class="role_option" id="tab_pending" role_tab_id="pending">Join Requests / Invites</a>
            </li>
        </ul>
        <div class="scroll_body">
            <div class="suggest_member_tabs">
                <ul id="all_members" class="user_list suggest_members"></ul>
                <ul id="super_admin" class="user_list suggest_members"></ul>
                <ul id="admin" class="user_list suggest_members"></ul>
                <ul id="member" class="user_list suggest_members"></ul>
                <ul id="follower" class="user_list suggest_members"></ul>
                <ul id="pending" class="user_list suggest_members"></ul>
                <ul id="no_role" class="user_list suggest_members"></ul>
            </div>
        </div>
    </div>
    <!-- role permission popup -->
    <div class="modal_item role_permission">
        <span class="modal_closer role_permission_closer">
            <i class="fas fa-times"></i>
        </span>
        <h6>
            Manage role permissions
        </h6>
        <ul class="role_permission_list">
            <li>
                <a disabled>Roles <i class="fas fa-filter"></i></a>
            </li>
            <li>
                <a class="role_permission_option active" id="tab_super_admin" role_tab_id="super_admin">Super Admin</a>
            </li>
            <li>
                <a class="role_permission_option" id="tab_admin" role_tab_id="admin">Admin</a>
            </li>
            <li>
                <a class="role_permission_option" id="tab_member" role_tab_id="member">Members</a>
            </li>
            <li>
                <a class="role_permission_option" id="tab_follower" role_tab_id="follower">Followers</a>
            </li>
        </ul>
        <div class="scroll_body">
            <div class="suggest_member_tabs">
                <form name="permission_form" id="permission_form" method="POST">
                <table class="table table-hovered table-condensed">
                    <thead>
                        <tr>
                            <th>Permission</th>
                            <th style='text-align:center;'>Allow</th>
                            <th style='text-align:center;'>Deny</th>
                        </tr>
                    </thead>
                    <tbody class="permission_list suggest_members">
                    </tbody>
                </table>
                </form>
            </div>
        </div>
        <div style="margin-top: 10px;">
            <button class="btn btn-primary pull-right save_group_permission">Save</button>
        </div>
    </div>
</div>
<?php $this->load->view('account/user_roles_template'); ?>
