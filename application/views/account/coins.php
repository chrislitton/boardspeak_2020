<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MyCoints</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assetsofpop/css/index.css">
<style>
	.col-xs-12{
		font-family: "Montserrat", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-weight: 700;
  line-height: 1.2;
	}
	@media screen and (max-width:400px){
	/* .navbar-header{
		display: flex;
    flex-direction: row-reverse;
    justify-content: space-between;"
	}
	.center-main{
		display:none;
	}
	.notif-header-logo{
		display:flex;
		justify-content:center;
	}
	.notif-header-logo img{
		width: 173px;
	} */
}
</style>
</head>
<body>
<style>
	@media screen and (max-width:500px) {
		.pp{padding:0;}
	
		.navbar-default .navbar-toggle {
			margin-right:0;
margin-left:0;
		}
		.navbar-header{padding-right: 10px;
    padding-left: 10px;}
	}
	@media (max-width:900px){
.logo {
    margin-top: 26px;
    width: 173px;
}
}
	@media (max-width:766px){
.logo {
    margin-top: 16px;

}
}


</style>
<!-- Navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid pp">
		<div class="col-md-12 col-lg-1"></div>
		<div class="col-md-12 col-lg-10 pp"  >
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="notif-header-logo" href="<?php echo base_url(); ?>">
					<img src="<?php echo base_url(); ?>assetsofpop/img/logo.svg" alt="logo" class="logo" >
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
				<ul class="nav navbar-nav navbar-right" style="margin-top: 5px; display: flex; flex-direction: row; align-items: center;">
					<li><a href="#">Explore</a></li>
					<li><a href="#">Create Group</a></li>
					<li>
						<a href="#">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-search.svg" alt="icon search">
						</a>
					</li>
					<li>
						<a href="<?php  echo base_url() ?>account/notifications">
							<div style="position: relative;">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-bell.svg" alt="icon bell">
								<div class="avaible-notif"></div>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>account/profile">

							<?php if(isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ ?>
								<img  alt="avatar" class="avatar" style="    max-width: fit-content!important;" src="<?php echo $this->session->userdata['logged_in']['Us_Photo']; ?>">
							<?php }else{ ?>
								<img src="<?php echo base_url(); ?>assetsofpop/img/avatar.svg"   alt="avatar" class="avatar">

							<?php } ?>

<!--							<img src="--><?php //echo base_url(); ?><!--assetsofpop/img/avatar.svg" alt="avatar" class="avatar">-->
						</a>
					</li>
					<li>
						<a href="#">
							<div class="container-reward">
								<p style="margin-bottom : 0px">Rewards</p>
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin-no-border.svg" alt="">
							</div>
						</a>
					</li>

				</ul>
			</div>
		</div>
		<div class="col-md-12 col-lg-2"></div>
	</div>
</nav>

<main id="main" class="main" style="padding-top: 70px; margin-bottom: 70px; ">
	<section class="not_content">
		<div class="not_content_container container-fluid">
			<div class="row">
				<div class="center-main"style="display:none;">
					<ul class="nav nav-tabs "  style="margin-top: 50px;">
						<li class="active"><a data-toggle="tab" href="#home">Reward</a></li>
						<li><a data-toggle="tab" href="#menu1">Exchange</a></li>
						<li  ><a data-toggle="tab" href="#menu2">My Coin</a></li>
					</ul>
				</div>

				<div class="tab-content">

					<!-- HOME SECTION -->
					<div id="home" class="tab-pane active">

						<div class="center-main"style="display:none;">
							<div class="category-coin-container">
								<div class="item">
									<a href="#" class="actives">Earn</a>
									<div class="underline"></div>
								</div>
								<div class="item">
									<a href="#" >Offer</a>
								</div>
								<div class="item">
									<a href="#" >Readeem</a>
								</div>
								<div class="item">
									<a href="#" >history</a>
								</div>
							</div>
						</div>

						<!-- Carousel -->
						<div class="col-sm-12 col-md-1 " >
						</div>
						<div class="col-sm-12 col-md-10 toggle-margin" >
							<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-example-generic" data-slide-to="1"></li>
									<li data-target="#carousel-example-generic" data-slide-to="2"></li>
								</ol>

								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="row" style="width: 100%;" >
											<div class="col-xs-12 col-md-6"style="color:#333333;">
												<p style="font-size:  30px; font-weight: 400; margin-bottom: 0px;">Easily claim rewards</p>
												<p style="font-size:  30px; font-weight: 400;">Collect coins and redeem.</p>
												<div style="width: 100px; height: 5px; background-color: #15DA57; margin: 10px 0px;"></div>
												<p style="font-size: 18px;color:#736e6e;font-weight:100;">Earn gift cards, cash vouchers, raffle entries,
													free passes and more from BoardSpeak Rewards.</p>
											</div>
											<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; ">
												<img class="illustration-mobile mobile-illustration-size top-img-space little-left" src="<?php echo base_url(); ?>assetsofpop/img/img-carousel-1.png">
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row" style="width: 100%;" >
											<div class="col-xs-12 col-md-6" style="#333333;">
												<p style="font-size:  30px; font-weight: 400; margin-bottom: 0px;">Get rewarded</p>
												<p style="font-size:  30px; font-weight: 400;">by doing what you already do!</p>
												<div style="width: 100px; height: 5px; background-color: #15DA57; margin: 10px 0px;"></div>
												<p style="font-size: 18px; color: #736e6e;font-weight:100;">Complete simple tasks offered here by
													group admins (e.g., like a post, register, visit
													websites, attend virtual meetings, etc.) to
													collect coins.</p>
											</div>
											<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; ">
												<img class="illustration-mobile" src="<?php echo base_url(); ?>assetsofpop/img/img-carousel-2.png">
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row" style="width: 100%;" >
											<div class="col-xs-12 col-md-6" style="#333333;">
												<p style="font-size:  30px; font-weight: 400; margin-bottom: 0px;">Motivate members
												</p>
												<p style="font-size:   30px; font-weight: 400;">with our instant rewards system.</p>
												<div style="width: 100px; height: 5px; background-color: #15DA57; margin: 10px 0px;"></div>
												<p style="font-size: 18px; color: #736e6e;font-weight:100;">Increase demand for your products and services
													with Referral Marketing. Simply assign referral tasks
													(share your promo, website, etc.) and reward them. </p>
											</div>
											<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center;">
												<img class="illustration-mobile mobile-illustration-size little-left"  src="<?php echo base_url(); ?>assetsofpop/img/img-carousel-3.png">
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					
						<style>
							.illustration-mobile{
									width:400px;
								}
							@media screen and (max-width:450px){
								.illustration-mobile{
									width:280px;
								}
								.mobile-illustration-size{
									width:320px
								}
								.top-img-space{
									position: relative;
									bottom:-10px;
								}
								.little-left{
									position: relative;
									left:-6px;
								}
							}
						</style>
						<div class="col-sm-12 col-md-1 " >
						</div>
<style>

</style>
						<!-- Package -->
						<div class="col-sm-12" style="padding-right: 0px; padding-left: 0px;">
							<div class="package-container" style="white-space:normal;height: auto;">
								<div class="col-sm-12 col-md-2 " style="width:0;display:none">
								</div>
								<div class="col-sm-12 col-md-8 width-mobile " style="padding:0;min-width: 340px;padding:0;display: grid;margin: auto;width: 71.666667%;">
									<p style="font-size: 30px; font-weight: 700; text-align: center;margin-top:5px;">Complete Task Offers. Get Instant Rewards.</p>
									<p style="font-size: 18px; font-weight: 400; text-align: center; color: #928D8D;">Do simple tasks below and get rewarded. Make sure to read terms & conditions for each offer. </p>
									<div class="package-container" style="white-space:normal;height: auto;">
										<div class="package-item" style=" transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-green.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">5mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: #08773B; text-align: center; margin-top: 25px;"> Get Bonus Coins</p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
												</div>
												<span style="margin-left: 10px; font-size: 50px; font-weight: 700; color: white;">+30</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: 20px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/img-simply.png" alt="">
												<button type="button" class="btn">SIGN UP</button>
											</div>
										</div>
										<div class="package-item" style="transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-orange.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">15mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: #BEDCF1; text-align: center; margin-top: 25px;"> Earn Coins</p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
												</div>
												<span style="margin-left: 10px; font-size: 50px; font-weight: 700; color: white;">+100</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: -15px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/img-adidas.png" alt="">
												<button type="button" class="btn">Do Simple Task</button>
											</div>
										</div>
										<div class="package-item" style="transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-ocean.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">5mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: #057E93; text-align: center; margin-top: 25px;"> Get Rewarded </p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
<!--													<img src="--><?php //echo base_url(); ?><!--assetsofpop/img/icon-coin.png" style="width: 42px;">-->
												</div>
												<span style="margin-left: 8px; font-size: 50px; font-weight: 700; color: white;"><img style="width: 30px;" src="<?php echo base_url()."assetsofpop/img/pesoicon.png" ?>">300</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: 5px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/img-splore.png" style="    width: 80%; margin: 9px;"  alt="">
												<button type="button" data-toggle="modal" data-target="#answertpollCoinModal" class="btn">Do Simple Task</button>
											</div>
										</div>
										<div class="package-item" style="transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-red.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">5mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: whitesmoke; text-align: center; margin-top: 25px;"> Earn Coins</p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
												</div>
												<span style="margin-left: 10px; font-size: 50px; font-weight: 700; color: white;">+50</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: -0px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/manginasal-logo.png" style="width: 85%; alt="">

<!--												// chris change the link here-->
												<a type="button" class="btn"   href="<?php echo base_url()."account/view/post/free_breakfast_worth_p1000_and_bmw_personalized_key_chain?op=1" ?>" >Do Simple Task</a>

											</div>
										</div>
										<div class="package-item" style="transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-yellow.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">15mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: #08773B; text-align: center; margin-top: 25px;"> Earn Coins</p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
												</div>
												<span style="margin-left: 10px; font-size: 50px; font-weight: 700; color: white;">+200</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: -18px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/img-22Holisticlogo.png" style="width: 80%; margin: 18px;" alt="">
												<button type="button" class="btnsz">Do Simple Task</button>
											</div>
										</div>
										<style>

											.package-item .btnsz {
												background-color: transparent;
												border: 2px solid #F7901E !important;
												border-radius: 16px;
												font-size: 20px !important;
												font-weight: 600 !important;
												color: #F7901E;
												width: 100%;
												padding: 9px;
												margin-top: 20px;
											}
											.btnsz:focus {
												outline: 2px solid #5cc1b0;
												outline-offset: -2px;
											}
											@media  screen and (max-width:400px;) {
												.package-container{
													padding:0;
												}
											}
										</style>
										<div class="package-item" style="transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-gold.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">3mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: #846208; text-align: center; margin-top: 25px;"> Earn Coins </p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
												</div>
												<span style="margin-left: 10px; font-size: 50px; font-weight: 700; color: white;">50</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: -20px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/Gruppologo.png" style="  width: 85%; margin: -14px;" alt="">
												<button type="button" class="btn">Do Simple Task</button>
											</div>
										</div>
										<div class="package-item" style="transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-bluegray.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">15mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: #205A93; text-align: center; margin-top: 27px;"> Earn Coins</p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
												</div>
												<span style="margin-left: 10px; font-size: 50px; font-weight: 700; color: white;">+200</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: -15px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/Giraffe_logo.png" style="width: 100%; margin: 9px;" alt="">
												<button type="button" class="btn">Do Simple Task</button>
											</div>
										</div>
										<div class="package-item" style="transform: scale(0.9);margin-right:0;width: 295px;height: 380px;background: url(<?php echo base_url(); ?>assetsofpop/img/bg-task-offer-blue.png);">
											<div style="display: flex; justify-content: center; align-items: center;">
												<span class="package-item-title">2mins</span>
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-clock.png" alt="">
												<span class="package-item-title">Task</span>
											</div>
											<p style="font-size: 20px; font-weight: 700; color: #1FA2B8; text-align: center; margin-top: 25px;"> Redeem Reward </p>

											<div style="display: flex; justify-content: center;align-items: center;">
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
												</div>
												<span style="margin-left: 10px; font-size: 50px; font-weight: 700; color: white;">100</span>
											</div>

											<div style="display: flex ; flex-direction: column; align-items: center; margin-top: -5px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/img-splore.png"  style="    width: 70%;"alt="">
												<button type="button" class="btn">SIGN UP</button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-2 " style="width:0;display:none">
								</div>
							</div>
						</div>

						<!-- Brands -->
						<div class="col-sm-12 col-md-2 " >
						</div>
						<div class="col-sm-12 col-md-8" style="display: grid;">
							<p style="font-size: 30px; font-weight: 700; text-align: center;">Amazing Rewards From You Favorite Brands And Retailers.</p>
							<p style="font-size: 18px; font-weight: 400; text-align: center; color: #928D8D;">Reward yourself with cash vouchers, products and services using the coins you collected. </p>
							<div class="row" style="margin-top: 30px;display:flex;flex-wrap:wrap;">
								<img src="<?php echo base_url(); ?>assetsofpop/img/PLDT.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Splore.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Jolibee.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Bos.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/MamaLou.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Nestle.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Human.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Nara.png" class="col-xs-6 col-md-3">
							</div>
							<div class="row">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Gruppo.png" class="col-xs-6 col-md-3">
								<img src="<?php echo base_url(); ?>assetsofpop/img/Cha.png" class="col-xs-6 col-md-3">
							</div>
						</div>
						<div class="col-sm-12 col-md-2 " >
						</div>


						<!-- Package -->
						<div class="col-sm-12" style="margin-top: 30px; padding-right: 0px; padding-left: 0px;">
							<div class="package-container">
								<div class="col-sm-12 col-md-2 " >
								</div>
								<div class="col-sm-12 col-md-8 " >
									<p style="font-size: 30px; font-weight: 700; text-align: center;">Increase Positive Experience On Every Task</p>
									<p style="font-size: 18px; font-weight: 400; text-align: center; color: #928D8D;">Check out Instant Reward  Coins Offered by Various Group Admin in BoardSpeak . Collect coins to redeem prize</p>
									<div class="row" style="margin-top: 40px;">
										<div class="col-xs-12 col-md-3">
											<div class="bubble bubble-bottom-left" contenteditable>Lorem Ipsum is simply dummy text of the printing and </div>
											<div style="display: flex; margin-top: 30px;" >
												<img src="https://greatmind.id/uploads/article-detail/7a22da3d4ba408a18ddcd39aceaa79822b46b309.jpg" style="width: 50px; height: 50px; border-radius: 10px;">
												<div style="display: flex; flex-direction: column; margin-left: 5px; justify-content: center;">
													<span>@hihello</span>
													<span>3 days Ago</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-md-3">
											<div class="bubble bubble-bottom-left" contenteditable>Lorem Ipsum is simply dummy text of the printing and </div>
											<div style="display: flex; margin-top: 30px;" >
												<img src="https://greatmind.id/uploads/article-detail/7a22da3d4ba408a18ddcd39aceaa79822b46b309.jpg" style="width: 50px; height: 50px; border-radius: 10px;">
												<div style="display: flex; flex-direction: column; margin-left: 5px; justify-content: center;">
													<span>@hihello</span>
													<span>3 days Ago</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-md-3">
											<div class="bubble bubble-bottom-left" contenteditable>Lorem Ipsum is simply dummy text of the printing and </div>
											<div style="display: flex; margin-top: 30px;" >
												<img src="https://greatmind.id/uploads/article-detail/7a22da3d4ba408a18ddcd39aceaa79822b46b309.jpg" style="width: 50px; height: 50px; border-radius: 10px;">
												<div style="display: flex; flex-direction: column; margin-left: 5px; justify-content: center;">
													<span>@hihello</span>
													<span>3 days Ago</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-md-3">
											<div class="bubble bubble-bottom-left" contenteditable>Lorem Ipsum is simply dummy text of the printing and </div>
											<div style="display: flex; margin-top: 30px;" >
												<img src="https://greatmind.id/uploads/article-detail/7a22da3d4ba408a18ddcd39aceaa79822b46b309.jpg" style="width: 50px; height: 50px; border-radius: 10px;">
												<div style="display: flex; flex-direction: column; margin-left: 5px; justify-content: center;">
													<span>@hihello</span>
													<span>3 days Ago</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-md-3">
											<div class="bubble bubble-bottom-left" contenteditable>Lorem Ipsum is simply dummy text of the printing and </div>
											<div style="display: flex; margin-top: 30px;" >
												<img src="https://greatmind.id/uploads/article-detail/7a22da3d4ba408a18ddcd39aceaa79822b46b309.jpg" style="width: 50px; height: 50px; border-radius: 10px;">
												<div style="display: flex; flex-direction: column; margin-left: 5px; justify-content: center;">
													<span>@hihello</span>
													<span>3 days Ago</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-md-3">
											<div class="bubble bubble-bottom-left" contenteditable>Lorem Ipsum is simply dummy text of the printing and </div>
											<div style="display: flex; margin-top: 30px;" >
												<img src="https://greatmind.id/uploads/article-detail/7a22da3d4ba408a18ddcd39aceaa79822b46b309.jpg" style="width: 50px; height: 50px; border-radius: 10px;">
												<div style="display: flex; flex-direction: column; margin-left: 5px; justify-content: center;">
													<span>@hihello</span>
													<span>3 days Ago</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-md-3">
											<div class="bubble bubble-bottom-left" contenteditable>Lorem Ipsum is simply dummy text of the printing and </div>
											<div style="display: flex; margin-top: 30px;" >
												<img src="https://greatmind.id/uploads/article-detail/7a22da3d4ba408a18ddcd39aceaa79822b46b309.jpg" style="width: 50px; height: 50px; border-radius: 10px;">
												<div style="display: flex; flex-direction: column; margin-left: 5px; justify-content: center;">
													<span>@hihello</span>
													<span>3 days Ago</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-2 " >
								</div>
							</div>
						</div>

					</div>

					<!-- MENU 1 -->
					<div id="menu1" class="tab-pane">
						<h3>Menu 1</h3>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div>

					<!-- MENU 2 -->
					<div id="menu2" class="tab-pane ">

						<div>
							<!-- adds right side -->
							<div class="col-sm-12 col-md-2 " >
								<!-- <img src="assets/img/add1.png" alt="" class="add1">
								<img src="assets/img/add2.png" alt="" class="add2">  -->
							</div>

							<div class="col-sm-12 col-md-8">
								<div class="center-main">
									<div class="category-coin-container">
										<div class="item">
											<a href="#" class="actives">Earn</a>
											<div class="underline"></div>
										</div>
										<div class="item">
											<a href="#" >Offer</a>
										</div>
										<div class="item">
											<a href="#" >Readeem</a>
										</div>
										<div class="item">
											<a href="#" >history</a>
										</div>
									</div>
								</div>

								<p>Your available balance is:</p>

								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" class="coin-logo" alt="">

								<p class="nominal"><?php echo $usercoins; ?></p>

								<button type="button" class="btn btn-primary btn-lg">Buy Coins</button>

								<p class="coin-info">3,000 Coins will expire in 29, January 2023 </p>

								<p style="margin-top: 40px;" >Use coins to assign referral marketing tasks, promote business, <br>
									purchase in-app features, redeem rewards, etc. <a href="#" style=" color: #0077FF; " >Learn More</a>  </p>


								<div class="menuss-container " style="margin-top: 20px;">
									<div style="display: flex;">
										<!-- bonus coin -->
										<div class=" menuss-item" style="min-width: 60%; margin-top: 20px;">
											<div class="card-coin bonus-coin">
												<p class="title-card-coin">Bonus Coins</p>
												<p class="value-card-coin">5,000</p>
												<button type="button" class="btn btn-lg" style="margin-top: 10px;"  data-toggle="modal" data-target="#bonusCoinModal">Get More!</button>
											</div>
										</div>

										<!-- redeem coin -->
										<div class="menuss-item" style="min-width: 37%; margin-top: 20px;">
											<div class="card-coin redeem-coin">
												<p class="title-card-coin">Redeemed coins</p>
												<p class="value-card-coin">7,000</p>
												<button type="button" class="btn btn-lg" style="margin-top: 10px;" data-toggle="modal" data-target="#redeemCoinModal">Redeem More!</button>
											</div>
										</div>
									</div>

									<div style="display: flex;">
										<!-- earned coin -->
										<div class="menuss-item" style="min-width: 48%; margin-top: 20px;">
											<div class="card-coin earned-coin">
												<p class="title-card-coin">Earned Coins</p>
												<p class="value-card-coin">9,000</p>
												<button type="button" class="btn btn-lg" style="margin-top: 10px;" data-toggle="modal" data-target="#earnedCoinModal">Earn More!</button>
											</div>
										</div>

										<!-- bought coin -->
										<div class="menuss-item" style="min-width: 49%;margin-top: 20px;">
											<div class="card-coin bought-coin">
												<p class="title-card-coin">Bought coins</p>
												<p class="value-card-coin">5,000</p>
												<button type="button" class="btn btn-lg" style="margin-top: 10px;" data-toggle="modal" data-target="#creditCoinModal">Get More!</button>
											</div>
										</div>
									</div>

									<a href="#" data-toggle="modal" data-target="#coinRewardModal" style="width: 100%;">
										<img src="<?php echo base_url(); ?>assetsofpop/img/bg-offer-reward.png" alt="offer reward" style="width: 100%; margin-top: 20px;">
									</a>

									<a href="#">
										<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.jpg" alt="add" style="width: 100%; margin-top: 20px;">
									</a>

								</div>

								<div class="center-main">
									<ul class="nav nav-tabs nav-tabs-category "  style="margin-top: 50px;">
										<li><a data-toggle="tab" href="#all">All</a></li>
										<li><a data-toggle="tab" href="#earned">Earned</a></li>
										<li ><a data-toggle="tab" href="#bonus">Bonus</a></li>
										<li><a data-toggle="tab" href="#bought">Bought</a></li>
										<li><a data-toggle="tab" href="#offered">Offered</a></li>
										<li class="active"><a data-toggle="tab" href="#redeemed" >Redeemed</a></li>
									</ul>
								</div>

								<div class="tab-content" style="margin-top: 20px; background-color: #EFEEEE; padding: 0px 30px;">
									<div id="all" class="tab-pane fade ">
										<div class="row tab-pane-container">
											<div class="earn-coin-card row">
												<div class="earn-coin-card-img col-sm-12 col-md-3" >
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
												</div>
												<div class="col-sm-12 col-md-6" >
													<p class="earn-coin-card-title" >You received BONUS coins </p>
													<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
												</div>
												<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
													<div class="earn-coin-card-container-value">
														<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
													</div>
												</div>
											</div>

											<div class="earn-coin-card row">
												<div class="earn-coin-card-img col-sm-12 col-md-3" >
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
												</div>
												<div class="col-sm-12 col-md-6" >
													<p class="earn-coin-card-title" >You received BONUS coins </p>
													<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
												</div>
												<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
													<div class="earn-coin-card-container-value">
														<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
													</div>
												</div>
											</div>

											<div class="earn-coin-card row">
												<div class="earn-coin-card-img col-sm-12 col-md-3" >
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
												</div>
												<div class="col-sm-12 col-md-6" >
													<p class="earn-coin-card-title" >You received BONUS coins </p>
													<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
												</div>
												<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
													<div class="earn-coin-card-container-value">
														<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="earned" class="tab-pane fade ">
										<div class="row tab-pane-container">
											<div class="col-sm-12 col-md-2"></div>
											<div class="col-sm-12 col-md-8">
												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-2"></div>
										</div>
									</div>
									<div id="bonus" class="tab-pane fade ">
										<div class="row tab-pane-container">
											<div class="col-sm-12 col-md-2"></div>
											<div class="col-sm-12 col-md-8">
												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-2"></div>
										</div>
									</div>
									<div id="bought" class="tab-pane fade ">
										<div class="row tab-pane-container">
											<div class="col-sm-12 col-md-2"></div>
											<div class="col-sm-12 col-md-8">
												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-2"></div>
										</div>
									</div>
									<div id="offered" class="tab-pane fade ">
										<div class="row tab-pane-container">
											<div class="col-sm-12 col-md-2"></div>
											<div class="col-sm-12 col-md-8">
												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>

												<div class="earn-coin-card row">
													<div class="earn-coin-card-img col-sm-12 col-md-3" >
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
													</div>
													<div class="col-sm-12 col-md-6" >
														<p class="earn-coin-card-title" >You received BONUS coins </p>
														<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
													</div>
													<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
														<div class="earn-coin-card-container-value">
															<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-2"></div>
										</div>
									</div>
									<div id="redeemed" class="tab-pane fade in active show">
										<div class="row tab-pane-container">
											<div class="earn-coin-card row">
												<div class="earn-coin-card-img col-sm-12 col-md-3" >
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
												</div>
												<div class="col-sm-12 col-md-6" >
													<p class="earn-coin-card-title" >You received BONUS coins </p>
													<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
												</div>
												<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
													<div class="earn-coin-card-container-value">
														<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
													</div>
												</div>
											</div>

											<div class="earn-coin-card row">
												<div class="earn-coin-card-img col-sm-12 col-md-3" >
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
												</div>
												<div class="col-sm-12 col-md-6" >
													<p class="earn-coin-card-title" >You received BONUS coins </p>
													<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
												</div>
												<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
													<div class="earn-coin-card-container-value">
														<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
													</div>
												</div>
											</div>

											<div class="earn-coin-card row">
												<div class="earn-coin-card-img col-sm-12 col-md-3" >
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coint-earned.png" alt="icon-earned" >
												</div>
												<div class="col-sm-12 col-md-6" >
													<p class="earn-coin-card-title" >You received BONUS coins </p>
													<p class="earn-coin-card-desc">+10 coins for 5 days of login </p>
												</div>
												<div style="display: flex;flex-direction: column;align-items: center;" class="col-sm-12 col-md-3">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="" class="earn-coin-card-coin-image">
													<div class="earn-coin-card-container-value">
														<p class="earn-coin-card-value"> <span class="earn-coin-card-value-plus" >+</span>  50</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div style="display: flex;  flex-direction: column; align-items : center; margin-top: 20px;">
									<a href="#" class="load-more"> Load More </a>
									<p class="term-and-service">By using BoardSpeak Coins, you agree with our <a href="#">Terms of Service. </a> <br>
										Coins are not refundable and expire in one year unless otherwise stated.</p>
								</div>
							</div>

							<!-- adds right side -->
							<div class="col-sm-12 col-md-2" >
								<img src="<?php echo base_url(); ?>assetsofpop/img/add3.png" alt="" class="add3">
								<div class="group-container">
									<div class="group-card">
										<p style="color: grey; font-size: 14px;">Featured Group.</p>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
									</div>

								</div>
								<div class="group-container">
									<div class="group-card">
										<p style="color: grey; font-size: 14px;">Featured Group.</p>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
									</div>

								</div>
								<div class="group-container">
									<div class="group-card">
										<p style="color: grey; font-size: 14px;">Featured Group.</p>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
										<div class="group-item">
											<div style="display: flex; align-items: center;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/add1.png" width="30px" height="30px" alt="">
												<div style="margin-left: 5px; text-align: start;">
													<p style="font-size: 12px; margin-bottom: 0px;">Bumenta MarketPlace</p>
													<p style="font-size: 12px; margin-bottom: 0px; color: grey;">8 Posts</p>
												</div>
											</div>
											<p style="font-size: 12px; margin-bottom: 0px;">></p>
										</div>
									</div>

								</div>
								<img src="<?php echo base_url(); ?>assetsofpop/img/add4.png" alt="" class="add4">
							</div>

							<div class="col-sm-12">

							</div>

						</div>

					</div>


				</div>
			</div>
	</section>
</main>

<footer>
	<div class="col-sm-12 col-md-2">
	</div>

	<div class="col-sm-12 col-md-8">
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<img src="<?php echo base_url(); ?>assetsofpop/img/logo.svg" alt="">
				<div style="display: flex; align-items: center;  margin-top: 40px; ">
					<div>
						<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pin-map.png" class="icon">
					</div>
					<p>Sta. Rosa, Laguna <br>
						Philippines</p>
				</div>
				<div style="display: flex; align-items: center; margin-top: 10px;">
					<div>
						<img src="<?php echo base_url(); ?>assetsofpop/img/icon-email.png" class="icon">
					</div>
					<p>info@boardspeak.com</p>
				</div>

			</div>
			<div class="col-xs-12 col-md-2">
				<p class="title">Links</p>
				<div style="margin-top: 20px; display: flex; flex-direction: column; ">
					<a style="color: white;" href="#" class="link">Expore</a>
					<a style="color: white;" href="#" class="link">Create Group</a>
					<a style="color: white;" href="#" class="link">Rewards</a>
				</div>
			</div>
			<div class="col-xs-12 col-md-2">
				<p class="title">Social Links</p>
				<div style="margin-top: 20px; display: flex; flex-direction: column; ">
					<div style="display: flex;align-items: center; margin-top: 5px;">
						<div>
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-facebook.png" alt="" style="width: 18px; height: 18px;">
						</div>
						<a style="color: white;"href="#" class="social-link">facebook</a>
					</div>
					<div style="display: flex;align-items: center; margin-top: 5px;">
						<div>
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-twitter.png" alt="" style="width: 18px;">
						</div>
						<a style="color: white;"href="#" class="social-link">twitter</a>
					</div>
					<div style="display: flex;align-items: center; margin-top: 5px;">
						<div>
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-linkedln.png" alt="" style="width: 18px;">
						</div>
						<a style="color: white;"href="#" class="social-link">linkedIn</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-3">
				<p class="title">About Boardspeak</p>
				<div style="margin-top: 20px;">
					<p style="margin-left: 0px;">BoardSpeak helps organize
						your team communication.
						It's your sales and marketing
						community bulletin board
						with enterprise social
						collaboration tools.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-12 col-md-2"></div>
</footer>




<!-- Modal Instant Reward -->
<div class="modal fade" id="ShowalertPopup" tabindex="-1" role="dialog" >
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row" >
			<div class="col-xs-12 col-md-5">
				<div class="instant-reward-bg bg" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-modal-instant-reward.png); ">
					<div>
						<p>Motivate members to perform your desired actions!</p>
						<img src="<?php echo base_url(); ?>assetsofpop/img/img-modal-instant-reward.png" alt="modal instant reward">
					</div>
					<p>Instantly reward your members with coins in exchange for digital marketing tasks!</p>

				</div>
			</div>
			<div class="col-xs-12 col-md-7" style="padding: 17px;" >
				<!-- exit button -->
				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">

						<center style="    margin-top: 30%;">
							<center  style="    margin-bottom: 19px!important;">
								<img	src="<?php echo base_url()?>assetsofpop/img/logo.svg"/>
							</center>
							<center  style="    margin-bottom: 19px!important;">
								<a href="#">
									<div class="container-reward" style="    width: fit-content;">
										<p style="margin-bottom : 0px">Rewards</p>
										<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin-no-border.svg" alt="">
									</div>
								</a>
							</center>

							<center style="    margin-bottom: 19px!important;">
								<h3>
									Get rewarded by doing </br> what you already do!
								</h3>
								<p style="color: gray;">
									We are currently working on super awesome </br> rewards from your favorite brands.
								</p>
							</center>
							<center >
								<button class="btn btn-primary btn-block"><a href="<?php echo base_url(); ?>pages/view/signup">Create Free Account</a></button>
								<button class="btn  btn-outline-info btn-block"><a href="<?php echo base_url() ?>account/explore">Explore</a></button>
							</center>
						</center>



				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Instant Reward -->
<div class="modal fade" id="coinRewardModal" tabindex="-1" role="dialog" >
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row" >
			<div class="col-xs-12 col-md-5">
				<div class="instant-reward-bg bg" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-modal-instant-reward.png); ">
					<div>
						<p>Motivate members to perform your desired actions!</p>
						<img src="<?php echo base_url(); ?>assetsofpop/img/img-modal-instant-reward.png" alt="modal instant reward">
					</div>
					<p>Instantly reward your members with coins in exchange for digital marketing tasks!</p>

				</div>
			</div>
			<div class="col-xs-12 col-md-7" style="padding: 17px;" >
				<!-- exit button -->
				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">
					<img
							src="https://media.baamboozle.com/uploads/images/551156/1636687300_25508.jpeg"
							alt="avatar"
							class="avatar" >
					<h2 class="avatar-name-text" >
						Hello, Chris!
					</h2>
					<p class="avatar-job-text">Designer</p>

					<!-- content modal instant reward  -->
					<div id="modalInstantReward" class="modal-contents" >
						<h2 class="title-instant-reward">
							Instant Rewards
						</h2>
						<p class="desc-instant-reward" style="color: #a5a5a5;">boost motivation!</p>

						<!-- form -->
						<div style="width: 100%; margin-top: 10px;" class="from-input">
							<p class="title-form-instant-reward ">What is your Reward Offer?</p>

							<!-- form input -->
							<div>
								<p class="label-input">How many coins per person?</p>
								<div class="container-form-input">
									<div style="position: relative;">
										<input
												type="number"
												class="form-control"
												style="width: 100px; padding-left: 40px;"
										>
										<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="position: absolute; top: 5px; left: 10px;  width: 25px;" >
									</div>
									<p class="placeholder">coins per person</p>
								</div>
							</div>

							<!-- form input -->
							<div>
								<p class="label-input">How many people do you want to reward when they
									complete your assigned task/s?</p>
								<div class="container-form-input">
									<input
											type="number"
											class="form-control"
											style="width: 100px;"
									>
									<p class="placeholder">people</p>
								</div>
							</div>

						</div>

						<p class="desc-instant-reward" style="color: #FCB731; margin-top: 15px;">Your Total Reward Coins Offer:</p>
						<div style="position: relative;">
							<input
									type="number"
									class="form-control"
									style="width: 200px; padding-left: 40px;"
							>
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="position: absolute; top: 5px; left: 10px;  width: 25px;" >
						</div>

						<div class="row" style="margin-top: 20px; justify-content: center; ">
							<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
								<button type="button" class="btn btn-primary btn-lg" id="cointCreditsButton">Pay with coin credits</button>
							</div>
							<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
								<button type="button" class="btn btn-primary-outline btn-lg" data-dismiss="modal">Cancel</button>
							</div>
						</div>

						<a href="#">
							<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.jpg" alt="ad banner"  style=" height: 60px; width: 100%; margin-top: 20px; " >
						</a>
					</div>


					<!-- content modal instant reward  -->
					<div id="modalInstantRewardCointCredit" class="modal-contents" style="width: 100%; display: initial!important;">
						<h2 class="title-instant-reward" style="color: #FCB731; margin-top: 20px;">
							Pay with Coin Credits
						</h2>
						<p class="desc-instant-reward" style="color: #1F62AE; margin-top: 20px;">Your Total Reward Coins Offer:</p>

						<div class="coin-input-value">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 25px;" >
							<p style="margin-left: 20px; color: #FCB731; margin-bottom: 0px; font-size: 18px; font-weight: 600; ">1000</p>
						</div>

						<div style="width : 100% ; margin-top: 20px;">
							<p class="title-form-instant-reward" style="color: #1F62AE;" >My Coins</p>
							<div style=" display: flex; flex-direction: row; ">
								<p> available balance: </p>
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 25px; margin-left: 20px;" >
								</div>
								<p style="color: #ECCD67; margin-left: 10px;" > 700 </p>
							</div>
							<p style="font-size: 12px; color: #009344;"> 500  Coins will expire in [date one year after]</p>
						</div>

						<p style="font-size: 14px; margin-top: 20px; margin-bottom: 5px;">Your available coin credits is not enough.</p>
						<p style="font-size: 20px; font-weight: 600; color: #088B34;">You are short by 300 coins.</p>

						<div style="display: flex; flex-direction: row; align-items: center;  padding: 10px; border-radius: 10px; background-color: #FCB731;">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 50px;" >
							<div style="margin: 0px 15px; height: 50px; width: 2px; background-color: #231F20;"></div>
							<p style="margin-bottom: 0px; font-size: 22px; font-weight: 700;">300</p>
						</div>

						<div class="row" style="margin-top: 30px; justify-content: center; ">
							<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
								<button type="button" class="btn btn-primary btn-lg">Buy Coins</button>
							</div>
							<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
								<button type="button" class="btn btn-primary-outline btn-lg" id="cointCreditsButtonCancel">Cancel</button>
							</div>
						</div>

						<a href="#">
							<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.jpg" alt="ad banner"  style=" height: 60px; width: 100%; margin-top: 20px; " >
						</a>
					</div>


				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Bonus Coin -->
<div class="modal fade" id="bonusCoinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="bonus-coin-bg bg" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-bonus-coin-modal.png); ">
					<p style="font-size: 26px; font-weight: 600;">You got Bonus coins
						just for signing up!</p>
					<div style="display: flex; justify-content: center; align-items: center; margin-top: 15px;">
						<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
						<p style="margin-left: 5px; margin-bottom: 0px; font-size: 48px; font-weight: 700; color: #FCB731;"> <span style="color: #10BC49;">+</span>50</p>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-7 modal-contentss " style="padding-top: 17px; padding-right: 40px;" >

				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">
					<img
							src="https://media.baamboozle.com/uploads/images/551156/1636687300_25508.jpeg"
							alt="avatar"
							class="avatar" >
					<h2 class="avatar-name-text" >
						Hello, Chris!
					</h2>
					<p class="avatar-job-text">Designer</p>

					<p style="margin-top: 10px; font-size: 28px; font-weight: 600; color: #FCB731;">Get Bonus Coins</p>
					<p style="font-size: 22px; color: #A5A5A5;">by doing what you already do!</p>

					<div class="bonus-coin-card">
						<div class="coin-value">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
							<p style="font-size: 22px ; color: #FCB731; font-weight: 700; margin-left: 5px; ">  <span style="color: #009344;">+</span>5</p>
						</div>
						<p style="margin-left: 15px;">per day when you log in on this site</p>
					</div>
					<div class="bonus-coin-card">
						<div class="coin-value">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
							<p style="font-size: 22px ; color: #FCB731; font-weight: 700; margin-left: 5px; ">  <span style="color: #009344;">+</span>3</p>
						</div>
						<p style="margin-left: 15px;">per day of constant activity in this site</p>
					</div>
					<div class="bonus-coin-card">
						<div class="coin-value">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
							<p style="font-size: 22px ; color: #FCB731; font-weight: 700; margin-left: 5px; ">  <span style="color: #009344;">+</span>15</p>
						</div>
						<p style="margin-left: 15px;">per week, if any of your posts have at least
							15 likes (within 7days the post was made)
						</p>
					</div>

					<p style="width: 100%; margin-top: 10px; font-size: 22px; font-weight: 600; color: #1F62AE;">My coins</p>

					<div class="row" style="width: 100%;">
						<div class="col-xs-12 col-md-6" style="margin-top: 5px; display: flex; flex-direction: column; align-items: center; justify-content: center;">
							<p>available balance:</p>
							<div style="display: flex;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								</div>
								<span style=" margin-left: 10px; margin-bottom: 0px; font-size: 16px; color: #FCB731;">10,000</span>
							</div>

							<button type="button" class="btn btn-primary btn-lg" style="margin-top: 10px;">Buy Coins</button>

						</div>
						<div class="col-xs-12 col-md-6" style="margin-top: 5px; display: flex; flex-direction: column; align-items: center; justify-content: center;">
							<p>available balance:</p>
							<div style="display: flex;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								</div>
								<span style=" margin-left: 10px; margin-bottom: 0px; font-size: 16px; color: #FCB731;">10,000</span>
							</div>

							<button type="button" class="btn btn-primary-outline btn-lg" data-dismiss="modal" style="margin-top: 10px;">Cancel</button>
						</div>
					</div>

					<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.png" style="width: 100%; margin-top: 10px;">

					<div style="display: flex; justify-content: center;">
						<span style="color: #1F62AE;">Follow Us:</span>
						<img src="<?php echo base_url(); ?>assetsofpop/img/icon-facebook-yellow.png" style="height: 14px; margin-left: 10px;">
						<img src="<?php echo base_url(); ?>assetsofpop/img/icon-instagram-yellow.png" style="height: 14px; margin-left: 10px;">
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Coin Credit -->
<div class="modal fade" id="creditCoinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="bonus-coin-bg bg" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-credit-coin-modal.png); background-color: #D2F324; ">
					<p style="font-size: 26px; font-weight: 600;">INSTANTLY REWARD
						your members</p>
					<p style="font-size: 16px; font-weight: 400;">with our automated tracking /selection
						& coin wallet system.</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-7 modal-contentss " style="padding-top: 17px; padding-right: 40px;" >

				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">
					<img
							src="https://media.baamboozle.com/uploads/images/551156/1636687300_25508.jpeg"
							alt="avatar"
							class="avatar" >
					<h2 class="avatar-name-text" >
						Hello, Chris!
					</h2>
					<p class="avatar-job-text">Designer</p>

					<p style="margin-top: 10px; font-size: 28px; font-weight: 600; color: #FCB731; margin-bottom: 0px;">Coin Credits</p>
					<p style="font-size: 16px; font-weight: 400;">Choose a Package:</p>

					<div class="promo-container">
						<span style="font-size: 14px; font-weight: 500;">Introductory Promo:</span>
						<div style="display: flex; align-items: center; ">
							<span style="font-size: 18px; font-weight: 700; color: #009344;">Get 20%</span>
							<span style="font-size: 14px; font-weight: 500; margin-left: 5px;">Extra on any package!</span>
						</div>
					</div>

					<div class="coin-credit-container" style="width: 100%; margin-top: 15px;" id="modalCreditPointBuy">
						<div class="card-credit-item-container ">
							<div class="card-promo-credit-coin">
								<div class="card-promo-credit-coin-title" style="background-color: #1F62AE;">
									<span style="color: white; font-size: 18px; font-weight: 500;">Basic</span>
								</div>
								<span class="card-promo-credit-coin-value" style="color: #1F62AE;">P5,000</span>
								<span style="text-align: center ; margin-top: 20px;">Equivalent to <br>
										5,000 coins</span>

								<button type="button" class="btn btn-primary" style="margin-top: 20px;">Buy Now</button>
							</div>
						</div>
						<div class="card-credit-item-container">
							<div class="card-promo-credit-coin-active">
								<div class="card-promo-credit-coin-title" style="background-color: #009344;">
									<span style="color: white; font-size: 18px; font-weight: 500;">Standard</span>
								</div>
								<span class="card-promo-credit-coin-value" style="color: #1F62AE;">P2,000</span>
								<span style="text-align: center ; margin-top: 20px;">Equivalent to <br>
										2,000 coins</span>

								<button type="button" class="btn btn-success" style="margin-top: 20px;" id="paymentStandardButton">Buy Now</button>
							</div>
						</div>
						<div class="card-credit-item-container">
							<div class="card-promo-credit-coin">
								<div class="card-promo-credit-coin-title" style="background-color: #FCB731;">
									<span style="color: white; font-size: 18px; font-weight: 500;">Premium</span>
								</div>
								<span class="card-promo-credit-coin-value" style="color: #1F62AE;">P1,000</span>
								<span style="text-align: center ; margin-top: 20px;">Equivalent to <br>
										1,000 coins</span>

								<button type="button" class="btn btn-warning" style="margin-top: 20px;">Buy Now</button>
							</div>
						</div>
					</div>

					<div style="width: 100%; margin-top: 15px;" id="modalCreditPointPay">
						<div class="row credit-coin-container-pay" style="width: 100%;">
							<div class="col-xs-12 col-md-6" style="padding-left: 0px; display: flex ; align-items: center; flex-direction: column;">
								<div class="payment-container-title">
									<span style="font-size: 18px; color: white; font-weight: 500;">Standard</span>
								</div>
								<p style="color: #009344; font-weight: 700; font-size: 20px; margin-top: 20px;">P2,000</p>
								<p style="margin-top: 20px; font-size: 14px; text-align: center;">Equivalent to <br>
									2,000 coins</p>
								<button type="button" class="btn btn-primary btn-lg" style="margin-top: 15px;">Pay Now</button>
							</div>
							<div class="col-xs-12 col-md-6" style="display: flex; justify-content: center; align-items: center;">
								<img src="<?php echo base_url(); ?>assetsofpop/img/payment-img.png" alt="" style=" width: 163px;  margin-top: 40px;">
							</div>
						</div>
					</div>

					<p style="font-size: 22px; font-weight: 500; margin-top: 20px;">Have any question?</p>
					<button type="button" class="btn btn-primary" style="margin-top: 15px;">Contact Us</button>

					<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.png" style="width: 100%; margin-top: 10px;">
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Earned Coin -->
<div class="modal fade" id="earnedCoinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="bonus-coin-bg bg" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-earned-coin-modal.png); ">
					<span style="font-size: 26px; font-weight: 600;">Get 500 Bonus coins</span>
					<span style="font-size: 18px; font-weight: 400; color: #FCB731;">by following easy steps in</span>
					<span style="font-size: 14px; font-weight: 500; color: #00FFE5;">(48 Hours)</span>
					<img src="<?php echo base_url(); ?>assetsofpop/img/img-earned-coin-modal.png" style="width: 100%; margin-top: 40px;">
				</div>
			</div>
			<div class="col-xs-12 col-md-7 modal-contentss " style="padding-top: 17px; padding-right: 40px;" >

				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">
					<img
							src="https://media.baamboozle.com/uploads/images/551156/1636687300_25508.jpeg"
							alt="avatar"
							class="avatar" >
					<h2 class="avatar-name-text" >
						Hello, Chris!
					</h2>
					<p class="avatar-job-text">Designer</p>

					<p style="margin-top: 30px; font-size: 26px; font-weight: 600; color: #1F62AE;">My Bonus Coins</p>
					<div style="display: flex; align-items: center;">
						<div>
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 42px;">
						</div>
						<span style=" margin-left: 10px; margin-bottom: 0px; font-size: 48px; font-weight: 700; color: #FCB731;">10,000</span>
					</div>

					<div style="display: flex; margin-top: 30px; justify-content: start; width: 100%;">
						<div style="width: 70px;">
							<a href="#" class="earned-coin-tab-active">Today</a>
						</div>
						<div style="width: 100px;">
							<a href="#">Last Week</a>
						</div>
						<div style="width: 100px;">
							<a href="#">Last Month</a>
						</div>
					</div>

					<div class="earned-coin-container">
						<div class="earned-coin-item ">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pizza-hut.png" style="width: 40px;" >
							<span style=" font-size: 14px; font-weight: 700; text-align: center; margin-left: 5px;">Pizzahut</span>
							<span style=" font-size: 14px; font-weight: 400; margin-left: 5px; ">sponsored 3 bonus coins per day if a post is made (for 20 days )</span>
							<div class="earned-value" style="margin-left: 5px;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 20px;" >
								</div>
								<span style=" color: #FCB731; font-size: 14px; font-weight: 600; ">+60</span>
							</div>
						</div>
						<div class="earned-coin-item ">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pizza-hut.png" style="width: 40px;" >
							<span style=" font-size: 14px; font-weight: 700; text-align: center; margin-left: 5px;">Pizzahut</span>
							<span style=" font-size: 14px; font-weight: 400; margin-left: 5px; ">sponsored 3 bonus coins per day if a post is made (for 20 days )</span>
							<div class="earned-value" style="margin-left: 5px;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 20px;" >
								</div>
								<span style=" color: #FCB731; font-size: 14px; font-weight: 600; ">+60</span>
							</div>
						</div>
						<div class="earned-coin-item ">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pizza-hut.png" style="width: 40px;" >
							<span style=" font-size: 14px; font-weight: 700; text-align: center; margin-left: 5px;">Pizzahut</span>
							<span style=" font-size: 14px; font-weight: 400; margin-left: 5px; ">sponsored 3 bonus coins per day if a post is made (for 20 days )</span>
							<div class="earned-value" style="margin-left: 5px;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 20px;" >
								</div>
								<span style=" color: #FCB731; font-size: 14px; font-weight: 600; ">+60</span>
							</div>
						</div>
						<div class="earned-coin-item ">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pizza-hut.png" style="width: 40px;" >
							<span style=" font-size: 14px; font-weight: 700; text-align: center; margin-left: 5px;">Pizzahut</span>
							<span style=" font-size: 14px; font-weight: 400; margin-left: 5px; ">sponsored 3 bonus coins per day if a post is made (for 20 days )</span>
							<div class="earned-value" style="margin-left: 5px;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 20px;" >
								</div>
								<span style=" color: #FCB731; font-size: 14px; font-weight: 600; ">+60</span>
							</div>
						</div>
						<div class="earned-coin-item ">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pizza-hut.png" style="width: 40px;" >
							<span style=" font-size: 14px; font-weight: 700; text-align: center; margin-left: 5px;">Pizzahut</span>
							<span style=" font-size: 14px; font-weight: 400; margin-left: 5px; ">sponsored 3 bonus coins per day if a post is made (for 20 days )</span>
							<div class="earned-value" style="margin-left: 5px;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 20px;" >
								</div>
								<span style=" color: #FCB731; font-size: 14px; font-weight: 600; ">+60</span>
							</div>
						</div>							<div class="earned-coin-item ">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pizza-hut.png" style="width: 40px;" >
							<span style=" font-size: 14px; font-weight: 700; text-align: center; margin-left: 5px;">Pizzahut</span>
							<span style=" font-size: 14px; font-weight: 400; margin-left: 5px; ">sponsored 3 bonus coins per day if a post is made (for 20 days )</span>
							<div class="earned-value" style="margin-left: 5px;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 20px;" >
								</div>
								<span style=" color: #FCB731; font-size: 14px; font-weight: 600; ">+60</span>
							</div>
						</div>							<div class="earned-coin-item ">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-pizza-hut.png" style="width: 40px;" >
							<span style=" font-size: 14px; font-weight: 700; text-align: center; margin-left: 5px;">Pizzahut</span>
							<span style=" font-size: 14px; font-weight: 400; margin-left: 5px; ">sponsored 3 bonus coins per day if a post is made (for 20 days )</span>
							<div class="earned-value" style="margin-left: 5px;">
								<div>
									<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 20px;" >
								</div>
								<span style=" color: #FCB731; font-size: 14px; font-weight: 600; ">+60</span>
							</div>
						</div>

					</div>

					<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner-pink.png" style="width: 100%; margin-top: 15px;">

				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="answertpollCoinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="redeem-coin-bg bg" >
				</div>
			</div>
			<div class="col-xs-12 col-md-7 modal-contentss " style="padding-top: 17px; padding-right: 40px;" >

				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">
					<img
							src="https://media.baamboozle.com/uploads/images/551156/1636687300_25508.jpeg"
							alt="avatar"
							class="avatar" >
					<h2 class="avatar-name-text" >
						Hello, Chris!
					</h2>
					<p class="avatar-job-text">Designer</p>

					<p style="margin-top: 10px; font-size: 28px; font-weight: 600; color: #FCB731; margin-bottom: 0px;">GET REWARDED</p>
					<p style="font-size: 16px; font-weight: 400;">by doing what you already do</p>

					<p style="font-size: 23px; font-weight: 500; margin-top: 10px; color: #FCB731;">Answer Poll</p>

					<div class="assignment-container">
						<p style="font-size: 18px; color: #758388; font-weight: 700; margin-bottom: 5px;">TASK ASSIGNMENT</p>
						<p style="font-size: 16px; color: #0A4680; font-weight: 500;">like Splore FB & IG pages</p>
						<a type="button" class="btn btn-primary" style="margin-top: 10px;"  href="<?php echo base_url()."account/view/post/free_breakfast_worth_p1000_and_bmw_personalized_key_chain?op=1" ?>" >Answer Poll</a>
						<div style="display: flex; margin-top: 20px; align-items: center;">
							<input type="checkbox" aria-label="...">
							<span style="margin-left: 5px; font-size: 10px; margin-top: 3px;">I have read and understood ALL of the terms & conditions written below.</span>
						</div>
					</div>

					<p style="margin-top: 20px; font-size: 16px; font-weight: 700; color: #758388;">TERMS & CONDITIONS</p>
					<p style="font-size: 12px; font-weight: 500;">This reward can be redeemed for 100 coins and task assigned should be
						accomplished accordingly. Splore has the right to validate your identity to
						verify task assigned is done.</p>
					<p style="font-size: 12px; font-weight: 500;">Limited offer: For new participants of Splore only. Available until supply lasts.
						Only BoardSpeak registered account can participate. Customers redeem
						rewards using BoardSpeak coins. Rewards shall be used based on their
						validity.  Read More</p>

					<p style="color: #958E8E; margin-bottom: 3px;">Got a question?</p>
					<button type="button" class="btn btn-warning" style="background-color: transparent !important; border : 2px solid #FCB731 !important ; color: #FCB731 !important;">Contact Us</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Redeem Coin -->
<div class="modal fade" id="redeemCoinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="redeem-coin-bg bg" >
				</div>
			</div>
			<div class="col-xs-12 col-md-7 modal-contentss " style="padding-top: 17px; padding-right: 40px;" >

				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">
					<img
							src="https://media.baamboozle.com/uploads/images/551156/1636687300_25508.jpeg"
							alt="avatar"
							class="avatar" >
					<h2 class="avatar-name-text" >
						Hello, Chris!
					</h2>
					<p class="avatar-job-text">Designer</p>

					<p style="margin-top: 10px; font-size: 28px; font-weight: 600; color: #FCB731; margin-bottom: 0px;">GET REWARDED</p>
					<p style="font-size: 16px; font-weight: 400;">by doing what you already do</p>

					<p style="font-size: 23px; font-weight: 500; margin-top: 10px; color: #FCB731;">Complete Tasks. Get Rewards</p>

					<div class="assignment-container">
						<p style="font-size: 18px; color: #758388; font-weight: 700; margin-bottom: 5px;">TASK ASSIGNMENT</p>
						<p style="font-size: 16px; color: #0A4680; font-weight: 500;">like Splore FB & IG pages</p>
						<button type="button" class="btn btn-primary" style="margin-top: 10px;" data-toggle="modal" data-target="#redeemRewardModal">Redeem for 100 coins</button>
						<div style="display: flex; margin-top: 20px; align-items: center;">
							<input type="checkbox" aria-label="...">
							<span style="margin-left: 5px; font-size: 10px; margin-top: 3px;">I have read and understood ALL of the terms & conditions written below.</span>
						</div>
					</div>

					<p style="margin-top: 20px; font-size: 16px; font-weight: 700; color: #758388;">TERMS & CONDITIONS</p>
					<p style="font-size: 12px; font-weight: 500;">This reward can be redeemed for 100 coins and task assigned should be
						accomplished accordingly. Splore has the right to validate your identity to
						verify task assigned is done.</p>
					<p style="font-size: 12px; font-weight: 500;">Limited offer: For new participants of Splore only. Available until supply lasts.
						Only BoardSpeak registered account can participate. Customers redeem
						rewards using BoardSpeak coins. Rewards shall be used based on their
						validity.  Read More</p>

					<p style="color: #958E8E; margin-bottom: 3px;">Got a question?</p>
					<button type="button" class="btn btn-warning" style="background-color: transparent !important; border : 2px solid #FCB731 !important ; color: #FCB731 !important;">Contact Us</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Redeem Reward -->
<div class="modal fade" id="redeemRewardModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document" style="background-color: white; border-radius: 25px; " >
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="bonus-coin-bg bg" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-reward-coin-modal.png); ">
					<p style="font-size: 28px; font-weight: 600; ">REDEEM REWARDS</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-7 modal-contentss " style="padding-top: 17px; padding-right: 40px;" >

				<img data-dismiss="modal" src="<?php echo base_url(); ?>assetsofpop/img/exit-modal.png" alt="exit modal" style="height: 30px ; position: absolute; right: 30px; cursor: pointer; " >

				<div style="display : flex ; align-items : center ; flex-direction: column; ">

					<div style="display: flex; width: 100%; margin-top: 30px;">
						<span style="font-size: 26px; font-weight: 600; color: #F26218;">rewards</span>
						<div style="position: relative; margin-left: 20px;">
							<input type="search" class="form-control" style="border : 2px solid #FCB731 !important">
							<img src="<?php echo base_url(); ?>assetsofpop/img/icon-search.svg" style="position: absolute; top: 0px; right: 0px; margin-top: 7px; margin-right: 10px;">
						</div>
					</div>

					<div class="card-reward-container" style="margin-top: 20px;">
						<div class="card-reward" data-toggle="modal" data-target="#successModal">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
						<div class="card-reward">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
						<div class="card-reward">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
					</div>

					<div class="card-reward-container" style="margin-top: 10px;">
						<div class="card-reward-small">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
						<div class="card-reward-small">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
						<div class="card-reward-small">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
					</div>

					<div class="card-reward-container" style="margin-top: 20px;">
						<div class="card-reward-small">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
						<div class="card-reward-small">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
						<div class="card-reward-small">
							<img src="<?php echo base_url(); ?>assetsofpop/img/img-reward-large.png">
							<p style="font-size: 14px; font-weight: 600; margin-top: 5px; color: #1F62AE; ">Splore Fitness Class</p>
							<div style="display: flex ; ">
								<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style="width: 24px;">
								<span style="font-size: 14px; color: #FCB731; margin-left: 5px; font-weight: 500;  ">100</span>
								<span style="font-size: 14px; color: #231F20; margin-left: 5px; font-weight: 500;  ">per pass</span>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Sucess Message -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="background-color: white; border-radius: 25px; ">
		<div style="display: flex; flex-direction: column; align-items: center; padding: 30px 0px;">
			<img src="<?php echo base_url(); ?>assetsofpop/img/icon-gift.png" style="width: 320px;">

			<p style="margin-top: 40px; font-size: 28px; color: #1F62AE; font-weight: 700;">BoardSpeak Reward</p>

			<div style="display: flex;align-items: center ; margin-top: 30px">
				<span style="font-size: 22px; font-weight: 500;">1 piece Mike</span>
				<div style="display: flex; align-items: center; ">
					<div>
						<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" style=" width: 24px; margin-left: 50px;" >
					</div>
					<span style="font-size: 28px; font-weight: 500; margin-left: 10px;">1200</span>
				</div>
			</div>

			<div style="display: flex; flex-direction: column; margin-top: 40px;">
				<button type="button" class="btn btn-lg btn-primary" style="margin-top: 30px; width: 400px;" >See Rewards</button>
				<button type="button" class="btn btn-lg btn-primary" style="margin-top: 30px; width: 400px; background-color: transparent !important; border: 2px solid #1F62AE !important; color: #1F62AE !important; "  data-dismiss="modal">Keep Browsing</button>
			</div>
		</div>
	</div>
</div>
<style>
							.margin-toggler{
								margin-top: 30px;
							}
						</style>
					
<script>
	// This Is The Navbar Toggle button
	let nav_toggler = document.getElementsByClassName('navbar-toggle');
	// This Is the class if below content of navbar
	let margin_sec = document.querySelector('.toggle-margin')
	Array.from(nav_toggler).forEach((e)=>{
		e.addEventListener('click',()=>{

			margin_sec.classList.toggle('margin-toggler');
		})
		
	})
							</script>
</body>
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!--<script src="--><?php //echo base_url(); ?><!--assetsofpop/js/script.js"></script>-->

</html>


<script>
	$('#ShowalertPopup').modal('show');
</script>
