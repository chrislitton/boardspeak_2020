<?php

	$ckGroups = (strcmp($checkGroups,'Groups')==0) ? ' active' : '';
	$ckTopics = (strcmp($checkTopics,'Topics')==0) ? ' active' : '';
	$ckPosts = (strcmp($checkPosts,'Posts')==0) ? ' active' : '';

	$vlGroups = (strcmp($checkGroups,'Groups')==0) ? ' checked' : '';
	$vlTopics = (strcmp($checkTopics,'Topics')==0) ? ' checked' : '';
	$vlPosts = (strcmp($checkPosts,'Posts')==0) ? ' checked' : '';

?>


<!-- Masthead -->
  <header class="dethead bg-primary text-white text-center">
    <!-- <div class="container d-flex align-items-center flex-column"> -->
    <div class="container">
    
  			<div class="row">
		      	<div class="col-md-12 col-lg-12">		      
		    	  <h3 class="text-uppercase mb-0">Advanced Search</h3>	
		    	  <!-- Icon Divider -->
			      <div class="divider-custom divider-light">
			        <div class="divider-custom-line"></div>
			      </div>
	     		</div>
	     	</div>

	</div>	
  </header>
  
  <section class="user-search-section" id="usersearch">
  	<div class="container">

			<div class="row">
				<div class="col-lg-12 mx-auto text-center mb-1 text-danger">
				<?php echo $error;?>
				</div>
			</div>

			<?php echo form_open('account/search'); ?>
			<div class="row mb-5">
				<div class="col-md-12 col-lg-12">

					<div class="input-group mb-3">
						<input type="text" id="txtSearch" name="txtSearch" value="<?php echo $searchText; ?>" class="form-control input-box" placeholder="Search boards and topics" aria-label="Search boards and topics" aria-describedby="basic-addon2">
						<div class="input-group-append">
							<button class="btn btn-primary btn-x" type="submit">Search</button>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-lg-12">
					<div class="custom-control-inline text-muted">Filter:</div>
					<div class="btn-group-toggle custom-control-inline" data-toggle="buttons">
					<label class="btn btn-checkbox<?php echo $ckGroups; ?>">
						<input name="chkGroups" type="checkbox" autocomplete="off" value="Groups"<?php echo $vlGroups; ?>>Groups
					</label>
					</div>

					<div class="btn-group-toggle custom-control-inline" data-toggle="buttons">
					<label class="btn btn-checkbox<?php echo $ckTopics; ?>">
						<input name="chkTopics" type="checkbox" autocomplete="off" value="Topics"<?php echo $vlTopics; ?>>Topics
					</label>
					</div>

					<div class="btn-group-toggle custom-control-inline" data-toggle="buttons">
					<label class="btn btn-checkbox<?php echo $ckPosts; ?>">
						<input name="chkPosts" type="checkbox" autocomplete="off" value="Posts"<?php echo $vlPosts; ?>>Posts
					</label>
					</div>
				</div>



			</div>
			</form>

			<?php if (strlen($searchText)>0) { ?>
			<?php foreach ($search_item as $item): ?>

			<div class="row mb-3">
				<div class="col-lg-3 mx-auto mb-3">
					<div class="admin-small-img bg-group <?php echo $item['Gr_Backcolor']; ?>">
						<a class="board-link" href="<?php echo base_url(); ?>account/group/<?php echo $item['Gr_ID']; ?>">
							<img class="img-fluid" src="<?php echo $item['Gr_Thumb']; ?>" alt="">
						</a>
					</div>
				</div>
				<div class="col-lg-9">
							<a class="board-link" href="<?php echo base_url(); ?>account/group/<?php echo $item['Gr_ID']; ?>">
								<h3 class="text-left"><?php echo $item['Gr_Name']; ?></h3>
							</a>
							<p class="lead board-date">Posted by <?php echo $item['Us_Name']; ?>, On <?php echo date( "F d, Y h:i", strtotime($item['Gr_DatePosted']) ); ?> </p>
							<p class="board-detail">Group | Category: <?php echo $item['Ca_Name']; ?></p>
				</div>
			</div>

			<?php endforeach; ?>
			<?php } ?>

  </div> 
  </section>
    
    
