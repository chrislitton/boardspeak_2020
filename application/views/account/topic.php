<?php
	$UserID = ($this->session->userdata['logged_in']['bs_id']);
	$show_load_btn = "";
	if (count($posts_items)<12) $show_load_btn = " d-none";
?>

<section class="mt-4">
    <div class="container">
		<div class="row">
	  		<div class="col-lg-12">

				<div class="topic_post_filter_container row"></div>  
			</div>    	

		</div>    	
	</div>
</section>
<input type="hidden" value = "<?php echo $member_info['role']; ?>" id="memberinfo">
<div onclick="checkpost()" class="col-4 subcategory-create-button-clone btn_create_post" style="display: none;">
	<a class="board-link   group-item-link" id="btn_create_post" name="btn_create_post"
	 >
	 	<div>
		<img class="post-thumbnail w-100 " src="<?php echo base_url();?>img/newpost.png"
		 alt="imagexxx"  />
		</div>
		<p>&nbsp;</p>
	</a>
</div>

<div class="topic-items-clone col-4" style="display: none;">
	<a href="javascript:void(0)" >
		<div>
			<label class="makepinclass" style="    cursor: pointer; background: rgb(69 141 180 / 63%);
			 padding: 5px; border-radius: 6px; left: 3px; position: absolute; color: white;">
	<span class="unpinedclass" style="font-size: 14px;">
	<i class="makepinclassforthumb fas fa-thumbtack" postitem="196" posttype="Post"><font style="color:white;text-transform: uppercase;
    font-family: monospace;
    font-size: 0.9rem;
    font-weight: 500;">Pin</font>
    	</i>

	</span>
				<span class="PinnedClass" style="font-size: 14px; display: none;">
	<i style="display:none; color:#616172;" class="pinclassforthumb fas fa-thumbtack"><font style="color:white;text-transform: uppercase;
    font-family: monospace;
    font-size: 0.9rem;
    font-weight: 500;">Pinned</font></i>
	</span></label>
			<a class="board-link topic-item-link" href="#">
			<img  src="<?php echo base_url();?>img/filler.png" class="post-thumbnail w-100">
			<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image" style="display:none;" />
	</a></div>
		<a class="board-link topic-item-link" href="#">
			<p class="topic-item-name"></p></a>
	</a>
</div>

<input type="hidden" id="group_id" value="" />
<input type="hidden" name="To_ID" id="To_ID" value="<?=$EncodedID?>" /> 
<input type="hidden" name="TopicID" id="TopicID" value="<?=$EncodedID?>" /> 
<input type="hidden" name="inp_category_id" id="inp_category_id" value="<?=$catId?>" /> 
<input type="hidden" name="inp_subcategory_id" id="inp_subcategory_id" value="<?=$subcatId?>" /> 

<!-- START OF NO ACCESS TO THE SUBTOPIC MODAL -->
<div class="modal fade" id="subtopicAccessModal" tabindex="-1" aria-labelledby="subtopicAccessModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-90 m-auto">
      <div class="modal-body pt-2 pb-4">
		<div class="mt-3">
			<h6 class="modal-header-text text-center">This is a Private Post.</h6>
			<div class="text-center viewb_all">
				<button class="btn btn-lg pt-1 col-12 col-lg-8" id="btn-request-post-access">Request Access</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<!-- END OF NO ACCESS TO THE SUBTOPIC MODAL -->
<style>
@media only screen and (max-width: 600px){
 .post-thumbnail {
    height: 150px;
}
}
@media only screen and (max-width: 768px) {
.col-4.btn_create_post.subcategory-create-button {
    flex: 0 0 49.66667% !important;
    max-width: 49.66667% !important;
}


}

.topic-thumbnail{
border-radius: 25px;

box-shadow: 0 0.125rem 0.25rem rgb(0 0 0 / 8%) !important;
}



</style>
