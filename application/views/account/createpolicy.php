<?php
	$error = '';//'$Parent='.$Parent.' $GroupID='.$GroupID.' $TopicID='.$TopicID.' $SubTopicID='.$SubTopicID;
	echo form_open_multipart('account/savepolicy');
	echo form_hidden('Parent', $Parent);
	echo form_hidden('GroupID', $GroupID);
	echo form_hidden('TopicID', $TopicID);
	echo form_hidden('SubTopicID', $SubTopicID);
?>

<section  class="dethead">
    <div class="container">
			<div class="row">
				<div class="col-lg-9">



<!-- Masthead -->
<header id="groupbanner" class="mb-4">
   <div class="container">
  			<div class="row">
        		<div class="col-md-12 col-lg-12 board-banner text-right bg-primary" id="site-banner">
						<div class="post-title text-white">
							New Policy
						</div>
				</div>
	     	</div>
	</div>
</header>



  <section class="user-section entry-section">
    <div class="container">


      <div class="row">
        <div class="col-lg-12 mx-auto text-center mb-1 text-danger">
      	<?php echo $error;?>
      	</div>
      </div>






      <div class="row">
        <div class="col-lg-12">


			<div class="control-group">
				<div class="admin-form-group controls mb-0 pb-1">
                <input class="form-control" id="txtTitle" name="txtTitle" type="text" placeholder="Enter Policy Title" required="required">
		        </div>
            </div>

			<div class="control-group">
				<div class="admin-form-group controls mb-0 pb-1">
					<textarea class="form-control" id="txtDescription" name="txtDescription" rows="3" placeholder="Description" required="required" data-validation-required-message="Description"></textarea>
					<p class="help-block text-danger"></p>
				</div>
			</div>



			<div class="control-group mt-2">
					<div class="admin-form-group">
						<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Create</button>
					</div>
			</div>

      </div>
      </form>

    </div>
  </section>



			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5">
				<h5 class="font-weight-normal text-primary">Creating Policy</h5>
				<ol>
					<li>Instruction 1</li>
					<li>Instruction 2</li>
					<li>Instruction 3</li>
					<li>Instruction 4</li>
					<li>Instruction 5</li>
				</ol>
				</div>




			</div>
    </div>
    </div>
  </section>
