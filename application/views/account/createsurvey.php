<?php
	echo form_open_multipart('account/savesurvey');
	echo form_hidden('paramSrc', $paramSrc);
	echo form_hidden('paramID', $paramID);



	$TopicID = $paramID;
	$SubTopicID = 0;
	$SubTopicName = "";

	if (strcmp($paramSrc,'subtopic')==0)
	{
		$TopicID = $topics_item['To_Pa_ID'];
		$SubTopicID = $topics_item['To_ID'];
		$SubTopicName = $subtopics_item['To_Name'];
	}

	echo form_hidden('selTopic', $TopicID);
	echo form_hidden('selSubTopic', $SubTopicID);


	$error = '';//'$paramSrc='.$paramSrc.' $paramID='.$paramID.' $GroupID='.$GroupID.' $TopicID='.$TopicID.' $SubTopicID='.$SubTopicID;



?>


<section  class="dethead">
    <div class="container">
			<div class="row">
				<div class="col-lg-9">



<!-- Masthead -->
<header id="groupbanner" class="mb-4">
    <div class="container">
  			<div class="row">
        		<div class="col-md-12 col-lg-12 board-banner text-right bg-post" id="site-banner">
						<div class="post-title text-white">
							New Survey
						</div>
						<div class="mt-3 changebgbtn">

							<div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
								<div class="input-group">
									<div class="input-group-prepend">
												<span class="input-group-text rounded-0 btn btn-default btn-file">Upload Image
													<input type="file" id="flebg" name="userfile">
												</span>
									</div>
									<input id="fleName" type="text" class="form-control" readonly>
								</div>
							</div>
							</div>

						</div>
  				</div>
	     	</div>
	</div>
</header>



<!-- Contact Section -->
  <section class="entry-section">
    <div class="container">


      <div class="row">
        <div class="col-lg-12 mx-auto text-center mb-1 text-danger">
      	<?php echo $error;?>
      	</div>
      </div>



      <div class="row">
        <div class="col-lg-12">

            <div class="control-group d-none">
							<div class="admin-form-group controls mb-0 pb-1">
                <input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
	            </div>
            </div>

						<?php if (strcmp($paramSrc,'topic')==0) { ?>

            <div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
                <div class="input-group">
									<div class="input-group-prepend">
										<label class="input-group-text rounded-0" for="selGroup">Community</label>
									</div>
									<select class="custom-select" id="selGroup" name="selGroup" required="required">
										<option value="">Select Community</option>
										<?php foreach ($groups_items as $item): ?>
													<?php
																$selected = '';
																if (strcmp($paramID,$item['Gr_ID'])==0)  $selected = ' selected';
													?>
													<?php echo '<option value="'.$item['Gr_ID'].'"'.$selected.'>'.$item['Gr_Name'].'</option>'; ?>
									<?php endforeach; ?>
									</select>
									<div class="input-group-append">
										<a class="btn btn-primary rounded-0" href="<?php echo base_url(); ?>account/creategroup"><i class="fa fa-plus px-1"></i></a>
									</div>
								</div>
              </div>
            </div>


						<div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
                <div class="input-group">
									<div class="input-group-prepend">
										<label class="input-group-text rounded-0" for="selTopic">Subgroup</label>
									</div>
									<select class="custom-select" id="selTopic" name="selTopic" required="required">
										<option value="">Select Subgroup</option>
									</select>
									<div class="input-group-append">
										<a class="btn btn-primary rounded-0" href="<?php echo base_url(); ?>account/createtopic"><i class="fa fa-plus px-1"></i></a>
									</div>
								</div>
              </div>
            </div>

						<?php } else { ?>


						<div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
                <div class="input-group">
									<div class="input-group-prepend">
										<label class="input-group-text rounded-0" for="selSubTopic">Sub-Topic</label>
									</div>
									<select class="custom-select" id="selSubTopic" name="selSubTopic" required="required" disabled="disabled">
										<option value="<?php echo $SubTopicID;?>"><?php echo $SubTopicName;?></option>
									</select>
								</div>
              </div>
            </div>

						<?php } ?>


						<div class="control-group">
							<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtTitle" name="txtTitle" type="text" placeholder="Enter Post Title" required="required">
							</div>
            </div>



						<div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
                <textarea class="form-control" id="txtDescription" name="txtDescription" rows="3" placeholder="Add Post" required="required" data-validation-required-message="Please enter post."></textarea>
                <p class="help-block text-danger"></p>
              </div>
            </div>




						<div class="control-group">
              <div class="admin-form-group controls mb-0 pb-1">
                <div class="input-group">
									<div class="input-group-prepend">
										<label class="input-group-text rounded-0" for="selQuestions">Number Of Questions</label>
									</div>
									<select class="custom-select" id="selQuestions" name="selQuestions" required="required">
										<option value="">Select number of questions</option>
										<?php for ($i=1; $i<=10; $i++){ ?>
											<?php
												$selected = '';
												if ($i==0)  $selected = ' selected';
											?>
											<?php echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>'; ?>
										<?php } ?>
									</select>
								</div>
              </div>
            </div>



			<div class="control-group mt-3 mb-5">
				<div class="admin-form-group">
					<button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Create and Edit Survey</button>
				</div>
			</div>




		</div>
      </div>


</form>

    </div>
  </section>




			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5">
				<h5 class="font-weight-normal text-primary">Creating Survey</h5>
				<ol>
					<li>Instruction 1</li>
					<li>Instruction 2</li>
					<li>Instruction 3</li>
					<li>Instruction 4</li>
					<li>Instruction 5</li>
				</ol>
				</div>


			</div>
    </div>
    </div>
  </section>




<script>
    CKEDITOR.replace( 'txtDescription' );
	</script>
