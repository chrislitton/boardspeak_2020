<?php
if(isset($selMenu)){
	$MessagesMenu = (strcmp($selMenu,'Messages')==0) ? ' active' : '';
	$SettingsMenu = (strcmp($selMenu,'Settings')==0) ? ' active' : '';

	$DetailMenu = (strcmp($selSubMenu,'Detail')==0) ? ' active' : '';
	$PhotoMenu = (strcmp($selSubMenu,'Photo')==0) ? ' active' : '';

	$UserID = ($this->session->userdata['logged_in']['bs_id']);
}

?>
<style>@media screen and (max-width:450px) {
		.nav-pills-mobilee{
			justify-content:center;
		}
	}
.leftRewardBox a{
	color: black;
}</style>

<!-- Posts Section -->
<section class="user-section entry-section" id="entryform">
	<div class="col-md-12 col-lg-12">

		<!-- photos and files-->
		<ul style="white-space: nowrap;
    flex-wrap: nowrap;" class="nav nav-pills-mobilee nav-pills mb-3" id="pills-tab" role="tablist">

<!--			--><?php
//			if(isset($polldata) && !empty($polldata)){ ?>
<!--				<button class="btn btn-outline-primary" id="pollQuestionButton" data-toggle="modal" data-target="#--><?php
//
//				if ($polldata[0]['poll_stop'] == 0){
//
//					if (isset($polluserDetail) && !empty($polluserDetail)){echo 'resultanswer';}else{echo 'poolanswer';}
//				}else{
//					echo 'resultanswer';
//				}
//				?><!--"  style="margin-bottom: 10px ; margin-right:10px;  height: 41px"><i class="fas fa-poll" style="font-size: 25px;    color: #06cbd1;"></i><span> &nbsp;&nbsp;--><?php
//						if ($polldata[0]['poll_stop'] == 0){
//							if (isset($polluserDetail) && !empty($polluserDetail)){echo 'Poll Result';}else{echo 'Answer Poll';}
//						}else{
//							echo 'Poll Result';
//						}
//						?><!--</span></button>-->
<!---->
<!--			--><?php //} ?>


			<?php if(count($posts_images) > 0){ ?>
				<!--					<li class="nav-item" role="presentation" style="line-height: 10px!important;">-->
				<!--						<a class="nav-link --><?php //if(count($posts_images) > 0){ ?><!-- active --><?php //} ?><!--" id="pills-media-tab" data-toggle="pill" href="#pills-media" role="tab" aria-controls="pills-media" aria-selected="true">-->
				<!--							<span class="mr-1 small">Media</span>-->
				<!--							<span class="badge badge-pill badge-danger small">--><?php //echo count($posts_images);?><!--</span>-->
				<!--						</a>-->
				<!--					</li>-->
			<?php } ?>
			<?php if(count($posts_files) > 0){ ?>
				<!--					<li class="nav-item" role="presentation" style="line-height: 10px!important;">-->
				<!--						<a class="nav-link --><?php //if(count($posts_files) > 0 && count($posts_images) == 0){ ?><!-- active --><?php //} ?><!--" id="pills-file-tab" data-toggle="pill" href="#pills-file" role="tab" aria-controls="pills-file" aria-selected="false">-->
				<!--							<span class="mr-1 small">Files</span>-->
				<!--							<span class="badge badge-pill badge-danger small">--><?php //echo count($posts_files);?><!--</span>-->
				<!--						</a>-->
				<!--					</li>-->
			<?php } ?>
		</ul>


		<p class="lead"><?php echo $posts_item['Po_Content']; ?></p>
	</div>
	<div class="container earnRewardsCont"id="mainPostContainer">


		<div class="earnRewardsSec">
		<div class="row mb-1 form-group">
			<div class="earnRewardsCon"style="border: none">
				<div class="text-center pie_graph_resp "style="display: flex;
    flex-direction: column;
    align-items: center;
padding-left: 0;
    border-radius: 10px;
margin:0;">
					<div class="head-pie-resp"style="    min-width: 332px;
    border-radius: 5px;    background: #dbf3fd;
    margin: 0;
    width: 100%;
">
						<div class="head-graph-resp">
							<div class="reward-graph-h5">
								<h5>75/100</h5>
								<h5>Completed Tasks</h5>
							</div>
							<p class=""style="font-size: 13px"> Complete tasks to qualify to earn coins<br>and redeem this reward.</p>
							<a class="bigAPie"><h5 style="font-size: 18px">Get a FREE SPLORE PASS!</h5></a>
						</div>
						<div class="pie no-round" style="--p:75;--c:#388cb3bd;--b:15px"> 75%
							<div class="pie pie2 no-round" style="--p:100;--c:#96c8ea;--b:15px;height: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="earnRewardsCon">
				<h2>Earn Coins to Redeem<br>this Reward</h2>
				<p>Tick check mark boxes to indicate what tasks you have completed. Redeem coins to earn your reward voucher!
				</p>

			<?php 		if(!isset($this->session->userdata['logged_in']['bs_id'])){ ?>
				<div class="earnRewardsBox earnRewardsBoxMain">
					<img  src="<?php  if(!isset($this->session->userdata['logged_in'])){ echo base_url().'assetsofpop/earnRewCheckBlack.png';} if(isset($this->session->userdata['logged_in'])){echo base_url().'assetsofpop/earnRewCheck.png';?>"style="<?php echo "background-color:green;";  }?>">
				<div class="earnRewardsBox ">
					<div class="leftRewardBox">
						<span>
					<a  href="<?php echo base_url(); ?>pages/view/signup">
						<button class="rewsctionundertext" type="button" style="
						<?php if( !isset($this->LoggedInUser)){ ?>
								background-color: #ffffff!important;

								padding:0;
						<?php }?>
								">
							Login to Join
						</button>
					</a>
				</span>
					</div>
					<div class="rightRewardBox">
						<img  src="<?php echo base_url() ?>assetsofpop/img/icon-coin-no-border.svg"><span class="afterCoinText">23</span>
						<i class="fa fa-angle-down downIcon"></i>
					</div>
				</div>
				</div>


				<?php }?>

				<div class="earnRewardsBox earnRewardsBoxMain" <?php if(!isset($this->session->userdata['logged_in'])){
					echo 'onclick="beamember()" id="joingroupfromcomment"';
					echo 'style="cursor:pointer"';
				} ?>>
					<img  src="<?php  if(!isset($this->session->userdata['logged_in'])){ echo base_url().'assetsofpop/earnRewCheckBlack.png';} if(isset($this->session->userdata['logged_in'])){echo base_url().'assetsofpop/earnRewCheck.png';?>"style="<?php echo "background-color:green;";  }?>">
					<div class="earnRewardsBox">
					<div class="leftRewardBox">
					<span class="rewsctionundertext">Become a Member</span>
					</div>
					<div class="rightRewardBox">
					<img  src="<?php echo base_url() ?>assetsofpop/img/icon-coin-no-border.svg"><span class="afterCoinText">23</span>
						<i class="fa fa-angle-down downIcon"></i>
					</div>
					</div>
				</div>
				<div class="earnRewardsBox earnRewardsBoxMain">
					<img  src="<?php echo base_url() ?>assetsofpop/earnRewCheckBlack.png">
				<div class="earnRewardsBox">
					<div class="leftRewardBox">
					<span class="rewsctionundertext"style="cursor: pointer">Follow @ImageSC</span>
					</div>
					<div class="rightRewardBox">
					<img  src="<?php echo base_url() ?>assetsofpop/img/icon-coin-no-border.svg"><span class="afterCoinText">23</span>
					<i class="fa fa-angle-down downIcon"></i>
					</div>
				</div>
				</div>
				<?php
				if (!empty($posts_item['Po_Bn_ID']) || !empty($posts_item['Po_Bn_Name'] ) || !empty($posts_item['Po_Bn_Page']) ) { ?>
					<!-- <a class="btn invite_member_btn btn post_action_button" href='#' data-toggle='modal' data-target="#external_link_modal"></a> -->

						<div class="earnRewardsBox earnRewardsBoxMain">
								<img id="answerSurveyCheck" src="<?php echo base_url() ?>assetsofpop/earnRewCheckBlack.png">
						<div class="earnRewardsBox">
							<div class="leftRewardBox">
								<a class="rewsctionundertext" id=" answerSurveyLink" target="_blank"  href="<?php echo $posts_item['Po_Bn_Page']; ?>"><span>Take a 5min Survey</span></a>
							</div>
							<div class="rightRewardBox">
								<img  src="<?php echo base_url() ?>assetsofpop/img/icon-coin-no-border.svg"><span class="afterCoinText">23</span>
								<i class="fa fa-angle-down downIcon"></i>
							</div>
						</div>


				<?php } ?>
							<style>
								.surveyChecked{
									background-color: green !important;
								}

							</style>
<script>
	let answerSurveyLink = document.getElementById(' answerSurveyLink')
	let answerSurveyCheck = document.getElementById('answerSurveyCheck')

	let isChecked = true;
	answerSurveyLink.style.color = "black";
answerSurveyLink.addEventListener("click",()=>{
	answerSurveyCheck.addEventListener("click", () => {
		if (isChecked) {
			answerSurveyCheck.style.background = "green";
			answerSurveyCheck.src = '<?php echo base_url() ?>assetsofpop/earnRewCheck.png';
			answerSurveyLink.innerText = "Action Completed";
			answerSurveyLink.style.color = "black";
			isChecked = false;
		} else {
			answerSurveyCheck.style.background = "transparent";
			answerSurveyCheck.src = '<?php echo base_url() ?>assetsofpop/earnRewCheckBlack.png';
			answerSurveyLink.innerText = "Take a 5min survey";
			answerSurveyLink.style.color = "black";
			isChecked = true;
		}

})

	});
</script>
<style>
	.rewsctionundertext{
		outline: none !Important;
		border: none !Important;
	}
	.rewsctionundertext:hover{
		color: #0D859B !important;
	}
</style>
				</div>
					<?php if(isset($polldata) && !empty($polldata)){ ?>
				<div class="earnRewardsBox earnRewardsBoxMain">
					<?php
					if (isset($polldata) && !empty($polldata)&& $polldata[0]['poll_stop'] == 0){
						if (isset($polluserDetail) && !empty($polluserDetail)){
							echo '<img  src= "'. base_url() . 'assetsofpop/earnRewCheck.png" style="background-color:green;">';
						}
						else{
							echo '<img  src= "'. base_url() . 'assetsofpop/earnRewCheckBlack.png">';
						}

					}else{
						echo '<img  src= "'. base_url() . 'assetsofpop/earnRewCheck.png" style="background-color:green;">';
					}
					?>


				<div class="earnRewardsBox">
					<div class="leftRewardBox">
					<span>		<?php
						if(isset($polldata) && !empty($polldata)){ ?>
							<button class="rewsctionundertext" id="pollQuestionButton" data-toggle="modal" data-target="#<?php

							if ($polldata[0]['poll_stop'] == 0){

								if (isset($polluserDetail) && !empty($polluserDetail)){echo 'resultanswer';}else{echo 'poolanswer';}
							}else{
								echo 'resultanswer';
							}
							?>"><span><?php
									if ($polldata[0]['poll_stop'] == 0){
										if (isset($polluserDetail) && !empty($polluserDetail)){echo 'View Poll';}else{echo 'Answer Poll';}
									}else{
										echo 'View Poll';
									}
									?></span></button>

						<?php } ?></span>
					</div>
					<div class="rightRewardBox">
					<img  src="<?php echo base_url() ?>assetsofpop/img/icon-coin-no-border.svg"><span class="afterCoinText">23</span>
					<i class="fa fa-angle-down downIcon"></i>
					</div>
				</div>

				</div>
				<?php	} ?>
				<div class="under-thumbnail ">
					<?php if(isset($rewarddetails[0]['Vp_radeemprice'])) {?>
						<div class=" "
							 style="border: 0px;
										display: inline-flex;
    									background: #f8b84c7d;
									    border-radius: 22px;
										padding: 0px 0 4px 3px;
										width: 100%;
										justify-content: center;
										align-items: center;">

							<div class="justify-content-center" style="display: inherit; width: 90%;">
								<center style="display: inherit">
									<span style="
									margin-top: 7px;
										display: -webkit-box;
									font-family: 'Lato';
									font-style: normal;
									font-weight: 500;
									font-size: 16px;
									line-height: 150%;
									color: #004C83;"> Redeem Reward for <?php print_r($rewarddetails[0]['Vp_radeemprice']); ?>
									<img style="width: 25px; margin-left: 4px; margin-right: 4px;    " src="<?php echo base_url() ?>assetsofpop/img/icon-coin-no-border.svg"> Coins </span>

								</center>
							</div>
						</div>
					<?php } ?>
					<p class="group_metta mobile-hide">
						<span class="group_badge group_badge_<?=strtolower($posts_item['Po_Privacy'])?>"style="    border: 1px solid #004C83 !important;
    background: none !important;
    border-radius: 2rem;
       padding: 1px 18px;
    color: #004C83;
    font-size: 11px;
    font-family: 'Lato';
    line-height: 21px;"><?php echo ucfirst($posts_item['Po_Privacy']);?></span>
						<!--<span class="group_hearts">
										<i class="fas fa-heart"></i>&nbsp;<span id="counterheader"><?php echo $posts_item['Po_Likes'];?></span></span>-->

						<span class="group_memebers" style="font-size: 12px;margin-left:10px;">

										<?php echo $member_counter; ?> members
									</span>


					</p>
					<?php
					if(isset($creator_info)){
						if(  $posts_item['Po_Privacy'] == 'private' && $creator_info['role'] == 'non_member'){?>
							style="display: none;"
						<?php } } ?>

				</div>

		<div class="termsAndCond">
			<a class="linkBoard" >View Terms & Conditions</a>
		</div>
			</div>


		</div>

		</div>

		</div>

	</div>
</section>
<script>
	// get the comments container element
	let commentsContainer = document.getElementById('commentsRewCont');
	let mobileContainer = document.getElementById('mainPostContainer')
	let pcContainer = document.getElementById('pcCommentsContainer')

	// check the screen size and move the comments container accordingly
	function moveCommentsContainer() {
		if (window.innerWidth <= 993) {
			// desktop view - move comments container to the left
			pcContainer.removeChild(commentsContainer)
			mobileContainer.appendChild(commentsContainer);
		} else {
			if(mobileContainer.contains(commentsContainer)){
			pcContainer.appendChild(commentsContainer)
			mobileContainer.removeChild(commentsContainer);

			}
		}
	}

	// move the comments container on page load
	moveCommentsContainer();

	// move the comments container on window resize
	window.addEventListener('resize', moveCommentsContainer);
</script>


<style>
	.jquery-comments .delete.enabled{
		background-color: #E87979 !important;
	}
	.jquery-comments .delete{
			 padding: 2px 10px !important;
			 border-radius: 8px;
			 font-size: 14px;
			 margin: 7px;
			 position: relative;
			 bottom: -2px;
	}
</style>
<input type="hidden" name="Po_ID" id="Po_ID" value="<?=$EncodedID?>" />
<input type="hidden" name="OwnerId" id="OwnerId" value="<?= isset($this->session->userdata['logged_in']['bs_id']) ? $this->session->userdata['logged_in']['bs_id'] : '' ?> "
/>
<input type="hidden" name="PostUdId" id="PostUdId" value="<?=$posts_item['Po_Us_ID']?>"
/>
<input type="hidden" name="GroupUdId" id="GroupUdId" value="<?=$posts_item['Po_Gr_ID']?>"
/>
<input type="hidden" name="Groupencode" id="Groupencode" value="<?php echo encode_id($posts_item['Po_Gr_ID']);?>"
/>
<input type="hidden" name="post_type" id="post_type" value="<?=$posts_item['Po_Privacy']?>"
/>
<input type="hidden" name="userType" id="userType" value="<?php if(isset($userType)){ echo $userType; }?>"/>
<script type="text/javascript">
	var profile_picture = "<?=(!empty($user_data['Us_Photo'])) ? $user_data['Us_Photo'] : ''; ?>";
</script>

