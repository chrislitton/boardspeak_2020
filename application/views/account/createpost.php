
<script>

	const isValidUrl = urlString=> {
		try {
			return  /^(ftp|http|https):\/\/[^ "]+$/.test(urlString);
		}
		catch(e){
			return false;
		}
	}
	function validationForm() {


		if ($('#privatepostlimit').val() == 0 && $('input[name=radPrivacy]:checked').val() == 'private') {
			Swal.fire({
				icon: 'info',
				title: 'info',
				text: 'You have reached FREE limit of board posting in PRIVATE mode',

			});
			return false;
		}

		if ($('#txtLandingPage').val() == '' && $('#selActionButton').val() != '') {



			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Please Enter Landing Page URL',

			})
			$('#txtLandingPage').focus();
			return false;
		}
		else {
			let ourstring = "https";
			let ourstring2 = "http";



			if($('#selActionButton').val() != ''){
				if( !isValidUrl($('#txtLandingPage').val())){
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Please make sure you enter https://www or http://www before your landing page website link',
						confirmButtonColor:'#15AFE8',
						confirmButtonText : 'Okay'

					})
					return false;
				}
			}



			if ($('#selActionButton').val() != '') {
				if ($('#txtLandingPage').val().includes(ourstring) || $('#txtLandingPage').val().includes(ourstring2)) {
					return true;
				} else {
					$('#txtLandingPage').focus();
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Please make sure you enter https://www or http://www before your landing page website link',

					})

					return false;
				}
			} else {
				return true;


			}
			return false;
		}
	}

</script>

<?php


	if (empty($topic_details)) {
		$topic_details['To_ID'] = '';
		$topic_details['To_Gr_ID'] = '0';

		if (!empty($EncodedID)) {
			$topic_details['To_ID'] = '0';
			$topic_details['To_Gr_ID'] = decode_id($EncodedID);
		}
	}
 		$attribute = array(
           'id' => 'created_Post_Form',
                    'onSubmit'  =>  'return validationForm(this)'
     );


	echo form_open_multipart('account/create/savepost' , $attribute);
?>

<input type="hidden" name="To_ID" id="To_ID" value="<?php echo $topic_details['To_ID'];?>" />
<!-- this is create post here ahsan -->
<div class="fomr d-flex">
	<section  class="dethead fomr">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
				
					<!-- Start of Masthead -->
					<header id="groupbanner" class="mb-4">
						<div class="container">
							<div class="row">
								<div class="col-md-12 col-lg-12 text-center">

									<?php
									if($topic_details['To_ID'] != 0  ){ ?>
										<a style=" position: absolute;
    left: 0;
    top: -22px;   float: left;" href="<?php echo base_url().'account/view/topic/'.encode_id($topic_details['To_ID']) ?>"> <  <?php echo $topic_details['To_Name'] ;?></a>

								<?php 	}else{
									 foreach ($groups_items as $item){
										 if($topic_details['To_Gr_ID'] == $item['Gr_ID']){ ?>
											  <a style=" position: absolute;
    left: 0;
    top: -22px;   float: left;" href="<?php echo base_url().'account/view/group/'.encode_id($item['Gr_ID']) ?>"> <  <?php echo $item['Gr_Name'] ;?></a>

										<?php  }



									 }
									}

									?>
								 <h3 class="text-primary">Create Post</h3>
								</div>
								<div class="col-md-12 col-lg-12 board-banner text-right bg-post post-cover-image" id="site-banner" style="box-shadow: none;" >
									<!-- <div class="post-title text-white">

									</div> -->
									
									<div style="box-shadow: none;"  class="col-md-12 col-lg-12 board-banner text-center">
										<div class="col-md-12 col-lg-12 text-center">
											<div class="btn btn-primary mt-3"  style="background: #398cb3a6!important;
                                                                                         border: 2px solid white!important;
                                                                                         border-radius: 0px!important;
                                                                                         color: white!important;
                                                                                         font-weight: 600!important;

																						 border-radius: 9px!important;"
                                                                                                                                                                       border: 2px solid white!important;
                                                                                                                                                                       border-radius: 0px!important;
                                                                                                                                                                       color: white!important;
                                                                                                                                                                       font-weight: 600!important;">
												<form method="post">
													<label class="title-tip title-tip-left"  title="Change Board Cover" for="upload_image" style="margin-bottom:0;">
														<img src="" id="uploaded_image" class="img-responsive img-circle" />
														<!-- <i class="fas fa-camera "></i> -->
														<img src="<?php echo base_url(); ?>assetsofpop/img/replaceCamera.png" alt="Upload">
														<input type="file" name="image" class="image" id="upload_image" style="display:none" accept="image/*" />
														<input type="hidden" name="userfile" id="userfile" />
													</label>
												</form>
											</div>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</header>
					<!-- End of Masthead -->

					<section class="entry-section">
						<div class="container">
			
							<?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>

								<div class="row">
									<div class="col-lg-12">

										<div class="control-group d-none">
											<div class="admin-form-group controls mb-0 pb-1">
												<input class="form-control" id="txtbg" name="txtbg" type="text" placeholder="Background">
												<input class="form-control" id="txtContent" name="txtContent" type="hidden" placeholder="Background">
											</div>
										</div>
											<input type="hidden" name="rewardid" id="rewardid" value="<?php if(isset($rewarddata[0]['Vp_id'])){
												echo $rewarddata[0]['Vp_id'];
											} ?>">
										<div class="control-group mb-2 form-group">
											<label class="form-required font-weight-bold label-h6">Title</label>
											<div class="admin-form-group controls mb-0 pb-1">
												<input class="form-control" id="txtTitle" name="txtTitle" type="text"

													   	value="<?php if(isset($rewarddata[0]['Vp_title'])){
															   echo $rewarddata[0]['Vp_title'];
														} ?>"
													   placeholder="Enter post title" required="required">
											</div>
										</div>

															
										<div class="control-group mb-2 form-group">
											<label class="font-weight-bold label-h6">Description </label><span class="ml-1 text-primary font-weight-normal text-small">You may skip and fill this up later.</span>
											<div class="admin-form-group controls mb-0 pb-1">
												<textarea class="form-control" id="txtDescription" name="txtDescription"></textarea>
											</div>
										</div>

										<div class="control-group mb-2 form-group">										
											<label class="form-required font-weight-bold label-h6">Group/Community Name </label>
											<div class="admin-form-group controls mb-0 pb-1">
												<div class="input-group">



<!--													--><?php
//													foreach ($groups_items as $item){
//														echo $topic_details['To_Gr_ID'] .' / /'. $item['Gr_ID']. ' .. .. ';
//													}
//
//													?>
													<select class="custom-select" id="selGroup" name="selGroup" required="required">



													<option value="">Choose group or create new group</option>
														<?php
															foreach ($groups_items as $item):
																$selected = ($topic_details['To_Gr_ID'] == $item['Gr_ID']) ? "selected='selected'" : '' ;
														?>
																<option value="<?php echo $item['Gr_ID']; ?>"  <?=$selected?>><?php echo $item['Gr_Name']; ?> </option>
														<?php endforeach; ?>
													</select>
												</div>
										</div>
										<div class="text-right">
											<a class="" href="<?php echo base_url(); ?>account/create/group">Create New Group</a>
										</div>
									</div>


									<div class="control-group mb-2 form-group">
										<label class="form-required font-weight-bold label-h6">Subgroups</label> <span class="ml-1 text-primary font-weight-normal text-small">Is this Post a subgroup? If not, choose None.</span>
										<div class="admin-form-group controls mb-0 pb-1">                
											<div class="input-group">
<!--												required="required"-->
												<select class="custom-select" id="selTopic" name="selTopic" >
													<option value="-1"  selected='selected' >Choose topic or create new subgroup</option>
													<option value="0">None</option>
												</select>
											</div>          
										</div>
										<div class="text-right">
											<a class="" href="<?php echo base_url(); ?>account/create/topic">Create New Subgroup</a>
										</div>
									</div>

									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Category</label> <span class="ml-1 text-primary font-weight-normal text-small">Categorize your Post to organize your group communication</span>
										<div class="admin-form-group controls pb-1">
											<div class="input-group">

<!--												--><?php
//												echo '<pre>';
//												print_r($category_items); ?>
												<select class="custom-select" id="selCategory" name="selCategory" required="required">
												<!-- <select class="custom-select" id="selCategory" name="selCategory" required="required" multiple="multiple"> -->


													<?php foreach ($category_items as $itemx):
										 		$selected = ($itemx['Ca_ID']  == $catId) ? "selected='selected'" : '' ;

													 ?>

													 <option <?php echo $selected; ?> value="<?php echo $itemx['Ca_ID']; ?>" >
														 <?php

														 if( $itemx['Ca_ID'] == 0  ||  $itemx['Ca_ID'] == -1){ ?>
															 <font style="background: red; color:red!important;"><?php  echo $itemx['Ca_Name'];
																 if(isset($itemx['Ca_Important'])){
																	 echo $itemx['Ca_Important'];
																 }
															 echo "(custom)"; ?></font>
														<?php }else{
															 echo  $itemx['Ca_Name'] ;
															 if(isset($itemx['Ca_Important'])){
																 echo $itemx['Ca_Important'];
															 }
														 }

														 ?>
														 </option>

													<?php endforeach; ?>

													<option value="0"  <?php if(!is_numeric($catId) && strlen( $catId ) > 1 ){
														echo  'selected';
													} ?>>Others, please specify</option>
												</select>
											</div>
										</div>
												
										<div class="control-group mb-2" id="selNewCategoryBox" style="display:<?php if(!is_numeric($catId) && strlen( $catId ) > 1){
											echo  '';
										}else{
											echo 'none';
										} ?>;">
											<div class="admin-form-group controls pb-1">

												<input class="form-control" id="txtNewCategory" value="<?php if(!is_numeric($catId) && strlen( $catId ) > 1){
													echo str_replace('%20', ' ', $catId);



												}else{

												} ?>" name="txtNewCategory" type="text" placeholder="Enter new category"   <?php if(!is_numeric($catId) && strlen( $catId ) > 1){
													echo  'readonly';
												}else{

												} ?>>
											</div>
										</div>
									</div>

									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Sub-Category</label> <span class="ml-1 text-primary font-weight-normal text-small">Sub-categorize your Post to organize your group communication</span>
										<div class="admin-form-group controls pb-1">
											<div class="input-group">

												<!-- <select class="custom-select" id="selSubCategory" name="selSubCategory" required="required" multiple="multiple">-->
<!--									--><?php
//
//									echo '<pre>';
//print_r($sub_catogery); ?>
												<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required">
 														<?php foreach ($sub_catogery as $items):
                                                		 $selected = ($items['Sc_ID']  == $subcatId) ? "selected='selected'" : '' ;  ?>
 													 <option <?php echo $selected; ?> value="<?php echo $items['Sc_ID']; ?>" ><?php echo $items['Sc_Name']; ?></option>
 													<?php endforeach; ?>


													<option value="0"  <?php

													if(count($sub_catogery) == 0){
														echo 'selected';
													}

													else if(!is_numeric($subcatId) && strlen( $subcatId ) > 1 ){
														echo  'selected';
													} ?>>Others, please specify</option>
												</select>
											</div>
										</div>

										<div class="control-group mb-2" id="selNewSubCategoryBox"
										<?php if(!is_numeric($subcatId) && strlen( $subcatId ) > 1){
											echo 'required';
										}?>

											 style="display:<?php
											 if(count($sub_catogery) == 0){
												 echo 'block';
											 }else   if(!is_numeric($subcatId) && strlen( $subcatId ) > 1){
											echo '';
										}else{
										  if(!is_numeric($catId) && strlen($catId) > 1)  {
											  echo 'block';
										  }else{
											  echo 'none';
										  }

										} ?>">
											<div class="admin-form-group controls pb-1">
												<input class="form-control" id="txtNewSubCategory"   name="txtNewSubCategory"
													   type="text" placeholder="Enter new sub-category"
													   value="<?php if(!is_numeric($subcatId) && strlen( $subcatId ) > 1){

													echo str_replace('%20', ' ', $subcatId);
												}else{

												} ?>">
											</div>
										</div>
									</div>

									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Add Action Button</label>
										<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<select class="custom-select" id="selActionButton" name="selActionButton" placeholder='Choose your Action Button'>
												<option value=''>Choose your Action Button</option>
												<?php foreach ($button_items as $item): ?>
													<?php echo '<option value="'.$item['Bn_ID'].'">'.$item['Bn_Caption'].'</option>'; ?>
												<?php endforeach; ?>
												<option value="0">Others, please specify</option>
												</select>
											</div>
										</div>

										<div class="control-group mb-2" id="selNewActionButton" style="display:none;">
											<div class="admin-form-group controls pb-1">									
												<input class="form-control" id="txtActionButton" name="txtActionButton" type="text" placeholder="Enter new action caption">
											</div>
										</div>
									</div>

									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Landing Page</label><span class="ml-1 text-primary font-weight-normal text-small" style="color:red!important;">Please complete the link (by adding https://www. )</span>
										<div class="admin-form-group controls mb-0 pb-1">
											<div class="input-group">
												<input class="form-control" id="txtLandingPage" name="txtLandingPage" type="text" placeholder="Enter the URL for your action button" disabled="disabled">
											</div>
										</div>
									</div>

									<div class="control-group mb-4 form-group">
										<div class="row plr10">
											<div class="col-sm-6 col-12 plr5 mt10">
												<label class="font-weight-bold label-h6">Add Image</label>
												<div class="input-group">
								
													<input type="file" name="images[]" class="dropify" accept=".gif,.png,.jpg,.jpeg" data-default-file="" multiple />
												</div>
											</div>
											<div class="col-sm-6 col-12 plr5 mt10">
												<label class="font-weight-bold label-h6">Add File</label>
												<div class="input-group">
													<input type="file" name="files[]" class="dropify" data-default-file="" multiple />
												</div>
											</div>
										</div>
									</div>
								
									<div class="control-group mb-2 form-group">
										<label class="font-weight-bold label-h6">Keyword Tags</label><span class="ml-1 text-primary font-weight-normal text-small">Tags will help people find your post. Separate tags with comma.</span>
										<div class="admin-form-group controls mb-0 pb-1">
											<input class="form-control" id="txtKeywords" name="txtKeywords" type="text">
										</div>
									</div>
										<style>
											.switch {
												position: relative;
												display: inline-block;
												width: 50px;
												height: 29px;
											}

											.switch input {
												opacity: 0;
												width: 0;
												height: 0;
											}

											.slider {
												position: absolute;
												cursor: pointer;
												top: 0;
												left: 0;
												right: 0;
												bottom: 0;
												background-color: #ccc;
												-webkit-transition: .4s;
												transition: .4s;
											}

											.slider:before {
												position: absolute;
												content: "";
												height: 21px;
												width: 21px;
												left: 4px;
												bottom: 4px;
												background-color: #00854B;
												-webkit-transition: .4s;
												transition: .4s;
											}

											input:checked + .slider {
												background-color: white;
											}

											input:focus + .slider {
												box-shadow: 0 0 1px #2196F3;
											}

											input:checked + .slider:before {
												-webkit-transform: translateX(26px);
												-ms-transform: translateX(26px);
												transform: translateX(26px);
											}

											/* Rounded sliders */
											.slider.round {
												border-radius: 34px;
											}

											.slider.round:before {
												border-radius: 50%;
											}
											#earnRewardsContainer{
												background-color: #F6F6F6;
												width: 100%;
												padding: 1rem;
											}
											.margin-t-1{
												margin-top: 1rem;
											}	.margin-l-1{
												margin-left: 1rem;
											}	.margin-l-half{
												margin-left: 0.5rem;
											}
										#earnRewardsContainer header p {
											text-align: right;
											margin: 0;
										}
											#earnRewardsContainer input.articleBox{
												width: 100%;
											}
											#earnRewardsContainer section{
												margin-top: 2rem;
											}
											#earnRewardsContainer article{
                                                width: 95%;
                                                max-width: 586px;
												display: flex;
												align-items: center;
												justify-content: space-between;
                                                margin: 1rem 0;
											}
											#earnRewardsContainer .middleArticle img{
                                                width: 15px;
                                                margin-left: 0.5rem;
												height: auto;
											}
											#earnRewardsContainer .articleBox{
												border-radius: 8px;
												padding: 0.4rem 0.8rem;
												border: 1px solid;
												background-color: white;
											}

                                            #earnRewardsContainer .middleArticle .articleBox{
                                                width: 90%;
                                            }

											#earnRewardsContainer .articleBox img{
												width: 20px;
                                                height: 20px;

											}
											#earnRewardsContainer .middleArticle{
												display:flex;
												align-items: center;
                                                gap: 5px;
                                                width: 80%;
                                                margin:0 0.5rem;
											}
											#earnRewardsContainer img{
												margin-right: 5px;
											}

											#earnRewardsContainer .rightArticle .articleBox{
												display: flex;
                                                align-items: center;
                                                justify-content: center;
											}

                                            .first-section-rew{
                                                display: flex;
                                                align-items: center;
                                                width: 100%;
                                                justify-content: space-between;
                                            }
                                            .right-section-rew{
                                                background-color: #FFFFFF;
                                                padding: 0.5rem 1rem;
                                            }
                                            #coin-text{
                                                display: flex;
                                                background-color: #F6F6F6;
                                                width: max-content;
                                                padding: 5px;
                                            }
                                            .left-section-rew{
                                                width: 70%;
                                            }
.input_long{
    min-width: 121%;
    margin-top: 0.5rem;
}
#earnRewardsContainer .article_inputs,#earnRewardsContainer .article_inputs .middleArticle{
    align-items: start;
}
#earnRewardsContainer .article_inputs .middleArticle {
    width: auto;
}
@media screen and (max-width:1199px) {
    .input_long {
        min-width: 121%;
    }
}@media screen and (max-width:991px) {
                                                .first-section-rew,.right-section-rew {
                                            width:100%;
                                            }
												.first-section-rew {
												flex-direction: column;}
												.left-section-rew{
													width:100%
												}
											}
											@media screen and (max-width:400px) {
												.input_long {
													min-width: 145%;
												}
											}
										</style>
										<div id="earnRewardsContainer">
											<header>
											<h3 class="margin-t-1">Earn Coins To Redeem Reward</h3>
											<input  class="margin-t-1 articleBox" placeholder=" Enter Reward that participants will get after accomplishing your tasks. Ex. Get FREE (your brand) meal!">
												<p class="margin-t-1">28/30</p>
											</header>
											<section>
										<p>Select from two (2) tasks below that your participants must complete in order to earn their coins and receive their reward.</p>
          <div class="first-section-rew">
                                                <div class="left-section-rew">
                                                <article>
	<div class="leftArticle">
		<label class="switch">
			<input type="checkbox" checked>
			<span class="slider round"></span>
		</label>
	</div>
	<div class="middleArticle">
		<img src="<?php echo base_url().'assetsofpop/drag-icon-post.png'   ?>">
		<div class="articleBox">
			Become a member
		</div>
	</div>
	<div class="rightArticle">
		<div class="articleBox">
			<img src="<?php echo base_url().'assetsofpop/coinPost.png'   ?>">
			<span>50</span>
		</div>
	</div>
</article>                                                <article>
	<div class="leftArticle">
		<label class="switch">
			<input type="checkbox" checked>
			<span class="slider round"></span>
		</label>
	</div>
	<div class="middleArticle">
		<img src="<?php echo base_url().'assetsofpop/drag-icon-post.png'   ?>">
		<div class="articleBox">
			Answer Poll
		</div>
	</div>
	<div class="rightArticle">
		<div class="articleBox">
			<img src="<?php echo base_url().'assetsofpop/coinPost.png'   ?>">
			<span>50</span>
		</div>
	</div>
</article>
                                                </div>
                                                <div class="right-section-rew">
                                                    <p>You're giving away</p>
                                                    <div id="coin-text">
                                                        <img src="<?php echo base_url().'assetsofpop/coinPost.png'   ?>">
                                                        <span class="margin-l-half">500</span>
                                                        <span class="margin-l-half">Boost Coins</span>
                                                    </div>
                                                    <p>per participant who completes and satisfies your terms & conditions.</p>
                                                </div>
          </div>

											</section>	<section>
										<p>Create other tasks that your participants must complete in order to earn their coins and receive their reward.</p>

                                                <article class="article_inputs">
	<div class="leftArticle">
		<label class="switch">
			<input type="checkbox" checked>
			<span class="slider round"></span>
		</label>
	</div>
	<div class="middleArticle">
		<img src="<?php echo base_url().'assetsofpop/drag-icon-post.png'   ?>">
        <div class="input-container">
		<input class="articleBox input_short"placeholder="Ex. Follow us @social media name">
		<input class="articleBox input_long"placeholder="Enter your Business Social Media URL Link Here">
        </div>
    </div>
	<div class="rightArticle">
		<div class="articleBox">
			<img src="<?php echo base_url().'assetsofpop/coinPost.png'   ?>">
			<span>50</span>
		</div>
	</div>
</article>
     <article class="article_inputs">
	<div class="leftArticle">
		<label class="switch">
			<input type="checkbox" checked>
			<span class="slider round"></span>
		</label>
	</div>
	<div class="middleArticle">
		<img src="<?php echo base_url().'assetsofpop/drag-icon-post.png'   ?>">
        <div class="input-container">
            <input class="articleBox input_short"placeholder="Ex. Follow us @social media name">
            <input class="articleBox input_long"placeholder="Enter your Business Social Media URL Link Here">
        </div>
	</div>
	<div class="rightArticle">
		<div class="articleBox">
			<img src="<?php echo base_url().'assetsofpop/coinPost.png'   ?>">
			<span>50</span>
		</div>
	</div>
</article>
                                                </div>


											</section>
										</div>
									<div class="control-group mb-4 form-group">
										<div class="admin-form-group controls mb-0 pb-1 radio_btn_group">
											<label class="form-required font-weight-bold label-h6">Privacy</label>

											<!-- Default unchecked -->
											<div class="custom-control custom-radio pb-2 radio_btn_item">
												<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public" checked>
												<label class="custom-control-label" for="radPublic">Public </label><br>Anyone in your group can access and join the conversation.
											</div>

											<!-- Default checked -->
											<div class="custom-control custom-radio pb-2 radio_btn_item">
												<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private">
												<label class="custom-control-label" for="radPrivate">Private </label><br>Anyone in your group can find your post. But access to join the conversations, is limited to invited members and approved by admin.
											</div>

											<!-- Default checked -->
											<div class="custom-control custom-radio pb-2 radio_btn_item">
												<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret">
												<label class="custom-control-label" for="radSecret">Secret </label><br>Only those with link and unique password in your group, can find and join your post.
											</div>

										</div>
									</div>

					
									<div class="control-group mt-5 text-center">
										<div class="admin-form-group">
											<a class="btn btn-light btn-xl mr-1" href="">Cancel</a>
											<button type="submit" class="btn btn-primary btn-xl ml-1" id="sendMessageButton">Create</button>
										</div>
									</div>
				
								</div>
							</div>

						</div>
					</section>
				</div>	
			</div>
		</div>
		<!-- here ahsan -->
	</section>
	<div class="right-column">
						<img style="border-radius: 20px; " src="http://localhost/boardspeak_2020/assetsofpop/img/GruppoAd_sidebar.png" alt="">
						</div>
					</div>
	
<style>
.fomr{justify-content:space-evenly;}
.right-column{
	width: 28%;
    background: #f5f5f5;
    padding: 29px;
	border-radius: 1rem;
	margin-top: 25px;"
}
.right-column img{margin:auto;}
@media screen and (max-width:973px) {
	.fomr{
		flex-direction:column;
		
	}
	.right-column{
	width:100%;
}
}</style>
<input type="hidden" name="groupid" id="groupid" value="<?php

if (isset($groups_data['Gr_ID'])){
	echo encode_id($groups_data['Gr_ID']);
}
 ?>" >

<input type="hidden" name="grouppaakcage" id="grouppaakcage" value="<?php
if (isset($groups_data['Gr_ID'])){
echo  $groups_data['Gr_packegtype']; } ?>" >
 	<input type="hidden" name="privatepostlimit" id="privatepostlimit" value="<?php if(isset($privatepostlimit)){echo $privatepostlimit;}  ?>">
	<input type="hidden" name="limitation" id="limitation" value="<?php if(isset($cat_limit)){ echo $cat_limit;}  ?>"
	<input type="hidden" name="To_Gr_ID" id="To_Gr_ID" value="<?php echo $topic_details['To_Gr_ID'];?>" />
	<input type="hidden" name="inp_category_id" id="inp_category_id" value="<?=$catId?>" /> 
	<input type="hidden" name="inp_subcategory_id" id="inp_subcategory_id" value="<?=$subcatId?>" /> 
	<input type="hidden" name="hid_none_topic" id="hid_none_topic" value="<?=$nonetopic?>" />
	<input type="hidden" name="createType" id="createType" value="<?php if(isset($createType)){  echo $createType; }  ?>" />
</form>
<div class="modal fade bd-example-modal-lg" id="packagesmodel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- This snippet uses Font Awesome 5 Free as a dependency. You can download it at fontawesome.io! -->
			<section class="pricing py-5" style="overflow-y: scroll;
													height: 600px;
													overflow-x: hidden;">
				<div class="container">
					<h4 align="center" style="color: white; margin-bottom: 20px;">Choose Your Plan</h4>
					<h6 align="center" style="margin-bottom: 22px;"><font style="color: #ffffff;" onclick="contatus()">Contact Us </font> for a more customized plan </h6>
					<div class="row">
						<!-- Free Tier -->
						<div class="col " id="firstPackege">
							<div class="card mb-5 mb-lg-0">
								<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">FREE</h4>
								<div class="card-body">

									<h5 class="card-title text-muted text-uppercase text-center">Free</h5>
									<h6 class="card-title text-center">PHP<span
												class="card-price text-center">0<span class="period">/month</span></h6>
									<h6 class="card-title text-muted text-uppercase text-center">No Payment Required</h6>
									<hr>
									<ul class="fa-ul">
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>1GB Storage</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting in Private Mode (with Select Members of the Group only)</li>

										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 6 Group Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting with Mute/Unmute Select Members Feature</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 5 Task Assignments/To Do's per Registered Member</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>

										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Priority Support </li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Monthly Subscription</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Customize your group page (background and fonts)</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>

									</ul>
									<div class="d-grid">
										<!--											<button  style="width: 100%;background: #1887f2 !important; "  onclick="choosePayment(1)" class="btn btn-primary text-uppercase" id="firstpage_button">Choose</button>-->
									</div>
								</div>
							</div>
						</div>
						<!-- Plus Tier -->
						<div class="col ">
							<div class="card mb-5 mb-lg-0">
								<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
								<div class="card-body">

									<h5 class="card-title text-muted text-uppercase text-center">Plus</h5>
									<h6 class="card-title text-center">PHP<span
												class="card-price text-center">599<span class="period">/month</span></h6>

									<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

									<hr>
									<ul class="fa-ul">
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>10GB Storage</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
										<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


									</ul>
									<div class="d-grid">
									 <a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment(2)" class="btn btn-primary text-uppercase">Choose</a>
									</div>
								</div>
							</div>
						</div>
						<!-- Pro Tier -->
						<div class="col ">
							<div class="card mb-5 mb-lg-0">
								<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
								<div class="card-body">
									<h5 class="card-title text-muted text-uppercase text-center">PRO</h5>
									<h6 class="card-title text-center">PHP<span
												class="card-price text-center">899<span class="period">/month</span></h6>

									<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

									<hr>
									<ul class="fa-ul">
										<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>30GB Storage</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>5 Sub-groups for multiple divisions / department's use</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Sub-groups own set of 6 Categories/Subcategories (any combination)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
										<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


									</ul>
									<div class="d-grid">
										<a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment(3)" class="btn btn-primary text-uppercase">Choose</a>
									</div>
								</div>
							</div>
						</div>
			</section>
			<script>
				function choosePayment(packagetype){
					// packagetype
					// respond_to_group_invitation
					$.ajax({
						url: base_url + 'group/upgrageplan',
						method: 'POST',
						dataType: 'json',
						data: {
							id: $('#groupid').val(),
							packagetype: packagetype
						},
						success: function (response2) {
							if(response2 == 1){
								Swal.fire({
									icon: 'success',
									title: 'Package upgraded',
									text: 'success',

								});
								location.reload();
							}
						}
					});
				}
			</script>
			<style>
				section.pricing {
					background: #007bff;
					background: linear-gradient(to right, #33AEFF, #398cb3);
				}

				.pricing .card {
					border: none;
					border-radius: 1rem;
					transition: all 0.2s;
					box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
				}

				.pricing hr {
					margin: 1.5rem 0;
				}

				.pricing .card-title {
					margin: 0.5rem 0;
					font-size: 0.9rem;
					letter-spacing: .1rem;
					font-weight: bold;
				}

				.pricing .card-price {
					font-size: 3rem;
					margin: 0;
				}

				.pricing .card-price .period {
					font-size: 0.8rem;
				}

				.pricing ul li {
					margin-bottom: 1rem;
				}

				.pricing .text-muted {
					opacity: 0.7;
				}

				.pricing .btn {
					font-size: 80%;
					border-radius: 5rem;
					letter-spacing: .1rem;
					font-weight: bold;
					padding: 1rem;
					opacity: 0.7;
					transition: all 0.2s;
				}

				/* Hover Effects on Card */

				@media (min-width: 992px) {
					.pricing .card:hover {
						margin-top: -.25rem;
						margin-bottom: .25rem;
						box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.3);
					}

					.pricing .card:hover .btn {
						opacity: 1;
					}
				}
			</style>

		</div>
	</div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
        		<div class="img-container">
            		<div class="row">
                		<div class="col-md-8">
                    		<img src="" id="crop_uploaded_image" />
                		</div>
                		<div class="col-md-4">
                    		<div class="preview" style="max-width:100%!important;"></div>
				      		<div class="modal-footer" style='padding-right: 0px;'>
				      			<button type="button" id="crop" class="btn btn-primary">Upload</button>
				        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				      		</div>
                		</div>
            		</div>
        		</div>
      		</div>
    	</div>
  	</div>
</div>
