<?php
	$UserID = ($this->session->userdata['logged_in']['bs_id']);
	$show_load_btn = "";
	if (count($posts_items)<12) $show_load_btn = " d-none";
?>

<!-- Masthead -->
  <header class="dethead mb-0 pb-0">
    <!-- <div class="container d-flex align-items-center flex-column"> -->
    <div class="container-fluid">

			<div class="row mb-3">
				<div class="col-lg-12 text-center">

						<h5 class="py-3">
								<a class="d-inline under-link px-2 active underlined" href="<?php echo base_url(); ?>account/followed">Followed</a>
								<a class="d-inline under-link px-2" href="<?php echo base_url(); ?>account">Explore</a>
						</h5>


				</div>
			</div>

	</div>
  </header>

<section  class="dethead mt-0 pt-0">
    <div class="container">
			<div class="row">
				<div class="col-lg-9">








					<section class="entry-section">
					  <div class="container">



							<div class="row">



							<?php foreach ($posts_items as $item): ?>
							<?php
								$Survey = "";
								$CanEditSurvey = "";


								$PostLink = base_url().'account/post/'. $item['Po_ID'];
								if ($item['Po_Survey']!=0)
								{
									$Survey = "<div class='admin-img-box-survey bg-success text-white px-2 py-1'>Survey</div>";
									if (strcmp($item['Po_SurveyStatus'],'started')==0) $PostLink = base_url().'account/survey/start/'. $item['Po_ID'];
									else
									{
										if (strcmp($CurrUserID,$item['Po_Us_ID'])==0)
										{
											$PostLink = base_url().'account/survey/edit/'. $item['Po_ID'];
										}
										else
										{
											$CanEditSurvey = "d-none";
										}

									}
								}
							?>



								<div class="col-md-6 col-lg-4 admin-img-box loadmore">
									 <?php echo $Survey;?>
									<div class="admin-img <?php  if (strlen($item['Po_Thumb'])==0) echo 'bg-post'; echo ' '.$item['Po_Backcolor']; ?>">
										<a class="board-link" href="<?php echo $PostLink; ?>">
											<img class="img-fluid" src="<?php echo $item['Po_Thumb']; ?>" alt="">
										</a>

										<div class="admin-img-label shadow-sm">
											<div class="row">
												<div class="col-md-12 col-lg-12">
													<a class="board-link" href="<?php echo $PostLink; ?>">
													<p class="board-title"><?php echo $item['Po_Title']; ?></p>
													</a>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12 col-lg-12">
													<p class="lead post-info-small text-left text-muted">Posted on <?php echo date( "M d, Y", strtotime($item['Po_DatePosted']) ); ?></p>
												</div>
											</div>

										</div>

								</div>
								</div>

							<?php endforeach; ?>
							<div class="col-md-12 col-lg-12 mt-3 text-center<?php echo $show_load_btn;?>">
								<a class="btn btn-primary px-5" href="#" id="btnload">Load More</a>
							</div>

					</div>




					  </div>
					</section>



			</div>
			<div class="col-lg-3">

				<div class="sidebar bg-light mb-5">
				<h5 class="font-weight-normal text-primary">Communities</h5>
				<?php foreach ($groups_items as $item): ?>

						<div class="members">
							<a class="board-link text-left" href="<?php echo base_url(); ?>account/group/<?php echo $item['Gr_ID'];?>">
							<div class="rounded-circle members-photo bg-group <?php echo $item['Gr_Backcolor']; ?>" style="background:transparent url('<?php echo $item['Gr_Thumb']; ?>') no-repeat center center /cover"></div>
							<p class="members-name"><?php echo $item['Gr_Name']; ?></p>
							</a>
						</div>

				<?php endforeach; ?>
				</div>


				<?php if (count($topics_items)>0){ ?>
				<div class="sidebar bg-light">
				<h5 class="font-weight-normal text-primary">Topics</h5>
				<?php foreach ($topics_items as $item): ?>

						<div class="members">
							<a class="board-link text-left" href="<?php echo base_url(); ?>account/topic/<?php echo $item['To_ID'];?>">
							<div class="rounded-circle members-photo bg-topic <?php echo $item['To_Backcolor']; ?>" style="background:transparent url('<?php echo $item['To_Thumb']; ?>') no-repeat center center /cover"></div>
							<p class="members-name"><?php echo $item['To_Name']; ?></p>
							</a>
						</div>

				<?php endforeach; ?>
				</div>
				<?php } ?>


			</div>
    </div>
    </div>
  </section>
