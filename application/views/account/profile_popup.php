<div class="popup_container contact_popup">
    <div class="popup_item contact_popup_item">
        <span class="popup_closer contact_popup_closer">
            <i class="cross fas fa-times"></i>
        </span>
        <div class="contact_popup_left">
         <h4  align="center">Add Contact/s</h4>
            <h4 class="contact_popup_header"></h4>
            <ul class="contact_popup_nav">
                <li class="active" id="request" contact_data="request">
                    <span></span>
                    Request/s
                </li>
                <li class="" id="suggest" contact_data="suggest">
                    <span></span>
                    Suggested
                </li>
                <li class="" id="all" contact_data="all">
                    <span></span>
                    All Contacts
                </li>
                <li class="" id="explore" contact_data="explore">
                    <span></span>
                        Explore
                 </li>
            </ul>
            <div class="contact_filter">
                <div class="form-group contact_search">
                    <input class="form-control searchKey" type="text" placeholder="Search User">
                    <button id="btnUserSearch" class="btn">
                        search
                    </button>
                </div>
                <ul class="contact_filter_list">
                    <li class="active" data-sort="">
                        <a href="javascript:void(0)">Default</a>
                    </li>
                    <li class="" data-sort="date_desc">
                        <a href="javascript:void(0)">Newest</a>
                    </li>
                    <li class="" data-sort="date_asc">
                        <a href="javascript:void(0)">Oldest</a>
                    </li>
                    <li class="" data-sort="name_asc">
                        <a href="javascript:void(0)">A-Z</a>
                    </li>
                    <li class="" data-sort="name_desc">
                        <a href="javascript:void(0)">Z-A</a>
                    </li>
                </ul>
            </div>
            <div class="contact_content">
                <div class="contact_content_item active request">
                    <ul class="contacts_list"></ul>
                    <!-- <div class="view_all">
                        <a class="btn" href="">Viewl all</a>
                    </div> -->
                </div>
                <div class="contact_content_item suggest">
                    <ul class="contacts_list"></ul>
                    <!-- <div class="view_all">
                        <a class="btn" href="">Viewl all</a>
                    </div> -->
                </div>
                <div class="contact_content_item all">
                    <ul class="contacts_list"></ul>
                    <!-- <div class="view_all">
                        <a class="btn" href="">Viewl all</a>
                    </div> -->
                </div>
                  <div class="contact_content_item explore">
                    <ul class="contacts_list" style="overflow-x: scroll;
                                                         height: 300px;"></ul>
                           <!-- <div class="view_all">
                                      <a class="btn" href="">Viewl all</a>
                              </div> -->
                                </div>
            </div>
        </div>
        <div class="contact_popup_right">
            <a class="contact_side_img" href="">
                <img src="src/image/board_one.jpg" alt="sideImg">
            </a>
            <ul class="contact_side_list">
                <h5>
                    Lorem ipsum dolor sit amet.
                </h5>
                <li>
                    <img src="src/image/user_background.jpg" alt="">
                    <div class="contact_side_text">
                        <h6>
                            Gabriel Iglesias
                        </h6>
                        <p>
                            The flufffy guy
                        </p>
                    </div>
                    <a href="">
                        <i class="fas fa-angle-right"></i>
                    </a>
                </li>
                <li>
                    <img src="src/image/user_background.jpg" alt="">
                    <div class="contact_side_text">
                        <h6>
                            Gabriel Iglesias
                        </h6>
                        <p>
                            The flufffy guy
                        </p>
                    </div>
                    <a href="">
                        <i class="fas fa-angle-right"></i>
                    </a>
                </li>
                <li>
                    <img src="src/image/user_background.jpg" alt="">
                    <div class="contact_side_text">
                        <h6>
                            Gabriel Iglesias
                        </h6>
                        <p>
                            The flufffy guy
                        </p>
                    </div>
                    <a href="">
                        <i class="fas fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <a class="see_more" href="">
                See More
            </a>
        </div>
    </div>
</div>

<li class="contact_template" style="display:none;">
    <div class="contact_profile">
        <img src="" alt="">
        <div class="contact_info">
            <h6></h6>
            <p></p>
        </div>
    </div>
    <div class="contact_action request_action" style="display:none;">
        <span class="request_day"></span>
        <a class="btn contact_request_btn" data-action="accept">
            Confirm
        </a>
        <a class="btn contact_request_btn" data-action="decline">
            <i class="fas fa-times"></i>
        </a>
    </div>
    <div class="contact_action request_action1" style="display:none;">
            <span class="request_day1"></span>
            <a class="btn contact_send_btn" data-action="accept">
                Add to Contect
            </a>

        </div>
    <div class="contact_action suggest_action" style="display:none;">
        <a class="btn contact_add_btn" data-action="add">
            Add
        </a>
        <a class="btn contact_add_btn" data-action="block">
            <i class="fas fa-times"></i>
        </a>
    </div>
    <div class="contact_action suggest_invited_action" style="display:none;">
        <a class="btn">
            Invited
        </a>
        <a class="btn contact_cancel_btn" data-action="cancel">
            <i class="fas fa-times"></i>
        </a>
    </div>
    <div class="contact_action all_action all_contact_action" style="display:none;">
        <a class="btn dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item send_message" data-action="message">
                Send message
            </a>
            <a class="dropdown-item action_favorite mark" data-action="mark">
                Add to favourites
            </a>
            <a class="dropdown-item action_favorite unmark" style="display:none" data-action="unmark">
                Remove from favourites
            </a>
            <a class="dropdown-item action_block block" data-action="block">
                Block message
            </a>
            <a class="dropdown-item action_block unblock" style="display:none" data-action="unblock">
                Unblock message
            </a>
            <a class="dropdown-item action_remove_contact" data-action="remove">
                Remove as contact
            </a>
        </div>
    </div>
    <div class="contact_action all_action all_profile_action" style="display:none;">
        <a class="btn contact_view_btn" data-action="add" style="background-color: #15AFE8 !important;">
            View Profile
        </a>
    </div>
</li>
<li class="no_contact_template" style="display:none;">
    <h6>No users found</h6>
</li>
