<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap" rel="stylesheet">
<?php
	$show_load_btn = "";
	// if (count($posts_items)<12) $show_load_btn = " d-none";
?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<section class="mt-4">
    <div class="container">
		<div class="row">
	  		<div class="col-lg-12">

				<div class="group_topic_filter_container row"></div>
			</div>

		</div>
	</div>
</section>
<div class="col-4 subcategory-create-button-clone" style="display: none; ">
	<a class="board-link group-item-link"
 			<?php  if($member_info['role'] != 'superadmin'){ ?>
 				onclick="showAlert()"  <?php }else{ ?>
	 	   href="#"  data-toggle="modal" data-target="#exampleModal"
			 <?php 	 }  ?> >
			 <div>
 			<!--<div class="shadow-sm bg-topic" style="background:transparent url('<?php echo base_url();?>img/newtopic.png') no-repeat center center /cover">-->
			<img  class="topic-thumbnail w-100 "  src="<?php echo base_url();?>img/newtopic.png" alt="image"  />
		   </div>

	</a>
</div>

<div class="group-items-clone col-4" style="display: none;">
	<label class="makepinclass" style=" display:none; background: rgb(69 141 180 / 63%); padding: 5px; border-radius: 6px;   position: absolute; color: white;">
	<span class="unpinedclass" style="font-size: 14px;">
	<i class="makepinclassforthumb fas fa-thumbtack"><font style="color:white;text-transform: uppercase;
    font-family: monospace;
    font-size: 0.9rem;
    font-weight: 500;">Pin</font>
    	</i>

	</span>
	<span class="PinnedClass"  style="font-size: 14px;">
	<i style="display:none; color: rgb(247 182 35);" class="pinclassforthumb fas fa-thumbtack"><font style="color:white;text-transform: uppercase;
    font-family: monospace;
    font-size: 0.9rem;
    font-weight: 500;">Pinned</font></i>
	</span></label>
     <a class="board-link group-item-link" href="#">
		<div>
			<img src="<?php echo base_url();?>img/filler.png" class="topic-thumbnail w-100  "  >
			<img class="privacy-key" src="<?php echo base_url(); ?>assets/img/lock.png" alt="image" style="display:none;" />

		</div>
		<div class="row">
		<div class='col-sm-4 col-md-2 col-xl-2' >
		<a id="board_like" style='display: inline-flex;'> <span class="board_likespan"></span> <i class="fa fa-heart" style="color:#06cbd1;     margin-left: 3px; margin-top: 5px;"></i>
			<i id="showfavourite"  class="fa" style="color:#ffc107 ;        padding: 3px;
    font-size: 20px;"></i>  </a>
		</div>
		<div class="col-sm-8  col-md-8 col-xl-8">
		<p style="text-align: center;     white-space: nowrap;     overflow: hidden;" class="group-item-name"> </p>
		</div>

		</div>

	</a>
</div>
	<div class="col-sm-12 view_all" align="center" style="margin-top:20px; margin-bottom:40px; border-top:1px solid grey; padding:10px;">
		<a class="btn  buttonforMore" href="<?php echo base_url() ?>account/groupall/posts/topic/<?=$EncodedID?>" id="allpostpage">View All</a>
	</div>
<input type="hidden" name="Gr_ID" id="Gr_ID" value="<?=$EncodedID?>" />
<input type="hidden" name="GroupID" id="GroupID" value="<?=$EncodedID?>" />
<input type="hidden" name="inp_category_id" id="inp_category_id" value="<?=$catId?>" />
<input type="hidden" name="inp_subcategory_id" id="inp_subcategory_id" value="<?=$subcatId?>" />
<input type="hidden" name="group_id" id="group_id" value="" />
<input type="hidden" name="referrer_id" id="referrer_id" value="<?= $referrer?>" />

<!-- START OF NO ACCESS TO THE TOPIC MODAL -->
<div class="modal fade" id="topicAccessModal" tabindex="-1" aria-labelledby="topicAccessModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-90 m-auto">
      <div class="modal-body pt-2 pb-4">
		<div class="mt-3">
			<h6 class="modal-header-text text-center">This is a Private SUBGROUP.</h6>
			<div class="text-center viewb_all">
				<button class="btn btn-lg pt-1 col-12 col-lg-8" id="btn-request-topic-access">Request Access</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
<?php
	if (isset($this->session->userdata['logged_in']['bs_id'])) {
		$this->load->view('account/group_users_pop_up');
	}
?>
<!-- END OF NO ACCESS TO THE TOPIC MODAL -->
<style>
	ul#tabmenu li a.active {
		background: #D7F1F0!important;
		padding: 10px 33px;
		border-radius: 5px 5px 0px 0px;
	}
</style>
<!-- create topic or post Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content w-85 m-auto">
      <div class="modal-body pt-2 pb-4">
		  <img src="<?php echo base_url(); ?>img/create-post-topic.png" width="100%">

		  <ul class="nav nav-tabs d-flex justify-content-around  mt-2" id="tabmenu">
			  <li style="    padding: 7px;
    " class="active"><a data-toggle="tab" href="#home" class="active">Create Post</a></li>
			  <li style="    padding: 7px;
   "><a data-toggle="tab" href="#menu1">Create Subgroup</a></li>

		  </ul>

		  <div class="tab-content">

			  <div id="menu1" class="tab-pane fade  ">
 				  <div class="mt-3">
					  <h6 class="text-info  text-center">Why do I need to create a SubGroup?</h6>
					  <div class="mt-3">
						  Create SUBGROUP to keep multiple posts focused and relevant!
						  <br><br>
						  SUBGROUP is a group within the main group.
						  Create SUBGROUP for specific team, topic or special interest with multiple posts
						  categories.
						  <br><br>
						  You may set your SUBGROUP to private mode and may be
						  composed of only some select members.
						  Set it in public mode for everyone in your group to see
					  </div>
					  <div class="text-center my-3">
						  <button type="button" id="btn_create_topic" class="btn btn-info btn-lg pt-1 col-12 col-lg-8">Create SubGroup</button>
						  <br>
						  <button type="button"  class="btn_create_post_group btn btn-default btn-lg pt-1 col-12 col-lg-8 text-info my-2">
							  <label class="col-12" style="margin-bottom:0;">Create Post</label>
							  <small class="text-dark font-weight-bold" style="font-size: 65%;">If you do not need a subgroup</small>
						  </button>
					  </div>
					  <div class="mt-3">

					  </div>
				  </div>
			  </div>
			  <div id="home" class="tab-pane fade in show active ">
				  <div class="mt-3">
					  <h6 class="text-info  text-center">Do you know you can offer and reward vouchers in your posts?</h6>
					  <div class="mt-3" style="    padding: 0px 20px;">
						  <br>
						 <h6>Create Reward Voucher</h6>

						  Personalize and base your reward incentive on custom
						  attribute and events, with desired actions from
						  your participants.
						  <br><br>
						  <h6>Auto-Generate Voucher Codes</h6>

						  In your Reward Post, create a poll to pre-select recipients who meet
						  your criteria and reward with auto-generated
						  voucher codes
					  </div>
					  <div class="text-center my-3">
						  <span type="button" id="btn_create_topic" style="     font-size: 14px; width: 80%;  background: white;
    					color: #398cb3;    height: 47px; display: inline-flex;
  					  		margin-top: 8px;  padding: 4px 9px; align-items:center;justify-content:center;" onclick="showrewardpop()" class="btn btn-xl btn-primary mr-2">
				 		 <img style="    width: 20px;  margin: 5px;" src="<?php echo base_url().'assetsofpop/img/GiftIcon.png' ?>">
							  <span style="text-align: center; font-weight: bolder;
    font-size: 1rem;  ">Create Reward Post</span> </span>
						  <br>
						  <button type="button"   class="btn btn-info mt-3 btn_create_post_group" style="width: 80%" >
					 Create Post
 						  </button>
					  </div>
					  <div class="mt-3">

					  </div>
				  </div>
			  </div>

		  </div>

      </div>
    </div>
  </div>
</div>


<style>
	.roundimage {
		width: 50px;
		border-radius: 50%;
	}
	.headingtext{
		font-size: 15px;
		margin-top: 3px;
	}

	.graycolor{
		font-size: 12px;
		color: grey;
		margin-top: -8px;
	}
	.title-instant-reward {
		margin-top: 10px;
		font-size: 28px;
		font-weight: 600;
		margin-bottom: 0px;
		color: #6312a0;
	}
	.desc-instant-reward {
		font-size: 22px;
		font-weight: 400;
	}

	.title-form-instant-reward {
		font-size: 22px;
		font-weight: 500;
	}
	.instant-reward-bg {
		display: flex;
		flex-direction: column;
		justify-content: space-between;
	}

	@media screen and (max-width: 995px){
		.bgz {
			height: 300px!important;
		}
		.instant-reward-bg img{
			margin-top: 0px;
		}
	}

	.bgz {
		height: 100%;
		border-radius: 12px;
		color: white;
		padding: 40px;
		text-align: center;

		background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
	}

</style>




<div id="coinRewardModal" class="modal fade" role="dialog" style="overflow-x: hidden;
    overflow-y: auto;">
	<div class="modal-dialog modal-lg" >
		<button style="  margin-top: -16px;
    position: absolute;
    z-index: 9;
    right: 0;
    padding: 7px;
    background: #2b2f33;
    border-radius: 50%;
    color: white;
    right: -16px;
    font-size: 25px;
    padding: 5px 10px;" type="button" class="close" data-dismiss="modal">&times;</button>
		<!-- Modal content-->
		<div class="modal-content">
			<style>
				.bonus-coin-bg {
					display: flex;
					flex-direction: column;
					justify-content: end;
					background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-redeem-coin-modal.png);
				}
			</style>
			<div class="modal-body" style="padding: 1px!important;">
				<div class="row" style=" ">
					<div id="rewardpurcase" style="display: none;" class="col-xs-12 col-md-5">
						<div class="bonus-coin-bg bgz" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-credit-coin-modal.png); background-color: #D2F324; ">
							<p style="font-size: 26px; font-weight: 600;">INSTANTLY REWARD
								your members</p>
							<p style="font-size: 16px; font-weight: 400;">with our automated tracking /selection
								& coin wallet system.</p>
						</div>
					</div>
					<div id="instatnt" class="col-xs-12 col-md-5">
						<div class="instant-reward-bg bgz" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-modal-instant-reward.png); ">
							<div>
								<p style="font-size: 30px;

    font-weight: 600;">Instant Rewards Boosts Motivation</p>
								<img style="margin-top:20px;" src="<?php echo base_url(); ?>assetsofpop/img/img-modal-instant-reward.png" alt="modal instant reward">
							</div>
							<p style="margin: 0px; font-size: 24px;

    font-weight: 500;">Increase member's
								positive experience
								on every task!</p>

						</div>
					</div>
					<div class="col-xs-12 col-md-7" style="padding: 17px;" >
						<!-- exit button -->

						<div style="display : flex ; align-items : center ; flex-direction: column; ">
<br>
							<?php

							if(isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ ?>
								<img class="avatar roundimage" src="<?php echo $this->session->userdata['logged_in']['Us_Photo']; ?>">
							<?php }else{ ?>
								<i class="fas fa-user-circle"></i>
							<?php } ?>
							<h2 class="avatar-name-text headingtext" >

								<?php print_r($this->session->userdata['logged_in']['bs_name']); ?>
							</h2>
							<p class="avatar-job-text graycolor"><?php echo $this->session->userdata['logged_in']['bs_alias']; ?></p>

							<!-- content modal instant reward  -->
							<div id="modalInstantvoucher" class="modal-contents" >
								<h2 align="center" class="title-instant-reward">
									Create Rewards Voucher
								</h2>
								<p class="desc-instant-reward" style="color: #a5a5a5;" align="center">Boost Customer Traffic</p>
								<br>
								<!-- form -->
								<form  onsubmit="return showdetailsofvoucher();">
									<div style="width: 100%; margin-top: 10px;" class="from-input">
										<p class="label-inputx">Reward Voucher Offer Main Title</p>
                                          <div class="container-form-input" >
											  <input type="text" id="vouchertitle"  required class="form-control" placeholder="Ex. (Brand Name) P500 cashback ">

										  </div>
										<p style="    margin-top: 18px;" class="label-inputx">Reward Voucher Offer Sub Title</p>

										<div class="container-form-input" >

											<input type="text" id="vouchersubtitle"  required class="form-control" placeholder="Ex.  on your first purchase.">
							</div>


										<style>
											.label-inputx {
												font-size: 18px;
												font-weight: 500;
												color: #1f62ae;
											}
											.container-form-input {
												padding: 5px;
												background-color: white;
												box-shadow: 0 3px 10px rgb(0 0 0 / 20%);
												margin-right: 20px;
												border-radius: 5px;
												display: flex;
												flex-direction: row;
												align-items: center;
												margin-bottom: 10px;
											}
											.container-form-input .placeholder {
												margin-left: 30px;
												font-size: 18px;
												font-weight: 500;
												margin-bottom: 0px;
											}

											div#coinRewardModal p {
												margin-top:0px;
												margin-bottom: 0px;
											}
											@media screen and (max-width: 995px){
												.bgz {
													height: 500px;
												}
											}

										</style>
										<!-- form input -->
										<div>  <br>
											<p class="label-inputx">Number of Voucher Recipients</p>
											<div class="container-form-input">
												<div style="position: relative;">
													<input
															onchange="voucherperperson()"
															type="number"
															id="voucherpersion"
															onkeyup="checkvalue()"
															value="1"
															min="1"
															required
															class="form-control"
															style="width: 160px; padding-left: 40px;"
													>
												</div>
												<p class="placeholder"> Recipient/s</p>
											</div>
										</div>
										<div> <br>
											<p class="label-inputx">Value of Each Voucher (in PhP)</p>
											<div class="container-form-input">
												<div style="position: relative;">
													<input
															onchange="voucherperperson()"
															type="number"
															id="paisaammount"
															min="50"
															value="50"
															required
															class="form-control"
															style="width: 160px; padding-left: 40px;"
													>
													<p  style="position: absolute; top: 5px; left: 10px;  width: 25px;" >₱</p>
												</div>
												<p class="placeholder"> per voucher </p>
											</div>
										</div>

										<!-- form input -->
										<div> <br>
											<p class="label-inputx">Voucher Validity (End Date) </p>
											<div class="container-form-input">

												<input type="date" min="<?php echo date('Y-m-d', strtotime('+1 days')); ?>" id="date" required>
												<p class="placeholder">Enter date</p>
											</div>
<!--											<script>-->
<!---->
<!--												var today = new Date();-->
<!--												var dd = today.getDate();-->
<!--												var mm = today.getMonth() + 1; //January is 0!-->
<!--												var yyyy = today.getFullYear();-->
<!---->
<!--												if (dd < 10) {-->
<!--													dd = '0' + dd;-->
<!--												}-->
<!---->
<!--												if (mm < 10) {-->
<!--													mm = '0' + mm;-->
<!--												}-->
<!---->
<!--												today = yyyy + '-' + mm + '-' + dd;-->
<!--												document.getElementById("date").setAttribute("min", today);-->
<!--											</script>-->
										</div>

									</div> <br>

									<p class="desc-instant-reward" style="color: #FCB731; margin-top: 15px;
										   text-align: center;    text-align: center;">Participants will redeem each voucher with coins:</p>
									<div style="position: relative;">
										<div class="d-flex justify-content-center">

											<div class="input-group mb-3 d-flex justify-content-center">
												<div class="input-group-prepend">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style=" width: 40px;
    padding: 7px; " >
												</div>
												<input type="number" disabled	min="5"  value="5" id="totalredeempay"
													   style="width: 200px;
													 padding-left: 40px;
													  border: 1px solid #80808063;
    border-radius: 6px;
    padding: 0px 10px;"    >
											</div>


										</div>
									</div>
									<style>
										.btnx {
											background-color: #1f62ae !important;
											padding: 10px 25px;
											font-size: 12px !important;
											background-image: none !important;
											font-weight: 500 !important;
											border: none !important;
											font-size: 14px;
											font-weight: bold;
											border-radius: 10px;
											color: white;
										}

										.btn-primary-outlinex {
											/*border: 2px solid #1f62ae !important;*/
											background: white!important;
											color: #1f62ae!important;
										}
									</style>
									<div class="col-sm-12">
										<label class="label-inputx">Please indicate here your Redemption Terms & Conditions:</label>
										<textarea class="form-control" required id="decription"></textarea>
									</div>
									<div class="row" style="margin-top: 20px; justify-content: center; ">
										<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
											<button type="button"  class="btnx btn-lg" onclick="showdetailsofvoucherx()"  id="cointCreditsButton">Preview Voucher</button>
										</div>
										<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
											<button type="button" class="btnx btn-primary-outlinex btn-lg" onclick="showanotherpopup()" data-dismiss="modal">Cancel</button>
										</div>
									</div>  <br>
									<div class="d-flex justify-content-center"><button type="submit" align="center" class="btn btn-info">Create Reward Post </button></div>
                                 <br>
								</form>

							</div>
							<!-- content modal instant reward  -->


							<!-- content modal instant reward  -->

						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>

<div id="preview" class="modal fade" role="dialog" style="border: 1px solid grey;">
	<div class="modal-dialog">
		<!-- <h4>Preview:</h4> -->
		<!-- Modal content-->
		<div class="modal-content" style="height:fit-content;">
			<button style="position: absolute; right: 4px" type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-content">

				<div  class="mt-3">
					<div  class="d-flex justify-content-center">
					<img id="userimage" style="     border-radius: 50%; width: 60px;" src="<?php 
	 echo $this->session->userdata['logged_in']['Us_Photo']; 
	?>">
					</div>
					<div >
						<h6 align="center" id="username"><?php 
					print_r($this->session->userdata['logged_in']['bs_name']);
					 ?></h6>
 						<h6 align="center" id="usertype" style="font-weight: 300;"><?php  echo $this->session->userdata['logged_in']['bs_alias'];  ?></h6>
					</div>
				</div>
				<div>
					
				</div>
				<div class="p-2" style="     background-image: linear-gradient(to bottom right, #0542c3, #10b647);
						padding: 10px;
						margin: 10px;
						border-radius: 10px;">
					<div  >
						<h3 align="center" style="font-size: 1.5rem;
    color: white;
    margin-top: 9px;
    margin-bottom: 16px;"  id="vouchertitletext"></h3>
						<h4 align="center" style="font-size: 1.1rem;font-weight: 300;
    				text-transform: uppercase; color:white" id="vouchersubtitleshow">Put here Subtitle</h4>

						<div  class="d-flex justify-content-center" style="   background-image: url(http://localhost/boardspeak_2020/assetsofpop/Couponbackground.png);
						height: 170px;
						padding-top: 36px;
						background-size: 100% 162px;
						background-repeat: no-repeat;">
<!--							--><?php //  if($vou['Vr_Status'] == 'purchased' || $vou['Vr_Status'] == 'used'){ ?>
								<button id="buttonpurchaseused" class="btn btn-info" style=" display: grid;
								 border: 0px; background: transparent; color:black;">
									<font style=" font-size: 16px;text-transform: uppercase;
										font-weight: 900;
										color: grey;
										padding: 0px;
										margin: 0px;
									">Voucher Code <br style="margin:0;"> will be auto-generated</font> <font style="font-size:25px;" id="vouchervodenumber"></font><font style="    font-size: 12px;
										font-weight: 900;
										color: grey;
										padding: 0px;
										margin: 0px;" id="valueatt">
										</font> </button>
<!--							--><?php //}else{ ?>
								<!-- <button class="btn btn-info" style="background: transparent;
						color: black; border:0px;
						 " id="buttonnopuchase">Voucher</button> -->
<!--							--><?php //} ?>

						</div>
					<center> <h4 style="    font-size: 14px;
    color: white;" id="showvoucherworth"></h4> </center>
						<div class="rect-date">
							<p style="font-size:11px;font-weight:600;margin:0;">Offer Valid Till</p>
							<div style='display:flex;'>

								<div class="month rectt">
									<span class="lett month0">3</span>
									<span class="lett  month1">2</span>
								</div>
								<div class="date rectt">
									<span class="lett day0">1</span>
									<span class="lett day1">1</span>
								</div>
								<div class="year rectt">
									<span class="lett year0">2</span>
									<span class="lett year1">0</span>
									<span class="lett year2" >2</span>
									<span class="lett year3">2</span>


								</div>
								
<!--		 <div class="year rectt">-->
<!--            <span class="lett">2</span> -->
<!--            <span class="lett">0</span>-->
<!--            <span class="lett">2</span> -->
<!--            <span class="lett">2</span>-->
<!---->
<!--        </div>-->
<!--        <div class="month rectt">-->
<!--			-->
<!--            <span class="lett">2</span> -->
<!--            <span class="lett">2</span>-->
<!--        </div>-->
<!--        <div class="date rectt">-->
<!--            <span class="lett">1</span> -->
<!--            <span class="lett">1</span>-->
<!--        </div>-->
      
</div>

			</div>
			
</div>
		<style>.rect-date{
			flex-direction:column;
    background-color: #70B8A3;
    display: flex;
    align-items: center;
    justify-content: center;
    /* padding: 20px; */
	height: 105px;
    width: 269px;
    margin: auto;
	font-family: 'Poppins', sans-serif;
	border-radius:4px;
}
.rectt{
    display: flex;
    margin: 5px;
    font-size: 3rem;
    
}
.lett{
    background-color: white;
    margin: 1px;
    border-radius: 2px;
	width: 27px;
    text-align: center;
    /* padding: 4px; */
	height: 59px;
    display: flex;
    align-items: center;
    justify-content: center;
}</style>
</div>
<center> <h4 style="    font-size: 14px;
     color:rgb(9, 156, 138)" id="showvoucherRadeem"></h4> </center>
</div>
<!--<div  class="d-flex justify-content-center">-->
<!--     <input type="hidden" id="voucherid" >-->
<!--	 <button class="btn btn-primary" style="  font-size: 20px;  font-weight: 900;" onclick="purchasevoucher()" id="radeemvoucherbutton">Radeem the voucher</button>-->
<!--	<font id="expiredbutton" style='font-size: 20px; color: #c00909;-->
<!--    margin-top: 10px;-->
<!--    text-transform: uppercase;'>Expired</font>-->
<!--      </div>-->
				<div>
					<p class="discription"  id="showdescription"></p>
				</div>
			</div>
		</div>
	</div>

	<style>
		.discription{
			padding: 10px;
			min-height: 100px;
			max-height: 200px;
			overflow: hidden;
			overflow-y: scroll;
		}
		</style>

			<!-- <div class="col-sm-12"> -->
				<!-- <img style="    width: 100%;
    height: 220px;" class="avatar roundimage" src="<?php 
	// echo $this->session->userdata['logged_in']['Us_Photo']; 
	?>"> -->

			<!-- </div> -->
			<!-- <div align="center">
				<?php

				// if(
					// isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ 
						?>
					<img class="avatar roundimage" src="<?php 
					// echo $this->session->userdata['logged_in']['Us_Photo']; 
					?>">
				<?php 
				// }else{
					 ?>
					<i class="fas fa-user-circle"></i>
				<?php
			//  }
			  ?>
				<h2 class="avatar-name-text headingtext" >

					<?php 
					// print_r($this->session->userdata['logged_in']['bs_name']);
					 ?>
				</h2>
				<p class="avatar-job-text graycolor"><?php
				//  echo $this->session->userdata['logged_in']['bs_alias']; 
				 ?></p>

			</div>
			<div align="center" style="    padding: 30px;
						background: #4492c0;
						color: white;
						margin: 0px 39px;">
				<h4 id="vouchertitletext"></h4>
				<label style="font-size: 12px;">Valid Till</label>
				<div id="demo"> </div>

			</div>
			<div>
				<p id="showdescription" style="     padding: 10px 33px;
						min-height: 150px!important;
						overflow: scroll;
						overflow-x: hidden;
						height: 100px;"></p>
			</div> -->

		</div>

	</div>
</div>
<script>

	function showrewardpop(){

		let adminAccount =	'<?php if(isset($this->session->userdata['logged_in']['bs_admin'])){
			echo $this->session->userdata['logged_in']['bs_admin'];
		} ?>';
		if(adminAccount == 1){

			$('#coinRewardModal').modal('show');
		}else{
			$('#otherpop').modal('show');
		}
		// data-toggle="modal" data-target="#coinRewardModal"
	}
	   function round5(x)
	{
		return Math.ceil(x/5)*5;
	}

	function checkvalue(){
		if(	$('#paisaammount').val() < 50){
			$('#paisaammount').val(50);
		}
		$paisaAmmount = 	$('#paisaammount').val();
		$numberofvoucher = 	$('#voucherpersion').val();

		$totalvalue = 	parseInt($paisaAmmount) * parseInt($numberofvoucher);
		$totalvalue = $totalvalue * 0.20;
		$totalvalue =  $totalvalue / parseInt($numberofvoucher);

		$totalvalue = Math.round($totalvalue);
		if($totalvalue  < 5){
			$totalvalue = 5;
		}else if($totalvalue > 5 ){
			$totalvalue =  round5($totalvalue);
		}
		console.log($totalvalue);
		$('#totalredeempay').val($totalvalue);
	}
	 function voucherperperson ( ) {

		   if(	$('#paisaammount').val() < 50){
			   $('#paisaammount').val(50);
		   }
		$paisaAmmount = 	$('#paisaammount').val();
		$numberofvoucher = 	$('#voucherpersion').val();

		$totalvalue = 	parseInt($paisaAmmount) * parseInt($numberofvoucher);
		$totalvalue = $totalvalue * 0.20;
		$totalvalue =  $totalvalue / parseInt($numberofvoucher);

		$totalvalue = Math.round($totalvalue);
		if($totalvalue  < 5){
			$totalvalue = 5;
		}else if($totalvalue > 5 ){
			$totalvalue =  round5($totalvalue);
		}
		console.log($totalvalue);
		$('#totalredeempay').val($totalvalue);
	}
	function showanotherpopup(){
		$('#coinRewardModal').modal('show');
	}
	   function showdetailsofvoucherx(){

		// this  is the method for preview voucher ahsan
		   $('#vouchertitle').val();
		   $('#voucherpersion').val();
		   $('#date').val();
		   $('#paisaammount').val();
		   $('#totalredeempay').val();
		   $('#decription').val();
        	 

			// this is the information need to shown in preview popup
		   $("#demo").text($('#date').val());
		   $('#showdescription').text( $('#decription').val());
		   $('#vouchertitletext').text($('#vouchertitle').val())
			$('#vouchersubtitleshow').text($('#vouchersubtitle').val());
				$('#showvoucherworth').text( 'Voucher Value: ₱' + $('#paisaammount').val() );
				$('#showvoucherRadeem').text( 'Radeem For: ' + $('#totalredeempay').val() );

		   var first = $('#date').val();
 
		   $year =	first.split('-')[0];
		   $month =	first.split('-')[1];
		   $days =	first.split('-')[2];

		   $('.month0').text($month[0]);
		   $('.month1').text($month[1]);

		   $('.day0').text($days[0]);
		   $('.day1').text($days[1]);

		   $('.year0').text($year[0]);
		   $('.year1').text($year[1]);

		   $('.year2').text($year[2]);
		   $('.year3').text($year[3]);



		   $('#preview').modal('show');
		   return false;

	   }
	  function showdetailsofvoucher(){
		$('#vouchertitle').val();
		$('#voucherpersion').val();
		$('#date').val();
		$('#paisaammount').val();
		$('#totalredeempay').val();
		$('#decription').val();

		 let totalvoucher = {
			'Vp_title' :  $('#vouchertitle').val(),
			 'Vp_Sub_title' : $('#vouchersubtitle').val(),
			'Vp_vounchercount' :  $('#voucherpersion').val(),
			'Vp_deadline' :  $('#date').val(),
			'Vp_paisaammount' :  $('#paisaammount').val(),
			'Vp_radeemprice' :  $('#totalredeempay').val(),
			'Vp_decription' :  $('#decription').val(),
			 'Vp_post_id' :  '',
			 'Vp_Group_id' :   $("#Gr_ID").val(),
			 'Vp_Status': 'active',
		 }

		  $.ajax({
			  url: base_url + 'account/createvoucher',
			  method: 'POST',
			  dataType: 'json',
			  data:  totalvoucher,
			  success: function (response2) {


				  var location = base_url + 'account/create/post/';

				  location = location + $('#Gr_ID').val() + "";

				  if($('#inp_category_id').val() != ''){
					  location +=   "/" + $('#inp_category_id').val();


					  if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*'){

						  location += "/" +  $('#inp_subcategory_id').val() + "/" + response2;
 					  }

				  }else{
					  if ($('#inp_subcategory_id').val() != '' && $('#inp_subcategory_id').val() != '*'){

						  location += "//" +  $('#inp_subcategory_id').val() + "/" + response2;

					  }else{
						  location += "/test/test/" + response2;
						  }

				  }

				  window.location.href = location;
			  }
		  });


		  return false;
	}
function showAlert(){

	Swal.fire({
		title:'You have limited access',
		text: 'Only SuperAdmins can post. Your role in this group is a  <?php echo $member_info['role']; ?>.  ',
		icon: 'info',
		showCancelButton: false,
		confirmButtonColor: '#15AFE8',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Okay'
	}).then((result) =>
	{

	})
}
</script>
<style>
.view_all a {
    padding: 10px 20px;
    border: 2px solid #15AFE8;
    border-radius: 10px;
    text-decoration: none;

    transition: 0.3s;
    text-transform: capitalize;
    color:#15AFE8!important;

    margin: 5px;
    margin-bottom: 25px;
}
.btn {
    line-height: 34px !important;
    border-radius: 0.25rem !important;
}
.view_all a:hover {
    background-color: #15AFE8;
    color:white!important;

}
@media only screen and (max-width: 600px) {
  .topic-thumbnail{
	  height:150px;
   }
}
img.topic-thumbnail.w-100:hover {
	object-fit: scale-down;
}
</style>
<script>
	$(document).ready(function(){

		setTimeout(function (){
			$('#date').datetimepicker({
				uiLibrary: 'bootstrap'
			});
			// var $j = jQuery.noConflict();
			// $j("#date").datepicker(
			// 	{
			// 		format: 'mm/dd/yyyy'
			// 	}
			// );

		},2000)

	});
</script>
