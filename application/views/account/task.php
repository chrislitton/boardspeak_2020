
	<section class="user-menu-section" id="breadcrumb">
  	<div class="container">
			<div class="row">				
					<nav aria-label="breadcrumb">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/profile">Profile</a></li>
							<li class="breadcrumb-item active" aria-current="page"><?php echo $tasks_item['Ta_Subject']; ?></li>
						</ul>
					</nav>
			</div>			
	</div>
  </section>
    
    
  <!-- Portfolio Section -->
  <section class="portfolio" id="portfolio">
  	<div class="container">
   		<div class="row">		
				<div class="col-lg-2 mx-auto">  					
						<div class="board-cal text-center">                          	
							<h1><?php echo date( "d", strtotime($tasks_item['Ta_StartDate'])); ?></h1>
							<p><?php echo date( "F Y", strtotime($tasks_item['Ta_StartDate']) );?></p>
						</div>
				</div>
        <div class="col-lg-8">
							<h3><?php echo $tasks_item['Ta_Subject']; ?></h3>
							<p class="lead board-date">Posted On <?php echo date( "F d, Y h:i", strtotime($tasks_item['Ta_DatePosted']) ); ?> </p>
							<p class="board-detail">Status <?php echo $tasks_item['Ta_Status']; ?>  | <?php echo $tasks_item['Ta_Priority']; ?> Priority | <?php echo $tasks_item['Ta_Completed']; ?> % Completed |
								<a class="text-danger" href="#" data-href="<?php echo base_url(); ?>account/deletetask/<?php echo $tasks_item['Ta_ID'];?>" data-toggle="modal" data-target="#confirm-delete">Delete</a>
							</p>
							<p class="board-detail"><?php echo 'Date: '. date( "F d, Y h:i", strtotime($tasks_item['Ta_StartDate']) ).' to '.date( "F d, Y h:i", strtotime($tasks_item['Ta_DueDate']) ); ?></p>		
							
				</div>
				<div class="col-lg-2">  					
						<div class="task-img">                          	
							<img class="img-fluid" src="<?php echo $tasks_item['Ta_Photo']; ?>" alt="" onerror="this.src='<?php echo base_url(); ?>img/primary.png'">
						</div>
				</div>
			</div>	
	</div> 
  </section>

