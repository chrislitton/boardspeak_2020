<!-- main content start here -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap" rel="stylesheet">

<main id="main" class="main">
	<section class="not_content">
		<div class="not_content_container container-fluid">
			<div class="row">
				<div style="padding:0;" class="col-md-7 col-12 not_main_parrent">
					<div class="not_main">

						<h2 class="text-center pb-3">
							Notification
						</h2>

						<div class="not_tab_button text-center">
							<button id="setting_tab_btn" class="btn">
								Settings
							</button>
							<button id="summary_tab_btn" class="btn active">
								Summary
							</button>
						</div>

						<div id="not_summary" class="not_summary">

							<ul class="not_filter owl-theme owl-carousel">
								<li>
									<a class="not_filter_btn active" selected_tab_id="all_not" title="">
										all
										<?php if ($allcounter > 0) { ?>
											<font style="color:red; font-size:12px;">(<?php echo $allcounter; ?>)</font>
										<?php } ?>
									</a>
								</li>
								<li>
									<a class="not_filter_btn" selected_tab_id="activity_not" onclick="clickactivity()"
									   title="">
										activities
										<?php if ($count_for_activity > 0) { ?>
											<font id="acctivitycounter"
												  style="color:red; font-size:12px;">(<?php echo $count_for_activity ?>
												)</font>
										<?php } ?>

									</a>
								</li>
								<li>
									<a class="not_filter_btn" selected_tab_id="commentTab" title="">


										Comment <?php if ($count_for_Comment > 0 || $count_for_mention > 0) { ?>
											<font style="color:red; font-size:12px;">(<span
														id="commentTabcounter"><?php echo $count_for_Comment + $count_for_mention; ?></span>)</font>
										<?php } ?>

									</a>
								</li>

								<li>
									<a class="not_filter_btn" selected_tab_id="permission_not" title="">
										permission
										<?php if ($permission_counter > 0) { ?>
											<font style="color:red; font-size:12px;">(<?php
												echo $permission_counter;

												?>)</font>
										<?php } ?>
									</a>
								</li>
								<li>
									<a class="not_filter_btn" selected_tab_id="todos_not" title="">
										to do's <?php if ($to_do > 0) { ?>
											<font style="color:red; font-size:12px;">(<span
														id="todoCounter"><?php echo $to_do ?></span>)</font>
										<?php } ?>
									</a>
								</li>
								<li>
									<a class="not_filter_btn" selected_tab_id="reward_not" title="">
										Reward<?php if ($reward > 0) { ?>
											<font style="color:red; font-size:12px;">(<?php echo $reward; ?>
											)</font><?php } ?>
									</a>
								</li>

							</ul>

							<div id="activity_not" class="not_tab activity_not">
								<h4 class="activity_not_header">
									Activity Feed
								</h4>
								<?php foreach ($notifications_activity as $notify) { ?>
									<div class="activity_not_item">
										<div class="not_info">
											<div class="not_img_text">

												<a href="<?php echo base_url() . 'u/' . encode_id($notify['Us_ID']); ?>">
													<img src="<?php
													if (!empty($notify['Us_Thumb'])) {
														echo $notify['Us_Thumb'];
													} else {
														echo base_url() . "img/nophoto.png";
													}
													?>" alt="">
												</a>
												<div class="not_text">
													<a href="<?php echo base_url() . 'u/' . encode_id($notify['Us_ID']); ?>"
													   class="author_name">
														<?php
														if (!empty($notify['Us_Alias'])) {
															echo $notify['Us_Alias'];
														} else {
															echo $notify['Us_Name'];
														}

														?>
													</a>
													<a href="" class="not_caption">
														<?php echo $notify['No_Message'] ?>

													</a>
												</div>
											</div>

											<div class="text-center">
												<p class="not_time">
													<?php echo date("F d, Y h:i", strtotime($notify['No_DatePosted'])); ?>

												</p>

											</div>
										</div>
									</div>

								<?php } ?>


							</div>

							<div id="permission_not" class="not_tab permission_not">


								<h4 class="permission_header">
									Pending Permissions & Invites

								</h4>
								<div class="permission_container">


									<?php


									foreach ($notifications_items as $item): ?>
										<div class="permission_item">
											<div class="permission_img">
												<?php
												if (isset($item['userinviteID'])) {
													$id = encode_id($item['userinviteID']);
												} else {
													$id = encode_id($item['Us_ID']);
												}
												?>
												<a href="<?php echo base_url() . 'u/' . $id; ?>">
													<img src="<?php
													if (!empty($item['Us_Thumb'])) {
														echo $item['Us_Thumb'];
													} else {
														echo base_url() . "img/nophoto.png";

													}

													?>" alt="">
												</a>
											</div>

											<div class="permission_text">
												<h4>
														<span>
														 <?php
														 if (!empty($item['Us_Alias'])) {
															 echo $item['Us_Alias'];
														 } else {
															 echo $item['Us_Name'];
														 }

														 ?>
														</span>
													<?php echo $item['No_Message']; ?>
													</br>
													<?php
													//

													if ($item['No_Type'] != 'group_invite' && $item['No_Type'] != 'topic_join_via_link' && $item['No_Type'] != 'creator_role_request') { ?>
														<?php if ($item['No_Type'] == 'post_join_via_link' || $item['No_Type'] == 'post_join_via_be_a_member') { ?>
															<span style="color:blue; cursor:pointer"
																  onclick="showGiveAnswer('<?php echo $item['Us_Name'] ?>',<?php echo $item['No_From'] ?> , <?php echo $item['No_To'] ?>  , <?php echo $item['No_From_Type'] ?>)">Check Questionnaire</span>

														<?php } else { ?>
															<span style="color:blue; cursor:pointer"
																  onclick="showGiveAnswer('<?php echo $item['Us_Name'] ?>',<?php echo $item['No_From'] ?> , <?php echo $item['No_To'] ?>)">Check Questionnaire</span>

														<?php } ?>

													<?php } ?>
												</h4>
												<p>
													<?php echo date("F d, Y h:i", strtotime($item['No_DatePosted'])); ?>
												</p>
											</div>

											<div class="permission_control">
												<!--<p class="permission_badge">
													<?php echo ucfirst($item['No_Type']); ?>
													</p> -->


												<?php if ($item['No_Type'] == 'group_invite') { ?>
													<a class="group_action btn active" style="width:100px ; background-color: #15AFE8;
    color: #fff; margin-bottom:5px;"
													   data-notype="<?php echo $item['No_Type']; ?>"
													   data-notification="<?php echo $item['No_ID'] ?>"
															<?php if ($item['No_Type'] == 'post_join_via_link' ||
																	$item['No_Type'] == 'post_join_via_be_a_member') { ?>
																data-postinvite="<?php echo $item['No_From_Type'] ?>"
															<?php } ?>
													   data-action="accept" data-user="<?php echo $item['No_From'] ?>"
													   data-group="<?php echo $item['No_To'] ?>"
													   data-name="<?php echo $item['No_Title'] ?>">Accept</a>
													<a class="group_action  btn "
													   style="    background-color: #f5f5f5; width:100px; margin-bottom:5px;"
													   data-notype="<?php echo $item['No_Type']; ?>"
													   data-notification="<?php echo $item['No_ID'] ?>"
															<?php if ($item['No_Type'] == 'post_join_via_link' ||
																	$item['No_Type'] == 'post_join_via_be_a_member') { ?>
																data-postinvite="<?php echo $item['No_From_Type'] ?>"
															<?php } ?>
													   data-action="rejected" data-user="<?php echo $item['No_From'] ?>"
													   data-group="<?php echo $item['No_To'] ?>"
													   data-name="<?php echo $item['No_Title'] ?> ">Reject</a>

												<?php } else if ($item['No_Type'] == 'creator_role_request') { ?>
													<a class="group_action btn active" style="width:100px ; background-color: #15AFE8;
    color: #fff; margin-bottom:5px;" data-notype="<?php echo $item['No_Type']; ?>"
													   data-notification="<?php echo $item['No_ID'] ?>" <?php if ($item['No_Type'] == 'post_join_via_link' || $item['No_Type'] == 'post_join_via_be_a_member' || $item['No_Type'] == 'topic_join_via_link') { ?> data-postinvite="<?php echo $item['No_From_Type'] ?>"  <?php } ?>
													   data-action="approvecreator"
													   data-user="<?php echo $item['No_From'] ?>"
													   data-group="<?php echo $item['No_To'] ?>"
													   data-slug="<?php echo $item['Gr_Slug'] ?>"
													   data-username="<?php echo $item['Us_Name'] ?>"
													   data-name="<?php echo $item['Gr_Name'] ?>">Accept</a>
													<a class="group_action  btn "
													   style="    background-color: #f5f5f5; width:100px ; margin-bottom:5px;"
													   data-notype="<?php echo $item['No_Type']; ?>"
													   data-notification="<?php echo $item['No_ID'] ?>" <?php if ($item['No_Type'] == 'post_join_via_link' || $item['No_Type'] == 'post_join_via_be_a_member' || $item['No_Type'] == 'topic_join_via_link') { ?> data-postinvite="<?php echo $item['No_From_Type'] ?>"  <?php } ?>
													   data-action="rejectcreator"
													   data-user="<?php echo $item['No_From'] ?>"
													   data-group="<?php echo $item['No_To'] ?>"
													   data-slug="<?php echo $item['Gr_Slug'] ?>"
													   data-username="<?php echo $item['Us_Name'] ?>"
													   data-name="<?php echo $item['Gr_Name'] ?> ">Reject</a>

												<?php } else { ?>

													<a class="group_action btn active" style="width:100px ; background-color: #15AFE8;
    color: #fff; margin-bottom:5px;" data-notype="<?php echo $item['No_Type']; ?>"
													   data-notification="<?php echo $item['No_ID'] ?>" <?php if ($item['No_Type'] == 'post_join_via_link' || $item['No_Type'] == 'post_join_via_be_a_member' || $item['No_Type'] == 'topic_join_via_link') { ?> data-postinvite="<?php echo $item['No_From_Type'] ?>"  <?php } ?>
													   data-action="approve" data-user="<?php echo $item['No_From'] ?>"
													   data-group="<?php echo $item['No_To'] ?>"
													   data-name="<?php echo $item['Us_Name'] ?>">Approve</a>
													<a class="group_action  btn  "
													   style="     background-color: #f5f5f5; width:100px ; margin-bottom:5px;"
													   data-notype="<?php echo $item['No_Type']; ?>"
													   data-notification="<?php echo $item['No_ID'] ?>" <?php if ($item['No_Type'] == 'post_join_via_link' || $item['No_Type'] == 'post_join_via_be_a_member' || $item['No_Type'] == 'topic_join_via_link') { ?> data-postinvite="<?php echo $item['No_From_Type'] ?>"  <?php } ?>
													   data-action="reject" data-user="<?php echo $item['No_From'] ?>"
													   data-group="<?php echo $item['No_To'] ?>"
													   data-name="<?php echo $item['Us_Name'] ?> ">Reject</a>

												<?php } ?>

												<!-- </div>-->
											</div>
										</div>
									<?php endforeach; ?>


								</div>

							</div>

							<div id="todos_not" class="not_tab todos_not">

								<header id="header">
									<div id="stuck_container">
										<div class="control-group input-group col pr-0">
											<input class="form-control" id="filter" style="    width: 50%;"
												   name="txtSearch" value="" type="text"
												   placeholder="Search in Todo List">
											<div class="input-group-append">
												<button class="btn btn-primary" style="padding:0;" id="btn_search_text"
														style="border-top-left-radius: 0 !important; border-bottom-left-radius: 0 !important;">
													<i class="fa fa-search py-1 px-2 text-white"></i></button>
											</div>

										</div>

									</div>
								</header>
								<ul>
									<h4 class="todos_header">
										To Do List


									</h4>
									<div id="myDIV">
										<?php foreach ($comment_todo_notification_owen as $todox) { ?>
											<li id="todoComment<?php echo $todox['No_ID'] ?>">
												<div class="col-sm-10">

													You Assigned Task
													</br>

													<h4 class="todos_name">

														<?php if ($todox['No_Status'] == 'done') { ?>
															<i class="fa fa-check-square showIcon"
															   id="showicon<?php echo $todox['No_ID'] ?>"
															   aria-hidden="true" style="color:green"></i>
														<?php } else { ?>


															<i class="fas fa-cog"
															   style="color: <?php if ($todox['No_Status'] == 'inprogress') {

																   echo 'yellow';

															   } ?>" id="showicon<?php echo $todox['No_ID'] ?>"></i>
														<?php } ?>

														<span>
													<?php if ($todox['No_Status'] == 'done') { ?>
														<label style="background: green; color: white; padding: 5px; border-radius: 10px;">Task is Done</label>

													<?php } else if ($todox['No_Status'] == 'inprogress') { ?>
														<label style="background: yellow; color: black; padding: 5px; border-radius: 10px;">Task is Inprogress</label>
													<?php } ?>
											You assigned <a href="<?php echo base_url() ?>u/<?php echo encode_id($todox['Us_ID']); ?>"><?php echo $todox['Us_Name'] ?></a> a task:

 															<?php $string = preg_replace('#\d+#', '', $todox['Co_Message']);
															// 										$string = preg_replace('/^([0-9]* \w+ )?(.*)$/', '$2', $todo['Co_Message']);
															$string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);

															echo $string;

															?>  on <a
																	href="<?php echo base_url() ?>account/view/post/<?php echo encode_id($todox['Co_Po_ID']); ?>"><?php echo $todox['Po_Title'] ?> Post</a>
											</span>


													</h4>
													<span style="display:none;"
														  id="commentxx<?php echo $todox['No_ID'] ?>"> <?php echo $todox['No_To_Type'] ?> </span>
												</div>

												<div class="col-sm-2">

													<div class="dropdown">
														<button type="button" class="btn btn-primary dropdown-toggle"
																data-toggle="dropdown">
															Action
														</button>
														<div class="dropdown-menu">
															<a class="dropdown-item" href="#"
															   onclick="addnoteComment( <?php echo $todox['No_ID'] ?>  )">
																<?php if ($todox['No_To_Type'] == 'user') { ?>
																	Add Note
																<?php } else { ?>
																	Notes
																<?php } ?>
															</a>
															<a class="dropdown-item" href="#"
															   onclick="removeComment(<?php echo $todox['No_ID'] ?> )"
															   id="deletebutton<?php echo $todox['No_ID'] ?>"
															   data-commenttype='todo'>Delete</a>
															<!--															<a class="dropdown-item" href="#" data-type="-->
															<?php //if($todox['No_Status'] == 'done'){ ?><!--unread-->
															<?php //}else{ ?><!--done-->
															<?php //} ?><!--" data-commenttype='todo'-->
															<!--															   id="donebutton-->
															<?php //echo $todox['No_ID'] ?><!--"-->
															<!--															   onclick="doneComment(-->
															<!--//															   -->
															<!--//															     )">-->
															<!--//																-->
															<?php ////if($todox['No_Status'] == 'done')
															//																{ ?><!--Un Done--><?php //}else{ ?>
															<!--																	Done	  --><?php //} ?>
															<!--															</a>-->

														</div>
													</div>

												</div>


											</li>
										<?php } ?>
										<?php foreach ($comment_todo_notification as $todo) { ?>
											<li id="todoComment<?php echo $todo['No_To'] ?>">
												<div class="col-sm-10">
													<?php if ($todo['No_From_Type'] != 'user') { ?>
														<?php if ($todo['No_Type'] == 'todo_reminder_post_notification') { ?>
															<h6>Reminder</h6>
														<?php } else { ?>

															<h6>Assigned </h6>

														<?php } ?>
													<?php } ?>

													<h4 class="todos_name">

														<?php if ($todo['No_Status'] == 'done') { ?>
															<i class="fa fa-check-square showIcon"
															   id="showicon<?php echo $todo['No_ID'] ?>"
															   aria-hidden="true" style="color:green"></i>
														<?php } else { ?>
															<i class="fas fa-cog"
															   id="showicon<?php echo $todo['No_ID'] ?>"></i>
														<?php } ?>

														<span>
<?php
//												if($todo['No_Type'] != 'todo_post_notification_own'){ ?>

															<?php if ($todo['No_Status'] == 'inprogress') { ?>
																<label style="background: yellow;
    padding: 7px;
    border-radius: 10px;" id="taskinporogess<?php echo $todo['No_ID'] ?>"
																	   style="background:orange;">Task In progress </label>
															<?php } else { ?>
																<label style="background: yellow;

    border-radius: 10px;" id="taskinporogess<?php echo $todo['No_ID'] ?>"></label>
															<?php } ?>
											<a href="<?php echo base_url() ?>u/<?php echo encode_id($todo['Us_ID']); ?>"><?php
												if (!empty($todo['Us_Alias'])) {
													echo $todo['Us_Alias'];
												} else {
													echo $todo['Us_Name'];
												}
												?></a>

															<!--										--><?php //}	?>
<!--												--><?php
															//												if($todo['No_Type'] == 'todo_post_notification_own'){ ?>
<!--													You assigned NAME a task: “comment” on POST link,-->
															<!--													You assigned-->
															<!--													<a href="-->
															<?php //echo base_url() ?><!--u/-->
															<?php //echo encode_id($todo['Us_ID']); ?><!--">-->
															<?php //echo $todo['Us_Name'] ?><!--</a> a Task-->
															<!---->
															<!--												--><?php //}else{ ?>
															<?php if ($todo['No_From_Type'] != 'user') { ?>
																Assigned You a Task:
															<?php } else { ?>
																Commented :
															<?php } ?>

<!--											--><?php //} ?>



															<?php

															$string = preg_replace('#\d+#', '', $todo['Co_Message']);
															// 										$string = preg_replace('/^([0-9]* \w+ )?(.*)$/', '$2', $todo['Co_Message']);
															$string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);

															echo $string;

															?>  on <a
																	href="<?php echo base_url() ?>account/view/post/<?php echo encode_id($todo['Co_Po_ID']); ?>"><?php echo $todo['Po_Title'] ?> Post</a>
											</span>


													</h4>
													<span style="display:none;"
														  id="commentxx<?php echo $todo['No_ID'] ?>"> <?php echo $todo['No_To_Type'] ?> </span>
												</div>

												<div class="col-sm-2">

													<div class="dropdown">
														<button type="button" class="btn btn-primary dropdown-toggle"
																data-toggle="dropdown">
															Action
														</button>
														<div class="dropdown-menu">
															<a class="dropdown-item" href="#"
															   onclick="addnoteComment( <?php echo $todo['No_ID'] ?>  )">
																<?php if ($todo['No_To_Type'] == 'user') { ?>
																	Add Note
																<?php } else { ?>
																	Notes
																<?php } ?>
															</a>
															<?php if ($todo['No_Type'] != 'todo_reminder_post_notification') { ?>
																<a class="dropdown-item" href="#"
																   onclick="removeComment(<?php echo $todo['No_ID'] ?> )"
																   id="deletebutton<?php echo $todo['No_ID'] ?>"
																   data-commenttype='todo'>Delete</a>
																<a class="dropdown-item" href="#"
																   data-type="inprogress"
																   data-commenttype='todo'
																   data-commentidnoti='<?php echo $todo['No_To'] ?>'
																   id="progress<?php echo $todo['No_ID'] ?>"
																   onclick="progressComment(<?php echo $todo['No_ID'] ?>  )">
																	In Progress
																</a>
															<?php } ?>
															<a class="dropdown-item" href="#"
															   data-type="<?php if ($todo['No_Status'] == 'done') { ?>unread<?php } else { ?>done<?php } ?>"
															   data-commenttype='todo'
															   data-commentidnoti='<?php echo $todo['No_To'] ?>'
															   id="donebutton<?php echo $todo['No_ID'] ?>"
															   onclick="doneComment(<?php echo $todo['No_ID'] ?>  )">
																<?php if ($todo['No_Status'] == 'done') { ?>Un Done<?php } else { ?>     Done
																<?php } ?></a>

														</div>
													</div>

												</div>


											</li>
										<?php } ?>
									</div>
								</ul>
							</div>


							<div  id="reward_not" style="padding-right:0;" class="not_tab reward_not todos_not tab-content"  >
								<center>
									<ul id="tabbutton" class="nav nav-tabs mb-4" style="    width: 100%;display: flex;
    justify-content: center; border: 0px;">
										<li style="font-size:1.1rem; display: flex;
    align-items: center;" class="active "><a  class="active " style=" margin-right: 20px;" data-toggle="tab" href="#cointab">Coins</a></li>
										<li style="font-size:1.1rem; display: flex;
    align-items: center;"><a data-toggle="tab" href="#vouchertab">Vouchers</a></li>

									</ul>
								</center>
							
								<p style="    TEXT-ALIGN: center; font-size: 1.2rem; font-weight: 600;  color:#2bb158;">
									Available Balance</p>
								<div class="available-balance container-reward"
									 style="width:130px; margin:auto;
								 justify-content:space-between;
								  position:relative;">
									<p id="balance-avail-para" style="    font-weight: 900;   color:#2bb158; margin-left: -12px;   margin-bottom: 0;"><?php echo $usercoins; ?></p>
									<img id='balance-avail-img'  src="https://boardspeak.com/assetsofpop/img/icon-coin-no-border.svg" style=""
										 alt="">
								</div>
								<div id="cointab" class="tab-pane show  in active">
									<div style="margin-top:14px;" >

										<ul style="max-height: 60px;display: flex; align-items: center;    border-bottom: 1px solid #3bb0e9;"
											class="nav nav-tabs">
											<li style="border:none;" class="active"><a class="active" data-toggle="tab" href="#menu">ALL</a></li>
											<li style="border:none;"><a data-toggle="tab" href="#menu1">RECIEVED</a></li>
											<li style="border:none;"><a data-toggle="tab" href="#menu2">SENT</a></li>
										</ul>

										<div class=" inputtt control-group input-group col">
											<input class="form-control" id="filter" style="    width: 50%;" name="txtSearch"
												   value="" type="text" placeholder="Search in Rewards">
											<div class="input-group-append">
												<button class="btn btn-primary" style="padding:0;" id="btn_search_text"><i
															class="fa fa-search py-1 px-2 text-white"></i></button>
											</div>
										</div>

										<div class="tab-content">
											<div id="menu" class="tab-pane fade in active show">
												<ul style="list-style: none;">

													<?php foreach ($ListofRewardsend as $ind => $listreward) { ?>

														<li style="display:flex;justify-content:center;padding:0;">
															<div class="row col-sm-12" style="margin-top:10px;padding:0;">
																<div style="flex-wrap: nowrap;justify-content: space-between;" class="col-sm-12 headerofdate">
																	<p style="font-family: inherit;font-weight: 500; margin:0;"><?php echo $listreward['Rs_created'] ?> </p>
																</div>
																<div class="col-sm-12" style="padding:0;">
																	<div class=""
																		 style='display:flex;justify-content:space-between;'>
																		<div style="    display: flex;flex-direction: column;justify-content: end;">
																			<p style="color:#388cb3; font-family: inherit;margin-bottom:0;font-size: 18px;"
																			   class="reward_coins_head">REWARD COINS</p>
																			<p style="margin-top:6px;">You
																				Send <?php echo $listreward['Rs_ammount'] ?>
																				to <?php echo $listreward['Rs_usercount']; ?>
																				Users </p>
																		</div>

																		<div style='align-items: end; display:flex; flex-direction:column; padding-top:3px;white-space: nowrap;'>
																			<div style="display:flex;">

																				<img style="width: 33px;height:33px; "
																					 src="<?php echo base_url() ?>/img/bcoin.png"
																				>
																				<p style="font-family: inherit;color:#388cb3;font-weight: 900;font-size:18px;">
																					- <?php echo $listreward['Rs_ammount'] ?></p>
																			</div>

																			<div class="refer-div">

																				<button style="margin-top:4px; color: #087ab7;"
																						type="button"
																						onclick="getsendrewardDetail(<?php echo $listreward['Rs_ID']; ?> , <?php echo $listreward['Rs_userinsertID'] ?>)"
																						class="btn btn-primary">
																					Ref:
																					# <?php echo $listreward['RS_Transection_ID']; ?>
																				</button>
																				<div class="dropdown-menu">

																					<!-- <a class="dropdown-item">Use Credit</a>
																					<a class="dropdown-item">A2</a> -->
																				</div>


																			</div>
																		</div>
																	</div>
																</div>
														</li>
													<?php } ?>
													<li style="display:flex;justify-content:center;padding:0;">
														<div class="row col-sm-12" style="margin-top:10px;padding:0;">
															<div style="flex-wrap: nowrap;justify-content: space-between;" class="col-sm-12 headerofdate">
																<p style="font-family: inherit; font-weight: 500; margin:0;">August 14, 2022 &nbsp;&nbsp;&nbsp; 19:45:32</p>
															</div>
															<div class="col-sm-12" style="padding:0;">
																<div class=""
																	 style='display:flex;justify-content:space-between;'>
																	<div style="    display: flex;flex-direction: column;justify-content: end;">
																		<p style="color:#388cb3; font-family: inherit;margin-bottom:0;font-size: 18px;"
																		   class="reward_coins_head">Bonus COINS</p>
																		<p style="margin-top:6px;">You Recieved For last <?php
																			$now = date('Y-m-d H:i:s');
																			$date1 = $this->session->userdata['logged_in']['Us_DateTime'];
																			$date2 = $now;
																			$diff = abs(strtotime($date2) - strtotime($date1));

																			$years = floor($diff / (365 * 60 * 60 * 24));
																			$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
																			$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

																			if ($years > 0) {
																				echo $years . ' Year';
																			} else if ($months > 0) {
																				echo $months . ' Month';
																			} else {
																				echo $days . ' Days';
																			}
																			?></p>
																	</div>
																	<div style='align-items: end; display:flex; flex-direction:column; padding-top:3px;white-space: nowrap;'>
																		<div style="display:flex;">

																			<img style="width: 33px;height:33px; "
																				 src="<?php echo base_url() ?>/img/bcoin.png"
																			>
																			<p style="font-family: inherit;color:#388cb3;font-weight: 900;font-size:18px;">    <?php

																				$totalvale = 0;
																				foreach ($coinrewarding as $stinrx => $val) {
																					$totalvale = $totalvale + $val['Cg_coins'];

																				}
																				?>
																				+
																				<?php foreach ($totalrecievedreward as $rewardsx => $redx) {

																					$totalvale = $totalvale + $redx['RC_Ammount'];
																				} ?>

																				<?php echo $totalvale; ?></p>
																		</div>
																		<div class="refer-div">

																			<button style=" margin-top:4px;color: #087ab7;"
																					type="button" class=""
																					data-toggle="dropdown">
																				Detail
																			</button>
																			<div class="dropdown-menu">

																				<a class="dropdown-item">Use Credit</a>
																				<a class="dropdown-item">A2</a>
																			</div>
																		</div>
																		<!-- <button type='button' >Ref. # 5129846234895</button> -->

																	</div>
																</div>
															</div>
														</div>
													</li>


													<?php foreach ($transeectionhistoryadmin as $treansectionad => $trad) { ?>

														<li style="display:flex;justify-content:center;padding:0;">
															<div class="row col-sm-12" style="margin-top:10px;padding:0;">
																<div style="flex-wrap: nowrap;justify-content: space-between;" class="col-sm-12 headerofdate">
																	<p style="font-family: inherit; font-weight: 500; margin:0;"><?php echo $trad['CTA_recieved_Date']; ?></p>
																</div>
																<div class="col-sm-12" style="padding:0;">
																	<div class=""
																		 style='display:flex;justify-content:space-between;'>
																		<div style="    display: flex;flex-direction: column;justify-content: end;">
																			<p style="color:#388cb3; font-family: inherit;margin-bottom:0;font-size: 18px;"
																			   class="reward_coins_head">REWARD COINS</p>
																			<p style="margin-top:6px;">You
																				received <?php echo $trad['CTA_Ammount'] ?>coins
																				from Admin </p>
																		</div>

																		<div style='align-items: end; display:flex; flex-direction:column; padding-top:3px;white-space: nowrap;'>
																			<div style="display:flex;">

																				<img style="width: 33px;height:33px; "
																					 src="<?php echo base_url() ?>/img/bcoin.png"
																				>
																				<p style="font-family: inherit;color:#388cb3;font-weight: 900;font-size:18px;">
																					+ <?php echo $trad['CTA_Ammount'] ?></p>
																			</div>
																			<!-- <label>Transection ID: <?php ?></label> -->
																			<div class="refer-div">

																				<button style="margin-top:4px;color: #087ab7;"
																						type="button" class=""
																						data-toggle="dropdown">
																					Ref.
																					# <?php echo $trad['CTA_Transection_ID'] ?>
																				</button>
																				<div class="dropdown-menu">

																					<a class="dropdown-item">Use Credit</a>
																					<a class="dropdown-item">A2</a>
																				</div>


																			</div>
																		</div>
																	</div>
																</div>
														</li>
													<?php } ?>

													<?php foreach ($transeectionhistory as $treansection => $tra) { ?>

														<li style="display:flex;justify-content:center;padding:0;">
															<div class="row col-sm-12" style="margin-top:10px;padding:0;">
																<div style="flex-wrap: nowrap;
justify-content: space-between;" class="col-sm-12 headerofdate">
																	<p style="font-family: inherit;
		font-weight: 500; margin:0;"> <?php echo $tra['CT_Purchase_Date']; ?></p>
																</div>
																<div class="col-sm-12" style="padding:0;">
																	<div class=""
																		 style='display:flex;justify-content:space-between;'>
																		<div style="    display: flex;flex-direction: column;justify-content: end;">
																			<p style="color:#388cb3; font-family: inherit;margin-bottom:0;font-size: 18px;"
																			   class="reward_coins_head">PURCHASED COINS</p>
																			<p style="margin-top:6px;">You
																				bought <?php echo $tra['CP_Ammount'] ?>coins</p>
																		</div>

																		<div style='align-items: end; display:flex; flex-direction:column; padding-top:3px;white-space: nowrap;'>
																			<div style="display:flex;">

																				<img style="width: 33px;height:33px; "
																					 src="<?php echo base_url() ?>/img/bcoin.png">
																				<p style="font-family: inherit;color:#388cb3;font-weight: 900;font-size:18px;">
																					+ <?php echo $tra['CP_Ammount'] ?></p>
																			</div>

																			<div class="refer-div">
																				<button style="margin-top:4px;color: #087ab7;"
																						type="button" class=""
																						data-toggle="dropdown">
																					Ref.
																					# <?php echo $tra['CT_Transection_ID'] ?>
																				</button>
																				<div class="dropdown-menu">

																					<a class="dropdown-item">Use Credit</a>
																					<a class="dropdown-item">A2</a>
																				</div>


																			</div>
																		</div>
																	</div>
																</div>
														</li>

													<?php } ?>
													<?php


													foreach ($totalrecievedreward as $rewards => $red) {

//														print_r($red)
														;?>
														<li style="display:flex;justify-content:center;padding:0;">
															<div class="row col-sm-12" style="margin-top:10px;padding:0;">
																<div style="flex-wrap: nowrap;
justify-content: space-between;" class="col-sm-12 headerofdate">
																	<p style="font-family: inherit;
		font-weight: 500; margin:0;"> <?php echo $red['RC_Date']; ?></p>
																</div>
																<div class="col-sm-12" style="padding:0;">
																	<div class=""
																		 style='display:flex;justify-content:space-between;'>
																		<div style="    display: flex;flex-direction: column;justify-content: end;">
																			<p style="color:#388cb3; font-family: inherit;margin-bottom:0;font-size: 18px;"
																			   class="reward_coins_head">EARNED COINS</p>
																			<p style="margin-top:6px;">You gave
																				earned <?php echo $red['RC_Ammount'] ?>coins</p>
																			<p>
																				<a href="<?php echo base_url() . "u/" . encode_id($red['Us_ID']) ?>"> <?php echo $red['Us_Name'] ?></a>
																				rewarded you for completeing task</p>
																		</div>

																		<div style='align-items: end; display:flex; flex-direction:column; padding-top:3px;white-space: nowrap;'>
																			<div style="display:flex;">

																				<img style="width: 33px;height:33px; "
																					 src="<?php echo base_url() ?>/img/bcoin.png">
																				<p style="font-family: inherit;color:#388cb3;font-weight: 900;font-size:18px;">
																					+ <?php echo $red['RC_Ammount'] ?></p>
																			</div>

																			<div class="refer-div">
																				<button style="margin-top:4px; color: #087ab7;"
																						type="button" class=""
																						data-toggle="dropdown">
																					Ref. #<?php echo $red['RC_transection']; ?>
																				</button>
																				<div class="dropdown-menu">

																					<a class="dropdown-item">Use Credit</a>
																					<a class="dropdown-item">A2</a>
																				</div>


																			</div>
																		</div>
																	</div>
																</div>
														</li>
													<?php } ?>
													<li>
														<img style="width: 100%; height: 80px;"
															 src="<?php echo base_url() ?>img/banner/FREEBIe.png">
													</li>
												</ul>

											</div>
											<div id="menu1" class="tab-pane fade">


											</div>
											<div id="menu2" class="tab-pane fade">


											</div>

										</div>

									</div>

								</div>
								<div id="vouchertab" class="tab-pane fade">
									<div style="margin-top:14px;padding:0;" id="reward_not" class="not_tab reward_not todos_not">

										<ul style="max-height: 60px;display: flex; align-items: center; border-bottom: 1px solid #3bb0e9;"
											class="nav nav-tabs">
											<li style="border:none;" class="active"><a class="active" data-toggle="tab" href="#menuvoucher">ALL</a></li>
											<li style="border:none;"><a data-toggle="tab" href="#menu1voucher">RECIEVED</a></li>
											<li style="border:none;"><a data-toggle="tab" href="#menu2voucher">SENT</a></li>
										</ul>

										<div class=" inputtt control-group input-group col">
											<input class="form-control" id="filter" style="    width: 50%;" name="txtSearch"
												   value="" type="text" placeholder="Search in Rewards">
											<div class="input-group-append">
												<button class="btn btn-primary" style="padding:0;" id="btn_search_text"><i
												 class="fa fa-search py-1 px-2 text-white"></i></button>
											</div>
										</div>

										<script>
											function allvoucherCode(val){

												getvoucherCode(val);

											}
										</script>
										<div class="tab-content">
											<div id="menuvoucher" class="tab-pane fade in active show">

												<div class="card card-body " style="padding-right:0;    border: 0px;">
													<?php foreach ($All_user_voucher as $voucher => $vouall) { ?>
														<div class="col-sm-12 d-flex justify-content-between row" style="padding: 10px;
													box-shadow: 1px 0px 7px 3px #80808033;

													border-radius: 10px;
													margin-bottom: 10px;
													margin-top: 10px;
													align-items:center;
													padding-right: 22px;">
															<div  class="col-sm-3 col-md-3 col-lg-3 d-flex justify-content-center" >
							<span  >
																<?php
																if(empty($vouall['Po_Thumb'])){ ?>
																	<img style=" min-height: 115px; width: 100%;   border-radius: 6px;" src="<?php echo base_url().'img/banner/create-topic.jpg' ?>">
																<?php }else{ ?>
																	<img   style="  min-height: 115px;  width: 100%;       border-radius: 6px;"
																		 src="<?php echo base_url() ?><?php echo $vouall['Po_Thumb'] ?>"
																		 alt="icon-earned">
																<?php	}
																?>
</span>

															</div>

															<div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-center">

																<div>
																	<?php if( !isset($vouall['Us_Email'])){ ?>
																		<p align="center" class="earn-coin-card-title mb-0"><strong
																					style="color: green; text-transform: uppercase;">You are Rewarded!</strong></p>

																	<?php }else{ ?>
																		<p align="center" class="earn-coin-card-title mb-0"><strong
																	    style="color: green; text-transform: uppercase;">You Send Voucher To <?php echo $vouall['Us_Name']?>!</strong></p>

																	<?php }?>
																		<hr>
																	<p align="center" class="earn-coin-card-title mb-0">
																		<strong> <?php echo $vouall['Vr_title'] ?> </strong></p>
																	<p align="center" class="earn-coin-card-desc mb-0">Valid
																		Till : <?php echo $vouall['Vr_deadline'] ?> </p>
																	<p style="    text-transform: uppercase; color: #0b9eb6; font-weight: 800;" align="center">Redeem for <img style="width: 24px;" src="<?php echo base_url().'/img/bcoin.png' ?>"> <?php echo $vouall['Vr_radeemprice'] ?>

																	</p>
																	<?php if( isset($vouall['Us_Email'])){ ?>
																	<p style=" text-transform: uppercase; color: #0b9eb6; font-weight: 800;" align="center">Voucher Price <?php echo $vouall['Vr_radeemprice'] ?> <img style="height: 20px;"
																	</p>
																	<?php } ?>
																</div>
															</div>
															<div class="col-sm-3 col-md-3 col-lg-3 d-flex justify-content-center">

																<?php if(  !isset($vouall['Us_Email'])){ ?>

																<button onclick="getvoucherCode(<?php echo $vouall['Vr_Id'] ?>)"
																		style="min-width: 148px;"	class="btn btn-outline-primary getVoucherBtn">Get Voucher Code
																</button>
																<?php }else{ ?>
																		<div class="col-sm-12">
 																			 <img src="<?php if(!empty( $vouall['Us_Thumb'])){
																			  echo  $vouall['Us_Thumb'];
																		  }else{
																			  echo base_url().'img/home/profileimage.jpg';
																		  }?>"  style="width: 60px;
																					border-radius: 50%;
																					height: 60px;
																				}" >
																			<h6><font style="color:grey">Voucher Code: </font><?php echo $vouall['Vr_Voucher_Token']; ?></h6>
																			<h6><font style="color:grey">Status: </font> <?php
																			$voucherDate =   date($vouall['Vr_deadline']);
																			$currentdate =   date('Y-m-d');

																			 if($currentdate > $voucherDate  ){
																				 echo '</br> Expire';
																			 }else{
																				 echo '</br>'. $vouall['Vr_Status'];
																			 } ?></h6>
																					</div>
																			<?php  } ?>
																<style>
																
																@media (min-width: 749px) and (max-width: 900px){
																.earn-coin-card-title {max-width:150px}
																}
																
																.getVoucherBtn{max-height: 48px;
																		min-width: 112px;
																		font-weight: 700;
																		white-space:nowrap;
																		padding-left: 7px;}
																</style>
																<!--												--><?php //	}else if( $vou['Vr_Status'] == 'purchased'){
																?>
																<!--														<button class="btn btn-darks">Present voucher code to avail reward</button>-->

																<!--													--><?php //}
																?>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
											<div id="menu1voucher" class="tab-pane fade">
												<div class="card card-body " style="padding-right:0;    border: 0px;">
													<?php foreach ($get_user_voucher as $voucher => $vou) { ?>
														<div class="col-sm-12 d-flex justify-content-between row" style="padding: 10px;
													box-shadow: 1px 0px 7px 3px #80808033;
												
													border-radius: 10px;
													margin-bottom: 10px;
													margin-top: 10px;
													align-items:center;
													padding-right: 22px;">
															<div class="col-sm-3 col-md-3 col-lg-3 d-flex justify-content-center">

																<?php
																if(empty($vou['Po_Thumb'])){ ?>
																	<img style=" min-height: 115px; width: 100%;   border-radius: 6px;" src="<?php echo base_url().'img/banner/create-topic.jpg' ?>">
																<?php }else{ ?>
																	<img style="  min-height: 115px;  width: 100%;       border-radius: 6px;"
																		 src="<?php echo base_url() ?><?php echo $vou['Po_Thumb'] ?>"
																		 alt="icon-earned">
															<?php	}
																?>


															</div>

															<div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-center">

																<div>
																	<p align="center" class="earn-coin-card-title mb-0"><strong
																				style="color: green; text-transform: uppercase;">You are Rewarded!</strong></p>
																	<hr>
																	<p align="center" class="earn-coin-card-title mb-0">
																		<strong> <?php echo $vou['Vr_title'] ?> </strong></p>
																	<p align="center" class="earn-coin-card-desc mb-0">Valid
																		Till : <?php echo $vou['Vr_deadline'] ?> </p>
																	<p style="    text-transform: uppercase; color: #0b9eb6; font-weight: 800;" align="center">Redeem for  <img style="width: 24px;" src="<?php echo base_url().'/img/bcoin.png' ?>"> <?php echo $vou['Vr_radeemprice'] ?> <img style="height: 20px;"
																	 </p>

																</div>
															</div>
															<div class="col-sm-3 col-md-3 col-lg-3 d-flex justify-content-center">
																<!--													--><?php //if (  $vou['Vr_Status'] == 'active' ){
																?>
																<button onclick="getvoucherCode(<?php echo $vou['Vr_Id'] ?>)"
																	style="min-width: 148px;"	class="btn btn-outline-primary getVoucherBtn">Get Voucher Code
																</button>
<style>.getVoucherBtn{max-height: 48px;
    min-width: 112px;
    font-weight: 700;
white-space:nowrap;
padding-left: 7px;}
</style>
																<!--												--><?php //	}else if( $vou['Vr_Status'] == 'purchased'){
																?>
																<!--														<button class="btn btn-darks">Present voucher code to avail reward</button>-->

																<!--													--><?php //}
																?>
															</div>
														</div>
													<?php } ?>
												</div>

											</div>
											<div id="menu2voucher" class="tab-pane fade">
												<div class="card card-body " style=" padding-right:0;   border: 0px;">
													<?php foreach ($send_user_voucher as $voucher => $voucal) { ?>
														<div class="col-sm-12 d-flex justify-content-between row" style="padding: 10px;
													box-shadow: 1px 0px 7px 3px #80808033;

													border-radius: 10px;
													margin-bottom: 10px;
													margin-top: 10px;
													align-items:center;
													padding-right: 22px;">
															<div class="col-sm-3 col-md-3 col-lg-3 d-flex justify-content-center">

																<?php
																if(empty($voucal['Po_Thumb'])){ ?>
																	<img style=" min-height: 115px; width: 100%;   border-radius: 6px;" src="<?php echo base_url().'img/banner/create-topic.jpg' ?>">
																<?php }else{ ?>
																	<img style="  min-height: 115px;  width: 100%;       border-radius: 6px;"
																		 src="<?php echo base_url() ?><?php echo $voucal['Po_Thumb'] ?>"
																		 alt="icon-earned">
																<?php	}
																?>


															</div>

															<div class="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-center">

																<div>
																	<p align="center" class="earn-coin-card-title mb-0"><strong
																				style="color: green; text-transform: uppercase;">You Send Voucher To <?php echo $voucal['Us_Name']?>!</strong></p>
																	<hr>
																	<p align="center" class="earn-coin-card-title mb-0">
																		<strong> <?php echo $voucal['Vr_title'] ?> </strong></p>
																	<p align="center" class="earn-coin-card-desc mb-0">Valid
																		Till : <?php echo $voucal['Vr_deadline'] ?> </p>
																	<p style="    text-transform: uppercase; color: #0b9eb6; font-weight: 800;" align="center">Voucher Price <?php echo $vou['Vr_radeemprice'] ?> <img style="height: 20px;"
																	</p>

																</div>
															</div>
															<div class="col-sm-3 col-md-3 col-lg-3 d-flex justify-content-center">
																<div class="col-sm-12">
																	<img src="<?php if(!empty( $vouall['Us_Thumb'])){
																		echo  $vouall['Us_Thumb'];
																	}else{
																		echo base_url().'img/home/profileimage.jpg';
																	}?>"  style="width: 60px;
																					border-radius: 50%;
																					height: 60px;
																				}" >
																	<h6><font style="color:grey">Voucher Code</font> <?php echo  $vouall['Vr_Voucher_Token']; ?></h6>
																	<h6><font style="color:grey">Status:</font> <?php
																		$voucherDate =   date($vouall['Vr_deadline']);
																		$currentdate =   date('Y-m-d');

																		if($currentdate > $voucherDate  ){
																			echo '</br> Expire';
																		}else{
																			echo '</br>'. $vouall['Vr_Status'];
																		} ?></h6>
																</div>
															</div>
														</div>
													<?php } ?>
												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

							<div id="commentTab" class="not_tab commentTab activity_not">

								<h4 class="activity_not_header">
									Comment Feed
								</h4>


								<?php foreach ($comment_menion_notification as $mention) { ?>
									<div class="activity_not_item" id="todoComment<?php echo $mention['No_ID'] ?>">
										<div class="not_info">
											<div class="not_img_text">
												<?php

												if ($mention['No_Status'] == 'done') { ?>
													<i class="fa fa-check-square"
													   id="showicon<?php echo $mention['No_ID'] ?>" aria-hidden="true"
													   style="color:green; font-size: 20px;"></i>
												<?php } else { ?>
													<i class="fas fa-cog" id="showicon<?php echo $mention['No_ID'] ?>"
													   style="font-size: 20px;"></i>
												<?php } ?>
												<a href="<?php echo base_url() . 'u/' . encode_id($mention['Us_ID']); ?>">
													<img src="<?php
													if (!empty($mention['Us_Thumb'])) {
														echo $mention['Us_Thumb'];
													} else {
														echo base_url() . "img/nophoto.png";
													}

													?>" alt="">
												</a>
												<div class="not_text">
													<a href="<?php echo base_url() . 'u/' . encode_id($mention['Us_ID']); ?>"
													   class="author_name">

														<?php
														if (!empty($mention['Us_Alias'])) {
															echo $mention['Us_Alias'];
														} else {
															echo $mention['Us_Name'];
														}

														?>

													</a>
													<a href="" class="not_caption">
														<?php echo $mention['No_Message'] ?>
													</a>
													</br>
													<a class="not_time">
														<?php echo date("F d, Y h:i", strtotime($mention['No_DatePosted'])); ?>
													</a>
												</div>
											</div>
											<div class="text-center">

													<span class="activities_drop_icon">
															<div class="dropdown">
																  <button type="button"
																		  class="btn btn-primary dropdown-toggle"
																		  data-toggle="dropdown">
																   Action
																  </button>

																  <div class="dropdown-menu">
																 <a class="dropdown-item" href="javascript:void(0)"
																	id="donebutton<?php echo $mention['No_ID'] ?>"
																	data-commenttype="mention"
																	data-postname="<?php echo $mention['Po_Title'] ?>"
																	data-type="<?php if ($mention['No_Status'] == 'done') { ?>unread<?php } else { ?>done<?php } ?>"
																	onclick="doneComment(<?php echo $mention['No_ID'] ?>  )"><?php if ($mention['No_Status'] == 'done') { ?>Un Read<?php } else { ?>     Read      <?php } ?></a>
																   <a onclick="removeComment(<?php echo $mention['No_ID'] ?> )"
																	  data-commenttype="mention"
																	  id="deletebutton<?php echo $mention['No_ID'] ?>"
																	  class="dropdown-item">Delete</a>
																  </div>
																</div>
													</span>
											</div>
										</div>

									</div>
								<?php } ?>

								<?php foreach ($comment_post_notification as $comments) { ?>

									<div class="activity_not_item" id="todoComment<?php echo $comments['No_ID'] ?>">
										<div class="not_info">
											<div class="not_img_text">
												<?php

												if ($comments['No_Status'] == 'done') { ?>
													<i class="fa fa-check-square"
													   id="showicon<?php echo $comments['No_ID'] ?>" aria-hidden="true"
													   style="color:green font-size: 20px;"></i>
												<?php } else { ?>
													<i class="fas fa-cog" id="showicon<?php echo $comments['No_ID'] ?>"
													   style="font-size: 20px;"></i>
												<?php } ?>
												<a href="<?php echo base_url() . 'u/' . encode_id($comments['Us_ID']); ?>">
													<img src="<?php

													if (!empty($comments['Us_Thumb'])) {
														echo $comments['Us_Thumb'];
													} else {
														echo base_url() . "img/nophoto.png";
													}
													?>" alt="">
												</a>
												<div class="not_text">
													<a href="<?php echo base_url() . 'u/' . encode_id($comments['Us_ID']); ?>"
													   class="author_name">

														<?php
														if (!empty($comments['Us_Alias'])) {
															echo $comments['Us_Alias'];
														} else {
															echo $comments['Us_Name'];
														}

														?>
													</a>
													<a href="" class="not_caption">
														<?php echo $comments['No_Message'] ?>
													</a>
													</br>
													<a class="not_time">
														<?php echo date("F d, Y h:i", strtotime($comments['No_DatePosted'])); ?>
													</a>
												</div>
											</div>
											<div class="text-center">

													<span class="activities_drop_icon">
														<div class="dropdown">
														  <button type="button" class="btn btn-primary dropdown-toggle"
																  data-toggle="dropdown">
														   Action
														  </button>

														  <div class="dropdown-menu">

														  <a class="dropdown-item" href="javascript:void(0)"
															 id="donebutton<?php echo $comments['No_ID'] ?>"
															 data-commenttype="mention"
															 data-type="<?php if ($comments['No_Status'] == 'done') { ?>unread<?php } else { ?>done<?php } ?>"
															 data-postname="<?php echo $comments['Po_Title'] ?>"
															 onclick="doneComment(<?php echo $comments['No_ID'] ?>  )"><?php if ($comments['No_Status'] == 'done') { ?>Un Read<?php } else { ?>     Read      <?php } ?></a>
														  <a onclick="removeComment(<?php echo $comments['No_ID'] ?> )"
															 id="deletebutton<?php echo $comments['No_ID'] ?>"
															 data-commenttype="mention" class="dropdown-item">Delete</a>

														  </div>
														</div>
													</span>
											</div>
										</div>

									</div>
								<?php } ?>
							</div>
						</div>

						<div id="not_setting" class="not_setting">
							<ul class="not_setting_list">
								<li>
									<p class="not_setting_name">
										Activites
									</p>
									<div class="not_setting_status on">
										<i class="tick fas fa-check"></i>
										<i class="cross fas fa-times"></i>
										<span class="circle"></span>
									</div>
								</li>
								<li>
									<p class="not_setting_name">
										Comments
									</p>
									<div class="not_setting_status on">
										<i class="tick fas fa-check"></i>
										<i class="cross fas fa-times"></i>
										<span class="circle"></span>
									</div>
								</li>
								<li>
									<p class="not_setting_name">
										Permission
									</p>
									<div class="not_setting_status on">
										<i class="tick fas fa-check"></i>
										<i class="cross fas fa-times"></i>
										<span class="circle"></span>
									</div>
								</li>
								<li>
									<p class="not_setting_name">
										Todo's
									</p>
									<div class="not_setting_status on">
										<i class="tick fas fa-check"></i>
										<i class="cross fas fa-times"></i>
										<span class="circle"></span>
									</div>
								</li>
								<li>
									<p class="not_setting_name">
										Reward
									</p>
									<div class="not_setting_status  on">
										<i class="tick fas fa-check"></i>
										<i class="cross fas fa-times"></i>
										<span class="circle"></span>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-12 not_sidebar_parrent">
					<div class="not_sidebar">
						<!--							<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9842180059459508"-->
						<!--									crossorigin="anonymous"></script>-->
						<!--						  notification page -->
						<!--							<ins class="adsbygoogle"-->
						<!--								 style="display:block"-->
						<!--								 data-ad-client="ca-pub-9842180059459508"-->
						<!--								 data-ad-slot="8209294405"-->
						<!--								 data-ad-format="auto"-->
						<!--								 data-full-width-responsive="true"></ins>-->
						<!--							<script>-->
						<!--								(adsbygoogle = window.adsbygoogle || []).push({});-->
						<!--							</script>-->


						<div class="recent_group mb-2">
							<h4 class="recent_group_header">
								Featured Post.
							</h4>
							<?php if (count($featured_post) > 0) {
								$select = rand(0, count($featured_post));

								?>
								<ul class="recent_group_list">
									<?php foreach ($featured_post as $key => $featuredpost) { ?>


										<li>
											<a href="<?php echo base_url() ?>account/view/post/<?php echo $featuredpost->Po_Slug ?>">
												<img class="img-fluid" style="border-radius: 10px;"
													 src="<?php echo $featuredpost->Po_Thumb ?>" alt="">
											</a>
											<div class="recent_group_text">


												<a class="recent_group_name"
												   href="<?php echo base_url() ?>account/view/post/<?php echo $featuredpost->Po_Slug ?>"
												   title="">
													<?php echo $featuredpost->Po_Title; ?>
												</a>

												<span class="recent_group_drop_icon">
												<i class="fas fa-chevron-right"></i>
											</span>
											</div>
											</a>
										</li>
									<?php } ?>

								</ul>
							<?php } else {
								echo 'No Post Found';
							} ?>
						</div>

						<div class="recent_group">
							<h4 class="recent_group_header">
								Featured groups.
							</h4>
							<ul class="recent_group_list">
								<?php foreach ($featured_Groups as $featured) { ?>


									<li>
										<a href="<?php echo base_url() ?>account/view/group/<?php echo $featured->Gr_Slug ?>">
											<img class="img-fluid" style="border-radius: 10px;"
												 src="<?php echo $featured->Gr_Thumb ?>" alt="">
										</a>
										<div class="recent_group_text">


											<a class="recent_group_name"
											   href="<?php echo base_url() ?>account/view/group/<?php echo $featured->Gr_Slug ?>"
											   title="">
												<?php echo $featured->Gr_Name; ?>
											</a>
											<p class="recent_group_number">
												<?php echo $featured->Us_Posts ?> posts
											</p>
											<span class="recent_group_drop_icon">
												<i class="fas fa-chevron-right"></i>
											</span>
										</div>
										</a>
									</li>
								<?php } ?>

							</ul>
							<!--								<a class="see_more" href="" title="">-->
							<!--									see more-->
							<!--								</a>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<div id="userlistreward" class="modal fade bd-example-modal-md" tabindex="-1" role="dialog"
	 aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="padding: 20px;">
			<div>
				Rewarded user:
			</div>
			<div class="col-sm-12">
				<div class="row" id="inserDiv">

				</div>
			</div>
		</div>
	</div>
</div>
<div id="purchasecoincredit" class="modal fade bd-example-modal-md" tabindex="-1" role="dialog"
	 aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="padding: 20px;">
			<div id="modalpurchasecoins" class=" modal-contentss ">


				<div style="display : flex ; align-items : center ; flex-direction: column; ">
					<p style="margin-top: 10px; font-size: 28px; font-weight: 600; color: #FCB731; margin-bottom: 0px;">
						Coin Credits</p>
					<p style="font-size: 16px; font-weight: 400;">Choose a Package:</p>

					<div class="promo-container">
						<span style="font-size: 14px; font-weight: 500;">Introductory Promo:</span>
						<div style="display: flex; align-items: center; ">
							<span style="font-size: 18px; font-weight: 700; color: #009344;">Get 20%</span>
							<span style="font-size: 14px; font-weight: 500; margin-left: 5px;">Extra on any package!</span>
						</div>
					</div>
					<style>
						.promo-container {
							padding: 5px;
							background-color: #dee6cc;
							width: 100%;
							display: flex;
							justify-content: space-between;
							align-items: center;
						}

						.card-promo-credit-coin {
							display: inline-block;
							padding-bottom: 20px;
							box-shadow: rgb(0 0 0 / 16%) 0px 1px 4px;
							display: flex;
							align-items: center;
							flex-direction: column;
							border-radius: 5px;
						}

						.card-promo-credit-coin-title {
							width: 115px;
							height: 68px;
							border-bottom-left-radius: 15px;
							border-bottom-right-radius: 15px;
							display: flex;
							justify-content: center;
							align-items: center;
						}

						.card-promo-credit-coin-value {
							margin-top: 20px;
							font-size: 20px;
							font-weight: 700;
						}

						.card-promo-credit-coin-active {
							border-radius: 5px;
							padding-bottom: 20px;
							box-shadow: #009344 0px 2px 8px 0px;
							display: flex;
							align-items: center;
							flex-direction: column;
							border: 1px solid #009344;
						}

						.card-promo-credit-coin:hover {
							border-radius: 5px;
							padding-bottom: 20px;
							box-shadow: #009344 0px 2px 8px 0px;
							display: flex;
							align-items: center;
							flex-direction: column;
							border: 1px solid #009344;
						}

						.credit-coin-container-pay {
							border-radius: 5px;
							padding-bottom: 20px;
							box-shadow: rgb(149 157 165 / 20%) 0px 8px 24px;
							border: 1px solid #009344;
						}

						.btns {
							padding: 10px 25px;
							font-size: 12px !important;
							background-image: none !important;
							font-weight: 500 !important;
							border: none !important;
							font-size: 14px;
							font-weight: bold;
							border-radius: 10px;
						}

						.payment-container-title {
							width: 100%;
							height: 68px;
							border-bottom-right-radius: 15px;
							background-color: #009344;
							display: flex;
							justify-content: center;
							align-items: center;
						}

					</style>
					<div class="row" style="width: 100%; margin-top: 15px;" id="modalCreditPointBuy">
						<?php foreach ($allpacakges as $pack) { ?>

							<div class="col-xs-12 col-md-4">
								<div class="card-promo-credit-coin">
									<div class="card-promo-credit-coin-title"
										 style="background-color: <?php echo $pack['CP_Color'] ?>;">
										<span style="color: white; font-size: 18px; font-weight: 500;"
											  id="cpName<?php echo $pack['CP_ID']; ?>"><?php echo $pack['CP_Name'] ?></span>
									</div>
									<span class="card-promo-credit-coin-value" style="color: #1F62AE;"
										  id="cpprice<?php echo $pack['CP_ID']; ?>"> P<?php echo $pack['CP_Ammount'] ?></span>
									<span style="text-align: center ; margin-top: 20px;"
										  id="cpdesc<?php echo $pack['CP_ID']; ?>"><?php echo $pack['CP_Description'] ?></span>
									<button type="button" class="btns  btn-xl"
											style="color: white; background: <?php echo $pack['CP_Color'] ?>; margin-top: 20px;"
											onclick="showthisone('<?php echo $pack['CP_ID']; ?>')">Buy Now
									</button>
								</div>
							</div>
						<?php } ?>

					</div>

					<div style="width: 100%; display: none; margin-top: 15px; margin-left: 51px!important;"
						 id="modalCreditPointPay">
						<div class="row credit-coin-container-pay" style="width: 100%;">
							<div class="col-xs-12 col-md-6"
								 style="padding-left: 0px; display: flex ; align-items: center; flex-direction: column;">
								<div class="payment-container-title">
									<input type="hidden" id="selectedpackege">
									<span style="font-size: 18px; color: white; font-weight: 500;"
										  id="paytype">Standard</span>
								</div>
								<p style="color: #009344; font-weight: 700; font-size: 20px; margin-top: 20px;"
								   id="payprice">P2,000</p>
								<p style="margin-top: 20px; font-size: 14px; text-align: center;" id="paydes">Equivalent
									to <br>
									2,000 coins</p>
								<button type="button" class="btns btn-primary btn-lg" style="margin-top: 15px;"
										onclick="purchasecredit()">Pay Now
								</button>
								<button class="btns  btn-lg" onclick="backtopayment()">Cancel</button>
							</div>
							<div class="col-xs-12 col-md-6"
								 style="display: flex; justify-content: center; align-items: center;">
								<img src="<?php echo base_url(); ?>assetsofpop/img/payment-img.png" alt=""
									 style=" width: 163px;  margin-top: 40px;">
							</div>
						</div>
					</div>

					<p style="font-size: 16px; font-weight: 500; margin-top: 20px;">Have any questions?</p>
					<button type="button" class="btn  btn-primary mr-2" style="    background: white;
											 color: #398cb3;
											 padding: 0px 8px;
											 margin-top: 10px;">Contact Us
					</button>

					<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.png"
						 style="width: 100%; margin-top: 10px;">
				</div>
			</div>
		</div>
	</div>
</div>
<div id="buyvoucher" class="modal fade bd-example-modal-md" tabindex="-1" role="dialog"
	 aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="padding: 20px;">
			<div id="modalInstantRewardCointCredit" class="modal-contents" style="width: 100%; /* display: flex; */">
				<h2 class="title-instant-reward" align="center" style="color: #FCB731; margin-top: 20px;">
					Pay with Coin Credits
				</h2>
				<p class="desc-instant-reward" align="center" style="color: #1F62AE; margin-top: 20px;">Your Total
					Reward Coins Offer:</p>

				<div class="coin-input-value d-flex justify-content-center">
					<div style="display: flex;
    background: #f2f1f1;
    padding: 5px 13px;
    border-radius: 50px;">
						<img src="http://localhost/boardspeak_2020/assetsofpop/img/icon-coin.png" alt="icon coin"
							 style="width: 25px;">
						<p style="margin-left: 20px; color: #FCB731; margin-bottom: 0px; font-size: 18px; font-weight: 600; "
						   id="totalsendingValue">48</p>
					</div>
				</div>
				<style>
					.coin-input-value {
						border-radius: 28px;
						/*background-color: #f2f1f1;*/
						position: relative;
						display: inline-flex;
						flex-direction: row;
						padding: 5px 20px;
					}
				</style>
				<div style="width : 100% ; margin-top: 20px;">
					<p class="title-form-instant-reward" style="color: #1F62AE;">My Coins</p>
					<div style=" display: flex; flex-direction: row; ">
						<p> available balance: </p>
						<div>
							<img src="http://localhost/boardspeak_2020/assetsofpop/img/icon-coin.png" alt="icon coin"
								 style="width: 25px; margin-left: 20px;">
						</div>
						<p style="color: #ECCD67; margin-left: 10px;" id="currentbalance">11821</p>
					</div>

				</div>
				<input type="hidden" id="voucheridx">
				<input type="hidden" id="voucherpricex">
				<input type="hidden" id="vouletprice">
				<div id="proceedpayment" style="">
					<h6 align="center">Account balanace will be <font id="remaningcredit">11773</font> coins</h6>
					<div class="d-flex justify-content-center">
						<div style="margin-right: 10px;">
							<button class="btn btn-primary" style="    font-size: 16px!important;
    text-transform: capitalize;" onclick="proceedAmmount()">Proceed
							</button>
						</div>
						<div>
							<button class="btn btn-info" style="    font-size: 16px!important;
    text-transform: capitalize;" onclick="showfron()">cancel
							</button>
						</div>
					</div>
				</div>
				<div id="buynowcoin" style="display: none;">
					<p style="font-size: 14px; margin-top: 20px; margin-bottom: 5px;">Your available coin credits is not
						enough.</p>
					<p style="font-size: 20px; font-weight: 600; color: #088B34;">You are short by <font
								class="shortcoins"></font> coins.</p>

					<div class="d-flex justify-content-center">
						<div style="display: inline-flex; flex-direction: row; align-items: center;  padding: 10px; border-radius: 10px; background-color: #FCB731;">
							<img src="http://localhost/boardspeak_2020/assetsofpop/img/icon-coin.png" alt="icon coin"
								 style="width: 50px;">
							<div style="margin: 0px 15px; height: 50px; width: 2px; background-color: #231F20;"></div>
							<p style="margin-bottom: 0px; font-size: 22px; font-weight: 700;" class="shortcoins"
							   id="shortcoins"></p>
						</div>
					</div>

					<div class="row" style="margin-top: 30px; justify-content: center; ">
						<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; ">

							<button type="button" onclick="purchasecoin()" class="btns btn-primary btn-lg">Buy Coins
							</button>
						</div>
						<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; ">
							<button type="button" onclick="showfron()" class="btn btn-primary-outline btn-lg"
									id="cointCreditsButtonCancel">Cancel
							</button>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<div id="showvoucherpurchase" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog"
	 aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">

			<div class="mt-3">
				<div class="d-flex justify-content-center">
					<img id="userimage" style="     border-radius: 50%; width: 60px;"
						 src="<?php echo base_url() ?>/assetsofpop/img/icon-coin.png">
				</div>
				<div>
					<h6 align="center" id="username">Chris</h6>
					<h6 align="center" id="usertype" style="font-weight: 300;">CEO</h6>
				</div>
			</div>
			<div>
				<img style="width: 100%!important;
					margin-bottom: 14px;
					height: 250px;"
					 id="postimage" src="<?php echo base_url() ?>/assetsofpop/img/icon-coin.png">
			</div>
			<div class="p-2" style="     background-image: linear-gradient(to bottom right, #0542c3, #10b647);
						padding: 10px;
						margin: 10px;
						border-radius: 10px;">
				<div>
					<h3 align="center" style="font-size: 2.5rem;
    color: white;
    margin-top: 9px;
    margin-bottom: 16px;" id="vouchertitle">50% Off</h3>
					<h6 align="center" style="font-weight: 300;
    				text-transform: uppercase; color:white" id="voucherprice">On your next purchase</h6>

					<div class="d-flex justify-content-center" style="    background-image: url(http://localhost/boardspeak_2020/assetsofpop/Couponbackground.png);
						height: 170px;
						padding: 14px;
						background-size: 100% 162px;
						background-repeat: no-repeat;">
 						<button id="buttonpurchaseused" class="btn btn-info" style=" display: grid;
								 border: 0px; background: transparent; color:black;">
							<font style="    font-size: 19px;
										font-weight: 900;
										color: grey;
										padding: 0px;
										margin: 0px;
										text-transform: uppercase;">Voucher Code</font> <font style="font-size:25px;"
																							   id="vouchervodenumber"></font><font
									style="    font-size: 21px;
										font-weight: 900;
										color: #349671;
                                        font-weight: 700;
										padding: 0px;
										margin: 0px;" id="valueatt">
							</font></button>

						<button class="btn btn-info" style="background: transparent;
						color: black; border:0px;
						 " id="buttonnopuchase">Voucher
						</button>


					</div>

					<center>
						<h4 id="showsubtitle"></h4>
					</center>

					<div class="rect-date">
						<p style="font-size:11px;font-weight:600;margin:0;">Offer Valid Till</p>
						<div style='display:flex;'>
							<div class="month rectt">
								<span class="lett month0">3</span>
								<span class="lett  month1">2</span>
							</div>
							<div class="date rectt">
								<span class="lett day0">1</span>
								<span class="lett day1">1</span>
							</div>
							<div class="year rectt">
								<span class="lett year0">2</span>
								<span class="lett year1">0</span>
								<span class="lett year2" >2</span>
								<span class="lett year3">2</span>


							</div>
						</div>
					</div>
					<style>
						.rect-date {
							flex-direction: column;
							background-color: #70B8A3;
							display: flex;
							align-items: center;
							justify-content: center;
							/* padding: 20px; */
							height: 105px;
							width: 269px;
							margin: auto;
							font-family: 'Poppins', sans-serif;
							border-radius: 4px;
						}

						.rectt {
							display: flex;
							margin: 5px;
							font-size: 3rem;

						}

						.lett {
							background-color: white;
							margin: 1px;
							border-radius: 2px;
							width: 27px;
							text-align: center;
							/* padding: 4px; */
							height: 59px;
							display: flex;
							align-items: center;
							justify-content: center;
						}
						#radeemvoucherbutton:hover{
background:rgb(54 177 176);
						}</style>
				</div>
			</div>
			<div style="align-items: center;" class="d-flex justify-content-center">
				<input type="hidden" id="voucherid">
				<button class="btn btn-primary" style="  font-size: 20px;  font-weight: 900;font-size: 15px;
																font-weight: 900;
																border: none;
																margin-top: 10px;
																/* padding: 10px; */
																padding-left: 23px;
																padding-right: 23px;"
						onclick="purchasevoucher()" id="radeemvoucherbutton">
						Redeem the voucher
				</button>
				<font id="expiredbutton" style='font-size: 20px; color: #c00909;
    margin-top: 10px;
    text-transform: uppercase;'>Expired</font>


			</div>
			<div>
				<p class="discription" id="descriptionshow"></p>
			</div>
		</div>
	</div>
</div>

<style>
	.discription {
		padding: 10px;
		min-height: 100px;
		max-height: 200px;
		overflow: hidden;
		overflow-y: scroll;
	}
</style>
<script type="text/javascript">

	function getsendrewardDetail(RID, userlist) {
		$.ajax({
			url: base_url + 'account/getsendrewardDetail/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: RID,
				list: userlist
			},
			success: function (response) {
				$('#inserDiv').empty();
				if (response.length > 0) {
					$string = '';

					console.log(response)
					for (let i = 0; i < response.length; i++) {

						if (response[i]['Us_Alias']) {
							$username = response[i]['Us_Alias'];
						} else {
							$username = response[i]['Us_FName'] + ' ' + response[i]['Us_LName'];
						}
						$imageaddress = base_url + 'assetsofpop/img/avatar.svg';
						$substring = `<div class='row col-sm-12'><div class='col-sm-8'><img src="${$imageaddress}">
							<h6>${$username}</h6></div><div class='col-sm-2' style='    text-align: center;'><span>Recieve ${response[i]['RC_Ammount']} Coins</span></div></div>`
						$string += $substring;

					}
					$('#inserDiv').append($string);
					$('#userlistreward').modal('show');
				}
				//

			}
		});
	}

	function proceedAmmount() {
		let voucherid = $('#voucheridx').val();

		$.ajax({
			url: base_url + 'account/getVoucherCodecharge/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: voucherid,

			},
			success: function (response) {


				console.log(response);

				Swal.fire({
					icon: response.type,
					title: response.action,
					text: response.message,

				});

				$('#buyvoucher').modal('hide');

			}
		});
	}

	function purchasecoin() {
		$('#buyvoucher').modal('hide');
		$('#purchasecoincredit').modal('show');
	}

	function backtopayment() {
		$('#modalCreditPointBuy').show();
		$('#modalCreditPointPay').hide();
	}

	function purchasecredit() {


		$.ajax({
			url: base_url + 'account/purcashingCoins',
			method: 'POST',
			dataType: 'json',
			data: {
				PackageId: $('#selectedpackege').val(),


			},
			success: function (response2) {

				console.log(response2);

				if (response2 == '1') {
					Swal.fire({
						icon: 'success',
						title: 'Coin Purchased',
						text: 'Successfully purchased Coins. Check your Wallet',

					});
					$('#purchasecoincredit').modal('hide');
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Please Try Again Later',

					});
				}


			}
		});
	}

	function showthisone(id) {


		$('#selectedpackege').val(id);
		$('#paytype').text($('#cpName' + id).text());
		$('#payprice').text($('#cpprice' + id).text());
		$('#paydes').text($('#cpdesc' + id).text());

		$('#modalCreditPointBuy').hide();
		$('#modalCreditPointPay').show();

	}

	function showfron() {
		$('#buyvoucher').modal('hide');
	}

	function purchasevoucher() {
		let voucherid = $('#voucherid').val();

		$.ajax({
			url: base_url + 'account/getVoucherCode/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: voucherid,

			},
			success: function (response) {


				console.log(response);
				if (response.length > 0) {


					$('#showvoucherpurchase').modal('hide');
					$('#buyvoucher').modal('show');
					$('#voucheridx').val(voucherid);


					$('#voucherpricex').val(response[0]['Vr_radeemprice'])
					$('#vouletprice').val(response[0]['Us_Coins'])


					$('#totalsendingValue').text(response[0]['Vr_radeemprice'])
					$('#currentbalance').text(response[0]['Us_Coins'])
					$('#remaningcredit').text(parseInt(response[0]['Us_Coins']) - parseInt(response[0]['Vr_radeemprice']))

					if (parseInt(response[0]['Us_Coins']) >= response[0]['Vr_radeemprice']) {

						$('#buynowcoin').hide();
						$('#proceedpayment').show();

					} else {
						$('#shortcoins').text(parseInt(response[0]['Vr_radeemprice']) - parseInt(response[0]['Us_Coins']));

						$('#proceedpayment').hide();
						$('#buynowcoin').show();
					}

				} else {
					Swal.fire({
						icon: 'error',
						title: 'Wrong Info',
						text: 'Something went wrong . please try Again!',

					})
				}
			}
		});
	}


		function ShowvoucherDetail(userList){

		console.log(userList);
		console.log(userList.length);

 	}
	function getvoucherCode(voucherid) {
		$('#expiredbutton').hide();

		$.ajax({
			url: base_url + 'account/getVoucherCode/',
			method: 'POST',
			dataType: 'json',
			data: {
				id: voucherid,
			},
			success: function (response) {
				if ($('#radeemvoucherbutton').hasClass('btn-primary')) {

				} else {

					$('#radeemvoucherbutton').addClass('btn-primary')
				}
				$('#radeemvoucherbutton').prop('disabled', true);

				if (response.length > 0) {
					// $("#radeemvoucherbutton").prop("onclick", null).off("click");
					$('#voucherid').val(voucherid);

					$('#showvoucherpurchase').modal('show');


					var first = response[0]['Vr_deadline'];


					if (new Date(first) > new Date()) {

						$('#validtext').text('valid Till')
					} else {
						$('#validtext').text('expire')

					}



				$year =	first.split('-')[0];
				$month =	first.split('-')[1];
				$days =	first.split('-')[2];

					 $('.month0').text($month[0]);
					 $('.month1').text($month[1]);

					$('.day0').text($days[0]);
					$('.day1').text($days[1]);

					$('.year0').text($year[0]);
					$('.year1').text($year[1]);

					$('.year2').text($year[2]);
					$('.year3').text($year[3]);




					$('#descriptionshow').text(response[0]['Vr_decription']);
					$('#vouchertitle').text(response[0]['Vr_title']);
					$('#voucherdeadtime').text(response[0]['Vr_deadline']);
					$('#userimage').attr("src", response[0]['Us_Photo']);
					$('#postimage').attr("src", base_url + response[0]['Po_Thumb']);

					$('#username').text(response[0]['Us_Name']);
					$('#usertype').text(response[0]['Us_JobTitle']);
					$('#voucherprice').text('Avail Voucher in ' + response[0]['Vr_radeemprice'] + 'coin. Worth of P' + response[0]['Vr_paisaammount']);
					$('#radeemvoucherbutton').text( ' Redeem ' + ' for  ' + response[0]['Vr_radeemprice'] + ' coins');
						$('#showsubtitle').text(response[0]['Vr_sub_title']);
					$('#showsubtitle').css('color' , 'white');
					$('#showsubtitle').css('font-size' , '14px');
					if (response[0]['Vr_Status'] == 'purchased') {
						$('#vouchervodenumber').text(response[0]['Vr_Voucher_Token']);
						$('#valueatt').text('Valued at P' + response[0]['Vr_paisaammount'] + '')
						$('#buttonpurchaseused').show();
						$('#buttonnopuchase').hide();
					} else if (response[0]['Vr_Status'] == 'used') {
						$('#vouchervodenumber').text(response[0]['Vr_Voucher_Token']);
						$('#valueatt').text('Valued at P' + response[0]['Vr_paisaammount'] + '')
						$('#buttonpurchaseused').show();
						$('#buttonnopuchase').hide();
					} else if (response[0]['Vr_Status'] == 'active') {
						$('#buttonpurchaseused').hide();
						$('#buttonnopuchase').show();
					}
					let voucherdate = new Date(first);
					let currentdate = new Date();
					$('#radeemvoucherbutton').css('color', '#f3f6f6')
					$('#radeemvoucherbutton').css('background', '#388cb3', 'important');

					if (voucherdate < currentdate) {

						$('#radeemvoucherbutton').text('Voucher is already ');
						$('#radeemvoucherbutton').removeClass('btn-primary')

						$('#radeemvoucherbutton').css('color', '#099C8A')
						$('#radeemvoucherbutton').css('background', 'rgba(245,245,245,0)')
						$('#expiredbutton').show();
 						$('#radeemvoucherbutton').prop('disabled', true);
					} else {
						if (response[0]['Vr_Status'] == 'purchased') {
							$('#radeemvoucherbutton').text('Present voucher code to avail reward');
							$('#radeemvoucherbutton').css('background' , 'white');
							$('#radeemvoucherbutton').css('color' , 'green');


							$('#radeemvoucherbutton').prop('disabled', true);
						} else if (response[0]['Vr_Status'] == 'used') {
							$('#radeemvoucherbutton').text('Voucher code already used');

							$('#radeemvoucherbutton').prop('disabled', true);
						} else if (response[0]['Vr_Status'] == 'active') {

							$('#radeemvoucherbutton').prop('disabled', false);

						}

					}


				} else {
					Swal.fire({
						icon: 'error',
						title: 'Wrong Info',
						text: 'Something went wrong . please try Again!',

					})
				}
			}
		});
	}

	window.onload = function () {
		$("#filter").keyup(function () {

			var filter = $(this).val(),
				count = 0;
			console.log(filter);
			$('#myDIV li ').each(function () {

				if ($(this).text().search(new RegExp(filter, "i")) < 0) {
					$(this).hide();

				} else {
					$(this).show();
					count++;
				}
			});
		});
	}
</script>
<script>
	// const rewBtn1 = document.querySelector('.rew-btn-1');
	// const rewBtn2 = document.querySelector('.rew-btn-2');
	// const Btn1 = document.querySelector('.btn-1');
	// const Btn2 = document.querySelector('.btn-2');
	// rewBtn1.addEventListener("click", () => {
	//
	// 	if (rewBtn1.classList.contains('bg-btn')) {
	// 		rewBtn1.classList.toggle('bg-btn');
	// 		rewBtn2.classList.toggle('bg-btn');
	// 	}
	// 	if (rewBtn1.classList.contains('bg-btn') == false) {
	// 		rewBtn1.classList.toggle('bg-btn');
	// 		rewBtn2.classList.toggle('bg-btn');
	// 	}
	//
	//
	// })
	// rewBtn2.addEventListener("click", () => {
	//
	// 	if (rewBtn2.classList.contains('bg-btn') == false) {
	// 		rewBtn2.classList.toggle('bg-btn');
	// 		rewBtn1.classList.toggle('bg-btn');
	// 	}
	//
	// })
</script>
<style>
	.imagesidebar {
		width: 100%;
		height: 150px;
		border-radius: 10px;
	}

	.centerclass {
		display: inline-flex;
		width: 100%;
	}

	.inputtt {
		padding-top: 27px;
		max-width: 609px;
		margin: auto;
	}

	.refer-div button {
		border: 1px solid #388cb3;
		padding: 5px;
		border-radius: 5px;
		padding-left: 7px;
		padding-right: 7px;
		font-size: 14px;
		white-space: normal;
		background: transparent;
		color: black;
	}

	.headerofdate {
		padding: 10px;
		background: hsl(156deg 16% 94%);
		border-radius: 3px;
		padding-left: 15px;
		font-size: 14px;
	}
	#tabbutton  li a.active {
		border: 1px solid #54b3ea;
		padding: 10px 16px;
		/* border-bottom: 0px; */
		border-radius: 3px;
		
		border: 0;
		background: #cbecf3;
    color: #4691b2;
    border-radius: 7px;
    font-size: 1.1rem;
	}
	.reward_coins_head {
		white-space: nowrap;
	}

	.reward-top-buttons {
		display: flex;
		justify-content: center;
		color:

	}

	.reward-top-buttons button {
		margin: 4px;
		border-radius: 6px;
		color: #458db4;
		padding: 5px;
		outline: none;
		font-weight: 600;

		font-size: 1.4rem;
		padding-left: 20px;
		margin-bottom: 20px;
		padding-right: 20px;
	}
	/*This will be the style of Radeem for button iftikhar */
	.youAreRewardedBtn{
		font-size: 15px;
		font-weight: 900;
		/* color: rgb(243, 246, 246); */
		/* background: rgb(56, 140, 179); */
		border: none;
		margin-top: 10px;
		/* padding: 10px; */
		padding-left: 23px;
		padding-right: 23px;
	}
	.bg-btn {
		background: #d0edf3;
	}
</style>
