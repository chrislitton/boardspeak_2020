

  <section class="board-menu-section boards-menu mt-4" id="boardmenu">
  	<div class="container">
   	<div class="row bg-light">			
	    	<div class="col-md-12 col-lg-12">
					<nav class="navbar navbar-expand-lg navbar-light justify-content-end">
							<!-- Navigation -->
							<button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-white rounded" type="button" data-toggle="collapse" data-target="#navSubMenu" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">        
								More <i class="fas fa-bars"></i>
							</button>
							<div class="collapse navbar-collapse" id="navSubMenu">
								<ul class="navbar-nav nav" role="tablist" id="navtab">
									<li class="nav-item mx-0 mx-lg-1">
										<a class="nav-link <?php if (strcmp($selMenu,'groups')==0) echo 'active';?>" href="<?php echo base_url(); ?>account/profile">Group</a>
									</li>
									<li class="nav-item mx-0 mx-lg-1">
										<a class="nav-link <?php if (strcmp($selMenu,'topics')==0) echo 'active';?>" href="<?php echo base_url(); ?>account/profile/topics">Topics</a>		
									</li>
									<li class="nav-item mx-0 mx-lg-1">
										<a class="nav-link <?php if (strcmp($selMenu,'posts')==0) echo 'active';?>" href="<?php echo base_url(); ?>account/profile/posts">Posts</a>
									</li>									
								</ul>
							</div>
					</nav>
      		</div>			
	</div>
	</div>
	</section>
	
	
