<footer id="footer" class="footer">
	<div class="footer_container container">
		<div class="row">
			<div class="col-md-4">
				<div class="footer_item">
					<h5>
						ABOUT US
					</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="footer_item">
					<h5>
						NECESSARY LINKS
					</h5>
					<ul>
						<li>
							<a href="" title="">
								For link one
							</a>
						</li>
						<li>
							<a href="" title="">
								To get link two
							</a>
						</li>
						<li>
							<a href="" title="">
								Click here link three
							</a>
						</li>
						<li>
							<a href="" title="">
								Click here
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-4">
				<div class="footer_item">
					<h5>
						FIND US ON
					</h5>
					<p>
						Stay Connected with Us. And get the latest update about our thing. Blah Blah.
					</p>
					<ul class="social_links">
						<li>
							<a href="" title="">
								<i class="fab fa-twitter"></i>
							</a>
						</li>
						<li>
							<a href="" title="">
								<i class="fab fa-facebook"></i>
							</a>
						</li>
						<li>
							<a href="" title="">
								<i class="fab fa-instagram-square"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo base_url(); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="<?php echo base_url(); ?>js/jqBootstrapValidation.js"></script>

<!-- Custom scripts for this template -->
<script src="<?php echo base_url(); ?>js/freelancer.min.js"></script>
<script src="<?php echo base_url(); ?>js/custom.js"></script>

<!--OwlCarousel-->
<script src="<?php echo base_url(); ?>vendor/slick/slick.js" type="text/javascript" charset="utf-8"></script>
	
<script src="<?php echo base_url(); ?>js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
<!-- google adsense -->
<script data-ad-client="ca-pub-1969299147736131" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<script>
	var base_url;
	$(document).ready( function() {
		base_url = <?php echo json_encode(base_url()); ?>;
	});
</script>

<!-- custom script per page -->
<?php if (isset($this->session->userdata['logged_in']) && isset($asset)){ ?>
	<script src="<?php echo base_url(); ?>assets/js/<?=$asset?>.js"></script>
<?php } ?>