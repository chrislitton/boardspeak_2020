<?php 
	$this->load->helper('url');
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" type="images/icon" href="<?php echo base_url(); ?>img/icon.png" />
  <meta name="description" content="">
  <meta name="author" content="">

  <title>BoardSpeak - Get more things done!</title>
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9842180059459508"
			crossorigin="anonymous"></script>
  <!-- Custom fonts for this theme -->
  <link href="<?php echo base_url(); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <!-- Theme CSS -->  
  <link href="<?php echo base_url(); ?>css/freelancer.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/signin.css" rel="stylesheet">

</head>

<body id="page-top">
