	<!-- Footer -->
  <footer class="account-footer">
    <div class="container">
      <div class="row">


        <!-- Footer Social Icons -->
        <div class="col-lg-6 mb-2 mb-lg-0 account-social">
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-facebook-f"></i>
						</a>
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-twitter"></i>
						</a>
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-linkedin-in"></i>
						</a>
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-dribbble"></i>
						</a>
        </div>


        <!-- Footer About Text -->
        <div class="col-lg-6 account-copyright">
          <p class="lead mb-1">© BoardSpeak 2020</p>
        </div>

      </div>
    </div>
  </footer>





	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
							<span class="text-muted">Delete Confirmation</span>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">
									<i class="fas fa-times"></i>
								</span>
							</button>
            </div>
            <div class="modal-body text-center py-2 pt-5">
                <p class="lead text-danger">Are you sure you want to delete this record?</p>
            </div>
            <div class="modal-footer pt-5 pb-4">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
	</div>




	<div class="modal fade" id="edit-groupcategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
			<?php echo form_open_multipart('admin/updategroupcategory'); ?>
            <div class="modal-header">
				<span class="text-muted">Edit Group Category</span>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<i class="fas fa-times"></i>
					</span>
				</button>
            </div>
            <div class="modal-body text-center py-2 pt-5">
                <div class="control-group">
					<div class="admin-form-group controls mb-0 pb-1">
						<div class="input-group">
								<div class="input-group-prepend">
									<label class="input-group-text rounded-0" for="txtEditGroupCategoryName">Name</label>
								</div>
								<input class="form-control" id="txtEditGroupCategoryName" name="txtEditGroupCategoryName" type="text" required="required">
								<input class="form-control" id="txtEditGroupCategoryID" name="txtEditGroupCategoryID" type="hidden">
						</div>
					</div>
				</div>
                <div class="control-group">
					<div class="admin-form-group controls mb-0 pb-1">
						<div class="input-group">
								<div class="input-group-prepend">
									<label class="input-group-text rounded-0" for="selEditGroupCategoryOrder">Order</label>
								</div>
								<input class="form-control" id="txtEditGroupCategoryOrder" name="txtEditGroupCategoryOrder" type="number" placeholder="Order" required="required" data-validation-required-message="Please enter order.">
						</div>
					</div>
				</div>

            </div>
            <div class="modal-footer pt-5 pb-4">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit" class="btn btn-primary">Save Changes</button>
            </div>
		</form>
        </div>
    </div>
	</div>



	<div class="bg_load"></div>
	<div class="wrapper">
			<div class="loader"></div>
			<p class="lead mt-1 ml-2">Loading.....</p>
	</div>



  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url(); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="<?php echo base_url(); ?>js/jqBootstrapValidation.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url(); ?>js/freelancer.min.js"></script>
	<script src="<?php echo base_url(); ?>js/custom.js"></script>

  <script>
	$(document).ready( function() {

	    $(document).on('change', '.btn-file :file', function() {
			var input = $(this),
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [label]);
			});

		$('.btn-file :file').on('fileselect', function(event, label) {

			var input = $(this).parents('.input-group').find(':text'),
				log = label;

			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}

		});

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img-upload').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}


		function readURLBG(input,img) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$(img).attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#userfile").change(function(){
			readURL(this);
		});


		$("#fleBackground").change(function(){
			readURLBG(this,'#img-background');
		});

		$("#flePhoto").change(function(){
			readURL(this);
		});


        $(".bg_load").fadeOut("slow");
        $(".wrapper").fadeOut("slow");


	});



	</script>
</body>

</html>
