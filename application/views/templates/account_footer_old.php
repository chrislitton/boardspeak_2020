<?php
	if (isset($this->session->userdata['logged_in'])){
		$bs_email = ($this->session->userdata['logged_in']['bs_email']);
		$bs_alias = ($this->session->userdata['logged_in']['bs_alias']);
		$bs_thumb = ($this->session->userdata['logged_in']['bs_thumb']);
		$bs_admin = ($this->session->userdata['logged_in']['bs_admin']);
	} 
?>



  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-mainmenu fixed-top" id="mainNav">
	  <div class="container">
      <a class="navbar-brand" href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>img/logo.png" class="imglogo"></a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas icon-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
				
        <ul class="navbar-nav ml-auto">
			<li class="nav-item mx-0 mx-lg-0">
				<a class="nav-link py-3 px-0 text-uppercase" href="<?php echo base_url(); ?>account/">Find Groups</a>
			</li>
			<li class="nav-item mx-0 mx-lg-0 dropdown">
            <a href="#" class="nav-link py-3 px-0 text-uppercase dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Start A Group</span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link px-3" href="<?php echo base_url(); ?>account/create/group">Start a Group</a></li>
                <li><a class="nav-link px-3" href="<?php echo base_url(); ?>account/create/topic">Create a SubGroup</a></li>
				<li><a class="nav-link px-3 pr-5" href="<?php echo base_url(); ?>account/create/post">Make a Post</a></li>
            </ul>
        </li>
				<li class="nav-item mx-0 mx-lg-2 row">
					<a class="col nav-link py-3 px-0 px-lg-0 text-uppercase position-relative menu-icon-link" href="<?php echo base_url(); ?>account/notifications"><i class="fa fa-bell menu-icon rounded-circle"></i></a>
					<div class="col nav-item mx-0 mx-lg-2 dropdown">
						<a href="#" class="nav-link py-3 px-0 px-lg-1 text-uppercase dropdown-toggle menu-photo-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
							<i class="fa fa-user menu-icon rounded-circle"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/profile">Profile</a></li>
								<li><a class="nav-link font-weight-normal px-3" href="javascript:void(0)"    >Help & Support</a></li>
								<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/settings">Settings</a></li>
								<?php if ($bs_admin=='1') { ?>
								<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>admin">Administrator</a></li>
								<?php } ?>

								<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>pages/view/logout">Log Out</a></li>
							</ul>
					</div>
				</li>

        </ul>
      </div>
    </div>
  </nav>
