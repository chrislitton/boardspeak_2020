<?php
	$GroupName = $subtopics_items['Gr_Name'];
	if (strlen($GroupName)>=25) $GroupName = substr($GroupName,0,25)."... ";
	
	$TopicName = $subtopics_items['Topic'];
	if (strlen($TopicName)>=25) $TopicName = substr($TopicName,0,25)."... ";
	
	$SubTopicName = $subtopics_items['To_Name'];
	if (strlen($SubTopicName)>=25) $SubTopicName = substr($SubTopicName,0,25)."... ";

?>

<!-- Masthead -->
  <header class="dethead" id="groupbanner">
    <div class="container">
  			<div class="row">
        		<div class="col-md-12 col-lg-12 board-banner bg-subtopic text-right <?php echo $subtopics_items['To_Backcolor']; ?>"  style="background:transparent url('<?php echo $subtopics_items['To_Photo'];?>') no-repeat center center /cover">
						<div class="post-title text-white"><?php echo $SubTopicName; ?></div>
  					<a class="btn btn-primary mt-3 changebgbtn <?php echo $ShowAdminAction;?>" href="<?php echo base_url(); ?>account/editsubtopicbg/<?php echo $subtopics_items['To_ID'];?>"><i class="fa fa-camera pr-2"></i>change background</a>
  				</div>
	     	</div>
	</div>
  </header>
  
  <section class="board-toolbar-section" id="boardtoolbar">
  	<div class="container">
		 	<div class="row bottom-line">
				<div class="col-md-12 col-lg-9 board-menu-title">
					<h4 class="text-uppercase mb-0">
						<?php echo $SubTopicName; ?>
						<a class="editoption <?php echo $ShowAdminAction;?>" href="<?php echo base_url(); ?>account/editsubtopicdetail/<?php echo $subtopics_items['To_ID'];?>"><i class="fa fa-edit px-1"></i>edit sub-topic</a>
						<a class="editoption text-danger <?php echo $ShowAdminAction;?>" href="#" data-href="<?php echo base_url(); ?>account/deletesubtopic/<?php echo $subtopics_items['To_ID'];?>" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash px-1"></i>delete sub-topic</a>
					</h4>
					<p class="lead board-category"><span class="bg-subtopic text-black pb-1 px-2"><?php echo ucfirst($subtopics_items['To_Privacy']); ?> Sub-Topic</span> | Created by <?php echo $subtopics_items['Us_Name']; ?> on <?php echo date( "F d, Y h:i", strtotime($subtopics_items['To_DatePosted']) ); ?> </p>
				</div>

					<div class="col-md-12 col-lg-3 board-menu-actions">
							<a class="btn btn-danger mr-2 <?php echo $ShowJoinAction; ?>" href="<?php echo base_url(); ?>account/join/subtopic/<?php echo $subtopics_items['To_ID']; ?>">Join Topic</a>
							<a class="btn btn-danger mr-2 <?php echo $ShowLeaveAction; ?>" href="<?php echo base_url(); ?>account/leave/subtopic/<?php echo $subtopics_items['To_ID']; ?>">Leave Topic</a>
					</div>	  	
			</div>


			<div class="row">				
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/profile">Profile</a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/group/<?php echo $subtopics_items['To_Gr_ID'];?>"><?php echo $GroupName; ?></a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/topic/<?php echo $subtopics_items['To_Pa_ID'];?>"><?php echo $TopicName; ?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url(); ?>account/subtopic/<?php echo $subtopics_items['To_ID'];?>"><?php echo $SubTopicName; ?></a></li>
					</ol>
				</nav>
			</div>
  </div>      
  </section>
	
	
	
