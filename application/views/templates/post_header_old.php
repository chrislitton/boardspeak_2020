
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.1.0/classic/ckeditor.js"></script>
<!--<link rel="stylesheet" href="--><?php //echo base_url(); ?><!--assetsofpop/css/index.css">-->
<meta property="og:title" content="<?php echo $posts_item['Po_Title']; ?>">
<meta property="og:description" content="<?php echo $posts_item['Po_Description']; ?>">
<meta property="og:image" content="<?php echo base_url().$posts_item['Po_Photo']; ?>">
<meta property="og:url" content="<?php echo base_url();?>">
<meta property="og:type" content="website">
<?php


	$GroupName = $posts_item['Gr_Name'];
	if (strlen($GroupName)>=25) $GroupName = substr($GroupName,0,25)."... ";

	$TopicName = $posts_item['TopicName'];
	if (strlen($TopicName)>=25) $TopicName = substr($TopicName,0,25)."... ";

	$SubTopicName = $posts_item['SubTopicName'];
	if (strlen($SubTopicName)>=25) $SubTopicName = substr($SubTopicName,0,25)."... ";

	$Title = $posts_item['Po_Title'];
	if (strlen($Title)>=25) $Title = substr($Title,0,25)."... ";

	$Photo = "";
	if (strlen($posts_item['Po_Photo'])>0) $Photo = '<img class="img-fluid my-3" src="'.base_url(). $posts_item['Po_Photo'].'">';

	$TopicCrumb = '';
	if (strlen($TopicName)>0)	$TopicCrumb = '<li class="breadcrumb-item"><a href="'. base_url() .'account/view/topic/'.encode_id($posts_item['Po_To_ID']).'">'.$TopicName.'</a></li>';

	$SubTopicCrumb = '';
	if (strlen($SubTopicName)>0)	$SubTopicCrumb = '<li class="breadcrumb-item"><a href="'. base_url() .'account/subtopic/'.$posts_item['Po_St_ID'].'">'.$SubTopicName.'</a></li>';

	if($posts_item['Po_Ca_ID'] == 0){
		$catName = $posts_item['Po_Ca_Name'];
	}else{
		$catName = $posts_item['Ca_Name'];
	}
?>
	<!-- main content start here -->

	<main id="gp_main" class="gp_main container"  ng-app="myApp" ng-controller="myCtrl">



			<!-- Modal -->
			<div id="paymentpackageModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<div class="modal-content">
						<div class="modal-header">
						 	<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div  style="padding: 10px;">
							<div style="    display: grid;">
								<div>
									<a href="javascript:void(0 )">
										<div class="container-reward" style="float: right; ">
											<p style="margin-bottom : 0px; margin: 0px;
    margin-right: 6px;">Rewards</p>
											<img src="https://boardspeak.com/assetsofpop/img/icon-coin-no-border.svg" alt="">
										</div>
									</a>
								</div>

								<div>
									<h4 align="left" class="mt-2  mb-2" style="color:#008a00;" > Promote Post </h4> <br/>

								</div>
							</div>
							<div style="display: inline-flex;">


								<?php

								if(empty($posts_item['Po_Photo'])){ ?>

									<img src="<?php echo base_url().'img/banner/create-topic.jpg' ?>"  style=" width: 170px;
    border-radius: 10px;     height: 90px;"/>

								<?php }else{ ?>
									<img src="<?php echo base_url().$posts_item['Po_Photo'] ?>"  style=" width: 170px;
    border-radius: 10px;     height: 90px;"/>

								<?php }?>

									<h6 style="padding: 10px;" align="center"><?php echo $posts_item['Po_Title']; ?></h6>
							</div>

							<div style="background-color: #7cd2e0; padding: 40px 6px;;">
							<h3 align="center" style="color:#FFFFFF;">Boost your Brand</h3>
								<p align="center">
									Showcase your post in Explore Featured section
								</p>
								<?php
									if($this->session->userdata['logged_in']['bs_admin'] == 1 ){ ?>
								<div class="form-group" style="padding: 0px 33px;">
									<label id="newexpiry">Select Expiry</label>
									<input class="form-control"  min="<?php echo date('y-m-d'); ?>" id="birthdate" name="txtBirthDate" type="date"
										   value="<?php
													if(isset($promoteddata[0]['Pp_Expiry_Date'])){
														   echo $promoteddata[0]['Pp_Expiry_Date'];
													}else{
														  echo date('y-m-d');
													}
										   ?>"
										   placeholder="yyyy-mm-dd"  required="required"
										   data-validation-required-message="Please Enter Expiry Date.">

								</div>
								<?php } ?>
								<button id="PromotedPostHeading" style="
								margin-top: 0px!important;
								 	width: 85%;
								 	font-weight: bold;
									background: white;
									color: #28a745;
									border: 0px;
									margin: 35px;"
										onclick="selectPackage()" class="btn btn-info">	<?php
									if($this->session->userdata['logged_in']['bs_admin'] == 1 ){
										echo 'Promote Post';
									}else{
										echo 'Contact Us';
									}
									?></button>
							</div>
							<p align="center" style="    padding: 0px 76px;">By purchasing ad promotion above,
								you accept our Terms of Service</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>

		<div class="modal fade" id="angsendmessage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" style="background: white;flex-direction:column;">
				<div class="modal-content" style="border-radius:0px; max-height: 646px;
    overflow: scroll; overflow-x: hidden; ">
					<div class="modal-header"style="padding-bottom:0;">
						<h5 style="font-weight: 500;" class="modal-title" id="exampleModalLabel">Send Message / Reward to Respondents</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body" style="padding: 1rem 0;">

						<div style="  " class="container">
							<label>Subject : <font style="text-transform: capitalize;font-weight: 600;
    font-size: 1.3rem;
    color: #458db4;">{{polldata[0].PL_Question}}</font></label>
							</br>
							<label><input style="display: none;    margin: 10px;" type="checkbox" id="allcheckbox" onchange="angular.element(this).scope().selecteallx(this)">
								<b>Select responsdents with answers below:</b></label>

							<div ng-repeat="(key , review) in  reviwer ">
									<div ng-repeat="(key1 , subreview) in  review" >
										<div>

											<div class="row justify-content-around"style="flex-basis:content; margin-bottom:10px;"  >
												<div class="col " style="flex-basis:content;">
													<input style="margin: 10px;" id="index{{key}}" onchange="angular.element(this).scope().selecteall(this)" data-keyname="{{key1}}" type="checkbox" ><font style="font-weight: 600; color:black;">{{key1}}</font>
												</div>
												<div class="col"style="flex-basis:content;">
 													<a data-toggle="collapse" href="#multiCollapseExample1{{key}}"  href="javascript:void(0)" style="float: right" >Show users</a>
												</div>
											</div>
											<div class="progress mb-3">
												<div class="progress-bar" role="progressbar" aria-valuenow="{{cal_percentage(countobj(subreview), <?php echo  $totaldata ?>)}}"
													 aria-valuemin="0" aria-valuemax="100" style="width:{{cal_percentage(countobj(subreview), <?php echo  $totaldata ?>)}}%">
													{{cal_percentage(countobj(subreview), <?php echo  $totaldata ?>)}}%
												</div>
											</div>
											<style>
												.left-boxx{
													flex-wrap: nowrap;
													display:flex;
													align-items:center;
													justify-content: center;
													margin:0;
												}
												@media screen and (max-width:422px) {
													.left-boxx{
												
													align-items:flex-start;
													
												}
												.select-btn-div{
													/*position: relative;*/
    top: -33px;
    right: -110px;
												}
												.poll-users{max-height: 75px;}
												}
												@media screen and (max-width:373px){
													.select-btn-div{right:-88px}
												}
											</style>
											<div class="collapse multi-collapse" id="multiCollapseExample1{{key}}">
												<div class="card card-body" style="max-height: 200px; overflow: scroll; overflow-x: hidden;">
 													<ul style="list-style: none;"   ng-if="subreview.length > 0">
														<li ng-repeat="sub in  subreview" class="poll-users" style=" margin-top: 10px;  border-bottom: 1px solid #80808026;">
 																	<div class=" row " style="padding: 0px;justify-content:space-between;">
																	<div class="row left-boxx"  id="sidebarlefttop">
																		<div >
																			<img ng-if="sub.Us_Photo" class="send-poll-img" style="border-radius: 50%;" src="
																{{sub.Us_Photo}}">

																			<img ng-if="!sub.Us_Photo"  src="<?php   echo base_url() . 'img/nophoto.png'; ?>" class="ml-3" style="width: 45px; height: 45px; border-radius: 50%;" >
																		</div>
																		<div style=""  >
																			<strong><h6 class="ml-2" style="font-size: 14px; color: grey; margin:0;"  ng-if="sub.Us_Alias">{{sub.Us_Alias}}</h6></strong>
																			<strong><h6 class="ml-2" style="font-size: 14px; color: grey;margin:0;" ng-if="!sub.Us_Alias">{{sub.Us_Name}}</h6></strong>
																			<h6 class="ml-2" style="margin:0; width: 10ch;

  overflow: hidden;
  white-space: nowrap; font-size: 10px; color: grey;">{{sub.Us_JobTitle}}</h6>

																		</div>


																	</div>
																	<style>
																.modal img {
																	max-width:70px;
																}
																	.send-poll-img
																	{
																		min-width:70px;
																	min-height:70px;
																		max-width:70px;
																	max-height:70px;
																}
																
																@media screen and (max-width:500px) {
																	.send-poll-img{min-width:50px;
																	min-height:50px;
																   max-width:50px;
																	max-height:50px;
																
																}
																.modal img {
																	max-width:50px;
																}
																}
																</style>
																	<div id="sidebarleft" class="select-btn-div" style="display: flex;
    justify-content: left;
    align-items: center;
    margin-right: 1rem;
	margin-left:0;" >
																		<input type="hidden" id="subtext{{sub.Us_ID}}" value="{{sub.Poa_note}}">
																		<button
																				 ng-click="shownote(sub.Us_ID)"
																				 ng-show="sub.Poa_note != ''"
																				 class="btn"><font id="ghas" style="line-height: normal; color: #398cb3;">Show Note</font></button>

 																		<button ng-if="selectedbutton(sub.Us_ID) == true" class="add_member_btn added_member btn selectedbutton{{key}}" ng-click="removeselected(sub.Us_ID)" style=" color: #398cb3;"><i class="fas fa-check"></i> Selected</button>
																		<button ng-if="selectedbutton(sub.Us_ID) == false" class="add_member_btn add_member btn unselectbutton{{key}} " ng-click="addselected(sub.Us_ID)" style=" color: #398cb3;">Select</button>
																	</div>
															</div>



														</li>
													</ul>
													<ul ng-if="!subreview.length > 0" style="list-style: none;">
														<li style="text-align: center">
															No Record Found
														</li>
													</ul>
 												</div>
											</div>
										</div>
									</div>
								</div>
								<div style="flex-basis:content; margin-bottom:10px;" >
									<label><input style="    margin: 10px;" type="checkbox" id="unanswer" onchange="angular.element(this).scope().selecteallnon(this)">Send To Non Responsdent/s </label>
									<center ng-show="selected.length > 0"><span style="color:red"> You Selected {{selected.length}} User </span></center>
									<div class=" ">

										<div class="col-12   " style=" margin-top: 10px;">
											<input	type="hidden" id="totalcoucherremain" value="<?php
											if(isset($rewarddetails[0])){
												echo $rewarddetails[0]['Vp_vounchercount'];
											}
											 ?>"
											<div class="col-sm-12 col-md-12 col-lg-12 row" >

												<div style="width: 100%;"  class="dropdown mb-3" ng-show="selected.length > 0">
													<button style="width: 100%;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														Send Rewarsds
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a class="dropdown-item" href="javascript:void(0)"  onclick="openreward()">Send Reward Coins to Selected Recipient/s</a>
														<?php 	if(isset($rewarddetails[0])){ ?>
														<a class="dropdown-item" href="javascript:void(0)"  onclick="openvoucher()" >Send Reward Voucher/s to Selected Recipient/s</a>
														<?php } ?>

													</div>
												</div>
												<h6 ng-show="selected.length > 0">Create Message</h6>
												<textarea ng-show="selected.length > 0" rows="7" class="form-control" placeholder="Enter Message here" id="textareamessage">{{polldata[0].Pl_message}}</textarea>

												<div class="col-12 d-flex justify-content-center" ng-show="selected.length > 0">
												<div style="padding: 10px;">
													<button style="" class=" " ng-click="discardmessage(polldata[0].Pl_ID)">Discard Message</button>

												</div >
													<div style="padding:10px;">
													<button style="" class=" " ng-click="savemessage(polldata[0].Pl_ID)">Save Message</button>

												</div>
												</div>
												<button ng-show="selected.length > 0"  style="width: 100%;" class="btn btn-primary" ng-click="sendingmessage()">Send Message</button>
											</div>



											 </div>
 										</div>
								</div>
							</div>

					</div>
					<div class="modal-footer">
						<button type="button" style="background: transparent;
    color: black;
    border: none;" class="btn btn-secondary" ng-click="closemodel()" data-bs-dismiss="modal">Close</button>

					</div>
				</div>
			</div>
		</div>
	<input type="hidden" id="pendingrequest" value="<?php     if(isset($general_Count_postx)) echo $general_Count_postx; ?>">

	 <input type="hidden"  name="muteinfo" id="muteinfo" value="<?php if(isset($mutedata)){echo $mutedata;} ?>">
		<div>
	 		<div class="col-md-12">
			 	<nav aria-label="breadcrumb">
					<ul class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/view/group/<?php echo encode_id($posts_item['Gr_ID']);?>"><?=$posts_item['Gr_Name']?></a></li>
						<li class="breadcrumb-item text-primary"><?=$catName;?></li>
						<?php
						echo $TopicCrumb ;
						  ?>
						<li class="breadcrumb-item active" aria-current="page"><?=$Title?></li>
					</ul>
				</nav>

			</div>
	 		<div class="col-md-12">

				<section class="banner_desc">

					<!-- group desc section -->
					<div class="group_desc">
						<div class="group_desc_container container">

							<div class="group_text">
								<div style="flex-wrap:nowrap;" class="d-flex justify-content-between col-md-12 col-lg-12 group_container_info row mb-3"  >

								<div style="display: inline-flex;">
									<a class="group_name" href="<?php echo base_url() ?>u/<?php
									if(isset($lastUpdate[0]['Po_Us_ID'])   ){

										echo encode_id($lastUpdate[0]['Us_ID']);
									}else{

										echo encode_id($posts_item['Po_Us_ID'] );
									}

									?>" >
										<img id="newpicofuser" src="<?php
										if(isset($lastUpdate[0]['Us_Photo'])  ){
											if(!empty($lastUpdate[0]['Us_Photo'])){
												echo $lastUpdate[0]['Us_Photo'] ;

											}else{
												echo base_url() . 'img/nophoto.png';
											}
										}
										else{
											if(!empty($posts_item['Us_Photo'])){

												echo $posts_item['Us_Photo'] ;
											}else{
												echo base_url() . 'img/nophoto.png';
											}

										}
										?>" style="width:44px ;    height: 44px; border-radius:50%;"> </a>
									<p style="white-space:nowrap;" class="lead text-gray-med p-0 ml-2 mt-1 group_name"   >
										<a style="font-size: 17px!important;" href="<?php echo base_url() ?>u/<?php

										if(isset($lastUpdate[0])){

											echo  encode_id($lastUpdate[0]['Us_ID']);
										}else{

											echo encode_id($posts_item['Po_Us_ID'] );
										}


										?>" > <?php
											if(isset($lastUpdate) && !empty($lastUpdate)){

												 if(!empty($lastUpdate[0]['Us_Alias'])){
													echo $lastUpdate[0]['Us_Alias'];
												 }else{
													echo $lastUpdate[0]['Us_Name'];
												 }
											}else{


												if(!empty($posts_item['Us_Alias'])){
													echo $posts_item['Us_Alias'];
												}else{
													echo $posts_item['Us_Name'];
												}


											}
											?> </a>
										<?php
										//										if(  $member_info_post['role'] == 'superadmin') {
										if(isset($lastUpdate) && count($lastUpdate) != 0){
											echo "<span style='font-size:12px;'></br>  ".date("F d, Y h:i", strtotime($lastUpdate[0]['Pu_date']))."</span>";

										}else{

											echo "<span style='font-size:12px;'></br>  ".date("F d, Y h:i", strtotime($posts_item['Po_DatePosted']))."</span>";

										}

										?>
									</p>
								</div>
									<?php
											if(isset($this->session->userdata['logged_in'])){


									if( $this->session->userdata['logged_in']['bs_id'] == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin'|| $member_info_post['role'] == 'member' ) { ?>
									<div class="secondery_item" style="display: flex;align-items: center;margin-left: 37px;"
											<?php


											if(  $posts_item['Po_Privacy'] == 'private' && $creator_info['role'] == 'non_member'){?>
												style="display: none;"
											<?php }else{ ?>
												style="display: flex;"
											<?php }?> >
										<?php
										if (isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin') { ?>

										<a href="<?php echo base_url(); ?>account/postinfo/edit/<?php echo $EncodedID;?>"  class="title-tip title-tip-left" style="padding: 0px 11px;" title="Edit Post Info"     >

											<img style="    width: 24px;" src="<?php echo base_url() ?>assetsofpop/img/Pen-icon.png">

										</a>

										<?php } ?>

													<span class="secondery_btn">

<!--													<i class="fa fa-bars" style="-->
<!--													color: #388cb3;-->
<!--													font-size: 28px;-->
<!--													padding: 10px;" aria-hidden="true"></i>-->
														<img style="    width: 24px;"  src="<?php echo base_url() ?>assetsofpop/img/MorebIcon.png"  >
													</span>
										<ul class="secondery_dropdown expanded_dropdown" style="
										/*padding: 0px 22px!important;*/
">
											<li>
												<a href="" data-toggle="modal" data-target="#sharepostmodel" >
													Share
												</a>
											</li>
											<?php if( isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] ||    $creator_info['role'] == 'creator' ) { ?>

												<li>
													<?php if(isset($polldata) && !empty($polldata)){ ?>

														<?php if(isset($polldatacount) && !empty($polldatacount)){ ?>
															<a  href="javascript:void(0);"  onclick="deletepool()">Delete Poll</a>
														<?php }else{ ?>
															<a  href="javascript:void(0);"  onclick="editpool()">Edit Poll</a>
														<?php } ?>


													<?php }else{ ?>
														<a  href="javascript:void(0);"  onclick="createpool()">Create Poll</a>

													<?php }?>
												</li>
											<?php }?>
											<li>
												<a href="">
													<i class="far fa-star"></i>
													Add to favourites
												</a>
											</li>
											<li>
												<a href="">
													Pin
												</a>
											</li>
											<?php
											if (strpos($_SERVER['REQUEST_URI'], '/postinfo/') != 0) {
												echo '<li><a href="'. base_url() . 'account/postinfo/edit/'.$EncodedID .'">Edit</a></li>';
											}
											?>
											<li>
												<a href="">
													Post Settings
												</a>
											</li>
											<li>
												<a href="">
													Notify Me
												</a>
											</li>

										</ul>
									</div>
										<?php }} ?>
								</div>

							<h4 class="group_name"><a id="posttitle" style="color:#0D7EBB"; href="<?php echo base_url(); ?>account/view/post/<?php echo $posts_item['Po_Slug'];?>"><?php echo $posts_item['Po_Title'];?></a>

							</h4>
								<p class="group_metta">
									<span class="group_badge group_badge_<?=strtolower($posts_item['Po_Privacy'])?>"><?php echo ucfirst($posts_item['Po_Privacy']);?></span>
									<!--<span class="group_hearts">
										<i class="fas fa-heart"></i>&nbsp;<span id="counterheader"><?php echo $posts_item['Po_Likes'];?></span></span>-->

									<span class="group_memebers" style="font-size: 12px;">

										<?php echo $member_counter; ?> members
									</span>


								</p>

								<pre class="group_taggline" style="font-size: 16px!important;"><?=$posts_item['Po_Description']?></pre>
							</div>
							<div class="group_btn">
								<!-- only visible if board is followed -->

								<?php
								 if(!isset($this->session->userdata['logged_in']['bs_id'])){ ?>

								<div class="group_btn">
											<a href="<?php echo base_url(); ?>pages/view/signup">
												<button class="btn" type="button" style="
												<?php if( !isset($this->LoggedInUser)){ ?>
														background-color: #ffffff!important;
														color:#24be97;
													line-height: 30px!important;
												<?php }?>
											">
													Login to Join
												</button>
											</a>
										</div>
								 <?php }else{ ?>


								<?php  if ($member_info_post['status'] == 'success' || $this->session->userdata['logged_in']['bs_id'] == $posts_item['Po_Us_ID']) { ?>
									<?php if ($member_info_post['is_invited'] && ($member_info_post['role_status'] == 'pending')) { ?>
										<!--<a class="action_button invite_response_btn btn btn-xl btn-danger mb-2 mr-2 mt-2" href="javascript:void(0)" data-action="accept">Accept</a>
										<a class="action_button invite_response_btn btn btn-xl btn-light mb-2 mr-2 mt-2" href="javascript:void(0)" data-action="decline">Decline</a>
										<a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a> -->
									<?php } else { ?>
										<?php if($posts_item['Po_Privacy'] == 'private'){ ?>
												 <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2 <?php echo ($member_info_post['role_status'] == 'pending') ? 'btn_pending' : 'invite_member_btn'; ?>" href="javascript:void(0)"><?php echo ($member_info_post['role_status'] == 'pending') ? 'Pending' : 'Invite  <li class="fa fa-plus" style="margin-left: 4px;"></li>'; ?></a>

												<?php } ?>
									<?php

											 if($this->session->userdata['logged_in']['bs_id'] == $posts_item['Po_Us_ID']
													 || $member_info_post['role'] == 'superadmin'  ) { ?>
									<div class="admin-form-group row">

														<?php if($promoted == '1'){ ?>
															<div id="dropding" class="dropdown">
															<button href="javascript:void(0)" id="promotebutton" class="btn btn-xl btn-success mr-2 ml-3  dropdown-toggle" type="button" data-toggle="dropdown"

															   style="background: white;  color: #398cb3; line-height: 19px !important;"> <?php
																	echo 'Promoted </br> <font style="font-size: 14px;">Expired at '. $promoteddata[0]['Pp_Expiry_Date'] .'</font>';

																 ?> 	<span class="caret" style="padding: 0; margin: 0;"></span></button>
																<ul  class="dropdown-menu" style="background: white; padding: 10px;">
																	<li class="promotedrop"><a href="#"  onclick="makePostextendpromote()">Select New Expiry Date</a></li>
																	<li class="promotedrop"><a href="#" onclick="EndPostPromote()">End Post Promotion</a></li>

																</ul>
															</div>
															<style>
																.promotedrop:hover {
																	background: #80808038;
																}
															</style>
														 <?php }else{ ?>
															<a href="javascript:void(0)" id="promotebutton" class="btn btn-xl btn-success mr-2 ml-3"
															   onclick="makePostPromote()"
															   style="background: white;  color: #398cb3; line-height: 19px !important;">
																		Promote
																 </a>
														 <?php }?>
<!--

										<div class="dropdown">
											<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
												<span class="caret"></span></button>

										</div>

 <!--                     					<button class="btn btn-outline-primary" onclick="createpool()">Create Poll</button>-->
										<div class="secondery_item" style="display: none;">
													<span class="secondery_btn">
													<i class="fa fa-bars" style="color: #388cb3;" aria-hidden="true"></i>
													</span>
											<ul class="secondery_dropdown expanded_dropdown" style="padding: 0px 22px!important;">
												<li>
													<a href="" data-toggle="modal" data-target="#sharepostmodel" >
														Share
													</a>
												</li>
											<?php if( isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] ||    $creator_info['role'] == 'creator' ) { ?>

													<li>
														<?php if(isset($polldata) && !empty($polldata)){ ?>

															<?php if(isset($polldatacount) && !empty($polldatacount)){ ?>
															<a  href="javascript:void(0);"  onclick="deletepool()">Delete Poll</a>
															<?php }else{ ?>
															<a  href="javascript:void(0);"  onclick="editpool()">Edit Poll</a>
														<?php } ?>


													<?php }else{ ?>
														<a  href="javascript:void(0);"  onclick="createpool()">Create Poll</a>

													<?php }?>
												</li>
											<?php }?>
												<li>
													<a href="">
														<i class="far fa-star"></i>
														Add to favourites
													</a>
												</li>
												<li>
													<a href="">
														Pin
													</a>
												</li>
												<?php
												if (strpos($_SERVER['REQUEST_URI'], '/postinfo/') != 0) {
													echo '<li><a href="'. base_url() . 'account/postinfo/edit/'.$EncodedID .'">Edit</a></li>';
												}
												?>
												<li>
													<a href="">
														Post Settings
													</a>
												</li>
												<li>
													<a href="">
														Notify Me
													</a>
												</li>
												<li>
												<?php
												if (isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin') { ?>

													   <a href="<?php echo base_url(); ?>account/postinfo/edit/<?php echo $EncodedID;?>" id="editpostinfobutton"    >Edit Post Info</a>

											<?php } ?>
												</li>
											</ul>
										</div>
                     				 </div>
												 <style>
													 ul.secondery_dropdown.expanded_dropdown li {
														 border-bottom: 1px solid #80808030!important;
														 padding-top: 10px!important;
													 }
													 
												 </style>
                     				 <?php } ?>
									<?php } ?>
								<?php } else { ?>
<!--here-->
										<?php  if( $this->session->userdata['logged_in']['bs_id'] == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin'|| $member_info_post['role'] == 'member' ) { ?>

										 <div class="secondery_item"
 											 <?php


											  if(  $posts_item['Po_Privacy'] == 'private' && $creator_info['role'] == 'non_member'){?>
												  style="display: none;"
											 <?php }?>
											  >
													<span class="secondery_btn">
													<i class="fa fa-bars" style="color: #388cb3;" aria-hidden="true"></i>
													</span>
											 <ul class="secondery_dropdown expanded_dropdown" style="padding: 0px 22px!important;">
												 <li>
													 <a href="" data-toggle="modal" data-target="#sharepostmodel" >
														 Share
													 </a>
												 </li>
												 <?php if( isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] ||    $creator_info['role'] == 'creator' ) { ?>

													 <li>
														 <?php if(isset($polldata) && !empty($polldata)){ ?>

															 <?php if(isset($polldatacount) && !empty($polldatacount)){ ?>
																 <a  href="javascript:void(0);"  onclick="deletepool()">Delete Poll</a>
															 <?php }else{ ?>
																 <a  href="javascript:void(0);"  onclick="editpool()">Edit Poll</a>
															 <?php } ?>


														 <?php }else{ ?>
															 <a  href="javascript:void(0);"  onclick="createpool()">Create Poll</a>

														 <?php }?>
													 </li>
												 <?php }?>
												 <li>
													 <a href="">
														 <i class="far fa-star"></i>
														 Add to favourites
													 </a>
												 </li>
												 <li>
													 <a href="">
														 Pin
													 </a>
												 </li>
												 <?php
												 if (strpos($_SERVER['REQUEST_URI'], '/postinfo/') != 0) {
													 echo '<li><a href="'. base_url() . 'account/postinfo/edit/'.$EncodedID .'">Edit</a></li>';
												 }
												 ?>
												 <li>
													 <a href="">
														 Post Settings
													 </a>
												 </li>
												 <li>
													 <a href="">
														 Notify Me
													 </a>
												 </li>
											 </ul>
										 </div>
										<?php } ?>
									<?php if($posts_item['Po_Privacy'] != 'public'){
 											 ?>

											<?php
 											 if($general_Count > 0 && $userType == 'non_member'){ ?>
											 <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2"   href="javascript:void(0)">Pending</a>

											 <?php } else{ ?>
											<?php if($general_Count == 0 && $userType == 'non_member') {
 											?>
 												<?php if( isset($inviteID) && $inviteID == 'member'){ ?>
												 <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2" id="btn_join" href="javascript:void(0)">Join</a>
 											<?php }else if(isset($inviteID) && $inviteID  == 'follower'){ ?>

															<?php }else{ ?>
												 <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2" id="btn_join_without_invite" href="javascript:void(0)">Join</a>
  												<?php } ?>
 											<?php }else{ ?>
											<?php if($member_info_post['role_status'] != 'pending' && $posts_item['Po_Privacy'] == 'private'){ ?>
                                             <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2  invite_member_btn" href="javascript:void(0)">  Invite  <li class="fa fa-plus" style="margin-left: 4px;"></li> </a>

                                            	 <?php } ?>


											<?php }?>

												<?php } ?>

									<?php } ?>
								<?php } ?>
 						<?php }  ?>

							</div>

							<div class="col-md-12 col-lg-12 px-0 py-3 group_container_info">

							 
								<p class="lead text-gray-med p-0 m-0 mt-1" <?php

								if(isset($creator_info)){


				   					if(  $posts_item['Po_Privacy'] == 'private' && $creator_info['role'] == 'non_member'){?>
									style="display: none;"
								<?php }}?>>
								<?php
									if (isset($this->LoggedInUser) && strpos($_SERVER['REQUEST_URI'], '/postinfo/') === false) {
										echo '<a class="editoption <?php echo $ShowAdminAction;?>" href="'.base_url().'account/postinfo/detail/'.$EncodedID.'">Post Info</a>';
									}
								?>
								</p>


							</div>
						</div>
					</div>
<!--					--><?php // if (isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID']
//							|| $member_info_post['role'] == 'superadmin') { echo 'd-flex justify-content-center';
//					}else{ echo 'd-flex justify-content-center'; }?>
					<!-- banner section -->
					<div class="gp_banner">

						<input type="hidden" id="userdatalist" value="<?php if(isset($rewarddetails[0]['Vp_radeemprice'])){
							echo $rewarddetails[0]['Vp_radeemprice'];
						} ?>">
						<?php

						if(isset($rewarddetails[0]['Vp_radeemprice'])) {?>
						<div class=" "

							 style="border: 0px;
										display: inline-flex;
										background: #ffc107;
										width: 100%; border-radius: 10px;
										margin-bottom: -20px;
										/* padding: 8px 0 0 3px; */
										padding: 0px 0 4px 3px;
										align-items: center;
										">


							<div class="justify-content-center" style="display: inherit; width: 90%;">
							<center style="display: inherit">
								<img style="width: 22px; height: 22px;    margin-right: 5px;
    margin-top: 10px;position:relative;top:-3px;" src="<?php echo base_url() ?>assetsofpop/img/giftbox.png">
								<span style="
										font-size: 17px; margin-top: 7px;
										font-weight: 700;
										display: -webkit-box;
										color: #067b8e;
											"> REDEEM FOR <?php print_r($rewarddetails[0]['Vp_radeemprice']); ?>
									<img style="width: 25px; margin-left: 4px; margin-right: 4px;    " src="<?php echo base_url() ?>assetsofpop/img/icon-coin-no-border.svg"> COINS </span>

							</center>
							</div>

							<?php  if (isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID']
									|| $member_info_post['role'] == 'superadmin') {?>
							<label title="Edit Reward Voucher" class="title-tip title-tip-left">
								<img  style="width:40px; height: 40px;position: relative;
    bottom: -8px;" ng-click="editReward()"  src="<?php echo base_url() ?>assetsofpop/img/White-edit-icon2.png">

							</label>
							<?php } ?>
						</div>
						<?php } ?>
					 	<div class="banner_container"  >

							<div class="col-md-12 col-lg-12 board-banner text-right bg-topic post-cover-image <?php echo $posts_item['Po_Backcolor']; ?>"
								<?php


								if(strlen($posts_item['Po_Photo'])>0) echo 'style="box-shadow: 1px 0px 5px 3px #80808033;background:transparent url('.base_url() . str_replace(base_url(), '', $posts_item['Po_Photo']).') no-repeat center center /cover; background-size: 100% 100%;"';?> >
								<?php if($posts_item['Po_Pin'] == 1){?>
								<?php 								if (isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin') {
									?>
									<i class="PinnedClass fas fa-thumbtack" onclick="changepinstatus(0)" style=" position: absolute;
										left: 0;
										
										color: rgb(247 182 35);
										padding: 7px;
										z-index: 11;
										background: rgb(69 141 180 / 63%);
										border-radius: 4px;
										cursor: pointer;">

										<font style="color:white;text-transform: uppercase;
    font-family: monospace;
    font-size: 0.9rem;
    font-weight: 500;">Pinned</font>

									</i>

							<?php } 	}else{ ?>
									<?php 								if (isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin') {
										?>
									<i class="PinnedClass fas fa-thumbtack" onclick="changepinstatus(1)" style="  position: absolute;
    left: 0;
    
    padding: 7px;    z-index: 11;
    background: rgb(69 141 180 / 63%);
    color: white;
    border-radius: 4px;
    cursor: pointer;" > <font  style="color:white;text-transform: uppercase;
    font-family: monospace;
    font-size: 0.9rem;
    font-weight: 500;">Pin</font></i>

							<?php 	}  } ?>
								<div class="col-md-12 col-lg-12" style="height: 100%;">
									<?php if( isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin' ) { ?>
									<div class="btn btn-primary mt-3 btn-upload-cover-image" style="
									   background: #7476779e!important; border-radius: 9px!important;

                                                                                                                                                                                                     /*border: 2px solid white!important;*/
                                                                                                                                                                                                     /*border-radius: 0px!important;*/
                                                                                                                                                                                                     /*color: white!important;*/
                                                                                                                                                                                                     font-weight: 600!important;">
										<form method="post">

											<label  class="title-tip title-tip-left"  title="Change Board Cover" for="upload_image" style="margin-bottom:0;">
												<img src="" id="uploaded_image" class="img-responsive img-circle" />
												<i class="fas fa-camera "></i>
												<input type="file" name="image" class="image" id="upload_image" style="display:none" accept="image/*" />
											</label>

										</form>
									</div>	<?php } ?>
							    </div>
							</div>

							<div class="group_btn" <?php
								if(isset($creator_info)){


							if(  $posts_item['Po_Privacy'] == 'private' && $creator_info['role'] == 'non_member'){?>
								style="display: none;"
							<?php } } ?>>

								<span class="group_hearts" onclick="counter_plus()">
									<?php
									if(isset($userPostLike) && $userPostLike == 1){ ?>
										<i  class="changeheart fa fa-heart"  style="color:#06cbd1; font-size:23px; "></i>
	                                 <?php 	}else{ ?>
	                                 <i class="changeheart fa fa-heart-o" style="color:#06cbd1; font-size: 23px;"></i>
	                                 <?php }
									?>


									<?php if($posts_item['Po_Likes'] >= 0){ ?>

									<font id="postcounter" style="font-size:14px;"><?php echo $posts_item['Po_Likes'] ;?></font>
									<?php } ?>

								</span>
								<?php
 									 if (!empty($posts_item['Po_Bn_ID']) || !empty($posts_item['Po_Bn_Name'] ) || !empty($posts_item['Po_Bn_Page']) ) { ?>
											<!-- <a class="btn invite_member_btn btn post_action_button" href='#' data-toggle='modal' data-target="#external_link_modal"></a> -->
										 <a class="btn btn-success" style=" line-height: 25px!important; border-color: #388cb3!important;  background: #388cb3!important" target="_blank"  href="<?php echo $posts_item['Po_Bn_Page']; ?>" ><?=((!empty($posts_item['Po_Bn_ID'])) ? $posts_item['Bn_Caption'] : $posts_item['Po_Bn_Name'])?>
										 </a>

                                 <?php } ?>
								<span class="group_hearts"  onclick="make_plus()">
								<?php
								if($userfav == 1){ ?>
									<i class="changefav  fa fa-star" style="color:#ffc107; font-size: smaller"></i>
								<?php 	}else{ ?>
									<i class="changefav  fa fa-star-o" style="color:#ffc107; font-size: smaller"></i>
								<?php }
								?>

<!--									<i class="fa fa-star" style="color:#ffc107"></i>-->

<!--									empty-->

								</span>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>




		</div>
		</div>
		<div id="sharepostmodel" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">

						<h6 class="modal-title">Share post </h6>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<label>Share <font style="color:lightblue; font-weight: bold;"><?php echo $posts_item['Po_Title'];?></font>
							post</label>
						<div style="margin-bottom:10px"
							 data-url="<?php echo base_url(); ?>account/view/post/<?php echo  $posts_item['Po_Slug'];?>"
							 class="addthis_inline_share_toolbox"
							 data-title="<?php echo $posts_item['Po_Title'];?>" ></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
		<style>
			.roundimage {
				width: 50px;
				border-radius: 50%;
			}
			.headingtext{
				font-size: 15px;
				margin-top: 3px;
			}

			.graycolor{
				font-size: 12px;
				color: grey;
				margin-top: -8px;
			}
			.title-instant-reward {
				margin-top: 10px;
				font-size: 28px;
				font-weight: 600;
				margin-bottom: 0px;
				color: #6312a0;
			}
			.desc-instant-reward {
				font-size: 22px;
				font-weight: 400;
			}

			.title-form-instant-reward {
				font-size: 22px;
				font-weight: 500;
			}
			.instant-reward-bg {
				display: flex;
				flex-direction: column;
				justify-content: space-between;
			}

			@media screen and (max-width: 995px){
				.bgz {
					height: 300px!important;
				}
				.instant-reward-bg img{
					margin-top: 0px;
				}

				#ghas{
					margin-left: 15px;
				}
			}
			@media screen and (min-width: 700px){
				#sidebarlefttop {
					margin-left: 2%;
					width: 21%!important;
					overflow: hidden;
				}
				#sidebarleft{
					margin-left: 52%;
				}

			}

			  .bgz {
				  height: 100%;
				  border-radius: 12px;
				color: white;
				padding: 40px;
				text-align: center;

				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
			}

		</style>

		<div id="coinRewardModal" class="modal fade" role="dialog" style="overflow-x: hidden;
    overflow-y: auto;">
			<div class="modal-dialog modal-lg" >
				<button style="   margin-top: -16px;
    position: absolute;
    z-index: 9;
    right: 0;
    padding: 7px;
    background: #2b2f33;
    border-radius: 50%;
    color: white;
    right: -16px;
    font-size: 25px;
    padding: 5px 10px;" type="button" class="close" data-dismiss="modal">&times;</button>
				<!-- Modal content-->
				<div class="modal-content">
					<style>
						.bonus-coin-bg {
							display: flex;
							flex-direction: column;
							justify-content: end;
							background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-redeem-coin-modal.png);
						}
					</style>
					<div class="modal-body" style="padding: 1px!important;">
						<div class="row" style=" ">
							<div id="rewardpurcase" style="display: none;" class="col-xs-12 col-md-5">
								<div class="bonus-coin-bg bgz" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-credit-coin-modal.png); background-color: #D2F324; ">
									<p style="font-size: 26px; font-weight: 600;">INSTANTLY REWARD
										your members</p>
									<p style="font-size: 16px; font-weight: 400;">with our automated tracking /selection
										& coin wallet system.</p>
								</div>
							</div>
							<div id="instatnt" class="col-xs-12 col-md-5">
								<div class="instant-reward-bg bgz" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-modal-instant-reward.png); ">
									<div>
										<p style="font-size: 30px;

    font-weight: 600;">Instant Rewards Boosts Motivation</p>
										<img style="margin-top:20px;" src="<?php echo base_url(); ?>assetsofpop/img/img-modal-instant-reward.png" alt="modal instant reward">
									</div>
									<p style="margin: 0px; font-size: 24px;

    font-weight: 500;">Increase member's
										positive experience
										on every task!</p>

								</div>
							</div>
							<div class="col-xs-12 col-md-7" style="padding: 17px;" >
								<!-- exit button -->

								<div style="display : flex ; align-items : center ; flex-direction: column; ">

 									<?php

									if(isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ ?>
										<img class="avatar roundimage" src="<?php echo $this->session->userdata['logged_in']['Us_Photo']; ?>">
									<?php }else{ ?>
										<i class="fas fa-user-circle"></i>
									<?php } ?>
									<h2 class="avatar-name-text headingtext" >

										<?php print_r($this->session->userdata['logged_in']['bs_name']); ?>
									</h2>
									<p class="avatar-job-text graycolor"><?php echo $this->session->userdata['logged_in']['bs_alias']; ?></p>
									<div id="preview" class="modal-contents"  style="width: 100%; display: none;">
 										<div class="col-sm-12">
											<img style="         max-width: 100%!important;
    width: 100%;
    height: 220px;" class="" src="<?php echo base_url(). $posts_item['Po_Thumb'] ; ?>">

										</div>
										<!-- <div align="center">
											<?php

											// if(isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ ?>
												<img class="avatar roundimage" src="<?php 
												// echo $this->session->userdata['logged_in']['Us_Photo']; 
												?>">
											<?php 
										// }else{ 
											?>
												<i class="fas fa-user-circle"></i>
											<?php 
										// }
										 ?>
											<h2 class="avatar-name-text headingtext" >

												<?php
												//  print_r($this->session->userdata['logged_in']['bs_name']);
												  ?>
											</h2>
											<p class="avatar-job-text graycolor"><?php
											//  echo $this->session->userdata['logged_in']['bs_alias']; 
											 ?></p>

										</div> -->
										<!-- ahsan-header -->
										<div class="mt-3">
			
			
			</div>
		
			<div class="p-2" style="     background-image: linear-gradient(to bottom right, #0542c3, #10b647);
						padding: 10px;
						margin: 10px;
						border-radius: 10px;">
				<div>
					<h3 align="center" style="font-size: 2.5rem;
    color: white;
    margin-top: 9px;
    margin-bottom: 16px;font-size: 1.5rem;" id="vouchertitletext"></h3>
					<h6 align="center" style="font-weight: 300;
    				text-transform: uppercase; color:white;font-size: 1.1rem;" id="voucherprice">On your next purchase</h6>

					<div class="d-flex justify-content-center" style="    background-image: url(http://localhost/boardspeak_2020/assetsofpop/Couponbackground.png);
						height: 170px;
						padding-top: 36px;
						background-size: 100% 162px;
						background-repeat: no-repeat;">
						<!--							--><?php //  if($vou['Vr_Status'] == 'purchased' || $vou['Vr_Status'] == 'used'){ ?>
						<button id="buttonpurchaseused" class="btn btn-info" style=" display: grid;
								 border: 0px; background: transparent; color:black;">
							<font style="    font-size: 19px;
										font-weight: 900;
										color: grey;
										padding: 0px;
										margin: 0px;
										text-transform: uppercase;">Voucher Code <br> will be auto generated </font>
							<font style="font-size:25px;"
											  id="vouchervodenumber"></font><font
									style="    font-size: 21px;
										font-weight: 900;
										color: #349671;
                                        font-weight: 700;
										padding: 0px;
										margin: 0px;" id="valueatt">
							</font></button>
						<!--							--><?php //}else{ ?>
						<!-- <button class="btn btn-info" style="background: transparent;
						color: black; border:0px;
						 " id="buttonnopuchase">Voucher
						</button> -->
						<!--							--><?php //} ?>

					</div>
					<!-- <div style="    padding: 10px;
						background: #d3c9c9;
						margin-top: 10px;
						border-radius: 10px; "
					>
						<div  class="d-flex justify-content-center">
							<h5 style="font-weight: 300" id="voucherdeadtime">20 / 16 / 2022</h5>
						</div>
						<div  class="d-flex justify-content-center">
							<h6 style="font-weight: 300" id="validtext"></h6>
						</div>
					</div> -->
				
					<div class="rect-date">
						<p style="font-size:11px;font-weight:600;margin:0;">Offer Valid Till</p>
						<div style='display:flex;'>
						<div class="month rectt">
								<span class="lett month0">3</span>
								<span class="lett  month1">2</span>
							</div>
							<div class="date rectt">
								<span class="lett day0">1</span>
								<span class="lett day1">1</span>
							</div>
							<div class="year rectt">
								<span class="lett year0">2</span>
								<span class="lett year1">0</span>
								<span class="lett year2" >2</span>
								<span class="lett year3">2</span>


							</div>
						</div>
					</div>
					<style>.rect-date {
							flex-direction: column;
							background-color: #70B8A3;
							display: flex;
							align-items: center;
							justify-content: center;
							/* padding: 20px; */
							height: 105px;
							width: 269px;
							margin: auto;
							font-family: 'Poppins', sans-serif;
							border-radius: 4px;
						}

						.rectt {
							display: flex;
							margin: 5px;
							font-size: 3rem;

						}

						.lett {
							background-color: white;
							margin: 1px;
							border-radius: 2px;
							width: 27px;
							text-align: center;
							/* padding: 4px; */
							height: 59px;
							display: flex;
							align-items: center;
							justify-content: center;
						}
						#radeemvoucherbutton:hover{
background:rgb(54 177 176);
						}</style>
				</div>
										</div>
										<p style=" font-size:1.3rem;text-align:center;padding-top:20px;color:gray;">Terms & Conditions</p>
											<p id="showdescription" style="   padding: 10px 33px;
						/* min-height: 150px!important; */
						overflow: scroll;
						overflow-x: hidden;
						max-height: 100px;
						line-break: anywhere;
    margin-bottom: 31px;
						/* margin-top: 22px; */
						"></p>
									<div style="padding: 0px 41px  43px 41px;    display: flex;
    justify-content: center;
">
											<button  class="btnx btn-lg" ng-click="createvoucher()">Send Voucher</button>
											<button  type="button" class="btnx btn-primary-outlinex btn-lg"  onclick="showvoucher()">Cancel</button>
										</div>
									</div>
									<div id="modalInstantRewardCointCredit" class="modal-contents"
										 style="width: 100%; display: none;">
										<h2 class="title-instant-reward" align="center" style="color: #FCB731; margin-top: 20px;">
											Pay with Coin Credits
										</h2>
										<p class="desc-instant-reward" align="center" style="color: #1F62AE; margin-top: 20px;">Your Total Reward Coins Offer:</p>

										<div class="coin-input-value d-flex justify-content-center">
											<div style="display: flex;
    background: #f2f1f1;
    padding: 5px 13px;
    border-radius: 50px;">
												<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 25px;" >
												<p style="margin-left: 20px; color: #FCB731; margin-bottom: 0px; font-size: 18px; font-weight: 600; " id="totalsendingValue">1000</p>
											</div>
										</div>
										<style>
											.coin-input-value {
												border-radius: 28px;
												/*background-color: #f2f1f1;*/
												position: relative;
												display: inline-flex;
												flex-direction: row;
												padding: 5px 20px;
											}
										</style>
										<div style="width : 100% ; margin-top: 20px;">
											<p class="title-form-instant-reward" style="color: #1F62AE;" >My Coins</p>
											<div style=" display: flex; flex-direction: row; ">
												<p> available balance: </p>
												<div>
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 25px; margin-left: 20px;" >
												</div>
												<p style="color: #ECCD67; margin-left: 10px;" id="currentbalance"> 700 </p>
											</div>
											<p style="font-size: 12px; color: #009344;
											 margin-top: 10px;
   													 margin-bottom: 51px;"> 500  Coins will expire in [date one year after]</p>
										</div>
										<div id="proceedpayment">
											<h6 align="center">Account balanace will be <font id="remaningcredit"></font> coins</h6>
											<div class="d-flex justify-content-center">
												<div >
													<button class="btnx btn-lg" style="    font-size: 16px!important;
    text-transform: capitalize;"  ng-click="proceedAmmount()">Proceed</button>
												</div>
												<div  >
													<button class="btnx btn-primary-outlinex btn-lg" style="    font-size: 16px!important;
    text-transform: capitalize;" onclick="showfron()">cancel</button>
												</div>
											</div>
										</div>
										<div id="buynowcoin">
											<p style="font-size: 14px; margin-top: 20px; margin-bottom: 5px;">Your available coin credits is not enough.</p>
											<p style="font-size: 20px; font-weight: 600; color: #088B34;">You are short by <font class="shortcoins"></font> coins.</p>

											<div class="d-flex justify-content-center" style="display: none">
												<div style="display: inline-flex; flex-direction: row; align-items: center;  padding: 10px; border-radius: 10px; background-color: #FCB731;">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="width: 50px;" >
													<div style="margin: 0px 15px; height: 50px; width: 2px; background-color: #231F20;"></div>
													<p style="margin-bottom: 0px; font-size: 22px; font-weight: 700;" class="shortcoins" id="shortcoins"></p>
												</div>
											</div>

											<div class="row" style="margin-top: 30px; justify-content: center; ">
												<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
													<button type="button" onclick="purchasecoin()"  class="btns btn-primary btn-lg">Buy Coins</button>
												</div>
												<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
													<button type="button" onclick="showfron()" class="btn btn-primary-outline btn-lg"  >Cancel</button>
												</div>
											</div>
										</div>

										<a href="#" style="  bottom: 0;">
											<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.jpg" alt="ad banner"  style="     height: 84px; width: 100%; margin-top: 20px; " >
										</a>
									</div>
									<!-- content modal instant reward  -->
									<div id="modalInstantvoucher" class="modal-contents" >
										<h2 align="center" class="title-instant-reward">
											  Send Reward Voucher Code
										</h2>
										<p class="desc-instant-reward" style="color: #a5a5a5;" align="center">Boost Customer Traffic</p>

										<!-- form -->
										<form >
											<div style="width: 100%; margin-top: 10px;" class="from-input">

												<p class="title-form-instant-reward ">Reward Voucher Offer Main Title</p>
															<label>Title</label>
												<input type="hidden" id="voucherpostID"
													   value="<?php if(isset( $rewarddetails[0]['Vp_id']))
													   {  echo  $rewarddetails[0]['Vp_id'];
													   }; ?>" >
													   
												<input type="text" id="vouchertitle" disabled
													   value="<?php if(isset( $rewarddetails[0]['Vp_title']))
													   {  echo  $rewarddetails[0]['Vp_title'];
											 }; ?>
"								  required class="form-control" placeholder="Ex.  Get P500 cashback">
								<input type="text" id="vouchersubtitle" disabled
													   value="<?php if(isset( $rewarddetails[0]['Vp_Sub_title']))
													   {  echo  $rewarddetails[0]['Vp_Sub_title'];
											 }; ?>
"								  required class="form-control" placeholder="Sub Title .">


												<style>
													.label-inputx {
														font-size: 18px;
														font-weight: 500;
														color: #1f62ae;
													}
													.container-form-input {
														padding: 5px;
														background-color: white;
														box-shadow: 0 3px 10px rgb(0 0 0 / 20%);
														margin-right: 20px;
														border-radius: 5px;
														display: flex;
														flex-direction: row;
														align-items: center;
														margin-bottom: 10px;
													}
													.container-form-input .placeholder {
														margin-left: 30px;
														font-size: 18px;
														font-weight: 500;
														margin-bottom: 0px;
													}

													div#coinRewardModal p {
														margin-top:0px;
														margin-bottom: 0px;
													}
													@media screen and (max-width: 995px){
														.bgz {
															height: 500px;
														}
													}

												</style>
												<!-- form input -->
												<div>
													<p class="label-inputx">Number of Voucher Recipients</p>
													<div class="container-form-input">
														<div style="position: relative;">
															<input
																	onchange="voucherperperson()"
																	type="number"
																	id="voucherpersion"
																	value="0"
																	disabled
																	required
																	class="form-control"
																	style="width: 160px; padding-left: 40px;"
															>
														</div>
														<p class="placeholder"> Recipient/s</p>
													</div>
												</div>
												<div>
													<p class="label-inputx">Value of Each Voucher (in PhP)</p>
													<div class="container-form-input">
														<div style="position: relative;">
															<input
																	onchange="voucherperperson()"
																	type="number"
																	id="paisaammount"
																	min="100"
																	disabled
																	value="<?php if(isset( $rewarddetails[0]['Vp_paisaammount']))
																	{  echo  $rewarddetails[0]['Vp_paisaammount'];}?>"
																	required
																	class="form-control"
																	style="width: 160px; padding-left: 40px;"
															>
															<p  style="position: absolute; top: 5px; left: 10px;  width: 25px;" >₱</p>
														</div>
														<p class="placeholder"> per voucher </p>
													</div>
												</div>

												<!-- form input -->
												<div>
													<p class="label-inputx">Voucher Validity (End Date)</p>
													<div class="container-form-input">
														<input type="date" disabled value="<?php
														if(isset( $rewarddetails[0]['Vp_deadline']))
														{  echo  $rewarddetails[0]['Vp_deadline'];}?>" id="date" required>
														<p class="placeholder">Enter date</p>
													</div>
												</div>

											</div>

											<p class="desc-instant-reward" style="color: #FCB731; margin-top: 15px;
										   text-align: center;    text-align: center;">Participants will redeem each voucher with coins:</p>
											<div style="position: relative;">
												<div class="d-flex justify-content-center">

													<div class="input-group mb-3 d-flex justify-content-center">
														<div class="input-group-prepend">
															<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style=" width: 40px;
    padding: 7px; " >
														</div>
														<input type="number" disabled 	min="5"  value="<?php if(isset( $rewarddetails[0]['Vp_radeemprice'])){  echo  $rewarddetails[0]['Vp_radeemprice'];}?>" id="totalredeempay"
															   style="width: 200px;
													 padding-left: 40px;
													  border: 1px solid #80808063;
    border-radius: 6px;
    padding: 0px 10px;"    >
													</div>


												</div>
											</div>
											<style>
												.btnx {
													background-color: #1f62ae !important;
													padding: 10px 25px;
													font-size: 12px !important;
													background-image: none !important;
													font-weight: 500 !important;
													border: none !important;
													font-size: 14px;
													font-weight: bold;
													border-radius: 10px;
													color: white;
												}

												.btn-primary-outlinex {
													/*border: 2px solid #1f62ae !important;*/
													background: white!important;
													color: #1f62ae!important;
												}
											</style>
											<div class="col-sm-12">
												<label class="label-inputx">Please indicate here your Redemption Terms & Conditions:</label>
												<textarea class="form-control" required id="decription"><?php if(isset( $rewarddetails[0]['Vp_decription'])){  echo  $rewarddetails[0]['Vp_decription'];}?></textarea>
											</div>
											<div class="row" style="margin-top: 20px; justify-content: center; ">
												<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
													<span   onclick="showdetailsofvoucher();" class="btnx btn-lg" >Preview Voucher</span>
												</div>
												<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
													<button type="button" class="btnx btn-primary-outlinex btn-lg" onclick="showanotherpopup()" data-dismiss="modal">Cancel</button>
												</div>
											</div>

										</form>
										<a href="#">
											<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.jpg" alt="ad banner"  style=" height: 60px; width: 100%; margin-top: 20px; " >
										</a>
									</div>
									<!-- content modal instant reward  -->
									<div id="modalInstantReward" class="modal-contents" >
										<h2 align="center" class="title-instant-reward">
											Instant Rewards
										</h2>
										<p class="desc-instant-reward" style="color: #a5a5a5;" align="center">boost motivation!</p>

										<!-- form -->
										<div style="width: 100%; margin-top: 10px;" class="from-input">
											<p class="title-form-instant-reward ">What is your Reward Offer?</p>
											<style>
												.label-inputx {
													font-size: 18px;
													font-weight: 500;
													color: #1f62ae;
												}
												.container-form-input {
													padding: 5px;
													background-color: white;
													box-shadow: 0 3px 10px rgb(0 0 0 / 20%);
													margin-right: 20px;
													border-radius: 5px;
													display: flex;
													flex-direction: row;
													align-items: center;
													margin-bottom: 10px;
												}
												.container-form-input .placeholder {
													margin-left: 30px;
													font-size: 18px;
													font-weight: 500;
													margin-bottom: 0px;
												}

												div#coinRewardModal p {
													margin-top:0px;
													margin-bottom: 0px;
												}
												@media screen and (max-width: 995px){
											  .bgz {
														height: 500px;
													}
												}

											</style>
											<!-- form input -->
											<div>
												<p class="label-inputx">How many coins per person?</p>
												<div class="container-form-input">
													<div style="position: relative;">
														<input
																onchange="coinperperson(this.value)"
																type="number"
																id="coinpersion"
																value="1"
																min="1"
																class="form-control"
																style="width: 160px; padding-left: 40px;"
														>
														<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style="position: absolute; top: 5px; left: 10px;  width: 25px;" >
													</div>
													<p class="placeholder">coins per person</p>
												</div>
											</div>

											<!-- form input -->
											<div>
												<p class="label-inputx">How many people do you want to reward when they
													complete your assigned task/s?</p>
												<div class="container-form-input">

													<?php if($asset != 'account/update/post'){ ?>


													<input
															onchange="countperson(this.value)"
															type="number"
															id="persioncount"
															value="{{selected.length}}"
															class="form-control"
															readonly
															style="width: 100px;"
													>
													<?php } ?>
													<p class="placeholder">people</p>
												</div>
											</div>

										</div>

										<p class="desc-instant-reward" style="color: #FCB731; margin-top: 15px;     text-align: center;    text-align: center;">Your Total Reward Coins Offer:</p>
										<div style="position: relative;">
										<div class="d-flex justify-content-center">

											<div class="input-group mb-3 d-flex justify-content-center">
												<div class="input-group-prepend">
													<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style=" width: 40px;
    padding: 7px; " >
												</div>
												<input type="number" disabled value="0" id="totalculculate"  ng-model="totalculculate" style="width: 200px; padding-left: 40px; border: 1px solid #80808063;
    border-radius: 6px;
    padding: 0px 10px;"    >
											</div>


										</div>
										</div>
									<style>
										.btnx {
											background-color: #1f62ae !important;
											padding: 10px 25px;
											font-size: 12px !important;
											background-image: none !important;
											font-weight: 500 !important;
											border: none !important;
											font-size: 14px;
											font-weight: bold;
											border-radius: 10px;
											color: white;
										}

										.btn-primary-outlinex {
											/*border: 2px solid #1f62ae !important;*/
											background: white!important;
											color: #1f62ae!important;
										}
									</style>
										<div class="row" style="margin-top: 20px; justify-content: center; ">
											<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
												<span class="btnx btn-lg" ng-click="showdetails()" id="cointCreditsButton">Pay with coin credits</span>
											</div>
											<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
												<button type="button" class="btnx btn-primary-outlinex btn-lg" onclick="showanotherpopup()" data-dismiss="modal">Cancel</button>
											</div>
										</div>

										<a href="#">
											<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.jpg" alt="ad banner"  style=" height: 60px; width: 100%; margin-top: 20px; " >
										</a>
									</div>


									<!-- content modal instant reward  -->

									<div id="modalpurchasecoins" class=" modal-contentss " style="   display: none; " >


										<div style="display : flex ; align-items : center ; flex-direction: column; ">
										  <p style="margin-top: 10px; font-size: 28px; font-weight: 600; color: #FCB731; margin-bottom: 0px;">Coin Credits</p>
										   <p style="font-size: 16px; font-weight: 400;">Choose a Package:</p>

											<div class="promo-container">
												<span style="font-size: 14px; font-weight: 500;">Introductory Promo:</span>
												<div style="display: flex; align-items: center; ">
													<span style="font-size: 18px; font-weight: 700; color: #009344;">Get 20%</span>
													<span style="font-size: 14px; font-weight: 500; margin-left: 5px;">Extra on any package!</span>
												</div>
											</div>
													<style>
														.promo-container {
															padding: 5px;
															background-color: #dee6cc;
															width: 100%;
															display: flex;
															justify-content: space-between;
															align-items: center;
														}
														.card-promo-credit-coin {
															display: inline-block;
															padding-bottom: 20px;
															box-shadow: rgb(0 0 0 / 16%) 0px 1px 4px;
															display: flex;
															align-items: center;
															flex-direction: column;
															border-radius: 5px;
														}.card-promo-credit-coin-title {
															 width: 115px;
															 height: 68px;
															 border-bottom-left-radius: 15px;
															 border-bottom-right-radius: 15px;
															 display: flex;
															 justify-content: center;
															 align-items: center;
														 }.card-promo-credit-coin-value {
															  margin-top: 20px;
															  font-size: 20px;
															  font-weight: 700;
														  }.card-promo-credit-coin-active {
															   border-radius: 5px;
															   padding-bottom: 20px;
															   box-shadow: #009344 0px 2px 8px 0px;
															   display: flex;
															   align-items: center;
															   flex-direction: column;
															   border: 1px solid #009344;
														   }
														.card-promo-credit-coin:hover {
															border-radius: 5px;
															padding-bottom: 20px;
															box-shadow: #009344 0px 2px 8px 0px;
															display: flex;
															align-items: center;
															flex-direction: column;
															border: 1px solid #009344;
														}
														.credit-coin-container-pay {
															border-radius: 5px;
															padding-bottom: 20px;
															box-shadow: rgb(149 157 165 / 20%) 0px 8px 24px;
															border: 1px solid #009344;
														}
														.btns {
															padding: 10px 25px;
															font-size: 12px !important;
															background-image: none !important;
															font-weight: 500 !important;
															border: none !important;
															font-size: 14px;
															font-weight: bold;
															border-radius: 10px;
														}
														.payment-container-title {
															width: 100%;
															height: 68px;
															border-bottom-right-radius: 15px;
															background-color: #009344;
															display: flex;
															justify-content: center;
															align-items: center;
														}

													</style>
											<div class="row" style="width: 100%; margin-top: 15px;" id="modalCreditPointBuy">
												<?php foreach ( $allpacakges as $pack   ){ ?>

												<div class="col-xs-12 col-md-4">
													<div class="card-promo-credit-coin">
														<div class="card-promo-credit-coin-title" style="background-color: <?php echo $pack['CP_Color'] ?>;">
															<span style="color: white; font-size: 18px; font-weight: 500;" id="cpName<?php echo  $pack['CP_ID']; ?>"><?php echo $pack['CP_Name'] ?></span>
														</div>
														<span class="card-promo-credit-coin-value" style="color: #1F62AE;" id="cpprice<?php echo  $pack['CP_ID']; ?>"> P<?php echo $pack['CP_Ammount'] ?></span>
														<span style="text-align: center ; margin-top: 20px;" id="cpdesc<?php echo  $pack['CP_ID']; ?>"><?php echo $pack['CP_Description'] ?></span>
 											<button type="button" class="btns  btn-xl" style="color: white; background: <?php echo $pack['CP_Color'] ?>; margin-top: 20px;"
									 onclick="showthisone('<?php echo  $pack['CP_ID']; ?>')">Buy Now</button>
													</div>
												</div>
												<?php } ?>
<!--												<div class="col-xs-12 col-md-4">-->
<!--													<div class="card-promo-credit-coin-active">-->
<!--														<div class="card-promo-credit-coin-title" style="background-color: #009344;">-->
<!--															<span style="color: white; font-size: 18px; font-weight: 500;">Standard</span>-->
<!--														</div>-->
<!--														<span class="card-promo-credit-coin-value" style="color: #1F62AE;">P2,000</span>-->
<!--														<span style="text-align: center ; margin-top: 20px;">Equivalent to <br>-->
<!--										2,000 coins</span>-->
<!---->
<!--														<button type="button" class="btns btn-success" style="margin-top: 20px;" onclick="showthisone()" id="paymentStandardButton">Buy Now</button>-->
<!--													</div>-->
<!--												</div>-->
<!--												<div class="col-xs-12 col-md-4">-->
<!--													<div class="card-promo-credit-coin">-->
<!--														<div class="card-promo-credit-coin-title" style="background-color: #FCB731;">-->
<!--															<span style="color: white; font-size: 18px; font-weight: 500;">Premium</span>-->
<!--														</div>-->
<!--														<span class="card-promo-credit-coin-value" style="color: #1F62AE;">P1,000</span>-->
<!--														<span style="text-align: center ; margin-top: 20px;">Equivalent to <br>-->
<!--										1,000 coins</span>-->
<!---->
<!--														<button type="button" class="btns btn-warning" style="margin-top: 20px;">Buy Now</button>-->
<!--													</div>-->
<!--												</div>-->
											</div>

											<div style="width: 100%; display: none; margin-top: 15px;" id="modalCreditPointPay">
												<div class="row credit-coin-container-pay" style="width: 100%;">
													<div class="col-xs-12 col-md-6" style="padding-left: 0px; display: flex ; align-items: center; flex-direction: column;">
														<div class="payment-container-title">
															<input type="hidden" id="selectedpackege">
															<span style="font-size: 18px; color: white; font-weight: 500;" id="paytype">Standard</span>
														</div>
														<p style="color: #009344; font-weight: 700; font-size: 20px; margin-top: 20px;" id="payprice">P2,000</p>
														<p style="margin-top: 20px; font-size: 14px; text-align: center;" id="paydes">Equivalent to <br>
															2,000 coins</p>
														<button type="button" class="btns btn-primary btn-lg" style="margin-top: 15px;" onclick="purchasecredit()" >Pay Now</button>
														<button class="btns  btn-lg" onclick="backtopayment()">Cancel</button>
													</div>
													<div class="col-xs-12 col-md-6" style="display: flex; justify-content: center; align-items: center;">
														<img src="<?php echo base_url(); ?>assetsofpop/img/payment-img.png" alt="" style=" width: 163px;  margin-top: 40px;">
													</div>
												</div>
											</div>

											<p style="font-size: 16px; font-weight: 500; margin-top: 20px;">Have any questions?</p>
											<button type="button" class="btn  btn-primary mr-2" style="    background: white;
											 color: #398cb3;
											 padding: 0px 8px;
											 margin-top: 10px;">Contact Us</button>

											<img src="<?php echo base_url(); ?>assetsofpop/img/ad-banner.png" style="width: 100%; margin-top: 10px;">
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
		<!-- Modal Instant Reward -->

 <input type="hidden" name="PostID" value="<?=$EncodedID?>" />
		 <input	type="hidden" name="baseurl" id="baseurl" value="<?php echo base_url(); ?>" >
		<input type="hidden" name="userID" id="userID" value="<?php if(isset($this->LoggedInUser)){echo $this->LoggedInUser;}?>" />
 <input type="hidden" name="group_idx" id="groupIDx" value="<?php echo encode_id( $posts_item['Po_Gr_ID']); ?>" />
<input type="hidden" name="privacy" id="privacy"  value="<?php echo $posts_item['Po_Privacy']; ?>">
 <input type="hidden" name="group_id" id="groupID" value="<?php echo $posts_item['Po_Gr_ID'] ?>" />
 <input type="hidden" name="post_id" id="Post_ID" value="<?php echo decode_id($EncodedID) ?>" />
  <input type="hidden" name="refererer" id="post_refererer" value="
  <?php if(isset( $_SESSION['post_referrerID'])){
   echo $_SESSION['post_referrerID'];
   } ?>" />
   <input type="hidden" name="post_id" id="post_role" value="<?php if(isset($_SESSION['post_role'])){echo $_SESSION['post_role'];} ?>" />
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	 <div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
        		<div class="img-container">
            		<div class="row">
                		<div class="col-md-8">
                    		<img src="" id="crop_uploaded_image" />
                		</div>
                		<div class="col-md-4">
                    		<div class="preview"></div>
				      		<div class="modal-footer" style='padding-right: 0px;'>
				      			<button type="button" id="crop" class="btn btn-primary">Upload</button>
				        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				      		</div>
                		</div>
            		</div>
        		</div>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="external_link_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px; height:600px!important'>
<!--        		<iframe style="width:100%;-->
<!--        			height : 580px!important-->
<!--        		" src="http://localhost/boardspeak/"></iframe>-->
      		</div>
    	</div>
  	</div>
</div>
<?php
	if (isset($this->session->userdata['logged_in']['bs_id'])) {
		$this->load->view('account/group_users_pop_up_post');
	}
?>

	<?php if (!isset($this->session->userdata['logged_in']['bs_id'])) { ?>
		<script>
			localStorage.setItem("post_id", <?php echo $ID; ?>);
			localStorage.setItem("post_role", <?php echo $role; ?>);
 			localStorage.setItem("post_referrer", <?php echo $referrer; ?>);
		</script>
	<?php } ?>
<script>
			let userdatalist = [];
	function checkUserList  (){
		//

		if($('#userdatalist').val() != ''){
			$.ajax({
				url: base_url + 'account/sendVoucherUser',
				method: 'POST',
				dataType: 'json',
				data: {
					id:  $('#Post_ID').val(),
				},
				success: function (response) {
					userdatalist  = response;

					console.log("userdatalist" , userdatalist);
				}
			});
		}

	}

	function changepinstatus(pintype){
		// pintype
		if(pintype == 1){
			$text = 'pin'
		}else{
			$text = 'un pin';
		}
		Notify.confirm("Are you sure you want to "+$text+" this post", function () {
			$.ajax({
				url: base_url + 'group/group_pinning_post',
				method: 'POST',
				dataType: 'json',
				data: {
					post_item:  $('#Post_ID').val(),
					post_type: 'Post',
					post_decide :  pintype
				},
				success: function (response2) {
					Notify.success(response2 , {
						afterClose : function () {
						  location.reload();
							// ViewGroup.search_topics_per_category($("#inp_category_id").val());
						}
					});
				}
			});
		}, function () {

		});
	}

</script>
		<div id="poolmodel" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" align="center">Create Poll</h4>
						<button type="button" class="close"  onclick="cclosemodel()">&times;</button>

					</div>
					<div class="modal-body" style="padding: 10px; background: #80808026;">
						<form onsubmit="return checkvailidation()">
						<h6>Start the poll question</h6>

						<div class="form-group">
							<label>Poll Description / Details</label>
							<textarea  placeholder="Enter Message."  id="pooldescription" class="form-control" style="width: 100%; " rows="4"></textarea>
						</div>
							<script>

									CKEDITOR.replace( 'pooldescription' );

							</script>
							<div class="form-group">
								<label>Question</label>
								<input type="text" required class="form-control" id="pool_question" name="pool_question">
							</div>
						<div class="form-group" id="answerDiv"  id="questionid1">
							<label>Possible Answer(one per input box)</label>
							<div class="input-group mb-3">
								<input type="text" class="form-control answerinput" id="answer1" required  placeholder="Enter Answer">

							</div>
							<div class="input-group mb-3" id="questionid2">
								<input type="text" class="form-control answerinput" id="answer2"  required  placeholder="Enter Answer">

							</div>
						</div>
						<a href="javascript:void(0);" onclick="addnewAnswer()"><i class="fa fa-plus">  </i>&nbsp;&nbsp;Add Possible Answer</a>
							<?php if($asset != 'account/update/post'){ ?>
							<div class="control-group mb-2 form-group">
								<label class="font-weight-bold label-h6">Add Action Button</label>
								<div class="admin-form-group controls mb-0 pb-1">
									<div class="input-group">
										<select class="custom-select" onchange="selectedButtton(this.value)" id="selActionButton" name="selActionButton" placeholder='Choose your Action Button'>
											<option value=''>Choose your Action Button</option>
											<?php foreach ($button_items as $item): ?>
												<?php echo '<option value="'.$item['Bn_Caption'].'">'.$item['Bn_Caption'].'</option>'; ?>
											<?php endforeach; ?>
											<option value="0">Others, please specify</option>
										</select>
									</div>
								</div>


								<div  class="control-group mb-2" id="selNewActionButton" style="display: none;" >
									<div class="admin-form-group controls pb-1">
										<input class="form-control" id="txtActionButton" readonly name="txtActionButton" type="text" placeholder="Enter new action caption">
									</div>
								</div>
									<?php } ?>
								<div class="control-group mb-2" id="serveryLinkButton" style="display: none;" >
									<span class="ml-1 text-primary font-weight-normal text-small" style="color:red!important;">Please complete the link (by adding https://www. )</span>
									<div class="admin-form-group controls pb-1">
										<input class="form-control" id="serveryLink" readonly   name="serveryLink" type="text" placeholder="Enter URL link here with https://">
									</div>
									<span id="errormessage" class="ml-1 text-primary font-weight-normal text-small" style="display: none; color:red!important;">Link is not correct.</span>

								</div>
							</div>
							<div class="form-group">
							<div class="row" style="margin: 10px;">
								<button type="submit"  class="btn btn-primary ">Save the poll</button>
								<button type="button" class="btn btn-light ml-2 " onclick="cclosemodel()">Cancel</button>

							</div>
						</div>
						</form>
					</div>
					<div class="modal-footer">

					</div>
				</div>

			</div>
		</div>
		<input type="hidden" id="userpool" value="<?php if(isset($polluserDetail) && !empty($polluserDetail)){
			echo count($polluserDetail);
		} ?>">

		<div class="modal" id="editingpool">
			<div class="modal-dialog">
				<div class="modal-content" style="height: 600px;">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Edit Poll </h4>

						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body" style="padding: 0px;">
						<form onsubmit="return answerupdatevalidation()" id="updatepoll" style="
						 overflow: scroll;
  						 height: 480px;margin: 20px;
						 padding: 10px; background: #388cb317;">
						<div style="margin-top: 15px;">
							<label>Description:</label>
							<textarea id="editpool"  >
									<?php if(isset($polldata[0]['PL_description'])){
										echo $polldata[0]['PL_description'];
									} ?>
							</textarea>
							<script>

								CKEDITOR.replace( 'editpool' );

							</script>

						</div>

							<input type="hidden" value="<?php if(isset($polldata[0]['Pl_ID'])){
								echo $polldata[0]['Pl_ID'];
							} ?>" id="poolidx">
							<div style="margin-bottom: 10px; margin-top: 21px; margin-left: 14px;">
								<label>Question</label>
								<input class="form-control" id="pollquestionmain" style="text-transform: capitalize;" value="<?php if(isset($polldata[0]['PL_Question'])){
									echo $polldata[0]['PL_Question'];
								} ?>">
							</div>
							<div class="col-sm-12" id="editanswerques">


							<?php if(isset($polldata[0]['PL_answers'])){
								$array = explode('^', $polldata[0]['PL_answers']);

								foreach ($array as $keyz => $value){ ?>
									<div class="col-sm-12" style="margin: 10px;">
										<label>Answer <?php echo $keyz +1; ?></label>
										<input  class="form-control" id="questionidedit<?php echo $keyz?>" type="text" name="questionupdate[]" value="<?php echo $value; ?>">
									</div>
								<?php	}

							} ?>
							</div>
									<div >
										<button onclick="editnewdnewAnswer()"><i class="fa fa-plus"></i> Add Possibly Answer</button>
									</div>
							<div class="control-group mb-2  mt-3 form-group">
								<label class="label-h6">Add Action Button</label>
								<div class="admin-form-group controls mb-0 pb-1">
									<div class="input-group">
										<select class="custom-select" onchange="selectedButttonedit(this.value)" id="selActionButtonedit" name="selActionButton" placeholder='Choose your Action Button'>
											<option value=''>Choose your Action Button</option>
											<?php
											$required = '';
											if( $polldata[0]['serverybuttonlink'] != '' &&  $polldata[0]['serverybuttonname'] != ''){
												$required  = 'required';
											}
											if($polldata[0]['serverybuttonname'] == ''){
												$found = true;
											}else{
												$found = false;
											}

											foreach ($button_items as $item){

												$selected = '';
												if($item['Bn_Caption'] == $polldata[0]['serverybuttonname']){
													$found = true;
													$selected = 'selected';
												}

												?>
												<?php echo '<option '.$selected.' value="'.$item['Bn_Caption'].'">'.$item['Bn_Caption'].'</option>'; ?>
											<?php } ?>

											<option <?php
												if($found == false){
													echo 'selected';
												}  ?> value="0">Others, please specify</option>
										</select>
									</div>
								</div>

								<div class="control-group mb-2" id="selNewActionButtonedit" style="

								display:  <?php
								if($found == false){
									echo 'block ';
								}else{
									echo 'none';
								}  ?>;"



								>
									<div class="admin-form-group controls pb-1">
										<input class="form-control" id="txtActionButtonedit" <?php
										if($found == false){
											echo '';
										}else{
											echo 'readonly';
										}  ?>;   <?php echo $required; ?> value="<?php  echo $polldata[0]['serverybuttonname'];?>" name="txtActionButton" type="text" placeholder="Enter new action caption">
									</div>
								</div>

								<div class="control-group mb-2" id="serveryLinkButtonedit" style="display:  <?php
								if( $polldata[0]['serverybuttonname'] != '' ){
									echo 'block';
								}else{
									echo 'none';
								}  ?>;" >
									<span class="ml-1 text-primary font-weight-normal text-small" style="color:red!important;">Please complete the link (by adding https://www. )</span>
									<div class="admin-form-group controls pb-1">
										<input class="form-control" id="serveryLinkedit"  <?php echo $required; ?>  value="<?php echo $polldata[0]['serverybuttonlink']; ?>"   name="serveryLink" type="text" placeholder="Enter URL link here with https://">
									</div>
									<span id="errormessage1" class="ml-1 text-primary font-weight-normal text-small" style="display: none; color:red!important;">Link is not correct.</span>

								</div>
							</div>
							<div class="form-group" style="margin-top: 10px;">
								<div class="row" style="margin: 10px;">
									<button type="submit"    class="btn btn-success">Update</button>
									<button type="button"  class="btn btn-light ml-2 " data-dismiss="modal">Cancel</button>

										<button type="button" style="    margin-left: 39%;
    border: 1px solid grey;" class="btn btn-default" data-dismiss="modal">Close</button>

								</div>
							</div>
						</form>
					</div>

					<!-- Modal footer -->


				</div>
			</div>
		</div>

		<div id="poolanswer" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content" style="  min-height: 600px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>

					</div>
					<h4 align="center">Poll</h4>
					<h5 class="modal-title" align="center"  style="color: #388cb3;" > <?=$posts_item['Po_Title']?></h5>
					<style>
    .post-seemore{
        /* font-family: "Poppins",sans-serif; */
    }
    .post-seemore p{
        font-size: 14px;
    }
    .post-seemore button{
        padding: 10px;
        background: transparent;
     outline:none;
        color: #2196f3;
        border: none;
        cursor: pointer;
        margin-left: auto;
        display: block;
    }
    .post-seemore:not(:last-child){
        margin-bottom: 50px;
    }
    .hide{
        display: none;
    }</style>
					<div class="modal-body post-seemore" style=" padding: 1rem 1rem!important;
							border: 1px solid grey;
							margin: 10px;
							border-radius: 10px;">
					 	<div style="margin-top: 15px;
    max-height: 200px;
    overflow: scroll;
    margin-bottom: 14px;
    overflow-x: hidden; ">
							<p class="post--content"><?php
							if(isset($polldata[0]['PL_description'])){
								echo $polldata[0]['PL_description'];
								}
								 ?> </p>
								<button class="tog-btn" onclick='readMore(this)'>See More</button>
						</div>
						<script>

// $( document ).ready(function() {
//
// 	$('#pollQuestionButton').click();
// });
</script>

<script>let noOfCharac = 5;
let contents = document.querySelectorAll(".post--content");
let btn=document.querySelector(".tog-btn")
contents.forEach(content => {
//If text length is less that noOfCharac... then hide the read more button
if(content.textContent.length < noOfCharac){
btn.style.display = "none";
}
else{
let displayText = content.textContent.slice(0,noOfCharac);
let moreText = content.textContent.slice(noOfCharac);
content.innerHTML = `${displayText}<span class="dots">...</span><span class="hide more">${moreText}</span>`;
}
});

function readMore(btn){
let post = btn.parentElement.parentElement;
post.querySelector(".dots").classList.toggle("hide");
post.querySelector(".more").classList.toggle("hide");
btn.textContent == "See More" ? btn.textContent = "See Less" : btn.textContent = "See More";
}</script>

						<form onsubmit="return answwercheckvailidation()" style=" padding: 10px; background: #388cb317;">

							<?php

							if($polldata[0]['serverybuttonname'] != '' && $polldata[0]['serverybuttonlink'] != ''){ ?>
								<center>
								<a class="btn btn-primary  title-tip title-tip-bottom" target="_blank" href="<?php echo $polldata[0]['serverybuttonlink']; ?>" title="Clicking this button will direct you to another tab. Just
go back to BoardSpeak tab to revert back to this page."><?php echo $polldata[0]['serverybuttonname'] ?></a>
								</center><?php }
							?>
							<input type="hidden" value="<?php if(isset($polldata[0]['Pl_ID'])){
						echo $polldata[0]['Pl_ID'];
					} ?>" id="poolid">
							<div style="margin-bottom: 10px; margin-top: 21px; margin-left: 14px;">
								<h6><?php if(isset($polldata[0]['PL_Question'])){
										echo $polldata[0]['PL_Question'];
									} ?></h6>
							</div>
							<?php if(isset($polldata[0]['PL_answers'])){
								$array = explode('^', $polldata[0]['PL_answers']);

								foreach ($array as $value){ ?>
									<div class="col-sm-12" style="margin: 10px;">
										<input required type="radio" name="questionname" value="<?php echo $value; ?>"> <?php echo $value; ?>

									</div>

								<?php	}

							} ?>
							<label>Note:</label>
							<textarea id="answerNote" class="form-control" row="6" placeholder="Add Note With Your Answer" ></textarea>

							<div class="form-group" style="margin-top: 10px;">
								<div class="row" style="margin: 20px;">
											<?php if(isset($this->LoggedInUser)) { ?>
									<button type="submit"    class="btn btn-success">Submit</button>
									<?php }else{ ?>

										 <button onclick="showloginpop()" class="btn btn-success" type="button">
														Submit
													</button>

											<?php } ?>
									<button type="button"  class="btn btn-light ml-2 " data-dismiss="modal">Cancel</button>
									<h6 align="right" style=" color: grey;font-size: 12px; width: 100%;">Vote (<?php if (isset($polldatacount) && !empty($polldatacount)){
										echo count($polldatacount);
										}else{
										echo 0;
										} ?>)</h6>
								</div>
							</div>
						</form>
					</div>
					<div class="" style="    margin: 13px;">
						<?php if(isset($creator_info)){ ?>


						<?php if( isset($this->LoggedInUser) && $this->LoggedInUser == $posts_item['Po_Us_ID'] ||   $creator_info['role'] == 'creator' ) { ?>
 							 <?php if(isset($polldata) && !empty($polldata)){ ?>
 									<?php if(isset($polldatacount) && !empty($polldatacount)){ ?>

										<a  class="btn btn-outline-danger"    onclick="deletepool()">Delete Poll</a>
										<a class="btn btn-outline-info" href="javascript:void(0);" onclick="endPoll()">End Poll</a>
									<?php }else{ ?>
									<div class="justify-content-around">

										<a  class="btn btn-outline-primary" style="margin-left: 30%;     margin-right: 10px;" href="javascript:void(0);"  onclick="editpool()">Edit Poll</a>
										<a  class="btn btn-danger delete-poll-btn" style="color:white;"   onclick="deletepool()">Delete Poll</a>
									</div>
									<?php } ?>
								<?php }else{ ?>

								<?php }?>
						<?php }?>
						<?php } ?>
						<style>
							/* .delete-poll-btn{
							color:red;
							}
							.delete-poll-btn:hover{
							color:white;
							} */
						</style>

						<?php if (isset($polldatacount) && !empty($polldatacount)){ ?>
						<span style="float:right;" class="btn btn-outline-primary" onclick="viewresult()">View Result</span>
						<?php  } ?>
					</div>
				</div>

			</div>
		</div>
		<!-- Modal -->
		<div id="resultmodel" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" align="center" id="changetext">Poll result </h4><span style="    margin-top: 9px; font-size: 12px;    margin-left: 5px;">Vote(<?php echo $totaldata; ?>)</span>
						<button type="button" class="close" data-dismiss="modal">&times;</button>

					</div>
					<div class="modal-body" style="padding: 5px;    height: 400px;  overflow-x: hidden;">
			<script>
				function quesstiondic(){
					 if($('#seemorenbutton').text() == 'see More'){
						 $('#quesstiondic').css('height' , 'auto');
						 $('#seemorenbutton').text('See less')
					}else{
						 $('#quesstiondic').css('height' , '22px');
						 $('#seemorenbutton').text('see More')
					 }

				}
			</script>
						<div class="mb-4 ml-5">
							<h6 id="quesstiondic" style="    min-height: 20px;
    overflow: hidden;
    height: 97px;"><?php if(isset($polldata[0]['PL_description'])){
									echo $polldata[0]['PL_description'];
								} ?></h6>
								<a id="seemorenbutton" style="
    float: right;
    margin-right: 10px!important;" onclick="quesstiondic()">See more</a>
						</div>
					 		<?php
							$countww = 1;
							foreach ($reviwer as $keyz => $viewz){

 									foreach ($viewz as $key => $valz){ ?>
							 <label  style="display: <?php if(count($valz) > 0 ){
								 echo 'block';
							 }else{
								 echo 'none';
							 }?>;   margin-left: 10px;">Answer <?php echo $countww; ?></label>
										<?php
										$countww++;
										$keyyy = 	rand(10,100);
										 ?>
									 <div style="   display: <?php if(count($valz) > 0 ){
											 echo 'block';
										 }else{
											 echo 'none';
										 }?>;  padding: 10px;
									border: 1px solid #8080805c;
									border-radius: 10px;
									box-shadow: 1px 1px 1px 1px #80808045;
									margin-bottom: 19px;">
										<div class="row "  style="display: <?php if(count($valz) > 0 ){
											echo 'block';
										}else{
											echo 'none';
										}?>">
												<div class="col-8">
													<h6 class="ml-5" style=" font-size: 12px; color: grey; text-transform: capitalize ">
														<?php echo $key . '-' .cal_percentage(count($valz), $totaldata).'%'; ?>
													</h6>
												</div>
												<div class="col-4">
													<h6  style=" font-size: 10px; color: grey; text-transform: capitalize ">
														<?php  echo count($valz) . " votes"; ?>
													</h6>
													<button class="add_member_btn add_member btn btn-light createmessage" id="allsel<?php echo $keyyy; ?>" style="" onclick="toggleselected('<?php echo $keyyy; ?>')">
														Select All
													</button>

												</div>
										</div>
										 <div class="row">
										<?php
										$poa_pl_id = 0;
										$textmessage  = '';
										foreach ($valz as $subval){
											$poa_pl_id =	$subval['Poa_pl_id'];
											$textmessage =	$subval['Poa_note'];

											?>

											 <div class="col-12">
												<div class="row">
													<div class="col-12 row" style="margin: 10px;">
														<div class="col-2">
															<img class="ml-3" style="min-width: 45px; min-height: 45px; border-radius: 50%;" src="<?php 	if(!empty($subval['Us_Photo'])){
																echo $subval['Us_Photo'] ;

															}else{
																echo base_url() . 'img/nophoto.png';
															} ?> ">
														</div>
															<div class="col-6 ml-4">
																<strong><h6 class="ml-2 mt-1" style="font-size: 14px; color: grey;">
																	<?php
																	if (!empty($subval['Us_Alias'])) {
																		echo $subval['Us_Alias'];

																	} else {
																		echo $subval['Us_Name'];
																	} 	?></h6></strong>

																<h6 class="ml-2 mt-1" style="font-size: 10px; color: grey;"><?php echo $subval['Us_JobTitle'] ?></h6>
															</div>
														<div class="col-4 createmessage">
															<button class="add_member_btn add_member btn btn-light selectedall<?php echo $keyyy; ?>"  id="selectedbutton<?php echo $subval['Poa_id'] ?>"  onclick="selecteduser('<?php echo $subval['Poa_id'] ?>' , <?php echo $subval['Us_ID']; ?> )" style="">
																select
															</button>
															<input type="checkbox" style="display: none; " name="allchck<?php echo $keyyy;?>" class="checkboxid<?php echo $keyyy; ?>" id="<?php echo $subval['Us_ID']; ?>userid<?php echo $subval['Poa_id']; ?>" value="<?php echo $subval['Us_ID']; ?>"	>
															<button class="add_member_btn added_member btn unselectedall<?php echo $keyyy; ?>" id="unselectedbutton<?php echo $subval['Poa_id'] ?>" onclick="unselected('<?php echo $subval['Poa_id'] ?>' , <?php echo $subval['Us_ID']; ?>)" style=" display: none;">
																<i class="fas fa-check"></i> Selected
															</button>
 														</div>

													</div>

												</div>

											 </div>
										<?php 	} ?>
										 </div>
										 <div style="background: #add8e654;    /* padding: 10px; */margin-bottom: 20px; padding-top: 12px;">


										 <?php if($poa_pl_id == 0){

										 }else{ ?>
											<?php if($textmessage == ''){ ?>
												 <button style=" padding: 1px;     font-size: 13px; margin-left: 30px;    margin-bottom: 15px;" class="btn btn-outline-primary createmessage" onclick="sendMessage( '<?php echo $keyyy; ?>' ,  '<?php echo $key; ?>' , '<?php echo $poa_pl_id; ?>' ,  <?php echo $subval['Poa_id'] ?>)">Create Message</button>

											<?php }else{ ?>
												 <button style=" padding: 1px;     font-size: 13px;  margin-left: 30px;    margin-bottom: 15px;" class="btn btn-outline-primary createmessage" onclick="sendMessage('<?php echo $keyyy; ?>' ,  '<?php echo $key; ?>' , '<?php echo $poa_pl_id; ?>'  , <?php echo $subval['Poa_id'] ?>)">Edit Message</button>

											 <?php } ?>

														<input type="hidden" id="notemessaage<?php echo $keyyy; ?>" value="<?php echo $textmessage; ?>">
											 <button  id="messagesecndbutton<?php echo $poa_pl_id; ?>" style="
													 display: <?php if($textmessage == ''){ ?>
																none;
											<?php  }else{ ?>    <?php  } ?> padding: 1px;     font-size: 13px;   margin-left: 51%;  margin-bottom: 15px;" class="btn btn-primary createmessage" onclick="throuhmessage('<?php echo $keyyy; ?>' , '<?php echo $poa_pl_id; ?>'  )">Send Message</button>
										 <?php  } ?>

										 </div>
										</div>
							 		<?php }

							 }?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default"  onclick="openanother()">Close</button>
					</div>
				</div>

			</div>
		</div>

		<div id="coinRewardModalforEdit" class="modal fade" role="dialog" style="overflow-x: hidden;
    overflow-y: auto;">
			<div class="modal-dialog modal-lg" >
				<button style="  margin-top: -16px;   position: absolute;  z-index: 9;   right: 0;   padding: 7px;
						background: #2b2f33;  border-radius: 50%;  color: white;  right: -16px;
						font-size: 25px;  padding: 5px 10px;" type="button" class="close" onclick="closeeditVouche()" data-dismiss="modal">&times;</button>
				<!-- Modal content-->
				<div class="modal-content">
					<style>
						.bonus-coin-bg {
							display: flex;
							flex-direction: column;
							justify-content: end;
							background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-redeem-coin-modal.png);
						}
					</style>
					<div class="modal-body" style="padding: 1px!important;">
						<div class="row" style=" ">
							<div id="rewardpurcase" style="display: none;" class="col-xs-12 col-md-5">
								<div class="bonus-coin-bg bgz" style="background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-credit-coin-modal.png); background-color: #D2F324; ">
									<p style="font-size: 26px; font-weight: 600;">INSTANTLY REWARD
										your members</p>
									<p style="font-size: 16px; font-weight: 400;">with our automated tracking /selection
										& coin wallet system.</p>
								</div>
							</div>
							<div id="instatnt" class="col-xs-12 col-md-5">
								<div class="instant-reward-bg bgz" style="    min-height: 100%;background-image: url(<?php echo base_url(); ?>assetsofpop/img/bg-modal-instant-reward.png); ">
									<div>
										<p style="font-size: 30px;  font-weight: 600;">Instant Rewards Boosts Motivation</p>
										<img style="margin-top:20px;" src="<?php echo base_url(); ?>assetsofpop/img/img-modal-instant-reward.png" alt="modal instant reward">
									</div>
									<p style="margin: 0px; font-size: 24px;  font-weight: 500;">Increase member's
										positive experience
										on every task!</p>

								</div>
							</div>
							<div class="col-xs-12 col-md-7" style="padding: 17px;" >
								<!-- exit button -->

								<div style="display : flex ; align-items : center ; flex-direction: column; ">

									<?php

									if(isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ ?>
										<img class="avatar roundimage" src="<?php echo $this->session->userdata['logged_in']['Us_Photo']; ?>">
									<?php }else{ ?>
										<i class="fas fa-user-circle"></i>
									<?php } ?>
									<h2 class="avatar-name-text headingtext" >

										<?php print_r($this->session->userdata['logged_in']['bs_name']); ?>
									</h2>
									<p class="avatar-job-text graycolor"><?php echo $this->session->userdata['logged_in']['bs_alias']; ?></p>

									<!-- content modal instant reward  -->
									<div id="modalInstantvoucher" class="modal-contents" >
										<h2 align="center" class="title-instant-reward">
											Edit Rewards Voucher
										</h2>
										<p class="desc-instant-reward" style="color: #a5a5a5;" align="center">Boost Customer Traffic</p>

										<!-- form -->
										<form  onsubmit="return editdetailsofvoucher();">
											<div style="width: 100%; margin-top: 10px;" class="from-input">
												<p class="title-form-instant-reward ">What is your Reward Offer?</p>

													<input type="hidden" id="voucherpostid">
												<label>Title</label>
												<input type="text"   id="vouchertitleedit"  required class="form-control" placeholder="Ex.  Get P500 cashback on your first purchase.">
												<label>Sub Title</label>
												<input type="text"   id="vouchersubtitleedit"  required class="form-control" placeholder="Subtitle.">


												<style>
													.label-inputx {
														font-size: 18px;
														font-weight: 500;
														color: #1f62ae;
													}
													.container-form-input {
														padding: 5px;
														background-color: white;
														box-shadow: 0 3px 10px rgb(0 0 0 / 20%);
														margin-right: 20px;
														border-radius: 5px;
														display: flex;
														flex-direction: row;
														align-items: center;
														margin-bottom: 10px;
													}
													.container-form-input .placeholder {
														margin-left: 30px;
														font-size: 18px;
														font-weight: 500;
														margin-bottom: 0px;
													}

													div#coinRewardModal p {
														margin-top:0px;
														margin-bottom: 0px;
													}
													@media screen and (max-width: 995px){
														.bgz {
															height: 500px;
														}
													}

												</style>
												<!-- form input -->
												<div>
													<p class="label-inputx">How many recipients do you want to give away vouchers to (after completion of your assigned task/s)?</p>
													<div class="container-form-input">
														<div style="position: relative;">
															<input

																type="number"
																id="voucherpersionedit"


																min="1"
																required

																class="form-control"
																style="width: 160px; padding-left: 40px;"
															>
														</div>
														<p class="placeholder"> Recipient/s</p>
													</div>
												</div>
												<div>
													<p class="label-inputx">How much (in Peso value) per voucher are you giving away?</p>
													<div class="container-form-input">
														<div style="position: relative;">
															<input

																type="number"
																id="paisaammountedit"
																min="50"
																value="50"

																required
																class="form-control"
																style="width: 160px; padding-left: 40px;"
															>
															<p  style="position: absolute; top: 5px; left: 10px;  width: 25px;" >₱</p>
														</div>
														<p class="placeholder"> per voucher </p>
													</div>
												</div>

												<!-- form input -->
												<div>
													<p class="label-inputx">Until when will vouchers be valid (after issuance)?</p>
													<div class="container-form-input">
														<input type="date" id="dateedit" required>
														<p class="placeholder">Enter date</p>
													</div>
												</div>

											</div>

											<p class="desc-instant-reward" style="color: #FCB731; margin-top: 15px;
										   text-align: center;    text-align: center;">Participants will redeem each voucher with coins:</p>
											<div style="position: relative;">
												<div class="d-flex justify-content-center">

													<div class="input-group mb-3 d-flex justify-content-center">
														<div class="input-group-prepend">
															<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin.png" alt="icon coin" style=" width: 40px;
    padding: 7px; " >
														</div>
														<input type="number" disabled 	min="5"  value="5" id="totalredeempayedit"
															   style="width: 200px;
													 padding-left: 40px;
													  border: 1px solid #80808063;
    border-radius: 6px;
    padding: 0px 10px;"    >
													</div>


												</div>
											</div>
											<style>
												.btnx {
													background-color: #1f62ae !important;
													padding: 10px 25px;
													font-size: 12px !important;
													background-image: none !important;
													font-weight: 500 !important;
													border: none !important;
													font-size: 14px;
													font-weight: bold;
													border-radius: 10px;
													color: white;
												}

												.btn-primary-outlinex {
													/*border: 2px solid #1f62ae !important;*/
													background: white!important;
													color: #1f62ae!important;
												}
											</style>
											<div class="col-sm-12">
												<label class="label-inputx">Please indicate here your Redemption Terms & Conditions:</label>
												<textarea class="form-control"     id="decriptionedit"></textarea>
											</div>
											<div class="row" style="margin-top: 20px; justify-content: center; ">
<!--												<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >-->
<!--													<button type="button"  class="btnx btn-lg" onclick="showdetailsofvoucherx()"  id="cointCreditsButton">Preview Voucher</button>-->
<!--												</div>-->
												<div class="col-xs-12 col-md-6" style=" display: flex; justify-content: center; " >
													<button type="button" class="btnx btn-primary-outlinex btn-lg"   data-dismiss="modal">Cancel</button>
												</div>
											</div>
											<div class="d-flex justify-content-center">
												<button type="submit" align="center" class="btn btn-info">Edit Reward Voucher </button>
											</div>

										</form>

									</div>
									<!-- content modal instant reward  -->


									<!-- content modal instant reward  -->

								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
				<script>
					function selectedButttonedit(buttonvalue){


						if(buttonvalue == ''){
							$('#selNewActionButtonedit').hide();
							$('#serveryLinkButtonedit').hide();

							$('#txtActionButtonedit').attr('readonly' , true);
							$('#txtActionButtonedit').text('');
							$('#txtActionButtonedit').attr('required' , false);

							$('#serveryLinkedit').text('');
							$('#serveryLinkedit').attr('readonly' , true);
							$('#serveryLinkedit').attr('required' , false);
							return;
						}

						if(buttonvalue == 0){
							$('#selNewActionButtonedit').show();
							$('#txtActionButtonedit').text('');
							$('#txtActionButtonedit').attr('readonly' , false);
							$('#txtActionButtonedit').attr('required' , true);

							$('#serveryLinkButtonedit').show();

							$('#serveryLinkedit').text('');
							$('#serveryLinkedit').attr('readonly' , false);
							$('#serveryLinkedit').attr('required' , true);
						}else{


							$('#selNewActionButtonedit').hide();
							$('#txtActionButtonedit').attr('readonly' , true);
							$('#txtActionButtonedit').text('');
							$('#txtActionButtonedit').attr('required' , false);

							$('#serveryLinkButtonedit').show();

							$('#serveryLinkedit').text('');
							$('#serveryLinkedit').attr('readonly' , false);
							$('#serveryLinkedit').attr('required' , true);

						}


					}

					function selectedButtton(buttonvalue){


						if(buttonvalue == ''){
							$('#selNewActionButton').hide();
							$('#serveryLinkButton').hide();

							$('#txtActionButton').attr('readonly' , true);
							$('#txtActionButton').text('');
							$('#txtActionButton').attr('required' , false);

							$('#serveryLink').text('');
							$('#serveryLink').attr('readonly' , true);
							$('#serveryLink').attr('required' , false);
							return;
						}

									 if(buttonvalue == 0){
										$('#selNewActionButton').show();
										$('#txtActionButton').text('');
										$('#txtActionButton').attr('readonly' , false);
 										$('#txtActionButton').attr('required' , true);

										 $('#serveryLinkButton').show();

										 $('#serveryLink').text('');
										 $('#serveryLink').attr('readonly' , false);
										 $('#serveryLink').attr('required' , true);
									}else{


											 $('#selNewActionButton').hide();
											 $('#txtActionButton').attr('readonly' , true);
											 $('#txtActionButton').text('');
											 $('#txtActionButton').attr('required' , false);

										 $('#serveryLinkButton').show();

										 $('#serveryLink').text('');
										 $('#serveryLink').attr('readonly' , false);
										 $('#serveryLink').attr('required' , true);

									 }


					}
					function closeeditVouche(){
						$('#coinRewardModalforEdit').modal('hide');
					}
					function  showloginpop(){

						Swal.fire({
							title: 'Log in to answer poll',
							text: "You need to be logged in to submit your answer",
							icon: 'info',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Login'
						}).then((result) => {
							if (result.isConfirmed) {
								 window.location.replace('<?php echo base_url() ?>'+'pages/view/signup');
							}
						})
					}
					function  purchasecredit(){


						$.ajax({
							url: base_url + 'account/purcashingCoins',
							method: 'POST',
							dataType: 'json',
							data: {
								PackageId:  $('#selectedpackege').val(),


							},
							success: function (response2) {

								 console.log(response2);

								 if(response2 == '1'){
									 Swal.fire({
										 icon: 'success',
										 title: 'Coin Purchased',
										 text: 'Successfully purchased Coins. Check your Wallet',

									 });
									 $('#coinRewardModal').modal('hide');
								 }else{
									 Swal.fire({
										 icon: 'error',
										 title: 'Oops...',
										 text: 'Please Try Again Later',

									 });
								 }


							}
						});
					}
					function backtopayment(){
						$('#modalCreditPointBuy').show();
						$('#modalCreditPointPay').hide();
					}


					// $(".close").on('click', function(event){
					//  alert("close click");
					// });
					function showthisone(id){


						$('#selectedpackege').val(id);
						$('#paytype').text($('#cpName'+id).text());
						$('#payprice').text($('#cpprice'+id).text());
						$('#paydes').text($('#cpdesc'+id).text());

					 	$('#modalCreditPointBuy').hide();
						$('#modalCreditPointPay').show();

					}
					function openanother(){
						$('#resultmodel').modal('hide');
						$('#resultanswer').modal('show');
					}
				</script>


		<div id="resultanswer" class="modal fade" role="dialog">
			<div class="modal-dialog  ">

				<!-- Modal content-->
				<div class="modal-content">
					<div style="padding-bottom:0;" class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>

					</div>
					<div class="modal-body" style="padding: 0 3rem!important;">
						<input type="hidden" id="polltitke" value="<?=$posts_item['Po_Title']?>">
						<div style="display:flex;flex-direction:column;text-align: center;">
							<h5 style="margin-bottom:16px; font-weight:400;  ">Poll Result </h5>
						<h5 class="modal-title mb-3" align="center" style="  color: #458db4;margin-bottom: 10px;">

							 <?=$posts_item['Po_Title']?></h5></div>
								<div style="padding: 10px;
								background: #d0c8c80f;
								border-radius: 10px;
								margin: 5px;
								margin-bottom: 19px;">
							<?php

							function cal_percentage($num_amount, $num_total) {
								if($num_total == 0){
									return 0;
								}
								$count1 = $num_amount / $num_total;
								$count2 = $count1 * 100;
								$count = number_format($count2, 0);
								return $count;
							}

							foreach ($totalreview as $view){
										foreach ($view as $key => $val){

											?>

 											<h6 style="font-size: 12px;"><?php echo $key; ?></h6>
											<div class="progress mb-3">
												<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo cal_percentage($val, $totaldata)  ; ?>"
													 aria-valuemin="0" aria-valuemax="100" style="width:<?php echo cal_percentage($val, $totaldata) ; ?>%">
													<?php echo cal_percentage($val, $totaldata) ; ?>%
												</div>
											</div>

 									<?php	}
								?>

								<?php
							} ?>

								</div>
						<div class="row justify-content-between">
<style>
	#change-ans-btn{
			
			padding:13px 37px;
		}
	@media screen and (max-width:400px) {
		#change-ans-btn{
			max-width:184px;
			padding:0px 37px;
		}
		
	}
</style>

							<?php if( isset($this->LoggedInUser)){ ?>


						<button class="btn btn-outline-primary" onclick="showresultmodel()">View Details</button>
						<?php if ($polldata[0]['poll_stop'] == 0){ ?>
							<button id="change-ans-btn"    onclick="changeanswer()" style="position: absolute;
    left: 30%;
    color: #398cb3;
    margin-left: 5px;">Change Answer</button>
						<?php } ?>

							<?php	if($this->session->userdata['logged_in']['bs_id'] == $posts_item['Po_Us_ID'] || $member_info_post['role'] == 'superadmin') { ?>
								<div class=" " style="padding-left: 0;">
									<button class="btn   dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>
									<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 58px, 0px);">

										<a class="dropdown-item" href="javascript:void(0)"  ng-click="sendmessageforcreator()">Send Message / Reward</a>

										<?php

										if(isset($polldata) && !empty($polldata)){ ?>

										<?php if(isset($polldatacount) && !empty($polldatacount)){ ?>
											<a class="dropdown-item" href="javascript:void(0)"  onclick="deletepool()">Delete Poll</a>

												<?php if ($polldata[0]['poll_stop'] == 0){ ?>
												<a class="dropdown-item" href="javascript:void(0)"  onclick="endPoll()">End Poll</a>
												<?php  }else { ?>
													<a class="dropdown-item" href="javascript:void(0)"  onclick="ExtendPoll()">Extend Poll</a>

												<?php }?>
											<?php }else{ ?>
											<a class="dropdown-item" href="javascript:void(0)"  onclick="editpool()">Edit Poll</a>
										<?php } } ?>


									</div>
								</div>
<!--						<button style="    float: right;" class="btn btn-outline-primary">Send Message</button>-->
<!--							<div class="secondery_item">-->
<!--								<span class="secondery_btn" style="color: #398cb3!important;-->
<!--									font-size: 19px;-->
<!--									position: absolute;-->
<!--									right: 2px;-->
<!--									top: 10px;">-->
<!--										More-->
<!--								</span>-->
<!--								<ul class="secondery_dropdown expanded_dropdown" style="padding: 0px 22px!important;">-->
<!--									<li>-->
<!--										<a href="">-->
<!--											Share-->
<!--										</a>-->
<!--									</li>-->
<!---->
<!--									<li>-->
<!--										<a href="">-->
<!--											<i class="far fa-star"></i>-->
<!--											Add to favourites-->
<!--										</a>-->
<!--									</li>-->
<!---->
<!--								</ul>-->
<!--							</div>-->

					<?php } ?>
							<?php } ?>
						</div>
					</div>

					<div class="modal-footer">

					</div>
				</div>

			</div>
		</div>
		<div class="modal" id="demosas" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document" style="margin-top: 312px;
	border: 2px solid #06CBD1;
    width: 76%;
    border-radius: 10px;
    margin-left: auto;">
				<div class="modal-content" >
					<div class="modal-header">
						<h5 style="color:#088c90;font-weight: 500;" class="modal-title">Note:</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="padding: 0px 17px;">
						<p id="demosa"></p>

					</div>

				</div>
			</div>
		</div>
		<div id="sendmessage" class="modal fade" role="dialog">
			<div class="modal-dialog  ">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Send Messge</h4>

						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
					 <div class="col-sm-12 row">

					 <div class="col-12">
						 <label>Message</label>
						 <label>Note For: <span id="textpool"></span></label>
						 <input id="pollnoteid" value="" type="hidden">
						 <input id="pollnoteid2" value="" type="hidden">
						<input type="hidden" value="" id="keyvalue">
						<textarea class="form-control" id="notetextarea" style="margin-bottom: 10px;" rows="5" ></textarea>

						 <button style="float: right;" class="btn btn-success" onclick="discardmessageuser()">Discard</button>
						 <button style="float: right; margin-right: 10px;" id="sendmessagebutton" class="btn btn-success" onclick="savemessageuser()">Save</button>

					 </div>
					 </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>


		<script>
					let interval ;
				function starttime(date){
					// Set the date we're counting down to
					var countDownDate = new Date(date).getTime();


					// Update the count down every 1 second
					 interval = setInterval(function() {

						// Get today's date and time
						var now = new Date().getTime();

						// Find the distance between now and the count down date
						var distance = countDownDate - now;

						// Time calculations for days, hours, minutes and seconds
						var days = Math.floor(distance / (1000 * 60 * 60 * 24));
						var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
						var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
						var seconds = Math.floor((distance % (1000 * 60)) / 1000);

						// Output the result in an element with id="demo"
						// document.getElementById("demo").innerHTML = days + "d " + hours + "h "
						// 	+ minutes + "m " + seconds + "s ";
						 let timerclock ;
						 if(days > 0){
							 timerclock =    '<div class="row" style="display: inline-flex;">\n'+
							'							<div style="display: inline-flex;" class="col-12">\n'+
							'								<div style="border: 2px solid grey;" class="col">\n'+
							days+'d'+
							'								</div>\n'+
							'								<div class="col">\n'+
							hours+'h'+
							'								</div>\n'+
							'								<div class="col">\n'+
							minutes+'m'+
							'								</div>\n'+
							'								<div class="col">\n'+
							seconds+'s'+
							'								</div>\n'+
							'								\n'+
							'							</div>\n'+
							'						</div>';
					}else{
						 timerclock =    '<div class="row" style="display: inline-flex;">\n'+
							'							<div style="display: inline-flex;" class="col-12">\n'+

							'								<div class="col">\n'+
							hours+'h'+
							'								</div>\n'+
							'								<div class="col">\n'+
							minutes+'m'+
							'								</div>\n'+
							'								<div class="col">\n'+
							seconds+'s'+
							'								</div>\n'+
							'								\n'+
							'							</div>\n'+
							'						</div>';
					}

						 document.getElementById("demo").innerHTML = timerclock;
						// If the count down is over, write some text
						if (distance < 0) {
							clearInterval(interval);
							document.getElementById("demo").innerHTML = "EXPIRED";
						}
					}, 1000);
				}
		</script>

<style>
	@media only screen and (max-width: 480px) {

		#newpicofuser {
			/*margin-left: 193%;*/
		}
		p.lead.text-gray-med.p-0.m-0.mt-4.group_name {
			margin-left: 36%!important;
		}
	}
</style>
		<?php    if(isset($this->session->userdata['logged_in'])){?>
		<script src="<?php echo base_url(); ?>assetsofpop/js/script.js"></script>
		<?php  } ?>
<script>
				function selecteduser(id, userid){
					$('#'+userid+'userid'+id).prop('checked' , true);
					$('#selectedbutton'+id).hide();
					$('#unselectedbutton'+id).show();
				}

				function toggleselected(key){

					if($('#allsel'+key).text() == 'Select All'){
						$('.unselectedall'+key).show();
						$('.selectedall'+key).hide();

						$('.checkboxid'+key).prop('checked' , true);
						$('#allsel'+key).text('Unselect All')
					}else{
						$('.selectedall'+key).show();
						$('.unselectedall'+key).hide();
						$('.checkboxid'+key).prop('checked' , false);
						$('#allsel'+key).text('Select All')
					}
				}
			function unselected(id , userid){
					$('#'+userid+'userid'+id).prop('checked' , false);
					$('#unselectedbutton'+id).hide();
					$('#selectedbutton'+id).show();

			}
			function discardmessageuser(){

				$.ajax({
					url: base_url + 'account/discardpoolnote',
					method: 'POST',
					dataType: 'json',
					data: {
						notetextarea:  $('#notetextarea').val(),
						pollnoteid: $('#pollnoteid2').val(),

					},
					success: function (response2) {

						if(response2 == 1){

							Notify.success('Message Successfully Discarded' , {
								afterClose : function () {
									location.reload();
								}
							});
						}else{
							Notify.error('Some thing wrong Try again.' , {

							});
						}


					}
				});


			}

			function throuhmessage(id ,val   ){
					let ans = $('#notemessaage'+id).val();
				let userarray = [];
				$("input:checkbox[name=allchck"+id+"]:checked").each(function () {

						userarray.push($(this).val());

					});
					if(userarray.length == 0){
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'Please select one of the user',

						});
					}else{
						$.ajax({
							url: base_url + 'account/createcommentz',
							method: 'POST',
							dataType: 'json',
							data: {
								Co_Gr_ID:  $('#groupID').val(),
								Co_To_ID : 0 ,
								Co_St_ID : 0,
 								Co_Po_ID: $('#Post_ID').val(),
								Co_Message : ans,
								posttiele : $('#posttitle').text(),
								PollTitle : $('#polltitke').val(),
								commentuser : userarray
							},
							success: function (response2) {

 										if(response2 == 'success'){
												Notify.success('Message Successfully Send' , {
													afterClose : function () {
														// location.reload();
													}
												});
											}else{
												Notify.error('Try Again' , {
													afterClose : function () {

													}
												});
											}

							}
						});

					}


			}
	function savemessageuser(){
			let message =  $('#notetextarea').val();
		$.ajax({
			url: base_url + 'account/savepoolnote',
			method: 'POST',
			dataType: 'json',
			data: {
				notetextarea:  $('#notetextarea').val(),
				pollnoteid: $('#pollnoteid2').val(),
				textpool :  $('#textpool').text()
			},
			success: function (response2) {

				if(response2 == 1){

					Notify.success('Message Successfully Saved' , {
						afterClose : function () {
							// location.reload();
							$('#sendmessage').modal('hide');

							angular.element(this).scope().sendmessageforcreator( )
							$('#messagesecndbutton'+$("#pollnoteid").val()).show();

							$('#notemessaage'+$("#keyvalue").val()).val(message);
 						}
					});
				}else{
					Notify.error('Some thing wrong Try again.' , {

					});
				}


			}
		});

	}
			function sendMessage(keyy , keytype , id , messsage){

				$.ajax({
					url: base_url + 'account/gwtpoollquwstion',
					method: 'POST',
					dataType: 'json',
					data: {
						poais:  messsage,

					},
					success: function (response2) {

							console.log(response2);

								if(response2.length > 0){

									$('#notetextarea').val(response2[0]['Poa_note']);
									$('#keyvalue').val(keyy);
									if(response2[0]['Poa_note'] == ''){
										$('#sendmessagebutton').text('Save')
									}else{
										$('#sendmessagebutton').text('Update');
									}
									$('#pollnoteid').val(id);
									$('#pollnoteid2').val(messsage);

									$('#textpool').text(keytype);
									$('#resultmodel').modal('hide');
									$('#sendmessage').modal('show');

								}else{

									$('#keyvalue').val(keyy);

										$('#sendmessagebutton').text('Save')

									$('#pollnoteid').val(id);
									$('#pollnoteid2').val(messsage);
									$('#textpool').text(keytype);
									$('#resultmodel').modal('hide');
									$('#sendmessage').modal('show');

								}

						} 	});

				// $('#notetextarea').val($('#notemessaage'+keyy).val());
				// $('#keyvalue').val(keyy);
				// if($('#notemessaage'+keyy).val() == ''){
				// 	$('#sendmessagebutton').text('Save')
				// }else{
				// 	$('#sendmessagebutton').text('Update');
				// }
				// $('#pollnoteid').val(id);
				// $('#textpool').text(keytype);
				// $('#resultmodel').modal('hide');
				// $('#sendmessage').modal('show');
			}


		function showresultmodel(){

			$('#changetext').text('Poll result');
			$('.createmessage').hide();
			$('#resultanswer').modal('hide');
			 $('#resultmodel').modal('show');
		}

		function changeanswer(){
 			  $('#poolanswer').modal('show');
			  $('#resultanswer').modal('hide');
		}
	function cclosemodel(){

		Swal.fire({
			title: 'Discard Poll',
			text: "Are you sure you want to discard this poll?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, discard it'
		}).then((result) => {
			if (result.isConfirmed) {
				$('#poolmodel').modal('hide');
			}
		})


	}
</script>


		<script>
			function showvoucher(){
				$('#preview').hide();
				$('#modalInstantvoucher').show();
			}
			function showfron(){
				$('#modalInstantRewardCointCredit').hide();
				$('#modalInstantReward').show();
				$('#modalInstantReward').css("display" , "initial!important" );
			}
			function purchasecoin(){



				$('#instatnt').hide();
				$('#rewardpurcase').show();


				$('#modalInstantRewardCointCredit').hide();
				$('#modalpurchasecoins').show();
			}

			function makePostPromote(){

				$('#PromotedPostHeading').text('Promote Post');
				$('#newexpiry').text('Select Expiry Date');
 				$('#paymentpackageModal').modal('show');


  			}
			  function makePostextendpromote(){

					$('#PromotedPostHeading').text('Extend Promotion Date');
				    $('#newexpiry').text('Select new Expiry Date');
					$('#paymentpackageModal').modal('show');
 				}
			  function EndPostPromote(){
				  Swal.fire({
					  title: 'Are you sure you want to end Promotion?',
					  text: 'End Promotion',
					  icon: 'success',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'End Promotion'
				  }).then((result) => {
					  if (result.isConfirmed) {
						  $.ajax({
							  url: base_url + 'account/end_promoted_post',
							  method: 'POST',
							  dataType: 'json',
							  data: {
								  'PostId' :  $('#Post_ID').val(),
							  },
							  success: function (response) {

								if(response == '1'){
									$('#promotebutton').text('Promote');
									$('#promotebutton').removeClass('dropdown-toggle');
									$('#promotebutton').removeClass('dropding');
									$('#promotebutton').on("click", makePostPromote);
									Swal.fire({
										icon: 'success',
										title: 'Promotion Ended',
										text: 'You Ended Your Promotion on this post successfully',

									});

								}else{
									Swal.fire({
										icon: 'error',
										title: 'Error',
										text: 'Something went wrong, please try again',

									});
								}
							  }
						  });
					  }
				  })
			  }
			function selectPackage()
			{



				if($('#birthdate').val() == ''){
					Swal.fire({
						icon: 'warning',
						title: 'Expiry Date missing',
						text: 'Please Select expiry Date First',
						confirmButtonColor:'#15AFE8',
						confirmButtonText : 'Okay'

					});
					return;
				}

				let adminAccount =	'<?php if(isset($this->session->userdata['logged_in']['bs_admin'])){
									echo $this->session->userdata['logged_in']['bs_admin'];
				} ?>';

				if(adminAccount == 1){

					$.ajax({
						url: base_url + 'account/makepostpromote',
						method: 'POST',
						dataType: 'json',
						data: {
							post_id :  $('#Post_ID').val(),
							Group_id:  $('#groupID').val(),
							expiryDate : $('#birthdate').val(),
							packageID : 0,
							promoteType : 'post'
						},
						success: function (response) {

							if(response == 'success'){
								Swal.fire({
									icon: 'success',
									title: 'Promote Post Requested',
									text: 'Request to promote post is under review. Check your Permissions Notification to know if your ad is approved and promoted.',
									confirmButtonColor:'#15AFE8',
									confirmButtonText : 'Okay'

								});
								$('#promotebutton').text('Promoted');
								location.reload();
							}else if(response == "underreview"){
								Swal.fire({
									icon: 'info',
									title: 'Ad Promotion is for review',
									text: 'Check your Permissions Notification to know if your ad is approved and promoted..',

								});
								$('#promotebutton').text('Promoted');
								location.reload();
							}else if(response == "updateDate"){
								Swal.fire({
									icon: 'info',
									title: 'Ad Promotion Expiry is updated',
									text: 'your Promotion Expirty date is Updated',

								});
								$('#promotebutton').text('Promoted');
								location.reload();
							}
							else if(response == 'already'){
								Swal.fire({
									icon: 'info',
									title: 'Post is promoted',
									text: 'Check your promoted post in Explore Featured Section',

								});
								$('#promotebutton').text('Promoted');
							}
							else if(response == 'datemissing'){
								Swal.fire({
									icon: 'error',
									title: 'Error',
									text: 'Date missing',

								});
							}else{
								Swal.fire({
									icon: 'error',
									title: 'Error',
									text: 'Something went wrong, please try again',

								});
							}
						}

					});
				}else{
					showcontectus();
				}


			}
			function showanotherpopup(){
				$('#angsendmessage').modal('show');
			}
			var app = angular.module('myApp', []);
			app.controller('myCtrl', function($scope) {
				$scope.selected = [];


				$scope.proceedAmmount = function ( ) {


					$.ajax({
						url: base_url + 'account/sendcoinstorecipe',
						method: 'POST',
						dataType: 'json',
						data: {
							id: $('#coinpersion').val(),
							list: $scope.selected,
							post_id :  $('#Post_ID').val(),
							Group_id:  $('#groupID').val(),
						},
						success: function (response) {
							if(response == '1'){


								$('#coinRewardModal').modal('hide');
								$scope.selected = [];
								Swal.fire({
									icon: 'success',
									title: 'Reward Coins Sent',
									text: 'You may check your COINS transactions under the Notification page',
									confirmButtonColor : '#15AFE8'
								});
							}

						}

					});
				}

				$scope.createvoucher = function(){
							let texttshow = 'You will send reward voucher/s to selected '+$scope.selected.length+'  users';
					Swal.fire({
					title: 'Are you sure you want to continue?',
					text: texttshow,
					icon: 'success',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Send Voucher/s'
					}).then((result) => {
					if (result.isConfirmed) {
						$.ajax({
						url: base_url + 'account/VoucherRemainCount',
						method: 'POST',
						dataType: 'json',
						data: {
							'Voucher_PID' : $('#voucherpostID').val(),
						},
						success: function (response) {
								console.log(response);
								if(response[0]['Vp_vounchercount'] > $('#voucherpersion').val()  ){

									$scope.totalvoucher = {
										'Vr_title' :  $('#vouchertitle').val(),
										'Vr_sub_title' :  $('#vouchersubtitle').val(),
 										'Vr_vounchercount' :  $('#voucherpersion').val(),
										'Vr_deadline' :  $('#date').val(),
										'Vr_paisaammount' :  $('#paisaammount').val(),
										'Vr_radeemprice' :  $('#totalredeempay').val(),
										'Vr_decription' :  $('#decription').val(),
										'Vr_post_id' :  $('#Post_ID').val(),
										'Vr_Group_id':  $('#groupID').val(),
										'selectedusers' : $scope.selected,
										'Voucher_PID' : $('#voucherpostID').val(),
									}


									$.ajax({
										url: base_url + 'account/createVoucherCode',
										method: 'POST',
										dataType: 'json',
										data: 	$scope.totalvoucher,
										success: function (response) {
											console.log(response);
											Swal.fire({
												icon: response.type,
												title:  response.action,
												text: response.message,
												confirmButtonColor:'#15AFE8',
												confirmButtonText : 'Okay'

											});
										}
									});
								}else{
										Swal.fire({
											icon: 'error',
											title: response[0]['Vp_vounchercount'] + ': Voucher Left ',
											text: 'Sorry You dont have enough Voucher remain',

										});
								}
								return
						}
					});
					}
					})		 
					



				}
				$scope.editdetailsofvoucher = function (){
 						$scope.totalvoucheredit = {
 						'Vp_vounchercount' :  $('#voucherpersionedit').val(),
						'Vp_deadline' :  $('#dateedit').val(),
 						}

					$.ajax({
						url: base_url + 'post/editrewardvoucher',
						method: 'POST',
						dataType: 'json',
						data: {
							voucherID : $('#voucherpostid').val(),
							voucherData : $scope.totalvoucheredit

						} ,
						success: function (response) {
										console.log(response);
									if(response == 'success'){
										Swal.fire({
											icon: 'success',
											title: 'Reward Voucher Updated',
											text: 'You can only add more recipients and / or extend validity date when editing Reward Vouchers.',

										});
									}else{
										Swal.fire({
											icon: 'error',
											title: 'Something went wrong, try again',
											text: 'error',

										});
									}
									$('#coinRewardModalforEdit').modal('hide');
						 return false;
						}
					});
					return false;
				}

				$scope.showdetailsofvoucher = function(){


				$scope.totalvoucher = {
					'Vr_title' :  $('#vouchertitle').val(),
					'Vr_counchercount' :  $('#voucherpersion').val(),
					'Vr_deadline' :  $('#date').val(),
					'Vr_paisaammount' :  $('#paisaammount').val(),
					'Vr_radeemprice' :  $('#totalredeempay').val(),
					'Vr_decription' :  $('#decription').val(),
				}

 					$("#demo").text($('#date').val());
					$('#showdescription').text( $('#decription').val());
				    $('#vouchertitletext').text($('#vouchertitle').val())

					var first = $('#date').val();
 
 				$year =	first.split('-')[0];
				$month =	first.split('-')[1];
				$days =	first.split('-')[2];

				$('.month0').text($month[0]);
				$('.month1').text($month[1]);

				$('.day0').text($days[0]);
				$('.day1').text($days[1]);

				$('.year0').text($year[0]);
				$('.year1').text($year[1]);

				$('.year2').text($year[2]);
				$('.year3').text($year[3]);
					$('#modalInstantvoucher').hide();
					$('#modalInstantRewardCointCredit').hide();
		  			$('#preview').show();

		 }
			$scope.showdetails = function(){


					if($('#totalculculate').val() == 0){
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'Coin should not be are zero',

						});
						return;
					}

			if($('#totalculculate').val() > 0){


				 $.ajax({
					url: base_url + 'account/getusercredit',
					method: 'POST',
					dataType: 'json',
					data: {

					},
					success: function (response) {

						$('#modalInstantReward').hide();
						$('#modalInstantRewardCointCredit').show();
						$('#modalInstantRewardCointCredit').css('display' , 'initial!important');
						$('#totalsendingValue').text($('#totalculculate').val());

						$('#currentbalance').text(response[0].Us_Coins);

						$('#proceedpayment').hide()

						$('#buynowcoin').hide()

						if(parseInt($('#totalculculate').val()) > parseInt(response[0].Us_Coins)){
						let shortammount = 	parseInt($('#totalculculate').val()) - parseInt(response[0].Us_Coins);


							$('.shortcoins').text(shortammount);
							$('#buynowcoin').show()

						}else{

							$('#remaningcredit').text( parseInt(response[0].Us_Coins) - parseInt($('#totalculculate').val()))
							// proceed show
							$('#proceedpayment').show()
						}
					}
				});

			}else{
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'Coin should not be are zero',

				});
				return;
			}

			}
						$scope.nonrespondent = [];
				$scope.selecteallnon = function(data){

					$.ajax({
						url: base_url + 'account/getallmembers',
						method: 'POST',
						dataType: 'json',
						data: {
							Co_Gr_ID:  $('#groupID').val(),
 							Co_Po_ID: $('#Post_ID').val(),
							Co_Po_type : $('#privacy').val(),
 							commentuser : $scope.selected
						},
						success: function (response2) {
							response2;

							$scope.registerd = [];
							console.log( $scope.reviwer);
							$.each($scope.reviwer, function( index, value ) {

								$.each(value, function( index1, value1 ) {



									if( Array.isArray(value1) ){
										console.log(value1);
										if(value1.length > 0){
											for(var x = 0; x < value1.length; x++){

												$scope.registerd.push(value1[x]['Us_ID']);
												// for( var y = 0; y < $scope.selected.length; y++){
												// 	if(	$scope.selected[y] == value1[x]['Us_ID']){
												// 		$scope.selected.splice(y, 1);
												// 	}
												// }
											}
										}

									}

								});
							});

							for(var i = 0 ; i < response2.length; i++){
								// $scope.selected.push(response2[i]['Me_Us_ID'])
								for( var y = 0; y < $scope.registerd.length; y++){
									if(	$scope.registerd[y] == response2[i]['Me_Us_ID']){
										response2.splice(y, 1);
									}
								}
							}


							console.log("respohse" , response2);
							$scope.nonrespondent =  response2;
							var remember = document.getElementById('unanswer');
							if (remember.checked){


								for(var i = 0 ; i < response2.length; i++){
									$scope.selected.push(response2[i]['Me_Us_ID'])
								}

								$scope.$digest();
							}else{
								for(var i = 0 ; i < response2.length; i++){

									for(var o = 0 ; o < $scope.selected.length; o++){
										if($scope.selected[o] == response2[i]['Me_Us_ID']){
											$scope.selected.splice(o, 1);
										}
									}
								}
								// $scope.selecteallx();
							}
							$scope.$digest();
							console.log($scope.selected);
						}
					});

				}
						$scope.closemodel = function(){
							$('#angsendmessage').modal('hide');
						}
					$scope.addselected = function($id)
					{

						$scope.selected.push($id);
						$scope.$digest();
					}
					$scope.removeselected = function($id){
					var index = $scope.selected.indexOf($id);
					if (index !== -1) {
						$scope.selected.splice(index, 1);
					}
					$scope.$digest();
				}
					$scope.selectedbutton = function($id){


						$value = $scope.selected.indexOf($id);
						if($value !== -1){
							return true;
						}else{
							return false;
						}
				}
				$scope.discardmessage = function(id){
					let ans = $('#textareamessage').val();
					if(ans == ''){
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'Message is empty',

						});
						return;
					}

					$.ajax({
						url: base_url + 'account/savemessageofpoll',
						method: 'POST',
						dataType: 'json',
						data: {
							remove : 1,
							pollid : id,
							message : $('#textareamessage').val()
						},
						success: function (response2) {

							if(response2 == 1){
								Swal.fire({
									icon: 'success',
									title: 'Success',
									text: 'Message Remove Successfully',

								});
								$('#textareamessage').val('')
							}

						}
					});
				}
				$scope.savemessage = function(id){

					let ans = $('#textareamessage').val();
					if(ans == ''){
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'Message is empty',

						});
						return;
					}

					$.ajax({
						url: base_url + 'account/savemessageofpoll',
						method: 'POST',
						dataType: 'json',
						data: {

							pollid : id,
							message : $('#textareamessage').val()
						},
						success: function (response2) {

								if(response2 == 1){
									Swal.fire({
										icon: 'success',
										title: 'Success',
										text: 'Message Save Successfully',

									});
								}

						}
					});
				}
				$scope.sendingmessage = function(){


					let ans = $('#textareamessage').val();


								if(ans == ''){
									Swal.fire({
										icon: 'error',
										title: 'Oops...',
										text: 'Message is empty',

									});
									return;
								}
					if($scope.selected.length == 0){
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'Please select one of the user',

						});
					}else{
						$.ajax({
							url: base_url + 'account/createcommentz',
							method: 'POST',
							dataType: 'json',
							data: {
								Co_Gr_ID:  $('#groupID').val(),
								Co_To_ID : 0 ,
								Co_St_ID : 0,
								Co_Po_ID: $('#Post_ID').val(),
								Co_Message : ans,
								posttiele : $('#posttitle').text(),
								PollTitle : $('#polltitke').val(),
								commentuser : $scope.selected
							},
							success: function (response2) {

								if(response2 == 'success'){
									Notify.success('Message Successfully Send' , {
										afterClose : function () {
											$scope.selected = [];
											$('#textareamessage').val('')

											document.getElementById("allcheckbox").checked = false;
											document.getElementById("unanswer").checked = false;
											$('#angsendmessage').modal('hide');
											$('#btn_comment_search_reset').click();

										}
									});
								}else{
									Notify.error('Try Again' , {
										afterClose : function () {

										}
									});
								}

							}
						});

					}

				}

				$scope.editReward = function(){

					$.ajax({
						url: base_url + 'post/getpostvoucher',
						method: 'POST',
						dataType: 'json',
						data: {
							id :   $('#Post_ID').val()
						},
						success: function (response) {



							  $('#vouchertitleedit').val(response[0]['Vp_title'])
							$('#vouchersubtitleedit').val(response[0]['Vp_Sub_title']);
							  $('#voucherpersionedit').val(response[0]['Vp_vounchercount'])

							$('#voucherpersionedit').attr('min' , response[0]['Vp_vounchercount']);
							  $('#dateedit').val(response[0]['Vp_deadline'])
							$('#dateedit').attr('min' , response[0]['Vp_deadline'])
								  $('#paisaammountedit').val(response[0]['Vp_paisaammount'])
								  $('#totalredeempayedit').val(response[0]['Vp_radeemprice'])
									 $('#decriptionedit').val(response[0]['Vp_decription'])
 								$('#voucherpostid').val(response[0]['Vp_id']);
							$('#coinRewardModalforEdit').modal('show');

						}
					});

				}
				$scope.polldata = {};
				$scope.reviwer = {};
				$scope.totalreview = {};
				$scope.countobj = function(obj){
					return obj.length;
				}
				$scope.selectname = '';
				$scope.totalculculate = 0;
				$scope.selecteallx = function(data){

					var remember = document.getElementById('allcheckbox');
					$.each($scope.reviwer, function( index, value ) {

						$.each(value, function( index1, value1 ) {
						if(Array.isArray(value1)){
 								for(var x = 0 ; x <  value1.length ; x++){

								if (remember.checked){
									$scope.addselected(value1[x]['Us_ID']);
								}else{
									$scope.removeselected(value1[x]['Us_ID']);
								}
							}
						}
 						});
					});
					$scope.$digest();
				}
				$scope.selecteall = function(data ){
					console.log(data.checked);

					$scope.selectname = data.getAttribute('data-keyname');
					$scope.selectarray = [];
					$scope.singleobj = [];

					$.each($scope.reviwer, function( index, value ) {

						$.each(value, function( index1, value1 ) {

							if($scope.selectname == index1){
								$scope.selectarray = value1
							}

						});
					});

					console.log('$scope.selectarray' , $scope.selectarray);

					for(var x = 0 ; x <  $scope.selectarray.length ; x++){

						if (data.checked){
							$scope.addselected($scope.selectarray[x]['Us_ID']);
						}else{
							$scope.removeselected($scope.selectarray[x]['Us_ID']);
						}
 					}
					$scope.$digest();
				}
				$scope.cal_percentage = function ($num_amount, $num_total) {
					if($num_total == 0){
						return 0;
					}
					$count1 = $num_amount / $num_total;
					$count2 = $count1 * 100;
					$count = parseInt($count2);
					return $count;
				}

				$scope.countperson = function(val) {

					let numberod = 	$('#coinpersion').val() != '' ?  parseInt($('#coinpersion').val()) : 1;

					$scope.totalculculate = 	val * numberod;
					 $('#totalculculate').val($scope.totalculculate);
				}
				$scope.voucherperperson = function ( ) {

				$paisaAmmount = 	$('#paisaammount').val();
				$numberofvoucher = 	$('#voucherpersion').val();

				$totalvalue = 	parseInt($paisaAmmount) * parseInt($numberofvoucher);
				    $totalvalue = $totalvalue * 0.12;
					$totalvalue =  $totalvalue / parseInt($numberofvoucher);

					$totalvalue = Math.round($totalvalue);
					if($totalvalue  < 5){
						$totalvalue = 5;
					}else if($totalvalue > 5 ){
						$totalvalue = $scope.round5($totalvalue);
					}
					console.log($totalvalue);
					$('#totalredeempay').val($totalvalue);
				}
				 $scope.round5 = function(x)
				{
					return Math.ceil(x/5)*5;
				}
				$scope.coinperperson = function(val) {

					let numberod = 	$('#persioncount').val() != '' ?  parseInt($('#persioncount').val()) : 1;
						 $scope.totalculculate = 	val * numberod ;
					$('#totalculculate').val($scope.totalculculate);
				}
			$scope.openvoucher = function(){

					console.log("userdatalist" , userdatalist);
 					for( var x = 0; x < userdatalist.length; x++){
					 var index = $scope.selected.indexOf(userdatalist[x]['Vr_Us_ID']);
						 if (index !== -1) {
							$scope.selected.splice(index, 1);
							 $scope.$digest();
						}
					}
					$found = false;

						console.log($scope.selected.length);
						for(var x = 0 ; x < $scope.nonrespondent.length ; x++){
							for(var  y = 0 ; y < $scope.selected.length ; y++){
								if($scope.nonrespondent[x]['Me_Us_ID'] = 	$scope.selected[y]){
									$found = true;
								}
							}
						}
						if($found == false){
							$('#unanswer').prop('checked', false);
						}


				$.each($scope.reviwer, function( index, value ) {

					if($('.selectedbutton'+index).is(":visible")){
						 
					}else{

						$('#index'+index).prop('checked', false);
					}

				})
					if($scope.selected.length  == 0)
					{
						Swal.fire({
							icon: 'error',
							title: 'Wrong Selection',
							text: 'You cannot send reward. You’re the one who created this reward post.',
							confirmButtonColor:'#15AFE8',
							confirmButtonText : 'Okay'

						});

			return;

					}

					$('#preview').hide();
					$('#voucherpersion').val($scope.selected.length);
 					$('#modalInstantReward').hide();
				$('#angsendmessage').modal('hide');
				$("#coinRewardModal").modal('show');
				$('#modalInstantvoucher').show();
			}
				$scope.shownote =function (value) {

					$('#demosas').modal('show');
					$('#demosa').text($('#subtext'+value).val());
				}

				$scope.sendmessageforcreator = function(){

					$('#resultanswer').modal('hide');

					$.ajax({
						url: base_url + 'account/getpollRecordbyPostID',
						method: 'POST',
						dataType: 'json',
						data: {
 						id :  $('#Post_ID').val()
						},
						success: function (response) {

 							$scope.polldata = response.polldata;
							$scope.reviwer = response.reviwer;
							$scope.totalreview = response.totalreview;
							$('#angsendmessage').modal('show');
							$scope.$digest();

						}
					});
				}
			});
		</script>
		<script>
			$(document).ready( function() {

				setTimeout(function (){
					// alert("hello");
					// $('#birthdate').datepicker({
					// 	dateFormat: 'yy-mm-dd'
					// });
				} , 4000)
			});
		</script>

		<script>


			function showdetailsofvoucher(){
				angular.element(document.getElementById('gp_main')).scope().showdetailsofvoucher( )
			}
			function editdetailsofvoucher(){
				angular.element(document.getElementById('gp_main')).scope().editdetailsofvoucher( )
			}
 			function voucherperperson( ){
					angular.element(document.getElementById('gp_main')).scope().voucherperperson( )

				}
			function coinperperson(val){

				angular.element(document.getElementById('gp_main')).scope().coinperperson(val)

			}
			function countperson(val){
				angular.element(document.getElementById('gp_main')).scope().countperson(val);
			}


			function openvoucher(){
				angular.element(document.getElementById('gp_main')).scope().openvoucher();

 
 				}
			 function openreward(){

				$('#preview').hide();
				 $('#modalInstantvoucher').hide();
				 $('#instatnt').show();
				 $('#rewardpurcase').hide();
				 $('#coinpersion').val('');
				 $('#totalculculate').val('');

				 $('#modalInstantRewardCointCredit').hide();

				 $('#modalInstantReward').show()
					$('#angsendmessage').modal('hide');
					$("#coinRewardModal").modal('show');
			 }
			//let userlogin = '<?php //   if(isset($this->session->userdata['logged_in'])){
			//		echo 	 $this->session->userdata['logged_in'] ;
			// }else{
			//	 echo '';
			// }  ?>//';
			// if(userlogin != ''){
				 $(document).ready(function(){
					 checkUserList();
				 });
			 // }


		</script>

<style>
	button.add_member_btn.add_member.btn.unselectbutton1:hover {

		background-color: #e2e6ea;
		border-color: #dae0e5;

	}
	</style>

		<script>


		</script>


