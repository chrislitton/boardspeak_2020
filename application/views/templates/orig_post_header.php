<?php
	$GroupName = $posts_item['Gr_Name'];
	if (strlen($GroupName)>=25) $GroupName = substr($GroupName,0,25)."... ";

	$TopicName = $posts_item['TopicName'];
	if (strlen($TopicName)>=25) $TopicName = substr($TopicName,0,25)."... ";

	$SubTopicName = $posts_item['SubTopicName'];
	if (strlen($SubTopicName)>=25) $SubTopicName = substr($SubTopicName,0,25)."... ";

	$Title = $posts_item['Po_Title'];
	if (strlen($Title)>=25) $Title = substr($Title,0,25)."... ";

	$Photo = "";
	if (strlen($posts_item['Po_Photo'])>0) $Photo = '<img class="img-fluid my-3" src="'.base_url(). $posts_item['Po_Photo'].'">';

	$TopicCrumb = '';
	if (strlen($TopicName)>0)	$TopicCrumb = '<li class="breadcrumb-item"><a href="'. base_url() .'account/topic/'.$posts_item['Po_To_ID'].'">'.$TopicName.'</a></li>';

	$SubTopicCrumb = '';
	if (strlen($SubTopicName)>0)	$SubTopicCrumb = '<li class="breadcrumb-item"><a href="'. base_url() .'account/subtopic/'.$posts_item['Po_St_ID'].'">'.$SubTopicName.'</a></li>';

	if($posts_item['Po_Ca_ID'] == 0){
		$catName = $posts_item['Po_Ca_Name'];
	}else{
		$catName = $posts_item['Ca_Name'];
	} 
?>
	<!-- main content start here -->
	<main id="gp_main" class="gp_main container">
	 	<div>
	 		<div class="col-md-12">
			 	<nav aria-label="breadcrumb">
					<ul class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/view/group/<?php echo encode_id($posts_item['Gr_ID']);?>"><?=$posts_item['Gr_Name']?></a></li>
						<li class="breadcrumb-item text-primary"><?=$catName;?></li>
						<li class="breadcrumb-item active" aria-current="page"><?=$Title?></li>
					</ul>
				</nav>
			</div>
	 		<div class="col-md-12">
				<section class="banner_desc">
					<!-- group desc section -->
					<div class="group_desc">
						<div class="group_desc_container container">
							<div class="group_text">
								<h4 class="group_name"><a href="<?php echo base_url(); ?>account/view/post/<?php echo $EncodedID;?>"><?php echo $posts_item['Po_Title'];?></a></h4>
								<p class="group_metta">
									<span class="group_badge group_badge_<?=strtolower($posts_item['Po_Privacy'])?>"><?php echo ucfirst($posts_item['Po_Privacy']);?></span>

									<span class="group_hearts">
										<i class="fas fa-heart"></i>&nbsp;<?php echo $posts_item['Po_Likes'];?></span>

									<span class="group_memebers">
										3225
									</span>
									members
								</p>

								<pre class="group_taggline"><?=$posts_item['Po_Description']?></pre>
							</div>
							<div class="group_btn">
								<!-- only visible if board is followed -->
								<?php if ($member_info['status'] == 'success' || $this->session->userdata['logged_in']['bs_id'] == $posts_item['Po_Us_ID']) { ?>
									<?php if ($member_info['is_invited'] && ($member_info['role_status'] == 'pending')) { ?>
										<a class="action_button invite_response_btn btn btn-xl btn-danger mb-2 mr-2 mt-2" href="javascript:void(0)" data-action="accept">Accept</a>
										<a class="action_button invite_response_btn btn btn-xl btn-light mb-2 mr-2 mt-2" href="javascript:void(0)" data-action="decline">Decline</a>
										<a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>	
									<?php } else { ?>
										<a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2 <?php echo ($member_info['role_status'] == 'pending') ? 'btn_pending' : 'invite_member_btn'; ?>" href="javascript:void(0)"><?php echo ($member_info['role_status'] == 'pending') ? 'Pending' : 'Invite  <li class="fa fa-plus" style="margin-left: 4px;"></li>'; ?></a>
									<?php } ?>
								<?php } else { ?>
									<a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2" id="btn_join" href="javascript:void(0)">Join</a>
									<a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>	
								<?php } ?>
								
								<div class="secondery_item">
									<span class="secondery_btn">
										<i class="fas fa-ellipsis-h"></i>
									</span>
									<ul class="secondery_dropdown">
										<li>
											<a href="">
												Share
											</a>
										</li>
										<li>
											<a href="">
												<i class="far fa-star"></i>
												Add to favourites
											</a>
										</li>
										<li>
											<a href="">
												Pin
											</a>
										</li>
										<?php
											if (strpos($_SERVER['REQUEST_URI'], '/postinfo/') != 0) {
												echo '<li><a href="'. base_url() . 'account/postinfo/edit/'.$EncodedID .'">Edit</a></li>';
											}
										?>
										<li>
											<a href="">
												Post Settings
											</a>
										</li>
										<li>
											<a href="">
												Notify Me
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-12 col-lg-12 px-0 py-3 group_container_info">
								<p class="lead text-gray-med p-0 m-0 mt-1">
								<?php
									if (strpos($_SERVER['REQUEST_URI'], '/postinfo/') === false) {
										echo '<a class="editoption <?php echo $ShowAdminAction;?>" href="'.base_url().'account/postinfo/detail/'.$EncodedID.'">Post Info</a>';
									}
								?>
								</p>
							</div>
						</div>
					</div>

					<!-- banner section -->
					<div class="gp_banner">
						<div class="banner_container">
							<div class="col-md-12 col-lg-12 board-banner text-right bg-topic post-cover-image <?php echo $posts_item['Po_Backcolor']; ?>" 
								<?php if(strlen($posts_item['Po_Photo'])>0) echo 'box-shadow: 1px 0px 5px 3px #80808033;style="background:transparent url('.base_url() . str_replace(base_url(), '', $posts_item['Po_Photo']).') no-repeat center center /cover; background-size: 100% 100%;"';?> >
								<div class="col-md-12 col-lg-12" style="height: 100%;">
									<div class="btn btn-primary mt-3 btn-upload-cover-image" style="  border-radius: 9px!important;  background: #7476779e!important;
                                                                                                                                                                                                     border: 2px solid white!important;
                                                                                                                                                                                                     border-radius: 0px!important;
                                                                                                                                                                                                     color: white!important;
                                                                                                                                                                                                     font-weight: 600!important;">
										<form method="post">
											<label class="title-tip title-tip-left"  title="Change Board Cover" for="upload_image" style="margin-bottom:0;">
												<img src="" id="uploaded_image" class="img-responsive img-circle" />
												<i class="fas fa-camera "></i>
												<input type="file" name="image" class="image" id="upload_image" style="display:none" accept="image/*" />
											</label>
										</form>
									</div>
							    </div>
							</div>

							<div class="group_btn">
								<span class="group_hearts">
									<i class="far fa-heart"></i>
								</span>
								<?php if (!empty($posts_item['Po_Bn_ID']) || !empty($posts_item['Po_Bn_Name'])) { ?>
									<a class="btn invite_member_btn btn post_action_button" href='#' data-toggle='modal' data-target="#external_link_modal">
										<?=((!empty($posts_item['Po_Bn_ID'])) ? $posts_item['Bn_Caption'] : $posts_item['Po_Bn_Name'])?>
									</a>



									<!-- <button class="btn invite_member_btn btn post_action_button" type="button" onclick="window.open('<?=prep_url($posts_item['Po_Bn_Page'])?>','_blank');">
										<?=((!empty($posts_item['Po_Bn_ID'])) ? $posts_item['Bn_Caption'] : $posts_item['Po_Bn_Name'])?>
									</button> -->
								<?php } ?>
								
								<span class="group_hearts">
									<i class="far fa-star"></i>
								</span>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
 <input type="hidden" name="PostID" value="<?=$EncodedID?>" />
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
        		<div class="img-container">
            		<div class="row">
                		<div class="col-md-8">
                    		<img src="" id="crop_uploaded_image" />
                		</div>
                		<div class="col-md-4">
                    		<div class="preview"></div>
				      		<div class="modal-footer" style='padding-right: 0px;'>
				      			<button type="button" id="crop" class="btn btn-primary">Upload</button>
				        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				      		</div>
                		</div>
            		</div>
        		</div>
      		</div>
    	</div>
  	</div>
</div>
<div class="modal fade" id="external_link_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
        		<iframe></iframe>
      		</div>
    	</div>
  	</div>
</div>
