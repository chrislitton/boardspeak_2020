<?php 
	$this->load->helper('url');
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" type="images/icon" href="<?php echo base_url(); ?>img/icon.png" />
  <meta name="description" content="">
  <meta name="author" content="">

<!--	--><?php //$data[''] ?>
  <title>BoardSpeak - Get more things done!</title>

  <!-- Custom fonts for this theme -->
  <link href="<?php echo base_url(); ?>vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">


  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/slick/slick-theme.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl-carousel/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl-carousel/owl.theme.default.min.css">

  <!-- Theme CSS -->  
  <link href="<?php echo version_url('css/freelancer.css'); ?>" rel="stylesheet">
  <link href="<?php echo version_url('css/custom.css'); ?>" rel="stylesheet">
  <link href="<?php echo version_url('assets/css/pop-up.css'); ?>" rel="stylesheet">
  <!-- <link href="<?php echo base_url(); ?>css/loader.css" rel="stylesheet"> -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>vendor/ckeditor/ckeditor.js"></script>

  <!--Dropify-->
  <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
            integrity="sha256-AWdeVMUYtwLH09F6ZHxNgvJI37p+te8hJuSMo44NVm0="
            crossorigin="anonymous" />

  <!-- Select2 -->
  <link href="<?php echo base_url(); ?>assets/css/datatable/jquery.dataTables.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/select2/select2.min.css" rel="stylesheet">
  <?php if (isset($this->session->userdata['logged_in']) && isset($asset)){ ?>
    <link href="<?php echo version_url('assets/css/modules/'.$asset.'.css'); ?>" rel="stylesheet">
  <?php } ?>

  <!-- Dropzone -->
  <link href="<?php echo base_url(); ?>assets/css/dropzone/dropzone.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/js/dropzone/dropzone.js"></script>

  <!-- Cropper -->
  <link href="<?php echo base_url(); ?>assets/css/cropper/cropper.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/js/cropper/cropper.js"></script>
  <link href="<?php echo base_url(); ?>assets/noty/lib/noty.css" rel="stylesheet" type="text/css">
  <style>
    /* importing fonts */
    @font-face {
      font-family: moon-light;
      src: url("<?php echo base_url(); ?>vendor/webfonts/Moon Light.otf");
    }
    @font-face {
      font-family: moon-bold;
      src: url("<?php echo base_url(); ?>vendor/webfonts/Moon Bold.otf");
    }
    @font-face {
      font-family: lato-regular;
      src: url("<?php echo base_url(); ?>vendor/webfonts/Lato-Regular.ttf");
    }
    @font-face {
      font-family: lato-bold;
      src: url("<?php echo base_url(); ?>vendor/webfonts/Lato-Bold.ttf");
    }
    @font-face {
      font-family: montserrat-regular;
      src: url("<?php echo base_url(); ?>vendor/webfonts/Montserrat-Regular.ttf");
    }
    @font-face {
      font-family: montserrat-bold;
      src: url("<?php echo base_url(); ?>vendor/webfonts/Montserrat-Bold.ttf");
    }
    @font-face {
      font-family: 'Glyphicons Halflings';
      src: url("<?php echo base_url(); ?>vendor/webfonts/glyphicons-halflings-regular.ttf");
    }
  </style>
</head>

<body id="page-top">
<div class="loader">
	<img src="<?php echo base_url(); ?>img/loader.gif">
</div>
