<!-- Masthead -->
  <header class="dethead bg-primary text-white text-center <?php echo $users_item['Us_Backcolor']; ?>" style="background:transparent url('<?php echo $users_item['Us_Background']; ?>') no-repeat center center /cover">
    <!-- <div class="container d-flex align-items-center flex-column"> -->
    <div class="container">
  			<div class="row user-banner">  			
   				<div class="col-md-12 col-lg-12 text-right">	
  					<a class="btn btn-primary changebgbtn" href="<?php echo base_url(); ?>account/settings/background"><i class="fa fa-camera pr-2"></i>change background</a>
	     		</div>
	     	</div>
	</div>	
  </header>
  
  <section class="user-menu-section" id="usermenu">
  	<div class="container">
   	<div class="row">
   			<div class="col-md-3 col-lg-3 mb-3 text-center">	    			
		    		<div class="rounded-circle user-photo" style="background:transparent url('<?php echo $users_item['Us_Thumb']; ?>') no-repeat center center /cover"></div>
		    		<a href="<?php echo base_url(); ?>account/settings/photo"><i class="fa fa-camera pr-2"></i>change photo</a>
		    </div>
	    	<div class="col-md-9 col-lg-9 text-responsive-align">
   					<div class="row">
	    				<div class="col-md-7 col-lg-7 mb-3">
				    		<h3 class="text-uppercase mb-0"><a href="<?php echo base_url(); ?>account/profile"><?php echo  (!empty($users_item['Us_Alias'])) ? $users_item['Us_Alias'] : $users_item['Us_Name']; ?></a></h3>
								<?php echo $users_item['Us_JobTitle']; ?>
							</div>

							<div class="col-md-5 col-lg-5 text-center">
								<div class="postsinfobox"><p><b><?php echo $users_item['Us_Groups']; ?></b></p><p>Groups</p></div>
								<div class="postsinfobox"><p><b><?php echo $users_item['Us_Topics']; ?></b></p><p>Topics</p></div>
								<div class="postsinfobox"><p><b><?php echo $users_item['Us_Posts']; ?></b></p><p>Posts</p></div>
							</div>
					</div>		 			
	    	</div>	    	  	
	</div>	
	</div>
  </section>


