<header id="header" class="header sticky-top">
	<nav class="main_nav_container container" id="mainNav">

		<div class="mobile_icon" disabled>
			<a id="mobile_menu_collase">
				<span></span>
				<span></span>
				<span></span>
			</a>
			<span  class="dot1" style="    position: absolute;  top: 27px;  left: 62px;" onclick="clicknotifiocion()" style=" display:none;" id="notificatioCountersecond">
             </span>
		</div>

		<div class="mobile_nav" id="mobile_nav">

			<ul class="mobile_nav_container container">

				<?php  if($this->uri->segment(2) != 'explore' && $this->uri->segment(2) != 'profile'  ){ ?>

					<li class="search_box">
						<div class="input-group mb-3">
							<input class="form-control" type="text" autocomplete="off" placeholder="Search" id="searchbarmobilbe">
							<div class="input-group-append">
								<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
							</div>
						</div>
						<div class="content-list" id="list" style=" display:none;  position: absolute;  left: 21px;  color: black;  background: #388cb3;  box-shadow: 1px 1px 1px 1px grey;
									border-radius: 5px;">
							<ul class="drop-list"  style="    max-height: 400px;  overflow-y: scroll; overflow-x:hidden; " id="saerchlistsohow">

							</ul>
						</div>
					</li>


				<?php } ?>

				<li class=""><a class="" href="<?php echo base_url(); ?>account/explore">Explore</a></li>
				<li class="expanded_menu">
					<a class="mobile_sub">
						<i class="fas fa-user"></i>
						<i class="fas fa-caret-down"></i>
					</a>
					<ul class="sub_nav expanded_menu">
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/profile">Profile/Dashboard</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/notifications">Notifications</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="javascript:void(0)"  data-toggle="modal" data-target="#ContactUsForm">Help & Support</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/settings">Settings</a></li>
						<?php if ($bs_admin=='1') { ?>
							<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>admin">Administrator</a></li>
						<?php } ?>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>pages/view/logout">Log Out</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<a class="logo_img" href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>img/logo.png" class="img-fluid"></a>

		<!-- only desktop nav -->
		<div class="desk_nav row">
			<ul class="desk_nav_container">
				<?php



				if($this->uri->segment(2) != 'explore' && $this->uri->segment(2) != 'profile'  ){ ?>
					<li class="only_desk_item search_box">
						<input class="form-control" id="searchfilterID" type="text" placeholder="Search" autocomplete="off">
						<i class="fas fa-search"></i>

						<div class="content-list" id="list1" style=" display:none;  position: absolute;  left: 21px;  color: black;  background: #388cb3;  box-shadow: 1px 1px 1px 1px grey;
										border-radius: 5px;     width: 317px;">
							<ul class="drop-list"  style="    max-height: 380px;  overflow-y: scroll!important; " align="left" id="saerchlistsohow1">
							</ul>
						</div>

					</li>
				<?php } ?>
				<li class="only_desk_item">
					<a class="" href="<?php echo base_url(); ?>account/explore">Explore</a>
				</li>
				<li class="">
					<a class="active sub_nav_container <?= $subNavContainer; ?>" >
						<span>Create Group</span>
						<i class="fas fa-angle-down"></i>
					</a>
					<ul class="sub_nav">
						<li><a class="nav-link px-3  <?= $listNav; ?>" href="<?php echo base_url(); ?>account/create/group">Start a Group</a></li>
						<!--                		<li><a class="nav-link px-3" href="#" data-toggle="modal" data-target="#createTopicModal">Create a SubGroup</a></li>-->
						<!--						<li><a class="nav-link px-3 pr-5" href="--><?php //echo base_url(); ?><!--account/create/post">Make a Post</a></li>-->
					</ul>
				</li>
				<li class="only_desk_item">
					<a class="sub_nav_container row">
						<span  class="dot " onclick="clicknotifiocion()" style=" display:none;" id="notificatioCounter">
						</span>
						<?php
 						if(isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ ?>
							<img class="rounded-circle " style="    width: 33px;

                                        margin: -5px 7px;
                                        border: 1px solid #8080809c;" src="<?php echo $this->session->userdata['logged_in']['Us_Photo']; ?>">
						<?php }else{ ?>
							<i class="fas fa-user-circle"></i>
						<?php } ?>



						<i class="fas fa-angle-down"></i>
					</a>

					<ul class="sub_nav">
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/profile">Profile / Dashboard</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/notifications">Notifications</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/pricing">Pricing Plan</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="javascript:void(0)"  data-toggle="modal" data-target="#ContactUsForm">Help & Support</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/settings">Settings</a></li>
						<?php if ($bs_admin=='1') { ?>
							<?php

							$this->db->select('bs_inquiries.*');
							$this->db->from('bs_inquiries');
							$resuult = 	  $this->db->get()->result_array();
							;
							?>
							<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>admin">Administrator <span class="dot " style="background:green!important;"><?php print_r(count($resuult)); ?></span></a></li>
						<?php } ?>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>pages/view/logout">Log Out</a></li>
					</ul>
				</li>



			</ul>
			<a href="<?php echo base_url(); ?>account/Coins">
				<img id="bcoinimage" class="img-fluid" src="<?php echo base_url(); ?>img/bcoin.png" style="
    width: 40px;" alt="about-pic">
			</a>

		</div>
	</nav>

</header>
