	
<!-- Masthead -->
<header class="dethead text-white" id="boardmenu">
	<!-- <div class="container d-flex align-items-center flex-column"> -->
	<div class="container">

		<div class="row bg-light">
			<div class="col-md-12 col-lg-12">
				<nav class="navbar navbar-expand-lg navbar-light justify-content-end">
					<!-- Navigation -->
					<button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-white rounded" type="button" data-toggle="collapse" data-target="#navSubMenu" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
						More <i class="fas fa-bars"></i>
					</button>
					<div class="collapse navbar-collapse" id="navSubMenu">
						<ul class="navbar-nav nav" role="tablist" id="navtab">
							<li class="nav-item mx-0 mx-lg-1">
								<a class="nav-link" href="<?php echo base_url(); ?>admin">Admin's</a>
							</li>
							<li class="nav-item mx-0 mx-lg-1">
								<a class="nav-link" href="<?php echo base_url(); ?>admin/users">Users</a>
							</li>
							 <li class="nav-item mx-0 mx-lg-1">
								<a class="nav-link" href="<?php echo base_url(); ?>admin/background">Background</a>
							</li>
							 <li class="nav-item mx-0 mx-lg-1">
								<a class="nav-link" href="<?php echo base_url(); ?>admin/category">Category</a>
							</li>
							 <li class="nav-item mx-0 mx-lg-1">
								<a class="nav-link" href="<?php echo base_url(); ?>admin/keyword">Keyword</a>
							</li>
							 <li class="nav-item mx-0 mx-lg-1">
								<a class="nav-link" href="<?php echo base_url(); ?>admin/message">Messages</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>



   
	
