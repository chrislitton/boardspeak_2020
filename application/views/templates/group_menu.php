<section class="board-toolbar-section">  
  	<div class="container">
		
		<!-- start of category -->
		<div class="row mb-1">

		    <div class="col-md-10 col-lg-10 bg-light py-2">

				<div class="variable categoryFilter slider">

					<div class="item bg-color">
						<button class="board-link btn-category-chooser"  >
							<a style="    margin-top: 14px;" href="<?php echo base_url(); ?>account/view/group/<?php echo $EncodedID;?>"><img style="    width: 35px;" src="<?php echo base_url()."assetsofpop/img/Reload_Icon.svg "; ?>" ></a>
 						</button>
					</div>
					<?php
						$i = 0; 
						foreach($category_items as $item) :
							$i++; if ($i==10) $i = 1;
							if($item['ID']) :
					?>
								<div class="item bg-color<?php echo $i;?>">
									<button class="board-link btn-category-chooser" id="<?=$item['ID'];?>" >
										<p class="text-white cat-link"><?php echo $item['Name'];?></p>
									</button>
								</div>
					<?php 
							endif;
						endforeach; 
					?>

				</div>
			</div>
	    </div>

		<div class="row mb-1">
			<!-- start of dropdown -->
		    <div class="col-6 py-2" style="padding-left: 0;">
				<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sort</button>
				<div class="dropdown-menu" style="width
				328px;">
<!--					 <a class="dropdown-item" href=">reset</a> -->
					<a class="dropdown-item" href="javascript:void(0)" onclick="getpopularPost()"> <i class="changeheart fa fa-heart" style="color:#06cbd1"></i> &nbsp;Popular</a>
					<a class="dropdown-item" href="javascript:void(0)" onclick="getfavPost()"><i class="far fa-star" style="color:#ffc107"></i>&nbsp; Favourite</a>
					<a class="dropdown-item" href="javascript:void(0)" onclick="getprivatepost()" style="    display: inline-flex;">
<!--						<i class="far fa-star" style="color:#ffc107"></i>-->
						<img style="width: 24px;" src="http://localhost/boardspeak_2020/assets/img/lock.png" alt="image" style=""> &nbsp;
						<span>Private</span></a>
					<a class="dropdown-item" href="javascript:void(0)" style="display: inline-flex" onclick="getSubgroup()" >
						<img style="width: 24px;" src="http://localhost/boardspeak_2020/assetsofpop/img/SubGroup_Icon.png" alt="image" style=""> &nbsp;

						<span>Sub Groups</span></a>

					<div role="separator" class="dropdown-divider"></div>
					<a class="dropdown-item" href="#">Separated link</a>
				</div>
			</div>
			<!-- end of dropdown -->

			<!-- start of search field -->
			<div class="col-6 py-2 form-group" style="padding-right: 0;">
				<?php echo form_open('account/view/group/'.$EncodedID); ?>
					<div class="control-group input-group col pr-0">
						<input style="width: 69%" class="form-control" id="txtSearch" name="txtSearch" value="<?php echo $txtSearch;?>" type="text" placeholder="Search in this group">
						<div class="input-group-append">
							<button class="btn btn-primary"style="padding:0;" id="btn_search_text" style="border-top-left-radius: 0 !important; border-bottom-left-radius: 0 !important;"><i class="fa fa-search py-1 px-2 text-white"></i></button>
						</div>
						
					</div>
				</form>
			</div>
			<!-- end of search field -->
	    </div>

	    <!-- start of subcategory -->
		<div class="row group_post_filter_row" style='display:none;'>
			<div class="col-md-12 col-lg-12 bg-light py-2">
				<!-- <div class="variable slider topicFilter"> -->
				<div class="group_topic_filter">

					<div class="item current" data-filter="*">
						<a class="board-link" href="#">
							<p class="text-primary cat-link">All</p>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="subcategory-item-current-clone item current" data-filter="*"  style="display: none;">
			<a class="board-link" href="#">
				<p class="text-primary cat-link">All</p>
			</a>
		</div>

		<div class="subcategory-item-clone item" style="display: none;">
			<a class="board-link subCategory" href="javascript:void(0)">
				<p class="text-primary cat-link"></p>
			</a>
		</div>
		<!-- end of subcategory -->
  	</div>      
</section>
<script>


	function getpopularPost(){

		ViewGroup.search_topics_per_category($("#inp_category_id").val() , 'yes');

	}
	function getfavPost(){

		ViewGroup.search_topics_per_category($("#inp_category_id").val() , 'no' ,'yes');

	}

	function getprivatepost(){
		ViewGroup.search_topics_per_category($("#inp_category_id").val() , 'no' ,'no' , 'yes');
	}

	function getSubgroup(){
		ViewGroup.search_topics_per_category($("#inp_category_id").val() , 'no' ,'no' , 'no' , 'yes');
	}
</script>
