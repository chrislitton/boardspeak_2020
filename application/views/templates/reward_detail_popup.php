<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<style>
		* {
			scroll-behavior: smooth;
			font-family: Arial, sans-serif;
		}

		.no-underline{
			text-decoration: none;
			color: black;
		}
		.mobile-display-none{
			display: none;
		}
		.cursor-pointer{
			cursor: pointer;
		}
		.mobile-selector{
			display: none;

		}
		.download-img{
			width: 22px;
		}
		.mobile-after-heading-content{
			display: none;
		}
		#mobilehr{
			display: none;
		}
		.mobile-headings{
			display: none;
		}
		/*Scroll Bar*/
		.popup::-webkit-scrollbar {
			width: 5px;

		}

		/* Track */
		.popup::-webkit-scrollbar-track {
			background:white;
			border-radius: 50px;
			margin-top: 20px;
		}

		/* Handle */
		.popup::-webkit-scrollbar-thumb {
			/* background: #00d6d6; */
			background: #388cb3;
			border-radius: 10px;

		}

		.button {
			font-size: 1em;
			padding: 10px;
			color: black;
			border: 2px solid #388cb3;
			/* border: 2px solid #06D85F; */
			border-radius: 20px/50px;
			text-decoration: none;
			cursor: pointer;
			transition: all 0.3s ease-out;
		}
		.button:hover {
			background: #06D85F;
		}

		.overlay {
			position: fixed;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			background: rgba(0, 0, 0, 0.7);
			transition: opacity 500ms;
			/* visibility: hidden;
			opacity: 0; */
		}
		.overlay:target {
			/* visibility: visible;
			opacity: 1; */
		}

		.popup {
			margin: 3% auto;
			padding: 20px;
			background: #fff;
			border-radius: 5px;
			overflow: auto;
			position: relative;
			width: 65%;
			height: 100%;

			border: 3px solid #00d6d6;
			border-radius: 24px;
			transition: all 5s ease-in-out;
		}
		#popup1{
			opacity: 0;
			visibility: hidden;
		}

		.first-popup-h2{
			display: flex;
			justify-content: space-between;
			margin-top: 0;
			color: #2bd3f8;
			font-family: Tahoma, Arial, sans-serif;
		}
		.popup .close {
			position: absolute;
			top: 0px;
			right: 12px;
			transition: all 200ms;
			font-size: 30px;
			font-weight: bold;
			text-decoration: none;
			color: #333;
		}
		.popup .close:hover {
			color: #00d6d6;
		}
		.popup .content {
			/* display: none; */
			height: 120%;

		}
		.content-close-btn{
			transition: all 200ms;
			margin-left: 22px;
			cursor: pointer;
		}
		.content-close-btn:hover{
			color:#388cb3;
			/* color: #00d6d6; */
		}
		.full-search-sec{
			width: 100%;

			display: flex;
			justify-content: space-between;
			align-items: center;
			flex-wrap: wrap;

		}
		.left-search-sec{
			display: flex;
			justify-content: space-between;
			align-items: center;
			width: 40%;
		}
		.left-search-content{
			display: flex;
			background-color:transparent;
			/* background-color: rgb(239 232 232); */
			width: 100%;
			justify-content:space-between;
			flex-direction: row-reverse;
			border: 1px solid #388cb3;
			border-radius: 0.25rem;
			max-height: 36px;

		}
		.left-search-content i{


			padding: 11px;
			padding-right: 10px;
			padding-left: 10px;

			border-radius: 0.25rem;
			color: white;
			background-color: #388cb3;
			cursor: pointer;

		}
		.left-search-sec input{
			display: flex;
			width: 100%;
			border-radius: 0.25rem;
			outline: none;
			border:none;
			background: transparent;
			margin-left: 3%;
		}


		.right-search-sec{
			display: flex;
			justify-content: space-between;
			align-items: center;
			margin-right: 62px;

		}
		.right-search-content{
			display: flex;
			justify-content: right;
			align-items: center;
			margin-right: 19%;

		}
		.export-btn{
			display: flex;
			justify-content: center;
			align-items: center;
			padding-right: 20px;
			background-color: transparent;
			color: #388cb3;
			border: 1px solid #388cb3;
			transition: all 0.5s ease;
			border-radius: 6px;
			padding: 2px;
			padding-right: 12px;
			padding-left: 8px;
			margin-left: 38px;
		}
		.export-btn:hover{
			/* box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.4); */
			background: #388cb3;
			color: white;
		}
		.reload-btn{
			display: flex;
			align-items: center;
			background: transparent;
			justify-content: center;
			border: 1px solid #388cb3;
			border-radius: 6px;
			margin-right: 13px;
			color: #388cb3;
			font-weight: 600;
			cursor: pointer;
		}
		.reload-btn:hover{
			background-color: #388cb3;
			color: white;
		}
		#reload-img{
			width: 30px;
			margin-right: 3%;
			font-size: 24px;

			cursor: pointer;
			/* margin-right: 11px; */
			/* display: none; */
		}

		#white-download-img{
			display: none;
		}
		.export-btn:hover #white-download-img{
			display: block;
		}
		.export-btn:hover .download-img{
			display: none;
		}
		.all-headings{
			margin-top: 3%;
			display: flex;
			justify-content: space-evenly;
			width: 100%;
			/* color:  #00d6d6; */
			color:  #388cb3;
			font-weight: 600;
			font-size: 12px;
		}
		.col1,.col2,.col3,.col4,.col5,.col6,.col7,.col8{
			width: 12.5%;
			display: flex;
			align-items: center;
			justify-content: center;

		}
		.after-heading-content{

			font-size: 13px;

			display: flex;
			flex-direction: column;

		}
		.row1,.row2,.row3,.row4,.row5,.row6{
			height: 14%;
			display: flex;
			width: 100%;
			justify-content: space-evenly;
			margin-top: 4%;
		}
		.after-heading-content img{
			width:40% ;
			border-radius: 50%;
		}

		#after-headings-hr{
			border: 1px solid  #00d6d6;
		}
		.pink-color{
			color: #ec92a2;

		}
		.coins-img{
			width: 20%;
		}
		.page-picker{

			width: 42%;
			margin: auto;
			display: flex;
			justify-content: space-evenly;
		}
		.page-picker img{
			width: 4%;
			cursor:pointer;
		}
		.page-picker-icons{
			width: 12%;
			display: flex;
		}
		.page-picker-icons img{
			width: 23%;
		}

		#left-picker-span{
			display: flex;
		}
		/*DropDown*/
		.dropbtn{


			width: 224%;
			border-radius: 0.25rem;

			background: transparent;
			border: 1px solid #388cb3;
			color: #388cb3;

			padding: 5px;
			padding-right: 7px;
			padding-left: 8px;
		}
		.dropbtn:hover{
			background-color: #388cb3;
			color: white;

		}

		.dropdown {

			display: inline-block;
			width: min-content;
			margin-right: 10%;
		}
		.dropdown img{
			width: 10%;
		}

		.dropdown-content {
			display: none;
			position: absolute;
			background-color: #ffff;

			/* box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); */
			z-index: 1;
		}

		.dropdown-content a {
			color: black;
			padding: 12px 16px;
			text-decoration: none;
			display: block;

			border-radius: 0.25rem
		}
		.dropdown-content a:hover{

			background-color: #ddd;
		}

		.dropdown:hover .dropdown-content{display: block;}



		.reward-header{
			display: flex;
			justify-content: space-between;
		}
		.history-button{
			position: relative;
			right: 10%;
			padding: 10px;
			padding-left: 30px;
			padding-right: 30px;
			border-radius: 10px;
			border: none;
			background-color: #2bd3f8;
			color: white;
			transition: 0.2s all;
			font-size: 1.2rem;
			text-decoration: none;
			align-items: center;

			display: flex;
		}
		.history-button:hover{
			box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.4);
			transform: scale(1.04);
			cursor: pointer;
		}

		@media screen and (max-width: 1000px){
			.underline-reward{
				color: #388cb3;
				text-decoration: underline;
				text-decoration-color:#388cb3;
				text-underline-offset: 2px;
				text-decoration-thickness: 2px;
			}
			.row1,.row2,.row3,.row4,.row5,.row6{
				width: 100%;
			}
			.mobile-rows{
				width: 200%;
			}
			#mobilehr{
				display: block;
			}

			.box{
				width: 70%;
			}
			.popup{
				width: 70%;
				height: 50%;
				overflow-x: hidden;
			}
			.after-heading-content{
				display: none;
			}
			.all-headings{
				display: none;
			}
			.mobile-selector{
				display: flex;
				justify-content: space-evenly;
				font-size: 17px;
				margin-top: 4%;
			}
			#after-headings-hr{
				display: none;
			}
			.mobile-headings{
				margin-top: 3%;
				display: flex;
				justify-content: space-evenly;
				width: 100%;
				color:  #388cb3;
				/* color:  #00d6d6; */
				font-weight: 600;
				font-size: 12px;

			}
			.all-elm .mobile-headings{
				margin-top: 3%;
				display: flex;
				justify-content: space-evenly;
				width: 200%;
				color:  #388cb3;
				/* color:  #00d6d6; */
				font-weight: 600;
				font-size: 12px;
				transition: 1s all;
				transform: translateX(1px);
			}
			.mobile-after-heading-content{
				height: 100%;
				font-size: 13px;
				transition: 1s all;
				display: flex;
				flex-direction: column;
				transform: translateX(1px);

			}

			.users-img{
				width:67px ;
				border-radius: 50%;
			}

			#popup1#all .m-all-selected{
				text-decoration: underline;
			}

			.unclaimed-flex{
				display: none;
			}
			.mobile-navigation-icons{
				position: absolute;
				width: 97.1%;
				display: flex;
				justify-content:right;

				bottom: 52%;
				right: 8%;
			}
			.mobile-navigation-icons-sec{
				position: absolute;
				width: 100%;
				left: 100%;
				display: flex;
				justify-content: right;
				bottom: 50%;
			}
			.mobile-navigation-icons img{
				transform: scale(0.8);
				/* position: fixed; */
				position: sticky;
			}
			.mobile-navigation-icons:before{
				content: '';
				display: inline-block;
				width: 30px;
				height: 30px;
				border-radius: 15px;
				background-color: white;
				position: absolute;
				top: -6px;
				right: -9px;
				box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

			}
			.mobile-navigation-icons-sec:before{
				content: '';
				display: inline-block;
				width: 30px;
				height: 30px;
				border-radius: 15px;
				background-color: white;
				position: absolute;
				top: -6px;
				right: -9px;
				box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;

			}
			.mobile-navigation-icons-sec img{
				transform: scale(0.8);
				/* position: fixed; */

			}


		}
		@media screen and (max-width:825px) {
			.full-search-sec{
				flex-direction: column;
				justify-content: center;
			}
			.left-search-sec {
				width: 90%;
			}
			.left-search-content{
				max-height: 30px;
			}
			.left-search-content i{
				padding-top: 7px;
				padding-right: 7px;
				padding-left: 7px;
			}
			.right-search-sec {

				margin-top: 13px;
			}
			.right-search-content {
				justify-content: center;
				margin-left: 10%;

			}

		}
		@media screen and (max-width:631px) {
			.mobile-selector a{
				font-size: 14px;
			}
			.page-picker{
				width: 100%;
			}

			.users-img{
				width:47px ;

			}

			.popup{
				width: 90%;
			}


			.all-elm .mobile-headings{
				width: 212%;
			}
			.mobile-navigation-icons img{
				transform: scale(0.7);
			}
			.mobile-navigation-icons-sec img{
				transform: scale(0.7);
			}
		}

		@media only screen and (max-width:425px) {

			.users-img{
				width:37px ;

			}
			.mobile-navigation-icons img{
				transform: scale(0.6);
			}
			.mobile-navigation-icons-sec img{
				transform: scale(0.6);
			}
			.mobile-col-edit{
				position: relative;
				left: 4%;
			}


		}
		@media only screen and (max-width:472px){
			.first-popup-h2{
				font-size: 1.4rem;
				font-weight: 500;
				width: 50%;
				color: #36c7fc;}
			.header{
				align-items: center;
			}
			.popup .close{
				top: -6px;
				right: 4px;
			}
			.reward-header{
				display: flex;
				justify-content: space-between;


				position: relative;
				bottom: -17px;
			}
			.history-button{
				height: 26px;
			}
			.full-search-sec{
				flex-wrap: wrap;
				justify-content: center;
			}
			.left-search-sec{
				width: 95%;
			}
			.right-search-sec{
				margin-top: 14px;
			}
		}</style>



	<style>
		.graphs{
			width: 100%;
			height: 100%;
			display: flex;
			/* justify-content: space-between; */
			align-items: center;

		}

		.pie-chart{
			/* transform: scale(2); */
			width: 60%;
			min-height: 400px;

		}
		.column-chart{
			width: 50%;
			z-index: 333;
			height: 50%;
		}


		/* CARDS */

		.cards {
			display: flex;
			flex-wrap: nowrap;
			justify-content: space-evenly;
			/* max-height: 100px; */

		}

		.card {
			margin: 10px;
			color: white;
			padding: 10px;
			width: 50%;

			/* display: grid;
			grid-template-rows: 20px 50px 1fr 50px; */
			border-radius: 10px;
			box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.25);
			transition: all 0.2s;

			max-height: 135px;
		}

		.card:hover {
			box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.4);
			transform: scale(1.01);
		}
		.cards-head{
			display: flex;
			justify-content: space-between;

		}
		.card-bars{
			width: 20%;

			display: flex;
			align-items: center;
			justify-content: flex-end;
		}
		.card-bar{
			background-color: white;
			z-index: 5;
			height: 50%;
			width: 50%;
			margin: 3%;
		}
		.bar-1{
			height: 20%;
		}
		.bar-2{
			height: 40%;
		}
		.bar-3{
			height: 30%;
		}



		.card__title {
			font-size: 1.3rem;
			color: #ffffff;
		}

		.card__apply {
			display: flex;
			align-items: center;

		}
		.card__apply p{
			margin-left: 1%;
		}

		/* CARD BACKGROUNDS */

		.card-1 {
			background: radial-gradient(#1fe4f5, #3fbafe);
		}

		.card-2 {
			background: radial-gradient(#fbc1cc, #fa99b2);
		}

		.card-3 {
			background: radial-gradient(#76b2fe, #b69efe);
		}
		@media screen and (max-width:1265px){
			.graphs{
				display: block;
				margin-top: 5%;
			}
			.pie-chart{
				/* transform: scale(2); */
				width: 70%;
				min-height: 400px;
				margin: auto;
			}
			.column-chart{
				width: 50%;
				z-index: 333;
				height: 50%;
				margin: auto;
			}
			.cards{
				/* flex-wrap: wrap; */
			}
		}
		@media screen and (max-width:924px){
			.graphs{
				justify-content: center;
			}

			.pie-chart{

				width: 100%;
				min-height: 400px;
				margin: auto;
			}
			.column-chart{
				/* transform: scale(0.8); */
				width: 100%;
				z-index: 333;
				height: 50%;
				margin: auto;
			}
			.card-bars{display: none;}
		}
		@media screen and (max-width:800px) {
			.card__apply{
				position: relative;
				top: -29px;
			}
			.card__apply img{
				margin-left: 3%;
			}
			.card__apply p {
				margin-left: 4px;
			}
			.card hr{
				position: relative;
				top: -16px;

			}
		}
		@media screen and (max-width:535px) {

			.pie-chart{
				width: 130%;


			}
			.column-chart{

				/* width: 300px;
				height: 300px; */
			}
		}
		@media screen and (max-width:419px){
			.pie-chart{
				width: 140%;


			}
			.card{
				margin: 5px;
				color: white;
				max-height: 130px;
				/* padding: 2px; */

			}
			/* .card hr{
			  position: relative;
		  top: -16px;

			} */
			/* .card__apply{
			  position: relative;
			  top: -29px;
			}
			.card__apply img{
			  margin-left: 3%;
			}
			.card__apply p {
			  margin-left: 4px;
			} */
			.graphs{
				margin-top: 17%;
			}
			.card {padding-top: 0;}
		}
		@media screen and (max-width:385px){
			.pie-chart{
				width: 555px;
				transform: scale(0.8);
				position: relative;
				right: 100px
			}

		}
		@media screen and (max-width:370px) {
			.card{margin: 0;}
		}</style>
	<link rel="stylesheet" href="graph.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rewards</title>
</head>
<body>


<!--<a class="button" href="#popup1">Let me Pop up</a>-->


<div id="popup1" class="overlay">
	<div class="popup">
		<a class="close" href="#">&times;</a>
		<div class="reward-header">
			<h2 class="first-popup-h2">Voucher Reward Title</h2>
			<a href="#popup1#content" class="history-button">History</a>
		</div>
		<hr>
		<div class="first-reward-">
			<div class="main-container">

				<div class="cards">
					<div class="card card-1">
						<!-- <div class="card__icon"><i class="fas fa-bolt"></i></div>
						<p class="card__exit"><i class="fas fa-times"></i></p> -->
						<div class="cards-head">
							<h2 class="card__title">31 <br> Used Vouchers</h2>
							<div class="card-bars">
								<div class="card-bar bar-1"></div>
								<div class="card-bar bar-2"></div>
								<div class="card-bar bar-3"></div>
								<div class="card-bar bar-4"></div>
							</div>

						</div>
						<hr style="margin:0;">
						<div class="card__apply">
							<i class="fa fa-clock-o"></i>
							<p>update : 2:15 am</p>
						</div>
					</div>
					<div class="card card-2">

						<div class="cards-head">
							<h2 class="card__title">55 <br> Claimed Vouchers</h2>
							<div class="card-bars">
								<div class="card-bar bar-1"></div>
								<div class="card-bar bar-2"></div>
								<div class="card-bar bar-3"></div>
								<div class="card-bar bar-4"></div>
							</div>

						</div>
						<hr style="margin:0;">
						<div class="card__apply">
							<i class="fa fa-clock-o"></i>
							<p>update : 2:15 am</p>
						</div>
					</div>
					<div class="card card-3">

						<div class="cards-head">
							<h2 class="card__title">45 <br> Unclaimed Vouchers</h2>
							<div class="card-bars">
								<div class="card-bar bar-1"></div>
								<div class="card-bar bar-2"></div>
								<div class="card-bar bar-3"></div>
								<div class="card-bar bar-4"></div>
							</div>

						</div>
						<style></style>
						<hr style="margin:0;">
						<div class="card__apply">
							<i class="fa fa-clock-o"></i>
							<p>update : 2:15 am</p>
						</div>
					</div>


				</div>
			</div>
			<div class="graphs">
				<div id="columnchart_material" class="column-chart"></div>
				<div id="piechart" class="pie-chart" ></div>

			</div>
			<hr>
		</div>



		<div id="popup1#content" class="content mobile-display-none">
			<i style="    display: flex;
					width: 97%;
					justify-content: right;" class="fa fa-times content-close-btn"></i>
			<div class="full-search-sec">
				<div class ='left-search-sec'>
					<div class="left-search-content">
						<!-- <img class="cursor-pointer"src="/images-svg/svg/search.svg" alt="Search"> -->
						<i class="fa fa-search"></i>
						<input type="text" placeholder="Search...">
					</div>

				</div>

				<div class ='right-search-sec'>
					<div class="right-search-content">
						<button class="reload-btn">	<i id="reload-img"style=" " class="fa fa-repeat"></i>Reload</button>

						<img class="cursor-pointer" style="display: none; " src="assets/images-svg/svg/refresh.svg" alt="Refresh">

						<div class="dropdown">
							<button class="dropbtn">Sort By
								<i class="fa fa-chevron-down"></i>
								<div class="dropdown-content">
									<a href="#">Link 1</a>
									<a href="#">Link 2</a>
									<a href="#">Link 3</a>
								</div>
							</button>
						</div>
						<button class="export-btn cursor-pointer">
							<img src="assets/images-svg/svg/download.svg"class="download-img" alt="">
							<img class='download-img'id="white-download-img"src="assets/images-svg/svg/download-white.svg" alt="Download">
							Export</button>

					</div>



				</div>



			</div>



			<div class="mobile-selector">
				<a href="#popup1#all" class="all-selected mobile-active underline-reward">All</a>
				<a href="#popup1#unclaimed" class="unclaimed-selected">Unclaimed</a>
				<a href="#popup1#claimed" class="claimed-selected">Claimed</a>
				<a href="#popup1#used" class="used-selected">Used</a>
			</div>
			<hr id="mobilehr">
			<div class="all-headings">
				<div class="col1">
					<div class="headin">Recipient</div>

				</div>
				<div class="col2">
					<div class="headin">Voucher Code</div>
				</div>
				<div class="col3">
					<div class="headin">Vouchers Sent</div>
				</div>
				<div class="col4">
					<div class="headin">Coins Used</div>
				</div>
				<div class="col5">
					<div class="headin">Vouchers Claimed</div>
				</div>
				<div class="col6">
					<div class="headin">Voucher Used</div>
				</div>
				<div class="col7">
					<div class="headin">Store Branched</div>
				</div>
				<div class="col8">
					<div class="headin">Date Expired</div>
				</div>
			</div>
			<hr id="after-headings-hr">
			<div class="after-heading-content mobile-display-none">

				<div class="row1">
					<div class="col1">
						<img src="assets/images-svg/man-svgrepo-com.svg" alt="">
					</div>
					<div class="col2">
						10202222
					</div>
					<div class="col3">
						06/21/22
					</div>
					<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
						<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					<div class="col5">06/21/22</div>
					<div class="col6">03/2/21</div>
					<div class="col7">SM BRANCH</div>
					<div class="col8 pink-color">07/22/22</div>
				</div>
				<div class="row2">
					<div class="col1">
						<img src="assets/images-svg/man-svgrepo-com.svg" alt="">
					</div>
					<div class="col2">
						10202222
					</div>
					<div class="col3">
						06/21/22
					</div>
					<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
						<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					<div class="col5">06/21/22</div>
					<div class="col6">03/2/21</div>
					<div class="col7">SM BRANCH</div>
					<div class="col8 pink-color">07/22/22</div>
				</div>
				<div class="row3">
					<div class="col1">
						<img src="assets/images-svg/man-svgrepo-com.svg" alt="">
					</div>
					<div class="col2">
						10202222
					</div>
					<div class="col3">
						06/21/22
					</div>
					<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
						<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					<div class="col5">06/21/22</div>
					<div class="col6">03/2/21</div>
					<div class="col7">SM BRANCH</div>
					<div class="col8 pink-color">07/22/22</div>
				</div>
				<div class="row4">
					<div class="col1">
						<img src="assets/images-svg/man-svgrepo-com.svg" alt="">
					</div>
					<div class="col2">
						10202222
					</div>
					<div class="col3">
						06/21/22
					</div>
					<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
						<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					<div class="col5">06/21/22</div>
					<div class="col6">03/2/21</div>
					<div class="col7">SM BRANCH</div>
					<div class="col8 pink-color">07/22/22</div>
				</div>
				<div class="row5">
					<div class="col1">
						<img src="assets/images-svg/man-svgrepo-com.svg" alt="">
					</div>
					<div class="col2">
						10202222
					</div>
					<div class="col3">
						06/21/22
					</div>
					<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
						<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					<div class="col5">06/21/22</div>
					<div class="col6">03/2/21</div>
					<div class="col7">SM BRANCH</div>
					<div class="col8 pink-color">07/22/22</div>
				</div>
				<div class="row6">
					<div class="col1">
						<img src="assets/images-svg/man-svgrepo-com.svg" alt="">
					</div>
					<div class="col2">
						10202222
					</div>
					<div class="col3">
						06/21/22
					</div>
					<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
						<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					<div class="col5">06/21/22</div>
					<div class="col6">03/2/21</div>
					<div class="col7">SM BRANCH</div>
					<div class="col8 pink-color">07/22/22</div>
				</div>

				<div class="page-picker">

					<p id="left-picker-para">Rows per page:</p>

					<p>10 </p><img src="assets/images-svg/down-arrow-svgrepo-com.svg" alt="">




					<p>1 - 1 of 1</p>
					<div class="page-picker-icons">
						<img src="assets/images-svg/left-arrow-svgrepo-com.svg" alt="">
						<img style=" margin-left: 14%;" src="assets/images-svg/right-arrow-svgrepo-com.svg" alt="">
					</div>
				</div>

			</div>
			<!-- For Mobile -->
			<div id="popup1#all" class="all-elm" >
				<div class="mobile-headings">
					<div class="col1">
						<div class="headin">Recipient</div>

					</div>
					<div class="col2">
						<div class="headin">Vouchers Code</div>
					</div>
					<div class="col3">
						<div class="headin">Vouchers Sent</div>
					</div>
					<div class="col4">
						<div class="headin">Coins Used</div>
					</div>
					<div class="col5 mobile-col-edit">
						<div class="headin">Vouchers Claimed</div>
					</div>
					<div class="col6 mobile-col-edit">
						<div class="headin">Voucher Used</div>
					</div>
					<div class="col7 mobile-col-edit">
						<div class="headin">Store Branched</div>
					</div>
					<div class="col8 mobile-col-edit">
						<div class="headin">Date Expired</div>
					</div>
				</div>
				<div  class="mobile-after-heading-content">
					<div class="mobile-navigation-icons ">
						<img src="assets/images-svg/svg/arrow-right-bold.svg" class="right-arrow-mobile">
					</div>
					<div class="mobile-navigation-icons-sec">
						<img src="assets/images-svg/svg/arrow-left-bold.svg" class="left-arrow-mobile">
					</div>
					<div class="row1 mobile-rows">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
						<div class="col5 mobile-col-edit">06/21/22</div>
						<div class="col6 mobile-col-edit">03/2/21</div>
						<div class="col7 mobile-col-edit">SM BRANCH</div>
						<div class="col8 mobile-col-edit pink-color">07/22/22</div>

					</div>
					<div class="row2 mobile-rows">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
						<div class="col5 mobile-col-edit">06/21/22</div>
						<div class="col6 mobile-col-edit">03/2/21</div>
						<div class="col7 mobile-col-edit">SM BRANCH</div>
						<div class="col8 mobile-col-edit pink-color">07/22/22</div>

					</div>
					<div class="row3 mobile-rows">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
						<div class="col5 mobile-col-edit">06/21/22</div>
						<div class="col6 mobile-col-edit">03/2/21</div>
						<div class="col7 mobile-col-edit">SM BRANCH</div>
						<div class="col8 mobile-col-edit pink-color">07/22/22</div>

					</div>
					<div class="row4 mobile-rows">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
						<div class="col5 mobile-col-edit">06/21/22</div>
						<div class="col6 mobile-col-edit">03/2/21</div>
						<div class="col7 mobile-col-edit">SM BRANCH</div>
						<div class="col8 mobile-col-edit pink-color">07/22/22</div>

					</div>
					<div class="page-picker">

						<p id="left-picker-para">Rows per page:</p>

						<p>10 </p><img src="assets/images-svg/down-arrow-svgrepo-com.svg" alt="">




						<p>1 - 1 of 1</p>
						<div class="page-picker-icons">
							<img src="assets/images-svg/left-arrow-svgrepo-com.svg" alt="">
							<img style=" margin-left: 14%;" src="assets/images-svg/right-arrow-svgrepo-com.svg" alt="">
						</div>
					</div>
				</div>

			</div>

			<div id="popup1#unclaimed" class="unclaimed-elm mobile-display-none">
				<div class="mobile-headings">
					<div class="col1">
						<div class="headin">Recipient</div>

					</div>
					<div class="col2">
						<div class="headin">Voucher Code</div>
					</div>
					<div class="col3">
						<div class="headin">Vouchers Unclaimed</div>
					</div>
					<div class="col4">
						<div class="headin">Coins Used</div>
					</div>
				</div>
				<div  class="mobile-after-heading-content">

					<div class="row1">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row2">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row3">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row4">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="page-picker">

						<p id="left-picker-para">Rows per page:</p>

						<p>10 </p><img src="assets/images-svg/down-arrow-svgrepo-com.svg" alt="">




						<p>1 - 1 of 1</p>
						<div class="page-picker-icons">
							<img src="assets/images-svg/left-arrow-svgrepo-com.svg" alt="">
							<img style=" margin-left: 14%;" src="assets/images-svg/right-arrow-svgrepo-com.svg" alt="">
						</div>
					</div>
				</div>

			</div>


			<div id="popup1#claimed" class="claimed-elm mobile-display-none" >
				<div class="mobile-headings">
					<div class="col1">
						<div class="headin">Recipient</div>

					</div>
					<div class="col2">
						<div class="headin">Voucher Code</div>
					</div>
					<div class="col3">
						<div class="headin">Vouchers Claimed</div>
					</div>
					<div class="col4">
						<div class="headin">Coins Used</div>
					</div>
				</div>
				<div  class="mobile-after-heading-content">

					<div class="row1">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row2">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row3">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row4">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="page-picker">

						<p id="left-picker-para">Rows per page:</p>

						<p>10 </p><img src="assets/images-svg/down-arrow-svgrepo-com.svg" alt="">




						<p>1 - 1 of 1</p>
						<div class="page-picker-icons">
							<img src="assets/images-svg/left-arrow-svgrepo-com.svg" alt="">
							<img style=" margin-left: 14%;" src="assets/images-svg/right-arrow-svgrepo-com.svg" alt="">
						</div>
					</div>
				</div>

			</div>

			<div id="popup1#used" class="used-elm mobile-display-none">
				<div class="mobile-headings">
					<div class="col1">
						<div class="headin">Recipient</div>

					</div>
					<div class="col2">
						<div class="headin">Voucher Code</div>
					</div>
					<div class="col3">
						<div class="headin">Vouchers Used</div>
					</div>
					<div class="col4">
						<div class="headin">Coins Used</div>
					</div>
				</div>
				<div  class="mobile-after-heading-content">

					<div class="row1">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row2">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row3">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div>
					<div class="row4">
						<div class="col1">
							<img class="users-img" src="assets/images-svg/man-svgrepo-com.svg" alt="">
						</div>
						<div class="col2">
							10202222
						</div>
						<div class="col3">
							06/21/22
						</div>
						<div class="col4"> <img style="width:25%;" src="assets/images-svg/BCoin.webp" alt="Coins">
							<img style="width:10%" src="assets/images-svg/paypal-p-svgrepo-com.svg"> 500</div>
					</div><div class="page-picker">

						<p id="left-picker-para">Rows per page:</p>

						<p>10 </p><img src="assets/images-svg/down-arrow-svgrepo-com.svg" alt="">




						<p>1 - 1 of 1</p>
						<div class="page-picker-icons">
							<img src="assets/images-svg/left-arrow-svgrepo-com.svg" alt="">
							<img style=" margin-left: 14%;" src="assets/images-svg/right-arrow-svgrepo-com.svg" alt="">
						</div>
					</div>

				</div>

			</div>
		</div>



	</div>
</div>
</div>


<script>const reload = document.querySelector(".reload-btn");
	reload.addEventListener("click", () => {
		location.reload();
	});
	const popup = document.querySelector('.popup');
	const close = document.querySelector(".close");
	close.addEventListener("click", () => {
		const overlay = document.querySelector(".overlay");

		overlay.style.opacity = 0;
		overlay.style.visibility = "hidden";

	});

	const btn = document.querySelector(".button");
	btn.addEventListener("click", () => {
		const overlay = document.querySelector(".overlay");

		overlay.style.opacity = 1;
		overlay.style.visibility = "visible";
	});
	function underline(section){
		if(section.classList.contains('no-underline') == false){
			section.classList.toggle('no-underline')
		}
	}
	const all = document.querySelector(".all-selected");
	const claimed = document.querySelector(".claimed-selected");
	const used = document.querySelector(".used-selected");
	const unclaimed = document.querySelector(".unclaimed-selected");
	const allElm = document.querySelector('.all-elm')
	const unclaimedElm = document.querySelector('.unclaimed-elm')
	const claimedElm = document.querySelector('.claimed-elm')
	const usedElm = document.querySelector('.used-elm')
	underline(all);
	underline(unclaimed);
	underline(claimed);
	underline(used);

	all.addEventListener("click", () => {

		all.classList.toggle("mobile-active");
		all.classList.toggle("underline-reward");
		if (unclaimed.classList.contains('mobile-active')&& unclaimed.classList.contains('underline-reward')) {
			unclaimed.classList.toggle('mobile-active')
			unclaimed.classList.toggle('underline-reward')
		}
		if (claimed.classList.contains('mobile-active')&& claimed.classList.contains('underline-reward')) {
			claimed.classList.toggle('mobile-active')
			claimed.classList.toggle('underline-reward')
		}
		if (used.classList.contains('mobile-active')&& used.classList.contains('underline-reward')) {
			used.classList.toggle('mobile-active')
			used.classList.toggle('underline-reward')
		}
		if(unclaimedElm.classList.contains('mobile-display-none') == false){
			unclaimedElm.classList.toggle('mobile-display-none')
		}
		if(allElm.classList.contains('mobile-display-none')){
			allElm.classList.toggle('mobile-display-none')
		}
		if(claimedElm.classList.contains('mobile-display-none') == false){
			claimedElm.classList.toggle('mobile-display-none')
		}
		if(usedElm.classList.contains('mobile-display-none') == false){
			usedElm.classList.toggle('mobile-display-none')
		}
	});
	unclaimed.addEventListener("click", () => {

		unclaimed.classList.toggle("mobile-active");
		unclaimed.classList.toggle("underline-reward");
		if (all.classList.contains('mobile-active')&& all.classList.contains('underline-reward')) {
			all.classList.toggle('mobile-active')
			all.classList.toggle('underline-reward')
		}
		if (claimed.classList.contains('mobile-active')&& claimed.classList.contains('underline-reward')) {
			claimed.classList.toggle('mobile-active')
			claimed.classList.toggle('underline-reward')
		}
		if (used.classList.contains('mobile-active')&& used.classList.contains('underline-reward')) {
			used.classList.toggle('mobile-active')
			used.classList.toggle('underline-reward')
		}
		if(unclaimedElm.classList.contains('mobile-display-none')){
			unclaimedElm.classList.toggle('mobile-display-none')
		}
		if(allElm.classList.contains('mobile-display-none') == false){
			allElm.classList.toggle('mobile-display-none')
		}
		if(claimedElm.classList.contains('mobile-display-none') == false){
			claimedElm.classList.toggle('mobile-display-none')
		}
		if(usedElm.classList.contains('mobile-display-none') == false){
			usedElm.classList.toggle('mobile-display-none')
		}

	});

	claimed.addEventListener("click", () => {
		claimed.classList.toggle("mobile-active");
		claimed.classList.toggle("underline-reward");
		if (all.classList.contains('mobile-active')&& all.classList.contains('underline-reward')) {
			all.classList.toggle('mobile-active')
			all.classList.toggle('underline-reward')
		}
		if (unclaimed.classList.contains('mobile-active')&& unclaimed.classList.contains('underline-reward')) {
			unclaimed.classList.toggle('mobile-active')
			unclaimed.classList.toggle('underline-reward')
		}
		if (used.classList.contains('mobile-active')&& used.classList.contains('underline-reward')) {
			used.classList.toggle('mobile-active')
			used.classList.toggle('underline-reward')
		}
		if(claimedElm.classList.contains('mobile-display-none')){
			claimedElm.classList.toggle('mobile-display-none')
		}
		if(allElm.classList.contains('mobile-display-none') == false){
			allElm.classList.toggle('mobile-display-none')
		}
		if(unclaimedElm.classList.contains('mobile-display-none') == false){
			unclaimedElm.classList.toggle('mobile-display-none')
		}
		if(usedElm.classList.contains('mobile-display-none') == false){
			usedElm.classList.toggle('mobile-display-none')
		}
	});

	used.addEventListener("click", () => {
		used.classList.toggle("mobile-active");
		used.classList.toggle("underline-reward");
		if (all.classList.contains('mobile-active')&& all.classList.contains('underline-reward')) {
			all.classList.toggle('mobile-active')
			all.classList.toggle('underline-reward')
		}
		if (claimed.classList.contains('mobile-active')&& claimed.classList.contains('underline-reward')) {
			claimed.classList.toggle('mobile-active')
			claimed.classList.toggle('underline-reward')
		}
		if (unclaimed.classList.contains('mobile-active')&& unclaimed.classList.contains('underline-reward')) {
			unclaimed.classList.toggle('mobile-active')
			unclaimed.classList.toggle('underline-reward')
		}
		if(usedElm.classList.contains('mobile-display-none')){
			usedElm.classList.toggle('mobile-display-none')
		}
		if(allElm.classList.contains('mobile-display-none') == false){
			allElm.classList.toggle('mobile-display-none')
		}
		if(unclaimedElm.classList.contains('mobile-display-none') == false){
			unclaimedElm.classList.toggle('mobile-display-none')
		}
		if(claimedElm.classList.contains('mobile-display-none') == false){
			claimedElm.classList.toggle('mobile-display-none')
		}
	});

	const mobileContent = document.querySelector('.mobile-after-heading-content');
	const mobileHeadings = document.querySelector('.mobile-headings');
	const rightArrow = document.querySelector('.right-arrow-mobile');
	const leftArrow = document.querySelector('.left-arrow-mobile');
	rightArrow.addEventListener('click',()=>{
		mobileContent.style.transform='translateX(-103%)';
		mobileHeadings.style.transform='translateX(-51.5%)';
	})
	leftArrow.addEventListener('click',()=>{
		mobileContent.style.transform='translateX(1px)';
		mobileHeadings.style.transform='translateX(1px)';
	})
	const history = document.querySelector('.history-button')
	const content = document.querySelector('.content')
	const contentClose = document.querySelector('.content-close-btn')

	history.addEventListener('click',()=>{
		content.classList.toggle('mobile-display-none')
	})
	contentClose.addEventListener("click",()=>{
		content.classList.toggle('mobile-display-none')
	})


</script>
<script type="text/javascript">

	$(function(){
		$('.bars li .bar').each(function(key, bar){
			var percentage = $(this).data('percentage');
			$(this).animate({
				'height' : percentage + '%'
			},2000);
		});
	});



</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {

		var data = google.visualization.arrayToDataTable([
			['Task', 'Hours per Day'],
			['Work',     11],
			['Eat',      2],
			['Commute',  2],
			['Watch TV', 2],
			['Sleep',    7]
		]);

		var options = {
			// title: 'My Daily Activities'
		};

		var chart = new google.visualization.PieChart(document.getElementById('piechart'));

		chart.draw(data, options);
	}
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {'packages':['bar']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {
		var data = google.visualization.arrayToDataTable([
			['Year', 'Sales', 'Expenses', 'Profit'],
			['2014', 1000, 400, 200],
			['2015', 1170, 460, 250],
			['2016', 660, 1120, 300],
			['2017', 1030, 540, 350]
		]);

		var options = {
			chart: {
				//    title: 'Company Performance',
				//    subtitle: 'Sales, Expenses, and Profit: 2014-2017',
			}
		};

		var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

		chart.draw(data, google.charts.Bar.convertOptions(options));
	}
</script>
</body>
</html>
