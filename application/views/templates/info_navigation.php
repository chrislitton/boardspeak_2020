<header id="header" class="header sticky-top">
	<nav class="main_nav_container container" id="mainNav">

		<div class="mobile_icon" disabled>
			<a id="mobile_menu_collase">
				<span></span>
				<span></span>
				<span></span>
			</a>
		</div>

		<div class="mobile_nav" id="mobile_nav">
			<ul class="mobile_nav_container container">
				<li class=""><a class="" href="<?php echo base_url(); ?>pages/view/signin">Log In</a></li>
				<li class=""><a class="" href="<?php echo base_url(); ?>pages/view/signup">Sign Up</a></li>
			</ul>
		</div>

		<a class="logo_img" href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>img/logo.png" class="img-fluid"></a>

		<!-- only desktop nav -->
		<div class="desk_nav">
			<ul class="desk_nav_container">
				<li class="only_desk_item">
					<a class="" href="<?php echo base_url(); ?>pages/view/signin">Log In</a>
				</li>
                <li class="only_desk_item">
					<a class="" href="<?php echo base_url(); ?>pages/view/signup">Sign Up</a>
				</li>
			</ul>
		</div>
	</nav>
</header>
