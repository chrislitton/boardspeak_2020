<?php
	$GroupName = $topics_items['Gr_Name'];
	if (strlen($GroupName)>=25) $GroupName = substr($GroupName,0,25)."... ";
	
	$TopicName = $topics_items['To_Name'];
	if (strlen($TopicName)>=25) $TopicName = substr($TopicName,0,25)."... ";

	$catName = $topics_items['catName'];
	$catId = $topics_items['catId'];
?>
<!-- main content start here -->
<main id="gp_main" class="gp_main container">
	 	<div>
	 		<div class="col-md-12">			
			 	<nav aria-label="breadcrumb">
					<ul class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>account/view/group/<?php echo encode_id($topics_items['Gr_ID']);?>"><?=$topics_items['Gr_Name']?></a></li>
						<li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>account/view/group/<?php echo encode_id($topics_items['Gr_ID']);?>/<?=$catId?>"><?=$catName;?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><?=$TopicName?></li>
					</ul>
				</nav>
			</div>
	 		<div class="col-md-12">
				<section class="banner_desc">
					<!-- group desc section -->
					<div class="group_desc">
						<div class="group_desc_container container">
							<div class="group_text">
								<h4 class="group_name"><a style="color:#0D7EBB"; id="grooupname" href="<?php echo base_url(); ?>account/view/topic/<?php echo $topics_items['To_Slugs'];?>"><?php echo $topics_items['To_Name'];?></a></h4>
								<p class="group_metta">
									<span class="group_badge group_badge_<?=strtolower($topics_items['To_Privacy'])?>"><?php echo ucfirst($topics_items['To_Privacy']);?></span>

<!--									<span class="group_hearts">-->
<!--										<i class="fas fa-heart"></i>-->
<!--										&nbsp;--><?php //echo $topics_items['To_Likes'];?>
<!--									</span>-->

									<span class="group_memebers">
										<?php
										 if(isset($topics_member)){

													echo $topics_member  ;

										 }

										  ?>
									</span>
									members
								</p>

								<pre class="group_taggline"><?=$topics_items['To_Description']?></pre>
							</div>
							<div class="group_btn">

								<?php

 							if(!isset($this->session->userdata['logged_in'])){ ?>
										<div class="group_btn">
											<a href="<?php echo base_url(); ?>pages/view/signup">
												<button class="btn" type="button">
													Login to Join
												</button> <br>
											</a>
										</div>
							<?php 	}  else {
								if (  $member_info['status'] == 'success' || $this->session->userdata['logged_in']['bs_id'] == $topics_items['To_Us_ID']) { ?>
									<?php if ( $member_info['role_status'] == 'pending') { ?>
 									    <a class="    btn btn-xl btn-danger mb-2 mr-2 mt-2" href="javascript:void(0)" >Pending</a>
										<!--<a class="action_button invite_response_btn btn btn-xl btn-light mb-2 mr-2 mt-2" href="javascript:void(0)" data-action="decline">Decline</a>
										<a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>	-->
									<?php } else {

										if($topics_items['To_Privacy']  == 'private'){

										?>
										<a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2 <?php echo ($member_info['role_status'] == 'pending') ? 'btn_pending' : 'invite_member_btn'; ?>" href="javascript:void(0)"><?php echo ($member_info['role_status'] == 'pending') ? 'Pending' : 'Invite  <li class="fa fa-plus" style="margin-left: 4px;"></li>'; ?></a>
									  <?php } if(isset($this->LoggedInUser) &&   $topics_items['To_Us_ID'] == $this->LoggedInUser || $creator_info['role'] == 'creator'){ ?>
									  		  <a href="<?php echo base_url(); ?>account/topicinfo/edit/<?php echo $EncodedID;?>" class="btn btn-xl btn-primary mr-2" style="background: white;  color: #398cb3;">Edit Subgroup Info</a>
 									  <?php } ?>

									<?php } ?>
								<?php } else { ?>

									<?php
									 if( isset($inviteID) ){


										 if( $inviteID == 'member' || $inviteID == 'admin'){ ?>
											  <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2"  data-invitetype='<?php echo $inviteID; ?>' id="btn_join" href="javascript:void(0)">Join</a>
											<?php }else if($inviteID  == 'follower'){ ?>
											 <a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2"  data-invitetype='<?php echo $inviteID; ?>' id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>

											 <?php }  }else{
										 		if($topics_items['To_Privacy'] == 'public'){

												}else{
										 ?>
											  <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2" id="btn_join_without_invite" href="javascript:void(0)">Join</a>
												<a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="btn_follow_without_invite" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>

											 <?php }}
										?>

									<!--<a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2" id="btn_join" href="javascript:void(0)">Join</a>
									<a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>-->

								<?php }} ?>
								
								<div class="secondery_item" style="display:none;	">
									<span class="secondery_btn">
										<i class="fas fa-ellipsis-h"></i>
									</span>
									<ul class="secondery_dropdown">
										<li>
											<a href="">
												Share
											</a>
										</li>
										<li>
											<a href="">
												<i class="far fa-star"></i>
												Add to favourites
											</a>
										</li>
										<li>
											<a href="">
												Pin
											</a>
										</li>
										<li><a href="<?= base_url() . 'account/topicinfo/edit/'.$EncodedID ?>">Edit</a></li>
										<li>
											<a href="">
												Topic Settings
											</a>
										</li>
										<li>
											<a href="">
												Notify Me
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-12 col-lg-12 px-0 py-3 group_container_info">
								<p class="lead text-gray-med p-0 m-0 mt-1">
								<?php
									if (strpos($_SERVER['REQUEST_URI'], '/topicinfo/') === false) {
										echo '<a class="editoption <?php echo $ShowAdminAction;?>" href="'.base_url().'account/topicinfo/detail/'.$EncodedID.'">Subgroup Info</a>';
									}
								?>
								</p>
							</div>
						</div>
					</div>

					<!-- banner section -->
					<div class="gp_banner">
						<div class="banner_container">
							<div class="col-md-12 col-lg-12 board-banner text-right bg-topic topic-cover-image <?php echo $topics_items['To_Backcolor']; ?>" 
								<?php if(strlen($topics_items['To_Photo'])>0) echo 'style="box-shadow: 1px 0px 5px 3px #80808033;background:transparent url('.base_url() . str_replace(base_url(), '', $topics_items['To_Photo']).') no-repeat center center /cover; background-size: 100% 100%;"';?> >
								<div class="col-md-12 col-lg-12" style="height: 100%;">
								 <?php if(isset($creator_info)){  if( isset($this->LoggedInUser) &&   $topics_items['To_Us_ID'] == $this->LoggedInUser || $creator_info['role'] == 'creator' ){ ?>
									<div class="btn btn-primary mt-3 btn-upload-cover-image" style="    background: #7476779e!important
                                                                                                        border: 2px solid white!important;
                                                                                                        border-radius: 0px!important;
                                                                                                        color: white!important;border-radius: 9px!important;
                                                                                                        font-weight: 600!important;">
										<form method="post">
											<label class="title-tip title-tip-left"  title="Change Board Cover" for="upload_image" style="margin-bottom:0;">
												<img src="" id="uploaded_image" class="img-responsive img-circle" />
												<i class="fa fa-camera pr-2"></i>
												<input type="file" name="image" class="image" id="upload_image" style="display:none" accept="image/*" />
											</label>
										</form>
									</div>
									<?php }} ?>
							    </div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
  
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title">Upload Board Image</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">×</span>
        		</button>
      		</div>
      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
        		<div class="img-container">
            		<div class="row">
                		<div class="col-md-8">
                    		<img src="" id="crop_uploaded_image" />
                		</div>
                		<div class="col-md-4">
                    		<div class="preview"></div>
				      		<div class="modal-footer" style='padding-right: 0px;'>
				      			<button type="button" id="crop" class="btn btn-primary">Upload</button>
				        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				      		</div>
                		</div>
            		</div>
        		</div>
      		</div>
    	</div>
  	</div>
</div>
<input type="hidden" id="topicID" value="<?php echo $EncodedID; ?>">
<input type="hidden" name="refererer" id="post_refererer" value="
  <?php if(isset( $_SESSION['post_referrerID'])){
   echo $_SESSION['post_referrerID'];
   } ?>" />
   <input type="hidden" name="post_role" id="post_role" value="<?php if(isset($_SESSION['post_role'])){echo $_SESSION['post_role'];} ?>" />

<?php

	if (isset($this->session->userdata['logged_in']['bs_id'])) {
		$this->load->view('account/group_user_pop_up_topic');
	}
?>
	<?php if (!isset($this->session->userdata['logged_in']['bs_id'])) { ?>
		<script>
			localStorage.setItem("topic_id", <?php echo $ID; ?>);
			localStorage.setItem("topic_role", <?php echo $role; ?>);
 			localStorage.setItem("topic_referrer", <?php echo $referrer; ?>);
		</script>
	<?php } ?>
<style>
	label.makepinclass:hover {
		border: 1px solid grey;
	}
</style>
