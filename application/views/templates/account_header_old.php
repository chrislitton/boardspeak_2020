	<!-- Footer -->
    <footer class="account-footer">
    <div class="container">
      <div class="row">


        <!-- Footer Social Icons -->
        <div class="col-lg-6 mb-2 mb-lg-0 account-social">
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-facebook-f"></i>
						</a>
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-twitter"></i>
						</a>
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-linkedin-in"></i>
						</a>
						<a class="btn btn-outline-light btn-social mx-1" href="#">
							<i class="fab fa-fw fa-dribbble"></i>
						</a>
        </div>


        <!-- Footer About Text -->
        <div class="col-lg-6 account-copyright">
          <p class="lead mb-1">© BoardSpeak 2020</p>
        </div>

      </div>
    </div>
  </footer>




	<div class="bg_load"></div>
	<div class="wrapper">
			<div class="loader"></div>
			<p class="lead mt-1 ml-2">Loading.....</p>
	</div>



  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url(); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="<?php echo base_url(); ?>js/jqBootstrapValidation.js"></script>

  	<!-- Custom scripts for this template -->
  	<script src="<?php echo base_url(); ?>js/freelancer.min.js"></script>
	<script src="<?php echo base_url(); ?>js/custom.js"></script>

  	<!--OwlCarousel-->
	<script src="<?php echo base_url(); ?>vendor/slick/slick.js" type="text/javascript" charset="utf-8"></script>
	  
	<script src="<?php echo base_url(); ?>js/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>

	<?php if (isset($this->session->userdata['logged_in']) && isset($asset)){ ?>
		<script src="<?php echo base_url(); ?>assets/js/<?=$asset?>.js"></script>
	<?php } ?>

  <script>
	$(document).ready( function() {

        $(".bg_load").fadeOut("slow");
        $(".wrapper").fadeOut("slow");

	});

	</script>
</body>

</html>
