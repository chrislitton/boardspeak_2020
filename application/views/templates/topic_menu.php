<section class="board-toolbar-section">
  	<div class="container">

		<!-- start of category -->
		<div class="row mb-1">
		    <div class="col-md-12 col-lg-12 bg-light py-2">
			    <div class="variable categoryFilter slider">

					<?php 
						$i = 0; 
						foreach($category_items as $item) :
							$i++; if ($i==10) $i = 1;

							if($item['ID']) :
					?>

								<div class="item bg-color<?php echo $i;?>">
									<button class="board-link btn-category-chooser" id="<?=$item['ID'];?>" >
										<p class="text-white cat-link"><?php echo $item['Name'];?></p>
									</button>
								</div>
					<?php 
							endif;
						endforeach; 
					?>

				</div>
			</div>
	    </div>

		<div class="row mb-1">
			<!-- start of dropdown -->
		    <div class="col-6 py-2" style="padding-left: 0;">
				<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reset</button>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo base_url(); ?>account/view/topic/<?php echo $EncodedID;?>">Reset</a>
					<a class="dropdown-item" href="#">Action</a>
					<a class="dropdown-item" style=" display: inline-flex;" href="#" onclick="getprivatepost()">
						<img style="width: 24px;" src="http://localhost/boardspeak_2020/assets/img/lock.png" alt="image" style="">
						<span>Private</span></a>

				</div>
			</div>
			<!-- end of dropdown -->

			<div class="col-6 py-2 form-group" style="padding-right: 0;">
				<?php echo form_open('account/view/topic/'.$EncodedID); ?>
				<div class="control-group input-group col pr-0">
					<input class="form-control" id="txtSearch" style="width: 70%;" name="txtSearch" value="<?php echo $txtSearch;?>" type="text" placeholder="Search">
					<div class="input-group-append">
						<button id="btn_search_text" class="btn btn-primary" style="padding:0;border-top-left-radius: 0 !important; border-bottom-left-radius: 0 !important;"><i class="fa fa-search py-1 px-2 text-white"></i></button>
					</div>
				</div>
				</form>
			</div>
	    </div>

	    <!-- start of subcategory -->
		<div class="row topic_post_filter_row"  style="display: none;">
			<div class="col-md-12 col-lg-12 bg-light py-2">
				<!-- <div class="variable slider topicFilter"> -->
				<div class="topic_post_filter">

					<div class="item current" data-filter="*">
						<a class="board-link" href="#">
							<p class="text-primary cat-link">All</p>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="subcategory-item-current-clone item current" data-filter="*"  style="display: none;">
			<a class="board-link" href="#">
				<p class="text-primary cat-link">All</p>
			</a>
		</div>

		<div class="subcategory-item-clone item" style="display: none;">
			<a class="board-link subCategory" href="#">
				<p class="text-primary cat-link"></p>
			</a>
		</div>
		<!-- end of subcategory -->
    </div>      
</section>
