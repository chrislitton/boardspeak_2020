<?php
	if (isset($this->session->userdata['logged_in'])){
		$bs_email = ($this->session->userdata['logged_in']['bs_email']);
		$bs_alias = ($this->session->userdata['logged_in']['bs_alias']);
		$bs_thumb = ($this->session->userdata['logged_in']['bs_thumb']);
		$bs_admin = ($this->session->userdata['logged_in']['bs_admin']);
		$bs_group = $this->session->userdata['logged_in']['bs_group'];
		$bs_group_count = $this->session->userdata['logged_in']['bs_group_count'];

		$subNavContainer = ($bs_group_count == 0) ? 'check_group' : '';
		$listNav = ($bs_group_count > 0) ? 'check_group' : '';
	} 
?>
<?php $this->load->view('account/create_group_pop_up'); ?>
<script src="http://localhost/boardspeak_2020/vendor/jquery/jquery.min.js"></script>
<nav class="navbar navbar-expand-md bg-light navbar-light d-flex justify-content-around" style="padding: 10px 10px;">
	<!-- Brand -->

	<div id="mainnavbar" style = "display:flex;justify-content:space-between;width:100%;">
		<a  class="navbar-brand" href="<?php echo base_url(); ?>">
			<img src="<?php echo base_url(); ?>img/logo.png" class="imglogo" alt="logo" class="logo" >
		</a>
		<!-- Toggler/collapsibe Button -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
	</div>

	<!-- Navbar links -->
	<div class="collapse navbar-collapse" id="collapsibleNavbar">

		<script>

			// $(document).on('click', '.check_group', function(e) {


			$(document).on('click', '.check_group', function(e) {


					$(".create_group_popup").addClass('popped');

			});


			function showsearchbar(){

				if($('#searchbox').is(":visible")){
					$('#searchbox').hide(1000);
				}else{
					$('#searchbox').show(1000);
					$('#searchfilterID').focus();
				}
			}
		</script>
		<ul class="navbar-nav" style="
 margin-top: 5px; display: flex; flex-direction: row;
		 align-items: center;">
			<li>
				<a href="#">
					<img onclick="showsearchbar()" src="<?php echo base_url(); ?>assetsofpop/img/icon-search.svg" style="max-width: fit-content!important;" alt="icon search">
				</a>
				<div class="input-group mb-3" id="searchbox" style="">

					<input type="search"  autocomplete="false" id="searchfilterID"  class="form-control" placeholder="Search here" aria-label="Username" aria-describedby="basic-addon1">
				</div>
				<div class="content-list" id="list1" style=" display:none; position: absolute;

    color: black;
    background: rgb(56, 140, 179);
    box-shadow: grey 1px 1px 1px 1px;
    border-radius: 5px;
    width: 317px;
    z-index: 18;
    top: 74px;

    margin-left: -85px;">
					<ul class="drop-list"  style="    max-height: 380px;      overflow-x: hidden; overflow-y: scroll!important; " align="left" id="saerchlistsohow1">
					</ul>
				</div>
			</li>
			<li><a href="<?php echo base_url()?>account/explore">Explore</a></li>
			<li><a class="<?= $subNavContainer; ?>  <?= $listNav; ?>" href="<?php echo base_url(); ?>account/create/group" >Create Group</a></li>

			<li>
				<a   href="<?php echo base_url()?>account/notifications" >
					<div style="position: relative;">
						<i id="notificatioCounter" style="position: absolute;
    margin-left: 11px;
    margin-top: -3px;
    color: #2c960c;
    font-size: 9px!important;" class="fa fa-circle"></i>
						<img style="max-width: fit-content!important;" src="<?php echo base_url(); ?>assetsofpop/img/icon-bell.svg" alt="icon bell">
						<div class="avaible-notif"></div>
					</div>
				</a>
			</li>
			<li>
				<a href="#" class="  dropdown-toggle" style=" flex-wrap: nowrap;
    display: flex;
    align-items: center;"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

					<?php if(isset($this->session->userdata['logged_in']['Us_Photo']) && !empty($this->session->userdata['logged_in']['Us_Photo']) ){ ?>
					<img  alt="avatar" class="avatar" style="height:40px; max-width: fit-content!important;" src="<?php echo $this->session->userdata['logged_in']['Us_Photo']; ?>">
					<?php }else{ ?>
						<img src="<?php echo base_url(); ?>assetsofpop/img/avatar.svg" style=" height:40px;   max-width: fit-content!important;" alt="avatar" class="avatar">

					<?php } ?>

				</a>
<style>
	/* .navbar-nav .dropdown-menu{
		top: 69px;
    left: 72%;
	}
	@media screen and (max-width:767px){
		.navbar-nav .dropdown-menu{
	
position: absolute;
    top: 142px;
    left: 38%;

		}
		
}
@media screen and (max-width:427px){
ul.navbar-nav li {
padding:3px;
}
#searchbox{
width:80%;
margin-top:18px;
}
} */
</style>

				<div class="dropdown-menu mainnevagtion" aria-labelledby="dropdownMenuButton" style="position:absolute;">
					<style>
						ol{
							width:250px!important;
						}
						ol li {
							padding: 0px!important;

							border-bottom: 1px solid #80808040;
							margin: 0px 18px!important;
						}
						ol li a{
							color: rgba(0, 0, 0, 0.9);
						}
						ol li a:hover {
							color: #15AFE8!important;
						}
					</style>
					<ol style="list-style: none;
    padding: 0px;" >
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/profile">Profile / Dashboard</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/notifications">Notifications</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/pricing">Pricing Plan</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="javascript:void(0)"  data-toggle="modal" data-target="#ContactUsForm">Help & Support</a></li>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>account/settings">Settings</a></li>
						<?php if ($bs_admin=='1') { ?>
							<?php

							$this->db->select('bs_inquiries.*');
							$this->db->from('bs_inquiries');
							$resuult = 	  $this->db->get()->result_array();
							;
							?>
							<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>admin">Administrator <span class="dot " style="background:green!important;"><?php print_r(count($resuult)); ?></span></a></li>
						<?php } ?>
						<li><a class="nav-link font-weight-normal px-3" href="<?php echo base_url(); ?>pages/view/logout">Log Out</a></li>
					</ol>
				</div>
			</li>
			<li>
				<a href="<?php echo base_url() ?>account/Coins">
					<div class="container-reward">
						<p style="margin-bottom : 0px; margin: 0px;
    margin-right: 6px;">Rewards</p>
						<img src="<?php echo base_url(); ?>assetsofpop/img/icon-coin-no-border.svg" alt="">
					</div>
				</a>
			</li>

		</ul>

	</div>
</nav>


<style>
	.container-reward {
		height: 50px;
		border-radius: 88px;
		border: 2px solid #15da57;
		padding: 0px 5px 0px 20px;
		display: flex;
		align-items: center;
	}
	@media (min-width: 768px){
		.navbar-expand-md .navbar-nav {
			flex-direction: row;
			margin-left: 42%;
		}
		#list1 {
		top: 74px;
		}
		#searchbox{
			margin-left: -265px!important;
			position: absolute;
			width: 230px;
			z-index: 1;
			margin-top: -32px!important;

			display: inline-flex;
		}
		}
		#collapsibleNavbar ul{
			float: right!important;
		}
	}
	@media (min-width: 480px){


		.dropdown-menu.mainnevagtion.show{
			top: 149px!important;
			left: 29%!important;
			position: absolute!important;
		}

		#list1 {
			top: 184px;
		}
		#collapsibleNavbar ul{
			float: right!important;
		}
	}
	@media (max-width: 480px)
	{
		#collapsibleNavbar ul{
			float: left!important;
		}
		div#list1 {
			margin-top: 110px;
			margin-left: 2px!important;
		}
		.dropdown-menu.mainnevagtion.show{
			top: 149px!important;
			left: 29%!important;
			position: absolute!important;
		}

		#collapsibleNavbar{
			width: 100%;
			overflow-x: scroll;
		}

	}

	#searchbox{
		position: absolute;
		display: none;
		width: 230px;
		z-index: 1;
		margin-top: 10px;
		margin-left: 0px;
	}
	#collapsibleNavbar::-webkit-scrollbar-track
	{
		/*-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);*/
		background-color: #ffffff;
	}
	button.navbar-toggler:focus {
		/* background: #ba121200; */
		/* color: red; */
		border: 1px solid #8080808f;
		border-radius: 8px;
	}
	#collapsibleNavbar::-webkit-scrollbar
	{
		width: 6px;
		background-color: #F5F5F5;
		height: 2px!important;
	}

	#collapsibleNavbar::-webkit-scrollbar-thumb
	{
		background-color: #faf8f8!important;

	}
	ul.navbar-nav li {
		padding: 10px;
	}
	img.avatar {
		width: 40px;
	
		border-radius: 50%;
	}
	.container-reward {
		height: 50px;
		border-radius: 88px;
		border: 2px solid #15da57;
		padding: 0px 5px 0px 20px;
		display: flex;
		align-items: center;
	}
</style>
