<?php
	$photo = (strlen($groups_items['Gr_Photo'])>0) ? base_url() . $groups_items['Gr_Photo'] : base_url() . 'img/banner/create-group.jpg';
	$GroupName = $groups_items['Gr_Name'];
	if (strlen($GroupName)>=25) $GroupName = substr($GroupName,0,25)."... ";
?>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- main content start here -->
 <input type="hidden" value="<?php echo $groups_items['Gr_packegtype']; ?>" id="packagedetail">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- The Modal -->
<div class="modal" id="myModalforadmin">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h6 class="modal-title">Re-assignment of Roles</h6>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<label id="textshow"></label>
				<input type="hidden" name="newwrole" id="newwrole">

				<input type="hidden" name="previousID" id="previousID">
				 <ul id="adminuserList">


				 </ul>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" onclick="changeRoleofSuperAdmin()" id="updateuser" class="btn btn-info"  >Update</button>
				<button  type="button"  class="btn btn-danger" id="deleteuser"  onclick="removingfromtheGroup()">Remove User</button>
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<main id="gp_main" class="gp_main container">
	<section class="banner_desc">
		<input type="hidden" name="userID" id="userID" value="<?php if(isset($this->LoggedInUser)){echo $this->LoggedInUser;}?>" />

		<input type="hidden" name="userType" id="userType" value="<?php

 		if(isset($member_info['role'])){
 		echo $member_info['role'];
 		}
 ?>" />

		<input type="hidden" value="<?php

		if(isset($this->session->userdata['logged_in'])){
			echo $this->session->userdata['logged_in']['bs_admin'];
 			}
	 ?>" name="admintype" id="admintype"/>

	<input type="hidden" name="totalmemberofgroup" id="totalmemberofgroup" value=<?=$member_counter ?> />

		<input type="hidden" name="totalpostofgroup" id="totalpostofgroup" value=<?php if(isset($post_count)){echo $post_count;} ?> />

		<!-- group desc section -->
		<div class="group_desc">
			<div class="group_desc_container container">
				<div class="group_text">
					<h4 class="group_name" style="width: 90%; white-space:nowrap;"><a id="groupname" style="   color:#0D7EBB"; href="<?php echo base_url(); ?>account/view/group/<?php echo $groups_items['Gr_Slug'];?>"><?php echo $groups_items['Gr_Name'];?></a>

						<?php if(isset( $CurrUserID ) && isset($member_info)){
						if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>
							<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
							<a style="display: inline-block;" href="<?php echo base_url(); ?>account/groupinfo/edit/<?php echo $EncodedID;?>" class="ml-2  "
							   data-title="Edit Group Info"  >
								<img style="  width: 20px;" src="<?php echo base_url() ?>assetsofpop/img/Pen-icon.png">
								<!--											<i class="fa fa-edit" style="font-size:2.5rem"></i>-->
							</a>

						<?php }  } ?></h4>
					<p class="group_metta">
						<span class="group_badge group_badge_<?=strtolower($groups_items['Gr_Privacy'])?>"><?php echo ucfirst($groups_items['Gr_Privacy']);?></span>

						<span class="group_hearts" style="font-size: 12px;">
						</span>
						<span class="group_memebers">

							<?php
								echo $member_counter."&nbsp;member/s";
							?>
						</span>


					</p>

						<input type="hidden" id="Group_privacy" value="<?php echo $groups_items['Gr_Privacy']; ?>"	>

					 <input type="hidden" id="Gr_ID" value="<?php echo $EncodedID ?>"	>
					<input type="hidden" id="Gr_ID_decode" value="<?php echo  decode_id( $EncodedID);?>"	>
					<pre class="group_taggline" style=" width: 90%; margin-top: 10px;	overflow: hidden;
   						 "><?php echo $groups_items['Gr_Description'];?></pre>
					<?php if(isset($groupinfo)){ ?>
						<span class="group_memebers">
							Created On:
							<?php
							echo date( "F d, Y h:i", strtotime($groups_items['Gr_DatePosted']) );
							?>
		                </span>
					<?php	} ?>

				</div>
				    <!--these are accepet and reject button show incorrect to user which is just join the group -->
					<!--<a class="action_button invite_response_btn btn btn-xl btn-danger mb-2 mr-2 mt-2" href="javascript:void(0)" data-action="accept">Accept</a>-->
                    <!-- <a class="action_button invite_response_btn btn btn-xl btn-light mb-2 mr-2 mt-2" href="javascript:void(0)" data-action="decline">Decline</a>-->

				<?php

				 if (isset($this->session->userdata['logged_in']['bs_id'])  && isset($member_info)   ) { ?>
					<div class="group_btn">
						<?php if ($member_info['status'] == 'success' || $this->session->userdata['logged_in']['bs_id'] == $groups_items['Gr_Us_ID'])
						{ ?>
							<?php if ($member_info['is_invited'] && ($member_info['role_status'] == 'pending')) { ?>
 						<!-- <a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>-->
							 <a  class="action_button btn btn-danger btn-xl  mb-2 mr-2 mt-2 <?php echo ($member_info['role_status'] == 'pending') ? 'btn_pending' : 'invite_member_btn'; ?>" style="background: #E87979 !important;     color: black;
    line-height: 19px !important;
    border: none;
    font-family: 'lato';
    border-radius: 2rem !important;
    padding: 10px 20px !important;
    font-size: 16px;
    min-width: 154px;" href="javascript:void(0)"><?php echo ($member_info['role_status'] == 'pending')  ? 'Pending Request' : 'Pending'; ?></a>
							<?php } else { ?>
								<a class="action_button btn btn-danger   mb-2 mr-2 mt-2 <?php echo ($member_info['role_status'] == 'pending') ? 'btn_pending' : 'invite_member_btn'; ?>"style="background: #E87979 !important;     color: black;
    line-height: 19px !important;
    border: none;
    font-family: 'lato';
    border-radius: 2rem !important;
    padding: 10px 20px !important;
    font-size: 16px;
    min-width: 154px;" href="javascript:void(0)"><?php echo ($member_info['role_status'] == 'pending') ? 'Pending' : 'Invite  <li class="fa fa-plus" style="margin-left: 4px;"></li>'; ?></a>
							<?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>
								<?php if(isset($promoteddata) && $promoted == '1'){ ?>
									<div id="dropding" class="dropdown">
										<button href="javascript:void(0)" id="promotebutton" class="btn btn-xl btn-success mr-2 ml-3  dropdown-toggle" type="button" data-toggle="dropdown"

												style="    background: #4CC89270;
    color: black;
    line-height: 19px !important;
    border: none;
    font-family: 'lato';
    border-radius: 2rem !important;
    padding: 10px 20px !important;
    font-size: 16px;"> <?php
											echo 'Promoted  <font style="font-size: 10px;">Expired at '. $promoteddata[0]['Pp_Expiry_Date'] .'</font>';

											?> 	<span class="caret" style="padding: 0; margin: 0;"></span></button>
										<ul  class="dropdown-menu" style="background: white; padding: 10px;">
											<li class="promotedrop"><a href="#"  onclick="makePostextendpromote()">Select New Expiry Date</a></li>
											<li class="promotedrop"><a href="#" onclick="EndPostPromote()">End Post Promotion</a></li>

										</ul>
									</div>
									<style>
										label::before{
											background-color: white !important;
											color:black !important;
										}

										.promotedrop:hover {
											background: #80808038;
										}
										@media screen and (max-width:500px) {
											.group_btn{
												display: flex;
												flex-wrap: nowrap;
												justify-content: flex-start;
											}
										}
									</style>
								<?php }else{ ?>
									<a href="javascript:void(0)" id="promotebutton" class="btn btn-xl btn-success mr-2 ml-3"
									   onclick="makePostPromote()"
									   style="background: #4CC892;    color: black;
    line-height: 19px !important;
    border: none;
    font-family: 'lato';
    border-radius: 2rem !important;
    padding: 10px 20px !important;
    font-size: 16px;
    min-width: 154px;
    margin-right: 43px !important;">
										Promote
									</a>
								<?php }?>

							<?php } ?>

							<?php } ?>
						<?php }
						else { ?>

						 <a class="action_button btn btn-xl btn-danger bg-danger mb-2 mr-2 mt-2" id="btn_join" href="javascript:void(0)">Join</a>
							<a class="action_button btn btn-xl btn-primary mb-2 mr-2 mt-2" id="<?php echo ($is_followed) ? '' : 'btn_follow'; ?>" href="javascript:void(0)"><?php echo ($is_followed) ? 'Following' : 'Follow'; ?></a>
						<?php } ?>
						<?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == "superadmin" ) {   ?>
						<div class="secondery_item"  style="display:none;">
							<span class="secondery_btn">
								<i class="fas fa-ellipsis-h"></i>
							</span>
							<ul class="secondery_dropdown">
								<?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>
                            	 <li><a data-toggle="modal" data-target="#questionmodal" style="color:blue">
                            	 <?php
                            	 if($groups_items['group_Question'] != null || $groups_items['group_Question'] != ''){
                            	 echo 'Edit member request questionnaire';
                            	 }else{
                            	 	echo 'Create  member request questionnaire';
                            	 }
                            	 ?>
                            </a></li>

                             <?php  }?>
								<li>
									<a href="">Share</a>
								</li>
								 <?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>

								<?php  }?>
								<li>
									<a href="">
										<i class="far fa-star"></i>
										Add to favourites
									</a>
								</li>
								<li>
									<a href="">Pin</a>
								</li>
										<?php   if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin'){   ?>
								<li><a href="<?=base_url() . 'account/groupinfo/edit/'.$EncodedID ?>">Edit</a></li>

								<?php  }?>
								 <li>
									<a href="">Group Settings</a>
								</li>
								<li>
									<a href="">Notify Me</a>
								</li>
							</ul>
						</div>
						<?php  }?>
					</div>
					<div class="col-md-12 col-lg-12 px-0 py-3 group_container_info">
						<p class="lead text-gray-med p-0 m-0 mt-1">
						<?php
							if (strpos($_SERVER['REQUEST_URI'], '/groupinfo/') === false) {
//								'. $ShowAdminAction.'
								echo '<a class="editoption  " href="'.base_url().'account/groupinfo/detail/'.$EncodedID.'">Group Info</a>';
							}
						?>
						</p>
					</div>
				<?php } else { ?>
					<div class="group_btn">
						<a href="<?php echo base_url(); ?>pages/view/signup">
							<button class="btn" type="button">
								Login to Join
							</button>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>

		<!-- banner section -->
		<div class="gp_banner">
			<div class="banner_container"style="position: relative">
				<div class="col-md-12 col-lg-12 board-banner text-right bg-group group-cover-image <?php echo $groups_items['Gr_Backcolor']; ?>"
					<?php if(strlen($groups_items['Gr_Photo'])>0) echo 'style="box-shadow: 1px 0px 5px 3px #80808033;background:transparent url('.base_url() . str_replace(base_url(), '', $groups_items['Gr_Photo']).') no-repeat center center /cover; background-size: 100% 100%;"';?> >
					<div class="col-md-12 col-lg-12" style="height: 100%;">

				    </div>
				</div>
				<div class="bottomThumbnailActions">
					<div style="display:flex;gap:20px;justify-content: end;color:white;    align-items: center;">


						<?php if (isset($this->session->userdata['logged_in']['bs_id'])) { ?>
							<?php if($groups_items['Gr_Us_ID'] == $CurrUserID || $member_info['role'] == 'superadmin') { ?>
								<?php if($member_info['role_status'] != 'pending'){ ?>
									<div style="   border-radius: 9px!important; background: #7476779e!important; ">
										<form method="post">
											<label title="Change Board Cover" class="title-tip-left title-tip" for="upload_image" style="margin-bottom:0;">
												<img src="" id="uploaded_image" class="img-responsive img-circle" />
												<i class="fas fa-camera "></i>

												<input type="file" name="image" class="image" id="upload_image" style="display:none" accept="image/*" />
											</label>
										</form>

									</div>
								<?php }  ?>

							<?php }  ?>
						<?php } ?>
								<span class="PinnedClass"  style="
 padding: 7px;    z-index: 11;
 color: white;
 border-radius: 4px;
 cursor: pointer;
	top:0;
display:flex;
align-items:center;
gap:3px;">
											<img src="<?php echo base_url() ?>assetsofpop/thumbtackPinned.png"style="width: 21px;
    filter: invert(1);">
											<font class="mobile-hide" style="color:white;
 font-family: monospace;
 font-size: 0.9rem;
 font-weight: 500;
top:0;
right:0;">Pinned</font></span>



						<label class="title-tip" title="Like" for="likeIcon" style="margin-bottom:0;">
											<span class="group_hearts margin-24 ml-0" id="likeIcon"style="cursor: pointer;">


										<i  class="changeheart fa fa-heart icons_mobile"  style="color:white;  "></i>


													<font id="postcounter" style="font-size:14px;margin-left:3px;">1</font>

								<span class="mobile-hide" style="margin-left:5px;">Like</span>
								</span>
						</label>



						<label class="title-tip title-tip-left" title="Favourite" for="favIcon" style="margin-bottom:0;">
										<span class="group_hearts margin-24 ml-0"id="favIcon" style="cursor: pointer" onclick="make_plus()">


												<i class="changefav  fa fa-star icons_mobile" style="color:white;   "></i>
												<soan  class="mobile-hide"style="margin-left:5px;">Favourite</soan>



								</span>
						</label>

						<label class="title-tip title-tip-left" title="Share" for="shareIcon" style="margin-bottom:0;">
											<span class="group_hearts margin-24 ml-0"id="shareIcon"style="cursor: pointer;display:flex;">

							<img class="share_img"style="width:20px;filter:invert(1);"src=<?php echo base_url() ?>assetsofpop/share_icon.png>
							<span class="mobile-hide" style="margin-left:5px;">Share</span>
						</span>
						</label>


					</div>
				</div>

			</div>
		</div>
	</section>

	<style>
		.bottomThumbnailActions {
			display: none;
			height: 47px;
			background: linear-gradient(360deg, #80808078,transparent);
			position: absolute;
			bottom: 0;
			width: 100%;
			border-radius: 16px;
			justify-content: end;
			padding-right:1.5rem

		}
		.post-cover-image{
			border-radius: 16px;

		}
		.post-cover-image:hover{
			transition: all 0.5s ease;
		}
		.banner_container:hover .bottomThumbnailActions {
			display: flex;
		}</style>
	<?php

		if(isset($member_info) ){
			if( $member_info['role_status'] == 'pending' || $member_info['role'] == 'non_member'  ){

	  if ( $groups_items['Gr_Slug'] == 'twin_oaks_place'  && $groups_items['Gr_Privacy'] == 'private'    ) { ?>
		<section>

		<img src="<?php echo base_url().'/assets/img/admin_group_image/abs.jpg' ?>" style="width:100%;" >
		</section>
		 <?php }
			}
		}else if(isset($referrer)){
			if ($groups_items['Gr_Slug'] == 'twin_oaks_place'  && $groups_items['Gr_Privacy'] == 'private'    ) { ?>
			<section>

				<img src="<?php echo base_url().'/assets/img/admin_group_image/abs.jpg' ?>" style="width:100%;" >
			</section>
			<?php } ?>

		<?php }  ?>

	<?php

	if(isset($member_info) ){
		if( $member_info['role_status'] == 'pending' || $member_info['role'] == 'non_member'  ){

			if ( $groups_items['Gr_Slug'] == 'cathay_land_brokers'  && $groups_items['Gr_Privacy'] == 'private'    ) { ?>
				<section>

					<img src="<?php echo base_url().'/assets/img/admin_group_image/CathayLandInsert.jpg' ?>" style="width:100%;" >
				</section>
			<?php }
		}
	}else if(isset($referrer)){
		if ($groups_items['Gr_Slug'] == 'cathay_land_brokers'  && $groups_items['Gr_Privacy'] == 'private'    ) { ?>
			<section>

				<img src="<?php echo base_url().'/assets/img/admin_group_image/CathayLandInsert.jpg' ?>" style="width:100%;" >
			</section>
		<?php } ?>

	<?php }  ?>

	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	  	<div class="modal-dialog modal-lg" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<h5 class="modal-title">Upload Board Image</h5>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          			<span aria-hidden="true">×</span>
	        		</button>
	      		</div>
	      		<div class="modal-body" style ='padding-top: 0; padding-bottom: 4px;'>
	        		<div class="img-container">
	            		<div class="row">
	                		<div class="col-md-8">
	                    		<img src="" id="crop_uploaded_image" />
	                		</div>
	                		<div class="col-md-4">
	                    		<div class="preview"></div>
					      		<div class="modal-footer" style='padding-right: 0px;'>
					      			<button type="button" id="crop" class="btn btn-primary">Upload</button>
					        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					      		</div>
	                		</div>
	            		</div>
	        		</div>
	      		</div>
	    	</div>
	  	</div>
	</div>
	<input type="hidden" id="groupID" value="<?php echo $EncodedID; ?>">

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<?php if (!isset($this->session->userdata['logged_in']['bs_id'])) { ?>
		<script>
			localStorage.setItem("group_id", <?php echo $ID; ?>);
			localStorage.setItem("role", <?php echo $role; ?>);
 			localStorage.setItem("referrer", <?php echo $referrer; ?>);
		</script>
	<?php } ?>
<style>
.swal2-popup.swal2-modal.swal2-icon-success.swal2-show {
    z-index: 99999999999999!important;
}
.group_info_container.popped {
    z-index: 236!important;
}
</style>
<div class="modal" id="questionmodal">
  <div class="modal-dialog">
    <div class="modal-content" style="width:700px;">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Member Questionnaire</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

 			 <form method="post" action="" onsubmit=" return saveQuestion();" style="margin-bottom: 15px;">
 			 <input type="hidden" id="Gr_Id_QUestion" value="<?php echo $EncodedID;?>" >
                   <div class="row">
                       <div class="col-lg-12">
                           <div id="inputFormRow">
                               <div class="input-group mb-3">
                                   <input type="text" name="title[]" required class="form-control m-input" placeholder="Enter Question Here" autocomplete="off">
                                   <div class="input-group-append">
                                       <button id="removeRow" type="button" class="btn btn-danger">X</button>
                                   </div>
                               </div>
                           </div>

                           <div id="newRow"></div>
                           <button id="addRow" type="button" class="btn btn-info" >Add Question</button>
                           <input type="submit" value="Save Question" class="btn btn-success">
                       </div>
                   </div>
               </form>
               <div id="previousQuestion">
				<?php
				 if(isset($prequestion[0]) && $prequestion[0]['group_Question'] != ''){

				 ?>

			<?php
 					$preque =  json_decode($prequestion[0]['group_Question'] , true);
 				foreach($preque as $question){


				?>
					<div class="col-sm-12" id="questiondiv<?php print($question['id']); ?>">
						<div class="col-sm-12 ">
 					  <Label class="label label-primary" style="margin-top: 1px;    background: white;
    color: black;">Question:</Label>
 					<div class="col-sm-9">
				 		<input style="border:none;" type="text" class="form-control" readonly  id="textquestion<?php print($question['id']); ?>" value="<?php print($question['value']); ?>"/>
						 <div class="col-sm-3 row buttonClass" style="     justify-content: space-evenly;
    flex-direction: row-reverse;">
						<i style="position:relative;right:-9px;" class="fa fa-trash" onclick="deletequestion(<?php echo $question['id'] ?>)">
						</i>
						<i class="fa fa-edit" id="edit<?=$question['id'] ?>" onclick="editquestion(<?php echo $question['id'] ?>)">
							  &nbsp;Edit</i>
					   <i class="fas fa-save" id="save<?=$question['id'] ?>" style="display:none" onclick="savequestion(<?php echo $question['id'] ?>)">
						 &nbsp;Save</i>
						 </div>
 					</div>
				 	</div>
 				 </div>

				<?php
 		} }
				?>
               </div>
		</div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

	<div id="paymentpackageModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div  style="padding: 10px;">
					<div style="    display: grid;">
						<div>



							<a href="javascript:void(0 )">

								<div class="container-reward" style="float: right; ">
									<p style="margin-bottom : 0px; margin: 0px;  margin-right: 6px;">Rewards</p>

									<img src="https://boardspeak.com/assetsofpop/img/icon-coin-no-border.svg" alt="">
								</div>
							</a>
						</div>

						<div>
							<h4 align="center" class="mt-4  mb-4" style="color:#008a00;"> Promote Group </h4>

						</div>
					</div>
					<div  class="m-4" style="display: inline-flex;">

						<?php

						if(empty($groups_items['Gr_Thumb'])){ ?>

							<img src="<?php echo base_url().'img/banner/create-topic.jpg' ?>"  style=" width: 170px;;
    border-radius: 10px;     height: 90px;"/>

						<?php }else{ ?>
							<img src="<?php echo base_url().$groups_items['Gr_Thumb'] ?>"  style=" width: 170px;;
    border-radius: 10px;     height: 90px;"/>

						<?php }?>


						<h6 style="padding: 10px;" align="center"><?php echo $groups_items['Gr_Name']; ?></h6>
					</div>

					<div class="mr-4 mt-0 ml-4" style="background-color: #06CBD1; border-radius: 10px; padding: 38px 6px;">
						<h3 align="center" style="color:#FFFFFF;">Boost your Brand</h3>
						<p align="center">
							Showcase your group in <br> <b>EXPLORE FEATURED SECTION</b>
						</p>
						<center>
							<button style="
								 	width: 85%;
								 	font-weight: bold;
									background: white;
									color: #28a745;
									border: 0px;
									margin: 10px;"
									onclick="selectPackage()" class="btn btn-info">
								<?php
								if($this->session->userdata['logged_in']['bs_admin'] == 1 ){
									echo 'Start 30 Day Free Trial';
								}else{
									echo 'Contact Us';
								}
								?>

								</button>

						</center>
							</div>
					<p align="center" style="    padding: 0px 76px;">By purchasing ad promotion above,
						you accept our Terms of Service</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

  <script type="text/javascript">


	  function makePostPromote(option = ''){
		  if(option == '1'){
			  Swal.fire({
				  icon: 'success',
				  title: 'Promote Group Requested',
				  text: 'Request to promote group is under review. Check your Permission Notification to know if your ad is approved and promoted.',
				  confirmButtonColor:'#15AFE8',
				  confirmButtonText : 'Okay'
			  });
		  }else{


				  $('#paymentpackageModal').modal('show');


		  }

	  }
	  function selectPackage(){
		  let adminAccount =	'<?php if(isset($this->session->userdata['logged_in']['bs_admin'])){
			  echo $this->session->userdata['logged_in']['bs_admin'];
		  } ?>';
		  if(adminAccount == 1){

			  $.ajax({
				  url: base_url + 'account/makepostpromote',
				  method: 'POST',
				  dataType: 'json',
				  data: {

					  Group_id:  $('#groupID').val(),
					  packageID : 0,
					  promoteType : 'group'
				  },
				  success: function (response) {

					  if(response == 'success'){
						  Swal.fire({
							  icon: 'success',
							  title: 'Group is Promoted!',
							  text: 'Your group is now featured in Explore page!',
							  confirmButtonColor:'#15AFE8',
							  confirmButtonText : 'Okay'

						  });
						  $('#promotebutton').text('Promoted');
					  }
					  else if(response == "underreview"){
						  Swal.fire({
							  icon: 'info',
							  title: 'Promoted group request is under review',
							  text: 'You will be notified when group is promoted',

						  });
						  $('#promotebutton').text('Promoted');
					  }
					  else if(response == 'already'){
						  Swal.fire({
							  icon: 'info',
							  title: 'Already promoted group',
							  text: 'Your post is already promoted',

						  });
						  $('#promotebutton').text('Promoted');
					  }else{
						  Swal.fire({
							  icon: 'error',
							  title: 'Something went wrong, try again',
							  text: 'error',

						  });
					  }
				  }

			  });
		  }else{
			  showcontectus();
			  // $('#otherpop').modal('show');
		  }


	  }



         // add row
         $("#addRow").click(function () {
             var html = '';
             html += '<div id="inputFormRow">';
             html += '<div class="input-group mb-3">';
             html += '<input type="text" name="title[]"  required class="form-control m-input" placeholder="Enter Question" autocomplete="off">';
             html += '<div class="input-group-append">';
             html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
             html += '</div>';
             html += '</div>';

             $('#newRow').append(html);
         });

         // remove row
         $(document).on('click', '#removeRow', function () {
             $(this).closest('#inputFormRow').remove();
         });

		function saveQuestion(){
		var groupID = $('#Gr_Id_QUestion').val();
		var questionvalues = $("input[name='title[]']")
              .map(function(){return $(this).val();}).get();


			let QuestionArray = [];
 		for(var x = 0 ;  x < questionvalues.length; x++){
 		let data = {};
 		      id = Math.floor(Math.random() * 99999999999999999999);
			data.id = id ;
			data.value = questionvalues[x];
			QuestionArray.push(data);

 		}

			 let Data = {
			 'groupid' : groupID ,
			 'question' : QuestionArray
			 };
 		let Data1 = {
			 'groupid' : groupID
			 };
          $.ajax({
              url: base_url + 'account/saveQuestion/',
               method: 'POST',
                dataType: 'json',
                data: Data,
                    success: function (response) {
 			     console.log(response);
     			       		if(response == 'success'){
     			       		$('#inputFormRow input').val('');
     			       		$('#newRow').empty();
     			       		 Swal.fire({
                               icon: 'success',
                               title: 'Successful',
                               text: 'Question/s added in questionnaire',

                             })

                              $.ajax({
                                           url: base_url + 'account/getSaveQuestion/',
                                            method: 'POST',
                                             dataType: 'json',
                                             data: Data1,
                                                 success: function (response) {
                              			     		console.log(response);
														 console.log(JSON.parse(response[0]['group_Question']) );

                              			     	 if(response[0]){
                              			     	 $questiondata = 	JSON.parse(response[0]['group_Question']) ;
												$('#previousQuestion').empty();
                              			     	$questionString = '';

													for($i = 0 ;  $i < $questiondata.length ; $i++){
												 $questionString  += '<div class="col-sm-12" id="questiondiv'+$questiondata[$i]['id']+'">'+
												 '<div class="col-sm-12 ">'+
												 '<Label style="margin-top: 1px;    background: white;color: black;" class="label label-primary">Question:</Label>'+
												 '<div class="col-sm-9">'+
												 '<input style="border:none;" type="text" class="form-control" id="textquestion'+$questiondata[$i]['id']+'" readonly value="'+$questiondata[$i]['value']+'"/>'+
												 
												 '<div class="col-sm-3 row buttonClass"  style=" float: right;   padding: 0px 11px;justify-content: space-evenly;flex-direction: row-reverse;">'+
												 '<i style="position:relative;right:-9px; " class="fa fa-trash" onclick="deletequestion('+$questiondata[$i]['id']+')">'+
												 '</i>'+
												 '<i class="fa fa-edit" id="edit'+$questiondata[$i]['id']+'" onclick="editquestion('+$questiondata[$i]['id']+')">'+
                                                  ' Edit</i>'+
                                                   '<i class="fas fa-save" id="save'+$questiondata[$i]['id']+'" style="display:none" onclick="savequestion('+$questiondata[$i]['id']+')">'+
                                                ' Save</i>'+
												 '</div>'+
												 '</div>'+
												 '</div>'+
												 '</div>';

													}
													$('#previousQuestion').append($questionString);
                              			     		}
                                  			 	   return false;
                              					  }
                                              });
     			       		}else{
     			       		Swal.fire({
                                                           icon: 'error',
                       title: 'Try Again , Some thing went Wrong',
                                                           text: 'error',

                                                         })
     			       		}


                    return false;
 					  }
                 });
                  return false;
		}


		function deletequestion(id){

		$('#textquestion'+id).val();
		 let Data = {
        			 'groupid' : $('#Gr_Id_QUestion').val() ,
        			 'question_Id' : id,
   					 };

                  $.ajax({
                      url: base_url + 'account/deleteQuestion/',
                       method: 'POST',
                        dataType: 'json',
                        data: Data,
                            success: function (response) {

                            if(response == 'success'){
                            $('#questiondiv'+id).hide();
                             Swal.fire({
                               icon: 'success',
                               title: 'success',
                               text: 'Question Delete SuccessFully',

                             })
                            }else{
                             Swal.fire({
                                                           icon: 'error',
                                                           title: 'error',
                                                           text: 'Question not Delete',

                                                         })
                            }
                            }
                            });

		}
		function editquestion(id){

		$('#edit'+id).hide();
		$('#save'+id).show();
		  $('#textquestion'+id).addClass('border');
		  $('#textquestion'+id).prop("readonly", false);
		  $('#save'+id).addClass('colorClass')

		 }
        function savequestion(id){

		$('#edit'+id).show();
		$('#save'+id).hide();

	    		$('#textquestion'+id).val();
        		 let Data = {
                			 'groupid' : $('#Gr_Id_QUestion').val() ,
                			 'question_Id' : id,
                			 'updateQuestion' : $('#textquestion'+id).val(),
           					 };

                          $.ajax({
                              url: base_url + 'account/updateQuestion/',
                               method: 'POST',
                                dataType: 'json',
                                data: Data,
                                    success: function (response) {

                                    if(response == 'success'){
                                     $('#save'+id).removeClass('colorClass')
                                       $('#textquestion'+id).removeClass('border')
                                    $('#textquestion'+id).prop("readonly", true);

                                     Swal.fire({
                                       icon: 'success',
                                       title: 'success',
                                       text: 'Question Update  SuccessFully',

                                     })
                                    }else{
                                     Swal.fire({
                                                                   icon: 'error',
                                                                   title: 'error',
                                                                   text: 'Question not Update',

                                                                 })
                                    }
                                    }
                                    });
       }
     </script>
     <style>
     label.label.label-primary {
         background: #0000ff85;
         color: whiite;
         color: white;
         padding: 2px;
         font-size: 14px;
         border-radius: 5px;
     }

     .buttonClass i {
         margin: 0;
         color:grey;
     }

     .border{
     	border: 1px solid green!important;
     }

     .colorClass{
     color : green!important
     }

     pre.group_taggline.hide-class div {
         font-size: 18px!important;
     }

	 #noty_layout__topRight {
		 top: 20px;
		 right: 4%!important;
		 width: 325px;
	 }
     </style>
	<div class="modal fade bd-example-modal-lg" id="packagesmodel1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- This snippet uses Font Awesome 5 Free as a dependency. You can download it at fontawesome.io! -->
				<section class="pricing py-5" style="overflow-y: scroll;
													height: 600px;
													overflow-x: hidden;">
					<div class="container">
						<h4 align="center" style="color: white; margin-bottom: 20px;">Choose Your Plan</h4>
						<h6 align="center" style="margin-bottom: 22px;"><font style="color: #ffffff;" onclick="contatus()">Contact Us </font> for a more customized plan </h6>
						<div class="row">
							<!-- Free Tier -->
							<div class="col " id="firstPackege">
								<div class="card mb-5 mb-lg-0">
									<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">FREE</h4>
									<div class="card-body">

										<h5 class="card-title text-muted text-uppercase text-center">Free</h5>
										<h6 class="card-title text-center">PHP<span
													class="card-price text-center">0<span class="period">/month</span></h6>
										<h6 class="card-title text-muted text-uppercase text-center">No Payment Required</h6>
										<hr>
										<ul class="fa-ul">
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>1GB Storage</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting in Private Mode (with Select Members of the Group only)</li>

											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 6 Group Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting with Mute/Unmute Select Members Feature</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 5 Task Assignments/To Do's per Registered Member</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>

											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Priority Support </li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Monthly Subscription</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Customize your group page (background and fonts)</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>

										</ul>
										<div class="d-grid">
										 <button  style="width: 100%;background: #1887f2 !important; "  onclick="choosePayment1(1)" class="btn btn-primary text-uppercase" id="firstpage_button">Choose</button>
										</div>
									</div>
								</div>
							</div>
							<!-- Plus Tier -->
							<div class="col ">
								<div class="card mb-5 mb-lg-0">
									<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
									<div class="card-body">

										<h5 class="card-title text-muted text-uppercase text-center">Plus</h5>
										<h6 class="card-title text-center">PHP<span
													class="card-price text-center">599<span class="period">/month</span></h6>

										<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

										<hr>
										<ul class="fa-ul">
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>10GB Storage</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


										</ul>
										<div class="d-grid">
											 <a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment1(2)" class="btn btn-primary text-uppercase">Choose</a>
										</div>
									</div>
								</div>
							</div>
							<!-- Pro Tier -->
							<div class="col ">
								<div class="card mb-5 mb-lg-0">
									<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
									<div class="card-body">
										<h5 class="card-title text-muted text-uppercase text-center">PRO</h5>
										<h6 class="card-title text-center">PHP<span
													class="card-price text-center">899<span class="period">/month</span></h6>

										<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

										<hr>
										<ul class="fa-ul">
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>30GB Storage</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>5 Sub-groups for multiple divisions / department's use</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Sub-groups own set of 6 Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


										</ul>
										<div class="d-grid">
											<a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment1(3)" class="btn btn-primary text-uppercase">Choose</a>
										</div>
									</div>
								</div>
							</div>
				</section>

				<style>
					section.pricing {
						background: #007bff;
						background: linear-gradient(to right, #33AEFF, #398cb3);
					}

					.pricing .card {
						border: none;
						border-radius: 1rem;
						transition: all 0.2s;
						box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
					}

					.pricing hr {
						margin: 1.5rem 0;
					}

					.pricing .card-title {
						margin: 0.5rem 0;
						font-size: 0.9rem;
						letter-spacing: .1rem;
						font-weight: bold;
					}

					.pricing .card-price {
						font-size: 3rem;
						margin: 0;
					}

					.pricing .card-price .period {
						font-size: 0.8rem;
					}

					.pricing ul li {
						margin-bottom: 1rem;
					}

					.pricing .text-muted {
						opacity: 0.7;
					}

					.pricing .btn {
						font-size: 80%;
						border-radius: 5rem;
						letter-spacing: .1rem;
						font-weight: bold;
						padding: 1rem;
						opacity: 0.7;
						transition: all 0.2s;
					}

					/* Hover Effects on Card */

					@media (min-width: 992px) {
						.pricing .card:hover {
							margin-top: -.25rem;
							margin-bottom: .25rem;
							box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.3);
						}

						.pricing .card:hover .btn {
							opacity: 1;
						}
					}
				</style>

			</div>
		</div>
	</div>

	<div class="modal fade bd-example-modal-lg" id="packagesmodel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- This snippet uses Font Awesome 5 Free as a dependency. You can download it at fontawesome.io! -->
				<section class="pricing py-5" style="overflow-y: scroll;
													height: 600px;
													overflow-x: hidden;">
					<div class="container">
						<h4 align="center" style="color: white; margin-bottom: 20px;">Choose Your Plan</h4>
						<h6 align="center" style="margin-bottom: 22px;"><font style="color: #ffffff;" onclick="contatus()">Contact Us </font> for a more customized plan </h6>
						<div class="row">
							<!-- Free Tier -->
							<div class="col " id="firstPackege">
								<div class="card mb-5 mb-lg-0">
									<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">FREE</h4>
									<div class="card-body">

										<h5 class="card-title text-muted text-uppercase text-center">Free</h5>
										<h6 class="card-title text-center">PHP<span
													class="card-price text-center">0<span class="period">/month</span></h6>
										<h6 class="card-title text-muted text-uppercase text-center">No Payment Required</h6>
										<hr>
										<ul class="fa-ul">
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>1GB Storage</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting in Private Mode (with Select Members of the Group only)</li>

											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 6 Group Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Board Posting with Mute/Unmute Select Members Feature</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 5 Task Assignments/To Do's per Registered Member</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>

											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Priority Support </li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Monthly Subscription</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Customize your group page (background and fonts)</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>

										</ul>
										<div class="d-grid">
<!--											<button  style="width: 100%;background: #1887f2 !important; "  onclick="choosePayment(1)" class="btn btn-primary text-uppercase" id="firstpage_button">Choose</button>-->
										</div>
									</div>
								</div>
							</div>
							<!-- Plus Tier -->
							<div class="col ">
								<div class="card mb-5 mb-lg-0">
									<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
									<div class="card-body">

										<h5 class="card-title text-muted text-uppercase text-center">Plus</h5>
										<h6 class="card-title text-center">PHP<span
													class="card-price text-center">599<span class="period">/month</span></h6>

										<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

										<hr>
										<ul class="fa-ul">
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>10GB Storage</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups for multiple divisions / department's use</li>
											<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Sub-groups own set of Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


										</ul>
										<div class="d-grid">
<!--											<a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment(2)" class="btn btn-primary text-uppercase">Choose</a>-->
										</div>
									</div>
								</div>
							</div>
							<!-- Pro Tier -->
							<div class="col ">
								<div class="card mb-5 mb-lg-0">
									<h4 align="center" style="    width: 100%;
    background: #1887f2;
        border-radius: 10px 10px 0px 0px;
    color: white;
    padding: 16px;">90 Day FREE Trial</h4>
									<div class="card-body">
										<h5 class="card-title text-muted text-uppercase text-center">PRO</h5>
										<h6 class="card-title text-center">PHP<span
													class="card-price text-center">899<span class="period">/month</span></h6>

										<h6 class="card-title text-muted text-uppercase text-center">Billed Annually</h6>

										<hr>
										<ul class="fa-ul">
											<li><span class="fa-li"><i class="fas fa-check"></i></span>One Group (Public or Private)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Number of Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>30GB Storage</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Postings</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting in Private Mode (with Select Members of the Group only)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Max 15 Group Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Board Posting with Mute/Unmute Select Members Feature</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Task Assignments/To Do's per Registered Member</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>User Profile Dashboard & Notifications for Registered Members</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>5 Sub-groups for multiple divisions / department's use</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Sub-groups own set of 6 Categories/Subcategories (any combination)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Priority Support </li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: 3 Months Subscription</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Customize your Group Page (background and fonts)</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited FREE Offer: Feature (Promote) your Group in the Site</li>
											<li><span class="fa-li"><i class="fas fa-check"></i></span>Limited PROMO Offer: Display your Ad on your Group's Join & Pending Request pages for only PHP299</li>


										</ul>
										<div class="d-grid">
											<a style="width: 100%; background: #1887f2 !important; " href="javascript:void(0)" onclick="choosePayment(3)" class="btn btn-primary text-uppercase">Choose</a>
										</div>
									</div>
								</div>
							</div>
				</section>

				<style>
					section.pricing {
						background: #007bff;
						background: linear-gradient(to right, #33AEFF, #398cb3);
					}

					.pricing .card {
						border: none;
						border-radius: 1rem;
						transition: all 0.2s;
						box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
					}

					.pricing hr {
						margin: 1.5rem 0;
					}

					.pricing .card-title {
						margin: 0.5rem 0;
						font-size: 0.9rem;
						letter-spacing: .1rem;
						font-weight: bold;
					}

					.pricing .card-price {
						font-size: 3rem;
						margin: 0;
					}

					.pricing .card-price .period {
						font-size: 0.8rem;
					}

					.pricing ul li {
						margin-bottom: 1rem;
					}

					.pricing .text-muted {
						opacity: 0.7;
					}

					.pricing .btn {
						font-size: 80%;
						border-radius: 5rem;
						letter-spacing: .1rem;
						font-weight: bold;
						padding: 1rem;
						opacity: 0.7;
						transition: all 0.2s;
					}

					/* Hover Effects on Card */

					@media (min-width: 992px) {
						.pricing .card:hover {
							margin-top: -.25rem;
							margin-bottom: .25rem;
							box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.3);
						}

						.pricing .card:hover .btn {
							opacity: 1;
						}
					}
				</style>

			</div>
		</div>
	</div>
