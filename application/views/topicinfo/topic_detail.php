
		<section class="group_info">


			<div class="div_group_info_container container">
 				<!-- tab nave -->
				<div class="grup_info_nav">
					<ul>
						<li class="nav-item">
							<a class="  nav-link active" id="post-info">
								Subgroup Info
							</a>
						</li>
						<?php   if($this->LoggedInUser == $topics_items['To_Us_ID']  || $creator_info['role'] == 'creator'){ ?>

						<li class="nav-item">
							<a class="nav-link" id="manage-user">
								Manage Users
							</a>
						</li>

							<?php
						}   ?>
					</ul>
				</div>
			<div id="topic-info-container" class="nav-container">
				<form class="div_group_info_container container">
				<div class="control-group mb-2 form-group">							
					<label class="font-weight-bold label-h6">Group/Community Name</label>
					<div class="admin-form-group controls mb-0 pb-1">
						<div class="input-group">
							<select class="custom-select" id="selGroup" name="selGroup" required="required" disabled="disabled">
								<option value="">Choose group</option>
								<?php foreach ($groups_items as $item): ?>
									<option value="<?php echo $item['Gr_ID'];?>" <?php if (strcmp($topics_items['To_Gr_ID'],$item['Gr_ID'])==0) echo 'selected';?>><?php echo $item['Gr_Name'];?></option>	
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

				<div class="control-group mb-2 form-group">
					<label class="font-weight-bold label-h6">Category</label>
					<div class="admin-form-group controls pb-1">
						<div class="input-group">
							<select class="custom-select" id="selCategory" name="selCategory" required="required" disabled="disabled">
								<option value="">Choose category</option>
								<?php foreach ($category_items as $item): ?>
									<option value="<?php echo $item['Ca_ID'];?>" <?php if (strcmp($topics_items['To_Ca_ID'],$item['Ca_ID'])==0) echo 'selected';?>><?php echo $item['Ca_Name'];?></option>
								<?php endforeach; ?>
								<option value="0" <?php if (strcmp($topics_items['To_Ca_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
							</select>
						</div>
					</div>
				</div>

				<div class="control-group mb-2 form-group" id="selNewCategoryBox" style="<?php if (strcmp($topics_items['To_Ca_ID'],'0')!=0) echo 'display:none;';?>">
					<div class="admin-form-group controls pb-1">									
						<input class="form-control" id="txtNewCategory" name="txtNewCategory" type="text" value="<?php echo $topics_items['To_Ca_Name'];?>" placeholder="Enter new category" disabled="disabled">
					</div>
				</div>

				<div class="control-group mb-2 form-group">
					<label class="font-weight-bold label-h6">Sub-Category</label>
					<div class="admin-form-group controls pb-1">
						<div class="input-group">
							<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required" disabled="disabled">
								<option value="">Choose sub-category</option>
								<?php foreach ($subcategory_items as $item): ?>
								<option value="<?php echo $item['Sc_ID'];?>" <?php if (strcmp($topics_items['To_Sc_ID'],$item['Sc_ID'])==0) echo 'selected';?>><?php echo $item['Sc_Name'];?></option>
								<?php endforeach; ?>
								<option value="0" <?php if (strcmp($topics_items['To_Sc_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
							</select>
						</div>
					</div>
				</div>
							
				<div class="control-group mb-2 form-group" id="selNewSubCategoryBox" style="<?php if (strcmp($topics_items['To_Sc_ID'],'0')!=0) echo 'display:none;';?>">
					<div class="admin-form-group controls pb-1">									
						<input class="form-control" id="txtNewSubCategory" name="txtNewSubCategory" type="text" value="<?php echo $topics_items['To_Sc_Name'];?>" placeholder="Enter new sub-category" disabled="disabled">
					</div>
				</div>

				<div class="control-group mb-2 form-group">
					<label class="font-weight-bold label-h6">Topic</label>
					<div class="admin-form-group controls mb-0 pb-1">
						<input class="form-control" id="txtTitle" name="txtTitle" type="text" value="<?php echo $topics_items['To_Name'];?>" placeholder="Enter topic name" required="required" disabled="disabled">
					</div>
				</div>

				<div class="control-group mb-4 form-group">
					<label class="font-weight-bold label-h6">Description</label>
					<div class="admin-form-group controls mb-0 pb-1">
						<input class="form-control" id="txtDescription" name="txtDescription" value="<?php echo $topics_items['To_Description'];?>" type="text" placeholder="What is your topic about?" required="required" disabled="disabled">
					</div>
				</div>

				<div class="control-group mb-2 form-group">
					<label class="font-weight-bold label-h6">Keyword Tags</label>
					<div class="admin-form-group controls mb-0 pb-1">
						<input class="form-control" id="txtKeywords" name="txtKeywords" type="text" value="<?=$topics_items['To_Keywords'];?>" disabled>
					</div>
				</div>

				<div class="control-group mb-4 form-group">
					<div class="admin-form-group controls mb-0 pb-1 radio_btn_group">
						<label class="font-weight-bold label-h6">Privacy</label>

						<!-- Default unchecked -->
						<div class="custom-control custom-radio pb-2 radio_btn_item">
							<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public"  disabled="disabled" <?php if (strcmp($topics_items['To_Privacy'],'public')==0) echo 'checked'; ?>>
							<label class="custom-control-label" for="radPublic">Public </label><br>Anyone in your group can access and join the conversation.
						</div>

						<!-- Default checked -->
						<div class="custom-control custom-radio pb-2 radio_btn_item">
							<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" disabled="disabled" <?php if (strcmp($topics_items['To_Privacy'],'private')==0) echo 'checked'; ?>>
							<label class="custom-control-label" for="radPrivate">Private </label><br>Anyone in your group can find your subgroup. But access to join the conversations, is limited to invited members and approved by admin.
						</div>

						<!-- Default checked -->
						<div class="custom-control custom-radio pb-2 radio_btn_item">
							<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" disabled="disabled" <?php if (strcmp($topics_items['To_Privacy'],'secret')==0) echo 'checked'; ?>>
							<label class="custom-control-label" for="radSecret">Secret </label><br>Only those with link and unique password in your group, can find and join your topic.
						</div>

					</div>
				</div>
						 <?php if(isset($this->LoggedInUser) &&   $topics_items['To_Us_ID'] == $this->LoggedInUser  || $creator_info['role'] == 'creator'){ ?>
				<div class="admin-form-group">
					<a href="<?php echo base_url(); ?>account/view/topic/<?php echo $EncodedID;?>" class="btn btn-xl btn-light mr-2">Cancel</a>
					<a href="<?php echo base_url(); ?>account/topicinfo/edit/<?php echo $EncodedID;?>" class="btn btn-xl btn-primary mr-2" style="background: white;  color: #398cb3;">Edit Subgroup Info</a>
					<a href="#" data-href="<?php echo base_url(); ?>account/topicinfo/delete/<?php echo $EncodedID;?>" class="btn btn-xl btn-danger"  data-toggle="modal" data-target="#confirm-delete">Delete Topic</a>								
				</div>
				<?php } ?>
				</form>
	        </div>
				<div id="manage-user-container" class="nav-container" style="display:none;">

					<!-- here need to be shown the manages users-->
					<div class="  user_role">
						<h6>
							Manage user roles
						</h6>
						<div class="search_member form-group">
							<input class="form-control" type="text" id="user_filter">
							<button class="btn btn-info" type="button" id="btnUserSearch">
								search
							</button>
						</div>
						<ul class="user_role_list">
							<li>
								<a disabled>Roles <i class="fas fa-filter"></i></a>
							</li>
							<li>
								<a class="role_option active" id="tab_all_members" role_tab_id="all_members">All</a>
							</li>
							<!--<li>
								<a class="role_option" id="tab_super_admin" role_tab_id="super_admin">Super Admin</a>
							</li>-->
							<li>
								<a class="role_option" id="tab_admin" role_tab_id="admin">Admin</a>
							</li>
							<li>
								<a class="role_option" id="tab_member" role_tab_id="member">Members</a>
							</li>
							<li>
								<a class="role_option" id="tab_follower" role_tab_id="follower">Followers</a>
							</li>
							<li>
								<a class="role_option" id="tab_pending" role_tab_id="pending">Join Requests / Invites</a>
							</li>
						</ul>
						<div class="scroll_body">
							<div class="suggest_member_tabs">
								<ul id="all_members" class="user_list suggest_members"></ul>
								<ul id="super_admin" class="user_list suggest_members"></ul>
								<ul id="admin" class="user_list suggest_members"></ul>
								<ul id="member" class="user_list suggest_members"></ul>
								<ul id="follower" class="user_list suggest_members"></ul>
								<ul id="pending" class="user_list suggest_members"></ul>
								<ul id="no_role" class="user_list suggest_members"></ul>
							</div>
						</div>
					</div>


				</div>
			</div>
		</section>
	</main><!-- /maincontent -->
<input type="hidden" name="TopicID" value="<?=$EncodedID?>" />
		<?php $this->load->view('account/user_roles_template'); ?>
<style type="text/css">
	.bootstrap-tagsinput { 
	    border: none !important;
	    box-shadow: none !important;
	    background-color: transparent !important;
	    padding: 0 !important;
	}

	.bootstrap-tagsinput span[data-role="remove"] {
	    display:none !important;
}
</style>
