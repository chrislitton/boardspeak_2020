<section class="user-section entry-section" id="entryform">
	<div class="container">
	    <div class="row">
	        <div class="col-lg-12">                   
	            <?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
	            <?php if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>

                <?php
                    echo validation_errors();
                    echo form_open('account/topicinfo/updatedetail/'.$EncodedID);
                    echo form_hidden('TopicID', $EncodedID);
                ?>

	                <div class="control-group mb-2 form-group">
						<label class="form-required font-weight-bold label-h6">Group/Community Name</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<select class="custom-select" id="selGroup" name="selGroup" required="required">
									<option value="">Choose group</option>
									<?php foreach ($groups_items as $item): ?>
										<option value="<?php echo $item['Gr_ID'];?>" <?php if (strcmp($topics_items['To_Gr_ID'],$item['Gr_ID'])==0) echo 'selected';?>><?php echo $item['Gr_Name'];?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="form-required font-weight-bold label-h6">Category</label>
						<div class="admin-form-group controls pb-1">
						<div class="input-group">


							<select class="custom-select" id="selCategory" name="selCategory" required="required">
								<option value="">Choose category</option>
								<?php foreach ($category_items as $item): ?>
									<option value="<?php echo $item['Ca_ID'];?>" <?php if (strcmp($topics_items['To_Ca_ID'],$item['Ca_ID'])==0) echo 'selected';?>><?php echo $item['Ca_Name'];?></option>
								<?php endforeach; ?>
								<option value="0" <?php if (strcmp($topics_items['To_Ca_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
							</select>
						</div>
						</div>
					</div>

					<div class="control-group mb-2 form-group" id="selNewCategoryBox" style="<?php if (strcmp($topics_items['To_Ca_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">
							<input class="form-control" id="txtNewCategory" name="txtNewCategory" type="text" value="<?php echo $topics_items['To_Ca_Name'];?>" placeholder="Enter new category">
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="form-required font-weight-bold label-h6">Sub-Category</label>
						<div class="admin-form-group controls pb-1">
						<div class="input-group">

						  <select class="custom-select" id="selSubCategory" name="selSubCategory" required="required">
								<option value="">Choose sub-category</option>
								<?php foreach ($subcategory_items as $item): ?>
								<?php  if($topics_items['To_Sc_ID'] == '0'){?>
										<option value="0" <?php if (strcmp($topics_items['To_Sc_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>

									<?php }else{ ?>
								 <option value="<?php echo $item['Sc_ID'];?>" <?php if ($topics_items['To_Sc_ID'] === $item['Sc_ID']) echo 'selected';?>><?php echo $item['Sc_Name'];?></option>
 								<?php	} ?>
								<?php endforeach; ?>
							</select>
						</div>
						</div>
					</div>

					<div class="control-group mb-2 form-group" id="selNewSubCategoryBox" style="<?php if (strcmp($topics_items['To_Sc_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">									
							<input class="form-control" id="txtNewSubCategory" name="txtNewSubCategory" type="text" value="<?php echo $topics_items['To_Sc_Name'];?>" placeholder="Enter new sub-category">
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="form-required font-weight-bold label-h6">Subgroup</label>
						<div class="admin-form-group controls mb-0 pb-1">
						<input class="form-control" id="txtTitle" name="txtTitle" type="text" value="<?php echo $topics_items['To_Name'];?>" placeholder="Enter topic name" required="required">
						</div>
					</div>
					
					<div class="control-group mb-4 form-group">
						<label class="font-weight-bold label-h6">Description</label><span class="ml-1 text-primary font-weight-normal text-small">You may skip and fill this up later.</span>
						<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtDescription" name="txtDescription" value="<?php echo $topics_items['To_Description'];?>" type="text" placeholder="What is your topic about?" >
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Keyword Tags</label><span class="ml-1 text-primary font-weight-normal text-small">Tags will help people find your topic. Separate tags with comma.</span>
						<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtKeywords" name="txtKeywords" type="text" value="<?=$topics_items['To_Keywords'];?>">
						</div>
					</div>

					<div class="control-group mb-4 form-group">
						<div class="admin-form-group controls mb-0 pb-1 radio_btn_group">
							<label class="form-required font-weight-bold label-h6">Privacy</label>

							<!-- Default unchecked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public"  <?php if (strcmp($topics_items['To_Privacy'],'public')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPublic">Public </label><br>Anyone in your group can access and join the conversation.
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" <?php if (strcmp($topics_items['To_Privacy'],'private')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPrivate">Private </label><br>Anyone in your group can find your subgroup. But access to join the conversations, is limited to invited members and approved by admin.
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2 radio_btn_item">
								<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" <?php if (strcmp($topics_items['To_Privacy'],'secret')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radSecret">Secret </label><br>Only those with link and unique password in your group, can find and join your topic.
							</div>

						</div>
					</div>


					<!-- <div class="control-group mt-5 text-center"> -->
		                <div class="admin-form-group">
		                    <a href="<?php echo base_url(); ?>account/topicinfo/detail/<?php echo $EncodedID;?>" class="btn btn-xl btn-light">Cancel</a>
		                    <button type="submit" class="btn btn-xl btn-primary">Save Changes</button>
		                </div>
	                <!-- </div> -->
	                <br >					
	            </form>
	        </div>
		</div>
	</div>
</section>

