
		<!-- group info only visble to admin and editor -->
		<section class="group_info">
			<form class="div_group_info_container container">
				<!-- tab nave -->
				<div class="grup_info_nav">
					<ul class="nav text-center">
						<li class="nav-item">
							<a class="nav-link active" id="post-info">
								Post Edit
							</a>
						</li>
							<li class="nav-item">
								<a class="nav-link" id="post-images">
									Images
								</a>
							</li>
						<!-- <li class="nav-item">
							<a class="nav-link" id="post-videos">
								Videos
							</a>
						</li> -->
						<li class="nav-item">
							<a class="nav-link" id="post-files">
								Files
							</a>
						</li>
					</ul>
				</div>

			</form>

				<div id="post-info-container" class="nav-container">
				<section class="user-section entry-section" id="entryform">
					<div class="container">
					    <div class="row">
	     				  <div class="col-lg-12">
	       			     <?php if (strlen($error)>0) echo '<div class="text-center mb-1 text-danger">'.$error.'</div>';?>
	         			   <?php if (strlen($notification)>0) echo '<div class="text-center mb-1 text-success">'.$notification.'</div>';?>

                <?php

                    echo validation_errors();
                    echo form_open('account/postinfo/updatedetail/'.$EncodedID);
                    echo form_hidden('PostID', $EncodedID);
                ?>

					<div class="control-group mb-2">
						<label class="form-required font-weight-bold">Title</label>
						<div class="admin-form-group controls mb-0 pb-1">
						<input class="form-control" id="txtTitle" name="txtTitle" type="text" value="<?php echo $posts_item['Po_Title'];?>" placeholder="Enter title" required="required">
						</div>
					</div>

					<div class="control-group mb-2">
						<label class="font-weight-bold">Description</label><span class="ml-1 text-primary font-weight-normal text-small">You may skip and fill this up later.</span>
						<div class="admin-form-group controls mb-0 pb-1">
							<textarea class="form-control" id="txtDescription" name="txtDescription" ><?php echo $posts_item['Po_Description'];?></textarea>
						</div>
					</div>

	                <div class="control-group mb-2">
						<label class="form-required font-weight-bold">Group/Community Name</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<select class="custom-select" id="selGroup" name="selGroup" required="required">
									<option value="">Choose group</option>
									<?php foreach ($groups_items as $item): ?>
										<option value="<?php echo $item['Gr_ID'];?>" <?php if (strcmp($posts_item['Po_Gr_ID'],$item['Gr_ID'])==0) echo 'selected';?>><?php echo $item['Gr_Name'];?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>

	                <div class="control-group mb-2">
						<label class="font-weight-bold">Subgroup</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<select class="custom-select" id="selTopic" name="selTopic" required="required">
									<option value="0">None</option>
									<?php foreach ($topics_items as $item): ?>
										<option value="<?php echo $item['To_ID'];?>" <?php if (strcmp($posts_item['Po_To_ID'],$item['To_ID'])==0) echo 'selected';?>><?php echo $item['To_Name'];?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>

					<div class="control-group mb-2">
						<label class="form-required font-weight-bold">Category</label>
						<div class="admin-form-group controls pb-1">
						<div class="input-group">


							<select class="custom-select" id="selCategory" name="selCategory" required="required">
								<option value="">Choose category</option>
								<?php foreach ($category_items as $item): ?>
									<option value="<?php echo $item['Ca_ID'];?>" <?php if (strcmp($posts_item['Po_Ca_ID'],$item['Ca_ID'])==0) echo 'selected';?>><?php echo $item['Ca_Name'];?> </option>
								<?php endforeach; ?>
								<option value="0" <?php if (strcmp($posts_item['Po_Ca_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
							</select>
						</div>
						</div>
					</div>

					<div class="control-group mb-2" id="selNewCategoryBox" style="<?php if (strcmp($posts_item['Po_Ca_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">
							<input class="form-control"
							<?php if ( $posts_item['Po_Ca_ID'] == 0){
								echo 'required';
							}?>

								   id="txtNewCategory" name="txtNewCategory" type="text" value="<?php echo $posts_item['Po_Ca_Name'];?>" placeholder="Enter new category">
						</div>
					</div>

					<div class="control-group mb-2">
						<label class="form-required font-weight-bold">Sub-Category</label>
						<div class="admin-form-group controls pb-1">
						<div class="input-group">
							<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required">
								<option value="">Choose sub-category</option>
								<?php foreach ($subcategory_items as $item): ?>
								<option value="<?php echo $item['Sc_ID'];?>" <?php if (strcmp($posts_item['Po_Sc_ID'],$item['Sc_ID'])==0) echo 'selected';?>><?php echo $item['Sc_Name'];?></option>
								<?php endforeach; ?>
								<option value="0" <?php if (strcmp($posts_item['Po_Sc_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
							</select>
						</div>
						</div>
					</div>

					<div class="control-group mb-2" id="selNewSubCategoryBox"


						 style="<?php if (strcmp($posts_item['Po_Sc_ID'],'0')!=0) echo 'display:none;';?>">
						<div class="admin-form-group controls pb-1">
							<input class="form-control"  <?php if ( $posts_item['Po_Sc_ID'] == 0){
								echo 'required';
							}?> id="txtNewSubCategory" name="txtNewSubCategory" type="text" value="<?php echo $posts_item['Po_Sc_Name'];?>" placeholder="Enter new sub-category">
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Action Button</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<select class="custom-select" id="selActionButton"  name="selActionButton" placeholder='Choose your Action Button'>
									<option value=''>Choose your Action Button</option>
									<?php foreach ($button_items as $item): ?>
									<option value="<?php echo $item['Bn_ID'];?>" <?php if (strcmp($posts_item['Po_Bn_ID'],$item['Bn_ID'])==0) echo 'selected';?>><?php echo $item['Bn_Caption'];?></option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($posts_item['Po_Bn_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
									<option value="-1">Remove Action Button </option>
								</select>
							</div>
						</div>
						<script>
							// function changebutton(val){
							//
							//
							//
							// 	document.getElementById('selNewActionButton').style.display ='none';
							// 	document.getElementById('txtActionButton').style.display ='none';
							// 	document.getElementById("txtActionButton").required = false;
							// 	document.getElementById("txtLandingPage").disabled = true;
							// 	document.getElementById("txtLandingPage").required = false;
							//
							// 	if(val == -1){
							// 		// $('#txtLandingPage').prop('disabled', false);
							// 		// $('#txtLandingPage').prop('required', false);
							// 		document.getElementById("txtLandingPage").value = '';
							// 		document.getElementById("txtLandingPage").disabled = true;
							// 		document.getElementById("txtLandingPage").required = false;
							//
							// 		return;
							// 	}
							//
							// 	if(val == ''){
							//
							// 		document.getElementById("txtLandingPage").value = '';
							// 		document.getElementById("txtLandingPage").disabled = true;
							// 		document.getElementById("txtLandingPage").required = false;
							// 		return;
							// 	}
							//
							//
							//
							// 		if (val == "0") {
							//
							// 			$("#selNewActionButton").show();
							// 			$("#txtActionButton").prop('required', true);
							// 			$('#txtLandingPage').prop('disabled', false);
							// 			$('#txtLandingPage').prop('required', true);
							// 		}
							//
							// 		else {
							//
							//
							// 			$('#txtLandingPage').prop('disabled', false);
							// 			$('#txtLandingPage').prop('required', true);
							// 		}
							// 	}


						</script>
						<div class="control-group mb-2" id="selNewActionButton" style="<?php if (empty($posts_item['Po_Bn_Name']) && $posts_item['Po_Bn_ID'] != '0') echo 'display : none;' ;?>">
							<div class="admin-form-group controls pb-1">
								<input class="form-control" id="txtActionButton" value="<?php echo $posts_item['Po_Bn_Name'];?>" name="txtActionButton" type="text" placeholder="Enter new action caption">
							</div>
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Landing Page</label><span class="ml-1 text-primary font-weight-normal text-small" style="color:red!important;">Please complete the link (by adding https://www. )</span>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<input class="form-control" id="txtLandingPage" name="txtLandingPage" type="text" value="<?php echo $posts_item['Po_Bn_Page'];?>"
										placeholder="Enter the URL for your action button"
										<?php if (empty($posts_item['Bn_ID'])){ echo 'required';}?>
										<?php
										if(isset($posts_item['Po_Bn_Name']) && !empty($posts_item['Po_Bn_Name']) ){

										}else{
											if(isset($posts_item['Po_Bn_Page']) && !empty($posts_item['Po_Bn_Page'])){
												echo  'required' ;
											}else{
												echo 'disabled';
											}

										} ?>>

							</div>
						</div>
					</div>

					<div class="control-group mb-2">
						<label class="font-weight-bold">Keyword Tags</label><span class="ml-1 text-primary font-weight-normal text-small">Tags will help people find your post. Separate tags with comma.</span>
						<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtKeywords" name="txtKeywords" type="text" value="<?php echo $posts_item['Po_Keywords'];?>">
						</div>
					</div>

					<div class="control-group mb-4">
						<div class="admin-form-group controls mb-0 pb-1">
							<label class="form-required font-weight-bold">Privacy</label>

							<!-- Default unchecked -->
							<div class="custom-control custom-radio pb-2">
								<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public"  <?php if (strcmp($posts_item['Po_Privacy'],'public')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPublic">Public <br><i class="text-muted">Anyone in your group can access and join the conversation.</i></label>
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2">
								<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" <?php if (strcmp($posts_item['Po_Privacy'],'private')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPrivate">Private <br><i class="text-muted">Anyone in your group can find your subgroup. But access to join the conversations, is limited to invited members and approved by admin.</i></label>
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2">
								<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" <?php if (strcmp($posts_item['Po_Privacy'],'secret')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radSecret">Secret <br><i class="text-muted">Only those with link and unique password in your group, can find and join your topic.</i></label>
							</div>

						</div>
					</div>

	                <div class="admin-form-group">
	                    <a href="<?php echo base_url(); ?>account/postinfo/detail/<?php echo $EncodedID;?>" class="btn btn-xl btn-light">Cancel</a>
	                    <button type="submit" class="btn btn-xl btn-primary">Save Changes</button>
	                </div>

	            </form>
	        </div>
		</div>
	</div>
					</section>
				 </div>

				<div id="post-images-container" class="nav-container">
					<div class="file-loading">
						<input id="input_file_images" name="file_images[]" type="file" multiple>
					</div>
				</div>

				<div id="post-videos-container" class="nav-container">videos
				</div>

				<div id="post-files-container" class="nav-container">

					<div class="file-loading">
						<input id="input_file_others" name="file_others[]" type="file" multiple>
					</div>
				</div>
	        </div>

		</section>
		</main>

	<style>.input-group{white-space: nowrap;}</style>
			<script>
	  		 document.getElementById('editpostinfobutton').style.display = 'none';
			</script>
<style >
	.bootstrap-tagsinput {
	    border: none !important;
	    box-shadow: none !important;
	    background-color: transparent !important;
	    padding: 0 !important;
	}

	.bootstrap-tagsinput span[data-role="remove"] {
	    display:none !important;
}
</style>


