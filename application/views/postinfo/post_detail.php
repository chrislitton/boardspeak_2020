
		<!-- group info only visble to admin and editor -->
		<section class="group_info">

				<!-- tab nave -->
			<form class="div_group_info_container container">
				<div class="grup_info_nav">
					<ul class="nav text-center">
						<li class="nav-item">
							<a class="nav-link active" id="post-info">
								Post Info
							</a>
						</li>
						<?php

						  if($userType == 'superadmin' || $userType == 'admin'){ ?>
						<li class="nav-item">
								<a class="nav-link" id="post-images">
									Images
								</a>
							</li>

						<li class="nav-item">
							<a class="nav-link" id="post-files">
								Files
							</a>
						</li>



						<li class="nav-item">
							<a class="nav-link" id="manage-user">
								Manage Users
							</a>
						</li>

					 <?php 	} ?>

					</ul>
				</div>

				<div id="post-info-container" class="nav-container">
					<!-- for item -->
					<div class="form-group">
						<label class="font-weight-bold">Title</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtTitle" name="txtTitle" type="text" value="<?php echo $posts_item['Po_Title'];?>" placeholder="Enter post title" required="required" disabled="disabled">
						</div>
					</div>

					<div class="form-group">
						<label class="font-weight-bold">Description</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<textarea class="form-control" id="txtDescription" name="txtDescription" disabled="disabled"><?php echo $posts_item['Po_Description'];?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="font-weight-bold">Group/Community Name</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<select class="custom-select" id="selGroup" name="selGroup" required="required" disabled="disabled">
									<option value="">Choose group</option>
									<?php foreach ($groups_items as $item): ?>
										<option value="<?php echo $item['Gr_ID'];?>" <?php if (strcmp($posts_item['Po_Gr_ID'],$item['Gr_ID'])==0) echo 'selected';?>><?php echo $item['Gr_Name'];?></option>	
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="font-weight-bold">Subgroup</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group"><? var_dump($topics_items);?>
								<select class="custom-select" id="selTopic" name="selTopic" required="required" disabled="disabled">
									<option value="">None</option>
									<?php foreach ($topics_items as $item): ?>
										<option value="<?php echo $item['To_ID'];?>" <?php if (strcmp($posts_item['Po_To_ID'],$item['To_ID'])==0) echo 'selected';?>><?php echo $item['To_Name'];?></option>	
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="font-weight-bold">Category</label>
						<div class="admin-form-group controls pb-1">
							<div class="input-group">
								<select class="custom-select" id="selCategory" name="selCategory" required="required" disabled="disabled">
									<option value="">Choose category</option>
									<?php foreach ($category_items as $item): ?>
										<option value="<?php echo $item['Ca_ID'];?>" <?php if (strcmp($posts_item['Po_Ca_ID'],$item['Ca_ID'])==0) echo 'selected';?>><?php echo $item['Ca_Name'];?> </option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($posts_item['Po_Ca_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="control-group mb-2" id="selNewCategoryBox" style="<?php if (strcmp($posts_item['Po_Ca_ID'],'0')!=0) echo 'display:none;';?>">
							<div class="admin-form-group controls pb-1">
								<input class="form-control" id="txtNewCategory" name="txtNewCategory" type="text" value="<?php echo $posts_item['Po_Ca_Name'];?>" placeholder="Enter new category" disabled="disabled">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="font-weight-bold">Sub-Category</label>
						<div class="admin-form-group controls pb-1">
							<div class="input-group">
								<select class="custom-select" id="selSubCategory" name="selSubCategory" required="required" disabled="disabled">
									<option value="">Choose sub-category</option>
									<?php foreach ($subcategory_items as $item): ?>
									<option value="<?php echo $item['Sc_ID'];?>" <?php if (strcmp($posts_item['Po_Sc_ID'],$item['Sc_ID'])==0) echo 'selected';?>><?php echo $item['Sc_Name'];?></option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($posts_item['Po_Sc_ID'],'0')==0) echo 'selected';?>>Others, please specify</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="control-group mb-2" id="selNewSubCategoryBox" style="<?php if (strcmp($posts_item['Po_Sc_ID'],'0')!=0) echo 'display:none;';?>">
							<div class="admin-form-group controls pb-1">
								<input class="form-control" id="txtNewSubCategory" name="txtNewSubCategory" type="text" value="<?php echo $posts_item['Po_Sc_Name'];?>" placeholder="Enter new sub-category" disabled="disabled">
							</div>
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Action Button</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<select class="custom-select" id="selActionButton" name="selActionButton" placeholder='Choose your Action Button' disabled="disabled">
									<option value=''>Choose your Action Button</option>
									<?php foreach ($button_items as $item): ?>
									<option value="<?php echo $item['Bn_ID'];?>" <?php if (strcmp($posts_item['Po_Bn_ID'],$item['Bn_ID'])==0) echo 'selected';?>><?php echo $item['Bn_Caption'];?></option>
									<?php endforeach; ?>
									<option value="0" <?php if (strcmp($posts_item['Po_Bn_ID'],0)==0) echo 'selected';?>>Others, please specify</option>
								</select>
							</div>
						</div>

						<div class="control-group mb-2" id="selNewActionButton" style="<?php if (empty($posts_item['Po_Bn_Name']) && $posts_item['Po_Bn_ID'] != '0') echo 'display:none;';?>">
							<div class="admin-form-group controls pb-1">
								<input class="form-control" id="txtActionButton" value="<?php echo $posts_item['Po_Bn_Name'];?>" name="txtActionButton" type="text" placeholder="Enter new action caption"  disabled="disabled">
							</div>
						</div>
					</div>

					<div class="control-group mb-2 form-group">
						<label class="font-weight-bold label-h6">Landing Page</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<div class="input-group">
								<input class="form-control" id="txtLandingPage" name="txtLandingPage" type="text" value="<?php echo $posts_item['Po_Bn_Page'];?>" placeholder="Enter the URL for your action button" disabled="disabled">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="font-weight-bold">Keyword Tags</label>
						<div class="admin-form-group controls mb-0 pb-1">
							<input class="form-control" id="txtKeywords" name="txtKeywords" type="text" value="<?=$posts_item['Po_Keywords'];?>" disabled>
						</div>
					</div>

					<div class="control-group mb-4">
						<div class="admin-form-group controls mb-0 pb-1">
							<label class="font-weight-bold">Privacy</label>

							<!-- Default unchecked -->
							<div class="custom-control custom-radio pb-2">
								<input type="radio" class="custom-control-input" id="radPublic" name="radPrivacy" value="public"  disabled="disabled" <?php if (strcmp($posts_item['Po_Privacy'],'public')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPublic">Public <br><i class="text-muted">Anyone in your group can access and join the conversation.</i></label>
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2">
								<input type="radio" class="custom-control-input" id="radPrivate" name="radPrivacy" value="private" disabled="disabled" <?php if (strcmp($posts_item['Po_Privacy'],'private')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radPrivate">Private <br><i class="text-muted">Anyone in your group can find your post. But access to join the conversations, is limited to invited members and approved by admin.</i></label>
							</div>

							<!-- Default checked -->
							<div class="custom-control custom-radio pb-2">
								<input type="radio" class="custom-control-input" id="radSecret" name="radPrivacy" value="secret" disabled="disabled" <?php if (strcmp($posts_item['Po_Privacy'],'secret')==0) echo 'checked'; ?>>
								<label class="custom-control-label" for="radSecret">Secret <br><i class="text-muted">Only those with link and unique password in your group, can find and join your topic.</i></label>
							</div>

						</div>
					</div>
					<br>

				  <?php

				  if($this->session->userdata['logged_in']['bs_id'] == $posts_item['Po_Us_ID']  || $this->session->userdata['logged_in']['bs_id'] == $posts_item['Gr_Us_ID']  ) { ?>

					<div class="admin-form-group" style="display: flex;
    max-height: 45px;
    line-height: 0;
    
    white-space: nowrap;">
						<a href="<?php echo base_url(); ?>account/view/post/<?php echo $EncodedID;?>" class="btn  btn-light mr-2">Cancel</a>
						<a href="<?php echo base_url(); ?>account/postinfo/edit/<?php echo $EncodedID;?>" class="btn btn-primary mr-2" style="background: white;  color: #398cb3;">Edit Post Info</a>
						<a href="#" data-href="<?php echo base_url(); ?>account/postinfo/delete/<?php echo $EncodedID;?>" class="btn btn-danger"  data-toggle="modal" data-target="#confirm-delete">Delete Post</a>								
					</div>

					<?php }  ?>
				</div>

				<div id="post-images-container" class="nav-container">
					<div class="file-loading">
						<input id="input_file_images" name="file_images[]" type="file" multiple>
					</div>
				</div>
			</form>
			
				<div id="manage-user-container" class="nav-container" style="display:none;">

					<!-- here need to be shown the manages users-->
					 <div class="  user_role">
                    							<h6>
                    								Manage user roles
                    							</h6>
                    							<div class="search_member form-group">
                    								<input class="form-control" type="text" id="user_filter">
                    								<button class="btn btn-info" type="button" id="btnUserSearch">
                    									search
                    								</button>
                    							</div>
                    							<ul class="user_role_list">
                    								<li>
                    									<a disabled>Roles <i class="fas fa-filter"></i></a>
                    								</li>
                    								<li>
                    									<a class="role_option active" id="tab_all_members" role_tab_id="all_members">All</a>
                    								</li>
													<?php  if($posts_item['Po_Privacy'] == 'public'){ ?>
                    								 <li>
                    									<a class="role_option" id="tab_super_admin" data-type="post" role_tab_id="super_admin">Super Admin</a>
                    								</li>
 		                								<li>
                    									<a class="role_option" id="tab_admin" data-type="post" role_tab_id="admin">Admin</a>
                   										</li>
													<?php } ?>
                    								<li>
                    									<a class="role_option" id="tab_member" role_tab_id="member">Members</a>
                    								</li>
                    								<li>
                    									<a class="role_option" id="tab_follower" role_tab_id="follower">Followers</a>
                    								</li>
                    								<li>
                    									<a class="role_option" id="tab_pending" role_tab_id="pending">Join Requests / Invites</a>
                    								</li>
                    							</ul>
                    							<div class="scroll_body" style="    padding: 0px;">
													<script>
														function checkonlnie(mutetype){

															// var role_tab = $('.role_option').attr('role_tab_id');
															var role_tab = $('.role_option.active').attr('role_tab_id');
															var role_tab = $('.role_option.active').attr('role_tab_id');

															if (role_tab == "all_members") {
																UpdatePost.mutealluser( role_tab , mutetype);
															}else{
 																if (role_tab == "super_admin") {
																	UpdatePost.mutealluser( role_tab, mutetype);
																} else if (role_tab == "admin") {
																	UpdatePost.mutealluser( role_tab, mutetype);
																} else if (role_tab == "member") {
																	UpdatePost.mutealluser( role_tab, mutetype);
																} else if (role_tab == "follower") {

																} else {

																}
															}
														}
													</script>
													<div class="suggest_member_tabs">
														<div style="    width: 100%!important;  height: 48px;">
															<span type="button" onclick="checkonlnie(1)" style="  display:none; background: white;    color: #398cb3;     padding: 1px 6px;      font-size: 16px " class="btn btn-xl btn-primary mr-2" id="mutemember"   class="mutemember">Mute All</span>
															<span type="button" onclick="checkonlnie(0)" style=" display:none; background: white;    color: #398cb3;     padding: 1px 6px;      font-size: 16px " class="btn btn-xl btn-primary mr-2" id="unmutemember"   class="unmutemember">UnMute All</span>


														</div>
                    									<ul id="all_members" class="user_list suggest_members">


														</ul>
                    									<ul id="super_admin" class="user_list suggest_members"></ul>
                    									<ul id="admin" class="user_list suggest_members"></ul>
                    									<ul id="member" class="user_list suggest_members">

														</ul>
                    									<ul id="follower" class="user_list suggest_members"></ul>
                    									<ul id="pending" class="user_list suggest_members"></ul>
                    									<ul id="no_role" class="user_list suggest_members"></ul>
                    								</div>
                    							</div>
                    					</div>


				</div>
				<div id="post-videos-container" class="nav-container">videos
				</div>

				<div id="post-files-container" class="nav-container">

					<div class="file-loading">
						<input id="input_file_others" name="file_others[]" type="file" multiple>
					</div>
				</div>
	        </div>

		</section>
	</main><!-- /maincontent -->

<?php $this->load->view('account/user_role_template_post'); ?>
<style type="text/css">
	.bootstrap-tagsinput { 
	    border: none !important;
	    box-shadow: none !important;
	    background-color: transparent !important;
	    padding: 0 !important;
	}

	.bootstrap-tagsinput span[data-role="remove"] {
	    display:none !important;
}

.modal_item.user_role {
    margin-bottom: 47px;
}
div#manage-user-container {
    padding: 13px;
    margin: 40px;
}
</style>
