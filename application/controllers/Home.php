<?php
class Home extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			$this->load->model('users_model');
			$this->load->model('groups_model');
			$this->load->model('topics_model');
			$this->load->model('posts_model');
			$this->load->model('comments_model');
			$this->load->model('survey_model');
			$this->load->model('policy_model');
			$this->load->model('events_model');
			$this->load->model('tasks_model');
			$this->load->model('category_model');
			$this->load->model('settings_model');
			$this->load->model('notifications_model');
			$this->load->model('medias_model');
			$this->load->model('subcategory_model');
			$this->load->helper('url_helper');
			$this->load->helper(array('form', 'url'));

			// Load session library
			$this->load->library('session');
    }
    
    public function index() {
        if (!isset($this->session->userdata['logged_in'])){
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);

		$data['title'] = "Home Page";
		$data['posts_items'] = $this->posts_model->get_AllPosts();
		$data['private_items'] = $this->groups_model->get_GroupByType("private");
		$data['public_items'] = $this->groups_model->get_GroupByType("public");

		$data['privatetopics_items'] = $this->topics_model->get_TopicsByType("private");
		$data['publictopics_items'] = $this->topics_model->get_TopicsByType("public");
		$data['asset'] = 'home/index';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('home/index.php', $data);
		$this->load->view('templates/footer', $data);
    }
}