<?php

class Account extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->model('groups_model');
		$this->load->model('topics_model');
		$this->load->model('posts_model');
		$this->load->model('comments_model');
		$this->load->model('survey_model');
		$this->load->model('policy_model');
		$this->load->model('events_model');
		$this->load->model('tasks_model');
		$this->load->model('category_model');
		$this->load->model('settings_model');
		$this->load->model('notifications_model');
		$this->load->model('medias_model');
		$this->load->model('subcategory_model');
		$this->load->model('members_model');
		$this->load->model('follower_model');
		$this->load->model('keyword_model');
		$this->load->model('buttons_model');
		$this->load->helper('url_helper');
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');

		try{
			if(isset($this->session->userdata['logged_in']['bs_id'])){
				$this->LoggedInUser = $this->session->userdata['logged_in']['bs_id'];
			}

		}catch (Exception $e){

		}
	}

	public function index()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);

		$data['title'] = "Account Page";
		$data['posts_items'] = $this->posts_model->get_AllPosts();
		$data['private_items'] = $this->groups_model->get_GroupByType("private");
		$data['public_items'] = $this->groups_model->get_GroupByType("public");

		$data['privatetopics_items'] = $this->topics_model->get_TopicsByType("private");
		$data['publictopics_items'] = $this->topics_model->get_TopicsByType("public");

		$data['asset'] = 'account/account';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/index.php', $data);
		$this->load->view('templates/footer', $data);
	}

	public function join()
	{
		$this->view = false;

		$data = $this->input->post();

		$join = $this->members_model->join($data);

		echo json_encode($join);
		exit;
	}

	public function show_group_posts_topics($data)
	{
		$this->view = false;

		$post_data = $data;

		$id = null;
		$category_id = null;
		$search_text = null;
		$ctr = 0;
		$all = null;
		if (isset($post_data['ID'])) {
			$id = decode_id($post_data['ID']);
		}

		if (isset($post_data['category_id'])) {
			$category_id = $post_data['category_id'];
		}

		if (isset($post_data['search_text'])) {
			$search_text = $post_data['search_text'];
		}
		if (isset($post_data['subcat'])) {
			$subcat = $post_data['subcat'];
		}
		if(isset($post_data['all'])){

			$all = 'all';
		}

		$subcategory_items = $this->category_model->get_group_subcategories($category_id, $id);
		$group_topics = $this->groups_model->show_group_topics($id, $category_id, $search_text , $all ,$subcat);
		return $group_topics;
//                 return  array('data_items' => $group_topics, 'subcategory_items' => $subcategory_items);

	}

	public function groupall($option, $privacy = false, $group_id = '', $catID = '', $subCat = '')
	{


		$data['title'] = "Posts";
		$SearchText = $this->input->post('txtSearchPost');
		$data['SearchText'] = $SearchText;


		$datax['ID'] = $group_id;
		$datax['category_id'] = $catID;
		$datax['search_text'] = $SearchText;
		$datax['subcat'] = $subCat;
		$datax['popoular'] = 'no';
			$datax['all'] = 'yes';

		$data['posts_items'] = $this->show_group_posts_topics($datax);

		$data['asset'] = 'account/account';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('explore/grouppost', $data);
		$this->load->view('templates/footer', $data);
	}

	public function all($option, $privacy = false, $group_id = '', $catID = '', $subCat = '')
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);

		if (strcmp($option, 'groups') == 0) {
			$data['title'] = ucfirst($privacy . " Groups");
			$data['ParamType'] = $privacy;
			$SearchText = $this->input->post('txtSearchGroup');
			$data['SearchText'] = $SearchText;
			$data['groups_items'] = $this->groups_model->get_GroupByType($privacy, $SearchText);

		} else if (strcmp($option, 'topics') == 0) {
			$data['title'] = ucfirst($privacy . " Topics");
			$data['ParamType'] = $privacy;
			$SearchText = $this->input->post('txtSearchTopic');
			$data['SearchText'] = $SearchText;
			$data['topics_items'] = $this->topics_model->get_TopicsByType($privacy, $SearchText);
		} else if (strcmp($option, 'posts') == 0) {


			$data['title'] = "Posts";
			$SearchText = $this->input->post('txtSearchPost');
			$data['SearchText'] = $SearchText;
			//

			$datax['ID'] = $group_id;
			$datax['category_id'] = $catID;
			$datax['search_text'] = $SearchText;
			$datax['popoular'] = 'no';

			$data['posts_items'] = $this->posts_model->get_AllPosts($SearchText, $group_id, $catID, $subCat);
// 				$data['posts_items'] = $this->posts_model->searchPosts($SearchText);


		}


		$data['asset'] = 'account/account';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		if (strcmp($option, 'groups') == 0) $this->load->view('explore/groups', $data);
		else if (strcmp($option, 'topics') == 0) $this->load->view('explore/topics', $data);
		else if (strcmp($option, 'posts') == 0) $this->load->view('explore/posts', $data);
		$this->load->view('templates/footer', $data);
	}

	public function validation_for_group()
	{

		if ($this->session->userdata['logged_in']['bs_admin'] == 1) {
			echo json_encode(array());
			exit;
		}

		if ($this->session->userdata['logged_in']['bs_email'] == 'betauser6@gmail.com' ||
			$this->session->userdata['logged_in']['bs_email'] == 'betauser5@gmail.com' ||
			$this->session->userdata['logged_in']['bs_email'] == 'betauser4@gmail.com' ||
			$this->session->userdata['logged_in']['bs_email'] == 'betauser3@gmail.com' ||
			$this->session->userdata['logged_in']['bs_email'] == 'betauser2@gmail.com' ||
			$this->session->userdata['logged_in']['bs_email'] == 'betauser1@gmail.com'
		) {
			echo json_encode(array());
			exit;
		}


		$this->db->select('bs_groups.*');
		$this->db->from('bs_groups');
		$this->db->where('bs_groups.Gr_Us_ID', $this->LoggedInUser);
		$query = $this->db->get()->result_array();

		echo json_encode($query);

	}
		function 	checkUserGroupValidation(){

			$this->db->select('bs_groups.Gr_packegtype');
			$this->db->from('bs_groups');
			$this->db->where('bs_groups.Gr_Us_ID', $_POST['user_id']);
			$query = $this->db->get()->result_array();

			echo json_encode($query);
		}
	function check_file_exists_here($url)
	{
		$result = get_headers($url);
		return stripos($result[0], "200 OK") ? true : false; //check if $result[0] has 200 OK
	}

	public function basepassword($id)
	{
		echo encode_id($id);
		exit;
	}

	public function searchbyheader()
	{


		$search = $_POST['text_search'];
		$groups = $this->groups_model->searchbyheadergroup($search);
		$post = $this->groups_model->searchbyheaderpost($search);
		$topic = $this->groups_model->searchbyheadertopic($search);
		$profile = $this->groups_model->searchbyheaderprofile($search);
		$catogeryBlock = $this->groups_model->searchbyheadercatogry($search);

		foreach ($catogeryBlock as $key => $citem) {
			if ($citem['Ca_Type'] == 'group') {
				$this->db->select('bs_groups.*');
				$this->db->from('bs_groups');
				$this->db->where('Gr_Ca_ID', $citem['Ca_ID']);
				$catGroup = $this->db->get()->result_array();
				$groups = array_merge($groups, $catGroup);
			} else if ($citem['Ca_Type'] == 'topic') {
				$this->db->select('bs_topics.*');
				$this->db->from('bs_topics');
				$this->db->where('To_Ca_ID', $citem['Ca_ID']);
				$cattopic = $this->db->get()->result_array();
				$topic = array_merge($topic, $cattopic);
			} else if ($citem['Ca_Type'] == 'post') {
				$this->db->select('bs_posts.* , bs_groups.*');
				$this->db->from('bs_posts');

				$this->db->join('bs_groups' , 'bs_groups.Gr_ID  = bs_posts.Po_Gr_ID');
				$this->db->where('bs_posts.Po_Ca_ID', $citem['Ca_ID']);
				$this->db->where('bs_groups.Gr_Privacy', 'public');
				$catpost = $this->db->get()->result_array();
				$post = array_merge($post, $catpost);
			}
		}

		foreach ($groups as $keyx => $gitem) {


			if (empty($gitem['Gr_Thumb'])) {

				$groups[$keyx]['Gr_Thumb'] = 'img/banner/create-group.jpg';

			}


		}

		foreach ($post as $key => $item) {


			$post[$key]['encode'] = encode_id($item['Po_ID']);
			if (empty($item['Po_Thumb'])) {
				$post[$key]['Po_Thumb'] = 'img/banner/create-group.jpg';
			}
// 								else {
// 							if($this->check_file_exists_here(base_url().$item['Po_Thumb'])){
// 								}else{
//                 					$post[$key]['Po_Thumb'] = 'img/banner/create-post.jpg';
//
// 							}
// 					}

		}
		foreach ($profile as $key => $uitem) {

			$profile[$key]['encode'] = encode_id($uitem['Us_ID']);

			if (empty($uitem['Us_Thumb'])) {
				$profile[$key]['Us_Thumb'] = base_url() . 'img/home/profileimage.jpg';
			}
// 								else {
// 							if($this->check_file_exists_here($uitem['Us_Thumb'])){
// 								}else{
// 							 $profile[$key]['Us_Thumb'] = base_url().'img/home/profileimage.jpg';
//
// 							}
// 						}

		}
		foreach ($topic as $key => $titem) {

			$topic[$key]['encode'] = encode_id($titem['To_ID']);

			if (empty($titem['To_Thumb'])) {
				$topic[$key]['To_Thumb'] = 'img/banner/create-group.jpg';
			}
// 								else {
// 							if($this->check_file_exists_here(base_url().$titem['To_Thumb'])){
// 								}else{
// 									$topic[$key]['To_Thumb'] = 'img/banner/create-topic.jpg';
//
// 							}
// 						}

		}
		echo json_encode(array(
			'group' => $groups,
			'post' => $post,
			'topic' => $topic,
			'profile' => $profile
		));
	}

	function check_subcatogry_validation(){
		$this->db->select('bs_posts.*');
		$this->db->from('bs_posts');
		if(isset($_POST['TopicID']) && !empty($_POST['TopicID'])){
			$this->db->where('Po_To_ID' , $_POST['TopicID']);
		}else{
			$this->db->where('Po_Gr_ID' , $_POST['GroupID']);
		}

		$totalrecord = 	$this->db->get()->result_array();
		$total_list = array();
		foreach ($totalrecord as $singlepost){

			if($singlepost['Po_Sc_ID'] == $_POST['sub_cat_id']){
				echo 1;
				exit;
			}

		}

		echo 0;
	}

			function check_catogry_validation_topic(){
				$this->db->select('bs_topics.*');
				$this->db->from('bs_topics');
				$this->db->where('To_Gr_ID' , $_POST['GroupID']);
				$totalrecord = 	$this->db->get()->result_array();
				$total_list = array();
				foreach ($totalrecord as $singlepost){

					if($singlepost['To_Ca_ID'] == $_POST['cat_id']){
						echo 1;
						exit;
					}

				}

				echo 0;
			}

			function purcashingCoins(){

				$this->db->select('coin_packages.*');
				$this->db->from('coin_packages');
				$this->db->where('CP_ID' , $_POST['PackageId']);
				$data = $this->db->get()->result_array();
				if(count($data) > 0){


					$this->db->set('Us_Coins', 'Us_Coins + ' . (int) $data[0]['CP_Ammount'] , FALSE);
					$this->db->where('Us_ID' , $this->LoggedInUser);
					$ress = $this->db->update('bs_users');

//

				 	if($ress){
						$TransectionData = array(
							'CT_Transection_ID' => rand(10000000000,10000000000000),
							'CT_Package_Id' =>  $_POST['PackageId'] ,
							'CT_Status' => 1,
							'CT_US_ID' => $this->LoggedInUser
						);
						$this->db->insert('credit_transection' , $TransectionData );
						 echo "1";
					}else{
						$TransectionData = array(
							'CT_Transection_ID' => rand(10000000000,10000000000000),
							'CT_Package_Id' =>  $_POST['PackageId'] ,
							'CT_Status' => 0
						);
						$this->db->insert('credit_transection' , $TransectionData );
						 echo "0";
					}
				}else{
					echo '0';
				}

//				echo json_encode();
			}
			function discardpoolnote(){
				$update= array(
					'Poa_note' => ''
				);
				$this->db->where('Poa_id' , $_POST['pollnoteid']);

				$res = $this->db->update('bs_poll_ans' , $update);
				echo  $res;

			}
			function createcommentz(){


				$strin = '';
			 foreach ($_POST['commentuser'] as $dta){
				 		$strin .=   '@'.$dta.' ';


  				 }

				 $strin .= $_POST['Co_Message'];


				 $createData = array(
					"Co_Gr_ID" => $_POST['Co_Gr_ID'],
					"Co_To_ID" =>  0 ,
					"Co_St_ID" => 0,
					"Co_Po_ID"  => $_POST['Co_Po_ID'],
					"Co_Message" => $strin
				);


					$this->db->insert('bs_comments' , $createData);
					$lastid = 	$this->db->insert_id();

				foreach ($_POST['commentuser'] as $dta){

					$Namelink = '<a href="'.base_url().'u/'.encode_id($this->session->userdata['logged_in']['bs_id']).'">';
					$Namelink_end = '</a>';

					$postlink = '<a href="'.base_url().'account/view/post/'.encode_id($_POST['Co_Po_ID']).'">Please reply ';
					$post_link_end = '</a>';
					$notif_type = "tag_in_comment_post_notification";
//
//					[username]  [message] on the poll [poll title]. Please reply [here]
					$content = $Namelink. $this->session->userdata['logged_in']['bs_name'] . $Namelink_end.  " sent you message ". $_POST['Co_Message'] ." on the poll ." . $postlink . $_POST['PollTitle'] .$post_link_end;
					$this->notifications_model->add_notification($notif_type, ''.$this->session->userdata['logged_in']['bs_name'].' mention you in the Comment', $content,   $this->session->userdata['logged_in']['bs_id'] , 'user', $dta , $lastid);

					$this->comments_model->insert_comment_tag(array('Ta_Co_ID' =>$lastid, 'Ta_Us_ID' => $dta));
				}
				echo json_encode('success');
			}

			function gwtpoollquwstion(){

					$this->db->select('bs_poll_ans.*' );
					$this->db->from('bs_poll_ans');
					$this->db->where('Poa_id'  , $_POST['poais']);
					$res = $this->db->get()->result_array();
					echo json_encode($res);
			}
			function savepoolnote(){

		$update= array(
			'Poa_note' => $_POST['notetextarea']
		);
		$this->db->where('Poa_id' , $_POST['pollnoteid']);

		$res =		$this->db->update('bs_poll_ans' , $update);
		echo  $res;
			}
				function check_catogry_validation(){


						$this->db->select('bs_posts.*');
						$this->db->from('bs_posts');
					if(isset($_POST['TopicID']) && !empty($_POST['TopicID'])){
						$this->db->where('Po_To_ID' , $_POST['TopicID']);
					}else{
						$this->db->where('Po_Gr_ID' , $_POST['GroupID']);
					}

					$totalrecord = 	$this->db->get()->result_array();
						$total_list = array();
					foreach ($totalrecord as $singlepost){

					 	if($singlepost['Po_Ca_ID'] == $_POST['cat_id']){
								echo 1;
								exit;
						}

					}

					echo 0;

				}

	function changepostSLUG(){
		$this->db->select('bs_posts.*');

		$this->db->from('bs_posts');
		$data = 	$this->db->get()->result_array();
		foreach ($data as $key => $value){


			$topic_Slug = $this->slug($value['Po_Title'], '_').'_'.rand(0,9999);
			$where = array('Po_ID' => $value['Po_ID'] );
			$update = array('Po_Slug' => $topic_Slug);
			$this->db->where($where);
			$this->db->update('bs_posts' , $update);
		}
	}
				function changeTOpicSLUG(){
						$this->db->select('bs_topics.*');

						$this->db->from('bs_topics');
					$data = 	$this->db->get()->result_array();
							foreach ($data as $key => $value){


								$topic_Slug = $this->slug($value['To_Name'], '_').'_'.rand(0,9999);
								$where = array('To_ID' => $value['To_ID'] );
								$update = array('To_Slugs' => $topic_Slug);
								$this->db->where($where);
								$this->db->update('bs_topics' , $update);
							}
				}
	public function create($option = false, $id = NULL, $catId = "", $subcatId = "" , $rewardId = "")
	{




		if($catId == 'test'){
			$catId = '';
		}
		if($subcatId == 'test'){
			$subcatId = '';
		}
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$UserID = ($this->session->userdata['logged_in']['bs_id']);




		$data['error'] = '';
		$data['notification'] = '';
		$data['ID'] = $id;
		$data['catId'] = $catId;
		$data['subcatId'] = $subcatId;
		$data['nonetopic'] = $catId;
		$data['rewardId'] = $rewardId;

		if (strcmp($option, 'group') == 0) {
			$data['title'] = "Create Group";
			$data['createType'] = 'group';
			$data['category_items'] = $this->category_model->get_CategoryByType('group');


			$data['GroupName'] = $this->input->post('txtName');
		}
		else if (strcmp($option, 'savegroup') == 0) {


			$data['title'] = "Create Group";

			$data['category_items'] = $this->category_model->get_CategoryByType('group');
			$data['GroupName'] = $this->input->post('txtName');


// 				if($this->validation_for_group() > 0){
// 					$data['error'] = 'You have reached the FREE limit with maximum of one (1) group per user.';
//
//
// 				}else{


			if ($this->input->post('radPrivacy') != 'private' && $this->session->userdata['logged_in']['bs_admin'] != 1) {

							$data['error'] = 'You can only Select Private group!';
 						} else {

				$data = $this->savegroup($data);
				$data['EncodedID'] = encode_id($data['PrimaryID']);

			}

// 				}
			if (strlen($data['error']) == 0) {
				redirect('/account/view/group/' . $data['EncodedID']);
			}


		}
		else if (strcmp($option, 'topic') == 0) {
			$data['title'] = "Create SubGroup";
			$selectedgroup = '';
			if(isset($id)){
				$selectedgroup = 	decode_id($id);
			}
			$data['selecetd'] = $selectedgroup;
			$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);
			$data['category_items'] = $this->category_model->get_CategoryByType('topic');
			$data['TopicName'] = $this->input->post('txtName');
			$data['GroupID'] = $this->input->post('selGroup');
			$data['EncodedID'] = $id;
			$data['createType'] = 'topic';


 			$this->db->select('bs_topics.*');
			$this->db->from('bs_topics');
			$this->db->where('To_Gr_ID' , decode_id($id));
			$groups_post = $this->db->get()->result_array();
			$listofcat = array();
			$listofsubcat  = array();
			$privatecounter = 0;
			foreach ($groups_post as $singlgroup){

				if($singlgroup['To_Privacy'] == 'private' ){
					$privatecounter++;
				}

				if (in_array($singlgroup['To_Ca_ID'], $listofcat)){


				}else{
					array_push($listofcat, $singlgroup['To_Ca_ID']);

				}
				if (in_array($singlgroup['To_Sc_ID'], $listofsubcat)){


				}else{
					array_push($listofsubcat, $singlgroup['To_Sc_ID']);


				}

			}
			$data['Catlist'] = $listofcat ;
			$data['subCatlist'] =  $listofsubcat  ;

			$data['sub_catogery'] = $this->category_model->get_SubCategory($catId);


			$data['createType'] = 'post';
			$data['button_items'] = $this->buttons_model->get_record();
			$data['EncodedID'] = $id;


			$data['groups_data'] = $this->groups_model->get_Group( decode_id($id));

			$limitremain = 0;

			$this->db->select('bs_packages.*')->from('bs_packages');
			$this->db->where('Pk_ID' , $data['groups_data']['Gr_packegtype']);
			$packkage_details = $this->db->get()->result_array();
			if(isset($packkage_details[0]['Pk_ID']) && $packkage_details[0]['Pk_ID'] == 1){
				if($privatecounter == 0 ){
					$data['privatepostlimit'] = 1;
				}else{
					$data['privatepostlimit'] = 0;
				}

			}else{
				$data['privatepostlimit'] = 1;
			}
			if(isset($packkage_details[0]['Pk_categries'])){

				$previousdata  =  count($listofcat) + count($listofsubcat);


				if($previousdata  > 0){

					$limitremain =      (int)$packkage_details[0]['Pk_categries'] - (int)$previousdata;
				}else{
					$limitremain =      (int)$packkage_details[0]['Pk_categries'] ;
				}


			}



			if($limitremain <= 0){
				$limitremain = 0;
			}

			$data['cat_limit']= $limitremain;
			if (!empty($id)) {
				$id = decode_id($id);
				$data['group_details'] = $this->groups_model->get_Group($id);
			}


		}
		else if (strcmp($option, 'savetopic') == 0) {


			$data['title'] = "Create SubGroup";

			$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);
			$data['category_items'] = $this->category_model->get_CategoryByType('topic');
			$data['TopicName'] = $this->input->post('txtName');
			$data['GroupID'] = $this->input->post('selGroup');
			$topic_Slug = $this->slug($_POST['txtTitle'], '_').'_'.rand(0,9999);
			$data['topic_slug'] = $topic_Slug;
			$data = $this->savetopic($data);
			$data['EncodedID'] = encode_id($data['PrimaryID']);



			$this->load->helper('url');
			$this->load->helper('date'); // load Helper for Date
			date_default_timezone_set("Asia/Manila");
			$now = date('Y-m-d H:i:s');
			$dataz = array(
				'Me_Parent' => "topic",
				'Me_Gr_ID' =>  $data['GroupID'],
				'Me_To_ID' => $data['PrimaryID'],
				'Me_St_ID' => 0,
				'Me_Us_ID' =>  $this->LoggedInUser,
				'Me_Role' => "superadmin",
				'Me_Status' => "active",
				'Me_DatePOsted' => $now
			);

			$this->db->insert('bs_members', $dataz);

			if (strlen($data['error']) == 0) {
				redirect('/account/view/topic/' . $data['EncodedID']);
			}

		}
		else if (strcmp($option, 'topicpost') == 0) {




				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
 				$this->db->where('Me_Parent' , 'topic');
			    $this->db->where('Me_Us_ID' , $UserID);
				$this->db->where('Me_To_ID' , decode_id($id));
			$total_subgroups_members = 	$this->db->get()->result_array();

			$data['title'] = "Make a Post";
			$data['category_items'] = $this->category_model->get_CategoryByType('topic');
			$data['categories'] = $this->category_model->get_Categories('topic');
			$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);


			$data['sub_catogery'] = $this->category_model->get_SubCategory($catId);
			$data['createType'] = 'post';
			$data['button_items'] = $this->buttons_model->get_record();
			$data['EncodedID'] = $id;



			$this->db->select('bs_posts.*');
			$this->db->from('bs_posts');
			$this->db->where('Po_To_ID' , decode_id($id));
			$groups_post = $this->db->get()->result_array();


			$listofcat = array();
			$listofsubcat  = array();
			$privatecounter = 0;
			foreach ($groups_post as $singlgroup){

				if($singlgroup['Po_Privacy'] == 'private' ){
					$privatecounter++;
				}

				if (in_array($singlgroup['Po_Ca_ID'], $listofcat)){


				}else{
					array_push($listofcat, $singlgroup['Po_Ca_ID']);

				}
				if (in_array($singlgroup['Po_Sc_ID'], $listofsubcat)){


				}else{
					array_push($listofsubcat, $singlgroup['Po_Sc_ID']);


				}

			}

			$data['Catlist'] = $listofcat ;
			$data['subCatlist'] =  $listofsubcat  ;

			$limitremain = 0;
 			$previousdata  =  count($listofcat) + count($listofsubcat);

				if($previousdata  > 0){

					$limitremain =     6 - (int)$previousdata;
				}else{
					$limitremain =     6 ;
				}
 			$data['privatepostlimit'] = 1;
			if($limitremain <= 0){
				$limitremain = 0;
			}

			$data['cat_limit']= $limitremain;
			if (!empty($id)) {
				$id = decode_id($id);
			}

			$data['topic_details'] = $this->topics_model->get_Topic($id);

//			print_r($data['topic_details'] );
//			exit;
			$data['asset'] = 'account/create/post';
			$option = 'post';
 			}
		else if (strcmp($option, 'post') == 0) {



			if(!empty($rewardId)){
				$this->db->select('bs_voucher_post.*');
				$this->db->from('bs_voucher_post');
				$this->db->where('Vp_id' , $rewardId );
			$res = 	$this->db->get()->result_array();
			$data['rewarddata'] = $res;


			}

//
			if (empty($id)) {
				$data['title'] = "Make a Post";

				$data['category_items'] = $this->category_model->get_CategoryByType('post');

				$data['categories'] = $this->category_model->get_Categories('post');
				$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);
				$data['button_items'] = $this->buttons_model->get_record();
				$data['EncodedID'] = $id;

				if (!empty($id)) {
					$id = decode_id($id);
				}

				if (strcasecmp($catId, "group") == 0) {
					$data['topic_details'] = array('To_Gr_ID' => $id, 'To_ID' => 0);
					$data['catId'] = $subcatId;
				} else {
					$data['topic_details'] = $this->topics_model->get_Topic($id);
				}
				$data['asset'] = 'account/create/post';
			}
			else{
            	$data['title'] = "Make a Post";
				$data['category_items'] = $this->category_model->get_CategoryByType('topic');



				$this->db->select('bs_posts.Po_Ca_ID , bs_posts.Po_Ca_Name  ');
				$this->db->from('bs_posts');
				$this->db->distinct();
				$this->db->where('Po_Gr_ID' , decode_id($id) );
				$listofcae= 	$this->db->get()->result_array();


				for($x = 0 ; $x < count($listofcae) ; $x++ ) {

					if ($listofcae[$x]['Po_Ca_ID'] == 0) {
						$datax = array(
							"Ca_ID" => 0,
							"Ca_Type" => "topic",
							"Ca_Name" => $listofcae[$x]['Po_Ca_Name'],
							"Ca_Important" => '*',
							"Ca_DatePosted" => '',
							"Ca_Default" => 0,
						);
						array_unshift($data['category_items'], $datax);
					} else {
						for ($y = 0; $y < count($data['category_items']); $y++) {
							if ($listofcae[$x]['Po_Ca_ID'] == $data['category_items'][$y]['Ca_ID']) {

								$datax = array(
									"Ca_ID" => $data['category_items'][$y]['Ca_ID'],
									"Ca_Type" => $data['category_items'][$y]['Ca_Type'],
									"Ca_Name" => $data['category_items'][$y]['Ca_Name'],
									"Ca_Important" => '*',
									"Ca_DatePosted" => $data['category_items'][$y]['Ca_DatePosted'],
									"Ca_Default" => $data['category_items'][$y]['Ca_Default'],
								);
								unset($data['category_items'][$y]);
								array_unshift($data['category_items'], $datax);
							}
						}
//
//
//
					}
				}
//				print_r($data['category_items']);
//				exit;
				$data['categories'] = $this->category_model->get_Categories('topic');




				$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);

				$this->db->select('bs_posts.*');
				$this->db->from('bs_posts');
				$this->db->where('Po_Gr_ID' , decode_id($id));
				$groups_post = $this->db->get()->result_array();
				$listofcat = array();
				$listofsubcat  = array();
				$privatecounter = 0;
				foreach ($groups_post as $singlgroup){

					if($singlgroup['Po_Privacy'] == 'private' ){
						$privatecounter++;
					}

					if (in_array($singlgroup['Po_Ca_ID'], $listofcat)){


					}else{
						array_push($listofcat, $singlgroup['Po_Ca_ID']);

					}
					if (in_array($singlgroup['Po_Sc_ID'], $listofsubcat)){


					}else{
						array_push($listofsubcat, $singlgroup['Po_Sc_ID']);


					}

				}

				$data['Catlist'] = $listofcat ;
				$data['subCatlist'] =  $listofsubcat  ;

				$catId =  str_replace('%20', ' ', $catId);


				if(is_numeric($catId) || empty($catId) ){
					$data['sub_catogery'] = $this->category_model->get_SubCategory($catId);

				}else if(is_string($catId)){

					$this->db->select('bs_posts.Po_Sc_Name as Sc_ID , bs_posts.Po_Sc_Name as Sc_Name');
					$this->db->from('bs_posts');
					$this->db->where('Po_Ca_Name' , $catId);
					$data['sub_catogery'] = $this->db->get()->result_array();

				}

				$data['createType'] = 'post';
				$data['button_items'] = $this->buttons_model->get_record();
				$data['EncodedID'] = $id;

				if (!empty($id)) {
					$id = decode_id($id);
				}
				$data['groups_data'] = $this->groups_model->get_Group($id);

				$limitremain = 0;

				$this->db->select('bs_packages.*')->from('bs_packages');
				$this->db->where('Pk_ID' , $data['groups_data']['Gr_packegtype']);
				$packkage_details = $this->db->get()->result_array();
				if(isset($packkage_details[0]['Pk_ID']) && $packkage_details[0]['Pk_ID'] == 1){
					if($privatecounter == 0 ){
						$data['privatepostlimit'] = 1;
					}else{
						$data['privatepostlimit'] = 0;
					}

				}else{
					$data['privatepostlimit'] = 1;
				}
				if(isset($packkage_details[0]['Pk_categries'])){

					$previousdata  =  count($listofcat) + count($listofsubcat);
 					if($previousdata  > 0){
 						$limitremain =      (int)$packkage_details[0]['Pk_categries'] - (int)$previousdata;
					}else{
						$limitremain =      (int)$packkage_details[0]['Pk_categries'] ;
					}


				}
 				if($limitremain <= 0){
					$limitremain = 0;
				}

				$data['cat_limit']= $limitremain;

				$data['group_id_for_post'] = $id;
				$data['topic_details'] = array('To_Gr_ID' => $id, 'To_ID' => 0);

				$data['catId'] = $catId;
				$data['groups_data'] = $this->groups_model->get_Group($id);


				$data['asset'] = 'account/create/post';
			}

		}
		else if (strcmp($option, 'savepost') == 0) {



			$data['title'] = "Make a Post";

			$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);
			$data['category_items'] = $this->category_model->get_CategoryByType('post');
			$data['categories'] = $this->category_model->get_Categories('post');

			$data = $this->savepost($data);
			$data['EncodedID'] = encode_id($data['PrimaryID']);
			if (strlen($data['error']) == 0) {
				redirect('/account/view/post/' . $data['EncodedID']);
			}


		}

		if (strpos($option, 'save') === false) {
			$data['asset'] = 'account/create/' . $option;
		}

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		if ((strcmp($option, 'group') == 0) || (strcmp($option, 'savegroup') == 0)) $this->load->view('account/creategroup', $data);
		else if ((strcmp($option, 'topic') == 0) || (strcmp($option, 'savetopic') == 0)) $this->load->view('account/createtopic', $data);
		else if ((strcmp($option, 'post') == 0) || (strcmp($option, 'savepost') == 0)) $this->load->view('account/createpost', $data);
		$this->load->view('templates/footer', $data);
	}

			function createvoucher(){

 					$this->db->insert('bs_voucher_post' , $_POST);
						 echo $this->db->insert_id();

			}
	public function savegroup($data)
	{

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtTitle', 'Title', 'required');


		if ($this->form_validation->run() === FALSE) {
			$data['error'] = 'Please fill up the required entry!';
		} else {

			$UserID = $data['CurrUserID'];
			$selCategory = $this->input->post('selCategory');
			$txtNewCategory = $this->input->post('txtNewCategory');
			if ($txtNewCategory == null) $txtNewCategory = '';
			$selSubCategory = $this->input->post('selSubCategory');
			$txtNewSubCategory = $this->input->post('txtNewSubCategory');
			if ($txtNewSubCategory == null) $txtNewSubCategory = '';
			$txtTitle = $this->input->post('txtTitle');
			$txtDescription = $this->input->post('txtDescription');
			$radPrivacy = $this->input->post('radPrivacy');
			$keywords = $this->input->post('txtKeywords');
			$checkPackage = $this->input->post('checkPackage');

			if($checkPackage == ''){
				$checkPackage = 0;
			}
			$data['PrimaryID'] = $this->groups_model->Insert($UserID, $txtTitle, $txtDescription, $selCategory, $txtNewCategory, $selSubCategory, $txtNewSubCategory, $radPrivacy, $keywords,$checkPackage);
			$data = $this->do_UploadPhoto($UserID, $data['PrimaryID'], 'group', $data);
			$this->check_and_save_new_keywords($keywords);
			//$this->users_model->add_Admin('group', $GroupID, 0, 0, $UserID);
		}

		return $data;
	}

	function slug($string, $spaceRepl = "-")
	{
		$string = str_replace("&", "and", $string);

		$string = preg_replace("/[^a-zA-Z0-9 _-]/", "", $string);

		$string = strtolower($string);

		$string = preg_replace("/[ ]+/", " ", $string);

		$string = str_replace(" ", $spaceRepl, $string);

		return $string;
	}

	public function savetopic($data)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtTitle', 'Title', 'required');
		$this->form_validation->set_rules('selGroup', 'Group', 'greater_than[0]');

		if ($this->form_validation->run() === FALSE) {
			$data['error'] = 'Please fill up the required entry!';
		}
		else
		{


			$UserID = $data['CurrUserID'];
			$selGroup = $this->input->post('selGroup');
			$selCategory = $this->input->post('selCategory');
			$txtNewCategory = $this->input->post('txtNewCategory');
			if ($txtNewCategory == null) $txtNewCategory = '';
			$selSubCategory = $this->input->post('selSubCategory');
			$txtNewSubCategory = $this->input->post('txtNewSubCategory');
			if ($txtNewSubCategory == null) $txtNewSubCategory = '';
			$txtTitle = $this->input->post('txtTitle');
			$txtDescription = $this->input->post('txtDescription');
			$radPrivacy = $this->input->post('radPrivacy');
			$keywords = $this->input->post('txtKeywords');
			$topic_slug = $data['topic_slug'];

			$data['PrimaryID'] = $this->topics_model->Insert($UserID, $selGroup, $txtTitle, $txtDescription, $selCategory, $txtNewCategory, $selSubCategory, $txtNewSubCategory, $radPrivacy, $keywords , $topic_slug);
			$data = $this->do_UploadPhoto($UserID, $data['PrimaryID'], 'topic', $data);
			$this->check_and_save_new_keywords($keywords);
			//$this->users_model->add_Admin('topic', $GroupID, $TopicID, 0, $UserID);
		}
		return $data;
	}

	public function savepost($data)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtTitle', 'Title', 'required');
		$this->form_validation->set_rules('selGroup', 'Group', 'greater_than[0]');

		if ($this->form_validation->run() === FALSE) {
			$data['error'] = 'Please fill up the required entry!';

		} else {


			$UserID = $data['CurrUserID'];
			$selGroup = $this->input->post('selGroup');
			$selTopic = $this->input->post('selTopic');
			$selCategory = $this->input->post('selCategory');
			$txtNewCategory = $this->input->post('txtNewCategory');
			if ($txtNewCategory == null) $txtNewCategory = '';
			$selSubCategory = $this->input->post('selSubCategory');
			$txtNewSubCategory = $this->input->post('txtNewSubCategory');
			if ($txtNewSubCategory == null) $txtNewSubCategory = '';
			$otherTxtNewSubCategory = $this->input->post('otherTxtNewSubCategory');
			if ($otherTxtNewSubCategory == null) $otherTxtNewSubCategory = '';
			$txtTitle = $this->input->post('txtTitle');
			$post_Slug = $this->slug($this->input->post('txtTitle'), '_').'_'.rand(0,9999);

			$txtDescription = $this->input->post('txtDescription');
			$txtContent = $this->input->post('txtContent');
			$radPrivacy = $this->input->post('radPrivacy');
			$keywords = $this->input->post('txtKeywords');
			$actionButton = $this->input->post('txtKeywords');
			$keywords = $this->input->post('txtKeywords');
			$actionButton = $this->input->post('selActionButton');
			$landingPage = $this->input->post('txtLandingPage');
				$rewardid = $this->input->post('rewardid');
			$otherNewActionButton = ($this->input->post('txtActionButton') != null) ? trim($this->input->post('txtActionButton')) : '';


			$this->db->select('bs_members.* , bs_groups.* ');
			$this->db->from('bs_members');
			$this->db->join('bs_groups', 'bs_groups.Gr_ID = bs_members.Me_Gr_ID', 'left');
			$this->db->where('bs_members.Me_Gr_ID', $selGroup);
			$this->db->where('bs_members.Me_Role !=', 'superadmin');
			$this->db->where('bs_members.Me_Role !=', 'admin');
			$this->db->where('bs_members.Me_post_Approved', 1);

			$memeberLIst = $this->db->get()->result_array();


			$formData = array(
				'Po_Us_ID' => $UserID,
				'Po_Gr_ID' => $selGroup,
				'Po_To_ID' => $selTopic == '-1' ? 0 : $selTopic,
				'Po_Title' => $txtTitle,
				'Po_Slug' => $post_Slug,
				'Po_Description' => $txtDescription,
				'Po_Content' => $txtContent,
				'Po_Ca_ID' => $selCategory,
				'Po_Ca_Name' => $txtNewCategory,
				'Po_Sc_ID' => $selSubCategory,
				'Po_Sc_Name' => $txtNewSubCategory,
				'Po_Privacy' => $radPrivacy,
				'Po_Survey' => 0,
				'Po_SurveyStatus' => $otherTxtNewSubCategory,
				'Po_DatePosted' => date('Y-m-d H:i:s'),
				'Po_Keywords' => $keywords,
				'Po_Bn_ID' => $actionButton,
				'Po_Bn_Page' => $landingPage,
				'Po_Bn_Name' => $otherNewActionButton,
				'Po_reward' =>  $rewardid
			);



			$data['PrimaryID'] = $this->posts_model->Insert($formData);
			if(isset($rewardid)){
					$where = array(
							'Vp_id' => $rewardid
					);
				$update = array(
					'Vp_post_id' => $data['PrimaryID'],
					'Vp_Group_id' => $selGroup
				);
				$this->db->where($where);
				$this->db->update('bs_voucher_post', $update );

			}

			if ($data['PrimaryID']) {

				foreach ($memeberLIst as $memberapro) {
					$postlink = '<a href="' . base_url() . '/account/view/post/' . encode_id($data['PrimaryID']) . '">';
					$grouplink = '<a href="' . base_url() . '/account/view/group/' . encode_id($selGroup) . '">';
					$post_link_end = '</a>';
					$notif_type = "followed_post_notification";
					$content = "Posted " . $postlink . $txtTitle . $post_link_end . " in " . $grouplink . $memberapro['Gr_Name'] . $post_link_end;

					$this->notifications_model->add_notification($notif_type, 'A new post create in ' . $memberapro['Gr_Name'] . ' Group', $content, $UserID, 'group', $memberapro['Me_Us_ID'], 'user');
				}
			}
			$this->check_and_save_new_keywords($keywords);

			if ($data['PrimaryID']) {
				$valueP = $data['PrimaryID'];
				if (isset($_FILES['files'])) {
					$FileFolder = '/posts/' . $valueP;
					$uploadDirFile = './uploads/' . $UserID . $FileFolder;
					if (!is_dir($uploadDirFile)) {
						mkdir($uploadDirFile, 0777, TRUE);
					}

					$filePath = base_url() . '/uploads/' . $UserID . $FileFolder . '/';

					$this->upload_media_file($uploadDirFile, $_FILES['files'], $filePath, $valueP);
				}

				if (isset($_FILES['images'])) {
					$ImgFolder = '/posts/' . $valueP;
					$uploadDirImg = './uploads/' . $UserID . $ImgFolder;
					if (!is_dir($uploadDirImg)) {
						mkdir($uploadDirImg, 0777, TRUE);
					}

					$PhotoImg = base_url() . '/uploads/' . $UserID . $ImgFolder . '/';

					$this->upload_media_image($uploadDirImg, $_FILES['images'], $PhotoImg, $valueP);
				}

				$data = $this->do_UploadPhoto($UserID, $valueP, 'post', $data);
			}


		}

		return $data;

	}

	private function upload_media_image($path, $files, $link, $id)
	{
		$config = array(
			'upload_path' => $path,
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => 1,
		);
		$this->load->library('upload', $config);
		foreach ($files['name'] as $key => $image) {
			$_FILES['images[]']['name'] = $files['name'][$key];
			$_FILES['images[]']['type'] = $files['type'][$key];
			$_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
			$_FILES['images[]']['error'] = $files['error'][$key];
			$_FILES['images[]']['size'] = $files['size'][$key];
			$file_ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
			$filename = $key . time() . date('ymd') . $id . '.' . $file_ext;
			$config['file_name'] = $filename;
			$this->upload->initialize($config);
			if ($this->upload->do_upload('images[]')) {
				$this->upload->data();
				$PhotoImgLink = $link . $filename;
				$dataArr = [];
				$dataArr['type'] = 'Post';
				$dataArr['parentId'] = $id;
				$dataArr['filetype'] = 'image';
				$dataArr['path'] = $PhotoImgLink;
				$dataArr['filename'] = $files['name'][$key];
				$this->medias_model->insert($dataArr);
			} else {
				return false;
			}
		}
		return $images;
	}

	private function upload_media_file($path, $files, $link, $id)
	{
		$config = array(
			'upload_path' => $path,
			'allowed_types' => '*',
			'overwrite' => 1,
		);
		$this->load->library('upload', $config);
		foreach ($files['name'] as $key => $image) {
			$_FILES['files[]']['name'] = $files['name'][$key];
			$_FILES['files[]']['type'] = $files['type'][$key];
			$_FILES['files[]']['tmp_name'] = $files['tmp_name'][$key];
			$_FILES['files[]']['error'] = $files['error'][$key];
			$_FILES['files[]']['size'] = $files['size'][$key];
			$file_ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
			$filename = $key . time() . date('ymd') . $id . '.' . $file_ext;
			$config['file_name'] = $filename;
			$this->upload->initialize($config);
			if ($this->upload->do_upload('files[]')) {
				$this->upload->data();
				$FileLink = $link . $filename;
				$fileDataArr = [];
				$fileDataArr['type'] = 'Post';
				$fileDataArr['parentId'] = $id;
				$fileDataArr['filetype'] = 'file';
				$fileDataArr['path'] = $FileLink;
				$fileDataArr['filename'] = $files['name'][$key];
				$this->medias_model->insert($fileDataArr);
			} else {
				return false;
			}
		}
		return $files;
	}

	public function do_UploadPhoto($UserID, $ParentID, $Option, $data)
	{
		$txtbg = $this->input->post('txtbg');
		if (strlen($txtbg) == 0) {
			$Folder = '';

			if (strcmp($Option, 'group') == 0) {
				$Folder = '/groups/' . $ParentID;
			} else if (strcmp($Option, 'topic') == 0) {
				$Folder = '/topics/' . $ParentID;
			} else if (strcmp($Option, 'userphoto') == 0) {
				$Folder = '';
			} else if (strcmp($Option, 'userbackground') == 0) {
				$Folder = '';
			} else {
				$Folder = '/posts/' . $ParentID;
			}

			$Path = 'uploads/' . $UserID . $Folder;

			if (strcmp($Option, 'userphoto') != 0 && strcmp($Option, 'userbackground') != 0) {
				$UserFile = $_POST['userfile'];

				if (isset($UserFile) && !empty($UserFile)) {
					$image_array_1 = explode(";", $UserFile);
					$image_array_2 = explode(",", $image_array_1[1]);
					$UserFile = base64_decode($image_array_2[1]);
					$FileName = "uploads/background/" . $UserID . "_" . $ParentID . "_" . time() . '.png';

					if (!file_exists($Path)) {
						mkdir($Path, '0777');
					}

					if (strcmp($Option, 'group') == 0) {
						$this->groups_model->set_changephoto($ParentID, $FileName, $FileName, $FileName);

					} else if (strcmp($Option, 'topic') == 0) {
						$this->topics_model->set_changephoto($ParentID, $FileName, $FileName, $FileName);

					} else {
						$this->posts_model->set_changephoto($ParentID, $FileName, $FileName, $FileName);

					}

					file_put_contents("./" . $FileName, $UserFile);
					$Error = $FileName;
				}
			} else {
				$Error = $this->uploadphoto($Path, 'userfile');
			}

			$ErrorMsg = substr($Error, 0, 6);
			if (strcmp('Error:', $ErrorMsg) == 0) {
				if (strcmp($Error, 'Error: Empty') != 0) {
					$data['error'] = $Error;
				}
			} else {

				$FileName = $Error;
				$Photo = $this->getPhotoPath($UserID, $Folder, $FileName);
				$Thumb = $this->getPhotoPath($UserID, $Folder . '/thumbnail', $FileName);

				$this->resizeImage($UserID, $Folder, $FileName);


				if (strcmp($Option, 'userphoto') == 0) {
					$this->users_model->set_UpdatePhoto($UserID, $Photo, $Thumb);
				} else if (strcmp($Option, 'userbackground') == 0) {
					$this->users_model->set_UpdateBackground($UserID, $Photo);
				}


			}
		} else {
			$Setting = $this->settings_model->get_Setting($txtbg);

			if (strcmp($Setting['Se_Type'], 'backcolor') == 0) {
				if (strcmp($Option, 'group') == 0) {
					$this->groups_model->set_UpdateBackcolor($GroupID, $Setting['Se_Value']);
				} else if (strcmp($Option, 'topic') == 0) {
					$this->topics_model->set_UpdateBackcolor($TopicID, $Setting['Se_Value']);
				} else if (strcmp($Option, 'post') == 0) {
					$this->posts_model->set_UpdateBackcolor($PostID, $Setting['Se_Value']);
				}
			} else {
				if (strcmp($Option, 'group') == 0) {
					$this->groups_model->set_changephoto($UserID, $GroupID, $Setting['Se_Value'], $Setting['Se_Photo'], $Setting['Se_Thumb']);
				} else if (strcmp($Option, 'topic') == 0) {
					$this->topics_model->set_changephoto($TopicID, $Setting['Se_Value'], $Setting['Se_Photo'], $Setting['Se_Thumb']);
				} else if (strcmp($Option, 'post') == 0) {
					$this->posts_model->set_changephoto($PostID, $Setting['Se_Value'], $Setting['Se_Photo'], $Setting['Se_Thumb']);
				}
			}


		}

		return $data;
	}

	public function followed()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Account Page";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['posts_items'] = $this->posts_model->get_FollowedPosts($UserID);
		$data['groups_items'] = $this->groups_model->get_FollowedGroups($UserID);
		$data['topics_items'] = $this->topics_model->get_FollowedTopics($UserID);

		$this->load->view('templates/account_header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/followed.php', $data);
		$this->load->view('templates/account_footer', $data);
	}


	public function profile($option = null, $open = null)
	{


		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['error'] = "";
		$data['errorProfile'] = "";
		$data['errorPassword'] = "";
		$data['errorPhoto'] = "";
		$data['errorBackground'] = "";

		$data['Name'] = "";
		$data['title'] = "";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$bs_id = ($this->session->userdata['logged_in']['bs_id']);
		$data['openTab'] = 0;
		if ($option == 'open') {
			$data['openTab'] = 1;
		} else if (strlen($option) == 0) $option = 'groups';
		$data['selMenu'] = $option;

		$data['users_item'] = $this->users_model->get_Users($bs_id);
		if (empty($data['users_item'])) {
			show_404();
		}


		$data['create_group_btn_link'] = ($this->session->userdata['logged_in']['bs_group']) ? base_url() . 'account/create/group' : 'javascript:void(0)';
		$data['create_group_btn_class'] = ($this->session->userdata['logged_in']['bs_group']) ? '' : 'create_group_btn';

		$data['groups_items'] = $this->groups_model->get_UserGroups($data['CurrUserID']);
		$data['topics_items'] = $this->topics_model->get_UserTopics($data['CurrUserID']);
		$data['posts_items'] = $this->posts_model->get_UserPosts($data['CurrUserID']);
		$data['asset'] = 'account/profile';

		$data['contect_counter'] = $this->groups_model->get_usetContect($data['CurrUserID']);
		$data['comment_todo_notification'] = $this->notifications_model->comment_todo_notification($this->session->userdata['logged_in']['bs_id']);

//		echo '<pre>';
//		print_R($data);
//		exit;
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/profile', $data);
		$this->load->view('templates/footer', $data);

	}

	public	function hideactivtiyNotify(){
			$update = array(
				'No_Status' => 'seen'
			);

		    $this->db->where('No_To' , $this->LoggedInUser);
			$this->db->where('No_Status !=' , 'seen');
 			$this->db->where('No_Type' , 'creator_role_request');
		 	$this->db->or_where('No_Type' , 'creator_role_invite');
			$this->db->or_where('No_Type' , 'approve_request');
			$this->db->or_where('No_Type' , 'reject_request');
			$this->db->or_where('No_Type' , 'member_out');
			$this->db->or_where('No_Type' , 'assign_role');
			$this->db->or_where('No_Type' , 'invite_accepted');
			$this->db->or_where('No_Type' , 'followed_post_notification');
			$this->db->or_where('No_Type' , 'invite_rejected');
			$this->db->or_where('No_Type' , 'creator_role_invite_reject_me');
			$this->db->or_where('No_Type' , 'creator_role_invite_reject');
			$this->db->or_where('No_Type' , 'creator_role_invite_accept');
			$this->db->or_where('No_Type' , 'creator_role_invite_accept_me');

 			$this->db->update('bs_notification' , $update);
			 echo '1';
		}
	public function totalNotification()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$UserID = ($this->session->userdata['logged_in']['bs_id']);


		$data['notifications_items'] = $this->notifications_model->counterUserNotification($UserID);



		$data['userACtivity'] = $this->notifications_model->get_Useractivity_new($UserID);
		$data['post_notification'] = $this->notifications_model->comment_post_notification($UserID);
		$data['mention_notificaion'] = $this->notifications_model->comment_menion_notification($UserID);


		$data['todoComment'] = $this->notifications_model->get_user_todo_Notification($UserID);


		$counterdata = array(
			'inviteCouunter' => count($data['notifications_items']),
			'userACtivity' => count($data['userACtivity']),
			'post_notification' => count($data['post_notification']),
			'mention_notificaion' => count($data['mention_notificaion']),
			'todoComment' => $data['todoComment']
		);


		echo json_encode($counterdata);
	}

	public function getSaveQuestionwithEncode()
	{
		$resp = $this->groups_model->getSaveQuestionwithEncode($_POST);

		echo json_encode($resp);
	}

	public function deleteQuestion()
	{


		$resp = $this->groups_model->deleteQuestion($_POST);

		if ($resp) {
			echo json_encode('success');
		} else {
			echo json_encode('error');
		}

	}

	public function SaveAnswerofGroupWithEncode()
	{
		$resp = $this->groups_model->SaveAnswerofGroupWithEncode($_POST);

		if ($resp) {
			echo json_encode('success');
		} else {
			echo json_encode('error');
		}

	}

	public function SaveAnswerofGroup()
	{
		$resp = $this->groups_model->SaveAnswerofGroup($_POST);

		if ($resp) {
			echo json_encode('success');
		} else {
			echo json_encode('error');
		}

	}

	public function getSaveQuestion()
	{


		$resp = $this->groups_model->getSaveQuestion($_POST);

		echo json_encode($resp);
	}

	public function saveQuestion()
	{


		$resp = $this->groups_model->saveQuestion($_POST);

		if ($resp) {
			echo json_encode('success');
		} else {
			echo json_encode('error');
		}


	}

	public function GetAnswerOfUserwithOUt()
	{
		$resp = $this->groups_model->GetAnswerOfUserwithOUt($_POST);
		echo json_encode($resp);

	}

	public function GetAnswerOfUser()
	{


		$resp = $this->groups_model->GetAnswerOfUser($_POST);
		echo json_encode($resp);
	}

	public function updateQuestion()
	{
		$resp = $this->groups_model->updateQuestion($_POST);

		if ($resp) {
			echo json_encode('success');
		} else {
			echo json_encode('error');
		}

	}

	public function addNoteForNotification()
	{
		$res = $this->notifications_model->addNoteForNotification();

		if ($res) {

			$data = array(
				'message' => 'Sccessfully note added'
			);
		} else {
			$data = array(
				'message' => 'Something Went Wrong'
			);
		}

		echo json_encode($data);
	}

	public function DoneTodo()
	{


		$res = $this->notifications_model->DoneTodo();


		if ($res) {


			if ($_POST['type'] == 'read') {
				$data = array(
					'message' => 'This notification is remove Successfully'
				);

			} else {

				if ($_POST['cType'] == 'todo') {

					if ($_POST['type'] == 'done') {
						$data = array(
							'message' => 'This To Do List is marked as DONE.'
						);
					} else {
						$data = array(
							'message' => 'This To Do List is marked as UNDONE.'
						);
					}
				} else {

					if ($_POST['type'] == 'done') {
						$data = array(
							'message' => 'This To Do List is marked as READ.'
						);
					} else {
						$data = array(
							'message' => 'This To Do List is marked as UNREAD.'
						);
					}
				}

			}

		} else {
			$data = array(
				'message' => 'Something Went Wrong'
			);
		}


		echo json_encode($data);
	}


	public function followfromtheprofile()
	{

		$data = $this->members_model->followfromtheprofile($_POST);
		echo json_encode($data);

	}

	public function beamember()
	{

		$groupid = $_POST['groupid'];
		$postId = decode_id($_POST['poid']);
		$general_Count = $this->members_model->check_if_member_record_access_exist("group", $groupid, '', '', "pending");
		$general_Count += $this->members_model->check_if_member_record_access_exist("group", $groupid, '', '', "active");
		$post_record = $this->posts_model->get_Record($postId);


		$group = $this->groups_model->get_Record($groupid);

		if ($_POST['member_status'] != 'non_member') {
			$response = array('status' => 'error', 'message' => 'This option is only for non_member Yet', 'id' => encode_id($postId), 'type' => 'post');

			echo json_encode($response);
			exit;

		}

		$user = $this->users_model->get_Users($this->LoggedInUser);
		$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';
		$linkmadeofgroup = "<a href='" . base_url() . "account/view/group/" . encode_id($groupid) . "'>";

		if ($general_Count == 0) {
			$resp = $this->members_model->add_Member($groupid, 'group', 'member', $status, $this->LoggedInUser, 1, 1, '', $postId);
			if ($resp) {
				$resp = $this->members_model->add_Member($groupid, 'post', 'member', 'pending', $this->LoggedInUser, 1, 1, '', $postId);
				$notif_type = "post_join_via_be_a_member";
				$linkmade = "<a href='" . base_url() . "account/view/post/" . encode_id($postId) . "'>";
				$content = " joined " . $post_record['Po_Privacy'] . " post " . $linkmade . $post_record['Po_Title'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> to become a member and be able to comment on the post. ";
				$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $post_record['Po_Title'] . ' post to become a member and be able to comment on the post.', $content, $this->LoggedInUser, $resp, $groupid, 'group');
				if ($status == 'pending') {
					$response = array('status' => 'success', 'message' => 'You joined ' . $post_record['Po_Privacy'] . ' post ' . strtoupper($post_record['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can make a comment on this post.', 'id' => encode_id($groupid), 'type' => 'group');
				} else {
					$response = array('status' => 'success', 'message' => 'You joined ' . $post_record['Po_Privacy'] . ' post ' . strtoupper($post_record['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can make a comment on this post. ', 'id' => encode_id($postId), 'type' => 'post');
				}
			}
			echo json_encode($response);
		} else if ($general_Count > 0) {
			$general_Count_post = $this->members_model->check_if_member_record_access_exist("post", $groupid, '', $postId, "pending");
			$general_Count_post += $this->members_model->check_if_member_record_access_exist("post", $groupid, '', $postId, "active");
			if ($general_Count_post > 0) {
				$response = array('status' => 'error', 'message' => 'Your Request to join this post is already under process ', 'id' => encode_id($postId), 'type' => 'post');
				echo json_encode($response);
			} else {
				$resp = $this->members_model->add_Member($groupid, 'post', 'member', 'pending', $this->LoggedInUser, 1, 1, '', $postId);
				$notif_type = "post_join_via_be_a_member";
				$linkmade = "<a href='" . base_url() . "account/view/post/" . encode_id($postId) . "'>";
				$content = " joined " . $post_record['Po_Privacy'] . " post " . $linkmade . $post_record['Po_Title'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> to become a member and be able to comment on the post. ";
				$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $post_record['Po_Title'] . ' post to become a member and be able to comment on the post.', $content, $this->LoggedInUser, $resp, $groupid, 'group');
				if ($status == 'pending') {
					$response = array('status' => 'success', 'message' => 'You joined ' . $post_record['Po_Privacy'] . ' post ' . strtoupper($post_record['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can make a comment on this post.', 'id' => encode_id($groupid), 'type' => 'group');
				} else {
					$response = array('status' => 'success', 'message' => 'You joined ' . $post_record['Po_Privacy'] . ' post ' . strtoupper($post_record['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can make a comment on this post. ', 'id' => encode_id($postId), 'type' => 'post');
				}

				echo json_encode($response);
			}


		}


	}
		function endpoll(){
			$updatearray = array(
				'poll_stop' => $_POST['Extendvalue']
			);
			$_POST['PostID'] = decode_id($_POST['PostID']);

			$this->db->where('Pl_PO_ID', $_POST['PostID']);
			$res = $this->db->update( 'bs_posts_poll', $updatearray);
			if($res){
				$response = array(
					'action' => 'success',
					'message' => 'Do you want to continue / extend the poll?.'
				);
			}else{
				$response = array(
					'action' => 'error',
					'message' => 'Poll not update. Try again.'
				);
			}

			echo json_encode($response);

		}
	function deletepoll(){
		$_POST['PostID'] = decode_id($_POST['PostID']);

		$this->db->where('Pl_PO_ID', $_POST['PostID']);
		$res  = $this->db->delete('bs_posts_poll');

		$this->db->where('Poa_po_id', $_POST['PostID']);
		$res  = $this->db->delete('bs_poll_ans');

		if($res){
			$response = array(
				'action' => 'success',
				'message' => 'Successfully delete a poll'
			);
		}else{
			$response = array(
				'action' => 'error',
				'message' => 'Poll not delete Try again.'
			);
		}

		echo json_encode($response);


	}
			function pollanswerupdate(){
				$_POST['PostID'] = decode_id($_POST['PostID']);


		 		$answerupdate =  implode("^",$_POST['answer']);

				 $updatedata = array(

					 'PL_answers' => $answerupdate,
					 'PL_description' => $_POST['description'],
					  'PL_Question' => $_POST['qesutttionmain'],

					 'serverybuttonlink' => $_POST['serverybuttonlink'],
					 'serverybuttonname' => $_POST['serverybuttonname']
				 );
				 $whereupdate = array(
						'Pl_ID' => $_POST['pollID'],
					 'Pl_PO_ID' => $_POST['PostID']
				 );

				$this->db->where($whereupdate);
				$res =  $this->db->update('bs_posts_poll', $updatedata);
					if($res){
						$response = array(
							'action' => 'success',
							'message' => 'Successfully updated a poll'
						);
					}else{
						$response = array(
							'action' => 'error',
							'message' => 'Poll not update Try again.'
						);
					}

					echo json_encode($response);
			}
			function answeraddpoll(){

			 	$_POST['PostID'] = decode_id($_POST['PostID']);
				$this->db->select('bs_poll_ans.*');
				$this->db->from('bs_poll_ans');
				$this->db->where('Poa_po_id' , $_POST['PostID']);
				$this->db->where('Poa_Us_ID' ,$this->LoggedInUser);
				$qdata = 	$this->db->get()->result_array();
				if(count($qdata) > 0 ) {
					$wheredata = array(
						'Poa_po_id' => $_POST['PostID'],
						'Poa_pl_id' => $_POST['pollID'] ,
						'Poa_Us_ID' => $this->LoggedInUser
					);

							$updatearray = array(
								'Poa_ans' => $_POST['answer'],
								'Poa_note' => $_POST['answernote']
							);
							$this->db->where($wheredata);
						$res = 	$this->db->update('bs_poll_ans' , $updatearray);
					if($res){
						$response = array(
							'action' => 'Your poll has been updated.',
							'message' => 'Success'
						);
					}else{
						$response = array(
							'action' => 'error',
							'message' => 'Try Again'
						);
					}

				} else	{
					$storedata = array(
						'Poa_po_id' => $_POST['PostID'],
					  	'Poa_pl_id' => $_POST['pollID'] ,
						'Poa_Us_ID' => $this->LoggedInUser ,
						'Poa_ans' => $_POST['answer'],
						'Poa_note' => $_POST['answernote']
					);

					$res =	$this->db->insert('bs_poll_ans' ,	$storedata );
					if($res){
						$response = array(
							'action' => 'Your poll has been submitted.',
							'message' => 'Success'
						);

					}else{
						$response = array(
							'action' => 'error',
							'message' => 'Try Again'
						);
					}
					}
				echo json_encode($response);
			}
	public	function addpoll(){

			$_POST['PostID'] = decode_id($_POST['PostID']);
				$this->db->select('bs_posts_poll.*');
				$this->db->from('bs_posts_poll');
				$this->db->where('Pl_PO_ID' , $_POST['PostID']);
			$qdata = 	$this->db->get()->result_array();
			if(count($qdata) > 0 ){


				$response = array(
					'action' => 'error',
					'message' => 'You can only have one poll for each post.'
				);
			}else{
				$storedata = array(
					'Pl_PO_ID' => $_POST['PostID'],
					'PL_Question' => $_POST['pollquestion'] ,
					'PL_description' => $_POST['pooldescription'] ,
					'PL_answers' =>  $_POST['answersheet'],
					 'serverybuttonlink' => $_POST['serverybuttonlink'],
				 	 'serverybuttonname' => $_POST['serverybuttonname']
				);
				$res =	$this->db->insert('bs_posts_poll' ,	$storedata );
				if($res){
					$response = array(
						'action' => 'success',
						'message' => 'Successfully created a poll'
					);

				}else{
					$response = array(
						'action' => 'error',
						'message' => 'Try Again'
					);
				}
			}


		echo json_encode($response);
			}

			function sendcoinstorecipe(){
 				$inserId = array();
			$ammountperperson =  $_POST['id'];

			$personID = 	$_POST['list'];


			for($x = 0; $x  < count($personID) ; $x++){

					if($this->LoggedInUser == $personID[$x]){
						continue;
					}
 					$datainsert = array(
				    "RC_Ammount" =>	$ammountperperson,
					"RC_Title" =>  "Pool Reward",
					"RC_From" => $this->LoggedInUser,
					"RC_To" =>  $personID[$x],
						"RC_transection" =>  rand(10000000000,10000000000000)

					);
				 
 				$this->db->insert('recievedcoins' , $datainsert ) ;

					array_push($inserId,  $this->db->insert_id()) ;
				}
			if(count($inserId) > 0){
		 		$this->db->set('Us_Coins', 'Us_Coins - ' . (int) $ammountperperson * count($personID) , FALSE);
				$this->db->where('Us_ID' , $this->LoggedInUser);
				$ress = $this->db->update('bs_users');
			}



 				$objectRewardSend = array(
					'Rs_ammount' => $_POST['id'],
					'Rs_Us_ID' => $this->LoggedInUser ,
					'Rs_post_id' => $_POST['post_id'],
					'Rs_Group_id' => $_POST['Group_id'],
					'Rs_usercount' => count($inserId),
					'Rs_userinsertID' => json_encode($inserId),
					'RS_Transection_ID' =>  rand(10000000000,10000000000000)

				);


				 if(count($inserId) != 0){
					$ress = $this->db->insert('rs_rewardcoin_send' , $objectRewardSend);
				 }else{
					 $ress = 1;
				 }

				 echo $ress ;


			}

			function coin_creted(){
				$_POST['id'];
				$where = array(
					'Us_ID' => $_POST['id']
				);
				$update = array(
					'Us_Coins' =>  $_POST['coins']
				);
				$this->db->where($where);


				$this->db->set('Us_Coins', 'Us_Coins + ' . (int)  $_POST['coins'] , FALSE);
				 $res = $this->db->update('bs_users'  );

					if($res){
						$TransectionData = array(
							'CTA_Transection_ID' => rand(10000000000,10000000000000),
							'CTA_Ammount' =>   $_POST['coins'] ,
							'CTA_staus' => 1,
							'CTA_US_ID' =>$_POST['id']
						);
						$ress = $this->db->insert('credit_transection_admin' , $TransectionData );

						if($ress){
							echo json_encode('success');
						}else{
							echo json_encode('error');
						}


					}else{
						echo json_encode('error');
					}


			}
				function getusercredit(){


					$this->db->select('bs_users.Us_Coins');
					$this->db->from('bs_users');
					$this->db->where('Us_ID ' ,$this->LoggedInUser);
					$res = $this->db->get()->result_array();
					echo json_encode($res);
				}
			public function Coins(){


				$this->db->select('coins_get.*');
				$this->db->from('coins_get');
				$this->db->where('Cg_US_ID' , $this->LoggedInUser);
			$rewardcoind = 	$this->db->get()->result_array();

//				print_r($rewardcoind);

					$rewardedCoin = 0;
				foreach ($rewardcoind as $val){
					$rewardedCoin += $val['Cg_coins'];
				};


		$this->db->select('bs_users.Us_Coins , bs_users.Us_ID');
		$this->db->from('bs_users');
		$this->db->where('bs_users.Us_ID' , $this->LoggedInUser);
		$usercoins = $this->db->get()->result_array();


				if (!isset($this->session->userdata['logged_in'])) {
					redirect('/pages/view/signin');
					//if session is not there, redirect to login page
				}
				$data['title'] = "Coin";
				$UserID = ($this->session->userdata['logged_in']['bs_id']);
				$data['usercoins'] = $usercoins[0]['Us_Coins'];
				$data['rewardCoins'] = $rewardedCoin;




//				$this->load->view('templates/header', $data);
//				$this->load->view('templates/navigation', $data);
				$this->load->view('account/coins', $data);
//				$this->load->view('templates/footer', $data);

//				$this->load->view('templates/header', $data);
//				$this->load->view('templates/navigation', $data);
//				$this->load->view('account/coins', $data);
//				$this->load->view('templates/footer', $data);
			}



			function FakeuserDelete(){

					$this->db->select('bs_users.*')->from('bs_users');
					$this->db->where('Us_Status' , 'pending');
					$data = $this->db->get()->result_array();

				   $this->db->delete('bs_users', array('Us_Status' => 'pending'));



			}


	function getVoucherCodecharge(){


		$where = array(
			'Vr_Us_ID' => $this->LoggedInUser,
			'Vr_Status' => 'active',
//			'Vr_deadline >=' => date('Y-m-d'),
			'Vr_Id' => $_POST['id']
		);
		$this->db->select('bs_vouchers.* , bs_users.* , bs_posts.*');
		$this->db->from('bs_vouchers');
		$this->db->join('bs_users' , 'bs_users.Us_ID = bs_vouchers.Vr_Us_ID');

		$this->db->join('bs_posts' , 'bs_posts.PO_ID = bs_vouchers.Vr_post_id');

		$this->db->where($where);
		$res = $this->db->get()->result_array();
		 if(count($res) > 0){


			 if((int) $res[0]['Us_Coins'] >= (int) $res[0]['Vr_radeemprice']){

				 $this->db->set('Us_Coins', 'Us_Coins + ' . (int) $res[0]['Vr_radeemprice'] , FALSE);
				 $this->db->where('Us_ID' , $this->LoggedInUser);
				 $ress = $this->db->update('bs_users');
				 if($ress){

					 $updte = array(
						 'Vr_Status' => 'purchased',
					 );

					 $this->db->where('Vr_Id' , $_POST['id']);
					 $resss = $this->db->update('bs_vouchers' , $updte);
					if($resss){
						$resp = array(
							'type' => 'success',
							'action' => 'success',
							'message' => 'Voucher redemption sucessful'
						);
						echo json_encode($resp);
					}else{
						$resp = array(
							'type' => 'error',
							'action' => 'error',
							'message' => 'Try Again Later!'
						);
						echo json_encode($resp);
					}
				 }
			 }else{
				 $resp = array(
					 'type' => 'info',
					 'action' => 'info',
					 'message' => 'Low Balance'
				 );
				 echo json_encode($resp);
			 }



		 }else{
			 $resp = array(
				 'type' => 'info',
				 'action' => 'info',
				 'message' => 'No record Found'
			 );
			 echo json_encode($resp);
		 }


	}
		function getVoucherCode(){


			$where = array(
				'Vr_Us_ID' => $this->LoggedInUser,
//				'Vr_Status' => 'active',
//				'Vr_deadline >=' => date('Y-m-d'),
				'Vr_Id' => $_POST['id']
			);



			  $this->db->select('bs_vouchers.Vr_deadline , 
			  	bs_vouchers.Vr_decription ,
			   bs_vouchers.Vr_title,
			   bs_vouchers.Vr_radeemprice,
			   bs_vouchers.Vr_paisaammount,
			   bs_vouchers.Vr_Voucher_Token, bs_vouchers.Vr_sub_title,
			   
			   bs_vouchers.Vr_Status,  bs_users.Us_Photo  ,bs_users.Us_Coins ,
			   bs_users.Us_Name ,bs_users.Us_JobTitle  , bs_posts.Po_Thumb , ');
			  $this->db->from('bs_vouchers');
			  $this->db->join('bs_users' , 'bs_users.Us_ID = bs_vouchers.Vr_Us_ID');

			 $this->db->join('bs_posts' , 'bs_posts.PO_ID = bs_vouchers.Vr_post_id');

			  $this->db->where($where);
			  $res = $this->db->get()->result_array();

			  if($res[0]['Vr_Status'] == 'active'){
				  $res[0]['Vr_Voucher_Token'] = '';
			  }
			  echo json_encode($res);


		}
		public function createVoucherCode(){


				$counter = 0;

				$this->db->select('bs_voucher_post.*');
				$this->db->from('bs_voucher_post');
				$this->db->where('Vp_id' , $_POST['Voucher_PID']) ;
				$remainvalues  = $this->db->get()->result_array();

					if($remainvalues[0]['Vp_vounchercount'] <= 0){

 						$resp = array(
							'type' => 'info',
							'action' => 'info',
							'message' => $remainvalues[0]['Vp_vounchercount'] .' '. 'Voucher Remain'
						);
						echo json_encode($resp);

					}else{
						if(isset($_POST['selectedusers'])){
							for($x = 0 ; $x < count($_POST['selectedusers']); $x++){


								if($_POST['selectedusers'][$x] == $this->LoggedInUser){
									continue;
								}

								$where = array(
									'Vr_Us_ID' => $_POST['selectedusers'][$x] ,
									'Vr_post_id' => $_POST['Vr_post_id'] ,

								);

								$this->db->select('bs_vouchers.Vr_Us_ID , bs_vouchers.Vr_post_id');
								$this->db->from('bs_vouchers');
								$this->db->where($where);
									$recordexist = 	$this->db->get()->result_array();

									 if(count($recordexist) == 0){
										 $insertdata = array(
											 'Vr_title'  => $_POST['Vr_title'],
											 'Vr_sub_title'  => $_POST['Vr_sub_title'],
											 'Vr_vounchercount'    => $_POST['Vr_vounchercount'],
											 'Vr_deadline'    => $_POST['Vr_deadline'],
											 'Vr_From_user' => $this->LoggedInUser,
 											 'Vr_paisaammount'  => $_POST['Vr_paisaammount'],
											 'Vr_radeemprice'  => $_POST['Vr_radeemprice'],
											 'Vr_decription'  => $_POST['Vr_decription'],
											 'Vr_post_id'  => $_POST['Vr_post_id'],
											 'Vr_Group_id'  => $_POST['Vr_Group_id'],
											 'Vr_Us_ID' => $_POST['selectedusers'][$x],
											 'Vr_Status' => 'active',
											 'Vr_Voucher_Token'  =>  rand(1000000000,1000000000000)
										 );

										 $this->db->insert('bs_vouchers' , $insertdata);
										 $insert_id = $this->db->insert_id();
										 $counter++;
									 }




							}



							if(isset($_POST['Voucher_PID'])){
 									$this->db->set('Vp_vounchercount', 'Vp_vounchercount - ' . (int) $counter, FALSE);
								$this->db->where('Vp_id', $_POST['Voucher_PID']);

							$res = 	$this->db->update('bs_voucher_post');

							if($counter == 0){
								$resp = array(
									'type' => 'info',
									'action' => 'info',
									'message' =>  'Voucher already sent to selected user/s'



								);
							}else{
								if($res == 1){
									$resp = array(
										'type' => 'success',
										'action' => 'Voucher/s Sent',
										'message' =>  'Voucher/s sent can be used only when recipient/s redeem.'


									);
								}else{
									$resp = array(
										'type' => 'error',
										'action' => 'error',
										'message' =>  'Something went wrong Try Again Later'
									);
								}
							}


								echo json_encode($resp);

//
							}

						}
					}





			}

			function makepostpromote(){

				  if(!isset($this->session->userdata['logged_in']['bs_admin'])) {
					  echo json_encode('error');
					  exit;
				}

					if($_POST['promoteType'] == 'post'){
						if($_POST['expiryDate'] == ''){
							echo json_encode('datemissing');
							exit;
						}
						$this->db->select('promoted_post.*');
						$this->db->from('promoted_post');
						$this->db->where('Pp_Post_ID' , $_POST['post_id']);
						$this->db->where('Pp_group_ID' , $_POST['Group_id']);
//						$this->db->where('Pp_Expiry_Date >' , date('Y-m-d'));
//						$this->db->where_in('Pp_status'  , array('active' , 'review'));

						$exiting = $this->db->get()->result_array();

						if(count($exiting) > 0){
							if( $_POST['expiryDate'] > $exiting[0]['Pp_Expiry_Date'] ){


								$update = array(
									'Pp_Expiry_Date' => $_POST['expiryDate']
								);
								$where = array(
									'Pp_ID' => $exiting[0]['Pp_ID']
								);
								$this->db->where($where);
								$res = $this->db->update('promoted_post' , $update);
								if($res){
									echo json_encode('updateDate');
								}else{
									echo 'error';
								}
							}
							else if($exiting[0]['Pp_status']){
								echo json_encode('underreview');
							}else{
								echo json_encode('already');
							}

						}
						else{

							$Expiry_Date = 	$_POST['expiryDate'];

							$status = 'review';
							 if(  $this->session->userdata['logged_in']['bs_admin'] == 1){
								 $status = 'active';
							 }
							$insertPromote = array(
								'Pp_Post_ID' => $_POST['post_id'],
								'Pp_Us_ID' => $this->LoggedInUser,
								'Pp_Expiry_Date' => $Expiry_Date ,
								'Pp_Package_ID' => $_POST['packageID'],
								'Pp_group_ID' => $_POST['Group_id'],
								'Pp_status' =>  $status

							);


							$res = $this->db->insert( 'promoted_post', $insertPromote);

							if($res){
								echo json_encode('success');
							}else{
								echo json_encode('fail');
							}
						}

					}else if($_POST['promoteType'] == 'group'){

						$groupID = decode_id($_POST['Group_id']);
						$this->db->select('promoted_group.*');
						$this->db->from('promoted_group');

						$this->db->where('Pg_group_ID' , $groupID);
						$this->db->where('Pg_Expiry_Date >' , date('Y-m-d'));
						$this->db->where_in('Pg_status' , array('active' , 'review'));

						$exiting = $this->db->get()->result_array();
						if(count($exiting) > 0){
							if($exiting[0]['Pg_status']){
								echo json_encode('underreview');
							}else{
								echo json_encode('already');
							}

						}else{
							$Expiry_Date = 	date('Y-m-d', strtotime(' +6 day'));

							$status = 'review';
							if(  $this->session->userdata['logged_in']['bs_admin'] == 1){
								$status = 'active';
							}
							$insertPromote = array(

								'Pg_Us_ID' => $this->LoggedInUser,
								'Pg_Expiry_Date' => $Expiry_Date ,
								'Pg_Package_ID' => $_POST['packageID'],
								'Pg_group_ID' => $groupID,
								'Pg_status' => $status

							);


							$res = $this->db->insert( 'promoted_group', $insertPromote);

							if($res){
								echo json_encode('success');
							}else{
								echo json_encode('fail');
							}
						}
					}else{
						echo json_encode('fail');
					}






			}
			function VoucherRemainCount(){

						$this->db->select('bs_voucher_post.Vp_vounchercount');
						$this->db->from('bs_voucher_post');
						$this->db->where('Vp_id', $_POST['Voucher_PID']);
						$res =  $this->db->get()->result_array();




					   echo json_encode($res);
			}
	public function notifications()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Notifications";
		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['notifications_items'] = $this->notifications_model->get_UserNotification($UserID);

        $data['get_UserNotification_post_joins'] = $this->notifications_model->get_UserNotification_post_joins($UserID);

		$data['notification_invite_group'] = $this->notifications_model->get_group_invite($UserID);
		$data['get_group_invite_creator'] = $this->notifications_model->get_group_invite_creator($UserID);


		$data['notifications_items'] = array_merge($data['notifications_items'], $data['get_UserNotification_post_joins']);

		$data['notifications_items'] = array_merge($data['notifications_items'], $data['notification_invite_group']);
		$data['notifications_items'] = array_merge($data['notifications_items'], $data['get_group_invite_creator']);

		function array_sort($array, $on, $order=SORT_ASC)
		{
			$new_array = array();
			$sortable_array = array();

			if (count($array) > 0) {
				foreach ($array as $k => $v) {
					if (is_array($v)) {
						foreach ($v as $k2 => $v2) {
							if ($k2 == $on) {
								$sortable_array[$k] = $v2;
							}
						}
					} else {
						$sortable_array[$k] = $v;
					}
				}

				switch ($order) {
					case SORT_ASC:
						asort($sortable_array);
						break;
					case SORT_DESC:
						arsort($sortable_array);
						break;
				}

				foreach ($sortable_array as $k => $v) {
					$new_array[$k] = $array[$k];
				}
			}

			return $new_array;
		}
		function date_sort_noti($a, $b)
		{
			return strtotime($b['No_DatePosted']) - strtotime($a['No_DatePosted']);
		}

		usort($data['notifications_items'], "date_sort_noti");

				$dataa = array(
					"keyword" => '',
					"alpha" => '',
					"fav"=> false,
					"popular"=> false,
					"latest"=> false
				);
		$data['featured_Groups'] = $this->groups_model->show_featured_groups($dataa);

		$data['featured_post'] = $this->posts_model->show_public_popular_posts_promote_Data( $UserID , $dataa);
		$data['featured_post'] =  $data['featured_post']['data'];

		$data['featured_Groups']  = $data['featured_Groups']['data'];

		$data['notifications_activity'] = $this->notifications_model->get_Useractivity($UserID);
		$data['comment_post_notification'] = $this->notifications_model->comment_post_notification($UserID);
		$data['comment_menion_notification'] = $this->notifications_model->comment_menion_notification($UserID);
		$data['comment_todo_notification'] = $this->notifications_model->comment_todo_notification($UserID);
		$data['comment_todo_notification_owen'] = $this->notifications_model->comment_todo_notification_owen($UserID);

		$data['notifications_activity_count'] = $this->notifications_model->get_Useractivity_new($UserID);
 		$data['to_do'] =  $this->notifications_model->get_user_todo_Notification($UserID);

		$allcount = 0;
		$allcount += count($data['notifications_items']);
		$allcount += count($data['notifications_activity_count']);
		$allcount += count($data['comment_post_notification']);
		$allcount += count($data['comment_menion_notification']);
		$allcount += $data['to_do'];
		$data['allcounter'] = $allcount;
		$data['asset'] = 'account/notifications';

		$data['count_for_activity'] = count($data['notifications_activity_count']);
		$data['count_for_Comment'] = $this->notifications_model->get_user_Comment_Notification($UserID);
		$data['count_for_mention'] = $this->notifications_model->get_user_mention_Notification($UserID);
		$data['permission_counter'] = count($data['notifications_items']);
     	$data['get_user_voucher'] = $this->notifications_model->get_user_voucher($UserID);
		$data['send_user_voucher'] = $this->notifications_model->send_user_voucher($UserID);

		$data['All_user_voucher'] = array_merge($data['get_user_voucher'],$data['send_user_voucher']);

		$data['All_user_voucher']  =	array_sort($data['All_user_voucher'], 'Vr_Id', SORT_DESC);
//		echo '<pre>';
//		print_r($data['All_user_voucher']);
//
//		exit;
		$this->db->select('coin_packages.*');
		$this->db->from('coin_packages');
		$ada = $this->db->get()->result_array();

		$data['allpacakges'] = $ada;
		$data['button_items'] = $this->buttons_model->get_record();


		$this->db->select('coins_get.* ');
		$this->db->from('coins_get');
//		$this->db->join('coins_reward' , 'coins_reward.Cr_ID  = coins_get.Cr_ID');
		$this->db->where('coins_get.Cg_US_ID' , $this->LoggedInUser);
		$ress = $this->db->get()->result_array();

		$data['coinrewarding'] = $ress;

		$this->db->select('rs_rewardcoin_send.*');
		$this->db->from('rs_rewardcoin_send');
		$this->db->where('Rs_Us_ID' , $this->LoggedInUser);
		$rewardresponse = $this->db->get()->result_array();
		$data['ListofRewardsend'] = $rewardresponse;


		$this->db->select('recievedcoins.* , bs_users.Us_ID , bs_users.Us_Name ,
				 bs_users.Us_Alias , bs_users.Us_LName , 	 bs_users.Us_FName');

		$this->db->from('recievedcoins');

		$this->db->join('bs_users' , 'recievedcoins.RC_From  =   bs_users.Us_ID');

		$this->db->where('recievedcoins.RC_To' , $this->LoggedInUser);

			$rewardingcoind = 	$this->db->get()->result_array();


			$data['totalrecievedreward'] = $rewardingcoind;


				$this->db->select('credit_transection.* , coin_packages.*');
				$this->db->from('credit_transection');
				$this->db->join('coin_packages' , 'coin_packages.CP_ID  = credit_transection.CT_Package_Id', 'left');
				$this->db->where('credit_transection.CT_US_ID' , $this->LoggedInUser);
				$res = 	$this->db->get()->result_array();
				$data['transeectionhistory'] = $res;

		$this->db->select('credit_transection_admin.* ');
		$this->db->from('credit_transection_admin');
 		$this->db->where('credit_transection_admin.CTA_US_ID' , $this->LoggedInUser);
		$this->db->order_by('credit_transection_admin.CTA_ID', 'DESC');
		$res = 	$this->db->get()->result_array();
		$data['transeectionhistoryadmin'] = $res;

		$this->db->select('bs_users.Us_Coins , bs_users.Us_ID');
		$this->db->from('bs_users');
		$this->db->where('bs_users.Us_ID' , $this->LoggedInUser);
		$usercoins = $this->db->get()->result_array();
		$data['usercoins'] = $usercoins[0]['Us_Coins'];

		$data['reward'] = 0;





		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/notifications', $data);
		$this->load->view('templates/footer', $data);
	}
		public function getsendrewardDetail(){

			$listofuser = array();
			for($x = 0 ; $x < count($_POST['list']);  $x++){
				$this->db->select('recievedcoins.* ,  bs_users.Us_Name ,
				 bs_users.Us_FName ,
				 bs_users.Us_LName ,
				  bs_users.Us_LName ,
				  bs_users.Us_Photo,
				 bs_users.Us_Alias ,
				 bs_users.Us_Email ');
				$this->db->from('recievedcoins');
				$this->db->join('bs_users' , 'bs_users.Us_ID  = recievedcoins.RC_To');
				$this->db->where('recievedcoins.RC_ID' , $_POST['list'][$x]);
			 $res = $this->db->get()->result_array();
			 array_push($listofuser , $res[0]);
			}



		echo json_encode( $listofuser);

		}
	public function settings($option = false)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}


		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$data['notification'] = "";
		$data['selMenu'] = 'profile';
		if (strlen($option) == 0) $option = 'profile';

		$data['users_item'] = $this->users_model->get_Users($data['CurrUserID']);
		if (empty($data['users_item'])) {
			show_404();
		}


		if (strcmp($option, 'profile') == 0) {
			$data['selMenu'] = 'profile';

		} else if (strcmp($option, 'saveprofile') == 0) {
			$data['selMenu'] = 'profile';
			$data = $this->SaveProfile($data);
			$data['users_item'] = $this->users_model->get_Users($data['CurrUserID']);
		} else if (strcmp($option, 'password') == 0) {
			$data['selMenu'] = 'password';

		} else if (strcmp($option, 'changepassword') == 0) {
			$data['selMenu'] = 'password';
			$data = $this->ChangePassword($data);

		} else if (strcmp($option, 'photo') == 0) {
			$data['selMenu'] = 'photo';

		} else if (strcmp($option, 'changephoto') == 0) {
			$data['selMenu'] = 'photo';
			$data = $this->do_UploadPhoto($data['CurrUserID'], '', 'userphoto', $data);
			$data['users_item'] = $this->users_model->get_Users($data['CurrUserID']);
		} else if (strcmp($option, 'background') == 0) {
			$data['selMenu'] = 'background';
			$data['backgrounds_items'] = $this->settings_model->get_Background();
			$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		} else if (strcmp($option, 'changebackground') == 0) {
			$data['selMenu'] = 'background';
			$data = $this->do_UploadPhoto($data['CurrUserID'], '', 'userbackground', $data);
			$data['users_item'] = $this->users_model->get_Users($data['CurrUserID']);
		}


		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/profile_header', $data);
		$this->load->view('templates/profile_settings', $data);
		if ((strcmp($option, 'profile') == 0) || (strcmp($option, 'saveprofile') == 0)) $this->load->view('settings/profile', $data);
		else if ((strcmp($option, 'password') == 0) || (strcmp($option, 'changepassword') == 0)) $this->load->view('settings/password', $data);
		else if ((strcmp($option, 'photo') == 0) || (strcmp($option, 'changephoto') == 0)) $this->load->view('settings/photo', $data);
		else if ((strcmp($option, 'background') == 0) || (strcmp($option, 'changebackground') == 0)) $this->load->view('settings/background', $data);
		$this->load->view('templates/footer', $data);
	}

	public function SaveProfile($data)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('txtName', 'Name', 'required');
		$this->form_validation->set_rules('txtFeatures', 'Features', 'required');
		if ($this->form_validation->run() === FALSE) {
			$data['error'] = "Invalid!";
		} else {
			$txtID = $this->input->post('UserID');
			$txtName = $this->input->post('txtName');
			$txtAlias = $this->input->post('txtAlias');
			$txtEmail = $this->input->post('txtEmail');
			$txtPhone = $this->input->post('txtPhone');
			$txtCompany = $this->input->post('txtCompany');
			$txtJobTitle = $this->input->post('txtJobTitle');
			$txtNoOfEmployees = $this->input->post('txtNoOfEmployees');
			$txtFeatures = $this->input->post('txtFeatures');
			$isPrivate = $this->input->post('txtPrivacy');

			$this->users_model->set_UpdateProfile($txtID, $txtName, $txtAlias, $txtEmail, $txtPhone, $txtCompany, $txtJobTitle, $txtNoOfEmployees, $txtFeatures, $isPrivate);
			$data['notification'] = "Profile was updated successfully!";
		}
		return $data;
	}

	public function ChangePassword($data)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtOldPassword', 'Old Password', 'required');
		$this->form_validation->set_rules('txtNewPassword', 'New Password', 'required');
		$this->form_validation->set_rules('txtRetypePassword', 'Re-type Password', 'required');

		if ($this->form_validation->run() === FALSE) {
			$data['error'] = "Invalid!";
		} else {


// 					$txtID = $this->input->post('UserID');
// 					$txtPassword = $this->input->post('txtNewPassword');
//
// 					$plaintext_password = $this->input->post('txtNewPassword');
//                     $hash = password_hash($plaintext_password,
//                                                       PASSWORD_DEFAULT);
//  					$this->users_model->set_UpdatePassword($txtID, $hash);
//  					exit;

			$this->db->select('bs_users.Us_Password ,bs_users.Us_ID');

			$this->db->from('bs_users');
			$this->db->where('Us_ID', $_POST['UserID']);
			$old_password = $this->db->get()->result_array();

			$verify = password_verify($this->input->post('txtOldPassword'), $old_password[0]['Us_Password']);


			if ($verify) {

// 			$txtPassword = $this->input->post('txtOldPassword');
// 			if (strcmp($txtPassword,$data['users_item']['Us_Password'])==0)
// 			{

				$txtNewPassword = $this->input->post('txtNewPassword');
				$txtRetypePassword = $this->input->post('txtRetypePassword');
				if (strcmp($txtNewPassword, $txtRetypePassword) == 0) {


					$txtID = $this->input->post('UserID');
					$txtPassword = $this->input->post('txtNewPassword');

					$plaintext_password = $this->input->post('txtNewPassword');
					$hash = password_hash($plaintext_password,
						PASSWORD_DEFAULT);
					$this->users_model->set_UpdatePassword($txtID, $hash);
					$data['notification'] = "Password was changed successfully!";
				} else {
					$data['error'] = "Password Mismatch!";
				}
			} else {
// 				 echo 'matched';
//             			  } else {
//             				echo 'not matched';
//             			  }

				$data['error'] = "Invalid Old Password!";

			}
		}

		return $data;
	}


	public function ChangeBackground($data)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$UserID = ($this->session->userdata['logged_in']['bs_id']);


		$data['error'] = "";
		$data['errorProfile'] = "";
		$data['errorPassword'] = "";
		$data['errorPhoto'] = "";
		$data['errorBackground'] = "";

		$data['title'] = "Change Background";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['selMenu'] = "Background";

		$Path = "";
		$UploadFile = $this->input->post('UploadFile');
		$Error = $this->uploadphoto($UserID, './uploads/' . $UserID . $Path, $UploadFile);
		$ErrorMsg = substr($Error, 0, 6);
		if (strcmp('Error:', $ErrorMsg) == 0) {
			$data['errorBackground'] = $Error;
		} else {
			$FileName = $Error;
			$Photo = $this->getPhotoPath($UserID, $Path, $FileName);
			$this->users_model->set_UpdateBackground($UserID, $Photo);
			$data['errorBackground'] = "Background was changed successfully!";
		}

		$this->showSettings($data);
	}


	public function survey($Status, $PostID, $error = false)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = $error;
		$data['selMenu'] = "Messages";
		$data['selSubMenu'] = "Detail";

		$this->showSurvey($Status, $PostID, $data);
	}

	public function beanadmin(){

		$this->db->select('bs_topics.*');
		$this->db->from('bs_topics');
		$this->db->where('To_ID' , decode_id($_POST['topicid']));
		$topicdata = 	$this->db->get()->result_array();


 		$query = $this->db->get_where('bs_members', array('Me_Parent' => 'topic', 'Me_Gr_ID' => $topicdata[0]['To_Gr_ID'],
			'Me_To_ID' => decode_id($_POST['topicid']), 'Me_Us_ID' => $this->LoggedInUser));
		if($query->num_rows() == 0){

			$this->load->helper('url');
			$this->load->helper('date'); // load Helper for Date
			date_default_timezone_set("Asia/Manila");
			$now = date('Y-m-d H:i:s');

			$data = array(
				'Me_Parent' => "topic",
				'Me_Gr_ID' =>  $topicdata[0]['To_Gr_ID'],
				'Me_To_ID' => decode_id($_POST['topicid']),
				'Me_St_ID' => 0,
				'Me_Us_ID' =>  $this->LoggedInUser,
				'Me_Role' => "admin",
				'Me_Status' => "pending",
				'Me_DatePOsted' => $now
			);

			$this->db->insert('bs_members', $data);
			echo json_encode($this->db->insert_id());
		}else{
			$where = array('Me_Parent' => 'topic', 'Me_Gr_ID' => $topicdata[0]['To_Gr_ID'],
				'Me_To_ID' => decode_id($_POST['topicid']), 'Me_Us_ID' => $this->LoggedInUser);
			$update = array(
				'Me_Role' => "admin",
				'Me_Status' => "pending",
			);

			$this->db->where($where);
 				$ress  = $this->db->update('bs_members' , $update);
			echo json_encode($ress);
		}



	}
	public function showSurvey($Status, $PostID, $data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['posts_item'] = $this->posts_model->get_Post($PostID);
		$data['survey_items'] = $this->survey_model->get_SurveyByPost($PostID);
		$data['topics_items'] = $this->topics_model->get_GroupTopics($data['posts_item']["Gr_ID"]);
		$data['comments_items'] = $this->comments_model->get_Comments($PostID);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();

		$data['ShowAdminAction'] = "d-none";
		if (strcmp($data['posts_item']['Po_Us_ID'], $UserID) == 0) {
			$data['ShowAdminAction'] = "";
		}

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/post_header', $data);

		if (strcmp($Status, 'edit') == 0) $this->load->view('account/editsurvey', $data);
		else {
			$data['answers_items'] = $this->survey_model->get_AnswersByPost($UserID, $PostID);
			$this->load->view('account/survey', $data);
		}

		$this->load->view('templates/footer', $data);
	}


	public function post($PostID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";
		$data['selMenu'] = "Messages";
		$data['selSubMenu'] = "Detail";

		$this->showPost($PostID, $data);
	}


	public function showPost($PostID, $data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);


		$data['posts_item'] = $this->posts_model->get_Post($PostID);
		$data['posts_items'] = $this->posts_model->get_GroupPosts($data['posts_item']['Po_Gr_ID']);
		$data['topics_items'] = $this->topics_model->get_GroupTopics($data['posts_item']["Gr_ID"]);
		$data['comments_items'] = $this->comments_model->get_Comments($PostID);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();

		$data['ShowAdminAction'] = "d-none";
		if (strcmp($data['posts_item']['Po_Us_ID'], $UserID) == 0) {
			$data['ShowAdminAction'] = "";
		}


		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/post_header', $data);
		$this->load->view('postinfo/post_detail', $data);
		$this->load->view('templates/footer', $data);
	}


	public function editpostdetail($PostID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";
		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Detail";

		$this->showPost($PostID, $data);
	}

	public function editpostbg($PostID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";
		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";

		$this->showPost($PostID, $data);
	}


	public function event($EventID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Events";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['users_item'] = $this->users_model->get_Users($UserID);
		$data['events_item'] = $this->events_model->get_Events($EventID);

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/profile_header', $data);
		$this->load->view('account/event', $data);
		$this->load->view('templates/footer', $data);

	}

	public function task($TaskID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Tasks";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['users_item'] = $this->users_model->get_Users($UserID);
		$data['tasks_item'] = $this->tasks_model->get_Tasks($TaskID);

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/profile_header', $data);
		$this->load->view('account/task', $data);
		$this->load->view('templates/footer', $data);

	}


	public function search()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Search";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);

		$chkGroups = $this->input->post('chkGroups');
		$chkTopics = $this->input->post('chkTopics');
		$chkPosts = $this->input->post('chkPosts');

		$data['checkGroups'] = $chkGroups;
		$data['checkTopics'] = $chkTopics;
		$data['checkPosts'] = $chkPosts;

		$data['error'] = "";//$chkGroups.$chkTopics.$chkPosts;


		$txtSearch = $this->input->post('txtSearch');
		if (strlen($txtSearch) == 0) {
			$data['searchText'] = "";
			$data['search_item'] = "";
		} else {
			$data['searchText'] = $txtSearch;
			$data['search_item'] = $this->groups_model->searchGroup($txtSearch);
		}


		$bs_email = ($this->session->userdata['logged_in']['bs_email']);
		$bs_alias = ($this->session->userdata['logged_in']['bs_alias']);
		$bs_id = ($this->session->userdata['logged_in']['bs_id']);

		$this->load->helper('form');
		$this->load->library('form_validation');


		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/search', $data);
		$this->load->view('templates/footer', $data);
	}

	public function setbackground($UserID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$bs_id = ($this->session->userdata['logged_in']['bs_id']);

		$data['error'] = "";
		$data['errorProfile'] = "";
		$data['errorPassword'] = "";
		$data['errorPhoto'] = "";
		$data['errorBackground'] = "";

		$data['title'] = "Change Background";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['selMenu'] = "Background";

		$Background = $this->settings_model->get_Setting($SettingID);
		$this->users_model->set_UpdateBackground($UserID, $Background['Se_Photo']);
		$data['errorBackground'] = "Backcolor was changed successfully!";

		$this->showSettings($data);
	}

	public function setbackcolor($UserID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$bs_id = ($this->session->userdata['logged_in']['bs_id']);

		$data['error'] = "";
		$data['errorProfile'] = "";
		$data['errorPassword'] = "";
		$data['errorPhoto'] = "";
		$data['errorBackground'] = "";

		$data['title'] = "Change Background";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['selMenu'] = "Background";

		$Backcolor = $this->settings_model->get_Setting($SettingID);
		$this->users_model->set_UpdateBackcolor($UserID, $Backcolor['Se_Value']);
		$data['errorBackground'] = "Backcolor was changed successfully!";

		$this->showSettings($data);
	}


	// public function join($Parent, $ParentID)
	// {

	// 	$UserID = ($this->session->userdata['logged_in']['bs_id']);
	// 	$UserName = ($this->session->userdata['logged_in']['bs_name']);
	// 	$GroupID = 0;
	// 	$TopicID = 0;
	// 	$SubTopicID = 0;
	// 	$OwnerID = 0;
	// 	$Board = '';

	// 	if (strcmp($Parent,'group')==0)
	// 	{
	// 		$Group = $this->groups_model->get_Record($ParentID);
	// 		$GroupID = $ParentID;
	// 		$OwnerID = $Group['Gr_Us_ID'];
	// 		$Board= $Group['Gr_Name'].' community';
	// 	}
	// 	else if (strcmp($Parent,'topic')==0)
	// 	{
	// 		$Topic = $this->topics_model->get_Record($ParentID);
	// 		$GroupID = $Topic['To_Gr_ID'];
	// 		$TopicID = $ParentID;
	// 		$OwnerID = $Topic['To_Us_ID'];
	// 		$Board= $Topic['To_Name'].' topic';
	// 	}
	// 	else
	// 	{
	// 		$SubTopic = $this->topics_model->get_Record($ParentID);
	// 		$GroupID = $SubTopic['To_Gr_ID'];
	// 		$TopicID = $SubTopic['To_Pa_ID'];
	// 		$SubTopicID = $ParentID;
	// 		$OwnerID = $SubTopic['To_Us_ID'];
	// 		$Board= $SubTopic['To_Name'].' sub-topic';
	// 	}


	// 	$MemID = $this->users_model->add_Member($Parent, $GroupID, $TopicID, $SubTopicID, $UserID);
	// 	if ($MemID!=0)
	// 	{
	// 		$this->notifications_model->Insert('join', $UserID, $OwnerID, 'User '.$UserName.' joined the '.$Board, '', 'pending');
	// 	}

	// 	if (strcmp($Parent,'group')==0) $this->group($ParentID);
	// 	else if (strcmp($Parent,'topic')==0) $this->topic($ParentID);
	// 	else $this->subtopic($ParentID);
	// }


	public function leave($Parent, $ParentID)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$UserName = ($this->session->userdata['logged_in']['bs_name']);
		$GroupID = 0;
		$TopicID = 0;
		$SubTopicID = 0;
		$OwnerID = 0;
		$Board = '';

		if (strcmp($Parent, 'group') == 0) {
			$Group = $this->groups_model->get_Record($ParentID);
			$GroupID = $ParentID;
			$OwnerID = $Group['Gr_Us_ID'];
			$Board = $Group['Gr_Name'] . ' community';
		} else if (strcmp($Parent, 'topic') == 0) {
			$Topic = $this->topics_model->get_Record($ParentID);
			$GroupID = $Topic['To_Gr_ID'];
			$TopicID = $ParentID;
			$OwnerID = $Topic['To_Us_ID'];
			$Board = $Topic['To_Name'] . ' topic';
		} else {
			$SubTopic = $this->topics_model->get_Record($ParentID);
			$GroupID = $SubTopic['To_Gr_ID'];
			$TopicID = $SubTopic['To_Pa_ID'];
			$SubTopicID = $ParentID;
			$OwnerID = $SubTopic['To_Us_ID'];
			$Board = $SubTopic['To_Name'] . ' sub-topic';
		}


		$MemID = $this->users_model->remove_Members($Parent, $GroupID, $TopicID, $SubTopicID, $UserID);
		if ($MemID != 0) {
			// $this->notifications_model->Insert('leave', $UserID, $OwnerID, 'User '.$UserName.' left the '.$Board, '', 'approved');
		}

		if (strcmp($Parent, 'group') == 0) $this->group($ParentID);
		else if (strcmp($Parent, 'topic') == 0) $this->topic($ParentID);
		else $this->subtopic($ParentID);
	}


	public function updatemember()
	{
		$ID = $this->input->post('txtEditID');
		$data['members_items'] = $this->users_model->get_Member($ID);
		$Parent = $data['members_items']['Me_Parent'];
		$GroupID = $data['members_items']['Me_Gr_ID'];
		$TopicID = $data['members_items']['Me_To_ID'];
		$SubTopicID = $data['members_items']['Me_St_ID'];

		$MemberID = $this->input->post('txtEditMemberID');
		$EditRole = $this->input->post('selEditRole');
		$EditStatus = $this->input->post('selEditStatus');
		if (strcmp($EditStatus, 'removed') == 0) $this->users_model->remove_Members($Parent, $GroupID, $TopicID, $SubTopicID, $MemberID);
		else $this->users_model->updateRole($ID, $EditRole, $EditStatus);

		if (strcmp($Parent, 'topic') == 0) $this->viewtopic($TopicID);
		else if (strcmp($Parent, 'subtopic') == 0) $this->viewsubtopic($SubTopicID);
		else $this->viewgroup($GroupID);
	}


	public function editgroupdetail($GroupID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Topics";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorGroupDetail'] = "";
		$data['errorGroupPhoto'] = "";
		$data['selSubMenu'] = 'GroupDetail';

		$this->showGroupSettings($GroupID, $data);
	}


	public function editgroupbg($GroupID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Topics";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorGroupDetail'] = "";
		$data['errorGroupPhoto'] = "";
		$data['selSubMenu'] = 'Photo';

		$this->showGroupSettings($GroupID, $data);
	}


	public function covertId()
	{

		$id = isset($_POST['userinviteID']) ? $_POST['userinviteID'] : $this->LoggedInUser;
		echo json_encode(encode_id($id));
		exit;

	}

	public function groupinfo($option, $id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$groupdatafor = array(
			'groupid' => $id
		);

		$data['prequestion'] = $this->groups_model->getSaveQuestion($groupdatafor);
		$data['title'] = "Group Info";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$UserID = $data['CurrUserID'];
		$data['selMenu'] = "detail";
		$data['error'] = "";
		$data['notification'] = "";
		$data['EncodedID'] = $id;
		$id = decode_id($id);

		$data['promoted'] = $this->groups_model->get_promoted_group($id);
		$data['groupinfo'] = 'yes';
		if (strcmp($option, 'updatedetail') == 0) {
 			 if (isset($_POST['radPrivacy']) && $_POST['radPrivacy'] != 'private') {
 				$data['error'] = 'You can only create the private Group';
				$option = 'edit';
			} else {
				$data = $this->UpdateGroupDetail($id, $data);
			}


		} else if (strcmp($option, 'background') == 0) {
			$data['selMenu'] = 'background';
		} else if (strcmp($option, 'changebackground') == 0) {
			$data['selMenu'] = 'background';
			$data = $this->do_UploadPhoto($data['CurrUserID'], $id, 'group', $data);
		} else if (strcmp($option, 'delete') == 0) {
			$this->groups_model->delete_row($id);
			redirect('/account/profile');
			return;
		}

		$data['groups_items'] = $this->groups_model->get_Group($id);

		if (empty($data['groups_items'])) {
			redirect('/account/profile');
			return;
		}

		$data['category_items'] = $this->category_model->get_Category();
		$data['subcategory_items'] = $this->category_model->get_SubCategory($data['groups_items']['Gr_Ca_ID']);
		$data['members_items'] = $this->users_model->get_Members('group', $id);

		$data['getMutualFriend'] = $this->members_model->getMutualFriend($data['members_items']);
		$data['members_items'] = $data['getMutualFriend'];
		$data['member_info'] = $this->members_model->get_member_information($id, 'group');
		$data['is_followed'] = $this->follower_model->check_if_followed($data['groups_items']['Gr_ID'], 'group');
		$data = $this->setPermissions($data);
		if (strcmp($UserID, $data['groups_items']['Gr_Us_ID']) == 0) {
			$data['ShowAdminAction'] = "";
		}
		$data['member_counter'] = count($this->groups_model->counterofMember($id));
		$data['post_count'] = count($this->groups_model->counterofpost($id));

		$data['asset'] = 'account/update/group';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/group_header', $data);
		//$this->load->view('templates/group_settings', $data);

			$this->db->select('bs_voucher_post.* , bs_posts.*');
			$this->db->from('bs_voucher_post');
			$this->db->join('bs_posts' , 'bs_posts.Po_ID   =  bs_voucher_post.Vp_post_id' );
			$this->db->where('Vp_Group_id' , $id);


//		echo '<pre>';
		$voucherforpost = 	$this->db->get()->result_array();

					for($x = 0 ; $x < count($voucherforpost) ; $x++){
									$this->db->select('bs_vouchers.Vr_Id');
									$this->db->from('bs_vouchers');
									$this->db->where('Vr_Status' , 'active');
									$this->db->where('Vr_post_id' , $voucherforpost[0]['Vp_post_id']);
								$result=$this->db->get()->result_array();
						$voucherforpost[$x]['totalvoucheractive'] = count($result);
						$this->db->select('bs_vouchers.Vr_Id');
						$this->db->from('bs_vouchers');
						$this->db->where('Vr_Status' , 'purchased');
						$this->db->where('Vr_post_id' , $voucherforpost[0]['Vp_post_id']);
						$result1=$this->db->get()->result_array();
						$voucherforpost[$x]['totalvoucherpurchased'] = count($result1);
						$this->db->select('bs_vouchers.Vr_Id');
						$this->db->from('bs_vouchers');
						$this->db->where('Vr_Status' , 'used');
						$this->db->where('Vr_post_id' , $voucherforpost[0]['Vp_post_id']);
						$result2=$this->db->get()->result_array();

						$voucherforpost[$x]['totalvoucherused'] = count($result2);

 					}


			$data['voucherofpost'] = $voucherforpost;
		if ((strcmp($option, 'detail') == 0) || (strcmp($option, 'updatedetail') == 0)) $this->load->view('groupinfo/group_detail', $data);
		else if ((strcmp($option, 'edit') == 0)) $this->load->view('groupinfo/group_edit', $data);
		else if ((strcmp($option, 'background') == 0) || (strcmp($option, 'changebackground') == 0)) $this->load->view('groupinfo/background', $data);
		$this->load->view('templates/footer', $data);


	}

	function getvoucherdetail(){

		$where = array(
			'Vr_post_id' =>$_POST['id'] ,
			'Vr_Voucher_Token' => $_POST['vouchercode']
		);
		 $this->db->select('bs_vouchers.* , bs_users.Us_Alias ,
		 bs_users.Us_Name
		, bs_users.Us_FName
		, bs_users.Us_LName, 
		, bs_users.Us_Email
		, bs_users.Us_Phone')->from('bs_vouchers');
		$this->db->join('bs_users' , 'bs_users.Us_ID  = bs_vouchers.Vr_Us_ID ');
			$this->db->where($where);
		$res =  $this->db->get()->result_array();

		echo json_encode($res);
	}

	function makevoucherused(){


		$where = array(
			'Vr_post_id' =>$_POST['id'] ,
			'Vr_Voucher_Token' => $_POST['vouchercode']
		);
		$res =  $this->db->select('bs_vouchers.*')->from('bs_vouchers')->where($where)->get()->result_array();
 			if(count($res)){
				 if($res[0]['Vr_Status'] == 'purchased'){
					 $where = array(
						 'Vr_post_id' =>$_POST['id'] ,
						 'Vr_Voucher_Token' => $_POST['vouchercode']
					 );
					 $update = array(
						 'Vr_Status' => 'used',
						 'murchentname' => $_POST['murchentname'],
						 'branchcode' => $_POST['branchcode'],
						 'usedDate' =>   date('Y-m-d H:i:s')
					 );
					 $this->db->where($where);
					 $res = $this->db->update('bs_vouchers' , $update );
				 		if($res){
							$response = array(
								'action' => 'success',
								'message' => 'Voucher is used successfully'
							);
						}else{
							$response = array(
								'action' => 'error',
								'message' => 'something went wrong. try again'
							);
						}
				 }else{
					 $response = array(
						 'action' => 'error',
						 'message' => 'Voucher is not yet purchased'
					 );
				 }
			}else{
				$response = array(
					'action' => 'error',
					'message' => 'No Record Found'
				);
			}
		echo json_encode($response);

	}
	function getvoucherverifiaction(){


						 $where = array(
							 'Vr_post_id' =>$_POST['id'] ,
							 'Vr_Voucher_Token' => $_POST['vouchercode']
						 );
						$res =  $this->db->select('bs_vouchers.*')->from('bs_vouchers')->where($where)->get()->result_array();
						echo json_encode($res);
	}
	public function UpdateGroupDetail($id, $data)
	{

		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('txtTitle', 'Group Name', 'required');
		if ($this->form_validation->run() === FALSE) {

			$data['error'] = "Invalid!";
		} else {
			$UserID = $data['CurrUserID'];
			$selCategory = $this->input->post('selCategory');
			$txtNewCategory = $this->input->post('txtNewCategory');
			if ($txtNewCategory == null) $txtNewCategory = '';
			$selSubCategory = $this->input->post('selSubCategory');
			$txtNewSubCategory = $this->input->post('txtNewSubCategory');
			if ($txtNewSubCategory == null) $txtNewSubCategory = '';
			$txtTitle = $this->input->post('txtTitle');
			$txtDescription = $this->input->post('txtDescription');
			$radPrivacy = $this->input->post('radPrivacy');
			$keywords = $this->input->post('txtKeywords');
			$groupplan = $this->input->post('groupplan');
			$data['PrimaryID'] = $this->groups_model->Update($id, $txtTitle, $txtDescription, $selCategory, $txtNewCategory, $selSubCategory, $txtNewSubCategory, $radPrivacy, $keywords , $groupplan);
			$data['notification'] = "Group detail was updated successfully!";
			$this->check_and_save_new_keywords($keywords);
		}

		return $data;
	}

	function getPOST()
	{

	}
// 			function totalNotification(){
//
// 				$data = 		$this->notifications_model->get_UserNotification($this->LoggedInUser);
// 				echo	json_encode($data);
//
// 			}

	public function showGroupSettings($GroupID, $data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['groups_items'] = $this->groups_model->get_Group($GroupID);
		$data['category_items'] = $this->category_model->get_Category();
		$data['backgrounds_items'] = $this->settings_model->get_Background();
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();

		$data['members_items'] = $this->users_model->get_Members('group', $GroupID);
		$data = $this->setPermissions($data);
		$data['member_counter'] = count($this->groups_model->counterofMember($GroupID));
		$data['post_count'] = count($this->groups_model->counterofpost($GroupID));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/group_header', $data);
		//$this->load->view('account/group_settings', $data);
		$this->load->view('templates/footer', $data);

	}


	public function invite($Parent, $ParentID)
	{
		if (strcmp($Parent, 'group') == 0) {
			$this->viewgroup($ParentID, true);
		}

	}

	public function addinTodo()
	{

		$this->db->select('bs_notification.No_ID');
		$this->db->from('bs_notification');
		$this->db->where('No_To', $_POST['commentId']);
		$this->db->where('No_From', $this->LoggedInUser);
		$query = $this->db->get()->result_array();
		if (count($query) == 0) {
			$notif_type = "todo_post_notification";
			$content = "You have added comment in the Todo list";
			$res = $this->notifications_model->add_notification($notif_type, 'A new Todo is Added', $content, $this->LoggedInUser, 'user', $_POST['commentId'], 'user');
			if ($res) {
				echo json_encode("Comment is added in ToDo list");
			} else {
				echo json_encode("Comment not added in ToDo list");
			}
		} else {
			echo json_encode("Already added in the ToDo list");
		}
	}

	function pricing(){
		$data = array();
		$this->load->view('templates/header', $data);

		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/price/pricing.html', $data);
		$this->load->view('templates/footer', $data);
	}
	public function getUserDetailForComment()
	{

		$string = explode("@", $_POST['searchName'][0])[1];
		$res = $this->groups_model->getUserDetailForComment($_POST['groupId'], $string);

		print_r(json_encode($res));
	}
	function checkmutestatu($post_id , $userid , $groupid){
		$where = array(
			"Mu_Status" => 'active',
			"Mu_Po_ID" =>  $post_id,
			"Mu_Gr_ID" => $groupid,
			"Mu_Us_ID" => $userid
		);



		$muteinfo = $this->db->select('bs_post_mute.*')->from('bs_post_mute')->where($where)->get()->result_array();

		if(count($muteinfo) > 0){
			return true;
		}else{
			return false;
		}
	}

	function sendVoucherUser  (){
	$this->db->select('bs_vouchers.Vr_Us_ID');
	$this->db->from('bs_vouchers');
	$this->db->where('Vr_post_id' , $_POST['id']);


	$responsdent = $this->db->get()->result_array();
	
	 echo  json_encode($responsdent);
}
	public function view($option, $id, $catId = null, $subcatId = null, $inviteID = null)
	{



		$promotedPost = 0;

		if (strcmp($option, 'group') == 0) {
			$this->db->select('bs_groups.*');
			$this->db->from('bs_groups');
			$this->db->where('Gr_Slug', $id);
			$query = $this->db->get()->result_array();
			if (count($query) > 0) {
				$id = encode_id($query[0]['Gr_ID']);
			}
		}
		else if(strcmp($option, 'topic') == 0){
			$this->db->select('bs_topics.*');
			$this->db->from('bs_topics');
			$this->db->where('To_Slugs', $id);
			$query1 = $this->db->get()->result_array();
			if (count($query1) > 0) {
				$id = encode_id($query1[0]['To_ID']);
			}
		}
		else if(strcmp($option, 'post') == 0){
			$this->db->select('bs_posts.*');
			$this->db->from('bs_posts');
			$this->db->where('Po_Slug', $id);

			$query1 = $this->db->get()->result_array();

			if(count($query1) == 0){
				$this->db->select('bs_posts.*');
				$this->db->from('bs_posts');
				$this->db->where('Po_ID' , decode_id($id));

				$query1 = $this->db->get()->result_array();

			}


			if (count($query1) > 0) {
				$id = encode_id($query1[0]['Po_ID']);


				$promotepost = $this->posts_model->getPromotedPost($query1[0]['Po_ID']);

				if(count($promotepost) > 0){
					if(!isset($this->session->userdata['logged_in'])){
						$promotedPost = 1;
					}

				}
			}

		}


		if ($catId == 'member' || $catId == 'follower' || $catId == 'admin') {

			$data['inviteID'] = $catId;
			$catId = null;
		}



		if (!isset($this->session->userdata['logged_in']) && $promotedPost == 0) {
 			redirect('/pages/view/signup?getback='.$option.'&pageID='.$id);
			 //if session is not there, redirect to login page

		}
		$groupdatafor = array(
			'groupid' => $id
		);

		if (strcmp($option, 'group') == 0) {
		$data['prequestion'] = $this->groups_model->getSaveQuestion($groupdatafor);
			}
		$data['EncodedID'] = $id;
		$id = decode_id($id);
 		$data['ID'] = $id;

		$option_record = $this->get_record($option, $id);

		if (empty($option_record)) {
			redirect('/account/profile');
			return;
		}

		if(isset($this->session->userdata['logged_in']['bs_id'])){
			$data['CurrUserID'] =  ($this->session->userdata['logged_in']['bs_id']);
			$UserID = ($this->session->userdata['logged_in']['bs_id']);
		}



		$data['error'] = '';
		$data['txtSearch'] = $this->input->post('txtSearch');
		$data['catId'] = isset($catId) ? $catId : null;
		$data['subcatId'] = isset($subcatId) ? $subcatId : null;

		$data['title'] = $page_title = ucfirst($option);



		$SearchText = $this->input->post('txtSearch');

		if (strcmp($option, 'group') == 0) {
			$data['members_items'] = $this->users_model->get_Members($option, $id);
			$data['category_items'] = $this->get_category_by_type($option, $id);
			$data['member_counter'] = count($this->groups_model->counterofMember($id));
			$data['post_count'] = count($this->groups_model->counterofpost($id));
			$data['groups_items'] = $option_record;
            $data['promoted'] = $this->groups_model->get_promoted_group($id);
			if (strcasecmp($data['groups_items']['Gr_Privacy'], "public") != 0 && $this->LoggedInUser != $data['groups_items']['Gr_Us_ID']) {
				$count = $this->members_model->check_if_member_record_access_exist("group", $data['groups_items']['Gr_ID'], 0, 0, "active");
				$inviteCount = $this->members_model->check_if_member_record_access_exist("group", $data['groups_items']['Gr_ID'], 0, 0, "active", 1);

				if (($count == 0) && ($inviteCount == 0)) {

				}
			}

			$data['posts_items'] = $this->posts_model->get_GroupPosts($id);
			$data['member_info'] = $this->members_model->get_member_information($id, 'group');

			$data['is_followed'] = $this->follower_model->check_if_followed($data['groups_items']['Gr_ID'], 'group');
			$data['referrer'] = isset($this->session->userdata['referrerID']) ? $this->session->userdata['referrerID'] : 0;

			if (!empty($catId))
				$data['subcategory_items'] = $this->category_model->get_SubCategory($catId);

			$data = $this->setPermissions($data);

		}
		else if (strcmp($option, 'topic') == 0) {
			$data['members_items'] = $this->users_model->get_Members($option, $id);
			$data['category_items'] = $this->get_category_by_type($option, $id);
			$data['topics_items'] = $this->topics_model->get_Topic($id);
			$data['creator_info'] = $this->members_model->get_admin_record($data['topics_items']['To_Gr_ID'], $this->LoggedInUser);

			 $this->db->select('bs_groups.*');
			 $this->db->from('bs_groups');
			 $this->db->where('Gr_ID' , $data['topics_items']['To_Gr_ID'] );
			$query  = $this->db->get()->result_array();

			$data['topics_member'] = $this->topics_model->topics_member($id);

//				print_R(	$data['topics_member']);
//				exit;

			if (empty($data['topics_items'])) {
				redirect('/account/profile');
				return;

			} else if (strcasecmp($data['topics_items']['To_Privacy'], "public") != 0 && $this->LoggedInUser != $data['topics_items']['To_Us_ID']) {
				$count = $this->members_model->check_if_member_record_access_exist("topic", $data['topics_items']['To_Gr_ID'], $data['topics_items']['To_ID'], 0, "active");

				if (empty($count)) {
// 					redirect('/account/profile');
// 					return;
				}
			}


			$data['posts_items'] = $this->posts_model->get_TopicPosts($id);
			$data['policy_items'] = $this->policy_model->get_TopicPolicy($id);
			$data['subtopics_items'] = $this->topics_model->get_SubTopics($id);
			if ($data['topics_items']['To_Us_ID'] == $this->LoggedInUser) {

				$data['member_info'] = $this->members_model->get_member_information($data['topics_items']['To_Gr_ID'], 'group');
			} else {


				if($data['topics_items']['To_Privacy'] == 'public'){
					$data['member_info'] = $this->members_model->get_member_information($id, 'topic');
					if($data['member_info']['role'] == 'non_member'){
						$data['member_info']['role']  = 'member';
						$data['member_info']['status']  = 'member';
					}

				}else{
					$data['member_info'] = $this->members_model->get_member_information($id, 'topic');
				}

			}


			if($data['creator_info']['role'] == 'creator'){

				$data['member_info'] = $this->members_model->get_member_information($data['topics_items']['To_Gr_ID'], 'group');
				$data['userType'] = $data['member_info']['role'];

			}

     	  	$data['is_followed'] = $this->follower_model->check_if_followed($data['topics_items']['To_ID'], 'topic');
 			$SearchText = $this->input->post('txtSearch');
			$data['catId'] = isset($catId) ? $catId : null;
			$data['subcategory_model'] = $this->subcategory_model;
			if (is_numeric($catId)) {
				$data['subcategory_items'] = $this->category_model->get_SubCategory($catId);
				$data['user_subcategory_items'] = $this->posts_model->get_UserPostSubCategory($id, $catId);
				$data['posts_items'] = $this->posts_model->get_TopicPostsCategory($id, $catId);
			} else if (is_string($catId)) {
				$data['user_subcategory_items'] = $this->posts_model->get_UserPostSubCategory($id, $catId);
				$data['posts_items'] = array();
				$data['user_posts_items'] = $this->posts_model->get_UserTopicPostsCategory($id, $catId);
			}

			if ($SearchText != '') {
				if (is_numeric($catId)) {
					$data['posts_items'] = $this->posts_model->searchTopicPosts($id, $catId, $SearchText);
				} else {
					$data['posts_items'] = $this->posts_model->searchTopicPosts($id, null, $SearchText);
				}
			}

			$data['showjoin'] = $this->users_model->check_Members('topic', $data['topics_items']['To_Gr_ID'], $id, 0, $UserID);


			$data = $this->setPermissions($data);
		}
		else if (strcmp($option, 'post') == 0) {
                $data['selMenu'] = "Messages";
				$data['selSubMenu'] = "Detail";
				$data['posts_item'] = $this->posts_model->get_Post($id);

				if($promotedPost == 1){

            $promotedlink = base_url()."invitepostLink/".$data['posts_item']['Po_ID']."/".$data['posts_item']['Po_Slug']."/".$data['posts_item']['Po_Us_ID']."/member";
            redirect($promotedlink);
 			exit;
		 }

			$promotedPostitem = 	$this->posts_model->get_promoted_post_data($id);
 			$data['promoted']   = count($promotedPostitem) > 0 ? 1 : 0;

			$data['promoteddata']  = $promotedPostitem;
			$respons = $this->posts_model->postreward($data['posts_item']['Po_reward']);
                    $data['rewarddetails'] = $respons;
					 if(isset($this->LoggedInUser)){
								$data['mutedata'] = $this->checkmutestatu( $id, $this->LoggedInUser , $data['posts_item']['Po_Gr_ID']) ? 1 : 0;
                      }else{
								$data['mutedata'] = 0;
                      }
					 $result_of_update = $this->posts_model->resultupdate($id);
				$data['notnumber'] = 1;
				$data['lastUpdate'] = $result_of_update;
				$data['userPostLike'] = $this->users_model->getuserLike($data['posts_item']['Po_Us_ID'], $id);
				$data['userfav'] = $this->users_model->getuserfav($data['posts_item']['Po_Us_ID'], $id);
				if($data['posts_item']['Po_Privacy'] == "public"){
				$data['member_counter'] = count($this->groups_model->counterofMember($data['posts_item']['Po_Gr_ID']));

			}else{
				$data['member_counter'] = count($this->groups_model->counterofMember($id , 'post'));
				$data['member_counter'] = $data['member_counter'] +1;
			}


				$data['post_count'] = count($this->groups_model->counterofpost($data['posts_item']['Po_Gr_ID']));

 		$data['user_data'] = $this->users_model->get_Users($data['posts_item']['Po_Us_ID']);

				if(isset($this->LoggedInUser)){
 			$data['getContect'] = $this->users_model->getContect($this->LoggedInUser);
				}

				$data['posts_files'] = $this->medias_model->get_PostMedia($id, 'files');
 				$data['posts_images'] = $this->medias_model->get_PostMedia($id, 'images');
				if(isset($this->LoggedInUser)){
					$data['creator_info'] = $this->members_model->get_admin_record($data['posts_item']['Po_Gr_ID'], $this->LoggedInUser);

				}


				$data['is_followed'] = $this->follower_model->check_if_followed($data['posts_item']['Po_ID'], 'post');
				if ($data['posts_item'] == null) {
					redirect('/account/profile');
					return;
				} else if (strcasecmp($data['posts_item']['Po_Privacy'], "public") != 0 && $this->LoggedInUser != $data['posts_item']['Po_Us_ID']) {

					$count = $this->members_model->check_if_member_record_access_exist("post", $data['posts_item']['Po_Gr_ID'], '', $data['posts_item']['Po_ID'], "active");

					if (empty($count)) {
	// 					redirect('/account/profile');
	// 					return;
					}
				}


				$general_Count_postx = $this->members_model->check_if_member_record_access_exist("post", $data['posts_item']['Po_Gr_ID'], '', $data['posts_item']['Po_ID'], "pending");
			    $data['general_Count'] = $general_Count_postx;
				$general_Count_postx += $this->members_model->check_if_member_record_access_exist("post", $data['posts_item']['Po_Gr_ID'], '', $data['posts_item']['Po_ID'], "active");
				$data['general_Count_postx'] = $general_Count_postx;



				$data['allpacakges'] = $this->posts_model->allpackages();

			   $data['button_items'] = $this->buttons_model->get_record();

				if (isset($this->LoggedInUser) && $this->LoggedInUser == $data['posts_item']['Po_Us_ID']) {

					$data['member_info_post'] = $this->members_model->get_member_information_admin($id, $data['posts_item']['Po_Gr_ID']);
					$data['userType'] = $data['member_info_post']['role'];

				} else {

					if (strcasecmp($data['posts_item']['Po_Privacy'], "public") != 0 && $this->LoggedInUser != $data['posts_item']['Po_Us_ID']) {
						$data['member_info_post'] = $this->members_model->get_member_POST_info($id, $data['posts_item']['Po_Gr_ID'], $data['posts_item']['Po_ID']);
						$data['userType'] = $data['member_info_post']['role'];
					} else {
						$data['member_info_post'] = $this->members_model->get_member_information_POST($id, $data['posts_item']['Po_Gr_ID']);
						$data['userType'] = $data['member_info_post']['role'];
				    }
				}


				if($data['posts_item']['Po_Privacy'] == 'public'){
					$data['member_info_post'] = $this->members_model->get_member_information($data['posts_item']['Po_Gr_ID'], 'group');
					$data['userType'] = $data['member_info_post']['role'];
				}
				if(isset($data['creator_info']) && $data['creator_info']['role'] == 'creator'){

						$data['member_info_post'] = $this->members_model->get_member_information($data['posts_item']['Po_Gr_ID'], 'group');
						$data['userType'] = $data['member_info_post']['role'];

 				    }
                if(isset($this->LoggedInUser)) {
				$data['topics_items'] = $this->topics_model->get_GroupTopics($data['posts_item']["Gr_ID"]);
				$data['user_data'] = $this->users_model->get_Users($UserID);
			}

				$data['comments_items'] = $this->comments_model->get_Comments($id);
				$data['backcolors_items'] = $this->settings_model->get_Backcolor();
				$data['backgrounds_items'] = $this->settings_model->get_Background();


				$data['ShowAdminAction'] = "d-none";

				$data['polldata'] =	$this->posts_model->allpollrecord($id);
				$data['totalreview'] =  array();
				$data['reviwer'] =  array();
			if(isset($data['polldata'][0]['PL_answers']) ){
				$array = explode('^', $data['polldata'][0]['PL_answers']);
			 		foreach ($array as $roww){
						 $this->db->select('bs_poll_ans.* , bs_users.*');
						$this->db->from('bs_poll_ans');
						$this->db->join('bs_users' , 'bs_poll_ans.Poa_Us_ID = bs_users.Us_ID' );
						 $this->db->where('Poa_ans' , $roww);
						$this->db->where('Poa_pl_id' , $data['polldata'][0]['Pl_ID']);
						$resss = $this->db->get()->result_array();
										$datas =  array($roww => count($resss));
										$datas1 =  array($roww =>  $resss);

 					array_push(	$data['totalreview'] , $datas);
					 array_push(	$data['reviwer'] , $datas1);
					}
					$data['totaldata'] = 0;

					 foreach ($data['totalreview'] as $val){
						 foreach ($val as $key => $vai){
							 $data['totaldata'] += $vai;
						 }
					 }
			}

			 $data['polldatacount'] =$this->posts_model->polluseraccount($id);
			 $data['polluserDetail'] =	$this->posts_model->polluserDetails($id);
		}

		if (strcmp($UserID, $option_record[substr($page_title, 0, 2) . '_Us_ID']) == 0) $data['ShowAdminAction'] = "";


		$data['asset'] = 'account/view/' . $option;
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/' . $option . '_header', $data);


		if (isset($data['posts_item']['Po_Us_ID']) && $this->LoggedInUser == $data['posts_item']['Po_Us_ID']) {
			$count = 1;
		}

		if(isset($data['creator_info'])  && $data['creator_info']['role'] == 'creator') {
			$count = 1;
		}

		if (isset($count) && $count == 0) {

			$this->load->view('templates/footer', $data);
		}
		else {
			if (strcmp($option, 'post') != 0) {


				$this->load->view('templates/' . $option . '_menu', $data);


			}

			if (strcmp($option, 'topic') == 0   && $data['member_info']['role'] == 'non_member' ) {
 				if($query[0]['Gr_Privacy'] == 'public'){
							$this->load->view('account/' . $option, $data);

 				}


			}else{
				$this->load->view('account/' . $option, $data);
			}


			$this->load->view('templates/footer', $data);
		}

	}

	public  function  end_promoted_post(){

		$where = array(
			'Pp_Post_ID'	=> $_POST['PostId']
		);
		$this->db->select('promoted_post.*');
		$this->db->from('promoted_post');
		$this->db->where($where);
		$res = 	$this->db->get()->result_array();

		if(count($res) > 0){
			    $this -> db -> where('Pp_Post_ID', $_POST['PostId']);
    			$res = $this -> db -> delete('promoted_post');
				if($res){
					echo '1';
				}else{
					echo '0';
				}
		}else{
			echo '0';
		}
	}
	public function postinfopromote($postid, $slug, $referrer = NULL, $role = NULL)
	{

		$promotedPost  = 0;
		$this->db->select('bs_posts.Po_ID');
		$this->db->from('bs_posts');
		$this->db->where('Po_Slug', $slug);
		$query1 = $this->db->get()->result_array();


		if(count($query1) == 0){
			$this->db->select('bs_posts.Po_ID ');
			$this->db->from('bs_posts');
			$this->db->where('Po_ID' , decode_id($slug));

			$query1 = $this->db->get()->result_array();

		}
		if (count($query1) > 0) {
			$id = encode_id($query1[0]['Po_ID']);
 			$promotepost =  $this->posts_model->getPromotedPost($query1[0]['Po_ID']);

			if(count($promotepost) > 0){
				$promotedPost = 1;
			}
		}

		$data['promotedPost'] = $promotedPost;

		$this->session->set_flashdata('registerType', $role);
		// After that you need to used redirect function instead of load view such as


		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->session->set_userdata('post_referrerID', $referrer);
			$this->session->set_userdata('post_role', $role);
			redirect('/account/view/post/' . encode_id($postid) . '/' . $role);
		}

		$data['EncodedID'] = encode_id($postid);

		$data['ID'] = $postid;
		$data['role'] = '';
		$data['referrer'] = $referrer;
		if (isset($role)) {
			if ($role == 'follower') {
				$data['role'] = 4;
			} else if ($role == 'member') {
				$data['role'] = 3;
			} else if ($role == 'admin') {
				$data['role'] = 2;
			} else if ($role == 'superadmin') {
				$data['role'] = 1;
			}
		}


		$data['posts_item'] = $this->posts_model->get_Post($postid);
		if ($data['posts_item'] == null) {
			redirect('/account/profile');
			return;
		}


		$data['userPostLike'] = $this->users_model->getuserLike($data['posts_item']['Po_Us_ID'], $postid);
		$data['userfav'] = $this->users_model->getuserfav($data['posts_item']['Po_Us_ID'], $postid);

		$data['member_counter'] = count($this->groups_model->counterofMember($data['posts_item']['Po_Gr_ID']));

		$data['user_data'] = $this->users_model->get_Users($data['posts_item']['Po_Us_ID']);

		$data['posts_files'] = $this->medias_model->get_PostMedia($postid, 'files');
		$data['posts_images'] = $this->medias_model->get_PostMedia($postid, 'images');
		$data['member_info'] = $this->members_model->get_member_information($postid, 'group');
		$data['is_followed'] = $this->follower_model->check_if_followed($data['posts_item']['Po_ID'], 'post');


		$data['member_info_post'] = $this->members_model->get_member_information_POST($postid, $data['posts_item']['Po_Gr_ID']);
		$data['userType'] = $data['member_info_post']['role'];

		$data['comments_items'] = $this->comments_model->get_Comments($postid);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();



		$data['ShowAdminAction'] = "d-none";
		$data['asset'] = 'account/view/post';
//		print_r(	$data['promotedPost']);
//		exit;
		$this->load->view('templates/header', $data);
		$this->load->view('templates/info_navigation', $data);
		$this->load->view('templates/post_header', $data);
		if($data['promotedPost'] == 1){
			$this->load->view('account/post' , $data);
		}

		$this->load->view('templates/footer', $data);


	}

	function savemessageofpoll(){

		if(isset($_POST['remove'])){

			$where = array(
				'Pl_ID' => $_POST['pollid']
			);

			$update = array(
				'Pl_message' => ''
			);

			$this->db->where($where);

			$res = $this->db->update('bs_posts_poll' , $update );

			echo $res;

		}else{
			$where = array(
				'Pl_ID' => $_POST['pollid']
			);
			$update = array(
				'Pl_message' => $_POST['message']
			);
			$this->db->where($where);
			$res = $this->db->update('bs_posts_poll' , $update );
			echo $res;
		}

	}
		public function getallmembers(){
			if($_POST['Co_Po_type'] == 'public') {
				$parent = 'group';
			}else{
				$parent = 'post';
			}


					$this->db->select('bs_members.* , bs_users.*');
					$this->db->from('bs_members');
					$this->db->join('bs_users' , 'bs_users.Us_ID = bs_members.Me_Us_ID ');
					$this->db->where('bs_members.Me_Gr_ID' , $_POST['Co_Gr_ID']);
					$this->db->where('bs_members.Me_Parent' ,$parent);
					$this->db->where('bs_members.Me_status' ,'active');
			 		$response = 	$this->db->get()->result_array();


					 echo json_encode($response);



 		 }
	public function checkuseralreadymemberforTopic()
	{
		$this->db->select('bs_topics.*');
		$this->db->from('bs_topics');
		$this->db->where('To_ID', $_POST['topic_id']);
		$query1 = $this->db->get()->result_array();

		if (count($query1) > 0) {

			$this->db->select('bs_members.*');
			$this->db->from('bs_members');
			$this->db->where('Me_Gr_ID', $query1[0]['To_Gr_ID']);
			$this->db->where('Me_Parent', 'group');
			$this->db->where('Me_Us_ID', $this->LoggedInUser);
			$query = $this->db->get()->result_array();


			$info = $this->members_model->get_member_information($query1[0]['To_Gr_ID'], 'group', $this->LoggedInUser);
			$group = $this->groups_model->get_Record($query1[0]['To_Gr_ID']);
			$user = $this->users_model->get_Users($this->LoggedInUser);
			$referrer = $this->users_model->get_Users($_POST['topic_referrer']);
			$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';
			$linkmadeofgroup = "<a href='" . base_url() . "account/view/group/" . encode_id($query1[0]['To_Gr_ID']) . "'>";

			if (count($query) == 0) {
				$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'group', $_POST['topic_role'], $status, $this->LoggedInUser, 1, 1, $_POST['topic_referrer'], 0, $_POST['topic_id']);
				if ($resp) {
					$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'topic', $_POST['topic_role'], 'pending', $this->LoggedInUser, 1, 1, $_POST['topic_referrer'], 0, $_POST['topic_id']);
					$notif_type = "topic_join_via_link";
					$linkmade = "<a href='" . base_url() . "account/view/topic/" . encode_id($_POST['topic_id']) . "'>";
					$content = " joined private Topic " . $linkmade . $query1[0]['To_Name'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> via invite link shared by  <a href=" . base_url() . "u/" . encode_id($referrer['Us_ID']) . ">" . $referrer['Us_Name'] . "</a>";
					$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['To_Name'] . ' post via share link', $content, $this->LoggedInUser, $resp, $query1[0]['To_Gr_ID'], 'group');
					if ($status == 'pending') {
						$response = array('status' => 'success', 'message' => 'You joined private topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the Topic details. ', 'id' => encode_id($_POST['topic_id']), 'type' => 'topic');

					} else {
						$response = array('status' => 'success', 'message' => 'You joined private topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the Topic details. ', 'id' => encode_id($_POST['topic_id']), 'type' => 'topic');

					}
				}
			} else {

				$data['general_Count'] = $this->members_model->check_if_member_record_access_exist("topic", $query1[0]['To_Gr_ID'], '', $_POST['topic_id'], "pending");
				$data['general_Count'] += $this->members_model->check_if_member_record_access_exist("topic", $query1[0]['To_Gr_ID'], '', $_POST['topic_id'], "active");

				if ($data['general_Count'] > 0) {
					$response = array('status' => 'success', 'message' => 'Your request to join private Topic ' . strtoupper($query1[0]['To_Name']) . ' under ' . strtoupper($group['Gr_Name']) . ' has been approved.', 'id' => encode_id($_POST['topic_id']), 'type' => 'topic');

				} else {
					$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'topic', $_POST['topic_role'], 'pending', $this->LoggedInUser, 1, 1, $_POST['topic_referrer'], 0, $_POST['topic_id']);
					$notif_type = "topic_join_via_link";
					$linkmade = "<a href='" . base_url() . "account/view/topic/" . encode_id($_POST['topic_id']) . "'>";
					$content = " joined private topic " . $linkmade . $query1[0]['To_Name'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> via invite link shared by  <a href=" . base_url() . "u/" . encode_id($referrer['Us_ID']) . ">" . $referrer['Us_Name'] . "</a>";
					$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['To_Name'] . ' topic via share link', $content, $this->LoggedInUser, $resp, $query1[0]['To_Gr_ID'], 'group');
					if ($query[0]['Me_Status'] == 'pending') {
						$response = array('status' => 'success', 'message' => 'You joined private topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the Topic details. ', 'id' => encode_id($_POST['topic_id']), 'type' => 'topic');
					} else {
						$response = array('status' => 'success', 'message' => 'You joined private topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the Topic details. ', 'id' => encode_id($_POST['topic_id']), 'type' => 'topic');
					}

				}


			}


		} else {
			$response = array('status' => 'error', 'message' => 'Post is not availble');

		}
		echo json_encode($response);
	}

			function unmutesingleuser(){



		 if (isset($this->session->userdata['logged_in']['bs_mutelimit'])) {
					$totalrecord = array();
					$this->db->select('bs_posts.*');
					$this->db->from('bs_posts');
					$this->db->where('bs_posts.Po_Us_ID' , $this->LoggedInUser);
					$query =	$this->db->get()->result_array();
					foreach($query as $prev){

						$this->db->select('DISTINCT(bs_post_mute.Mu_Po_ID)')->where('Mu_Po_ID' ,$prev['Po_ID']);
						$this->db->distinct();
						$checkrecord = 	$this->db->get('bs_post_mute')->result_array();


						if(count($checkrecord) > 0){
							array_push($totalrecord , $checkrecord[0]['Mu_Po_ID']);
						}
					}
					$found = false;
					foreach ($totalrecord as $singleid){
						if($singleid == $_POST['postalid']){
							$found = true;
						}
					}
					if(count($totalrecord) > 0){

						if($found  ){

						}else{
							if($this->session->userdata['logged_in']['bs_mutelimit'] > count($totalrecord) ){

							}	else{
								$res = 2;
								echo $res;
								exit;
							}
						}
					}
				}
				$posts_item  = $this->posts_model->get_Post($_POST['postalid']);
				$_POST['mutetype']  = 0;
				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_ID' , $_POST['postid']);
				$query = 	$this->db->get()->result_array();




				$dataa = array();
				array_push($dataa , 	$query[0]['Me_Us_ID']);

				foreach ($dataa  as $me_id) {

					$where= array( "Mu_Po_ID" =>  $posts_item['Po_ID'],
						"Mu_Gr_ID" => $posts_item['Gr_ID'],
						"Mu_Us_ID" => $me_id

					);

					$query = $this->db->select('bs_post_mute.*')->from('bs_post_mute')->where($where)->get()->result_array();


					if(count($query) > 0 ){

						if( $_POST['mutetype'] == 1){
							if($query[0]['Mu_Status'] == 'unactive'){

								$update = array(
									"Mu_Status" => 'active'

								);

								$this->db->where($where);
								$res = 	$this->db->update('bs_post_mute' ,	$update );

							}


						}else if( $_POST['mutetype'] == 0){
							if($query[0]['Mu_Status'] == 'active'){

								$update = array(
									"Mu_Status" => 'unactive'

								);

								$this->db->where($where);
								$res= 	$this->db->update('bs_post_mute' ,	$update );

							}
						}

					}else{
						if( $_POST['mutetype'] == 1){


							$update = array(
								"Mu_Status" => 'active',
								"Mu_Po_ID" =>  $posts_item['Po_ID'],
								"Mu_Gr_ID" => $posts_item['Gr_ID'],
								"Mu_Us_ID" => $me_id

							);


							$res =	$this->db->insert('bs_post_mute' ,	$update );

						}else if( $_POST['mutetype'] == 0){
							$update = array(
								"Mu_Status" => 'unactive',
								"Mu_Po_ID" =>  $posts_item['Po_ID'],
								"Mu_Gr_ID" => $posts_item['Gr_ID'],
								"Mu_Us_ID" => $me_id
							);
							$res =	$this->db->insert('bs_post_mute' ,	$update );


						}
					}
				}


				echo $res ;
			}
			function muresingleuser(){

				;				if (isset($this->session->userdata['logged_in']['bs_mutelimit'])) {
					$totalrecord = array();
					$this->db->select('bs_posts.*');
					$this->db->from('bs_posts');
					$this->db->where('bs_posts.Po_Us_ID' , $this->LoggedInUser);
					$query =	$this->db->get()->result_array();
					foreach($query as $prev){

						$this->db->select('DISTINCT(bs_post_mute.Mu_Po_ID)')->where('Mu_Po_ID' ,$prev['Po_ID']);
						$this->db->distinct();
						$checkrecord = 	$this->db->get('bs_post_mute')->result_array();


						if(count($checkrecord) > 0){
							array_push($totalrecord , $checkrecord[0]['Mu_Po_ID']);
						}
					}
					$found = false;
					foreach ($totalrecord as $singleid){
						if($singleid == $_POST['postalid']){
							$found = true;
						}
					}
					if(count($totalrecord) > 0){

						if($found  ){

						}else{
							if($this->session->userdata['logged_in']['bs_mutelimit'] > count($totalrecord) ){

							}	else{
								$res = 2;
								echo $res;
								exit;
							}
						}
					}
				}
				$posts_item  = $this->posts_model->get_Post($_POST['postalid']);
				$_POST['mutetype']  = 1;
		  $this->db->select('bs_members.*');
		   $this->db->from('bs_members');
		   $this->db->where('Me_ID' , $_POST['postid']);
			$query = 	$this->db->get()->result_array();




				$dataa = array();
				array_push($dataa , 	$query[0]['Me_Us_ID']);

				foreach ($dataa  as $me_id) {

					$where= array( "Mu_Po_ID" =>  $posts_item['Po_ID'],
						"Mu_Gr_ID" => $posts_item['Gr_ID'],
						"Mu_Us_ID" => $me_id

					);

					$query = $this->db->select('bs_post_mute.*')->from('bs_post_mute')->where($where)->get()->result_array();


					if(count($query) > 0 ){

						if( $_POST['mutetype'] == 1){
							if($query[0]['Mu_Status'] == 'unactive'){

								$update = array(
									"Mu_Status" => 'active'

								);

								$this->db->where($where);
								$res = 	$this->db->update('bs_post_mute' ,	$update );

							}


						}else if( $_POST['mutetype'] == 0){
							if($query[0]['Mu_Status'] == 'active'){

								$update = array(
									"Mu_Status" => 'unactive'

								);

								$this->db->where($where);
								$res= 	$this->db->update('bs_post_mute' ,	$update );

							}
						}

					}else{
						if( $_POST['mutetype'] == 1){


							$update = array(
								"Mu_Status" => 'active',
								"Mu_Po_ID" =>  $posts_item['Po_ID'],
								"Mu_Gr_ID" => $posts_item['Gr_ID'],
								"Mu_Us_ID" => $me_id

							);


							$res =	$this->db->insert('bs_post_mute' ,	$update );

						}else if( $_POST['mutetype'] == 0){
							$update = array(
								"Mu_Status" => 'unactive',
								"Mu_Po_ID" =>  $posts_item['Po_ID'],
								"Mu_Gr_ID" => $posts_item['Gr_ID'],
								"Mu_Us_ID" => $me_id
							);
							$res =	$this->db->insert('bs_post_mute' ,	$update );


						}
					}
				}


				echo $res ;
			}
			function murealluser(){


;				if (isset($this->session->userdata['logged_in']['bs_mutelimit'])) {
						$totalrecord = array();
					$this->db->select('bs_posts.*');
					$this->db->from('bs_posts');
					$this->db->where('bs_posts.Po_Us_ID' , $this->LoggedInUser);
							$query =	$this->db->get()->result_array();
							foreach($query as $prev){

							$this->db->select('DISTINCT(bs_post_mute.Mu_Po_ID)')->where('Mu_Po_ID' ,$prev['Po_ID']);
								$this->db->distinct();
							 $checkrecord = 	$this->db->get('bs_post_mute')->result_array();

 									if(count($checkrecord) > 0){
								array_push($totalrecord , $checkrecord[0]['Mu_Po_ID']);
								}
							}
									$found = false;
								foreach ($totalrecord as $singleid){
									if($singleid == decode_id($_POST['postid'])){
										$found = true;
									}
								}
								if(count($totalrecord) > 0){

									if($found  ){

									}else{
										if($this->session->userdata['logged_in']['bs_mutelimit'] > count($totalrecord) ){
								
									}	else{
											$res = 2;
											echo $res;
											exit;
										}
									}
								}
 					}

			if($_POST['role'] == 'super_admin'){
				$_POST['role'] = 'superadmin';
			}

				$posts_item  = $this->posts_model->get_Post(decode_id($_POST['postid']));
				if($posts_item['Po_Privacy'] == 'private'){

					$this->db->select('bs_members.Me_Us_ID');
					$this->db->from('bs_members');
					$this->db->where('bs_members.Me_St_ID' , decode_id($_POST['postid']) );
					$this->db->where('bs_members.Me_Us_ID !=' ,  $this->LoggedInUser );
					$this->db->where('bs_members.Me_parent ' ,  'post');
					if($_POST['role'] != 'all_members'){

						$this->db->where('bs_members.Me_Role' , $_POST['role']);
					}

					$member_mute = 	$this->db->get()->result_array();

				   $dataa = array();
					foreach ($member_mute  as $mute){
					 array_push($dataa , $mute['Me_Us_ID']);
					}


				}else{

					$this->db->select('bs_members.Me_Us_ID');
					$this->db->from('bs_members');
					$this->db->where('bs_members.Me_Gr_ID' , $posts_item['Gr_ID']  );
					$this->db->where('bs_members.Me_Us_ID !=' ,  $this->LoggedInUser );
					$this->db->where('bs_members.Me_parent ' ,  'group');
					if($_POST['role'] != 'all_members'){
						$this->db->where('bs_members.Me_Role' , $_POST['role']);
					}
					$member_mute = 	$this->db->get()->result_array();

					$dataa = array();
					foreach ($member_mute  as $mute){
						array_push($dataa , $mute['Me_Us_ID']);
					}


				}
				foreach ($dataa  as $me_id) {

				$where= array( "Mu_Po_ID" =>  $posts_item['Po_ID'],
								"Mu_Gr_ID" => $posts_item['Gr_ID'],
								"Mu_Us_ID" => $me_id

				);

				$query = $this->db->select('bs_post_mute.*')->from('bs_post_mute')->where($where)->get()->result_array();



					if(count($query) > 0 ){

						if( $_POST['mutetype'] == 1){
							if($query[0]['Mu_Status'] == 'unactive'){

								$update = array(
									"Mu_Status" => 'active'

								);

								$this->db->where($where);
								$res = 	$this->db->update('bs_post_mute' ,	$update );

							}


						}else if( $_POST['mutetype'] == 0){
							if($query[0]['Mu_Status'] == 'active'){

								$update = array(
									"Mu_Status" => 'unactive'

								);

								$this->db->where($where);
								$res= 	$this->db->update('bs_post_mute' ,	$update );

							}
						}

					}else{
						if( $_POST['mutetype'] == 1){


								$update = array(
								"Mu_Status" => 'active',
								"Mu_Po_ID" =>  $posts_item['Po_ID'],
								"Mu_Gr_ID" => $posts_item['Gr_ID'],
								"Mu_Us_ID" => $me_id

								);


							$res =	$this->db->insert('bs_post_mute' ,	$update );

						}else if( $_POST['mutetype'] == 0){
								$update = array(
								"Mu_Status" => 'unactive',
								"Mu_Po_ID" =>  $posts_item['Po_ID'],
								"Mu_Gr_ID" => $posts_item['Gr_ID'],
								"Mu_Us_ID" => $me_id
								);
							$res =	$this->db->insert('bs_post_mute' ,	$update );


						}
					}
				}


					echo $res ;
			}

			function checkquestionofgroup(){

			$this->db->select('bs_groups.group_Question');
			$this->db->from('bs_groups');
			$this->db->where('Gr_ID' , $_POST['groupid']);
				$res = 	$this->db->get()->result_array();

			if(!empty($res[0]['group_Question'])){
				$this->db->select('bs_group_answer.*');
				$this->db->from('bs_group_answer');
				$this->db->where('ga_Us_ID' ,$this->LoggedInUser);
				$this->db->where('ga_gr_ID' , $_POST['groupid']);
				$res1 = 	$this->db->get()->result_array();
				if(count($res1) > 0){
					echo json_encode('allowed');
				}else{
					echo json_encode('notallowed');
				}
			}else{
				echo json_encode('allowed');
			}

			}
	public function checkuseralreadymemberforPOST()
	{

		$this->db->select('bs_posts.*');
		$this->db->from('bs_posts');
		$this->db->where('Po_ID', $_POST['post_id']);
		$query1 = $this->db->get()->result_array();


		if (isset($_POST['join_type']) && $_POST['join_type'] == 'group_direct_join') {
			if (count($query1) > 0) {
				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_Gr_ID', $query1[0]['Po_Gr_ID']);
				$this->db->where('Me_Us_ID', $this->LoggedInUser);
				$query = $this->db->get()->result_array();


				$group = $this->groups_model->get_Record($query1[0]['Po_Gr_ID']);
				$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';
				$linkmadeofgroup = "<a href='" . base_url() . "account/view/group/" . encode_id($query1[0]['Po_Gr_ID']) . "'>";
				if (count($query) == 0) {
					$resp = $this->members_model->add_Member($query1[0]['Po_Gr_ID'], 'group', $_POST['post_role'], $status, $this->LoggedInUser, 0, 0, 0, $_POST['post_id']);
					if ($resp) {
						$resp = $this->members_model->add_Member($query1[0]['Po_Gr_ID'], 'post', $_POST['post_role'], 'pending', $this->LoggedInUser, 0, 0, 0, $_POST['post_id']);
						$notif_type = "post_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/post/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined private post " . $linkmade . $query1[0]['Po_Title'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a>";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['Po_Title'] . ' post  ', $content, $this->LoggedInUser, $resp, $query1[0]['Po_Gr_ID'], 'group');
						if ($status == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

						} else {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

						}
					}
				} else {
					$data['general_Count'] = $this->members_model->check_if_member_record_access_exist("post", $query1[0]['Po_Gr_ID'], '', $_POST['post_id'], "pending");
					$data['general_Count'] += $this->members_model->check_if_member_record_access_exist("post", $query1[0]['Po_Gr_ID'], '', $_POST['post_id'], "active");

					if ($data['general_Count'] > 0) {
						$response = array('status' => 'success', 'message' => 'Your request to join private post ' . strtoupper($query1[0]['Po_Title']) . ' under ' . strtoupper($group['Gr_Name']) . ' has been approved.', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

					} else {
						$resp = $this->members_model->add_Member($query1[0]['Po_Gr_ID'], 'post', $_POST['post_role'], 'pending', $this->LoggedInUser, 0, 0, 0, $_POST['post_id']);
						$notif_type = "post_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/post/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined private post " . $linkmade . $query1[0]['Po_Title'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> ";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['Po_Title'] . ' post ', $content, $this->LoggedInUser, $resp, $query1[0]['Po_Gr_ID'], 'group');
						if ($query[0]['Me_Status'] == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						} else {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						}

					}


				}


			} else {
				$response = array('status' => 'error', 'message' => 'Post is not availble');
			}

		} else {


			if (count($query1) > 0) {

				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_Gr_ID', $query1[0]['Po_Gr_ID']);
				$this->db->where('Me_Us_ID', $this->LoggedInUser);
				$query = $this->db->get()->result_array();
				$info = $this->members_model->get_member_information($query1[0]['Po_Gr_ID'], 'group', $this->LoggedInUser);
				$group = $this->groups_model->get_Record($query1[0]['Po_Gr_ID']);
				$user = $this->users_model->get_Users($this->LoggedInUser);
				$referrer = $this->users_model->get_Users($_POST['post_referrer']);
				$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';
				$linkmadeofgroup = "<a href='" . base_url() . "account/view/group/" . encode_id($query1[0]['Po_Gr_ID']) . "'>";

				if (count($query) == 0) {
					$resp = $this->members_model->add_Member($query1[0]['Po_Gr_ID'], 'group', $_POST['post_role'], $status, $this->LoggedInUser, 1, 1, $_POST['post_referrer'], $_POST['post_id']);
					if ($resp) {
						$resp = $this->members_model->add_Member($query1[0]['Po_Gr_ID'], 'post', $_POST['post_role'], 'pending', $this->LoggedInUser, 1, 1, $_POST['post_referrer'], $_POST['post_id']);
						$notif_type = "post_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/post/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined private post " . $linkmade . $query1[0]['Po_Title'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> via invite link shared by  <a href=" . base_url() . "u/" . encode_id($referrer['Us_ID']) . ">" . $referrer['Us_Name'] . "</a>";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['Po_Title'] . ' post via share link', $content, $this->LoggedInUser, $resp, $query1[0]['Po_Gr_ID'], 'group');
						if ($status == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

						} else {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

						}
					}
				} else {


					$data['general_Count'] = $this->members_model->check_if_member_record_access_exist("post", $query1[0]['Po_Gr_ID'], '', $_POST['post_id'], "pending");
					$data['general_Count'] += $this->members_model->check_if_member_record_access_exist("post", $query1[0]['Po_Gr_ID'], '', $_POST['post_id'], "active");

					if ($data['general_Count'] > 0) {
						$response = array('status' => 'error', 'message' => 'Your request to join private post ' . strtoupper($query1[0]['Po_Title']) . ' under ' . strtoupper($group['Gr_Name']) . ' has been approved.');

					} else {
						$resp = $this->members_model->add_Member($query1[0]['Po_Gr_ID'], 'post', $_POST['post_role'], 'pending', $this->LoggedInUser, 1, 1, $_POST['post_referrer'], $_POST['post_id']);
						$notif_type = "post_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/post/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined private post " . $linkmade . $query1[0]['Po_Title'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> via invite link shared by  <a href=" . base_url() . "u/" . encode_id($referrer['Us_ID']) . ">" . $referrer['Us_Name'] . "</a>";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['Po_Title'] . ' post via share link', $content, $this->LoggedInUser, $resp, $query1[0]['Po_Gr_ID'], 'group');
						if ($query[0]['Me_Status'] == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						} else {
							$response = array('status' => 'success', 'message' => 'You joined private post ' . strtoupper($query1[0]['Po_Title']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						}

					}


				}


			} else {
				$response = array('status' => 'error', 'message' => 'Post is not availble');

			}

		}


		echo json_encode($response);

	}

	public function checkuseralreadymemberfotopic()
	{

		$_POST['post_id'] = decode_id($_POST['post_id']);


		$this->db->select('bs_topics.*');
		$this->db->from('bs_topics');
		$this->db->where('To_ID', $_POST['post_id']);
		$query1 = $this->db->get()->result_array();

//				print_r($query1);
//				exit;

		if (isset($_POST['join_type']) && $_POST['join_type'] == 'group_direct_join') {
			if (count($query1) > 0) {
				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_Gr_ID', $query1[0]['To_Gr_ID']);
				$this->db->where('Me_Us_ID', $this->LoggedInUser);
				$query = $this->db->get()->result_array();



				$group = $this->groups_model->get_Record($query1[0]['To_Gr_ID']);
				$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';
				$linkmadeofgroup = "<a href='" . base_url() . "account/view/group/" . encode_id($query1[0]['To_Gr_ID']) . "'>";
				if (count($query) == 0) {
					$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'group', $_POST['post_role'], $status, $this->LoggedInUser, 0, 0, 0, 0, $_POST['post_id']);

					if ($resp) {
						$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'topic', $_POST['post_role'], 'pending', $this->LoggedInUser, 0, 0, 0, 0, $_POST['post_id']);


						$notif_type = "topic_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/topic/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined  Topic " . $linkmade . $query1[0]['To_Name'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a>    ";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['To_Name'] . ' topic  ', $content, $this->LoggedInUser, $resp, $query1[0]['To_Gr_ID'], 'group');
						if ($status == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						} else {
							$response = array('status' => 'success', 'message' => 'You joined topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the post details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

						}
					}
				}
				else {
					$data['general_Count'] = $this->members_model->check_if_member_record_access_exist("topic", $query1[0]['To_Gr_ID'], $_POST['post_id'], '', "pending");
					$data['general_Count'] += $this->members_model->check_if_member_record_access_exist("topic", $query1[0]['To_Gr_ID'], $_POST['post_id'], '', "active");

					if ($data['general_Count'] > 0) {
						$response = array('status' => 'success', 'message' => 'Your request to join private topic ' . strtoupper($query1[0]['To_Name']) . ' under ' . strtoupper($group['Gr_Name']) . ' has been approved.', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

					} else {
						$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'topic', $_POST['post_role'], 'pending', $this->LoggedInUser, 0, 0, 0, 0, $_POST['post_id']);
						$notif_type = "topic_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/topic/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined private post " . $linkmade . $query1[0]['To_Name'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> ";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['To_Name'] . ' post ', $content, $this->LoggedInUser, $resp, $query1[0]['To_Gr_ID'], 'group');
						if ($query[0]['Me_Status'] == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined   topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the topic details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						} else {
							$response = array('status' => 'success', 'message' => 'You joined   topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the topic details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						}

					}


				}

			} else {
				$response = array('status' => 'error', 'message' => 'Topic is not availble');
			}

		} else {


			if (count($query1) > 0) {

				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_Gr_ID', $query1[0]['To_Gr_ID']);
				$this->db->where('Me_Us_ID', $this->LoggedInUser);
				$query = $this->db->get()->result_array();

				$info = $this->members_model->get_member_information($query1[0]['To_Gr_ID'], 'group', $this->LoggedInUser);
				$group = $this->groups_model->get_Record($query1[0]['To_Gr_ID']);
				$user = $this->users_model->get_Users($this->LoggedInUser);
				$referrer = $this->users_model->get_Users($_POST['post_referrer']);
				$status = ($group['Gr_Privacy'] == 'public') ? 'active' : 'pending';
				$linkmadeofgroup = "<a href='" . base_url() . "account/view/group/" . encode_id($query1[0]['To_Gr_ID']) . "'>";

				if (count($query) == 0) {
					$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'group', $_POST['post_role'], $status, $this->LoggedInUser, 1, 1, $_POST['post_referrer'], 0, $_POST['post_id']);
					if ($resp) {
						$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'topic', $_POST['post_role'], 'pending', $this->LoggedInUser, 1, 1, $_POST['post_referrer'], 0, $_POST['post_id']);
						$notif_type = "topic_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/topic/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined  topic " . $linkmade . $query1[0]['To_Name'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> via invite link shared by  <a href=" . base_url() . "u/" . encode_id($referrer['Us_ID']) . ">" . $referrer['Us_Name'] . "</a>";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['To_Name'] . ' topic via share link', $content, $this->LoggedInUser, $resp, $query1[0]['To_Gr_ID'], 'group');
						if ($status == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the topic details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

						} else {
							$response = array('status' => 'success', 'message' => 'You joined topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the topic details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');

						}
					}
				} else {


					$data['general_Count'] = $this->members_model->check_if_member_record_access_exist("topic", $query1[0]['To_Gr_ID'], $_POST['post_id'], '', "pending");
					$data['general_Count'] += $this->members_model->check_if_member_record_access_exist("topic", $query1[0]['To_Gr_ID'], $_POST['post_id'], '', "active");
					if ($data['general_Count'] > 0) {
						$response = array('status' => 'error', 'message' => 'Your request to join subgroup ' . strtoupper($query1[0]['To_Name']) . ' under ' . strtoupper($group['Gr_Name']) . ' has been approved.');

					} else {
						$resp = $this->members_model->add_Member($query1[0]['To_Gr_ID'], 'topic', $_POST['post_role'], 'pending', $this->LoggedInUser, 1, 1, $_POST['post_referrer'], 0, $_POST['post_id']);
						$notif_type = "topic_join_via_link";
						$linkmade = "<a href='" . base_url() . "account/view/topic/" . encode_id($_POST['post_id']) . "'>";
						$content = " joined topic " . $linkmade . $query1[0]['To_Name'] . " </a> under " . $linkmadeofgroup . strtoupper($group['Gr_Name']) . "</a> via invite link shared by  <a href=" . base_url() . "u/" . encode_id($referrer['Us_ID']) . ">" . $referrer['Us_Name'] . "</a>";
						$this->notifications_model->add_notification($notif_type, 'A user joined  ' . $query1[0]['To_Name'] . ' post via share link', $content, $this->LoggedInUser, $resp, $query1[0]['To_Gr_ID'], 'group');
						if ($query[0]['Me_Status'] == 'pending') {
							$response = array('status' => 'success', 'message' => 'You joined topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the topic details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						} else {
							$response = array('status' => 'success', 'message' => 'You joined  topic ' . strtoupper($query1[0]['To_Name']) . ' under group  ' . strtoupper($group['Gr_Name']) . '. Once you are approved by Admin, you can access the topic details. ', 'id' => encode_id($_POST['post_id']), 'type' => 'post');
						}

					}


				}


			} else {
				$response = array('status' => 'error', 'message' => 'topic is not availble');

			}

		}


		echo json_encode($response);

	}
	public function submitprivacyform(){
		$update = array(
			'aggrement_one' => 'on',
			'aggrement_two' => 'on',
			'aggrement_three' => 'on'
		);
		$where = array(
			'Us_ID' =>  $this->LoggedInUser
		);
		$this->db->where($where);
		$res = $this->db->update('bs_users' , $update);
					echo json_encode($res);
	}
		public function checkuserprivacy(){
			$this->db->select('bs_users.*');
			$this->db->from('bs_users');

			$this->db->where('Us_ID', $this->LoggedInUser);
			$query = $this->db->get();
			echo json_encode( $query->result_array());
		}
	public function checkuseralreadymember()
	{

		$this->db->select('bs_members.*');
		$this->db->from('bs_members');
		$this->db->where('Me_Gr_ID', $_POST['group_id']);
		$this->db->where('Me_Us_ID', $this->LoggedInUser);
		$query = $this->db->get();
		echo json_encode(count($query->result_array()));
	}
	public function getpollRecordbyPostID(){
		$this->db->select('bs_posts_poll.*');
		$this->db->from('bs_posts_poll');
		$this->db->where('Pl_PO_ID' , $_POST['id']);

		$data['polldata'] =	$this->db->get()->result_array();
//		print_r($data);
//		exit;
		$data['totalreview'] =  array();
		$data['reviwer'] =  array();
		if(isset($data['polldata'][0]['PL_answers']) ){
			$array = explode('^', $data['polldata'][0]['PL_answers']);
			foreach ($array as $roww){
				$this->db->select('bs_poll_ans.* , bs_users.*');
				$this->db->from('bs_poll_ans');
				$this->db->join('bs_users' , 'bs_poll_ans.Poa_Us_ID = bs_users.Us_ID' );
				$this->db->where('Poa_ans' , $roww);
				$this->db->where('Poa_pl_id' , $data['polldata'][0]['Pl_ID']);
				$resss = $this->db->get()->result_array();
				$datas =  array($roww => count($resss));
				$datas1 =  array($roww =>  $resss);

				array_push(	$data['totalreview'] , $datas);
				array_push(	$data['reviwer'] , $datas1);
			}



		}



		echo  json_encode($data);
	}
	public function viewgroup($GroupID, $Invite = false)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Topics";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorGroupDetail'] = "";
		$data['errorGroupPhoto'] = "";
		$data['txtSearchProduct'] = $this->input->post('txtSearchProduct');

		$UserID = ($this->session->userdata['logged_in']['bs_id']);


		$data['users_item'] = $this->users_model->get_Users($UserID);
		$data['groups_items'] = $this->groups_model->get_Group($GroupID);
		$data['posts_items'] = $this->posts_model->get_GroupPosts($GroupID);
		$data['topics_items'] = $this->topics_model->get_GroupTopics($GroupID);
		$data['category_items'] = $this->category_model->get_CategoryByType('topic');
		$data['members_items'] = $this->users_model->get_Members('group', $GroupID);
		$data['policy_items'] = $this->policy_model->get_GroupPolicy($GroupID);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();

		if ($Invite == true) {
			$txtSearchMember = $this->input->post('txtSearchMember');
			$txtInviteUserID = $this->input->post('txtInviteUserID');
			$data['SearchMember'] = $txtSearchMember;

			if (strlen($txtInviteUserID) > 0) {
				$this->users_model->add_Invite('group', $GroupID, 0, 0, $txtInviteUserID);
				// $this->notifications_model->Insert('invite', $UserID, $txtInviteUserID, 'You are invited to join the '.$data['groups_items']['Gr_Name'].' community', '', 'pending');
				$data['members_items'] = $this->users_model->get_Members('group', $GroupID);
			}

			$data['invites_items'] = $this->users_model->searchUsers($txtSearchMember, $UserID);
		}

		$data = $this->setPermissions($data);
		if (strcmp($UserID, $data['groups_items']['Gr_Us_ID']) == 0) {
			$data['ShowAdminAction'] = "";
		}


//		$data['member_counter'] = count($this->groups_model->counterofMember($GroupID));

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/group_header', $data);
		if ($Invite == false) $this->load->view('account/group', $data);
		else    $this->load->view('account/groupinvite', $data);
		$this->load->view('templates/footer', $data);
	}

	public function setPermissions($data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['ShowAdminAction'] = "d-none";
		$data['ShowJoinAction'] = "";
		$data['ShowLeaveAction'] = "d-none";
		$data['ShowCreateAction'] = "d-none";
		$data['ShowInviteAction'] = "d-none";
		$data['ShowPolicyAction'] = "d-none";
		$data['ShowCreatePostAction'] = "d-none";

		foreach ($data['members_items'] as $item):
			if (strcmp($item['Me_Us_ID'], $UserID) == 0) {
				if (strcmp($item['Me_Role'], 'admin') == 0) {
					$data['ShowJoinAction'] = "d-none";
					$data['ShowLeaveAction'] = "d-none";
					$data['ShowAdminAction'] = "";
					$data['ShowCreateAction'] = "";
					$data['ShowInviteAction'] = "";
					$data['ShowPolicyAction'] = "";
					$data['ShowCreatePostAction'] = "";
				} else if (strcmp($item['Me_Role'], 'moderator') == 0) {
					$data['ShowJoinAction'] = "d-none";
					$data['ShowLeaveAction'] = "";
					$data['ShowCreateAction'] = "";
					$data['ShowInviteAction'] = "";
					$data['ShowPolicyAction'] = "";
					$data['ShowCreatePostAction'] = "";
				} else if (strcmp($item['Me_Role'], 'member') == 0) {
					$data['ShowJoinAction'] = "d-none";
					$data['ShowLeaveAction'] = "";
					$data['ShowCreateAction'] = "";
					$data['ShowCreatePostAction'] = "";
				}

			}
		endforeach;

		return $data;
	}


	public function changegroupphoto()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}


		$GroupID = $this->input->post('GroupID');
		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorGroupDetail'] = "";
		$data['errorGroupPhoto'] = "";

		$data['selSubMenu'] = "Photo";

		$Path = '/groups/' . $GroupID;
		$Error = $this->uploadphoto($UserID, './uploads/' . $UserID . $Path, 'userfile');
		$ErrorMsg = substr($Error, 0, 6);
		if (strcmp('Error:', $ErrorMsg) == 0) {
			$data['errorGroupPhoto'] = $Error;
		} else {
			$FileName = $Error;
			$Photo = $this->getPhotoPath($UserID, $Path, $FileName);
			$Thumb = $this->getPhotoPath($UserID, $Path . '/thumbnail', $FileName);

			$ErrorResize = $this->resizeImage($UserID, $Path, $FileName);
			$this->groups_model->set_changephoto($UserID, $GroupID, $FileName, $Photo, $Thumb);

			if (strlen($ErrorResize) > 0) $data['errorGroupPhoto'] = $ErrorResize;
			else $data['errorGroupPhoto'] = "Photo was changed successfully!";
		}


		$this->showGroupSettings($GroupID, $data);
	}

	public function setgroupphoto($GroupID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}


		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorGroupDetail'] = "";
		$data['errorGroupPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";

		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$Background = $this->settings_model->get_Setting($SettingID);
		$this->groups_model->set_changephoto($UserID, $GroupID, $Background['Se_Value'], $Background['Se_Photo'], $Background['Se_Thumb']);
		$data['errorGroupPhoto'] = "Photo was changed successfully!";

		$this->showGroupSettings($GroupID, $data);
	}


	public function setgroupbackcolor($GroupID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorGroupDetail'] = "";
		$data['errorGroupPhoto'] = "";

		$data['selSubMenu'] = "Photo";

		$Backcolor = $this->settings_model->get_Setting($SettingID);
		$this->groups_model->set_UpdateBackcolor($GroupID, $Backcolor['Se_Value']);
		$data['errorGroupPhoto'] = "Background was changed successfully!";

		$this->showGroupSettings($GroupID, $data);
	}


	public function settopicphoto($TopicID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorTopicDetail'] = "";
		$data['errorTopicPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";

		$Background = $this->settings_model->get_Setting($SettingID);
		$this->topics_model->set_changephoto($TopicID, $Background['Se_Value'], $Background['Se_Photo'], $Background['Se_Thumb']);
		$data['errorTopicPhoto'] = "Background was changed successfully!";

		$this->showTopicSettings($TopicID, $data);
	}


	public function settopicbackcolor($TopicID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorTopicDetail'] = "";
		$data['errorTopicPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";


		$Backcolor = $this->settings_model->get_Setting($SettingID);
		$this->topics_model->set_UpdateBackcolor($TopicID, $Backcolor['Se_Value']);
		$data['errorGroupPhoto'] = "Background was changed successfully!";

		$this->showTopicSettings($TopicID, $data);
	}

	public function setsubtopicphoto($SubTopicID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";

		$Background = $this->settings_model->get_Setting($SettingID);
		$this->topics_model->set_changephoto($SubTopicID, $Background['Se_Value'], $Background['Se_Photo'], $Background['Se_Thumb']);
		$data['errorPhoto'] = "Background was changed successfully!";

		$this->showSubTopicSettings($SubTopicID, $data);
	}


	public function setsubtopicbackcolor($SubTopicID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}


		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";


		$Backcolor = $this->settings_model->get_Setting($SettingID);
		$this->topics_model->set_UpdateBackcolor($SubTopicID, $Backcolor['Se_Value']);
		$data['errorGroupPhoto'] = "Background was changed successfully!";

		$this->showSubTopicSettings($SubTopicID, $data);
	}


	public function setpostphoto($PostID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";

		$Background = $this->settings_model->get_Setting($SettingID);
		$this->posts_model->set_changephoto($PostID, $Background['Se_Value'], $Background['Se_Photo'], $Background['Se_Thumb']);
		$data['errorPhoto'] = "Photo was changed successfully!";


		$data['posts_item'] = $this->posts_model->get_Post($PostID);
		$this->showPost($PostID, $data);
	}

	public function setpostbackcolor($PostID, $SettingID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";

		$Backcolor = $this->settings_model->get_Setting($SettingID);
		$this->posts_model->set_UpdateBackcolor($PostID, $Backcolor['Se_Value']);
		$data['errorGroupPhoto'] = "Background was changed successfully!";


		$data['posts_item'] = $this->posts_model->get_Post($PostID);
		$this->showPost($PostID, $data);
	}


	public function topicinfo($option, $id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$UserID = $data['CurrUserID'];
		$data['selMenu'] = 'detail';
		$data['error'] = "";
		$data['notification'] = "";
		$data['EncodedID'] = $id;

		$id = decode_id($id);
//		print_R($id);
		$data['topics_member'] = $this->topics_model->topics_member($id);
//		print_r($data['topics_member']);
//		exit;
		if (strcmp($option, 'updatedetail') == 0) {
			$data = $this->UpdateTopicDetail($id, $data);
		} else if (strcmp($option, 'background') == 0) {
			$data['selMenu'] = 'background';
		} else if (strcmp($option, 'changebackground') == 0) {
			$data['selMenu'] = 'background';
			$data = $this->do_UploadPhoto($data['CurrUserID'], $id, 'topic', $data);
		} else if (strcmp($option, 'delete') == 0) {
			$data['topics_items'] = $this->topics_model->get_Topic($id);
			$GroupID = $data['topics_items']['To_Gr_ID'];
			$this->topics_model->delete_topic($id);
			redirect('/account/view/group/' . $GroupID);
			return;
		}


		$data['topics_items'] = $this->topics_model->get_Topic($id);
		$data['creator_info'] = $this->members_model->get_admin_record($data['topics_items']['To_Gr_ID'], $this->LoggedInUser);

		$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);
		$data['category_items'] = $this->category_model->get_CategoryByType('topic');
		$data['subcategory_items'] = $this->category_model->get_SubCategory($data['topics_items']['To_Ca_ID']);

		if (empty($data['topics_items'])) {
			show_404();
		}

		$data['posts_items'] = $this->posts_model->get_TopicPosts($id);
		$data['members_items'] = $this->users_model->get_Members('topic', $id);
		$data['subtopics_items'] = $this->topics_model->get_SubTopics($id);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();

		$data['member_info'] = $this->members_model->get_member_information($id, 'topic');


		if($data['creator_info']['role'] == 'creator'){

			$data['member_info'] = $this->members_model->get_member_information($data['topics_items']['To_Gr_ID'], 'group');
			$data['userType'] = $data['member_info']['role'];

		}

		$data['is_followed'] = $this->follower_model->check_if_followed($data['topics_items']['To_ID'], 'topic');

		$data['showjoin'] = $this->users_model->check_Members('topic', $data['topics_items']['To_Gr_ID'], $id, 0, $UserID);
		$data = $this->setPermissions($data);
		if (strcmp($UserID, $data['topics_items']['To_Us_ID']) == 0) {
			$data['ShowAdminAction'] = "";
		}

		$data['asset'] = 'account/update/topic';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/topic_header', $data);
		if ((strcmp($option, 'detail') == 0) || (strcmp($option, 'updatedetail') == 0)) $this->load->view('topicinfo/topic_detail', $data);
		else if ((strcmp($option, 'edit') == 0)) $this->load->view('topicinfo/topic_edit', $data);
		else if ((strcmp($option, 'background') == 0) || (strcmp($option, 'changebackground') == 0)) $this->load->view('topicinfo/background', $data);
		$this->load->view('templates/footer', $data);
	}

	public function UpdateTopicDetail($TopicID, $data)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtTitle', 'Topic Name', 'required');
		if ($this->form_validation->run() === FALSE) {
			$data['error'] = "Invalid!";
		} else {
			$UserID = $data['CurrUserID'];
			$selGroup = $this->input->post('selGroup');
			$selCategory = $this->input->post('selCategory');
			$txtNewCategory = $this->input->post('txtNewCategory');
			if ($txtNewCategory == null) $txtNewCategory = '';
			$selSubCategory = $this->input->post('selSubCategory');
			$txtNewSubCategory = $this->input->post('txtNewSubCategory');
			if ($txtNewSubCategory == null) $txtNewSubCategory = '';
			$txtTitle = $this->input->post('txtTitle');
			$txtDescription = $this->input->post('txtDescription');
			$radPrivacy = $this->input->post('radPrivacy');
			$keywords = $this->input->post('txtKeywords');

			$data['PrimaryID'] = $this->topics_model->Update($TopicID, $selGroup, $txtTitle, $txtDescription, $selCategory, $txtNewCategory, $selSubCategory, $txtNewSubCategory, $radPrivacy, $keywords);
			$data['notification'] = "Topic detail was updated successfully!";

			$data['EncodedID'] = encode_id($TopicID);

			$this->check_and_save_new_keywords($keywords);
		}

		return $data;
	}

	public function postinfo($option, $id)
	{


//			this is update

		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}


		$data['title'] = "Post Info";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$UserID = $data['CurrUserID'];
		$data['selMenu'] = "detail";
		$data['error'] = "";
		$data['notification'] = "";
		$data['EncodedID'] = $id;
		$id = decode_id($id);

		$this->db->select("post_update.* ,bs_users.* , bs_posts.*");
		$this->db->from("post_update");

		$this->db->join('bs_users' , 'bs_users.Us_ID = post_update.Pu_user_id');

	    $this->db->join('bs_posts' , 'bs_posts.Po_ID = post_update.Pu_post_id');
		$this->db->limit(1);
		$this->db->order_by('Pu_ID',"DESC");
		$this->db->where('Pu_post_id' , $id );
		$query = $this->db->get();
		$result_of_update = $query->result_array();

		$data['lastUpdate'] = $result_of_update;



		if (strcmp($option, 'updatedetail') == 0) {

 				$dataq = array(
				'Pu_user_id' => $this->LoggedInUser,
				'Pu_post_id' => $id,
			);

			$this->db->insert('post_update', $dataq);

			$data = $this->updatepostdetail($id, $data);
		}
		else if (strcmp($option, 'background') == 0) {
			$data['selMenu'] = 'background';
		} else if (strcmp($option, 'changebackground') == 0) {
			$data['selMenu'] = 'background';
			$data = $this->do_UploadPhoto($data['CurrUserID'], $id, 'post', $data);
		} else if (strcmp($option, 'delete') == 0) {

			$data['posts_item'] = $this->posts_model->get_Post($id);
			$GroupID = $data['posts_item']['Po_Gr_ID'];
			$TopicID = $data['posts_item']['Po_To_ID'];
			$this->posts_model->delete_row($id);


			if ( $TopicID  > 0) {
				redirect('/account/view/topic/' . encode_id($TopicID));
				return;
			} else {
  				redirect('/account/view/group/' . encode_id($GroupID));
				return;
			}
		}

		$data['button_items'] = $this->buttons_model->get_record();
		$data['posts_item'] = $this->posts_model->get_Post($id);

		$data['creator_info'] = $this->members_model->get_admin_record($data['posts_item']['Po_Gr_ID'], $this->LoggedInUser);
		$data['topics_items'] = $this->topics_model->get_GroupTopics($data['posts_item']['Po_Gr_ID']);
		$data['groups_items'] = $this->groups_model->get_user_group_access($UserID);
		$data['general_Count'] = $this->members_model->check_if_member_record_access_exist("post", $data['posts_item']['Po_Gr_ID'], '', $data['posts_item']['Po_ID'], "pending");


		if ($this->LoggedInUser == $data['posts_item']['Po_Us_ID']) {

			$data['member_info_post'] = $this->members_model->get_member_information_admin($id, $data['posts_item']['Po_Gr_ID']);
			$data['userType'] = $data['member_info_post']['role'];

		} else {
			if (strcasecmp($data['posts_item']['Po_Privacy'], "public") != 0 && $this->LoggedInUser != $data['posts_item']['Po_Us_ID']) {
				$data['member_info_post'] = $this->members_model->get_member_POST_info($id, $data['posts_item']['Po_Gr_ID'], $data['posts_item']['Po_ID']);
				$data['userType'] = $data['member_info_post']['role'];

			} else {
				$data['member_info_post'] = $this->members_model->get_member_information_POST($id, $data['posts_item']['Po_Gr_ID']);
				$data['userType'] = $data['member_info_post']['role'];

			}
		}
		
		if($data['posts_item']['Po_Privacy'] == 'public'){
			$data['member_info_post'] = $this->members_model->get_member_information($data['posts_item']['Po_Gr_ID'], 'group');
			$data['userType'] = $data['member_info_post']['role'];
		}
		if($data['creator_info']['role'] == 'creator'){

			$data['member_info_post'] = $this->members_model->get_member_information($data['posts_item']['Po_Gr_ID'], 'group');
			$data['userType'] = $data['member_info_post']['role'];

		}
		if (empty($data['posts_item']['Po_To_ID']))
			$data['category_items'] = $this->category_model->get_CategoryByType('topic');
		else
			$data['category_items'] = $this->category_model->get_CategoryByType('post');

		$data['subcategory_items'] = $this->category_model->get_SubCategory($data['posts_item']['Po_Ca_ID']);

		$data['posts_files'] = $this->medias_model->get_PostMedia($id, 'files');
		$data['posts_images'] = $this->medias_model->get_PostMedia($id, 'images');
		$data['members_items'] = $this->users_model->get_Members('post', $id);

		$data['member_info'] = $this->members_model->get_member_information($id, 'post');


		$data['is_followed'] = $this->follower_model->check_if_followed($data['posts_item']['Po_ID'], 'post');

		$data['showjoin'] = $this->users_model->check_Members('post', $data['posts_item']['Po_Gr_ID'], $id, 0, $UserID);
		$data = $this->setPermissions($data);
		if (strcmp($UserID, $data['posts_item']['Po_Us_ID']) == 0) {
			$data['ShowAdminAction'] = "";
		}

		if($data['posts_item']['Po_Privacy'] == "public"){
			$data['member_counter'] = count($this->groups_model->counterofMember($data['posts_item']['Po_Gr_ID']));

		}else{
			$data['member_counter'] = count($this->groups_model->counterofMember($id , 'post'));
			$data['member_counter'] = $data['member_counter'] +1;
		}


		$data['post_count'] = count($this->groups_model->counterofpost($data['posts_item']['Po_Gr_ID']));


		$data['userPostLike'] = $this->users_model->getuserLike($data['CurrUserID'], $id);
		$data['userfav'] = $this->users_model->getuserfav($data['posts_item']['Po_Us_ID'], $id);

		$data['getContect'] = $this->users_model->getContect($this->LoggedInUser);
		$this->db->select('bs_voucher_post.* , bs_posts.*');
		$this->db->from('bs_voucher_post');
		$this->db->join('bs_posts' , 'bs_posts.Po_ID  = Vp_post_id' );
		$this->db->where('Vp_id' , $data['posts_item']['Po_reward']);


		$respons = $this->db->get()->result_array();

		$data['rewarddetails'] = $respons;

		
		$data['promoted'] = $this->posts_model->get_promoted_post($id);
		$data['promoteddata'] = $this->posts_model->get_promoted_post_data($id);

		$data['asset'] = 'account/update/post';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/post_header', $data);



		if ((strcmp($option, 'detail') == 0) || (strcmp($option, 'updatedetail') == 0)) $this->load->view('postinfo/post_detail', $data);
		else if ((strcmp($option, 'edit') == 0)) {


			$this->load->view('postinfo/post_edit', $data);
		} else if ((strcmp($option, 'background') == 0) || (strcmp($option, 'changebackground') == 0)) $this->load->view('postinfo/background', $data);
// 			}


		$this->load->view('templates/footer', $data);
	}


	public function edittopicbg($TopicID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorTopicDetail'] = "";
		$data['errorTopicPhoto'] = "";

		$data['selSubMenu'] = 'Photo';
		$this->showTopicSettings($TopicID, $data);
	}

	public function showTopicSettings($TopicID, $data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['topics_items'] = $this->topics_model->get_Topic($TopicID);
		$data['posts_items'] = $this->posts_model->get_TopicPosts($TopicID);
		$data['members_items'] = $this->users_model->get_Members('topic', $TopicID);
		$data['subtopics_items'] = $this->topics_model->get_SubTopics($TopicID);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();

		$data['showjoin'] = $this->users_model->check_Members('topic', $data['topics_items']['To_Gr_ID'], $TopicID, 0, $UserID);
		$data = $this->setPermissions($data);

		if (empty($data['topics_items'])) {
			show_404();
		}

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/topic_header', $data);
		$this->load->view('account/topic_settings', $data);
		$this->load->view('templates/footer', $data);

	}

	public function topic($TopicID)
	{
		$this->viewtopic($TopicID);
	}

	public function viewtopic($TopicID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorTopicDetail'] = "";
		$data['errorTopicPhoto'] = "";

		$this->showTopic($TopicID, $data);
	}

	public function showTopic($TopicID, $data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['topics_items'] = $this->topics_model->get_Topic($TopicID);
		$data['posts_items'] = $this->posts_model->get_TopicPosts($TopicID);
		$data['members_items'] = $this->users_model->get_Members('topic', $TopicID);
		$data['policy_items'] = $this->policy_model->get_TopicPolicy($TopicID);
		$data['subtopics_items'] = $this->topics_model->get_SubTopics($TopicID);

		$data['showjoin'] = $this->users_model->check_Members('topic', $data['topics_items']['To_Gr_ID'], $TopicID, 0, $UserID);
		$data = $this->setPermissions($data);

		if (empty($data['topics_items'])) {
			show_404();
		}

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/topic_header', $data);
		$this->load->view('account/topic', $data);
		$this->load->view('templates/footer', $data);
	}

	public function savesurvey()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Survey";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['error'] = "";
		$data['Name'] = "";

		$paramSrc = $this->input->post('paramSrc');
		$paramID = $this->input->post('paramID');

		$this->form_validation->set_rules('txtTitle', 'Title', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->view_createsurvey($paramSrc, $paramID, "Invalid!");
		} else {
			$UserID = ($this->session->userdata['logged_in']['bs_id']);
			$selGroup = $this->input->post('selGroup');
			$selTopic = $this->input->post('selTopic');
			$selSubTopic = $this->input->post('selSubTopic');

			$selTopicID = $this->input->post('selTopicID');
			$selSubTopicID = $this->input->post('selSubTopicID');

			$txtTitle = $this->input->post('txtTitle');
			$txtDescription = $this->input->post('txtDescription');
			$selQuestions = $this->input->post('selQuestions');

			if (strcmp($paramSrc, 'topic') == 0) {
				$data['topics_items'] = $this->topics_model->get_Topic($selTopic);
				$GroupID = $data['topics_items']['To_Gr_ID'];
				$TopicID = $selTopic;
				$SubTopicID = 0;
			} else {
				$data['topics_items'] = $this->topics_model->get_SubTopic($paramID);
				$GroupID = $data['topics_items']['To_Gr_ID'];
				$TopicID = $data['topics_items']['To_Pa_ID'];
				$SubTopicID = $data['topics_items']['To_ID'];
			}

			$PostID = $this->posts_model->Insert($UserID, $paramSrc, $GroupID, $TopicID, $SubTopicID, $txtTitle, $txtDescription, $selQuestions);

			$this->do_uploadsurvey($UserID, $PostID);
			$this->survey('edit', $PostID);
		}
	}

	public function startsurvey()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Start Survey";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['error'] = "";
		$data['Name'] = "";

		$this->form_validation->set_rules('txtSurvey', 'Survey', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->survey('edit', $PostID, 'Invalid Survey Entry!');
		} else {
			$UserID = ($this->session->userdata['logged_in']['bs_id']);
			$GroupID = $this->input->post('GroupID');
			$TopicID = $this->input->post('TopicID');
			$SubTopicID = $this->input->post('SubTopicID');
			$PostID = $this->input->post('PostID');
			$txtSurvey = $this->input->post('txtSurvey');

			for ($i = 1; $i <= $txtSurvey; $i++) {
				$txtQuestion = $this->input->post('txtQuestion' . $i);
				$selType = $this->input->post('selType' . $i);
				$txtA = $this->input->post('txtA' . $i);
				$txtB = $this->input->post('txtB' . $i);
				$txtC = $this->input->post('txtC' . $i);
				$txtD = $this->input->post('txtD' . $i);

				$Options = "";
				if (strcmp($selType, 'multiple') == 0) {
					$Options = $txtA . "|" . $txtB;
					if (strlen($txtC) > 0) $Options = $Options . "|" . $txtC;
					if (strlen($txtD) > 0) $Options = $Options . "|" . $txtD;
				}

				$this->survey_model->Insert($GroupID, $TopicID, $SubTopicID, $PostID, $UserID, $i, $selType, $txtQuestion, $Options);

			}

			$this->posts_model->UpdateSurveyStatus($PostID, 'started');
			$this->survey('started', $PostID);

		}

	}

	public function submitsurvey()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Start Survey";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['error'] = "";
		$data['Name'] = "";

		$this->form_validation->set_rules('txtSurvey', 'Survey', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->survey('started', $PostID, 'Invalid Survey Entry!');
		} else {
			$UserID = ($this->session->userdata['logged_in']['bs_id']);
			$GroupID = $this->input->post('GroupID');
			$TopicID = $this->input->post('TopicID');
			$SubTopicID = $this->input->post('SubTopicID');
			$PostID = $this->input->post('PostID');
			$txtSurvey = $this->input->post('txtSurvey');

			for ($i = 1; $i <= $txtSurvey; $i++) {
				$selType = $this->input->post('selType' . $i);
				$txtAnswer = $this->input->post('txtAnswer' . $i);

				if (strcmp($selType, 'multiple') == 0) {
					$txtAnswer = $this->input->post('radOption' . $i);
				} else if (strcmp($selType, 'trueorfalse') == 0) {
					$txtAnswer = $this->input->post('radTrueFalse' . $i);
				}

				$this->survey_model->Answer($GroupID, $TopicID, $SubTopicID, $PostID, $UserID, $i, $txtAnswer);
			}

			$this->survey('started', $PostID);
		}
	}

	public function saveevent()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['title'] = 'Events';
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtSubject', 'Subject', 'required');
		$this->form_validation->set_rules('txtLocation', 'Location', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->view_createevent('Invalid!');
		} else {

			$GroupID = "0";
			$Subject = $this->input->post('txtSubject');
			$Location = $this->input->post('txtLocation');
			$StartDate = $this->input->post('txtStartDate');
			$DueDate = $this->input->post('txtDueDate');
			$Priority = $this->input->post('selPriority');
			$Type = $this->input->post('selType');
			$EventID = $this->events_model->set_Insert($UserID, $GroupID, $Subject, $Location, $StartDate, $DueDate, $Priority, $Type);

			$this->do_uploadevent($UserID, $EventID);
			$this->event($EventID);
		}
	}

	public function savetask()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['title'] = 'Tasks';
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtSubject', 'Subject', 'required');

		if ($this->form_validation->run() === FALSE) {

			$data['error'] = "";
			$this->load->view('templates/header', $data);
			$this->load->view('templates/navigation', $data);
			$this->load->view('account/createtask', $data);
			$this->load->view('templates/footer', $data);
		} else {
			$GroupID = "0";
			$Subject = $this->input->post('txtSubject');
			$StartDate = $this->input->post('txtStartDate');
			$DueDate = $this->input->post('txtDueDate');
			$Priority = $this->input->post('selPriority');
			$Status = $this->input->post('selStatus');
			$Completed = $this->input->post('txtCompleted');
			$TaskID = $this->tasks_model->set_Insert($UserID, $GroupID, $Subject, $StartDate, $DueDate, $Priority, $Status, $Completed);

			$this->do_uploadtask($UserID, $TaskID);
			$this->task($TaskID);
		}
	}

	public function savecomment()
	{
		$this->view = false;

		$post_data = $this->input->post();
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$this->load->helper('form');
		$this->load->library('form_validation');

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$PostID = decode_id($post_data['PostID']);
		$Title = $post_data['txtMessage'];
		$data = array();

		$this->form_validation->set_rules('txtMessage', 'Title', 'required');
		if ($this->form_validation->run() === FALSE) {
			$data['error'] = 'Please type in the message!';
		} else {
			$GroupID = $this->input->post('GroupID');
			$TopicID = $this->input->post('TopicID');
			$txtMessage = $this->input->post('txtMessage');
			$comment_id = $this->comments_model->Insert($GroupID, $TopicID, $PostID, $UserID, $txtMessage);
			$data['comments_items'] = ($this->comments_model->get_Comments($PostID, $comment_id))[0];

			$data['comments_items']['Co_DatePosted'] = date("F d, Y h:i", strtotime($data['comments_items']['Co_DatePosted']));
		}
		print json_encode($data['comments_items']);
		exit;
	}

	public function savepolicy()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$this->load->helper('form');
		$this->load->library('form_validation');


		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$Parent = $this->input->post('Parent');
		$GroupID = $this->input->post('GroupID');
		$TopicID = $this->input->post('TopicID');
		$SubTopicID = $this->input->post('SubTopicID');

		$this->form_validation->set_rules('txtTitle', 'Title', 'required');
		if ($this->form_validation->run() === FALSE) {
			$data['error'] = 'Please type in the title!';
		} else {
			$txtTitle = $this->input->post('txtTitle');
			$txtDescription = $this->input->post('txtDescription');
			$this->policy_model->Insert($GroupID, $TopicID, $SubTopicID, $txtTitle, $txtDescription);
		}

		if (strcmp($Parent, 'topic') == 0) $this->topic($TopicID);
		else if (strcmp($Parent, 'subtopic') == 0) $this->subtopic($SubTopicID);
		else $this->group($GroupID);

	}

	public function updatepostdetail()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$PostID = decode_id($this->input->post('PostID'));
		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Detail";
		$data['EncodedID'] = $this->input->post('PostID');

		$PostID = decode_id($this->input->post('PostID'));

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtTitle', 'Title', 'required');
		if ($this->form_validation->run() === FALSE) {
			$data['errorPostDetail'] = "Invalid!";
		} else {
			$data['errorPostDetail'] = "Post detail was updated successfully!";

			$txtTitle = $this->input->post('txtTitle');
			$txtDescription = $this->input->post('txtDescription');

			$selGroup = $this->input->post('selGroup');
			$selTopic = $this->input->post('selTopic');

			$selCategory = $this->input->post('selCategory');
			$txtNewCategory = $this->input->post('txtNewCategory');
			if ($txtNewCategory == null) $txtNewCategory = '';
			$selSubCategory = $this->input->post('selSubCategory');
			$txtNewSubCategory = $this->input->post('txtNewSubCategory');
			if ($txtNewSubCategory == null) $txtNewSubCategory = '';

			$radPrivacy = $this->input->post('radPrivacy');
			$keywords = $this->input->post('txtKeywords');
			$actionButton = $this->input->post('selActionButton');
			$buttonLink = $this->input->post('txtLandingPage');



			if(!empty($actionButton)){

			}
			$actionName = ($actionButton == 0) ? $this->input->post('txtActionButton') : null;

			$formData = array(
				'Po_Title' => $txtTitle,
				'Po_Description' => $txtDescription,
				'Po_Gr_ID' => $selGroup,
				'Po_To_ID' => $selTopic,
				'Po_Ca_ID' => $selCategory,
				'Po_Ca_Name' => $txtNewCategory,
				'Po_Sc_ID' => $selSubCategory,
				'Po_Sc_Name' => $txtNewSubCategory,
				'Po_Privacy' => $radPrivacy,
				'Po_Keywords' => $keywords,
				'Po_Bn_ID' => $actionButton,
				'Po_Bn_Page' => $buttonLink,
				'Po_Bn_Name' => $actionName
			);

			$this->posts_model->Update($PostID, $formData);
			$data['posts_item'] = $this->posts_model->get_Post($PostID);

			$this->check_and_save_new_keywords($keywords);
		}

		return $data;
	}


	public function changetopicphoto()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}


		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorTopicDetail'] = "";
		$data['errorTopicPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";


		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$GroupID = $this->input->post('GroupID');
		$TopicID = $this->input->post('TopicID');

		$Path = '/topics/' . $TopicID;
		$Error = $this->uploadphoto($UserID, './uploads/' . $UserID . $Path, 'userfile');
		$ErrorMsg = substr($Error, 0, 6);
		if (strcmp('Error:', $ErrorMsg) == 0) {
			$data['errorTopicPhoto'] = $Error;
		} else {
			$FileName = $Error;
			$Photo = $this->getPhotoPath($UserID, $Path, $FileName);
			$Thumb = $this->getPhotoPath($UserID, $Path . '/thumbnail', $FileName);

			$ErrorResize = $this->resizeImage($UserID, $Path, $FileName);
			$this->topics_model->set_changephoto($TopicID, $FileName, $Photo, $Thumb);

			if (strlen($ErrorResize) > 0) $data['errorTopicPhoto'] = $ErrorResize;
			else $data['errorTopicPhoto'] = "Photo was changed successfully!";
		}

		$this->showTopicSettings($TopicID, $data);
	}


	public function subtopic($SubTopicID)
	{
		$this->viewsubtopic($SubTopicID);
	}

	public function editsubtopicdetail($SubTopicID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selSubMenu'] = 'Detail';
		$this->showSubTopicSettings($SubTopicID, $data);
	}

	public function editsubtopicbg($SubTopicID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selSubMenu'] = 'Photo';
		$this->showSubTopicSettings($SubTopicID, $data);
	}

	public function showSubTopicSettings($SubTopicID, $data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['subtopics_items'] = $this->topics_model->get_SubTopic($SubTopicID);
		$data['subposts_items'] = $this->posts_model->get_SubTopicPosts($SubTopicID);
		$data['members_items'] = $this->users_model->get_Members('subtopic', $SubTopicID);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();

		$data['showjoin'] = $this->users_model->check_Members('subtopic', $data['subtopics_items']['To_Gr_ID'], $data['subtopics_items']['To_Pa_ID'], $SubTopicID, $UserID);
		$data = $this->setPermissions($data);

		if (empty($data['subtopics_items'])) {
			show_404();
		}

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/subtopic_header', $data);
		$this->load->view('account/subtopic_settings', $data);
		$this->load->view('templates/footer', $data);

	}


	public function viewsubtopic($SubTopicID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Posts";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$this->showSubTopic($SubTopicID, $data);
	}


	public function showSubTopic($SubTopicID, $data)
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['subtopics_items'] = $this->topics_model->get_SubTopic($SubTopicID);
		$data['posts_items'] = $this->posts_model->get_SubTopicPosts($SubTopicID);
		$data['members_items'] = $this->users_model->get_Members('subtopic', $SubTopicID);
		$data['policy_items'] = $this->policy_model->get_SubTopicPolicy($SubTopicID);

		$data['showjoin'] = $this->users_model->check_Members('subtopic', $data['subtopics_items']['To_Gr_ID'], $data['subtopics_items']['To_Pa_ID'], $SubTopicID, $UserID);
		$data = $this->setPermissions($data);

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/subtopic_header', $data);
		$this->load->view('account/subtopic', $data);
		$this->load->view('templates/footer', $data);
	}


	public function updatesubtopicdetail()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Detail";

		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$TopicID = $this->input->post('TopicID');
		$SubTopicID = $this->input->post('SubTopicID');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('txtName', 'Name', 'required');
		if ($this->form_validation->run() === FALSE) {

			$data['errorDetail'] = "Invalid!";
		} else {
			$data['errorDetail'] = "Sub-Topic detail was updated successfully!";


			$SubTopicName = $this->input->post('txtName');
			$Privacy = $this->input->post('radPrivacy');
			$this->topics_model->Update($SubTopicID, $SubTopicName, $Privacy);
		}


		$this->showSubTopicSettings($SubTopicID, $data);
	}


	public function changesubtopicphoto()
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}


		$data['title'] = "Settings";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['errorPost'] = "";
		$data['errorDetail'] = "";
		$data['errorPhoto'] = "";

		$data['selMenu'] = "Settings";
		$data['selSubMenu'] = "Photo";

		$GroupID = $this->input->post('GroupID');
		$TopicID = $this->input->post('TopicID');
		$SubTopicID = $this->input->post('SubTopicID');
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$Path = '/subtopics/' . $SubTopicID;
		$Error = $this->uploadphoto($UserID, './uploads/' . $UserID . $Path, 'userfile');
		$ErrorMsg = substr($Error, 0, 6);
		if (strcmp('Error:', $ErrorMsg) == 0) {
			$data['errorPhoto'] = $Error;
		} else {
			$FileName = $Error;
			$Photo = $this->getPhotoPath($UserID, $Path, $FileName);
			$Thumb = $this->getPhotoPath($UserID, $Path . '/thumbnail', $FileName);

			$this->resizeImage($UserID, $Path, $FileName);
			$this->topics_model->set_changephoto($SubTopicID, $FileName, $Photo, $Thumb);
			$data['errorPhoto'] = "Photo was changed successfully!";
		}

		$this->showSubTopicSettings($SubTopicID, $data);
	}


	public function subtopics($To_ID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Sub-Topics";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = "";
		$bs_id = ($this->session->userdata['logged_in']['bs_id']);


		$data['topics_items'] = $this->topics_model->get_Topic($To_ID);
		$data['subtopics_items'] = $this->subtopics_model->get_TopicSubTopics($To_ID);

		$this->load->helper('form');
		$this->load->library('form_validation');


		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('templates/topic_header', $data);
		$this->load->view('account/subtopics', $data);
		$this->load->view('templates/footer', $data);

	}

	public function createsubtopic($TopicID = false)
	{
		$this->view_createsubtopic($TopicID, '');
	}


	public function view_createsubtopic($TopicID = false, $error)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Create SubGroup";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$bs_email = ($this->session->userdata['logged_in']['bs_email']);
		$bs_alias = ($this->session->userdata['logged_in']['bs_alias']);
		$UserID = ($this->session->userdata['logged_in']['bs_id']);


		$data['topics_items'] = $this->topics_model->get_Topic($TopicID);
		$data['backgrounds_items'] = $this->settings_model->get_Background();
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['users_items'] = $this->users_model->select_Users($UserID);

		$data['error'] = $error;
		$data['TopicName'] = "";
		$data['TopicID'] = $TopicID;
		$data['GroupID'] = $data['topics_items']['To_Gr_ID'];

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/createsubtopic', $data);
		$this->load->view('templates/footer', $data);
	}


	public function createtask()
	{
		$this->view_createtask('');
	}


	public function view_createtask($error)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Create Task";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = $error;

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/createtask', $data);
		$this->load->view('templates/footer', $data);
	}

	public function createevent()
	{
		$this->view_createevent('');
	}


	public function view_createevent($error)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Create Event";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = $error;

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/createevent', $data);
		$this->load->view('templates/footer', $data);
	}


	public function createsurvey($paramSrc = false, $paramID = false)
	{
		$this->view_createsurvey($paramSrc, $paramID, '');
	}


	public function view_createsurvey($paramSrc = false, $paramID = false, $error)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$UserID = ($this->session->userdata['logged_in']['bs_id']);

		$data['title'] = "Create Survey";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);


		$data['groups_items'] = $this->groups_model->get_UserGroups($UserID);
		$data['error'] = $error;


		$data['paramSrc'] = $paramSrc;
		$data['paramID'] = $paramID;

		$data['TopicID'] = 0;
		$data['SubTopicID'] = 0;

		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/createsurvey', $data);
		$this->load->view('templates/footer', $data);
	}


	public function createpolicy($Parent, $ParentID)
	{
		$this->view_createpolicy($Parent, $ParentID, '');
	}


	public function view_createpolicy($Parent, $ParentID, $error)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = "Create Policy";
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);
		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$data['error'] = $error;

		$data['Parent'] = $Parent;
		$data['GroupID'] = 0;
		$data['TopicID'] = 0;
		$data['SubTopicID'] = 0;
		if (strcmp($Parent, 'group') == 0) {
			$data['GroupID'] = $ParentID;
		} else if (strcmp($Parent, 'topic') == 0) {
			$data['topics_item'] = $this->topics_model->get_Topic($ParentID);
			$data['GroupID'] = $data['topics_item']['To_Gr_ID'];
			$data['TopicID'] = $ParentID;
		} else if (strcmp($Parent, 'subtopic') == 0) {
			$data['subtopics_item'] = $this->topics_model->get_SubTopic($ParentID);
			$data['GroupID'] = $data['subtopics_item']['To_Gr_ID'];
			$data['TopicID'] = $data['subtopics_item']['To_Pa_ID'];
			$data['SubTopicID'] = $ParentID;
		}


		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/createpolicy', $data);
		$this->load->view('templates/footer', $data);
	}

	public function deletepolicy($Parent, $ID)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$data['policy_item'] = $this->policy_model->get_Policy($ID);
		$GroupID = $data['policy_item']['Po_Gr_ID'];
		$TopicID = $data['policy_item']['Po_To_ID'];
		$SubTopicID = $data['policy_item']['Po_St_ID'];

		$this->policy_model->delete_row($ID);

		if (strcmp($Parent, 'topic') == 0) $this->topic($TopicID);
		else if (strcmp($Parent, 'subtopic') == 0) $this->subtopic($SubTopicID);
		else $this->group($GroupID);

	}

	public function deletesubtopic($id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$data['subtopics_item'] = $this->topics_model->get_SubTopic($id);
		$TopicID = $data['subtopics_item']['To_Pa_ID'];

		$this->topics_model->delete_subtopic($id);
		$this->topic($TopicID);
	}

	public function deletetopic($id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$this->topics_model->delete_topic($id);
		$this->viewprofile("Topics", "Profile");
	}

	public function deletegroup($id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$this->groups_model->delete_row($id);
		$this->viewprofile("Groups", "Profile");
	}


	public function deleteevent($id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$this->events_model->delete_row($id);
		$this->viewprofile("Events", "Profile");
	}

	public function deletetask($id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$this->tasks_model->delete_row($id);
		$this->viewprofile("Calendar", "Profile");
	}

	public function deletenotification($id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$this->notifications_model->delete_row($id);
		$this->notifications();
	}

	public function deletepost($id)
	{
		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$data['posts_item'] = $this->posts_model->get_Post($id);
		$GroupID = $data['posts_item']['Po_Gr_ID'];
		$TopicID = $data['posts_item']['Po_To_ID'];
		$SubTopicID = $data['posts_item']['Po_St_ID'];

		$this->posts_model->delete_row($id);
		if ($SubTopicID != 0) $this->subtopic($SubTopicID);
		else if ($TopicID != 0) $this->topic($TopicID);
		else $this->group($GroupID);

	}

	public function getPhotoPath($UserID, $Folder, $FileName)
	{
		return base_url() . './uploads/' . $UserID . '/' . $Folder . '/' . $FileName;
	}

	public function getThumbPath($UserID, $Folder, $FileName)
	{
		return base_url() . './uploads/' . $UserID . '/' . $Folder . '/thumbnail/' . $FileName;
	}


	public function uploadphoto($UploadDir, $UserFile)
	{

		if (empty($_FILES[$UserFile]['name'])) {
			return 'Error: Empty';
		}

		if (!is_dir($UploadDir)) {
			mkdir($UploadDir, 0777, TRUE);
		}

		$new_name = time() . $_FILES[$UserFile]['name'];

		$config['upload_path'] = $UploadDir;
		$config['file_name'] = $new_name;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = 50000;
		$config['max_width'] = 0;
		$config['max_height'] = 0;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload($UserFile)) {
			return 'Error: ' . $this->upload->display_errors();
		}

		$upload_data = $this->upload->data();
		$FileName = $upload_data['file_name'];

		return $FileName;
	}


	public function resizeImage($UserID, $Folder, $Filename)
	{
		$source_path = './uploads/' . $UserID . '/' . $Folder . '/' . $Filename;
		$target_path = './uploads/' . $UserID . '/' . $Folder . '/thumbnail/';

		if (!is_dir($target_path)) {
			mkdir($target_path, 0777, TRUE);
		}

		$config_manip = array(
			'image_library' => 'gd2',
			'source_image' => $source_path,
			'new_image' => $target_path . $Filename,
			'maintain_ratio' => TRUE,
			'create_thumb' => TRUE,
			'thumb_marker' => '',
			'width' => 400
		);


		$this->load->library('image_lib', $config_manip);
		if (!$this->image_lib->resize()) {
			return $this->image_lib->display_errors();
		}

		$this->image_lib->clear();
		return "";
	}


	public function group_list()
	{
		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$CatID = $this->input->post('catID');

		$data = $this->groups_model->get_UserGroupsFiltered($UserID, $CatID);
		echo json_encode($data);
	}


	public function getPingComment()
	{
		$data = $this->groups_model->getPingComment();

		echo json_encode($data);

	}


	public function addNotificationreminder()
	{


		$notif_type = "todo_reminder_post_notification";
		$content = "Reminder is added in the Todo List";
		$res = $this->notifications_model->add_notification($notif_type, 'A new Todo is Added',
			$content, $_POST['userID'], $this->LoggedInUser, $_POST['CommentId'], 'user');
		echo json_encode($res);


	}



		function inprogress(){

			$data = array('No_Status' => 'inprogress');
			if(isset($_POST['noticomment'])){
				$this->db->where('No_To', $_POST['noticomment']);
			}else{
				$this->db->where('No_ID', $_POST['commentId']);
			}

			$query = $this->db->update('bs_notification', $data);
			$data = array(
				'message' => 'This Todo list is inprogress'
			);
			echo json_encode($data);
		}
	public function addNotificationfotTag()
	{

		$notif_type = "todo_post_notification_own";
		$content = "assigned Task is added in the Todo List";
		$res = $this->notifications_model->add_notification($notif_type, 'A new Todo is Added',
			$content, $this->LoggedInUser, $_POST['userID'], $_POST['CommentId'], 'user');


		$notif_type = "todo_post_notification";
		$content = "Task is added in the Todo List";
		$res = $this->notifications_model->add_notification($notif_type, 'A new Todo is Added',
			$content, $_POST['userID'], $this->LoggedInUser, $_POST['CommentId'], 'user');



		echo json_encode($res);

	}

	public function get_topics()
	{


		$this->view = false;
		$GroupID = $this->input->post('GroupID');


		$ctr = 0;
		$topics_list[$ctr++] = array("id" => " ", "text" => "Choose Subgroup or create new Subgroup");
		$topics_list[$ctr++] = array("id" => "0", "text" => "None");

		$data = $this->topics_model->get_user_topic_per_group($GroupID);

		if (!empty($data)) {
			foreach ($data as $idx => $row) {
				$topics_list[$ctr++] = array("id" => $row['To_ID'], "text" => $row['To_Name']);
			}
		}

		echo json_encode($topics_list);
	}

	public function get_subcategory()
	{
		$this->view = false;
		$CatID = $this->input->post('CatID');
		$ctr = 0;
		$subcategory_list = array();

		$data = $this->category_model->get_SubCategory($CatID);

		if (!empty($data)) {
			foreach ($data as $idx => $row) {
				$subcategory_list[$ctr++] = array("id" => $row['Sc_ID'], "text" => $row['Sc_Name']);
			}
		}

		$subcategory_list[$ctr++] = array("id" => "0", "text" => "Others, please specify");

		echo json_encode($subcategory_list);
	}

	public function get_category()
	{
		$this->view = false;
		$type = $this->input->post('type');
		$ctr = 0;
		$category_list = array();

		$data = $this->category_model->get_CategoryByType($type);

		if (!empty($data)) {
			foreach ($data as $idx => $row) {
				$category_list[$ctr++] = array("id" => $row['Ca_ID'], "text" => $row['Ca_Name']);
			}
		}
		$category_list[$ctr++] = array("id" => "0", "text" => "Others, please specify");

		echo json_encode($category_list);
	}


	function request_grant_access()
	{
		$this->view = false;
		$ParentID = decode_id($this->input->post('ParentID'));
		$Parent = $this->input->post('Parent');

		$UserID = ($this->session->userdata['logged_in']['bs_id']);
		$UserName = ($this->session->userdata['logged_in']['bs_name']);
		$GroupID = 0;
		$TopicID = 0;
		$SubTopicID = 0;
		$OwnerID = 0;
		$Board = '';

		if (strcmp($Parent, 'group') == 0) {
			$Group = $this->groups_model->get_Record($ParentID);
			$GroupID = $ParentID;
			$OwnerID = $Group['Gr_Us_ID'];
			$Board = $Group['Gr_Name'] . ' community';
		} else if (strcmp($Parent, 'topic') == 0) {
			$Topic = $this->topics_model->get_Record($ParentID);
			$GroupID = $Topic['To_Gr_ID'];
			$TopicID = $ParentID;
			$OwnerID = $Topic['To_Us_ID'];
			$Board = $Topic['To_Name'] . ' topic';
		} else {
			$SubTopic = $this->posts_model->get_Post($ParentID);
			$GroupID = $SubTopic['Po_Gr_ID'];
			$TopicID = $SubTopic['Po_To_ID'];
			$SubTopicID = $ParentID;
			$OwnerID = $SubTopic['Po_Us_ID'];
			$Board = $SubTopic['Po_Title'] . ' subtopic';
		}

		$count = $this->members_model->check_if_member_record_access_exist($Parent, $GroupID, $TopicID, $SubTopicID);

		if (empty($count)) {
			$MemID = $this->members_model->add_Member($Parent, $GroupID, $TopicID, $SubTopicID);
			if ($MemID != 0) {
				// $this->notifications_model->Insert('join', $UserID, $OwnerID, 'User '.$UserName.' joined the '.$Board, '', 'pending');
			}

			print json_encode("success");
			exit;
		} else {
			print json_encode("failed");
			exit;
		}
	}

	function enable_disable_user_create_group_popup()
	{
		$this->view = false;

		$userID = $this->session->userdata['logged_in']['bs_id'];
		$disablePopUp = (boolean)$this->input->post('enabled');

		$this->users_model->enable_disable_user_create_group_popup($userID, $disablePopUp);

		return true;
	}

	private function get_record($type, $id)
	{
		$data = array();

		if (strcasecmp($type, 'group') == 0) {
			$data = $this->groups_model->get_Record($id);
		} else if (strcasecmp($type, 'topic') == 0) {
			$data = $this->topics_model->get_Record($id);
		} else if (strcasecmp($type, 'post') == 0) {
			$data = $this->posts_model->get_Record($id);
		}

		return $data;
	}

	private function get_category_by_type($type, $id)
	{
		$data = array();

		if (strcasecmp($type, 'group') == 0) {
			$data = $this->category_model->get_menu_categories('topic', $id);
		} else if (strcasecmp($type, 'topic') == 0) {
			$data = $this->category_model->get_topic_categories('post', $id);
		}

		return $data;
	}

	public function explore()
	{


		if (!isset($this->session->userdata['logged_in'])) {
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}
		$data['CurrUserID'] = ($this->session->userdata['logged_in']['bs_id']);

		$data['title'] = "Account Page";
		$data['create_group_btn_link'] = ($this->session->userdata['logged_in']['bs_group']) ? base_url() . 'account/create/group' : 'javascript:void(0)';
		$data['create_group_btn_class'] = ($this->session->userdata['logged_in']['bs_group']) ? '' : 'create_group_btn';

		$data['asset'] = 'account/explore';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('account/explore', $data);
		$this->load->view('templates/footer', $data);
	}

	public function show_group_items()
	{
		$this->view = false;
		$id = null;
		$category_id = null;
		$search_text = null;
		$ctr = 0;
		$data = array();

		$post_data = $this->input->post();

		if (isset($post_data['ID'])) {
			$id = decode_id($post_data['ID']);
		}

		if (isset($post_data['category_id'])) {
			$category_id = $post_data['category_id'];
		}

		if (isset($post_data['search_text'])) {
			$search_text = $post_data['search_text'];
		}

		$topics_items = $this->topics_model->get_GroupTopics($id, $category_id, $search_text);
		$none_topics_items = $this->posts_model->get_GroupTopicsNone($id, $category_id, $search_text);

		$subcategory_items = $this->category_model->get_group_subcategories($category_id, $id);

		if (!empty($topics_items)) {
			foreach ($topics_items as $item) {
				if ($item['Searchable']) {

					$item_href = base_url() . 'account/view/topic/' . $item['To_Slugs'];
					$target = "";
					if ($item['AskForPermission']) {
						// $item_href =  '<a class="board-link" href="#" data-toggle="modal" data-target="#topicAccessModal" data-id="'.$item['EncodedID'].'">';
						$item_href = '#';
						$target = '#topicAccessModal';
					}

					$thumb = str_replace(base_url(), '', $item['To_Thumb']);
					$thumb = base_url() . ((empty($item['To_Thumb'])) ? 'img/topic.png' : $thumb);

					$data[$ctr++] = array(
						'To_ID' => $item['To_ID'],
						'EncodedID' => $item['EncodedID'],
						'To_Name' => (strlen($item['To_Name']) >= 50) ? substr($item['To_Name'], 0, 50) . "... " : $item['To_Name'],
						'To_Ca_ID' => $item['To_Ca_ID'],
						'To_Sc_ID' => $item['To_Sc_ID'],
						'To_Thumb' => $thumb,
						'Searchable' => $item['Searchable'],
						'AskForPermission' => $item['AskForPermission'],
						'To_Backcolor' => $item['To_Backcolor'],
						'To_Href' => $item_href,
						'Target' => $target,
						'To_Privacy' => $item['To_Privacy']
					);
				}
			}
		}

		if (!empty($none_topics_items)) {
			foreach ($none_topics_items as $item) {
				if ($item['Searchable']) {

					$item_href = base_url() . 'account/view/post/' . $item['EncodedID'];
					$target = "";
					if ($item['AskForPermission']) {
						// $item_href =  '<a class="board-link" href="#" data-toggle="modal" data-target="#subtopicAccessModal" data-id="'.$item['EncodedID'].'">';
						$item_href = '#';
						$target = '#subtopicAccessModal';
					}


					$thumb = str_replace(base_url(), '', $item['Po_Thumb']);
					$thumb = base_url() . ((empty($item['Po_Thumb'])) ? 'img/topic.png' : $thumb);

					$data[$ctr++] = array(
						'To_ID' => $item['Po_ID'],
						'EncodedID' => $item['EncodedID'],
						'To_Name' => (strlen($item['Po_Title']) >= 50) ? substr($item['Po_Title'], 0, 50) . "... " : $item['Po_Title'],
						'To_Ca_ID' => $item['Po_Ca_ID'],
						'To_Sc_ID' => $item['Po_Sc_ID'],
						'To_Thumb' => $thumb,
						'Searchable' => $item['Searchable'],
						'AskForPermission' => $item['AskForPermission'],
						'To_Backcolor' => $item['Po_Backcolor'],
						'To_Href' => $item_href,
						'Target' => $target,
						'To_Privacy' => $item['Po_Privacy']
					);
				}
			}
		}

		print json_encode(array('data_items' => $data, 'subcategory_items' => $subcategory_items));
		exit;
	}

	public function follow()
	{
		$this->view = false;

		$data = $this->input->post();

		$this->follower_model->follow($data, TRUE);

		print json_encode(array('status' => 'success'));
		exit;

	}


	public function search_post_comments()
	{
		$this->view = false;

		$postID = decode_id($this->input->post('PostID'));
		$searchTxt = $this->input->post('search_txt');

		$comment_items = $this->comments_model->get_comments_by_search($postID, $searchTxt);

		print json_encode(array('comment_items' => $comment_items));
		exit;
	}

	public function get_keywords_list($query = '')
	{
		print json_encode($this->keyword_model->get_list($query));
		exit;
	}

	public function check_and_save_new_keywords($keywords)
	{
		$this->view = false;

		$keywords_list = explode(",", $keywords);

		foreach ($keywords_list as $keyword) {
			if (empty($this->keyword_model->get_keyword($keyword, false))
				&& count($this->keyword_model->get_keywords_occurence($keyword)) >= 2) {
				$this->keyword_model->add_keyword($keyword);
			}
		}
	}

	public function validate_keyword()
	{
		$this->view = false;
		$keyword = $this->input->post('keyword');

		$record = $this->keyword_model->get_keyword($keyword);

		print json_encode($record);
		exit;
	}
}



