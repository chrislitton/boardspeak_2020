<?php
class Post extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('posts_model', 'post');
        $this->load->model('comments_model');
        $this->load->model('contacts_model');
        $this->load->model('medias_model');
        $this->load->model('users_model');
        $this->load->model('groups_model');

        $this->load->model('notifications_model');
        $this->load->helper('url_helper');
        $this->load->helper(array('form', 'url'));

        // Load session library
        $this->load->library('session');
    }

		 public function show_group_members() {


		    	$this->view = false;
  					$data = $this->input->post();
				 if(isset($data['topicdata'])) {
					$data['pid'] = decode_id($data['pid']);
				 }
					$members = $this->post->show_members($data);

				 $already  = array();

					 foreach ($members['data'] as $key => $row) {

					 if (in_array($row['Us_Email'], $already))
					 {

						 unset($members['data'][$key]);
					 }
					 else
					 {
						 array_push($already, $row['Us_Email']);

					 }

			 }

					echo json_encode($members);
				}

				public function show_public_popular_posts_promote(){
					$this->view = false;

					$userID = $this->session->userdata['logged_in']['bs_id'];

//					$data = $this->input->post();

					$data  = array();
					$posts = $this->post->show_public_popular_posts_promote_Data($userID, $data);


					echo json_encode($posts);
				}
		public	function show_public_popular_posts(){

		$this->view = false;

        $userID = $this->session->userdata['logged_in']['bs_id'];

        $data = $this->input->post();
        $posts = $this->post->show_public_popular_posts($userID, $data);

        echo json_encode($posts);
			}

			public function getpostvoucher(){
					$post_id = $_POST['id'];
					$this->db->select('bs_voucher_post.*');
					$this->db->from('bs_voucher_post');
					$this->db->where('Vp_post_id' , $post_id);
				$res = 	$this->db->get()->result_array();
						echo json_encode($res);
			}
		public	function editrewardvoucher(){


//		print_r($_POST['voucherData']);
//		exit;
			$this->db->where('Vp_id' , $_POST['voucherID']);
				$res = 	$this->db->update('bs_voucher_post' , $_POST['voucherData']);

					if($res){
						echo json_encode('success');
					}else{
						echo json_encode('error');
					}
			}
    public function show_posts() {
        $this->view = false;

        $userID = $this->session->userdata['logged_in']['bs_id'];

        $data = $this->input->post();


        $posts = $this->post->show_posts($userID, $data);

        echo json_encode($posts);
    }
     public function show_group_join_posts() {
            $this->view = false;
 		    $data = $this->input->post();


			$userID = (isset($data['user_id'])) ? decode_id($data['user_id']) : $this->session->userdata['logged_in']['bs_id'];
 	        $posts = $this->post->get_join_group_Post_modal($userID, $data);

            echo json_encode($posts);
        }
    public function show_user_created_posts() {
        $this->view = false;
			$data = $this->input->post();

	 $userID = (isset($data['user_id'])) ? decode_id($data['user_id']) : $this->session->userdata['logged_in']['bs_id'];


        $posts = $this->post->show_user_created_posts($userID, $data);

        echo json_encode($posts);
    }

		function addfavourite(){

			$res = $this->post->addfavourite(decode_id($_POST['PostID']));
			echo $res ;
		}
	public function updateCounter(){

		$res = $this->post->updateCounter(decode_id($_POST['PostID']));

		echo $res ;

	}
    public function upload_image() {
        $this->view = false;
        $postID = decode_id($_POST['PostID']);
        $userID = ($this->session->userdata['logged_in']['bs_id']);

        if(isset($_POST['image'])) {
            $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $img_path = 'uploads/background/';
            $image_name = $img_path. $userID. "_". $postID. "_". time() . '.png';

            $this->post->set_changephoto($postID, $image_name, $image_name, $image_name);

            file_put_contents($image_name, $data);

            echo trim($image_name);
            exit;
        }
    }



    public function get_comments() {
        $this->view = false;
        $id = decode_id($this->input->post('PostID'));
        $searchTxt = $this->input->post('search_txt');

        if (empty($searchTxt))
            $comment_items = $this->comments_model->get_Comments($id);
        else
            $comment_items = $this->comments_model->get_comments_by_search($id, $searchTxt);

        $current_user_id = isset($this->session->userdata['logged_in']['bs_id']) ? $this->session->userdata['logged_in']['bs_id'] : '';
        $current_date = date('Y-m-d', strtotime('now'));

        foreach ($comment_items as $key => $row) {
            $comment_like_items = $this->comments_model->get_comment_likes($row['Co_ID']);
            $user_comment_like = $this->comments_model->get_user_comment_like($row['Co_ID'], $current_user_id);

            $comment_attachments = $this->comments_model->show_comment_attachment($row['Co_ID']);
            $comment_tags = $this->comments_model->get_comment_tag($row['Co_ID']);

            $pings = array();
            $comment = $row['Co_Message'];
            foreach ($comment_tags as $tag_key => $tag_row) {

                $name = (!empty($tag_row['Us_Alias'])) ?$tag_row['Us_Alias']: $tag_row['Us_Name'];
                $pings[$tag_row['Us_ID']] = $name;
                $comment = str_replace('@' . $tag_row['Us_ID'] . ' ', '@' . $name . ' ', $comment);
            }

            $comment_items[$key]['id'] = $row['Co_ID'];
            $comment_items[$key]['parent'] = ($row['Co_Parent_ID'] && empty($searchTxt)) ? $row['Co_Parent_ID'] : NULL;
            $comment_items[$key]['created'] = $row['Co_DatePosted'];
            $comment_items[$key]['modified'] = $row['Co_DateModified'];
            $comment_items[$key]['content'] = $comment;
            $comment_items[$key]['creator'] = $row['Co_Us_ID'];
            $comment_items[$key]['fullname'] = empty($row['Us_Alias']) ? $row['Us_Name'] : $row['Us_Alias'];
            $comment_items[$key]['profile_picture_url'] = $row['Us_Photo'];
            $comment_items[$key]['upvote_count'] = count($comment_like_items);
            $comment_items[$key]['user_has_upvoted'] = (count($user_comment_like)) ? true : false;
            $comment_items[$key]['created_by_current_user'] = ($row['Co_Us_ID'] == $current_user_id) ? true : false;
            $comment_items[$key]['is_new'] = ($current_date ==date('Y-m-d', strtotime($row['Co_DatePosted']))) ? true : false;
            $comment_items[$key]['pings'] = $pings;
            $comment_items[$key]['attachments'] = $comment_attachments;
            $comment_items[$key]['show_pin_icon'] =  true ;

//          $comment_items[$key]['show_pin_icon'] = ($row['Co_Us_ID'] == $current_user_id) ? true : false;
            $comment_items[$key]['is_pinned'] = (empty($row['Co_Pinned'])) ? false: true;
        }

        echo json_encode($comment_items);
    }

    public function comment_upvote() {
        $this->view = false;
        $comment_id = $this->input->post('comment_id');
        $upvote = $this->input->post('upvote', true);

        if ($upvote == 'true') {
            $this->comments_model->insert_like($comment_id);
        } else {
            $this->comments_model->delete_like($comment_id);
        }

        return true;
    }

    public function comment_pin() {
        $this->view = false;
        $comment_id = $this->input->post('comment_id');
        $is_pinned = $this->input->post('is_pinned', true);

        $data['Co_Pinned'] = ($is_pinned) ? 1 : 0;

        $this->comments_model->update_comment($comment_id, $data);

        return true;
    }

    public function savecomment() {
        $this->view = false;

        $post_data = $this->input->post();
        $PostID = decode_id($post_data['postID']);

        if (!isset($this->session->userdata['logged_in'])){
            redirect('/pages/view/signin'); //if session is not there, redirect to login page
        }


       		$UserID = ($this->session->userdata['logged_in']['bs_id']);
       	 	$post_record = $this->post->get_Record($PostID);
	    	$postAdminId = $post_record['Po_Us_ID'];
     		$postTitle = $post_record['Po_Title'];
     		$postIDx = $post_record['Po_ID'];
            $GroupID = $post_record['Po_Gr_ID'];
            $TopicID = $post_record['Po_To_ID'];

		if( isset($post_data['tags'][0]) && $post_data['tags'][0] == 'ALL'){
			$comment = str_replace('@0', '@All' , $post_data['content']);

			$post_data['content'] =  $comment ;

		}

        $data = array(
                        "Co_Gr_ID" => $GroupID,
                        "Co_To_ID" => $TopicID,
                        "Co_Po_ID" => $PostID,
                        "Co_Us_ID" => $UserID,
                        "Co_Message" => ((isset($post_data['content'])) ? $post_data['content'] : ''),
                        "Co_DatePosted" => date('Y-m-d H:i:s', strtotime('now')),
                        "Co_Parent_ID" => ((isset($post_data['parent'])) ? $post_data['parent'] : 0)
                    );

        $comment_id = $this->comments_model->Insert($data);


        if (!empty($post_data['tags'])) {

			if( isset($post_data['tags'][0]) && $post_data['tags'][0] == 'ALL'){
				$record = $this->groups_model->getuserDetailForallcomment($GroupID);


				foreach ($record  as $users){

 				$this->comments_model->insert_comment_tag(array('Ta_Co_ID' => $comment_id, 'Ta_Us_ID' => $users['Me_Us_ID']));


				}
			}else{
				foreach ($post_data['tags'] as $tag_user_id => $tag_username) {
					$this->comments_model->insert_comment_tag(array('Ta_Co_ID' => $comment_id, 'Ta_Us_ID' => $tag_user_id));
				}
			}

       	 }
			$uploadornot = 1;
        	if (!empty($_FILES)) {

					$postitem  = $this->GetAllpost($GroupID);
					$total_ammount_of_data = 0;
					$total_ammount_of_dataz = 0;

				foreach($postitem  as $item){
					$mediadata = $this->getmedia_per_post( $item['Po_ID']);
					foreach($mediadata as $singledata){


						try{
					 $total_ammount_of_data += filesize(str_replace(base_url() .'/', '', $singledata['Me_Path']));
						}catch(Exception $e){

						}

					}
					$mediacomment = 	 $this->getmedia_per_post_comment( $item['Po_ID']);
					foreach($mediacomment as $singledatac){

						try{
							$path = './uploads/post_attachment/'. $singledatac['At_Co_ID'] .'/';
							$dh = dir($path);
							while(($file=$dh->read()) !== false) {

								$realfile = $path . "/" . $singledatac['At_Filename'];

								$total_ammount_of_dataz += filesize($realfile);

							}

						}catch(Exception $e){

						}

					}
				}

					$res1 = 	$this->convert($total_ammount_of_dataz , 'GB');
				$res = 	$this->convert($total_ammount_of_data , 'GB');

					$total_spave = $res1 + $res;
					 if($total_spave > 1){
						 $uploadornot = 0;
					 }else{
						 $uploadornot = 1;
						 $path = './uploads/post_attachment/'. $comment_id .'/';
						 $this->upload_post_attachment($comment_id, $path, $_FILES);
					 }


        }

        $comment_items = $this->comments_model->get_Comments($PostID, $comment_id);
        $comment_attachments = $this->comments_model->show_comment_attachment($comment_id);
        $comment_tags = $this->comments_model->get_comment_tag($comment_id);




		if($UserID != $postAdminId){

		 $postlink = '<a href="'.base_url().'/account/view/post/'.encode_id($postIDx).'">';
              $post_link_end = '</a>';
              $notif_type = "comment_post_notification";
             $content = "Commented on your " . $postlink . $postTitle .$post_link_end;

             $this->notifications_model->add_notification($notif_type, ''.$comment_items[0]['Us_Name'].' do a comment on Your Post', $content, $UserID, 'user', $postAdminId , $comment_id);



		}

		 if(isset($post_data['taguserid'])){

							if (!empty($post_data['tags'])) {

								if( isset($post_data['tags'][0]) &&  $post_data['tags'][0] == 'ALL'){
									$record = $this->groups_model->getuserDetailForallcomment($GroupID);
									$data['users_item'] = $this->users_model->get_Users($this->session->userdata['logged_in']['bs_id']);

									foreach ($record  as $users){

										$Namelink = '<a href="'.base_url().'u/'.encode_id($data['users_item']['Us_ID']).'">';
										$Namelink_end = '</a>';

										$postlink = '<a href="'.base_url().'account/view/post/'.encode_id($postIDx).'">';
										$post_link_end = '</a>';
										$notif_type = "tag_in_comment_post_notification";
										$content = $Namelink. $data['users_item']['Us_Name'] . $Namelink_end.  " mentioned you in a comment on  " . $postlink . $postTitle .$post_link_end;
										$this->notifications_model->add_notification($notif_type, ''.$comment_items[0]['Us_Name'].' mention you in the Comment', $content,   $this->session->userdata['logged_in']['bs_id'] , 'user', $users['Me_Us_ID'] , $comment_id);


									}
								}else{

									$data['users_item'] = $this->users_model->get_Users($this->session->userdata['logged_in']['bs_id']);
									$Namelink = '<a href="'.base_url().'u/'.encode_id($data['users_item']['Us_ID']).'">';
									$Namelink_end = '</a>';

									$postlink = '<a href="'.base_url().'account/view/post/'.encode_id($postIDx).'">';
									$post_link_end = '</a>';
									$notif_type = "tag_in_comment_post_notification";
									$content = $Namelink. $data['users_item']['Us_Name'] . $Namelink_end.  " mentioned you in a comment on  " . $postlink . $postTitle .$post_link_end;
									$this->notifications_model->add_notification($notif_type, ''.$comment_items[0]['Us_Name'].' mention you in the Comment', $content,   $this->session->userdata['logged_in']['bs_id'] , 'user',$post_data['taguserid'] , $comment_id);


								}
							}

                    	}



        $pings = array();
        $comment = $post_data['content'];

		if( isset($post_data['tags'][0]) &&  $post_data['tags'][0] == 'ALL') {
			$comment = str_replace('@All', '' , $comment);
			$attachmee = '';
		}


		if( isset($post_data['tags'][0]) &&  $post_data['tags'][0] == 'ALL') {

			$name =  'All';

			$attachmee = 	$attachmee . '@' . $name . ' ';

			foreach ($comment_tags as $tag_key => $tag_row) {

				$name = (!empty($tag_row['Us_Alias'])) ?$tag_row['Us_Alias']: $tag_row['Us_Name'];
				$pings[$tag_row['Us_ID']] = $name;
			 	}
		}else{
			foreach ($comment_tags as $tag_key => $tag_row) {

				$name = (!empty($tag_row['Us_Alias'])) ?$tag_row['Us_Alias']: $tag_row['Us_Name'];
				$pings[$tag_row['Us_ID']] = $name;
				$comment = str_replace('@' . $tag_row['Us_ID'] . ' ', '@' . $name . ' ', $comment);
 				}

		}


		if( isset($post_data['tags'][0]) &&  $post_data['tags'][0] == 'ALL') {

			$comment = 	$attachmee . ' ' .$comment;
		}
        $post_data['id'] =  $comment_items[0]['Co_ID'];
        $post_data['parent'] = ($comment_items[0]['Co_Parent_ID']) ? $comment_items[0]['Co_Parent_ID'] : NULL;
        $post_data['creator'] = $comment_items[0]['Co_Us_ID'];
        $post_data['fullname'] = $comment_items[0]['Us_Name'];
        $post_data['profile_picture_url'] = $comment_items[0]['Us_Photo'];
        $post_data['upvote_count'] = 0;
        $post_data['user_has_upvoted'] = false;
        $post_data['created_by_current_user'] = true;
		$post_data['uploadornot'] = $uploadornot;
        $post_data['is_new'] = true;
        $post_data['content'] = $comment;
        $post_data['pings'] = $pings;
        $post_data['attachments'] = $comment_attachments;
//         $post_data['show_pin_icon'] = ($comment_items[0]['Co_Us_ID'] == $UserID) ? true : false;
        $post_data['show_pin_icon'] =   true ;
        $post_data['is_pinned'] = (empty($comment_items[0]['Co_Pinned'])) ? false: true;

        print json_encode($post_data);
        exit;
    }

    public function updatecomment() {
        $this->view = false;

        $post_data = $this->input->post();
        $current_time = date('Y-m-d H:i:s', strtotime('now'));
        $data = array("Co_Message" => $post_data['content'], 'Co_DateModified' => $current_time);

        $this->comments_model->update_comment($post_data['id'], $data);

        $this->comments_model->delete_tags($post_data['id']);

        if (!empty($post_data['tags'])) {
            foreach ($post_data['tags'] as $tag_user_id => $tag_username) {
                $this->comments_model->insert_comment_tag(array('Ta_Co_ID' => $post_data['id'], 'Ta_Us_ID' => $tag_user_id));
            }
        }

        $path = './uploads/post_attachment/'. $post_data['id'] .'/';

        if (!empty($post_data['existing_attachment'])) {
            $this->delete_comment_attachments($post_data['id'], $post_data['existing_attachment'], $path);
        }

        if (!empty($_FILES)) {
            $this->upload_post_attachment($post_data['id'], $path, $_FILES);
        }

        $comment_attachments = $this->comments_model->show_comment_attachment($post_data['id']);
        $comment_tags = $this->comments_model->get_comment_tag($post_data['id']);

        $pings = array();
        $comment = $post_data['content'];
//		print_r($comment);
//		exit;

        foreach ($comment_tags as $tag_key => $tag_row) {
            $name = (!empty($tag_row['Us_Alias'])) ?$tag_row['Us_Alias']: $tag_row['Us_Name'];
            $pings[$tag_row['Us_ID']] = $name;
            $comment = str_replace('@' . $tag_row['Us_ID'] . ' ', '@' . $name . ' ', $comment);
        }

        $post_data['content'] = $comment;
        $post_data['modified'] = $current_time;
        $post_data['attachments'] = $comment_attachments;
        $post_data['pings'] = $pings;

        print json_encode($post_data);
        exit;
    }

    public function deletecomment() {
        $this->view = false;
        $post_data = $this->input->post();

        $this->comments_model->delete_comment($post_data['id']);

        return true;
    }

    public function get_contacts() {
        $this->view = false;
        $current_user_id = $this->session->userdata['logged_in']['bs_id'];
        $keywords = $this->input->post('keywords');

 		$res =  $this->groups_model->getUserDetailForComment($_POST['groupId'] ,  $keywords  );
		$record = $this->groups_model->getuserDetailForallcomment($_POST['groupId']);
		$totalrecord = count($record);
        $data = array();

        if (!empty($res)) {
            foreach ($res as $key => $row) {

                $data[$key]['id'] = $row['Us_ID'];
                $data[$key]['fullname'] = (strlen( $row['Us_Alias'])==0) ?$row['Us_Name'] : $row['Us_Alias'];
                $data[$key]['email'] = $row['Us_Email'];
                $data[$key]['profile_picture_url'] = (strlen($row['Us_Thumb'])==0) ? base_url()."img/nophoto.png" : $row['Us_Thumb'];
            }
			$data[$key+1]['id'] = 0;
			$data[$key+1]['fullname'] = 'ALL';
			$data[$key+1]['email'] = 'Tag all user';
			$data[$key+1]['profile_picture_url'] = base_url()."img/nophoto.png" ;


		}
		$newdata = array('data'=> $data, 'total' => $totalrecord);
        echo json_encode($newdata);
    }
    private function upload_post_attachment($id, $path, $files) {
        if (!is_dir($path)) {
            mkdir($path, 0777, TRUE);
        }

        $config = array(
            'upload_path'   => $path,
            'allowed_types' => '*',
            'overwrite'     => 1,
        );
        $this->load->library('upload', $config);
        $file_count = count($files['attachment']['name']);

        $file = $files['attachment'];
        for ($key = 0 ; $key < $file_count; $key++) {
            $_FILES['files[]']['name']= $file['name'][$key];
            $_FILES['files[]']['type']= $file['type'][$key];
            $_FILES['files[]']['tmp_name']= $file['tmp_name'][$key];
            $_FILES['files[]']['error']= $file['error'][$key];
            $_FILES['files[]']['size']= $file['size'][$key];
            $file_ext = pathinfo($file['name'][$key],PATHINFO_EXTENSION);
            $config['file_name'] = $file['name'][$key];
            $this->upload->initialize($config);
            if ($this->upload->do_upload('files[]')) {
                $this->upload->data();
                $filename = $this->upload->file_name;
                $fileDataArr = [];
                $fileDataArr['At_Co_ID'] = $id;
                $fileDataArr['At_Orig_Filename'] = $filename;
                $fileDataArr['At_Filename'] = $filename;
                $fileDataArr['At_File_Type'] = $file['type'][$key];
                $this->comments_model->insert_attachment($fileDataArr);
            } else {
                return false;
            }
        }
        return $files;
    }

    function delete_comment_attachments($id, $attachment_ids, $path) {

        $comment_attachments = $this->comments_model->get_attachments($id, $attachment_ids);

        if (!empty($comment_attachments)) {

            foreach($comment_attachments as $key => $row) {
                $this->comments_model->delete_attachment($row['At_ID']);

                unlink($path. '/' . $row['At_Filename']);
            }
        }
    }

    function get_post_uploaded_files() {
        $this->view = false;
        $id = decode_id($this->input->post('id'));

        $data = array( 'url' => array(), 'initialPreviewConfig' => array());

        $posts_files = $this->medias_model->get_PostMedia($id,'files');
        $posts_images = $this->medias_model->get_PostMedia($id,'images');

         $data['images']['url'] = array();
         $data['images']['initialPreviewConfig'] = array();
         $data['files']['url'] = array();
         $data['files']['initialPreviewConfig'] = array();

        if (!empty($posts_images)) {
            foreach($posts_images as $idx => $file) {
                $data['images']['url'][] = $file['Me_Path'];
                $data['images']['initialPreviewConfig'][] = array('caption' => $file['Me_Filename'], 'downloadUrl' => $file['Me_Path'], 'key' => $file['Me_ID'], 'showDrag' => false);
            }
        }
        if (!empty($posts_files)) {
            foreach($posts_files as $idx => $file) {
                $data['files']['url'][] = $file['Me_Path'];
                $data['files']['initialPreviewConfig'][] = array(
				'size' => filesize(str_replace(base_url() .'/', '', $file['Me_Path'])),
				'filetype' => mime_content_type(str_replace(base_url() .'/', '', $file['Me_Path'])),
				'type' => pathinfo($file['Me_Path'], PATHINFO_EXTENSION),
				'caption' => $file['Me_Filename'],
				'downloadUrl' => $file['Me_Path'],
				'key' => $file['Me_ID'],
				'showDrag' => false,
				'showZoom' => false);
            }
        }
        echo json_encode($data);
    }

    function delete_post_uploaded_file() {
        $this->view = false;
        $id =$this->input->post('key');

        $media = $this->medias_model->get_record($id);

        // unlink($media['Me_Path']);
        $this->medias_model->delete_record($id);

        echo json_encode(true);
    }

			public function GetAllpost($id){
			$data = 	$this->db->select('bs_posts.*')->from('bs_posts')->where('Po_Gr_ID' ,$id)->get()->result_array();
			return $data;
			}

			public function getmedia_per_post($poid){
					$data = array(
					'Me_Parent' => $poid,
					'Me_Type' => 'Post'
					);
				$response = 	$this->db->select('bs_medias.*')->from('bs_medias')->where($data)->get()->result_array();
 				return $response;

			}
			public function getmedia_per_post_comment($poid){
				$data = array(
					'bs_comments.Co_Po_ID' => $poid,

				);
					$this->db->select('bs_comments.* , bs_comment_attachment.*')->from('bs_comments');
				$this->db->join('bs_comment_attachment' , 'bs_comment_attachment.At_Co_ID = bs_comments.Co_ID');
				$response = 	$this->db->where($data)->get()->result_array();


				return $response;

			}
			function convert($size,$unit)
            {
             if($unit == "KB")
             {
              return $fileSize = round($size / 1024,4);
             }
             if($unit == "MB")
             {
              return $fileSize = round($size / 1024 / 1024,4);
             }
             if($unit == "GB")
             {
              return $fileSize = round($size / 1024 / 1024 / 1024,4) ;
             }
            }

    function upload_post_file() {
        $this->view = false;
        $UserID = ($this->session->userdata['logged_in']['bs_id']);
        $id = decode_id($this->input->post('PostID'));
         $group_id =  $this->input->post('groupid') ;


 	    $type = $this->input->post('type');
        $input = $this->input->post('input');

	    $postitem  = $this->GetAllpost($group_id);
		 $total_ammount_of_data = 0;


		foreach($postitem  as $item){
			$mediadata = $this->getmedia_per_post( $item['Po_ID']);
				foreach($mediadata as $singledata){


					try{
//						$total_ammount_of_data += filesize(str_replace(base_url() .'/', '', $singledata['Me_Path']));

					}catch(Exception $e){

					}

				}


		}
  			$res = 	$this->convert($total_ammount_of_data , 'GB');

				 if($res >= 1){
				  $out = array(
					  'message'=>'error' ,
					  'reason' => 'Space Exceed'
				  );
					 echo json_encode($out);
					 exit;
				 }
        if (!array_key_exists($input, $_FILES)) {
            $out = array('message'=>'error');
            echo json_encode($out);
            exit;
        }

        $Path = 'uploads/'.$UserID. '/posts/'.$id;

        $ret = $this->uploadphoto($Path, $_FILES[$input]);

        $dataArr = [];
        $dataArr['type'] = 'Post';
        $dataArr['parentId'] = $id;
        $dataArr['filetype'] = $type;
        $dataArr['path'] = base_url() . '/' .$Path . '/' . $ret;
        $dataArr['filename'] = $_FILES[$input]['name'][0];

        $mediaID = $this->medias_model->insert($dataArr);

        $preview[] = $dataArr['path'];
        $config[] =  array(
			'key' => $mediaID,
			'caption' => $_FILES[$input]['name'][0],
			'size' => $_FILES[$input]['size'][0],
			'downloadUrl' => $dataArr['path'], // the url to download the file
			'showDrag' => false
		);
        $out = array('message'=>'success', 'initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true);

        echo json_encode($out);
    }

    public function uploadphoto($UploadDir, $UserFile)
    {
        if (empty($UserFile['name'])) {
            return 'Error: Empty';
        }
        else {
            $_FILES['userfile']['name'] = $UserFile['name'][0];
            $_FILES['userfile']['type'] = $UserFile['type'][0];
            $_FILES['userfile']['tmp_name'] = $UserFile['tmp_name'][0];
            $_FILES['userfile']['error'] = $UserFile['error'][0];
            $_FILES['userfile']['size'] = $UserFile['size'][0];
        }

        if (!is_dir($UploadDir)) {
            mkdir($UploadDir, 0777, TRUE);
        }

        $new_name = time().$UserFile['name'][0];

        $config['upload_path']          = $UploadDir;
        $config['file_name']            = $new_name;
        $config['allowed_types']        = '*';
        $config['max_size']             = 100000;
        $config['max_width']            = 0;
        $config['max_height']           = 0;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload()) {
            return 'Error: '.$this->upload->display_errors();
        }

        $upload_data = $this->upload->data();
        $FileName = $upload_data['file_name'];

        return $FileName;
    }
}
