<?php
class Admin extends CI_Controller {

	public function __construct()
    {
            parent::__construct();
            $this->load->model('users_model');
            $this->load->model('settings_model');
            $this->load->model('category_model');
			$this->load->model('background_model');
			$this->load->model('inquiries_model');
			$this->load->model('keyword_model');
            $this->load->helper('url_helper');
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');

			if ($this->session->userdata['logged_in']['bs_admin'] == 0) {
				header('Location: ' . base_url() . 'home');
			}
    }

    public function index()
    {
		$data['title'] = 'Dashboard';
		$data['selMenu'] = 'dashboard';

		$data['UserName'] = ($this->session->userdata['logged_in']['bs_name']);
		$data['error'] = "";
		$data['notification'] = "";
		$data['asset'] = 'admin/admin';


		$data['selSubMenu'] = 'dashboard';
		$data['users_item'] = $this->users_model->get_admins();


		$data['settings_items'] = $this->settings_model->get_Background();
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
        $data['category_items'] = $this->category_model->get_Category();
        $data['keyword_items'] = $this->keyword_model->get_records();

	    $this->load->view('templates/header', $data);
	    $this->load->view('templates/navigation', $data);
	    $this->load->view('templates/admin_header', $data);
		$this->load->view('admin/index.php', $data);
    	$this->load->view('templates/footer', $data);
    }


	public function removeadmin(){


	$res = 	$this->users_model->removeadmin($_POST['id']);
	echo $res;
	}

    public function users($option = false, $id = false)
    {
		if (!isset($this->session->userdata['logged_in'])){
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = 'Users';
		$data['selMenu'] = 'users';

		$data['UserName'] = ($this->session->userdata['logged_in']['bs_name']);
		$data['error'] = "";
		$data['notification'] = "";
		$data['asset'] = 'admin/admin';



		if (strcmp($option,'delete')==0)
		{
			$this->users_model->delete_row($id);
			redirect('/admin/users'); //if session is not there, redirect to login page
			return;
		}
		else if (strcmp($option,'detail')==0)
		{
			$data['users_item'] = $this->users_model->get_Users($id);
			$data['txtID'] = $data['users_item']['Us_ID'];
			$data['txtFirstName'] = $data['users_item']['Us_FName'];
			$data['txtLastName'] = $data['users_item']['Us_LName'];
			$data['txtEmail'] = $data['users_item']['Us_Email'];
			$data['txtPhone'] = $data['users_item']['Us_Phone'];
			$data['txtCompany'] = $data['users_item']['Us_Company'];
			$data['txtJobTitle'] = $data['users_item']['Us_JobTitle'];
			$data['txtNoOfEmployees'] = $data['users_item']['Us_NoOfEmployees'];
			$data['txtFeatures'] = $data['users_item']['Us_Features'];
			$data['txtStatus'] = $data['users_item']['Us_Status'];

		}
		else if (strcmp($option,'save')==0)
		{
			$data['txtID'] = $this->input->post('txtID');

			if (strlen($data['txtID'])==0) $data['error'] = $this->savegroupcategory();
			else $data['error'] = $this->updategroupcategory();
		}
		else
		{
			$data['users_item'] = $this->users_model->get_Users();
		}



	    $this->load->view('templates/header', $data);
	    $this->load->view('templates/navigation', $data);
	    $this->load->view('templates/admin_header', $data);

		if (strcmp(strtolower($option),'detail')==0) $this->load->view('admin/users_form', $data);
		else $this->load->view('admin/users', $data);

    	$this->load->view('templates/footer', $data);
    }

	public function message($option = false, $id = false)
    {
		if (!isset($this->session->userdata['logged_in'])){
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = 'Messages';
		$data['selMenu'] = 'messages';

		$data['UserName'] = ($this->session->userdata['logged_in']['bs_name']);
		$data['error'] = "";
		$data['notification'] = "";
		$data['asset'] = 'admin/admin';



		if (strcmp($option,'delete')==0)
		{
			$this->inquiries_model->delete_row($id);
			redirect('/admin/message'); //if session is not there, redirect to login page
			return;
		} else if (strcmp($option,'detail')==0) {
			$data['inquiries_item'] = $this->inquiries_model->get_Records($id);
			$data['txtID'] = $data['inquiries_item']['In_ID'];
			$data['txtFirstName'] = $data['inquiries_item']['In_FName'];
			$data['txtLastName'] = $data['inquiries_item']['In_LName'];
			$data['txtEmail'] = $data['inquiries_item']['In_Email'];
			$data['txtSubject'] = $data['inquiries_item']['In_Subject'];
			$data['txtMessage'] = $data['inquiries_item']['In_Message'];
		} else if (strcmp($option,'reply')==0) {
			$data['inquiries_item'] = [] ;
			$data['inquiries_item'][0] = $this->inquiries_model->get_Records($id);

			$data['txtID'] = $data['inquiries_item'][0]['In_ID'];
			$data['txtFirstName'] = $data['inquiries_item'][0]['In_FName'];
			$data['txtLastName'] = $data['inquiries_item'][0]['In_LName'];
			$data['txtEmail'] = $data['inquiries_item'][0]['In_Email'];
			$data['txtSubject'] = $data['inquiries_item'][0]['In_Subject'];
			$data['txtMessage'] = $data['inquiries_item'][0]['In_Message'];
		} else {
			$data['inquiries_item'] = $this->inquiries_model->get_Records();
		}

	    $this->load->view('templates/header', $data);
	    $this->load->view('templates/navigation', $data);
	    $this->load->view('templates/admin_header', $data);

		if (strcmp(strtolower($option),'detail')==0) $this->load->view('admin/inquiries_form', $data);
		else $this->load->view('admin/inquiries', $data);

    	$this->load->view('templates/footer', $data);
    }


    public function background($option = false, $id = false, $subid = false)
    {
		if (!isset($this->session->userdata['logged_in'])){
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = 'Background';
		$data['selMenu'] = 'background';

		$data['UserName'] = ($this->session->userdata['logged_in']['bs_name']);
		$data['error'] = "";
		$data['notification'] = "";
		$data['asset'] = 'admin/admin';



		if (strcmp($option,'delete')==0)
		{
			$this->background_model->delete_row($id);
			redirect('/admin/background'); //if session is not there, redirect to login page
			return;
		}
		else if (strcmp($option,'deletephoto')==0)
		{
			$this->background_model->delete_bgphoto($subid);
			redirect('/admin/background/photos/'.$id);
			return;
		}
		else if ((strcmp($option,'detail')==0) || (strcmp($option,'photos')==0))
		{
			$data['bgcategory_item'] = $this->background_model->get_Record($id);
			$data['txtID'] = $data['bgcategory_item']['BgC_ID'];
			$data['txtName'] = $data['bgcategory_item']['BgC_Name'];
			$data['txtOrder'] = $data['bgcategory_item']['BgC_Order'];

			$data['bgphotos_items'] = $this->background_model->get_BgPhotos($id);
		}
		else if (strcmp($option,'addnew')==0)
		{
			$data['txtID'] = '';
			$data['txtName'] = '';
			$data['txtOrder'] = '';
		}
		else if (strcmp($option,'save')==0)
		{
			$data['txtID'] = $this->input->post('txtID');

			if (strlen($data['txtID'])==0) $data['error'] = $this->savebgcategory();
			else $data['error'] = $this->updatebgcategory();
		}
		else if (strcmp($option,'savephoto')==0)
		{
			$data = $this->savebgphoto($data);
		}
		else
		{
			$data['bgcategory_items'] = $this->background_model->get_Record();
		}



	    $this->load->view('templates/header', $data);
	    $this->load->view('templates/navigation', $data);
	    $this->load->view('templates/admin_header', $data);

		if ((strcmp(strtolower($option),'addnew')==0) || (strcmp(strtolower($option),'detail')==0)) $this->load->view('admin/background_form', $data);
		else if ((strcmp(strtolower($option),'photos')==0) || (strcmp(strtolower($option),'savephoto')==0)) $this->load->view('admin/background_photos', $data);
		else $this->load->view('admin/background', $data);

    	$this->load->view('templates/footer', $data);
    }

	public function savebgcategory()
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtName', 'Name', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			return 'Invalid Entry';
	    }
	    else
	    {
		    $txtName = $this->input->post('txtName');
			$txtOrder = $this->input->post('txtOrder');
			$this->background_model->Insert($txtName, $txtOrder);

			redirect('/admin/background');
	    }
	}

	public function updatebgcategory()
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtName', 'Name', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			return 'Invalid Entry';
	    }
	    else
	    {
			$txtID = $this->input->post('txtID');
		    $txtName = $this->input->post('txtName');
			$txtOrder = $this->input->post('txtOrder');
			$this->background_model->Update($txtID, $txtName, $txtOrder);

			redirect('/admin/background');
	    }
	}
	
	public function savebgphoto($data)
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtName', 'Name', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			$data['error'] = 'Invalid Entry';
	    }
	    else
	    {
			$txtID = $this->input->post('txtID');
		    $txtName = $this->input->post('txtName');
			$PrimaryID = $this->background_model->InsertPhoto($txtID, $txtName);
			$data['error'] = $this->uploadbgphoto($PrimaryID);

			if (strlen($data['error'])==0) redirect('/admin/background/photos/'.$txtID);
	    }
		
		return $data;
	}

	public function uploadbgphoto($ID)
    {
		$Error = $this->uploadphoto('./uploads/background/', 'userfile');
		$ErrorMsg = substr($Error,0,6);
		if (strcmp('Error:',$ErrorMsg)==0)
		{
			return $Error;
		}
		else
		{
			$this->resizeImage('background',$Error);
			$this->background_model->set_photo($ID, 'background', $Error);
		}

		return '';
    }



    public function category($option = false, $id = false, $subid = false)
    {
		if (!isset($this->session->userdata['logged_in'])){
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = 'Category';
		$data['selMenu'] = 'category';

		$data['UserName'] = ($this->session->userdata['logged_in']['bs_name']);
		$data['error'] = "";
		$data['notification'] = "";
		$data['asset'] = 'admin/admin';
		


		if (strcmp($option,'delete')==0)
		{
			$this->category_model->delete_row($id);
			redirect('/admin/category'); 
			return;
		}
		else if (strcmp($option,'deletesub')==0)
		{
			$this->category_model->delete_sub($subid);
			redirect('/admin/category/sub/'.$id); 
			return;
		}
		else if (strcmp($option,'detail')==0)
		{
			$data['category_item'] = $this->category_model->get_Record($id);
			$data['txtID'] = $data['category_item']['Ca_ID'];
			$data['txtName'] = $data['category_item']['Ca_Name'];
			$data['selType'] = $data['category_item']['Ca_Type'];
			$data['default'] = $data['category_item']['Ca_Default'];
		}
		else if (strcmp($option,'sub')==0)
		{
			$data['category_item'] = $this->category_model->get_Record($id);
			$data['subcategory_items'] = $this->category_model->get_SubCategory($id);
			$data['subcategory_item'] = $this->category_model->get_SubCategory($id,$subid);
			
			$data['txtID'] = $data['category_item']['Ca_ID'];
			$data['txtName'] = $data['category_item']['Ca_Name'];
			$data['selType'] = $data['category_item']['Ca_Type'];
			$data['txtSubID'] = '';
			$data['txtSubName'] = ''; 	

			if ($subid!=false)
			{
				$data['txtSubID'] = $data['subcategory_item']['Sc_ID'];
				$data['txtSubName'] = $data['subcategory_item']['Sc_Name']; 	
			}
		}
		else if (strcmp($option,'addnew')==0)
		{
			$data['txtID'] = '';
			$data['txtName'] = '';
			$data['selType'] = '';
			$data['default'] = '';
		}
		else if (strcmp($option,'save')==0)
		{
			$data['txtID'] = $this->input->post('txtID');
			
			$default = $this->input->post('default');
			$data['default'] = 0;
			if(isset($default)) {
				if($this->input->post('default') == 'on' || $this->input->post('default') == 1){
					$data['default'] = 1;
				}	
			}

			if (strlen($data['txtID'])==0) $data['error'] = $this->savecategory();
			else $data['error'] = $this->updatecategory();
		}
		else if (strcmp($option,'savesub')==0)
		{
			$data['txtSubID'] = $this->input->post('txtSubID');
		
			if (strlen($data['txtSubID'])==0) $data['error'] = $this->savesubcategory();
			else $data['error'] = $this->updatesubcategory();
		}
		else
		{
			$data['category_items'] = $this->category_model->get_Category();
		}



	    $this->load->view('templates/header', $data);
	    $this->load->view('templates/navigation', $data);
	    $this->load->view('templates/admin_header', $data);

		if ((strcmp(strtolower($option),'addnew')==0) || (strcmp(strtolower($option),'detail')==0)) $this->load->view('admin/category_form', $data);
		else if (strcmp(strtolower($option),'sub')==0) $this->load->view('admin/category_sub', $data);
		else $this->load->view('admin/category', $data);

    	$this->load->view('templates/footer', $data);
		
    }

	
    
	
	public function savecategory()
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtName', 'Name', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			return 'Invalid Entry';
	    }
	    else
	    {
		    $txtName = $this->input->post('txtName');
			$selType = $this->input->post('selType');
			
			$default = 0;
			if(isset($default)) {
				if($this->input->post('default') == 'on' || $this->input->post('default') == 1){
					$default = 1;
				}	
			}

			$this->category_model->Insert($txtName, $selType, $default);
			
			redirect('/admin/category');
	    }
	}

	public function updatecategory()
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtName', 'Name', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			return 'Invalid Entry';
	    }
	    else
	    {
			$txtID = $this->input->post('txtID');
		    $txtName = $this->input->post('txtName');
			$selType = $this->input->post('selType');

			$default = $this->input->post('default');
			
			if(isset($default)) {
				if($this->input->post('default') == 'on' || $this->input->post('default') == 1){
					$default = 1;
				}	
			}

			$this->category_model->Update($txtID, $txtName, $selType, $default);

			redirect('/admin/category');
	    }
	}

	
	public function savesubcategory()
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtSubName', 'Name', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			return 'Invalid Entry';
	    }
	    else
	    {
		    $txtName = $this->input->post('txtSubName');
			$txtID = $this->input->post('txtID');
			$this->category_model->InsertSub($txtID, $txtName);
			
			redirect('/admin/category/sub/'.$txtID);
	    }
	}

	
	public function updatesubcategory()
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtSubName', 'Name', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			return 'Invalid Entry';
	    }
	    else
	    {
			$txtID = $this->input->post('txtID');
			$txtSubID = $this->input->post('txtSubID');
		    $txtName = $this->input->post('txtSubName');
			$this->category_model->UpdateSub($txtSubID, $txtName);

			redirect('/admin/category/sub/'.$txtID);
	    }
	}



	

    public function uploadphoto($uploadDir, $UserFile)
    {

		if (!is_dir($uploadDir)) {
		    mkdir($uploadDir, 0777, TRUE);
		}

		$new_name = time().$_FILES[$UserFile]['name'];

        $config['upload_path']          = $uploadDir;
		$config['file_name'] 			= $new_name;
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
        $config['max_width']            = 0;
        $config['max_height']           = 0;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($UserFile))
        {
                return 'Error: '.$this->upload->display_errors();
        }

        $upload_data = $this->upload->data();
        $FileName = $upload_data['file_name'];
        return $FileName;
    }


       public function resizeImage($Folder,$Filename)
	   {
	      $source_path = './uploads/'.$Folder.'/'.$Filename;
	      $target_path = './uploads/'.$Folder.'/thumbnail/';

		  if (!is_dir($target_path)) {
		    mkdir($target_path, 0777, TRUE);
			}

	      $config_manip = array(
	          'image_library' => 'gd2',
	          'source_image' => $source_path,
	          'new_image' => $target_path,
	          'maintain_ratio' => TRUE,
	          'create_thumb' => TRUE,
	          'thumb_marker' => '',
	          'height' => 250
	      );

	      $this->load->library('image_lib', $config_manip);
	      if (!$this->image_lib->resize()) {
        	return  $this->image_lib->display_errors();
	      }
	      $this->image_lib->clear();

		  $config_manip = array(
	          'image_library' => 'gd2',
	          'source_image' => $target_path.$Filename,
	          'new_image' => $target_path,
	          'maintain_ratio' => FALSE,
	          'quality' => '100%',
	          'width' => 400,
	          'height' => 250
	      );

		  $this->image_lib->initialize($config_manip);
	      if (!$this->image_lib->crop()) {
        	return  $this->image_lib->display_errors();
	      }

		  $this->image_lib->clear();
		  return "";

	   }


    public function keyword($option = false, $id = false) {
		if (!isset($this->session->userdata['logged_in'])){
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
		}

		$data['title'] = 'Keyword';
		$data['selMenu'] = 'keyword';

		$data['UserName'] = ($this->session->userdata['logged_in']['bs_name']);
		$data['error'] = "";
		$data['notification'] = "";
		$data['asset'] = 'admin/admin';

		if (strcmp($option,'delete')==0) {
			$this->keyword_model->delete($id);
			redirect('/admin/keyword'); 
			return;

		} else if (strcmp($option,'detail')==0) {
			$data['keyword_item'] = $this->keyword_model->get_Record($id);
			$data['txtID'] = $data['keyword_item']['Kw_ID'];
			$data['txtName'] = $data['keyword_item']['Kw_Keyword'];
			$data['banned'] = $data['keyword_item']['Kw_Banned'];

		} else if (strcmp($option,'addnew')==0) {
			$data['txtID'] = '';
			$data['txtName'] = '';
			$data['banned'] = '';

		} else if (strcmp($option,'save')==0) {
			$data['txtID'] = $this->input->post('txtID');
			$banned = $this->input->post('banned');
			
			if(isset($banned)) {
				if($this->input->post('banned') == 'on' || $this->input->post('banned') == 1){
					$data['banned'] = 1;
				}	
			}

			if (strlen($data['txtID'])==0) $data['error'] = $this->save_keyword();
			else $data['error'] = $this->update_keyword();

		} else {
			$data['keyword_items'] = $this->keyword_model->get_records();
		}

	    $this->load->view('templates/header', $data);
	    $this->load->view('templates/navigation', $data);
	    $this->load->view('templates/admin_header', $data);

		if ((strcmp(strtolower($option),'addnew')==0) || (strcmp(strtolower($option),'detail')==0)) $this->load->view('admin/keyword_form', $data);
		else $this->load->view('admin/keyword', $data);

    	$this->load->view('templates/footer', $data);
    }

	public function save_keyword() {
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtName', 'Keyword', 'required');

	    if ($this->form_validation->run() === FALSE) {
			return 'Invalid Entry';

	    } else {
		    $keyword = $this->input->post('txtName');
			$banned = $this->input->post('banned');

			if(isset($banned)) {
				if($this->input->post('banned') == 'on' || $this->input->post('banned') == 1){
					$banned = 1;
				}	
			}

			$data = array('Kw_Keyword' => $keyword, 'Kw_Banned' => $banned);

			$this->keyword_model->insert($data);
			
			redirect('/admin/keyword');
	    }
	}

	public function update_keyword()
	{
		$this->load->helper('form');
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('txtName', 'Keyword', 'required');
	    if ($this->form_validation->run() === FALSE)
	    {
			return 'Invalid Entry';
	    }
	    else
	    {
			$txtID = $this->input->post('txtID');
		    $txtName = $this->input->post('txtName');
			$banned = $this->input->post('banned');
			
			if(isset($banned)) {
				if($this->input->post('banned') == 'on' || $this->input->post('banned') == 1){
					$banned = 1;
				}	
			}

			$data = array('Kw_Keyword' => $txtName, 'Kw_Banned' => $banned);

			$this->keyword_model->update($txtID, $data);

			redirect('/admin/keyword');
	    }
	}
}
