<?php
class Pages extends CI_Controller {


	private $bs = null;


	public function __construct()
    {
		parent::__construct();
		$this->load->model('users_model');
		$this->load->model('inquiries_model');
		$this->load->helper('url_helper');
		$this->bs =& get_instance();
		// Load session library
		$this->load->library('session');
		$this->load->library('EmailManager');

		$this->load->library('PHPMailerManager');
		$this->load->library('PasswordManager');
    }

			function addWalletCredit($points , $id ){

				$this->db->set('Us_Coins', 'Us_Coins + ' . (int) $points, FALSE);
				$this->db->where('Us_ID' , $id);
			return 	$this->db->update('bs_users');
			}
		public	function addcoins($id){
				$data = array(
				'Cr_ID' => 1,
				'Cg_title' => 'Login Reward' ,
					'Cg_coins' => 50,
					'Cg_date' => date(),
					'Cg_US_ID' => $id

				);
			 	$this->db->insert('coins_get' , $data);
			}
	public	function checkUserLoginReward($ID){
				$this->db->select('coins_get.*');
				$this->db->from('coins_get');
				$this->db->where('Cg_US_ID' , $ID);
				$this->db->where('Cr_ID' , 1);
				$res = $this->db->get()->result_array();
							if(count($res) > 0){
								return 1;
							}else{
								return  0;
							}
		}
    public function view($page = 'home') {

        $data['title'] = ucfirst($page); // Capitalize the first letter
	    if (strcmp($page,'signin')==0)
        {

	        $data['error_message'] = "";

	        $this->load->helper('form');
		    $this->load->library('form_validation');

		    $this->form_validation->set_rules('txtEmail', 'Email', 'required');
		    $this->form_validation->set_rules('txtPassword', 'Password', 'required');



		    if ($this->form_validation->run() === FALSE)
		    {

			    if(isset($this->session->userdata['logged_in'])){

					if(isset($_GET['getback'])){



						redirect('/account/view/'.$_GET['getback'].'/'.$_GET['pageID']);
					}else{
						redirect('/home');
					}

				}
				else{

					$this->load->view('templates/signin_header', $data);
	    			$this->load->view('pages/signin', $data);
				}
			}
			else
			{
				$data['usersx'] = $this->users_model->get_UserSignInonlyEmail();
				if(!empty($data['usersx']) && $data['usersx']['Us_Status'] != 'approved'){
					$data['error_message'] = "You haven’t activated your account. Pls request for activation email below, if you haven’t received any"; // Capitalize the first letter
					$this->load->view('templates/signin_header', $data);
					$this->load->view('pages/signin', $data);
				}else{
					$data['users'] = $this->users_model->get_UserSignIn();

					if (!empty($data['users']))
					{

						$plaintext_password = $this->input->post('txtPassword');
						$verify = password_verify($plaintext_password, $data['users']['Us_Password'] );


						if ($verify) {
							$user_groups = $this->users_model->count_user_created_groups($data['users']['Us_ID']);

							$session_data = array(
								'Us_DateTime' => $data['users']['Us_DateTime'],
								'Us_Photo' => $data['users']['Us_Photo'],
								'bs_thumb' => $data['users']['Us_Thumb'],
								'bs_email' => $data['users']['Us_Email'],
								'bs_alias' => $data['users']['Us_Alias'],
								'bs_admin' => $data['users']['Us_IsAdmin'],
								'bs_mutelimit' => $data['users']['Us_Mute_Limit'],
								'bs_name' => $data['users']['Us_Name'],
								'bs_id' => $data['users']['Us_ID'],
								'bs_group' => ($data['users']['Us_ShowCreateGroupPopUp'] == 1) ? true : false,
								'bs_group_count' => $user_groups
							);
							$this->session->set_userdata('logged_in', $session_data);
							$ress = $this->checkUserLoginReward($data['users']['Us_ID']);
							if($ress == 0){
								$this->addcoins($data['users']['Us_ID']);
								$this->addWalletCredit(50 , $data['users']['Us_ID']);
							}
							if(isset($_POST['getback'])){


								redirect('/account/view/'.$_POST['getback'].'/'.$_POST['pageID']);
							}else{
								redirect('/home');
							}

						} else {
							$data['error_message'] = "Wrong Password!";
							$this->load->view('templates/signin_header', $data);
							$this->load->view('pages/signin', $data);
						}


					}
					else if(empty($data['users'])){
						$data['error_message'] = "User data not found!"; // Capitalize the first letter
						$this->load->view('templates/signin_header', $data);
						$this->load->view('pages/signin', $data);
					}
				}


			}
        }

        else if(strcmp($page , 'update-password' )==0){

		  $plaintext_password = $_POST['txtpassword'];
  		 $hash = password_hash($plaintext_password,
             PASSWORD_DEFAULT);

				$data = array(
				'Us_ID' => $_POST['userID']

				);
				$update = array(
				'Us_Password' => $hash

				);
				$this->db->where($data);
			$res = $this->db->update('bs_users' , $update);
				if($res){

				redirect('');


				}else{
				redirect('');


				}


        }

        else if( strcmp($page , 'reset-password' )==0){


				$this->db->select('bs_users.*');
				$this->db->from('bs_users');
				$this->db->where('Us_Email' , $_POST['txtEmail']);
				$query = $this->db->get()->result_array();


				if(count($query) == 0){
				   $this->load->helper('form');
                		    $this->load->library('form_validation');
                			$data['error_message'] = "Sorry this email is not registered with us";
                			$this->load->view('templates/signin_header', $data);
                			$this->load->view('pages/forgot', $data);
				}else{



			 $response = $this->emailmanager->forgetPasswordverification(	$query[0]['Us_ID'],$query[0]);

				$data['error_message'] = "";
       			$this->load->view('templates/signin_header', $data);
       			$this->load->view('pages/instruction', $data);
				}


        }
		else if( strcmp($page , 'resend-password' )==0){


			$this->db->select('bs_users.*');
			$this->db->from('bs_users');
			$this->db->where('Us_Email' , $_POST['txtEmail']);
			$query = $this->db->get()->result_array();


			if(count($query) == 0){
				$this->load->helper('form');
				$this->load->library('form_validation');
				$data['error_message'] = "Sorry this email is not registered with us";
				$this->load->view('templates/signin_header', $data);
				$this->load->view('pages/resendemail', $data);
			}else{

				$data['sFName'] = 	$query[0]['Us_FName'];
				$data['sLName'] = 	$query[0]['Us_LName'];
				$data['sEmail'] = 	$query[0]['Us_Email'];
				$data['sPhone'] = 	$query[0]['Us_Phone'];
				$data['sBirthDate'] = 	$query[0]['Us_BirthDate'];
				$data['sCompany'] = 	$query[0]['Us_Company'];
				$data['sJobTitle'] = 	$query[0]['Us_JobTitle'];
				$data['sNoOfEmployees'] = 	$query[0]['Us_NoOfEmployees'];
				$data['sFeatures'] = 	$query[0]['Us_Features'];
				$password = $this->passwordmanager->generatePassword();

				$data['password'] = $password ;

				$plaintext_password = $password;
				$hash = password_hash($plaintext_password, PASSWORD_DEFAULT);
//				$response = $this->emailmanager->sendRegistrationVerificationEmail($user_id , $data, $password , $message);

				$data['id'] = $query[0]['Us_ID'];
				$message = $this->load->view('templates/email/Registration', $data, TRUE);
				$where  = array(
					'Us_ID' => 	$query[0]['Us_ID']
				);
				$update  = array(
							'Us_Password' => $hash
				);
				$this->db->where($where);
			    $this->db->update('bs_users' , $update);



				try {
					$response = $this->emailmanager->sendRegistrationVerificationEmail(	$query[0]['Us_ID'], $data, $password , $message);

					$data['error_message'] = "";
					$this->load->view('templates/signin_header', $data);
					$this->load->view('pages/successresend', $data);


				} catch (Exception $e) {
					//alert the user.
					print_r( $e->getMessage());
				}

			}


		}


        else if (strcmp($page,'logout')==0)
        {
	        // Removing session data
			$sess_array = array('bs_email' => '','bs_alias' => '');

			$this->session->unset_userdata('logged_in');
		    redirect('');

        }
        else if (strcmp($page,'signup')==0)
        {
	        $this->signup();
        }
        else if (strcmp($page,'success')==0)
        {
			$data['error_message'] = "";
			$this->load->view('templates/signin_header', $data);
			$this->load->view('pages/success', $data);
        }
        else if (strcmp($page,'forgot')==0)
        {
	        $this->load->helper('form');
		    $this->load->library('form_validation');
			$data['error_message'] = "";
			$this->load->view('templates/signin_header', $data);
			$this->load->view('pages/forgot', $data);

        }
		else if(strcmp($page,'resendemail')==0){
			$this->load->helper('form');
			$this->load->library('form_validation');
			$data['error_message'] = "";
			$this->load->view('templates/signin_header', $data);
			$this->load->view('pages/resendemail', $data);
		}
        else
        {
	        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
	        {
	                // Whoops, we don't have a page for that!
	                show_404();
	        }
	        $this->load->view('templates/header', $data);
        	$this->load->view('pages/'.$page, $data);
        	$this->load->view('templates/footer', $data);
    	}
	}


	public function signup()
	{

	    $this->load->helper('form');
	    $this->load->library('form_validation');
		$this->load->helper('email');
		$this->load->helper('security');

		$data['error_message'] = "";
		$data['sFName'] = "";
		$data['sLName'] = "";
		$data['sEmail'] = "";
		$data['sBirthDate'] = "";
       	$data['sPhone'] = "";
		$data['sCompany'] = "";
       	$data['sJobTitle'] = "";
		$data['sNoOfEmployees'] = "";
		$data['sFeatures'] = "";
		   
		if ($this->input->post('signup')) {


			$this->form_validation->set_rules('txtFName', 'First Name', 'required');
			$this->form_validation->set_rules('txtLName', 'Last Name', 'required');
			$this->form_validation->set_rules('txtEmail', 'Email Address', 'required');
			$this->form_validation->set_rules('txtPhone', 'Phone Number', 'required');
//			$this->form_validation->set_rules('txtPassword', 'Password', 'required');

			$txtFName = $this->input->post('txtFName');
			$txtLName = $this->input->post('txtLName');
	    	$txtEmail = $this->input->post('txtEmail');
//			$txtPassword = $this->input->post('txtPassword');


			$selCode = $this->input->post('selCode');
	    	$txtPhone = $this->input->post('txtPhone');
	    	$txtBDate = $this->input->post('txtBirthDate');
	    	$txtCompany = $this->input->post('txtCompany');
	    	$txtJobTitle = $this->input->post('txtJobTitle');
	    	$txtNoOfEmployees = $this->input->post('txtNoOfEmployees');
	    	$txtFeatures = $this->input->post('txtFeatures');
			$txtPhone = $selCode.' '.$txtPhone;

			$data['sFName'] = $txtFName;
			$data['sLName'] = $txtLName;			
			$data['sEmail'] = $txtEmail;
//			$data['sPassword'] = $txtPassword;
			$data['sPhone'] = $txtPhone;
			$data['sBirthDate'] = $txtBDate;
			$data['sCompany'] = $txtCompany;
	       	$data['sJobTitle'] = $txtJobTitle;
			$data['sNoOfEmployees'] = $txtNoOfEmployees;
			$data['sFeatures'] = $txtFeatures;

			$data['aggrement_one'] = $_POST['aggrement'];
			$data['aggrement_two'] = $_POST['aggrement1'];
			$data['aggrement_three'] = $_POST['aggrement2'];




 	$this->form_validation->set_rules('txtEmail', 'Email Address', 'required|valid_email|callback_email_check');
//
 	$this->form_validation->set_rules('txtPhone', 'Phone Number', 'callback_phone_check');
 		$this->form_validation->set_rules('txtBirthDate', 'Birth Date', 'callback_age_check');
//			$this->form_validation->set_rules('txtPassword', 'Password', 'callback_password_check');



			if ($this->form_validation->run()) {

				$password = $this->passwordmanager->generatePassword();

				$data['password'] = $password ;

				  	$plaintext_password = $password;
	                $hash = password_hash($plaintext_password, PASSWORD_DEFAULT);
 					try{
				 	$user_id = $this->users_model->set_Register($txtFName,
						$txtLName,
						$txtEmail,
					$hash , $txtPhone, $txtBDate,
						$txtCompany,
					$txtJobTitle, $txtNoOfEmployees,
						$txtFeatures,$_POST['aggrement'], $_POST['aggrement1'],
					$_POST['aggrement2']);

					$data['id'] =$user_id ;
					$message = $this->load->view('templates/email/Registration', $data, TRUE);
//					$response = $this->emailmanager->sendRegistrationVerificationEmail($user_id , $data, $password , $message);

						$mail = $this->bs->phpmailermanager->load();

						$mail->SMTPDebug = 0;
						$mail->isSMTP();
						$mail->Host       = 'email-smtp.us-east-1.amazonaws.com';
						$mail->SMTPAuth   = true;
						$mail->Username   = 'AKIAQPM5BONNSDWDPYWP';
						$mail->Password   = 'BHDJ0/orD2re9F1fuX4a36nPymZZvUjiGIINFA14+I2p';
						$mail->Port       = 587;

						$mail->setFrom('info@boardspeak.com', 'Board Speak');
						$mail->addAddress($data['sEmail'], $data['sFName'] . " " . $data['sLName']);              // Name is optional
						$mail->addReplyTo('no-reply@boardspeak.com', 'Boardspeak');

						$mail->isHTML(true);
						$mail->Subject = 'Welcome to Board Speak!';


						$mail->Body    = $message;
							try{

								$status = $mail->send();
								$this->load->view('templates/signin_header', $data);
                             	$this->load->view('pages/success', $data);

							}catch (Exception $e) {



							}




//					$this->load->view('templates/signin_header', $data);
//					$this->load->view('pages/success', $data);
				}catch (Exception $e) {

//					 print_r($e->getMessage());
//					 exit;
				}


			} else {

				$this->load->view('templates/signin_header', $data);
				$this->load->view('pages/signup', $data);
			}



		} else {

			$this->load->view('templates/signin_header', $data);
			$this->load->view('pages/signup', $data);
		}
	}

	public function save_inquiry() {
		$this->view = false;

		$data = $this->input->post();

		if (!$data['token']) {
			throw new Error('Token not found');
		}

		$this->inquiries_model->save_inquiry_information($data);

		$response = $this->emailmanager->sendContactUsAlert($data);

		echo json_encode($response);
		
	}
	public function save_inquiry2() {
		$this->view = false;

		$data = $this->input->post();
 
		$this->inquiries_model->save_inquiry_information($data);

		$response = $this->emailmanager->sendContactUsAlert($data);

		echo json_encode($response);

	}
	public function email_check($email) {
		$emailExists = $this->users_model->get_UserByEmail($email);

		if ($emailExists > 0) {
			$this->form_validation->set_message('email_check', 'Email address is already used');
			return FALSE;
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->form_validation->set_message('email_check', 'Invalid email address');
			return FALSE;
		}



//
//		if(stristr($email,'@uni.com') == false){
//			$this->form_validation->set_message('email_check', 'Please provide an acceptable email   address.');
//
//			return FALSE;
//		}



		return TRUE;
	}

	public function phone_check($phone) {
		$phoneExists = $this->users_model->get_UserByPhone('+63'. " " . $phone);

		if ($phoneExists > 0) {
			$this->form_validation->set_message('phone_check', 'Phone number is already used');
			return FALSE;
		}

		return TRUE;
	}
	public function password_check($password) {


		if(strlen($password) < 8){
			$this->form_validation->set_message('password_check', 'Password Must be Greater then 8 Character');

			return FALSE;
		}
		if (!preg_match('~[0-9]+~', $password)) {
			$this->form_validation->set_message('password_check', 'Password Must Contain 1 Number and  1 Special Character');

			return FALSE;
		}





		return TRUE;
	}
	public function age_check($birthdate) {
			try{
				$dobObject = new DateTime($birthdate);
				$nowObject = new DateTime();
			}catch(Exception $e){
				$this->form_validation->set_message('age_check', 'Please enter date format correctly: YYYY-MM-DD');
				return FALSE;
			}


        $diff = $dobObject->diff($nowObject);

        if ($diff->y < 16) {
			$this->form_validation->set_message('age_check', 'Age must be 16 years old and above');
			return FALSE;
		}

		return TRUE;
	}
}
