<?php
class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->helper('url_helper');
		
		// Load session library
		$this->load->library('session');
	}

	public function index() {
		$data['users'] = $this->users_model->get_Users();                
		$data['title'] = 'Users Archive';

		$this->load->view('templates/header', $data);
		$this->load->view('users/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($id = NULL) {
			$data['users_item'] = $this->users_model->get_Users($id);
			
			if (empty($data['users_item']))
			{
				show_404();
			}


		$data['title'] = $data['users_item']['Us_FName'];
		$this->load->view('templates/header', $data);
		$this->load->view('users/view', $data);
		$this->load->view('templates/footer');
	}
	
			
	public function create() {
	    $this->load->helper('form');
	    $this->load->library('form_validation');

	    $data['title'] = 'Create a USER item';

	    $this->form_validation->set_rules('txtName', 'First Name', 'required');
	    $this->form_validation->set_rules('txtFeatures', 'Features', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
		$this->load->view('templates/header', $data);
		$this->load->view('users/create');
		$this->load->view('templates/footer');

	    }
	    else
	    {
		$this->users_model->set_users();
		$this->load->view('templates/header');
		$this->load->view('users/success');
		$this->load->view('templates/footer');
	    }
	}

	public function success()
	{
		$this->load->view('templates/header');
		$this->load->view('users/success');
		$this->load->view('templates/footer');
	}

	function handle_user_create_group_popup() {
		$this->view = false;
		echo "aw";
	}	
		
}
