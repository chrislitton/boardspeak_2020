<?php
class User extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('notifications_model');
        $this->load->model('users_model');
        $this->load->model('contacts_model');
         $this->load->model('groups_model');
        $this->load->model('follower_model');
		$this->load->helper('url_helper');
        $this->load->helper(array('form', 'url'));
        $this->load->library('EmailManager');
    }

    public function profile($encodedUserID) {
        if (!isset($this->session->userdata['logged_in'])){
			redirect('/pages/view/signin'); //if session is not there, redirect to login page
        }

        $id = decode_id($encodedUserID);

        if ($id == $this->session->userdata['logged_in']['bs_id']) {
            redirect('/account/profile');
        }

        $data['error'] = "";
		$data['errorProfile'] = "";
		$data['errorPassword'] = "";
		$data['errorPhoto'] = "";
		$data['errorBackground'] = "";

		$data['Name'] = "";
		$data['title'] = "";
		$data['CurrUserID'] = $id;
		
        $data['users_item'] = $this->users_model->get_Users($id);
		if (empty($data['users_item'])) {
			show_404();
        }
        
        $data['contact_info'] = $this->contacts_model->get_contact_information($id);
            $data['group_Detail'] = $this->groups_model->get_group_user($id);
        $data['is_followed'] = $this->follower_model->check_if_followed($id, 'user');
        $data['create_group_btn_link'] = ($this->session->userdata['logged_in']['bs_group']) ? base_url().'account/create/group' : 'javascript:void(0)';
        $data['create_group_btn_class'] = ($this->session->userdata['logged_in']['bs_group']) ? '' : 'create_group_btn';
        
        $data['asset'] = 'user/profile';

//		print_r($data);
//		exit;
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navigation', $data);
		$this->load->view('user/profile', $data);
		$this->load->view('templates/footer', $data);
    }

    public function user_contact_count() {
        $this->view = false;

        $keyword = $this->input->post('keyword');
        $user_id = $this->input->post('user_id');
        $userID = (isset($user_id)) ? decode_id($user_id) : $this->session->userdata['logged_in']['bs_id'];

        $response = $this->contacts_model->show_user_contact_count($userID, $keyword);

        echo json_encode($response);
    }

    public function show_contact_request_list() {
        $this->view = false;

        $keyword = $this->input->post('keyword');
        $sort = $this->input->post('sort');
        $user_id = $this->input->post('user_id');
        $userID = (isset($user_id)) ? decode_id($user_id) : $this->session->userdata['logged_in']['bs_id'];

        $response = $this->contacts_model->show_contact_request_list($userID, $keyword, $sort);

        echo json_encode($response);
    }

    public function show_contact_suggest_list() {
        $this->view = false;

        $keyword = $this->input->post('keyword');
        $sort = $this->input->post('sort');
        $user_id = $this->input->post('user_id');
        $userID = (isset($user_id)) ? decode_id($user_id) : $this->session->userdata['logged_in']['bs_id'];

        $response = $this->contacts_model->show_contact_suggest_list($userID, $keyword, $sort);

        echo json_encode($response);

    }
    function show_contact_explore_list (){
     $this->view = false;

            $keyword = $this->input->post('keyword');
            $sort = $this->input->post('sort');
            $user_id = $this->input->post('user_id');
            $userID = (isset($user_id)) ? decode_id($user_id) : $this->session->userdata['logged_in']['bs_id'];

            $response = $this->contacts_model->show_contact_explore_list($userID, $keyword, $sort);

            echo json_encode($response);
    }

    public function show_contact_list() {
        $this->view = false;
        
        $keyword = $this->input->post('keyword');
        $sort = $this->input->post('sort');
        $user_id = $this->input->post('user_id');
  		 $type = $this->input->post('type');

        $userID = (isset($user_id)) ? decode_id($user_id) : $this->session->userdata['logged_in']['bs_id'];
        $showPrivate = $this->input->post('show_private');

        $response = $this->contacts_model->show_contact_list($userID, $keyword, $sort, $showPrivate);

        echo json_encode($response);

    }

    public function respond_to_user_contact_request() {
        $this->view = false;

        $encoded = $this->input->post('encoded');
        $user = (isset($encoded)) ? decode_id($this->input->post('user_id')) : $this->input->post('user_id');
        $action = $this->input->post('action');

        $response = $this->contacts_model->respond_to_user_contact_request($user, $action);

        echo json_encode($response);
    }

    public function add_block_user() {
        $this->view = false;

        $encoded = $this->input->post('encoded');
        $user = (isset($encoded)) ? decode_id($this->input->post('user_id')) : $this->input->post('user_id');
        $action = $this->input->post('action');

        $response = $this->contacts_model->add_block_user($user, $action);

        echo json_encode($response);
    }

    public function cancel_pending_user_invite() {
        $this->view = false;

        $encoded = $this->input->post('encoded');
        $user = (isset($encoded)) ? decode_id($this->input->post('user_id')) : $this->input->post('user_id');
        $action = $this->input->post('action');

        $response = $this->contacts_model->cancel_pending_user_invite($user, $action);

        echo json_encode($response);
    }

    public function contact_favorite_action() {
        $this->view = false;

        $encoded = $this->input->post('encoded');
        $user = (isset($encoded)) ? decode_id($this->input->post('user_id')) : $this->input->post('user_id');
        $action = $this->input->post('action');

        $response = $this->contacts_model->contact_favorite_action($user, $action);

        echo json_encode($response);
    }

    public function contact_block_message_action() {
        $this->view = false;

        $encoded = $this->input->post('encoded');
        $user = (isset($encoded)) ? decode_id($this->input->post('user_id')) : $this->input->post('user_id');
        $action = $this->input->post('action');

        $response = $this->contacts_model->contact_block_message_action($user, $action);

        echo json_encode($response);
    }

    public function remove_contact() {
        $this->view = false;

        $encoded = $this->input->post('encoded');
        $user = (isset($encoded)) ? decode_id($this->input->post('user_id')) : $this->input->post('user_id');
        $action = $this->input->post('action');

        $response = $this->contacts_model->remove_contact($user, $action);

        echo json_encode($response);
    }

    public function follow() {
        $this->view = false;

        $data = array();
        
        $encoded = $this->input->post('encoded');
        $data['id'] = (isset($encoded)) ? decode_id($this->input->post('user_id')) : $this->input->post('user_id');
        $data['type'] = 'user';
        $action = $this->input->post('action');

        if ($action == 'follow') {
            $this->follower_model->follow($data);
        } else {
            $this->follower_model->unfollow($data);
        }

        $response = array('status' => 'success', 'message' => 'User ' . $action . 'ed successfuly');

        echo json_encode($response);
    }

    public function email_verification($encodedID) {
        $user_id = decode_id($encodedID);

        $data = $this->users_model->activate_user_account($user_id);

        $this->load->view('templates/signin_header');
        $this->load->view('pages/verified', $data);
    }
		  public function    reset_email_verification($encodedID) {
                $user_id = decode_id($encodedID);
				 $data['userId'] = $user_id;

				$data['success_message'] = "";
				$data['error_message'] = "";
       			$this->load->view('templates/signin_header', $data);
       			$this->load->view('pages/updatepassword', $data);
            }



}
