<?php
class Topic extends CI_Controller {

	public function __construct()
	{
            parent::__construct();
            
			$this->load->model('topics_model', 'topic');
            $this->load->model('category_model', 'category');
            $this->load->model('posts_model', 'post');
			$this->load->helper('url_helper');
			$this->load->helper(array('form', 'url'));

			// Load session library
			$this->load->library('session');
    }

    public function show_topics() {
        $this->view = false;
        
        $userID = $this->session->userdata['logged_in']['bs_id'];
        $data = $this->input->post();



        $topics = $this->topic->show_topics($userID, $data);
        
        echo json_encode($topics);
	}
    
    public function show_user_created_topics() {
        $this->view = false;

 			$data = $this->input->post();


  	   $userID = (isset($data['user_id'])) ? decode_id($data['user_id']) : $this->session->userdata['logged_in']['bs_id'];


        
        $topics = $this->topic->show_user_created_topics($userID, $data);
        
        echo json_encode($topics);
	}



    public function show_topics_posts() {
        $this->view = false;
        $post_data = $this->input->post();
        
        $id = null;
        $category_id = null;
        $search_text = null;
		$sub_category_id = null;
        $ctr = 0;

        if (isset($post_data['ID'])) {
            $id = decode_id($post_data['ID']);
        }

        if (isset($post_data['category_id'])) {
            $category_id = $post_data['category_id'];
        }

        if (isset($post_data['search_text'])) {
            $search_text = $post_data['search_text'];
        }

		if(isset($post_data['sub_category_id'])){
			$sub_category_id = $post_data['sub_category_id'];

		}
        $subcategory_items = $this->category->get_topic_subcategories($category_id, $id);
        $posts = $this->post->show_topics_post($id, $category_id, $search_text , $sub_category_id);

        echo json_encode(array('data_items' => $posts, 'subcategory_items' => $subcategory_items));

    }

    public function upload_image() {
        $this->view = false;

        $TopicID = decode_id($_POST['GroupID']);
        $userID = ($this->session->userdata['logged_in']['bs_id']);

        if(isset($_POST['image'])) {
            $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $img_path = 'uploads/background/';
            $image_name = $img_path. $userID. "_". $TopicID. "_". time() . '.png';

            $this->topic->set_changephoto($TopicID, $image_name,$image_name,$image_name);

            file_put_contents($image_name, $data);

            echo trim($image_name);
            exit;
        }
    }
}
