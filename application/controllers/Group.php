<?php

class Group extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('groups_model');
		$this->load->model('members_model');
		$this->load->model('posts_model');
		$this->load->model('medias_model');
		$this->load->model('follower_model');
		$this->load->model('topics_model');
		$this->load->model('settings_model');
		$this->load->model('comments_model');

		$this->load->model('category_model');
		$this->load->model('notifications_model');
		$this->load->model('users_model');
		$this->load->model('contacts_model');
		$this->load->model('role_model');
		$this->load->model('permission_model');
		$this->load->helper('url_helper');
		$this->load->helper(array('form', 'url'));

		// Load session library
		$this->load->library('session');
	}

	public function show_groups()
	{
		$this->view = false;
		$data = $this->input->post();
		$groups = $this->groups_model->show_groups($data);
		echo json_encode($groups);
	}

	public function show_user_created_groups()
	{
		$this->view = false;

		$data = $this->input->post();
		$userID = (isset($data['user_id'])) ? decode_id($data['user_id']) : $this->session->userdata['logged_in']['bs_id'];

		$groups = $this->groups_model->show_user_created_groups($userID, $data);

		echo json_encode($groups);
	}

	public function show_user_followed_groups()
	{
		$this->view = false;

		$data = $this->input->post();

		$userID = (isset($data['user_id'])) ? decode_id($data['user_id']) : $this->session->userdata['logged_in']['bs_id'];

		$groups = $this->groups_model->show_user_followed_groups($userID, $data);

//		print_r($groups);
//		exit;
		echo json_encode($groups);
	}

		function member_leave_group(){

			 $userID =  $this->session->userdata['logged_in']['bs_id'];
 		$res = 	 $this->groups_model->member_leave_group($userID, decode_id($_POST['GroupID']));

		 echo json_encode($res);
		}
	//make non_slug to slug
	public function convert_aLl_group_slug()
	{
		$this->db->select('bs_groups.*');
		$this->db->from('bs_groups');
		$allgroupr = $this->db->get()->result_array();

		foreach ($allgroupr as $all) {

			$post_Slug = $this->slug($all['Gr_Name'], '_').'_'.rand(0,9999);
			$data = array(
				'Gr_Slug' => $post_Slug
			);
			$this->db->where('Gr_ID', $all['Gr_ID']);
			$this->db->update('bs_groups', $data);

		}

	}

	function slug($string, $spaceRepl = "-")
	{
		$string = str_replace("&", "and", $string);

		$string = preg_replace("/[^a-zA-Z0-9 _-]/", "", $string);

		$string = strtolower($string);

		$string = preg_replace("/[ ]+/", " ", $string);

		$string = str_replace(" ", $spaceRepl, $string);

		return $string;
	}

	public function show_featured_groups()
	{
		$this->view = false;

		$data = $this->input->post();

		$groups = $this->groups_model->show_featured_groups($data);

		echo json_encode($groups);
	}

	public function show_suggested_groups()
	{
		$this->view = false;

		$data = $this->input->post();

		$groups = $this->groups_model->show_suggested_groups($data);

		echo json_encode($groups);
	}

	public function group_pinning_post()
	{


		$res = $this->groups_model->group_pinning_post();

		echo json_encode($res);
	}
	public function topic_pinning_post()
	{


		$res = $this->groups_model->topic_pinning_post();

		echo json_encode($res);
	}
	public function show_group_topics()
	{
		$this->view = false;

		$post_data = $this->input->post();

		$id = null;
		$category_id = null;
		$search_text = null;
		$sub_category_id  = null;
		$ctr = 0;

		if (isset($post_data['ID'])) {
			$id = decode_id($post_data['ID']);
		}

		if (isset($post_data['category_id'])) {
			$category_id = $post_data['category_id'];
		}

		if (isset($post_data['search_text'])) {
			$search_text = $post_data['search_text'];
		}

		if(isset($post_data['sub_category_id'])){
			$sub_category_id = $post_data['sub_category_id'];

 		}

		$subcategory_items = $this->category_model->get_group_subcategories($category_id, $id);
		$group_topics = $this->groups_model->show_group_topics($id, $category_id, $search_text , null , $sub_category_id);

		print json_encode(array('data_items' => $group_topics, 'subcategory_items' => $subcategory_items));
		exit;
	}

	public function upload_image()
	{
		$this->view = false;
		$groupID = decode_id($_POST['GroupID']);
		$userID = ($this->session->userdata['logged_in']['bs_id']);

		if (isset($_POST['image'])) {
			$data = $_POST['image'];
			$image_array_1 = explode(";", $data);
			$image_array_2 = explode(",", $image_array_1[1]);
			$data = base64_decode($image_array_2[1]);
			$img_path = 'uploads/background/';
			$image_name = $img_path . $userID . "_" . $groupID . "_" . time() . '.png';

			$this->groups_model->set_changephoto($groupID, $image_name, $image_name, $image_name);

			file_put_contents($image_name, $data);

			echo trim($image_name);
			exit;
		}
	}

	public function show_group_members()
	{
//         $this->view = false;

		$data = $this->input->post();

		$members = $this->members_model->show_members($data);

		echo json_encode($members);
	}

	public function show_roles_for_dropdown()
	{
		$this->view = false;

		$data = $this->input->post();

		$members = $this->role_model->show_roles_for_dropdown($data);

		echo json_encode($members);
	}

	public function assign_group_role()
	{
		$this->view = false;

		$data = $this->input->post();
		$id = decode_id($data['type_id']);
 		if(isset($data['updatetouser'])){

 				$update = array(
					'Po_Us_ID' => $data['updatetouser']
				);
				$where = array(
					'Po_Us_ID' => $data['user_id'],
					'Po_Gr_ID'=>  $id
				);
				$this->db->where($where);
				$this->db->update('bs_posts' , $update);


				$update = array(
					'To_Us_ID' => $data['updatetouser']
				);
				$where = array(
					'To_Us_ID' => $data['user_id'],
					'To_Gr_ID'=>  $id
				);
				$this->db->where($where);
				$this->db->update('bs_topics' , $update);
			if($data['role'] == 'leave'){
				$this->members_model->leave_group_role($id, $data['type'], $data['user_id'], $data['role']);
			}else{
				$this->members_model->assign_group_role($id, $data['type'], $data['user_id'], $data['role']);
			}



		}else{
			if($data['role'] == 'creator'){



				$group = $this->groups_model->get_Record($id);
				$user = $this->users_model->get_Users($data['user_id']);
				$invitor = $this->users_model->get_Users($this->session->userdata['logged_in']['bs_id']);

				$notif_type = 'creator_role_request';
				$userRole = str_replace("_", " ", $data['role']);

				$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
				$content = " has assigned you as new Creator for " . $linkmade ." group. All superior rights and ownership of this group will be passed on to you";
				$this->notifications_model->add_notification($notif_type, 'Asigned you creator role', $content, $id, 'group', $data['user_id'], 'user');


				$notif_type = 'creator_role_invite';
				$linkmade = '<a href="'.base_url().'account/view/group/'.encode_id($id).'">'.$group['Gr_Name'].'</a>';
				$content =   "You have assigned ". $user['Us_Name'] ." as new Creator for " . $linkmade ." group. All superior rights and ownership of this group will be passed on to new Creator";

				$this->notifications_model->add_notification($notif_type, 'invite member as creator in your group', $content, $id, 'group', $this->session->userdata['logged_in']['bs_id'], 'user');
 				}else{
					$post = null;
				if(isset($data['post_id'])){
					$post = $data['post_id'];
				}
				$this->members_model->assign_group_role($id, $data['type'], $data['user_id'], $data['role'] , $post );

			}
		}



		echo true;
	}

	public function remove_group_user()
	{
		$this->view = false;

		$data = $this->input->post();
		$id = decode_id($data['type_id']);



		if(isset($data['updatepersonId'])){
 				$update = array(
				'Po_Us_ID' => $data['updatepersonId']
			);
			$where = array(
				'Po_Us_ID' => $data['user_id'],
				'Po_Gr_ID'=>  $id
			);
			$this->db->where($where);
			$this->db->update('bs_posts' , $update);


			$update = array(
				'To_Us_ID' => $data['updatepersonId']
			);
			$where = array(
				'To_Us_ID' => $data['user_id'],
				'To_Gr_ID'=>  $id
			);
			$this->db->where($where);
			$this->db->update('bs_topics' , $update);

   			}
		$this->members_model->remove_group_user($id, $data['user_id']);

		echo true;
	}


			function approve_reject_join_request_from_notification_creator(){

				if (isset($_POST['notification_ID'])) {
					$notification_ID = $_POST['notification_ID'];

					$this->db->where('No_ID', $_POST['notification_ID']);
					$this->db->update('bs_notification', array('No_Status' => $_POST['action'] ));
				}
				$group = $this->groups_model->get_Record($_POST['groupid']);
				$userinfo = $this->users_model->get_Users($_POST['userid']);
				$previous_creator = $this->users_model->get_Users($group['Gr_Us_ID']);

					if($_POST['action'] == 'approvecreator'){



//						$this->db->where('Me_Gr_ID', $_POST['groupid']);
//						$this->db->where('Me_Us_ID', $_POST['userid']);
//						$this->db->where('Me_Parent','group');
//						$this->db->delete('bs_members');


								$update = array(
									'Gr_Us_ID' => 	$_POST['userid']
								);
								$where = array(
									'Gr_ID' => $_POST['groupid']

								);
								$this->db->where($where);
								$this->db->update('bs_groups',$update);


								$update = array(
									'Po_Us_ID' => $_POST['userid']
								);
								$where = array(
//									'Po_Us_ID' => $this->session->userdata['logged_in']['bs_id'],
									'Po_Gr_ID'=>   $_POST['groupid']
								);
								$this->db->where($where);
								$this->db->update('bs_posts' , $update);


								$update = array(
									'To_Us_ID' => $_POST['userid']
								);
								$where = array(
//									'To_Us_ID' => $this->session->userdata['logged_in']['bs_id'],
									'To_Gr_ID'=>   $_POST['groupid']
								);
								$this->db->where($where);
								$this->db->update('bs_topics' , $update);

								$update = array(
									'Gr_Us_ID' => $_POST['userid']
								);
								$where = array(
//									'Gr_Us_ID' => $this->session->userdata['logged_in']['bs_id'],
									'Gr_ID'=>   $_POST['groupid']
								);
								$this->db->where($where);
								$this->db->update('bs_groups' , $update);


//								$this->db->select('bs_members.*');
//								$this->db->from('bs_members');
//								$this->db->where('Me_Gr_ID', $_POST['groupid']);
//								$this->db->where('Me_Us_ID', $group['Gr_Us_ID']);
//								$this->db->where('Me_Parent','group');
//						$resp = $this->members_model->add_Member($_POST['groupid'], 'group', 'superadmin', 'active', $group['Gr_Us_ID'] );

						$this->members_model->assign_group_role( $_POST['groupid'], 'group', $_POST['userid'], 'creator'  );

						$notif_type = 'creator_role_invite_accept';
						$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
						$content = "accepted the group creator request of  " . $group_lnk . " group";
						$this->notifications_model->add_notification($notif_type, 'user accepet the request', $content, $_POST['userid'], 'user', $previous_creator['Us_ID'], 'user');


						$notif_type = 'creator_role_invite_accept_me';
						$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
						$content = "You accepted the creator role of " . $group_lnk . " group";
						$this->notifications_model->add_notification($notif_type, 'user accepet the request', $content, $previous_creator['Us_ID'] , 'user', $_POST['userid'], 'user');
						$response = array('status' => 'success', 'message' => 'Successfully accepted the request.');
					}else if($_POST['action'] == 'rejectcreator'){


						$notif_type = 'creator_role_invite_reject';
					 	$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
 						$content = "rejected the group creator request of  " . $group_lnk . " group";
						$this->notifications_model->add_notification($notif_type, 'user reject the request', $content, $_POST['userid'], 'user', $previous_creator['Us_ID'], 'user');


						$notif_type = 'creator_role_invite_reject_me';
						$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
						$content = "You rejected the creator role of " . $group_lnk . " group";
						$this->notifications_model->add_notification($notif_type, 'user reject the request', $content, $previous_creator['Us_ID'] , 'user', $_POST['userid'], 'user');
						$response = array('status' => 'success', 'message' => 'You have rejected the request.');
					}
 				echo json_encode($response);
			}


			function getgroupname(){

						$this->db->select('bs_groups.*');
						$this->db->from('bs_groups');
						$this->db->where('Gr_ID' , $_POST['groupiss']);
						$res = 	$this->db->get()->result_array();
						echo json_encode($res);
			}


				function gettopicname(){
					$this->db->select('bs_members.Me_To_ID');
					$this->db->from('bs_members');
					$this->db->where('Me_ID ',  $_POST['groupiss']);
					$ress = $this->db->get()->result_array();
					$this->db->select('bs_topics.* , bs_users.*');
					$this->db->from('bs_topics');
					$this->db->join('bs_users' , 'bs_users.Us_ID = bs_topics.To_Us_ID');
					$this->db->where('TO_ID'  , $ress[0]['Me_To_ID']);
					$res = 	$this->db->get()->result_array();

					$res[0]['SLUG']= encode_id($res[0]['To_ID']);
					echo json_encode($res);
				}
			function getpostname(){



				$this->db->select('bs_members.Me_St_ID');
				$this->db->from('bs_members');
				$this->db->where('Me_ID ',  $_POST['groupiss']);
				$ress = $this->db->get()->result_array();

				$this->db->select('bs_posts.* , bs_users.*');
				$this->db->from('bs_posts');
				$this->db->join('bs_users' , 'bs_users.Us_ID = bs_posts.Po_Us_ID');
				$this->db->where('Po_ID'  , $ress[0]['Me_St_ID']);
				$res = 	$this->db->get()->result_array();
				echo json_encode($res);

			}
	public function approve_reject_join_request_from_notification()
	{


		$this->view = false;
		$data = $this->input->post();
		$id = $data['type_id'];
		$user_id = $data['user_id'];
		$action = $data['action'];


		if (isset($data['postinvite'])) {

			if ($action == 'approve') {
				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_ID', $data['postinvite']);
				$query = $this->db->get()->result_array();

				if ($data['notype'] == 'topic_join_via_link') {
					$this->db->select('bs_topics.*');
					$this->db->from('bs_topics');
					$this->db->where('To_ID', $query[0]['Me_To_ID']);
					$post_details = $this->db->get()->result_array();
				} else {
					$this->db->select('bs_posts.*');
					$this->db->from('bs_posts');
					$this->db->where('Po_ID', $query[0]['Me_St_ID']);
					$post_details = $this->db->get()->result_array();
				}

				$this->db->where('Me_To_ID', $query[0]['Me_To_ID']);
				$this->db->where('Me_St_ID', $query[0]['Me_St_ID']);
				$this->db->where('Me_Us_ID', $query[0]['Me_Us_ID']);

				$this->db->where_in('Me_Parent', array('post', 'topic', 'group'));
				$this->db->update('bs_members', array('Me_Status' => 'active'));
			}
		}

		if (isset($data['notification_ID'])) {
			$notification_ID = $data['notification_ID'];

			$this->db->where('No_ID', $data['notification_ID']);
			$this->db->update('bs_notification', array('No_Status' => $action));
		}

		$group = $this->groups_model->get_Record($id);
		$this->db->select('bs_notification.*');
		$this->db->from('bs_notification');
		$this->db->where('No_ID', $data['notification_ID']);
		$noti_query = $this->db->get()->result_array();

		if ($action == 'approve') {
			$datax = array('Me_Status' => 'active');
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->update('bs_members', $datax);
			$notif_type = "approve_request";

			$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
			if (isset($data['postinvite'])) {

				$string = '';
				if ($data['notype'] == 'post_join_via_be_a_member') {

					$string = 'You may now comment on this post';
					$content = "Your request to join  post   " . $keyword . " " . $post_lnk . " under group " . $group_lnk . " has been approved " . $string;

				}


				if ($data['notype'] == 'topic_join_via_link') {
					$keyword = 'topic';
					$post_lnk = '<a href=' . base_url() . 'account/view/topic/' . encode_id($post_details[0]['To_ID']) . '>' . $post_details[0]['To_Name'] . '</a>';
					$content = "Your request to join  topic   " . $keyword . " " . $post_lnk . " under group " . $group_lnk . " has been approved " . $string;

				} else {
					$keyword = 'post';
					$post_lnk = '<a href=' . base_url() . 'account/view/post/' . encode_id($post_details[0]['Po_ID']) . '>' . $post_details[0]['Po_Title'] . '</a>';
					$content = "Your request to join private post " . $keyword . " " . $post_lnk . " under group " . $group_lnk . " has been approved " . $string;

				}
//  				$post_lnk ='<a href='.base_url().'account/view/post/'.encode_id($post_details[0]['Po_ID']).'>'.$post_details[0]['Po_Title'].'</a>';
			} else {
				$content = "Your request to join " . $group_lnk . " has been approved";
			}

			$this->notifications_model->add_notification($notif_type, 'You have been approved', $content, $group['Gr_ID'], 'group', $user_id, 'user');

			$response = array('status' => 'success', 'message' => 'User join request approved.');

		}
		else if ($action == 'reject')
		{
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->delete('bs_members');

			$notif_type = "reject_request";
			$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
			if (isset($data['postinvite'])) {


				if ($data['notype'] == 'topic_join_via_link') {
					$keyword = 'topic';
					$post_lnk = '<a href=' . base_url() . 'account/view/topic/' . encode_id($post_details[0]['To_ID']) . '>' . $post_details[0]['To_Name'] . '</a>';

				} else {
					$keyword = 'post';
					$post_lnk = '<a href=' . base_url() . 'account/view/post/' . encode_id($post_details[0]['Po_ID']) . '>' . $post_details[0]['Po_Title'] . '</a>';

				}
// 				$post_lnk ='<a href='.base_url().'account/view/post/'.encode_id($post_details[0]['Po_ID']).'>'.$post_details[0]['Po_Title'].'</a>';
				$content = "Your request to join private " . $keyword . " " . $post_lnk . " under group " . $group_lnk . " has been rejected";
			} else {
				$content = "Your request to join " . $group_lnk . " has been rejected";
			}

			$this->notifications_model->add_notification($notif_type, 'You have been rejected', $content, $group['Gr_ID'], 'group', $user_id, 'user');

			$response = array('status' => 'success', 'message' => 'User join request rejected.');
		}
		else if ($action == 'accept') {
			if ($group['Gr_Us_ID'] == $noti_query[0]['No_To_Type']) {
				$strin = '';
				$datax = array('Me_Status' => 'active');


				$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
				$notif_type = "invite_accepted";
				$content = "accepted the request to join group " . $group_lnk;
				$this->notifications_model->add_notification($notif_type, 'user accepet the request', $content, $user_id, 'user', $group['Gr_Us_ID'], 'group');

			} else {
				$strin = '. pending admin approval';
				$datax = array('Me_Status' => 'pending');
				$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
				$notif_type = "invite_accepted";
				$content = "accepted the request to join group " . $group_lnk;
				$this->notifications_model->add_notification($notif_type, 'user accepet the request', $content, $user_id, 'user', $group['Gr_Us_ID'], 'group');

			}

			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->update('bs_members', $datax);

			$response = array('status' => 'success', 'message' => 'You accepted the request to join group. ' . $group['Gr_Name'] . '. ' . $strin);
		}
		else if ($action == 'rejected') {

			if ($group['Gr_Us_ID'] == $noti_query[0]['No_To_Type']) {

				$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
				$notif_type = "invite_rejected";
				$content = "rejected the request to join group " . $group_lnk;
				$this->notifications_model->add_notification($notif_type, 'user reject the request', $content, $user_id, 'user', $group['Gr_Us_ID'], 'group');

			} else {


				$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';
				$notif_type = "invite_rejected";
				$content = "rejected the request to join group " . $group_lnk;
				$this->notifications_model->add_notification($notif_type, 'user reject the request', $content, $user_id, 'user', $group['Gr_Us_ID'], 'group');

			}
			$datax = array('Me_Status' => 'reject');
			$this->db->where('Me_Gr_ID', $id);
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->update('bs_members', $datax);


			$response = array('status' => 'success', 'message' => 'You rejected the request to join group. ' . $group['Gr_Name']);
		}
		echo json_encode($response);

	}

	public function approve_reject_join_request()
	{

		$this->view = false;

		$data = $this->input->post();
//		print_r($data['notification_ID']);exit();
		if (isset($_POST['topicsec'])) {
			$topicid = $data['type_id'];
			$this->db->select('bs_topics.*');
			$this->db->from('bs_topics');
			$this->db->where('To_ID', decode_id($data['type_id']));
			$dataqq = $this->db->get()->result_array();

			$data['type_id'] = encode_id($dataqq[0]['To_Gr_ID']);
		}

		$id = $data['type_id'];
		$user_id = $data['user_id'];
		$action = $data['action'];
		if (isset($data['postinvite'])) {

			if ($action == 'approve') {
				$this->db->select('bs_members.*');
				$this->db->from('bs_members');
				$this->db->where('Me_ID', $data['postinvite']);
				$query = $this->db->get()->result_array();
				if (isset($_POST['topicsec'])) {
					$this->db->select('bs_topics.*');
					$this->db->from('bs_topics');
					$this->db->where('To_ID', $query[0]['Me_To_ID']);
					$post_details = $this->db->get()->result_array();
				} else {

					$this->db->select('bs_posts.*');
					$this->db->from('bs_posts');
					$this->db->where('Po_ID', $query[0]['Me_St_ID']);
					$post_details = $this->db->get()->result_array();
				}
				$this->db->where('Me_To_ID', $query[0]['Me_To_ID']);
				$this->db->where('Me_St_ID', $query[0]['Me_St_ID']);
				$this->db->where('Me_Us_ID', $query[0]['Me_Us_ID']);

				$this->db->where_in('Me_Parent', ['post', 'topic', 'group']);
				$this->db->update('bs_members', array('Me_Status' => 'active'));
			}
		}


		if (isset($data['notification_ID'])) {
			$notification_ID = $data['notification_ID'];
			$this->db->where('No_ID', $data['notification_ID']);
			$this->db->update('bs_notification', array('No_Status' => $action));
		}


		$group = $this->groups_model->get_Record(decode_id($id));
		if ($action == 'approve') {
			$datax = array('Me_Status' => 'active');
			$this->db->where('Me_Gr_ID', decode_id($id));
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->update('bs_members', $datax);

			$notif_type = "approve_request";
			$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';


			if (isset($data['postinvite'])) {


				$string = '';
				if ($data['notype'] == 'post_join_via_be_a_member') {

					$string = 'You may now comment on this post';
					$post_lnk = '<a href=' . base_url() . 'account/view/post/' . encode_id($post_details[0]['Po_ID']) . '>' . $post_details[0]['Po_Title'] . '</a>';

				} else if ($data['notype'] == 'post_join_via_link') {
					$post_lnk = '<a href=' . base_url() . 'account/view/post/' . encode_id($post_details[0]['Po_ID']) . '>' . $post_details[0]['Po_Title'] . '</a>';

				} else if ($data['notype'] == 'topic_join_via_link') {
					$post_lnk = '<a href=' . base_url() . 'account/view/topic/' . encode_id($post_details[0]['To_ID']) . '>' . $post_details[0]['To_Name'] . '</a>';


				}


				$content = "Your request to join private post " . $post_lnk . " under group " . $group_lnk . " has been approved " . $string;
			} else {


				$content = "Your request to join " . $group_lnk . " has been approved";
			}

			$this->notifications_model->add_notification($notif_type, 'You have been approved', $content, $group['Gr_ID'], 'group', $user_id, 'user');

			$response = array('status' => 'success', 'message' => 'User join request approved.');

		}
		else {
			$this->db->where('Me_Gr_ID', decode_id($id));
			$this->db->where('Me_Us_ID', $user_id);
			$this->db->delete('bs_members');

			$notif_type = "reject_request";
			$group_lnk = '<a href=' . base_url() . 'account/view/group/' . encode_id($group['Gr_ID']) . '>' . $group['Gr_Name'] . '</a>';

			if (isset($data['postinvite'])) {
				$string = '';
				if ($data['notype'] == 'post_join_via_be_a_member') {
					$string = 'You may now comment on this post';
					$post_lnk = '<a href=' . base_url() . 'account/view/post/' . encode_id($post_details[0]['Po_ID']) . '>' . $post_details[0]['Po_Title'] . '</a>';
					$content = "Your request to join post " . $post_lnk . " under group " . $group_lnk . " has been rejected";

				} else if ($data['notype'] == 'post_join_via_link') {
					$post_lnk = '<a href=' . base_url() . 'account/view/post/' . encode_id($post_details[0]['Po_ID']) . '>' . $post_details[0]['Po_Title'] . '</a>';
					$content = "Your request to join private post " . $post_lnk . " under group " . $group_lnk . " has been rejected";

				} else if ($data['notype'] == 'topic_join_via_link') {
					$post_lnk = '<a href=' . base_url() . 'account/view/topic/' . encode_id( $dataqq[0]['To_ID']) . '>' . $dataqq[0]['To_Name'] . '</a>';
					$content = "Your request to join  topic " . $post_lnk . " under group " . $group_lnk . " has been rejected";

				}


			} else {
				$content = "Your request to join " . $group_lnk . " has been rejected";
			}


			$this->notifications_model->add_notification($notif_type, 'You have been rejected', $content, $group['Gr_ID'], 'group', $user_id, 'user');

			$response = array('status' => 'success', 'message' => 'User join request rejected.');
		}


		echo json_encode($response);

	}

	public function search_invite_contacts()
	{
		$this->view = false;

		$data = $this->input->post();
		$group = decode_id($data['id']);


// 				$this->getDetail_contect();
		$response = $this->contacts_model->search_contact_for_group_invite($group, $data['keyword']);

		echo json_encode($response);
	}

//     function getDetail_contect(){
//     $this->db->select('bs_contacts.* , bs_users.*');
//     $this->db->from('bs_contacts');
//     $this->db->join('bs_user' , 'bs_users.Us_ID == bs_contacts.Co_Contact_ID' , 'left');
//     $this->db->where('bs_contacts.Co_Us_ID' , $this->logged_in);
//  	$query =   $this->db->get()->result_array();
// 		print_r($query);
// 		exit;
//     }

	public function invite_user_to_group()
	{
		$this->view = false;

		$data = $this->input->post();
		$group = decode_id($data['id']);

		$response = $this->members_model->invite_user_to_group($group, $data['user_id'], $data['role']);

		echo json_encode($response);
	}

	function upgrageplan(){

	  $where = array(
				'Gr_ID' => decode_id($_POST['id'])
			);
		$update = array(
			'Gr_packegtype' => $_POST['packagetype']
		);
		$this->db->where($where);
		$res = $this->db->update('bs_groups' , $update);
		echo $res ;

	}
	public function respond_to_group_invitation()
	{
		$this->view = false;

		$data = $this->input->post();
		$group = decode_id($data['id']);

		$response = $this->members_model->respond_to_group_invitation($group, $this->session->userdata['logged_in']['bs_id'], $data['action']);

		echo json_encode($response);
	}

	public function topicinfo($topicid, $referrer = NULL, $role = NULL)
	{


		$this->session->set_flashdata('registerType', $role);


		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->session->set_userdata('post_referrerID', $referrer);
			$this->session->set_userdata('post_role', $role);

			redirect('/account/view/topic/' . $topicid . '/' . $role);

		}

		$data['EncodedID'] = $topicid;
		$data['role'] = '';
		$data['referrer'] = $referrer;
		if (isset($role)) {
			if ($role == 'follower') {
				$data['role'] = 4;
			} else if ($role == 'member') {
				$data['role'] = 3;
			} else if ($role == 'admin') {
				$data['role'] = 2;
			} else if ($role == 'superadmin') {
				$data['role'] = 1;
			}
		}
		$data['topics_items'] = $this->topics_model->get_topic(decode_id($topicid));
		$data['member_info'] = $this->members_model->get_member_information(decode_id($topicid), 'topic');

		if ($data['topics_items'] == null) {
			redirect('/account/profile');
			return;
		}

		$data['ID'] = decode_id($topicid);

		$data['referrer'] = $referrer;


		$data['asset'] = 'account/view/topic';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/info_navigation', $data);
		$this->load->view('templates/topic_header', $data);
		$this->load->view('templates/footer', $data);


	}

	public function postinfo($postid, $slug, $referrer = NULL, $role = NULL)
	{

		$promotedPost  = 0;
		$this->db->select('bs_posts.*');
		$this->db->from('bs_posts');
		$this->db->where('Po_Slug', $slug);
		$query1 = $this->db->get()->result_array();


		if(count($query1) == 0){
			$this->db->select('bs_posts.*');
			$this->db->from('bs_posts');
			$this->db->where('Po_ID' , decode_id($slug));

			$query1 = $this->db->get()->result_array();

		}
		if (count($query1) > 0) {
			$id = encode_id($query1[0]['Po_ID']);

			$this->db->select('promoted_post.Pp_ID');
			$this->db->from('promoted_post');
			$this->db->where('Pp_Post_ID' , $query1[0]['Po_ID']);
			$promotepost = 	$this->db->get()->result_array();

			if(count($promotepost) > 0){
				$promotedPost = 1;
			}
		}

		$data['promotedPost'] = $promotedPost;

		$this->session->set_flashdata('registerType', $role);
		// After that you need to used redirect function instead of load view such as


		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->session->set_userdata('post_referrerID', $referrer);
			$this->session->set_userdata('post_role', $role);


			redirect('/account/view/post/' . encode_id($postid) . '/' . $role);
		}

		$data['EncodedID'] = encode_id($postid);

		$data['ID'] = $postid;


		$this->db->select('bs_posts_poll.*');
		$this->db->from('bs_posts_poll');
		$this->db->where('Pl_PO_ID' , $postid);

		$data['polldata'] =	$this->db->get()->result_array();
		$data['totalreview'] =  array();
		$data['reviwer'] =  array();
		if(isset($data['polldata'][0]['PL_answers']) ){
			$array = explode('^', $data['polldata'][0]['PL_answers']);
			foreach ($array as $roww){
				$this->db->select('bs_poll_ans.* , bs_users.*');
				$this->db->from('bs_poll_ans');
				$this->db->join('bs_users' , 'bs_poll_ans.Poa_Us_ID = bs_users.Us_ID' );
				$this->db->where('Poa_ans' , $roww);
				$this->db->where('Poa_pl_id' , $data['polldata'][0]['Pl_ID']);
				$resss = $this->db->get()->result_array();
				$datas =  array($roww => count($resss));
				$datas1 =  array($roww =>  $resss);

				array_push(	$data['totalreview'] , $datas);
				array_push(	$data['reviwer'] , $datas1);
			}
			$data['totaldata'] = 0;

			foreach ($data['totalreview'] as $val){
				foreach ($val as $key => $vai){
					$data['totaldata'] += $vai;
				}
			}
		}

		$this->db->select('bs_poll_ans.*');
		$this->db->from('bs_poll_ans');
		$this->db->where('Poa_po_id' , $postid);
		$data['polldatacount'] =	$this->db->get()->result_array();
		$this->db->select('bs_poll_ans.*');
		$this->db->from('bs_poll_ans');
		$this->db->where('Poa_po_id' , $id);

		$data['polluserDetail'] =	$this->db->get()->result_array();




		$this->db->select('bs_voucher_post.* , bs_posts.*');
		$this->db->from('bs_voucher_post');
		$this->db->join('bs_posts' , 'bs_posts.Po_ID  = Vp_post_id' );
		$this->db->where('Vp_id' , $query1[0]['Po_reward']);


		$respons = $this->db->get()->result_array();

		$data['rewarddetails'] = $respons;


		$data['role'] = '';
		$data['referrer'] = $referrer;
		if (isset($role)) {
			if ($role == 'follower') {
				$data['role'] = 4;
			} else if ($role == 'member') {
				$data['role'] = 3;
			} else if ($role == 'admin') {
				$data['role'] = 2;
			} else if ($role == 'superadmin') {
				$data['role'] = 1;
			}
		}

		$data['posts_item'] = $this->posts_model->get_Post($postid);
		if ($data['posts_item'] == null) {
			redirect('/account/profile');
			return;
		}


		$data['userPostLike'] = $this->users_model->getuserLike($data['posts_item']['Po_Us_ID'], $postid);
		$data['userfav'] = $this->users_model->getuserfav($data['posts_item']['Po_Us_ID'], $postid);

		$data['member_counter'] = count($this->groups_model->counterofMember($data['posts_item']['Po_Gr_ID']));

		$data['user_data'] = $this->users_model->get_Users($data['posts_item']['Po_Us_ID']);

		$data['posts_files'] = $this->medias_model->get_PostMedia($postid, 'files');
		$data['posts_images'] = $this->medias_model->get_PostMedia($postid, 'images');
		$data['member_info'] = $this->members_model->get_member_information($postid, 'group');
		$data['is_followed'] = $this->follower_model->check_if_followed($data['posts_item']['Po_ID'], 'post');


		$data['member_info_post'] = $this->members_model->get_member_information_POST($postid, $data['posts_item']['Po_Gr_ID']);
		$data['userType'] = $data['member_info_post']['role'];

 		$data['comments_items'] = $this->comments_model->get_Comments($postid);
		$data['backcolors_items'] = $this->settings_model->get_Backcolor();
		$data['backgrounds_items'] = $this->settings_model->get_Background();



		$data['ShowAdminAction'] = "d-none";
		$data['asset'] = 'account/view/post';
//		print_r(	$data['promotedPost']);
//		exit;
		$this->load->view('templates/header', $data);
		$this->load->view('templates/info_navigation', $data);
		$this->load->view('templates/post_header', $data);
		if($data['promotedPost'] == 1){
			$this->load->view('account/post' , $data);
		}

		$this->load->view('templates/footer', $data);


	}

	public function info($encodedID, $referrer = NULL, $role = NULL)
	{


		if (isset($this->session->userdata['logged_in']['bs_id'])) {
			$this->session->set_userdata('referrerID', $referrer);
			redirect('/account/view/group/' . $encodedID);
		}

		$this->db->select('bs_groups.*');
		$this->db->from('bs_groups');
		$this->db->where('Gr_Slug', $encodedID);
		$res = $this->db->get()->result_array();

		if (count($res) == 0) {

			redirect('/home');
		}

		$id = $res[0]['Gr_ID'];
		$data['EncodedID'] = encode_id($id);


		$data['ID'] = $id;
		$data['role'] = '';
		$data['referrer'] = $referrer;
		if (isset($role)) {
			if ($role == 'follower') {
				$data['role'] = 4;
			} else if ($role == 'member') {
				$data['role'] = 3;
			} else if ($role == 'admin') {
				$data['role'] = 2;
			} else if ($role == 'superadmin') {
				$data['role'] = 1;
			}
		}


		$option_record = $this->groups_model->get_Record($id);
		$data['groups_items'] = $option_record;

		$data['member_counter'] = count($this->groups_model->counterofMember($id));

		$data['asset'] = 'account/view/group';
		$this->load->view('templates/header', $data);
		$this->load->view('templates/info_navigation', $data);
		$this->load->view('templates/group_header', $data);
		$this->load->view('templates/footer', $data);
	}

	public function add_to_group_by_link()
	{
		$this->view = false;

		$data = $this->input->post();

		if (isset($data['encoded'])) {
			$groupID = decode_id($data['group_id']);
		} else {
			$groupID = $data['group_id'];
		}

		if (isset($this->session->userdata['referrerID'])) {
			$this->session->unset_userdata('referrerID');
		}
		$role = 'member';
		if (isset($data['role'])) {

			$role = $data['role'];
		}
		$response = $this->members_model->add_to_group_by_link($groupID, $this->session->userdata['logged_in']['bs_id'], $data['referrer'], $role);
		echo json_encode($response);
	}

	public function show_group_permissions()
	{
		$this->view = false;

		$data = $this->input->post();

		$group = decode_id($data['group_id']);
		$role = $data['role'];

		$response = $this->permission_model->show_group_role_permission($group, $role);
		echo json_encode($response);
	}

	public function save_group_permissions()
	{
		$this->view = false;

		$data = $this->input->post();

		$group = decode_id($data['group_id']);
		$role = $data['role'];
		$permissions = $data['permission'];
		parse_str($data['permission'], $permissions);

		$response = $this->permission_model->save_group_role_permission($group, $role, $permissions);

		echo json_encode($response);
	}
}
